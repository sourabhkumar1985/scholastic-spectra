package com.rl.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import com.rl.email.Mail;

public class DiUtils {
	
	public String getHostName() throws UnknownHostException {
		return InetAddress.getLocalHost().getHostName();
	}

	
	public String getFormattedDate() {
		SimpleDateFormat DateFor = new SimpleDateFormat("ddMMyyyy_hhmmss");
		return DateFor.format(new Date());
	}
	
	
	public void sendExceptionEmail(Exception e,Properties prop) {
		Mail mail = new Mail();
		String p = "";
		for (int i = 0;i< e.getStackTrace().length;i++) {	
			 p =p +e.getStackTrace()[i] + " \n";
		}
		String s = e.toString() + "\n" +
				"Messgae :" + e.getMessage() + "\n" +
				"cause :" + e.getCause() + "\n" +
				"Stacktrace : " + p;
		
		try {
			
			mail.sendEmail(prop,s,"Failed");
			
		}catch (Exception ep) {
			ep.printStackTrace();
		}
		
		
	}	
	
	public void sendSuccessEamil(Properties prop,String message,String subject) {
		Mail mail = new Mail();
		try {	
			
			mail.sendEmail(prop,message,subject);
			
		}catch (Exception ep) {
			ep.printStackTrace();
		}
		
	}
	
}

