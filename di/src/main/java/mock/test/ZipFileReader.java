package mock.test;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.util.Progressable;

public class ZipFileReader {

	private final static Long MILLS_IN_DAY = 86400000L;
	public static void main(String args[]) throws Exception {
		BufferedWriter bw = null;

        byte[] buffer = new byte[2048];
        int cnt = 0;
        Path outDir = Paths.get("src/resources/output/");
        String zipFileName = "C:\\Users\\Bhuvan\\Desktop\\test_zip\\file_fscmtopmodelam_egpitemspublicmodelam_inventoryitem-batch614363067-20200514_151109.zip";
        String readLine = "";
        
    	Configuration config = new Configuration(); 
    	FileSystem hdfs = FileSystem.get(new URI("hdfs://rlmaster:8020"), config);
    	org.apache.hadoop.fs.Path file = new org.apache.hadoop.fs.Path("hdfs://rlmaster:8020/tmp/tttte5.csv");

    	  final int[] i={0};

        

    	OutputStream os = hdfs.create( file,
    	
         	    new Progressable() {
						
						@Override
						public void progress() {
							 System.out.println("...bytes written:" + i[0]++);								
						}
					});
    bw = new BufferedWriter( new OutputStreamWriter( os, "UTF-8" ),258 );


    	
        try (FileInputStream fis = new FileInputStream(zipFileName);
                BufferedInputStream bis = new BufferedInputStream(fis);
                ZipInputStream stream = new ZipInputStream(bis)) {
        	ZipEntry entry;
        	while ((entry = stream.getNextEntry()) != null) {
        	BufferedReader br = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
        	
        	        	
        	while ((readLine = br.readLine()) != null) {
                //System.out.println(readLine);
                String[] splitStir = readLine.split("\",\"");
                StringBuilder sb = new StringBuilder();
                splitStir[0] = splitStir[0].replaceFirst("\"", "");
            	if (splitStir[splitStir.length -1].endsWith("\"")) {
            		splitStir[splitStir.length -1] = splitStir[splitStir.length -1].substring(0,splitStir[splitStir.length -1].length() -1);   		
            	}
                for (int k = 0;k<splitStir.length;k++) {
                	
                	sb.append(splitStir[k]);
                	sb.append("|");
                	
                }
               // System.out.println(sb);
                System.out.println(sb.toString());
                bw.write(sb.toString());
                bw.write("\n");
                cnt ++;
                System.out.println(cnt);
                
        	}
        	}
        	bw.flush();
        	bw.close();
        	
        	

       /*     ZipEntry entry;
            while ((entry = stream.getNextEntry()) != null) {

                Path filePath = outDir.resolve(entry.getName());

                try (FileOutputStream fos = new FileOutputStream(filePath.toFile());
                        BufferedOutputStream bos = new BufferedOutputStream(fos, buffer.length)) {

                    int len;
                    while ((len = stream.read(buffer)) > 0) {
                        bos.write(buffer, 0, len);
                    }
                }
            }*/
        }
    }
}


