package mock.test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class TestBackSlash {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		Properties prop = null;
		
		try (InputStream input = new FileInputStream("conf/oracle.properties")) {
			prop = new Properties();
			prop.load(input);
					}catch (Exception e) {
			e.printStackTrace();
		}
		String mysql = prop.getProperty("mysql");
		String mysqlUsername = prop.getProperty("mysql_username");
		String mysqlPassword = prop.getProperty("mysql_password");
		Class.forName("com.mysql.jdbc.Driver");  
		Connection conn = DriverManager.getConnection(  
				mysql,mysqlUsername,mysqlPassword);
		
		
		
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select * from di_files" );
		
		while(rs.next()) {
			//System.out.println(rs.getString("file_name"));
			
			System.out.println(formatFileName(rs.getString("file_name")));
		}
		
		
	
	}
	
	public static String formatFileName(String s) {
		
		if (s.contains("/")) {
			
			s = s.replace("/", "_");
			System.out.println("File name contains / , changes to _");
			
		}
		return s;

		
	}
	
	

}
