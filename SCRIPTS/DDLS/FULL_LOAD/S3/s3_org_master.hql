DROP TABLE

IF EXISTS rlsf_prod.s3_org_master;
	CREATE EXTERNAL TABLE rlsf_prod.s3_org_master (
		oiorgid STRING
		,oidescr STRING
		,oicode STRING
		,oitype STRING
		) partitioned by (run_date string) STORED AS PARQUET LOCATION 's3a://schl-ams-nonprod-spectra/Data/org_master';