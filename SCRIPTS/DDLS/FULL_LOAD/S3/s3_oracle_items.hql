DROP TABLE

IF EXISTS rlsf_prod.s3_oracle_items;
	CREATE EXTERNAL TABLE rlsf_prod.s3_oracle_items (
		oracle_id String
		,title String
		,isbn13 String
		,inventory_item_flag String
		,item_type String
		,inventory_asset_flag String
		,costing_enabled_flag String
		,short_id String
		,mdm_id String
		,creation_dt String
		,inventory_item_id String
		,opus_num String
		,bf_num String
		) partitioned by (run_date string) STORED AS PARQUET LOCATION 's3a://schl-ams-nonprod-spectra/Data/oracle_items';