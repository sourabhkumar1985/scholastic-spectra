DROP TABLE IF EXISTS rlsf_prod.le_lob_channels;
CREATE EXTERNAL TABLE rlsf_prod.le_lob_channels (
country STRING,
legal_entity STRING,
legal_entity_description STRING,
line_of_business STRING,
line_of_business_description STRING,
inventory STRING,
major_channel STRING,
channel_description STRING,
owner STRING,
product_owner STRING,
product_owner_desc STRING
) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' STORED AS TEXTFILE LOCATION '/sch/l0/le_lob_channels';
