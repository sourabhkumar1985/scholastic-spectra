DROP TABLE IF EXISTS rlsf_prod.ohq_po;
CREATE EXTERNAL TABLE rlsf_prod.ohq_po (
oracle_item STRING,
isbn13 STRING,
border_item STRING,
opus_id STRING,
cost_center STRING,
org_id STRING,
rec_trans_id STRING,
inventory_item_id STRING,
total_quantity decimal(38,6),
unit_cost decimal(38,6),
total_amount decimal(38,6),
po_number STRING,
transaction_type_name STRING,
layer_date STRING,
date_ STRING,
run_date STRING,
as_of_date STRING
) STORED AS PARQUET LOCATION '/sch/l3/ohq_po';