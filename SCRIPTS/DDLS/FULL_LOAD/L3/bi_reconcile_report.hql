DROP TABLE

IF EXISTS rlsf_prod.bi_reconcile_report;
	CREATE EXTERNAL TABLE rlsf_prod.bi_reconcile_report (
		p_cost_org_name String
		,p_cost_book_name String
		,p_as_of_date String
		,p_val_unit_code String
		,p_item String
		,p_category String
		,item_code String
		,category_name String
		,val_unit_code String
		,meaning String
		,source_ref String
		,cost_element_code String
		,uom_code String
		,quantity DECIMAL(38, 6)
		,unit_cost DECIMAL(38, 6)
		,value DECIMAL(38, 6)
		,functional_currency String
		,rec_trxn_id String
		,sum_quantity String
		,val_unit_id String
		,rank String
		,organization_id String
		) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|'
		WITH SERDEPROPERTIES('field.delim' = '|', 'serialization.format' = '|') STORED AS TEXTFILE LOCATION '/sch/l0/bi_reconcile_report';