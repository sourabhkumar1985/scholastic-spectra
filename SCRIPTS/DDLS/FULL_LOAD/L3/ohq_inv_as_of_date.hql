DROP TABLE IF EXISTS rlsf_prod.ohq_inv_as_of_date;
CREATE EXTERNAL TABLE rlsf_prod.ohq_inv_as_of_date (   oracle_item STRING,
   isbn13 STRING,
   border_item STRING,
   opus_id STRING,
   org_id STRING,
   cost_center STRING,
   inventory_item_id STRING,
   total_qty DECIMAL(38,6),   run_date STRING ) STORED AS PARQUET LOCATION '/sch/l3/ohq_inv_as_of_date';
