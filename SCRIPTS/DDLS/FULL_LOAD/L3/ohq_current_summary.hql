DROP TABLE

IF EXISTS rlsf_prod.ohq_current_summary;
	CREATE EXTERNAL TABLE rlsf_prod.ohq_current_summary (
		oracle_item STRING
		,cost_center STRING
		,rec_trans_id string
		,unit_cost DECIMAL(38, 6)
		,tot_qty DECIMAL(38, 6)
		,tot_amount DECIMAL(38, 6)
		) STORED AS PARQUET LOCATION '/sch/l3/ohq_current_summary';