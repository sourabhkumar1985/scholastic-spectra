DROP TABLE

IF EXISTS rlsf_prod.cost_trans_debit;
	CREATE EXTERNAL TABLE rlsf_prod.cost_trans_debit (
		costdistributionlineseocostelementid STRING
		,distributionlineid STRING
		,transactionseoinventoryitemid STRING
		,costdistributionseolayerquantity STRING
		,transactionseoinventoryorgid STRING
		,transactionseocreationdate STRING
		,transactionseocstinvtransactionid STRING
		,cstlayercostspeorectrxnid STRING
		,cstlayercostspeocosttransactiontype STRING
		,costdistributionlineseoenteredcurrencyamount STRING
		,cstlayercostspeounitcost STRING
		,dist_lines_last_update_date STRING
		,txn_external_system_ref_id STRING
		,layer_unit_cost DECIMAL(38, 6)
		,cstlayercosttransid STRING
		,po_number STRING
		,transaction_type_name STRING
		) STORED AS PARQUET LOCATION '/sch/l1/cost_trans_debit';