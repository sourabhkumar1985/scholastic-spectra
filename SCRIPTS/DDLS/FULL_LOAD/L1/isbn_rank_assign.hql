DROP TABLE IF EXISTS rlsf_prod.ISBN_rank_assign;
CREATE EXTERNAL TABLE rlsf_prod.isbn_rank_assign (   JRNL_BATCH_NAME STRING,
   ranknumber BIGINT ) STORED AS PARQUET LOCATION '/sch/l1/ISBN_rank_assign';
