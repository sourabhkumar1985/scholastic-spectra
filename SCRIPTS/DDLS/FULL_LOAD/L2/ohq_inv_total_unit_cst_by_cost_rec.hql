DROP TABLE IF EXISTS rlsf_prod.ohq_inv_total_unit_cst_by_cost_rec;
CREATE EXTERNAL TABLE rlsf_prod.ohq_inv_total_unit_cst_by_cost_rec (   trans_eo_inventory_item_id STRING,
   lc_p_eo_rec_trans_id STRING,
   date_ STRING,
   oracle_id STRING,
   cost_unit DECIMAL(38,6) ) 
   STORED AS PARQUET LOCATION '/sch/l2/ohq_inv_total_unit_cst_by_cost_rec' ;