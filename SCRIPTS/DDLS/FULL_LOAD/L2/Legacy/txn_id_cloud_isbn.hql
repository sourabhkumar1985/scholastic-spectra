DROP TABLE IF EXISTS rlsf_prod.txn_id_cloud_isbn;
CREATE EXTERNAL TABLE rlsf_prod.txn_id_cloud_isbn (
transaction_id STRING,
short_id STRING,
item STRING,
isbn13 STRING,
title STRING,
legacy_reason STRING,
transaction_date STRING,
txn_id_status STRING,
qty_cloud INT,
qty_hadoop INT,
qty_variance INT,
qty_status STRING
) STORED AS PARQUET LOCATION '/sch/l2/txn_id_cloud_isbn';