DROP TABLE IF EXISTS rlsf_prod.le_lob_wms_adjustment;
CREATE EXTERNAL TABLE rlsf_prod.le_lob_wms_adjustment (
run_date STRING,
item STRING,
isbn13 STRING,
title STRING,
source STRING,
legal_entity STRING,
line_of_business STRING,
layer_quantity decimal(38,6),
split_percent decimal(38,6),
split_quantity decimal(38,6),
final_quantity decimal(38,6)
) STORED AS PARQUET LOCATION '/sch/l2/le_lob_wms_adjustment';