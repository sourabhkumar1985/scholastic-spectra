DROP TABLE

IF EXISTS rlsf_prod.lite_cost_reconcile;
	CREATE EXTERNAL TABLE rlsf_prod.lite_cost_reconcile (
		transaction_date STRING
		,lite_txn_ref STRING
		,cost_txn_ref STRING
		,ref_status STRING
		,lite_org STRING
		,cost_org STRING
		,org_status STRING
		,cost_center STRING
		,lite_item_id STRING
		,cost_item_id STRING
		,inv_status STRING
		,oracle_id STRING
		,item_type STRING
		) STORED AS PARQUET LOCATION 'hdfs://nameservice1/sch/l4/lite_cost_reconcile';