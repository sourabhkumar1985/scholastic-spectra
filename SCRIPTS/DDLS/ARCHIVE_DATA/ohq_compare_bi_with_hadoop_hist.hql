DROP TABLE IF EXISTS rlsf_prod.ohq_compare_bi_with_hadoop_hist;
CREATE EXTERNAL TABLE rlsf_prod.ohq_compare_bi_with_hadoop_hist (   oracle_item_number STRING,
   inventory_organization STRING,
   bi_extract_qty DECIMAL(38,6),
   hadoop_extract_qty DECIMAL(38,6),
   hadoop_inventory_item_id STRING,
   hadoop_org_id STRING,
   diff_state STRING,
   diff_value DECIMAL(38,6),
   diff_value_abs DECIMAL(38,6),
   item_type string
   ) PARTITIONED BY (run_date STRING) STORED AS PARQUET LOCATION '/sch/l3/ohq_compare_bi_with_hadoop_hist' ;