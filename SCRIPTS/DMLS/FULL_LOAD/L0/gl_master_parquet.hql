INVALIDATE METADATA rlsf_prod.gl_master;

INSERT INTO TABLE rlsf_prod.gl_master_parquet
SELECT *
FROM rlsf_prod.gl_master;

TRUNCATE rlsf_prod.gl_master;