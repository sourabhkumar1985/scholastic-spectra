INSERT OVERWRITE TABLE rlsf_prod.ohq_vs_isbn_reconcile 

with ohq as (
select oracle_item, cost_center, lc_p_eo_rec_trans_id,inventory_item_id,org_id, sum(total_qty) as total_qty
FROM rlsf_prod.ohq_inv_as_of_date_with_layer_cost
where substr(date_,1,10)<='2020-12-09'
group by oracle_item, cost_center, lc_p_eo_rec_trans_id,inventory_item_id,org_id
)
,
isbn as (
(SELECT item, organization_code, rec_trxn_id, title, sum(layer_quantity) layer_quantity 
FROM rlsf_prod.isbn_activity_summary GROUP BY item, organization_code, rec_trxn_id, title)
)

SELECT 
CASE WHEN ohq.oracle_item IS NULL THEN isbn.item 
ELSE ohq.oracle_item 
END oracle_item, 
CASE WHEN inv.description IS NULL THEN isbn.title 
ELSE inv.description 
END title, 
ohq.inventory_item_id inv_item_id, 
CASE WHEN ohq.cost_center IS NULL THEN isbn.organization_code 
ELSE ohq.cost_center 
END cost_center, 
ohq.org_id, 
ohq.lc_p_eo_rec_trans_id ohq_layer, 
isbn.rec_trxn_id activity_layer, 
if(ohq.lc_p_eo_rec_trans_id = isbn.rec_trxn_id, 'Matched', 'NA') layer_status, 
isbn.layer_quantity sum_qty_activity, 
ohq.total_qty sum_qty_ohq, 
abs(ohq.total_qty - isbn.layer_quantity) diff_qty, 
if(ohq.total_qty = isbn.layer_quantity, 'Matched', 'Not Matched') qty_status, 
substr(CAST(now() AS STRING), 1, 10) Run_Date, 
substr(substr(CAST(now() AS STRING), 12, 19), 1, 8) run_Time 
FROM ohq 
FULL OUTER JOIN isbn 
ON ohq.oracle_item = isbn.item AND ohq.cost_center = isbn.organization_code AND ohq.lc_p_eo_rec_trans_id = isbn.rec_trxn_id 
LEFT OUTER JOIN rlsf_prod.inv_master_distinct_parquet inv ON inv.oracle_item_id = ohq.oracle_item
;
