refresh rlsf_prod.material_transactions;

INSERT OVERWRITE TABLE rlsf_prod.lite_cost_reconcile
SELECT DISTINCT substr(m.transaction_date, 1, 10) transaction_date
	,m.txn_id lite_txn_ref
	,c.txn_external_system_ref_id cost_txn_ref
	,CASE 
		WHEN trim(c.txn_external_system_ref_id) = trim(m.txn_id)
			THEN 'Ref Available'
		WHEN m.txn_id IS NOT NULL
			AND c.txn_external_system_ref_id IS NULL
			THEN 'Not-Available'
		WHEN m.txn_id IS NULL
			AND c.txn_external_system_ref_id IS NOT NULL
			THEN 'Lite_Ref Not-Available'
		ELSE 'Not-Available'
		END ref_status
	,m.organization_id lite_org
	,c.org_id cost_org
	,CASE 
		WHEN c.org_id = m.organization_id
			THEN 'Org Available'
		WHEN m.organization_id IS NOT NULL
			AND c.org_id IS NULL
			THEN 'Cost_Org Not-Available'
		WHEN m.organization_id IS NULL
			AND c.org_id IS NOT NULL
			THEN 'Lite_Org Not-Available'
		ELSE 'Not-Available'
		END org_status
	,o.oicode cost_center
	,m.item_id lite_item_id
	,c.inventory_item_id cost_item_id
	,CASE 
		WHEN m.item_id = c.inventory_item_id
			THEN 'Inv Available'
		WHEN m.item_id IS NOT NULL
			AND c.inventory_item_id IS NULL
			THEN 'Cost_Inv Not-Available'
		WHEN m.item_id IS NULL
			AND c.inventory_item_id IS NOT NULL
			THEN 'Lite_Inv Not-Available'
		ELSE 'Not-Available'
		END Inv_status
	,oi.oracle_id
	,oi.item_type
FROM (
	SELECT *
	FROM rlsf_prod.material_transactions
	WHERE costed_flag != 'N'
	) m
LEFT OUTER JOIN (
	SELECT DISTINCT txn_external_system_ref_id
		,org_id
		,inventory_item_id
	FROM rlsf_prod.cost_trans_prod_allorgs_hist
	) c ON c.txn_external_system_ref_id = m.txn_id
	AND c.inventory_item_id = m.item_id
	AND c.org_id = m.organization_id
LEFT OUTER JOIN rlsf_prod.org_master_parquet o ON o.oiorgid = c.org_id
LEFT OUTER JOIN rlsf_prod.oracle_items_parquet oi ON oi.inventory_item_id = m.item_id
WHERE m.txn_id NOT IN ('TXN_ID');