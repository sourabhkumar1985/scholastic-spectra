INSERT OVERWRITE TABLE rlsf_prod.ohq_lvr_compare_8_24
SELECT DISTINCT 
a.oracle_item hadoop_item_id, 
a.cost_center hadoop_cost_center, 
o.isbn13 ISBN13, 
o.short_id border_item, 
o.title description, 
o.opus_num opus_item, 
o.inventory_item_flag inv, 
o.costing_enabled_flag costed, 
o.inventory_asset_flag asset, 
o.item_type, 
o.Category_code, 
o.cat_description, 
o.catalog_code, 
b.orc_item cloud_item_id, 
round(a.tot_qty, 2) hadoop_tot_qty, 
round(b.quantity, 2) cloud_quantity, 
CASE WHEN a.oracle_item = b.orc_item AND round(a.tot_qty, 2) = round(b.quantity, 2) THEN 'Matched' 
WHEN a.oracle_item IS NULL AND a.cost_center IS NULL THEN 'NA – Hadoop' 
WHEN b.orc_item IS NULL THEN 'NA - Cloud' ELSE 'Not Matched' 
END quantity_status, 
(abs(round(a.tot_qty, 2)) - abs(round(b.quantity, 2))) diff_qty, 
abs((abs(round(a.tot_qty, 2)) - abs(round(b.quantity, 2)))) abs_diff_qty, 
round(unit_cost, 2) hadoop_unit_cost, 
b.cost cloud_unit_cost, 
a.tot_amount hadoop_tot_amount, 
b.amt cloud_amount, 
CASE WHEN a.oracle_item = b.orc_item AND round(a.tot_amount, 2) = round(b.amt, 2) THEN 'Matched' 
WHEN a.oracle_item IS NULL AND a.cost_center IS NULL THEN 'NA – Hadoop' 
WHEN b.orc_item IS NULL THEN 'NA - Cloud' ELSE 'Not Matched' 
END amount_status, 
(abs(round(a.tot_amount, 2)) - abs(round(b.amt, 2))) diff_amount, 
abs((round(a.tot_amount, 2) - round(b.amt, 2))) abs_diff_amount 
FROM (SELECT oracle_item, cost_center, avg(unit_cost) unit_cost, sum(total_qty) tot_qty, sum(total_amount) tot_amount 
FROM rlsf_prod.ohq_inv_as_of_date_with_layer_cost 
WHERE cost_center = 'JCD' GROUP BY oracle_item, cost_center) a 
FULL OUTER JOIN rlsf_prod.bi_reconcile_report_003 b ON trim(a.oracle_item) = trim(b.orc_item) 
LEFT OUTER JOIN rlsf_prod.oracle_items_parquet o ON trim(o.oracle_id) = trim(a.oracle_item)
;