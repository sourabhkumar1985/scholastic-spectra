SET DECIMAL_V2 = false;

INSERT overwrite TABLE rlsf_prod.ohq_po
WITH gl AS (
		SELECT DISTINCT inventory_org_id
			,rec_trxn_id
			,inventory_item_id
			,source_reference
			,inv_transaction_type
		FROM rlsf_prod.gl_master_parquet
		WHERE distribution_dr_cr_sign = 'DR'
			AND inv_transaction_type IN (
				'Purchase Order Receipt'
				,'Purchase Order Receipt Adjustment'
				,'Work in Process Product Completion'
				)
		)
	,recp AS (
		SELECT *
		FROM gl
		WHERE inv_transaction_type IN ('Purchase Order Receipt')
		)
	,recp_adj AS (
		SELECT *
		FROM recp
		
		UNION
		
		SELECT *
		FROM gl left anti
		JOIN recp ON gl.inventory_org_id = recp.inventory_org_id
			AND gl.rec_trxn_id = recp.rec_trxn_id
			AND gl.inventory_item_id = recp.inventory_item_id
			AND gl.source_reference = recp.source_reference
		WHERE gl.inv_transaction_type IN (
				'Purchase Order Receipt Adjustment'
				,'Work in Process Product Completion'
				)
		)
SELECT oracle_item
	,ohq.isbn13
	,border_item
	,opus_id
	,cost_center
	,org_id
	,org.organization_name
	,lc_p_eo_rec_trans_id
	,ohq.inventory_item_id AS inventory_item_id
	,cast(total_qty as bigint)
	,cast(unit_cost as decimal(38,2))
	,cast(total_amount as decimal(38,2))
	,recp_adj.source_reference AS po_number
	,recp_adj.inv_transaction_type AS transaction_type_name
	,layer_date
	,substr(layer_date, 1, 4) AS year_
	,CASE 
		WHEN substr(layer_date, 6, 2) = '01'
			THEN 'Jan'
		WHEN substr(layer_date, 6, 2) = '02'
			THEN 'Feb'
		WHEN substr(layer_date, 6, 2) = '03'
			THEN 'Mar'
		WHEN substr(layer_date, 6, 2) = '04'
			THEN 'Apr'
		WHEN substr(layer_date, 6, 2) = '05'
			THEN 'May'
		WHEN substr(layer_date, 6, 2) = '06'
			THEN 'Jun'
		WHEN substr(layer_date, 6, 2) = '07'
			THEN 'Jul'
		WHEN substr(layer_date, 6, 2) = '08'
			THEN 'Aug'
		WHEN substr(layer_date, 6, 2) = '09'
			THEN 'Sep'
		WHEN substr(layer_date, 6, 2) = '10'
			THEN 'Oct'
		WHEN substr(layer_date, 6, 2) = '11'
			THEN 'Nov'
		WHEN substr(layer_date, 6, 2) = '12'
			THEN 'Dec'
		ELSE ''
		END month_
	,date_
	,substr(run_date,1,16) as run_date
	,as_of_date
	,og1.category
FROM rlsf_prod.ohq_inv_as_of_date_with_layer_cost ohq
LEFT JOIN recp_adj ON ohq.org_id = recp_adj.inventory_org_id
	AND ohq.lc_p_eo_rec_trans_id = recp_adj.rec_trxn_id
	AND ohq.inventory_item_id = recp_adj.inventory_item_id
LEFT JOIN rlsf_prod.inventory_orgs_parquet org ON org.organization_code = ohq.cost_center
LEFT JOIN rlsf_prod.org_category og1 ON og1.org = ohq.cost_center;