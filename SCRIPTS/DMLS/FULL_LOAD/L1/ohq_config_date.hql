insert overwrite  table rlsf_prod.ohq_config_date
select 
cast(now() as string) as run_date,
substr(cast(now() as string),1,10) as as_of_date;
