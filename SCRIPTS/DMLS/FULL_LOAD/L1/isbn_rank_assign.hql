insert overwrite table rlsf_prod.isbn_rank_assign
select 
period,
row_number() OVER (
    ORDER BY period) as period  
from (select distinct   
period
from rlsf_prod.gl_master_parquet ) ll;