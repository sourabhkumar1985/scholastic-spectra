set decimal_v2=false;
insert overwrite table rlsf_prod.po_split_percent

with po_split_ext as (
select distinct purchase_order_number,lob_forecast,split_1,split_2,split_3,split_4,split_5,split_6,split_7,split_8 
from rlsf_prod.po_split_ext
)
,
split_tot_qty as (
select purchase_order_number,
sum(if(split_part(lob_forecast,'-',2) is null or split_part(lob_forecast,'-',2) in ('','NULL'),0,cast(split_part(lob_forecast,'-',2) as decimal(38,6))) +
if(split_part(split_1,'-',2) is null or split_part(split_1,'-',2) in ('','NULL'),0,cast(split_part(split_1,'-',2) as decimal(38,6))) +
if(split_part(split_2,'-',2) is null or split_part(split_2,'-',2) in ('','NULL'),0,cast(split_part(split_2,'-',2) as decimal(38,6))) +
if(split_part(split_3,'-',2) is null or split_part(split_3,'-',2) in ('','NULL'),0,cast(split_part(split_3,'-',2) as decimal(38,6))) +
if(split_part(split_4,'-',2) is null or split_part(split_4,'-',2) in ('','NULL'),0,cast(split_part(split_4,'-',2) as decimal(38,6))) +
if(split_part(split_5,'-',2) is null or split_part(split_5,'-',2) in ('','NULL'),0,cast(split_part(split_5,'-',2) as decimal(38,6))) +
if(split_part(split_6,'-',2) is null or split_part(split_6,'-',2) in ('','NULL'),0,cast(split_part(split_6,'-',2) as decimal(38,6))) +
if(split_part(split_7,'-',2) is null or split_part(split_7,'-',2) in ('','NULL'),0,cast(split_part(split_7,'-',2) as decimal(38,6))) +
if(split_part(split_8,'-',2) is null or split_part(split_8,'-',2) in ('','NULL'),0,cast(split_part(split_8,'-',2) as decimal(38,6))) ) tot_qty
from po_split_ext
where lob_forecast  not in ('NULL','') group by purchase_order_number
)

select '' as po_header_id, '' as po_status, ext.purchase_order_number, '' as po_creation_date, '' as po_last_update_date, '' as document_style, '' as po_line_id, 
'' as po_line_quantity,  
--cast(quantity as decimal(38,6)) as quantity, 
0 as quantity,
case when lob_forecast like '%-%-%' then cast(split_part(lob_forecast,'-',3) as decimal (38,6))
else cast(split_part(lob_forecast,'-',2) as decimal (38,6)) end as lob_forecast,
case when lob_forecast like '%-%-%' then split_part(lob_forecast,'-',1)
else split_part(lob_forecast,'-',1) end as major_channel,
case when lob_forecast like '%-%-%' 
then cast(cast(split_part(lob_forecast,'-',3) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
else cast(cast(split_part(lob_forecast,'-',2) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
end as po_qty_percent
from po_split_ext ext
left join split_tot_qty tot on ext.purchase_order_number=tot.purchase_order_number
where lob_forecast  not in ('NULL','')

union all

select '' as po_header_id, '' as po_status, ext.purchase_order_number, '' as po_creation_date, '' as po_last_update_date, '' as document_style, '' as po_line_id, 
'' as po_line_quantity,  
--cast(quantity as decimal(38,6)) as quantity, 
0 as quantity, 
case when split_1 like '%-%-%' then cast(split_part(split_1,'-',3) as decimal (38,6))
else cast(split_part(split_1,'-',2) as decimal (38,6)) end as lob_forecast,
case when split_1 like '%-%-%' then split_part(split_1,'-',1)
else split_part(split_1,'-',1) end as major_channel,
case when split_1 like '%-%-%' 
then cast(cast(split_part(split_1,'-',3) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
else cast(cast(split_part(split_1,'-',2) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
end as po_qty_percent
from po_split_ext ext
left join split_tot_qty tot on ext.purchase_order_number=tot.purchase_order_number
where split_1  not in ('NULL','')

union all

select '' as po_header_id, '' as po_status, ext.purchase_order_number, '' as po_creation_date, '' as po_last_update_date, '' as document_style, '' as po_line_id, 
'' as po_line_quantity,  
--cast(quantity as decimal(38,6)) as quantity, 
0 as quantity, 
case when split_2 like '%-%-%' then cast(split_part(split_2,'-',3) as decimal (38,6))
else cast(split_part(split_2,'-',2) as decimal (38,6)) end as lob_forecast,
case when split_2 like '%-%-%' then split_part(split_2,'-',1)
else split_part(split_2,'-',1) end as major_channel,
case when split_2 like '%-%-%' 
then cast(cast(split_part(split_2,'-',3) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
else cast(cast(split_part(split_2,'-',2) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
end as po_qty_percent
from po_split_ext ext
left join split_tot_qty tot on ext.purchase_order_number=tot.purchase_order_number
where split_2 not in ('NULL','')

union all

select '' as po_header_id, '' as po_status, ext.purchase_order_number, '' as po_creation_date, '' as po_last_update_date, '' as document_style, '' as po_line_id, 
'' as po_line_quantity,  
--cast(quantity as decimal(38,6)) as quantity, 
0 as quantity,
case when split_3 like '%-%-%' then cast(split_part(split_3,'-',3) as decimal (38,6))
else cast(split_part(split_3,'-',2) as decimal (38,6)) end as lob_forecast,
case when split_3 like '%-%-%' then split_part(split_3,'-',1)
else split_part(split_3,'-',1) end as major_channel,
case when split_3 like '%-%-%' 
then cast(cast(split_part(split_3,'-',3) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
else cast(cast(split_part(split_3,'-',2) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
end as po_qty_percent
from po_split_ext ext
left join split_tot_qty tot on ext.purchase_order_number=tot.purchase_order_number
where split_3  not in ('NULL','')

union all

select '' as po_header_id, '' as po_status, ext.purchase_order_number, '' as po_creation_date, '' as po_last_update_date, '' as document_style, '' as po_line_id, 
'' as po_line_quantity,  
--cast(quantity as decimal(38,6)) as quantity, 
0 as quantity, 
case when split_4 like '%-%-%' then cast(split_part(split_4,'-',3) as decimal (38,6))
else cast(split_part(split_4,'-',2) as decimal (38,6)) end as lob_forecast,
case when split_4 like '%-%-%' then split_part(split_4,'-',1)
else split_part(split_4,'-',1) end as major_channel,
case when split_4 like '%-%-%' 
then cast(cast(split_part(split_4,'-',3) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
else cast(cast(split_part(split_4,'-',2) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
end as po_qty_percent
from po_split_ext ext
left join split_tot_qty tot on ext.purchase_order_number=tot.purchase_order_number
where split_4  not in ('NULL','')

union all

select '' as po_header_id, '' as po_status, ext.purchase_order_number, '' as po_creation_date, '' as po_last_update_date, '' as document_style, '' as po_line_id, 
'' as po_line_quantity,  
--cast(quantity as decimal(38,6)) as quantity, 
0 as quantity,
case when split_5 like '%-%-%' then cast(split_part(split_5,'-',3) as decimal (38,6))
else cast(split_part(split_5,'-',2) as decimal (38,6)) end as lob_forecast,
case when split_5 like '%-%-%' then split_part(split_5,'-',1)
else split_part(split_5,'-',1) end as major_channel,
case when split_5 like '%-%-%' 
then cast(cast(split_part(split_5,'-',3) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
else cast(cast(split_part(split_5,'-',2) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
end as po_qty_percent
from po_split_ext ext
left join split_tot_qty tot on ext.purchase_order_number=tot.purchase_order_number
where split_5  not in ('NULL','')

union all

select '' as po_header_id, '' as po_status, ext.purchase_order_number, '' as po_creation_date, '' as po_last_update_date, '' as document_style, '' as po_line_id, 
'' as po_line_quantity,  
--cast(quantity as decimal(38,6)) as quantity, 
0 as quantity,
case when split_6 like '%-%-%' then cast(split_part(split_6,'-',3) as decimal (38,6))
else cast(split_part(split_6,'-',2) as decimal (38,6)) end as lob_forecast,
case when split_6 like '%-%-%' then split_part(split_6,'-',1)
else split_part(split_6,'-',1) end as major_channel,
case when split_6 like '%-%-%' 
then cast(cast(split_part(split_6,'-',3) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
else cast(cast(split_part(split_6,'-',2) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
end as po_qty_percent
from po_split_ext ext
left join split_tot_qty tot on ext.purchase_order_number=tot.purchase_order_number
where split_6  not in ('NULL','')

union all

select '' as po_header_id, '' as po_status, ext.purchase_order_number, '' as po_creation_date, '' as po_last_update_date, '' as document_style, '' as po_line_id, 
'' as po_line_quantity,  
--cast(quantity as decimal(38,6)) as quantity, 
0 as quantity, 
case when split_7 like '%-%-%' then cast(split_part(split_7,'-',3) as decimal (38,6))
else cast(split_part(split_7,'-',2) as decimal (38,6)) end as lob_forecast,
case when split_7 like '%-%-%' then split_part(split_7,'-',1)
else split_part(split_7,'-',1) end as major_channel,
case when split_7 like '%-%-%' 
then cast(cast(split_part(split_7,'-',3) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
else cast(cast(split_part(split_7,'-',2) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
end as po_qty_percent
from po_split_ext ext
left join split_tot_qty tot on ext.purchase_order_number=tot.purchase_order_number
where split_7  not in ('NULL','')

union all

select '' as po_header_id, '' as po_status, ext.purchase_order_number, '' as po_creation_date, '' as po_last_update_date, '' as document_style, '' as po_line_id, 
'' as po_line_quantity,  
--cast(quantity as decimal(38,6)) as quantity, 
0 as quantity, 
case when split_8 like '%-%-%' then cast(split_part(split_8,'-',3) as decimal (38,6))
else cast(split_part(split_8,'-',2) as decimal (38,6)) end as lob_forecast,
case when split_8 like '%-%-%' then split_part(split_8,'-',1)
else split_part(split_8,'-',1) end as major_channel,
case when split_8 like '%-%-%' 
then cast(cast(split_part(split_8,'-',3) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
else cast(cast(split_part(split_8,'-',2) as decimal(38,6)) / tot.tot_qty as decimal(38,6))
end as po_qty_percent
from po_split_ext ext
left join split_tot_qty tot on ext.purchase_order_number=tot.purchase_order_number
where split_8  not in ('NULL','')
;