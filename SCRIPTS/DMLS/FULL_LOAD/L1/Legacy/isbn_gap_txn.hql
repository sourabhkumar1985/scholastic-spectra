with isbn_ct as(
select count(*), txn_external_system_ref_id,item,legacy_reason,
concat(trim(txn_external_system_ref_id),trim(item),trim(legacy_reason)) as isbn_group from (
select distinct txn_external_system_ref_id,short_id,item,legacy_reason, transaction_date
from rlsf_prod.isbn_activity_summary
)aa  group by txn_external_system_ref_id,item,legacy_reason having count(*)>1
)

INSERT OVERWRITE TABLE rlsf_prod.isbn_gap_txn

select txn_external_system_ref_id,short_id,item,isbn13,title,legacy_reason, transaction_date, sum(qty) as qty_hadoop,
concat(trim(txn_external_system_ref_id),trim(item),trim(legacy_reason)) as isbn_grp 
from rlsf_prod.isbn_activity_summary isbn
left anti join isbn_ct on isbn_ct.isbn_group=concat(trim(isbn.txn_external_system_ref_id),trim(isbn.item),trim(isbn.legacy_reason))
group by txn_external_system_ref_id,short_id,item,isbn13,title,legacy_reason,transaction_date,isbn_grp
;


with isbn_ct as(
select count(*), txn_external_system_ref_id,item,legacy_reason,
concat(trim(txn_external_system_ref_id),trim(item),trim(legacy_reason)) as isbn_group from (
select distinct txn_external_system_ref_id,short_id,item,legacy_reason, transaction_date
from rlsf_prod.isbn_activity_summary
)aa  group by txn_external_system_ref_id,item,legacy_reason having count(*)>1
)

INSERT into TABLE rlsf_prod.isbn_gap_txn

select isbn.txn_external_system_ref_id,short_id,isbn.item,isbn13,title,isbn.legacy_reason, '' as transaction_date, sum(qty) as qty_hadoop,
concat(trim(isbn.txn_external_system_ref_id),trim(isbn.item),trim(isbn.legacy_reason)) as isbn_grp 
from rlsf_prod.isbn_activity_summary isbn
join isbn_ct on isbn_ct.isbn_group=concat(trim(isbn.txn_external_system_ref_id),trim(isbn.item),trim(isbn.legacy_reason))
group by isbn.txn_external_system_ref_id,short_id,isbn.item,isbn13,title,isbn.legacy_reason,transaction_date,isbn_grp
;