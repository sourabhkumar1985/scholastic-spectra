set decimal_v2=false;
with only_receipt as
(
select distinct
deb.transactionseoinventoryitemid,
deb.transactionseoinventoryorgid,
deb.cstlayercostspeorectrxnid
from rlsf_prod.cost_trans_debit deb -- change table name accordingly
where cast(deb.COSTDISTRIBUTIONLINESEOENTEREDCURRENCYAMOUNT as decimal(38,6)) <> 0 and
deb.cstlayercostspeocosttransactiontype = 'RECEIPT'
)

insert into table rlsf_prod.ohq_inv_unit_cst_by_cost_rec_po
select distinct
deb.transactionseoinventoryitemid,
deb.transactionseoinventoryorgid,
deb.cstlayercostspeorectrxnid,
cast(deb.layer_unit_cost as decimal(38,6)) as cost_unit,
deb.transactionseocreationdate as transactionseocreationdate,
deb.costdistributionlineseocostelementid,
deb.distributionlineid,
deb.cstlayercostspeocosttransactiontype
from rlsf_prod.cost_trans_debit deb -- change table name accordingly
left anti join only_receipt recp on recp.transactionseoinventoryitemid=deb.transactionseoinventoryitemid and 
recp.transactionseoinventoryorgid=deb.transactionseoinventoryorgid and recp.cstlayercostspeorectrxnid=deb.cstlayercostspeorectrxnid 
join rlsf_prod.cost_trans_rank_assign assign on assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
where cast(deb.COSTDISTRIBUTIONLINESEOENTEREDCURRENCYAMOUNT as decimal(38,6)) > 0 and
cast(costdistributionseolayerquantity as decimal (38,6)) > 0 and 
deb.cstlayercostspeocosttransactiontype = 'ADJUST'
and deb.cstlayercostspeorectrxnid in (select distinct cstlayercostspeorectrxnid from 
(select cstlayercosttransid,cstlayercostspeorectrxnid,sum(cast(costdistributionseolayerquantity as decimal (38,6))) as layer_unit_qty 
from rlsf_prod.cost_trans_debit group by cstlayercosttransid,cstlayercostspeorectrxnid) aa
where  cast(layer_unit_qty as decimal(38,6)) >0);

set decimal_v2=false;
insert into table rlsf_prod.ohq_inv_unit_cst_by_cost_rec_po
select distinct
deb.transactionseoinventoryitemid,
deb.transactionseoinventoryorgid,
deb.cstlayercostspeorectrxnid,
--cast((cast(deb.COSTDISTRIBUTIONLINESEOENTEREDCURRENCYAMOUNT as decimal(38,6)) / cast(deb.COSTDISTRIBUTIONSEOLAYERQUANTITY as --decimal(38,6))) as decimal(38,6)) as cost_unit,
cast(deb.layer_unit_cost as decimal(38,6)) as cost_unit,
deb.transactionseocreationdate as transactionseocreationdate,
deb.costdistributionlineseocostelementid,
deb.distributionlineid,
deb.cstlayercostspeocosttransactiontype
from rlsf_prod.cost_trans_debit deb -- change table name accordingly
left anti join rlsf_prod.ohq_inv_unit_cst_by_cost_rec_po un_cst
on un_cst.transactionseoinventoryitemid=deb.transactionseoinventoryitemid and 
un_cst.transactionseoinventoryorgid=deb.transactionseoinventoryorgid and un_cst.cstlayercostspeorectrxnid=deb.cstlayercostspeorectrxnid
join rlsf_prod.cost_trans_rank_assign assign on assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
where cast(deb.COSTDISTRIBUTIONLINESEOENTEREDCURRENCYAMOUNT as decimal(38,6)) <> 0 and
cast(costdistributionseolayerquantity as decimal (38,6)) > 0 and 
deb.cstlayercostspeocosttransactiontype in ('ADJUST','RECEIPT')
--and deb.cstlayercostspeorectrxnid in (select distinct cstlayercostspeorectrxnid from 
--(select cstlayercostspeorectrxnid,sum(cast(costdistributionseolayerquantity as decimal (38,6))) as layer_unit_qty 
--from rlsf_prod.cost_trans_debit group by cstlayercostspeorectrxnid) aa
--where  cast(layer_unit_qty as decimal(38,6)) >0);