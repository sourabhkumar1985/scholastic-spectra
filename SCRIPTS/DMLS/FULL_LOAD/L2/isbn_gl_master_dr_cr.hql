insert into table rlsf_prod.isbn_gl_master_dr_cr

select  dr.* from rlsf_prod.isbn_gl_master_dr dr
join rlsf_prod.isbn_rank_assign assign on assign.PERIOD = dr.PERIOD and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}

union all

select  cr.* from rlsf_prod.isbn_gl_master_cr cr
join rlsf_prod.isbn_rank_assign assign on assign.PERIOD = cr.PERIOD and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
;