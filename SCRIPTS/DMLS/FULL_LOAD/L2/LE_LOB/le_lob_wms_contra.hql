insert overwrite table rlsf_prod.le_lob_wms_contra
select * from rlsf_prod.le_lob
--where item='201428A'
;


insert into table rlsf_prod.le_lob_wms_contra
SELECT
le.organization_code,
le.item,
le.short_id,
le.transaction_type,
le.reference,
le.legacy_transaction_type,
le.legacy_reason,
le.transaction_source,
le.source,
le.gl_batch_desc,
le.jrnl_batch_name,
le.dr_jrnl_line_num,
le.cr_jrnl_line_num,
le.dr_account_num,
le.cr_account_num,
le.total_amount,
le.layer_quantity * (-1) as layer_quantity,
le.cost_unit,
le.entered_dr,
le.period,
le.imt_creation_date,
le.imt_creation_time,
le.transaction_date,
le.transaction_time,
le.journal_creation_date,
le.journal_creation_time,
le.txn_transaction_id,
le.rec_trxn_id,
le.source_line,
le.title,
le.item_type,
le.opus_item,
le.isbn13,
le.dr_legal_entity,
le.dr_line_of_business,
le.dr_account,
le.dr_cost_center,
le.dr_product_line,
le.dr_channel,
le.dr_loc,
le.dr_interco,
le.cr_legal_entity,
le.cr_line_of_business,
le.cr_account,
le.cr_cost_center,
le.cr_product_line,
le.cr_channel,
le.cr_loc,
le.cr_interco,
le.txn_external_system_ref_id,
le.po_wo,
le.destination_org,
le.item_category,
le.category_description,
le.journal,
le.accounting_class_code,
le.po_wo_transaction_type,
le.po_wo_transaction_source,
'LE/LOB' as lelob_source,
'Contra' as lelob_source_description,
case when lelob_source_description='PO_Split' then 'PO Contra'
when lelob_source_description='WO_Split' then 'WO Contra' 
else '' end as event
from rlsf_prod.le_lob le
JOIN rlsf_prod.le_lob_wms_adjustment wms on wms.item=le.item and wms.legal_entity=le.dr_legal_entity and
wms.line_of_business=le.dr_line_of_business
where wms.split_quantity<>0
--and le.item='201428A'
;

insert into table rlsf_prod.le_lob_wms_contra
SELECT
le.organization_code,
le.item,
le.short_id,
le.transaction_type,
le.reference,
le.legacy_transaction_type,
le.legacy_reason,
le.transaction_source,
le.source,
le.gl_batch_desc,
le.jrnl_batch_name,
le.dr_jrnl_line_num,
le.cr_jrnl_line_num,
le.dr_account_num,
le.cr_account_num,
le.total_amount,
le.layer_quantity * (-1) as layer_quantity,
le.cost_unit,
le.entered_dr,
le.period,
le.imt_creation_date,
le.imt_creation_time,
le.transaction_date,
le.transaction_time,
le.journal_creation_date,
le.journal_creation_time,
le.txn_transaction_id,
le.rec_trxn_id,
le.source_line,
le.title,
le.item_type,
le.opus_item,
le.isbn13,
le.dr_legal_entity,
le.dr_line_of_business,
le.dr_account,
le.dr_cost_center,
le.dr_product_line,
le.dr_channel,
le.dr_loc,
le.dr_interco,
le.cr_legal_entity,
le.cr_line_of_business,
le.cr_account,
le.cr_cost_center,
le.cr_product_line,
le.cr_channel,
le.cr_loc,
le.cr_interco,
le.txn_external_system_ref_id,
le.po_wo,
le.destination_org,
le.item_category,
le.category_description,
le.journal,
le.accounting_class_code,
le.po_wo_transaction_type,
le.po_wo_transaction_source,
'LE/LOB' as lelob_source,
'Contra' as lelob_source_description,
case when lelob_source_description='PO_Split' then 'PO Contra'
when lelob_source_description='WO_Split' then 'WO Contra' 
else '' end as event
from rlsf_prod.le_lob le
JOIN rlsf_prod.le_lob_wms_adjustment wms on wms.item=le.item and wms.legal_entity=le.cr_legal_entity and
wms.line_of_business=le.cr_line_of_business
where wms.split_quantity<>0 and concat(le.dr_legal_entity,le.dr_line_of_business) not in ('101000','510000')
and concat(le.cr_legal_entity,le.cr_line_of_business)<>concat(le.dr_legal_entity,le.dr_line_of_business) 
--and le.item='201428A'
;

insert into table rlsf_prod.le_lob_wms_contra

with wms as (
SELECT distinct item, substr(legal_entity,1,1) as le 
from rlsf_prod.le_lob_wms_adjustment
where split_quantity<>0
--and item='201428A'
)

SELECT
le.organization_code,
le.item,
le.short_id,
le.transaction_type,
le.reference,
le.legacy_transaction_type,
le.legacy_reason,
le.transaction_source,
le.source,
le.gl_batch_desc,
le.jrnl_batch_name,
le.dr_jrnl_line_num,
le.cr_jrnl_line_num,
le.dr_account_num,
le.cr_account_num,
le.total_amount,
le.layer_quantity * (-1) as layer_quantity,
le.cost_unit,
le.entered_dr,
le.period,
le.imt_creation_date,
le.imt_creation_time,
le.transaction_date,
le.transaction_time,
le.journal_creation_date,
le.journal_creation_time,
le.txn_transaction_id,
le.rec_trxn_id,
le.source_line,
le.title,
le.item_type,
le.opus_item,
le.isbn13,
le.dr_legal_entity,
le.dr_line_of_business,
le.dr_account,
le.dr_cost_center,
le.dr_product_line,
le.dr_channel,
le.dr_loc,
le.dr_interco,
le.cr_legal_entity,
le.cr_line_of_business,
le.cr_account,
le.cr_cost_center,
le.cr_product_line,
le.cr_channel,
le.cr_loc,
le.cr_interco,
le.txn_external_system_ref_id,
le.po_wo,
le.destination_org,
le.item_category,
le.category_description,
le.journal,
le.accounting_class_code,
le.po_wo_transaction_type,
le.po_wo_transaction_source,
'LE/LOB' as lelob_source,
'Contra' as lelob_source_description,
'WMS Contra' as event
from rlsf_prod.le_lob le
JOIN wms on wms.item=le.item and wms.le=substr(le.dr_legal_entity,1,1)
where trim(le.transaction_source)='WMS ADJUSTMENTS'
and concat(le.dr_legal_entity,le.dr_line_of_business) in ('101000','510000') 
--and le.item='201428A'
;