insert into table rlsf_prod.isbn_activity_agg
select 
isbn.reference as ref_number ,
split_part(isbn.reference,'-',1) as proforma_ref ,
isbn.isbn13 as isbn13,
isbn.title as title ,
sum(isbn.total_amount) as cop_amt ,
sum(isbn.layer_quantity) * (-1) as cop_qty ,
isbn.legacy_reason as legacy_reason ,
isbn.legacy_transaction_type as legacy_trans_type ,
upper(concat(split_part(isbn.period, '-',1),'-20',split_part(isbn.period, '-',2))) as period,
'' as transaction_date,  --need to add transaction date
if(dr_account = '1421000' and cr_account <> '1421000',isbn.cr_Legal_Entity,isbn.dr_Legal_Entity) as isbn_Legal_Entity ,
if(dr_account = '1421000' and cr_account <> '1421000',isbn.cr_Line_of_Business,isbn.dr_Line_of_Business) as isbn_Line_of_Business ,
if(dr_account = '1421000' and cr_account <> '1421000',isbn.cr_Account,isbn.dr_Account) as isbn_Account ,
if(dr_account = '1421000' and cr_account <> '1421000',isbn.cr_Cost_Center,isbn.dr_Cost_Center) as isbn_Cost_Center ,
if(dr_account = '1421000' and cr_account <> '1421000',isbn.cr_Product_Line,isbn.dr_Product_Line) as isbn_Product_Line ,
if(dr_account = '1421000' and cr_account <> '1421000',isbn.cr_Channel,isbn.dr_Channel) as isbn_Channel ,
if(dr_account = '1421000' and cr_account <> '1421000',isbn.cr_Loc,isbn.dr_Loc) as isbn_Loc ,
if(dr_account = '1421000' and cr_account <> '1421000',isbn.cr_Interco,isbn.dr_Interco) as isbn_Interco 
from rlsf_prod.isbn_activity_summary isbn -- change the table names accordingly
join rlsf_prod.isbn_rank_assign assign on assign.JRNL_BATCH_NAME = isbn.JRNL_BATCH_NAME and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
group by 
isbn.title ,
proforma_ref ,
isbn.isbn13 ,
isbn.legacy_reason ,
isbn.legacy_transaction_type  ,
isbn.reference,
isbn_Legal_Entity ,
isbn_Line_of_Business ,
isbn_Account ,
isbn_Cost_Center ,
isbn_Product_Line ,
isbn_Channel ,
isbn_Loc ,
isbn_Interco,
period,
isbn.isbn13,
transaction_date;