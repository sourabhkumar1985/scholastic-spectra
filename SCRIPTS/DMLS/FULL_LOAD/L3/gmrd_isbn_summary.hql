insert overwrite table rlsf_prod.gmrd_isbn_summary
select 
isbn13 ,
title ,
rev_amt,
rev_qty,
cop_amt,
cop_qty,
case 
when rev_qty=0 and cop_qty=0 then 0 
else rev_qty - cop_qty end as  qty_var,
case 
when rev_amt = 0 or cop_amt =0 then 0
when abs(cop_amt/rev_amt) > 1 then 1 
else cast((cop_amt/rev_amt) as decimal(38,6))
end as cop_margin,
case 
when rev_qty = 0 or cop_amt = 0 or cop_qty = 0 then 0
else 
cast(((cop_amt/cop_qty) * rev_qty) as decimal(38,6))
end as profroma_cop_amt,
case 
when rev_amt = 0 or cop_amt =0 or cop_qty= 0 or rev_qty =0 then 0
when abs((((cop_amt/cop_qty) * rev_qty) / rev_amt)) > 1 then 1 
else cast((((cop_amt/cop_qty) * rev_qty) / rev_amt) as decimal(38,6))
end as proforma_margin,
case 
when rev_amt =0 or rev_qty = 0 then 0 
else cast((rev_amt/rev_qty) as decimal(38,6)) end as rev_per_unit,
case 
when cop_amt =0 or cop_qty = 0 then 0 
else cast((cop_amt/cop_qty) as decimal(38,6)) end as cop_per_unit,
period ,
isbn_Legal_Entity ,
isbn_Line_of_Business ,
isbn_Account ,
isbn_Cost_Center ,
isbn_Product_Line ,
isbn_Channel ,
isbn_Loc ,
isbn_Interco ,
gmrd_Legal_Entity ,
gmrd_Line_of_Business ,
gmrd_Account ,
gmrd_Cost_Center ,
gmrd_Product_Line ,
gmrd_Channel ,
gmrd_Loc ,
gmrd_Interco
from 
rlsf_prod.gmrd_isbn_rev_isbn13_details; 