invalidate metadata rlsf_prod.sch_bi_extract;

insert overwrite table rlsf_prod.sch_bi_extract_parquet
select oracle_item,org_id,
case when length(ohqty) >= 19 then cast(substr(ohqty, 1, length(ohqty) - 20) as string)
else ohqty end as ohqty
from rlsf_prod.sch_bi_extract;


truncate table rlsf_prod.sch_bi_extract;
