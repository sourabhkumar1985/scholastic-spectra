from flask import json, Response, abort, make_response, jsonify
import os
import pyarrow as pa
import subprocess
from app.exception.custom_exception import DeleteError
from app.exception.custom_exception import UploadError
from app.exception.custom_exception import InvalidDirError
from app.exception.custom_exception import SolrDataImportError
import datetime
import set_env
import configparser
from impala.dbapi import connect
import stat
import requests
from random import seed
from random import randint
from datetime import datetime



def run_cmd(args_list):
    """
    run linux commands
    """
    # import subprocess
    print('Running system command: {0}'.format(' '.join(args_list)))
    proc = subprocess.Popen(args_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    s_output, s_err = proc.communicate()
    s_return = proc.returncode
    return s_return, s_output, s_err



class Hadoop:
    def __init__(self):

        abs_path = os.path.abspath(os.path.dirname(__file__))
        config_path = os.path.join(abs_path, '..//..//config//config.ini')
        config = configparser.ConfigParser()
        config.read(config_path)
        self.configs = config
        set_env.set_env()

    def list_files(self,dirname):
        master = str(self.configs['hdfs']['master'])
        h_port = int(self.configs['hdfs']['port'])
        hdfs_dir = dirname['dir_name']
        hdfs = pa.hdfs.connect(host=master, port=h_port,)
        file_list = hdfs.ls(hdfs_dir, detail=True)
        return  file_list


    def delete_files(self,dirname):
        master = str(self.configs['hdfs']['master'])
        h_port = int(self.configs['hdfs']['port'])
        hdfs_dir = dirname['dir_name']
        if not str(hdfs_dir).endswith("/"):
            hdfs_dir = f'{hdfs_dir}/'
        delete_files = f'hdfs://{master}/{hdfs_dir}*'
        (ret, out, err) = run_cmd(['hdfs', 'dfs', '-rm','-skipTrash', delete_files])
        if ret != 0:
            raise DeleteError(f'RC: {ret} Stdout: {out} Error: {err}')
        return {'message':'Files Deleted'}

    def put_files(self,dir_name):
        local_dir = f'/home/python/FRV/{dir_name}'
        date_1 = datetime.datetime.now()
        (ret, out, err) = run_cmd(['sh', '/home/python/FRV/api_load.sh', local_dir])
        if ret != 0:
            raise UploadError(f'Stdout: {out} Error: {err}')
        date_2 = datetime.datetime.now()
        difference = date_2 - date_1
        message = f'Time taken to uplaod file ${difference}'
        put_time = f'{err}'
        return {'message':message, 'put_time':put_time}

    def get_local_files(self,dir_name):
        if dir_name not in ['one','multi']:
            raise InvalidDirError("Directory must be one or multi")
        output = []
        work_dir = self.configs['local']['base_dir']
        abs_path = f'{work_dir}{dir_name}'
        with os.scandir(abs_path) as entries:
            for entry in entries:
                fileStatsObj = os.stat(abs_path +'/' +entry.name)
                last_modified_time = (fileStatsObj[stat.ST_MTIME])
                last_access_time = (fileStatsObj[stat.ST_ATIME])
                size = (fileStatsObj[stat.ST_SIZE])
                x = {'name': entry.name, 'size': size, 'last_modified_time': last_modified_time,
                     'last_access_time': last_access_time}
                output.append(x)
        return output

    def impala_refresh(self,json_obj):
        impala_host = self.configs['impala']['host']
        impala_port = int(self.configs['impala']['port'])
        table_name = json_obj['table_name']
        query = f'refresh {table_name};'
        query2 = f'insert overwrite {pq_table} select * from {table_name};'
        conn = connect(host=impala_host, port=impala_port)
        cursor = conn.cursor()
        cursor.execute(query)
        refresh_status = cursor.description
        cursor.execute(query2)
        parq_status = cursor.description
        message = f'Impala Refresh complete'
        query_output = f'Refresh Status:{refresh_status}  Parquet status :{parq_status}'
        return {'message':message,'Query_status':query_output}


    def solr_import_status(self):
        url = self.configs['solr']['status']
        response = requests.get(url)
        return response.json()

    def solr_import_src(self):
        url = self.configs['solr']['full_import']
        di_status = self.solr_import_status()
        if di_status['status'] != 'idle':
            raise SolrDataImportError("Data Import is Running , Wait ot Abort current process")
        response = requests.get(url)
        return response.json()

    def solr_import_abort(self):
        url = self.configs['solr']['abort']
        response = requests.get(url)
        return response.json()

    def solr_delete_data(self):
        url = self.configs['solr']['purge']
        response = requests.get(url)
        return response.json()



    def update_cache(self,cache):
        if 'dummy' in cache:
            val = cache['dummy'] + 1
            cache['dummy'] = val
        else:
            cache['dummy'] = 0
        return str(cache['dummy'])


    def update_context_data(self,cache):
        return 'x'









