from flask import request, make_response, jsonify, abort,stream_with_context, Response
from app import app
from app.modles.orable_modules import Oracle
from app.exception.custom_exception import SolrDataImportError
import http
# import sqlite3
from flask import g
import requests
import base64

cache = {}

@app.route('/load/oracle', methods=['POST'])
def orcle_di(**kwargs):
    json_obj = request.get_json()
    print(json_obj)

    try:
        oracle = Oracle()
        rep = oracle.load_oracle_data(json_obj)
        response = make_response(jsonify(rep),http.HTTPStatus.ACCEPTED)
    except Exception as e:
        response = make_response(jsonify({"ERROR While Deleting Solr Data": str(e)}),
                                 http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response

@app.route('/log/<jobid>/<instanceid>', methods=['GET'])
def get_logs(jobid,instanceid,**kwargs):
    try:
        oracle = Oracle()
        rep = oracle.read_log(cache,jobid,instanceid)
        response = make_response(jsonify(rep))
    except Exception as e:
        response = make_response(jsonify({"ERROR Cannot read": str(e)}),
                                 http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response


@app.route('/job/oracle', methods=['POST'])
def run_job(**kwargs):
    json_obj = request.get_json()
    #sprint(json_obj)

    try:
        oracle = Oracle()
        rep = oracle.run_job(json_obj,cache)
        response = make_response(jsonify(rep),http.HTTPStatus.ACCEPTED)
    except Exception as e:
        response = make_response(jsonify({"ERROR While Submitting Job": str(e)}),
                                 http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response

@app.route('/files/oracle/<container_name>', methods=['GET'])
def get_files(container_name,**kwargs):
    try:
        oracle = Oracle()
        rep = oracle.get_oracle_files(container_name)
        response = make_response(jsonify(rep))
    except Exception as e:
        response = make_response(jsonify({"ERROR While Fetching The files from Oracle Cloud Storage": str(e)}),
                                 http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response

@app.route('/container/oracle', methods=['GET'])
def get_containers(**kwargs):
    try:
        oracle = Oracle()
        rep = oracle.get_containers()
        response = make_response(jsonify(rep))
    except Exception as e:
        response = make_response(jsonify({"ERROR While Fetching The Containers from Oracle Cloud Storage": str(e)}),
                                 http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response


@app.route('/files/oracle/<file_name>', methods=['GET'])
def get_files_oracle(file_name,**kwargs):
    try:
        oracle = Oracle()
        rep = oracle.download_file(file_name)
        response = rep
    except Exception as e:
        response = make_response(jsonify({"ERROR While Fetching The files from Oracle Cloud Storage": str(e)}),
                                 http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return  response


@app.route('/stream/<container>/<file>')
def streamed_response(container, file):
    if request.authorization and request.authorization.username == 'admin' and request.authorization.password == 'admin@123':
        def generate():
            username = 'cloud_bi_user'
            password = 'TestCloud2020'
            userpass = "{0}:{1}".format(username, password)
            url = 'https://uscom-east-1.storage.oraclecloud.com/v1/Storage-schlprodpaas/' + container + '/'+file
            value = base64.b64encode(bytes(userpass, "utf-8")).decode().replace('\n', '')
            headers = {'Authorization': 'Basic {}'.format(value), "content-type": "application/zip"}
            r = requests.get(url=url, headers=headers, verify=True, stream=True)
            for chunk in r.iter_content(chunk_size=128):
                yield chunk
        return Response(generate(), mimetype='application/zip')
    return make_response('Unauthorized', 401, {'WWW-Authenticate': 'Basic realm="Login required"'})

