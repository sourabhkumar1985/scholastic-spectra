package com.di.data;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipInputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.permission.FsPermission;


public class HDFS {

FileSystem hdfs = null;
Map<String, String> deleteTrackerMap = null;
public HDFS(String hdfsMaster) throws IOException, URISyntaxException {

	Configuration config = new Configuration(); 
	hdfs = FileSystem.get(new URI(hdfsMaster), config);
	deleteTrackerMap = new HashMap<String, String>();
	System.setProperty("HADOOP_USER_NAME", "hdfs");
	
	
}

public void hdfsProcess(FileConfig fconfg,ZipInputStream zis,String oracleFileName) throws IllegalArgumentException, IOException {
	Path hdfsDirName = new Path(fconfg.getTargetDir());
	Path hdfsFileName = new Path(fconfg.getTargetDir() + oracleFileName);
	
	
	if (fconfg.getCleanDir() && !this.deleteTrackerMap.containsKey(fconfg.getTargetDir())) {
		this.cleanDir(hdfsDirName,fconfg.getTargetDir());
	}
	
	if (this.checkHdfsFileExists(hdfsFileName)) {
		if (fconfg.getOverwriteType().equalsIgnoreCase("force")) {
			this.deleteHdfsFile(hdfsFileName);
			this.writeToHdfs(hdfsFileName, zis);
		}else {
		//	log.info("File " + oracleFileName + " Will not written to hdfs because file already exists and Overwrite is set to False");
			
		}
	}else {
		this.writeToHdfs(hdfsFileName, zis);	
	}
}

public void cleanDir(Path hdfsDirName,String targetDir) {
	this.deleteTrackerMap.put(targetDir, null);
	try {
		if (this.hdfs.exists(hdfsDirName)) {
			this.hdfs.delete(hdfsDirName,true);
			this.hdfs.mkdirs(hdfsDirName);	
		}else {
			this.hdfs.mkdirs(hdfsDirName, new FsPermission("777"));
		}
	}catch (IOException e) {
		e.printStackTrace();
	}
	
}

private boolean checkHdfsFileExists(Path hdfsFileName) {
	boolean returnstatus  = false;
	
	try {
	if (this.hdfs.exists(hdfsFileName)) {
		
		returnstatus = true;
	}
	}catch (IOException e) {
		e.printStackTrace();
	}
	return returnstatus;
}

private void deleteHdfsFile(Path hdfsFileName) {
	try {
		this.hdfs.delete(hdfsFileName,true);
		
	}catch (IOException e) {
		e.printStackTrace();
	}	
}

private void writeToHdfs(Path hdfsFileName,ZipInputStream zis) throws IllegalArgumentException, IOException {
	
	FSDataOutputStream out = hdfs.create(hdfsFileName);
	byte[] buffer = new byte[256];
    int bytesRead;
    while ((bytesRead = zis.read(buffer)) != -1) {
        out.write(buffer, 0, bytesRead);
    }
	
}


}
