package com.relevance.ppa.data;


public class EMEAView {

	private String poCount;
	private String currency;
	private String sapPC;
	private String plantDesc;
	private String companyCode;
	private String companyCodeDesc;
	private String poReqDate;
	private String sseCI;
	private String cI;
	private String mismatch;
	private String pVP;
	private String netValueVP;
	private String sumNetValue;
	private String sseIMI;
	private String pVPQuartile;
	private String poPlant;
	private String ssePlant;
	private String poNumber;
	private String poLineNumber;
	private String vendorNumber;
	private String vendorName;
	private String poCreationDate;
	private String poCurrency;
	private String poExchangeRate;
	private String poMaterialNumber;
	private String sseMaterialId;
	private String sseMaterialNumber;
	private String materialDesc;
	private String poMaterialDesc;
	private String orderQty;
	private String poOrderQty;
	private String uom;
	private String poUom;
	private String expCost;
	private String poExpCost;
	private String ssePrice;
	private String priceVariance;
	private String priveVP;
	private String totalNetValue;
	private String poTotalNetValue;
	private String incoTerm;
	private String poIncoTerm;
	private String incoTermDesc;
	private String leadTime;
	private String qtPrice;
	private String poLeadTime;
	private String contract;
	private String materialGroup;
	private String poMaterialGroup;
	private String materialGroupDesc;
	private String typeCategory;
	private String currencyMisMatch;
	private String leadTimeMisMatch;
	private String moqMisMatch;
	private String unitMisMatch;
	private String itemType;
	private String incoTermMismatch;
	private String sseContractFound;
	private String matchCategory;
	private String sseMatchFound;
	private String poItemType;
	private String sseMoq;
	private String sseCurrency;
	private String sseIncoTerm;
	private String sseLeadTime;
	private String currencyMI;
	private String leadTimeMI;
	private String moqMI;
	private String unitMI;
	private String incoTermMI;
	private String sumPriceVariance;
	private String qtCount;
	private String contractStartDate;
	private String sapPlant;
	private String qtPlant;
	private String qtVendorNumber;
	private String qtVendorName;
	private String qtCurrency;
	private String qtMaterialNumber;
	private String qtMaterialDesc;
	private String qtUom;
	private String qtIncoTerm;
	private String qtLeadTime;
	private String qtContractStartDate;
	private String totalNetvalueVariance;
	private String qtYear;
	private String ppvCount;
	private String postingYear;
	private String postingMonth;
	private String plantName;
	private String postingDateDoc;
	private String documentCurrency;
	private String invoiceCurrency;
	private String paymentTerms;
	private String paymentTermsDesc;
	private String sumamountBaseCurrency;
	private String materialType;
	private String invoicePriceBaseCurrency;
	private String vendorFreqQuantityQuartile;
	private String vendorFreqPosQuartile;
	private String materialFreqQuantityQuartile;
	private String materialFreqPosQuartile;
	private String gssStatus;
	private String category;
	private String subCategory;
	private String effectiveContract;
	private String direction;
	private String transactionType;
	private int totalppvDocCurrency; 
	private int sumInvoicespendActual;
	private int sumInvoicespendWahr;
	private int sumInvoicespendBp;
	private int totalppvFcurActualrate;
	private int totalppvFcurWahr;
	private int totalppvFcurBp;
	private String fcur;
	private int qty;
	private int otherMismatchCount;
	
	
	// fields for EMEA PPV L2 View
	private String purchaseDocNo;
	private String glAccount;
	private String materialCode;
	private String materialCodePred;
	private String materialDesPred;
	private String materialSpecRef;
	private String vendorNo;
	private String secondaryVendorNo;
	private String secondaryVendorName;
	private String vendorCountry;
	private String vendorCity;
	private String secondaryVendorCountry;
	private String secondaryVendorCity;
	private String distributorNo;
	private String distributorName;
	private String distributorCountry;
	private String distributorCity;
	private String secondaryDistributorNo;
	private String secondaryDistributorName;
	private String secondaryDistributorCountry;
	private String secondaryDistributorCity;
	private String manufacturerNo;
	private String manufacturerName;
	private String manufacturerCountry;
	private String manufacturerCity;
	private String secondaryManufacturerNo;
	private String secondaryManufacturerName;
	private String secondaryManufacturerCountry;
	private String secondaryManufacturerCity;
	private String materialCategory;
	private String materialSubCategory;
	private String purchasingUom;
	private String stockingUom;
	private String conversionFactor;
	private String invQuantity;
	private String receiptQuantity;
	private String moq;
	private String ioq;
	private String incotermCode;
	private String incotermDesc;
	private String incotermLocationCountry;
	private String incotermLocationCity;
	private String paymentDays;
	private String vmiFlag;
	private String purchaseCurrency;
	private String priceValidityStartDate;
	private String priceValidityEndDate;
	private String amountDocCurrency;
	private String amountBaseCurrency;
	private String amountEurActualrate;
	private String amountEur15wahrRate;
	private String pounitpriceDocCurrency;
	private String pounitpriceEurConvertedActualrate;
	private String pounitpriceEurConverted15wahr;
	private String totalpoEurActualrate;
	private String totalpoEur15wahr;
	private String purchasePriceUom;
	private String invoicePriceDocCurrency;
	private String invoicePriceEurActualrate;
	private String invoicePriceEurIr;
	private String invoicePriceEur15wahr;
	private String invoicePriceEurIr15wahr;
	private String standardCostperunitBaseCurrency;
	private String standardCostperunitEurActualrate;
	private String standardCostperunitEur15wahr;
	private String totalStandardCost;
	private String ppv2AmountDocCurrency;
	private String ppv2AmountEurActualrate;
	private String ppv2AmountEur15wahr;
	private String ppv1AmountDocCurrency;
	private String ppv1AmountEurActualrate;
	private String ppv1AmountEur15wahr;
	private String documentType;
	private String sseTransaction;
	private String actualExchangeRate;
	private String weightedAverageHedgeRate15wahr;
	private String poCostQuartile;
	private String netvalueQuartile;
	private String netvarianceQuartile;
	private String ppvMismatch;
	private String documentCategory;
	private String documentCategoryDesc;
	private String favourable;
	private String variance;
	private String totalInvoicePriceQuartile;
	private String standardCostQuartile;
	private String ppv2Quartile;
	private String invoiceNumber;
	private String invoiceLineNumber;
	private String totalpoAmtEurActualrate;
	private String totalpoAmtEur15wahr;
	private String invoiceSpendEurActualrate;
	private String invoiceSpendEur15wahr;
	private String totalAmountAtStandardCost;
	private String totalPpvDocCurrency;
	private String totalPpvEurActualrate;
	private String totalPpvNewEurActualrate;
	private String totalPpvEur15wahr;
	private String totalAmtAtStandardCostBaseCurrency;
	private String totalAmtAtStandardCostEurActualrate;
	private String totalAmtAtStandardCostEur15wahr;
	private String totalpoAmtDocCurrency;
	private String sumInvoicespendEur;
	private String sumInvoicespendUsd;
	private String sumTotalppvEur;
	private String sumTotalppvUsd;
	private String sapPlantCode;
	private String commodityLevel1;
	private String standardCostperunitDocCurrency;
	private String vendorMismatch;
	private String amountType;
	private String currencyType;
	private int totalAmt;
	private int cairoAmt;
	private int wuppertalAmt;
	private int sezanneAmt;
	private int pomeziaAmt;
	private int capeTownAmt;
	private int eastLondonAmt;
	private int valDeRuilAmt;
	private int helsingborgAmt;
	private int madridAmt;
	private int mandraAmt;
	
	
	
	public String getPoCount() {
		return poCount;
	}
	public void setPoCount(String poCount) {
		this.poCount = poCount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getSapPC() {
		return sapPC;
	}
	public void setSapPC(String sapPC) {
		this.sapPC = sapPC;
	}
	public String getPlantDesc() {
		return plantDesc;
	}
	public void setPlantDesc(String plantDesc) {
		this.plantDesc = plantDesc;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getCompanyCodeDesc() {
		return companyCodeDesc;
	}
	public void setCompanyCodeDesc(String companyCodeDesc) {
		this.companyCodeDesc = companyCodeDesc;
	}
	public String getPoReqDate() {
		return poReqDate;
	}
	public void setPoReqDate(String poReqDate) {
		this.poReqDate = poReqDate;
	}
	public String getSseCI() {
		return sseCI;
	}
	public void setSseCI(String sseCI) {
		this.sseCI = sseCI;
	}
	public String getcI() {
		return cI;
	}
	public void setcI(String cI) {
		this.cI = cI;
	}
	public String getMismatch() {
		return mismatch;
	}
	public void setMismatch(String mismatch) {
		this.mismatch = mismatch;
	}
	public String getpVP() {
		return pVP;
	}
	public void setpVP(String pVP) {
		this.pVP = pVP;
	}
	public String getNetValueVP() {
		return netValueVP;
	}
	public void setNetValueVP(String netValueVP) {
		this.netValueVP = netValueVP;
	}
	public String getSumNetValue() {
		return sumNetValue;
	}
	public void setSumNetValue(String sumNetValue) {
		this.sumNetValue = sumNetValue;
	}
	public String getSseIMI() {
		return sseIMI;
	}
	public void setSseIMI(String sseIMI) {
		this.sseIMI = sseIMI;
	}
	public String getpVPQuartile() {
		return pVPQuartile;
	}
	public void setpVPQuartile(String pVPQuartile) {
		this.pVPQuartile = pVPQuartile;
	}
	public String getPoPlant() {
		return poPlant;
	}
	public void setPoPlant(String poPlant) {
		this.poPlant = poPlant;
	}
	public String getSsePlant() {
		return ssePlant;
	}
	public void setSsePlant(String ssePlant) {
		this.ssePlant = ssePlant;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getPoLineNumber() {
		return poLineNumber;
	}
	public void setPoLineNumber(String poLineNumber) {
		this.poLineNumber = poLineNumber;
	}
	public String getVendorNumber() {
		return vendorNumber;
	}
	public void setVendorNumber(String vendorNumber) {
		this.vendorNumber = vendorNumber;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getPoCreationDate() {
		return poCreationDate;
	}
	public void setPoCreationDate(String poCreationDate) {
		this.poCreationDate = poCreationDate;
	}
	public String getPoCurrency() {
		return poCurrency;
	}
	public void setPoCurrency(String poCurrency) {
		this.poCurrency = poCurrency;
	}
	public String getPoExchangeRate() {
		return poExchangeRate;
	}
	public void setPoExchangeRate(String poExchangeRate) {
		this.poExchangeRate = poExchangeRate;
	}
	public String getPoMaterialNumber() {
		return poMaterialNumber;
	}
	public void setPoMaterialNumber(String poMaterialNumber) {
		this.poMaterialNumber = poMaterialNumber;
	}
	public String getSseMaterialId() {
		return sseMaterialId;
	}
	public void setSseMaterialId(String sseMaterialId) {
		this.sseMaterialId = sseMaterialId;
	}
	public String getSseMaterialNumber() {
		return sseMaterialNumber;
	}
	public void setSseMaterialNumber(String sseMaterialNumber) {
		this.sseMaterialNumber = sseMaterialNumber;
	}
	public String getMaterialDesc() {
		return materialDesc;
	}
	public void setMaterialDesc(String materialDesc) {
		this.materialDesc = materialDesc;
	}
	public String getPoMaterialDesc() {
		return poMaterialDesc;
	}
	public void setPoMaterialDesc(String poMaterialDesc) {
		this.poMaterialDesc = poMaterialDesc;
	}
	public String getOrderQty() {
		return orderQty;
	}
	public void setOrderQty(String orderQty) {
		this.orderQty = orderQty;
	}
	public String getPoOrderQty() {
		return poOrderQty;
	}
	public void setPoOrderQty(String poOrderQty) {
		this.poOrderQty = poOrderQty;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getPoUom() {
		return poUom;
	}
	public void setPoUom(String poUom) {
		this.poUom = poUom;
	}
	public String getExpCost() {
		return expCost;
	}
	public void setExpCost(String expCost) {
		this.expCost = expCost;
	}
	public String getPoExpCost() {
		return poExpCost;
	}
	public void setPoExpCost(String poExpCost) {
		this.poExpCost = poExpCost;
	}
	public String getSsePrice() {
		return ssePrice;
	}
	public void setSsePrice(String ssePrice) {
		this.ssePrice = ssePrice;
	}
	public String getPriceVariance() {
		return priceVariance;
	}
	public void setPriceVariance(String priceVariance) {
		this.priceVariance = priceVariance;
	}
	public String getPriveVP() {
		return priveVP;
	}
	public void setPriveVP(String priveVP) {
		this.priveVP = priveVP;
	}
	public String getTotalNetValue() {
		return totalNetValue;
	}
	public void setTotalNetValue(String totalNetValue) {
		this.totalNetValue = totalNetValue;
	}
	public String getPoTotalNetValue() {
		return poTotalNetValue;
	}
	public void setPoTotalNetValue(String poTotalNetValue) {
		this.poTotalNetValue = poTotalNetValue;
	}
	public String getIncoTerm() {
		return incoTerm;
	}
	public void setIncoTerm(String incoTerm) {
		this.incoTerm = incoTerm;
	}
	public String getPoIncoTerm() {
		return poIncoTerm;
	}
	public void setPoIncoTerm(String poIncoTerm) {
		this.poIncoTerm = poIncoTerm;
	}
	public String getIncoTermDesc() {
		return incoTermDesc;
	}
	public void setIncoTermDesc(String incoTermDesc) {
		this.incoTermDesc = incoTermDesc;
	}
	public String getLeadTime() {
		return leadTime;
	}
	public void setLeadTime(String leadTime) {
		this.leadTime = leadTime;
	}	
	public String getQtPrice() {
		return qtPrice;
	}
	public void setQtPrice(String qtPrice) {
		this.qtPrice = qtPrice;
	}
	public String getPoLeadTime() {
		return poLeadTime;
	}
	public void setPoLeadTime(String poLeadTime) {
		this.poLeadTime = poLeadTime;
	}
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	public String getMaterialGroup() {
		return materialGroup;
	}
	public void setMaterialGroup(String materialGroup) {
		this.materialGroup = materialGroup;
	}
	public String getPoMaterialGroup() {
		return poMaterialGroup;
	}
	public void setPoMaterialGroup(String poMaterialGroup) {
		this.poMaterialGroup = poMaterialGroup;
	}
	public String getMaterialGroupDesc() {
		return materialGroupDesc;
	}
	public void setMaterialGroupDesc(String materialGroupDesc) {
		this.materialGroupDesc = materialGroupDesc;
	}
	public String getTypeCategory() {
		return typeCategory;
	}
	public void setTypeCategory(String typeCategory) {
		this.typeCategory = typeCategory;
	}
	public String getCurrencyMisMatch() {
		return currencyMisMatch;
	}
	public void setCurrencyMisMatch(String currencyMisMatch) {
		this.currencyMisMatch = currencyMisMatch;
	}
	public String getLeadTimeMisMatch() {
		return leadTimeMisMatch;
	}
	public void setLeadTimeMisMatch(String leadTimeMisMatch) {
		this.leadTimeMisMatch = leadTimeMisMatch;
	}
	public String getMoqMisMatch() {
		return moqMisMatch;
	}
	public void setMoqMisMatch(String moqMisMatch) {
		this.moqMisMatch = moqMisMatch;
	}
	public String getUnitMisMatch() {
		return unitMisMatch;
	}
	public void setUnitMisMatch(String unitMisMatch) {
		this.unitMisMatch = unitMisMatch;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public String getIncoTermMismatch() {
		return incoTermMismatch;
	}
	public void setIncoTermMismatch(String incoTermMismatch) {
		this.incoTermMismatch = incoTermMismatch;
	}
	public String getSseContractFound() {
		return sseContractFound;
	}
	public void setSseContractFound(String sseContractFound) {
		this.sseContractFound = sseContractFound;
	}
	public String getMatchCategory() {
		return matchCategory;
	}
	public void setMatchCategory(String matchCategory) {
		this.matchCategory = matchCategory;
	}
	public String getSseMatchFound() {
		return sseMatchFound;
	}
	public void setSseMatchFound(String sseMatchFound) {
		this.sseMatchFound = sseMatchFound;
	}
	public String getPoItemType() {
		return poItemType;
	}
	public void setPoItemType(String poItemType) {
		this.poItemType = poItemType;
	}
	public String getSseMoq() {
		return sseMoq;
	}
	public void setSseMoq(String sseMoq) {
		this.sseMoq = sseMoq;
	}
	public String getSseCurrency() {
		return sseCurrency;
	}
	public void setSseCurrency(String sseCurrency) {
		this.sseCurrency = sseCurrency;
	}
	public String getSseIncoTerm() {
		return sseIncoTerm;
	}
	public void setSseIncoTerm(String sseIncoTerm) {
		this.sseIncoTerm = sseIncoTerm;
	}
	public String getSseLeadTime() {
		return sseLeadTime;
	}
	public void setSseLeadTime(String sseLeadTime) {
		this.sseLeadTime = sseLeadTime;
	}
	public String getCurrencyMI() {
		return currencyMI;
	}
	public void setCurrencyMI(String currencyMI) {
		this.currencyMI = currencyMI;
	}
	public String getLeadTimeMI() {
		return leadTimeMI;
	}
	public void setLeadTimeMI(String leadTimeMI) {
		this.leadTimeMI = leadTimeMI;
	}
	public String getMoqMI() {
		return moqMI;
	}
	public void setMoqMI(String moqMI) {
		this.moqMI = moqMI;
	}
	public String getUnitMI() {
		return unitMI;
	}
	public void setUnitMI(String unitMI) {
		this.unitMI = unitMI;
	}
	public String getIncoTermMI() {
		return incoTermMI;
	}
	public void setIncoTermMI(String incoTermMI) {
		this.incoTermMI = incoTermMI;
	}
	public String getSumPriceVariance() {
		return sumPriceVariance;
	}
	public void setSumPriceVariance(String sumPriceVariance) {
		this.sumPriceVariance = sumPriceVariance;
	}
	public String getQtCount() {
		return qtCount;
	}
	public void setQtCount(String qtCount) {
		this.qtCount = qtCount;
	}
	public String getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public String getSapPlant() {
		return sapPlant;
	}
	public void setSapPlant(String sapPlant) {
		this.sapPlant = sapPlant;
	}
	public String getQtPlant() {
		return qtPlant;
	}
	public void setQtPlant(String qtPlant) {
		this.qtPlant = qtPlant;
	}
	public String getQtVendorNumber() {
		return qtVendorNumber;
	}
	public void setQtVendorNumber(String qtVendorNumber) {
		this.qtVendorNumber = qtVendorNumber;
	}
	public String getQtCurrency() {
		return qtCurrency;
	}
	public void setQtCurrency(String qtCurrency) {
		this.qtCurrency = qtCurrency;
	}
	public String getQtMaterialNumber() {
		return qtMaterialNumber;
	}
	public void setQtMaterialNumber(String qtMaterialNumber) {
		this.qtMaterialNumber = qtMaterialNumber;
	}
	public String getQtUom() {
		return qtUom;
	}
	public void setQtUom(String qtUom) {
		this.qtUom = qtUom;
	}
	public String getQtIncoTerm() {
		return qtIncoTerm;
	}
	public void setQtIncoTerm(String qtIncoTerm) {
		this.qtIncoTerm = qtIncoTerm;
	}
	public String getQtLeadTime() {
		return qtLeadTime;
	}
	public void setQtLeadTime(String qtLeadTime) {
		this.qtLeadTime = qtLeadTime;
	}
	public String getQtContractStartDate() {
		return qtContractStartDate;
	}
	public void setQtContractStartDate(String qtContractStartDate) {
		this.qtContractStartDate = qtContractStartDate;
	}
	public String getTotalNetvalueVariance() {
		return totalNetvalueVariance;
	}
	public void setTotalNetvalueVariance(String totalNetvalueVariance) {
		this.totalNetvalueVariance = totalNetvalueVariance;
	}
	public String getQtYear() {
		return qtYear;
	}
	public void setQtYear(String qtYear) {
		this.qtYear = qtYear;
	}
	public String getPpvCount() {
		return ppvCount;
	}
	public void setPpvCount(String ppvCount) {
		this.ppvCount = ppvCount;
	}
	public String getPostingYear() {
		return postingYear;
	}
	public void setPostingYear(String postingYear) {
		this.postingYear = postingYear;
	}
	public String getPostingMonth() {
		return postingMonth;
	}
	public void setPostingMonth(String postingMonth) {
		this.postingMonth = postingMonth;
	}
	public String getPlantName() {
		return plantName;
	}
	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}
	public String getPostingDateDoc() {
		return postingDateDoc;
	}
	public void setPostingDateDoc(String postingDateDoc) {
		this.postingDateDoc = postingDateDoc;
	}
	public String getDocumentCurrency() {
		return documentCurrency;
	}
	public void setDocumentCurrency(String documentCurrency) {
		this.documentCurrency = documentCurrency;
	}	
	public String getInvoiceCurrency() {
		return invoiceCurrency;
	}
	public void setInvoiceCurrency(String invoiceCurrency) {
		this.invoiceCurrency = invoiceCurrency;
	}
	public String getPaymentTerms() {
		return paymentTerms;
	}
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	public String getPaymentTermsDesc() {
		return paymentTermsDesc;
	}
	public void setPaymentTermsDesc(String paymentTermsDesc) {
		this.paymentTermsDesc = paymentTermsDesc;
	}
	public String getSumamountBaseCurrency() {
		return sumamountBaseCurrency;
	}
	public void setSumamountBaseCurrency(String sumamountBaseCurrency) {
		this.sumamountBaseCurrency = sumamountBaseCurrency;
	}	
	public String getMaterialType() {
		return materialType;
	}
	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}
	public String getPurchaseDocNo() {
		return purchaseDocNo;
	}
	public void setPurchaseDocNo(String purchaseDocNo) {
		this.purchaseDocNo = purchaseDocNo;
	}
	public String getGlAccount() {
		return glAccount;
	}
	public void setGlAccount(String glAccount) {
		this.glAccount = glAccount;
	}
	public String getMaterialCode() {
		return materialCode;
	}
	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}
	public String getMaterialCodePred() {
		return materialCodePred;
	}
	public void setMaterialCodePred(String materialCodePred) {
		this.materialCodePred = materialCodePred;
	}
	public String getMaterialDesPred() {
		return materialDesPred;
	}
	public void setMaterialDesPred(String materialDesPred) {
		this.materialDesPred = materialDesPred;
	}
	public String getMaterialSpecRef() {
		return materialSpecRef;
	}
	public void setMaterialSpecRef(String materialSpecRef) {
		this.materialSpecRef = materialSpecRef;
	}
	public String getVendorNo() {
		return vendorNo;
	}
	public void setVendorNo(String vendorNo) {
		this.vendorNo = vendorNo;
	}
	public String getSecondaryVendorNo() {
		return secondaryVendorNo;
	}
	public void setSecondaryVendorNo(String secondaryVendorNo) {
		this.secondaryVendorNo = secondaryVendorNo;
	}
	public String getSecondaryVendorName() {
		return secondaryVendorName;
	}
	public void setSecondaryVendorName(String secondaryVendorName) {
		this.secondaryVendorName = secondaryVendorName;
	}
	public String getVendorCountry() {
		return vendorCountry;
	}
	public void setVendorCountry(String vendorCountry) {
		this.vendorCountry = vendorCountry;
	}
	public String getVendorCity() {
		return vendorCity;
	}
	public void setVendorCity(String vendorCity) {
		this.vendorCity = vendorCity;
	}
	public String getSecondaryVendorCountry() {
		return secondaryVendorCountry;
	}
	public void setSecondaryVendorCountry(String secondaryVendorCountry) {
		this.secondaryVendorCountry = secondaryVendorCountry;
	}
	public String getSecondaryVendorCity() {
		return secondaryVendorCity;
	}
	public void setSecondaryVendorCity(String secondaryVendorCity) {
		this.secondaryVendorCity = secondaryVendorCity;
	}
	public String getDistributorNo() {
		return distributorNo;
	}
	public void setDistributorNo(String distributorNo) {
		this.distributorNo = distributorNo;
	}
	public String getDistributorName() {
		return distributorName;
	}
	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}
	public String getDistributorCountry() {
		return distributorCountry;
	}
	public void setDistributorCountry(String distributorCountry) {
		this.distributorCountry = distributorCountry;
	}
	public String getDistributorCity() {
		return distributorCity;
	}
	public void setDistributorCity(String distributorCity) {
		this.distributorCity = distributorCity;
	}
	public String getSecondaryDistributorNo() {
		return secondaryDistributorNo;
	}
	public void setSecondaryDistributorNo(String secondaryDistributorNo) {
		this.secondaryDistributorNo = secondaryDistributorNo;
	}
	public String getSecondaryDistributorName() {
		return secondaryDistributorName;
	}
	public void setSecondaryDistributorName(String secondaryDistributorName) {
		this.secondaryDistributorName = secondaryDistributorName;
	}
	public String getSecondaryDistributorCountry() {
		return secondaryDistributorCountry;
	}
	public void setSecondaryDistributorCountry(String secondaryDistributorCountry) {
		this.secondaryDistributorCountry = secondaryDistributorCountry;
	}
	public String getSecondaryDistributorCity() {
		return secondaryDistributorCity;
	}
	public void setSecondaryDistributorCity(String secondaryDistributorCity) {
		this.secondaryDistributorCity = secondaryDistributorCity;
	}
	public String getManufacturerNo() {
		return manufacturerNo;
	}
	public void setManufacturerNo(String manufacturerNo) {
		this.manufacturerNo = manufacturerNo;
	}
	public String getManufacturerName() {
		return manufacturerName;
	}
	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}
	public String getManufacturerCountry() {
		return manufacturerCountry;
	}
	public void setManufacturerCountry(String manufacturerCountry) {
		this.manufacturerCountry = manufacturerCountry;
	}
	public String getManufacturerCity() {
		return manufacturerCity;
	}
	public void setManufacturerCity(String manufacturerCity) {
		this.manufacturerCity = manufacturerCity;
	}
	public String getSecondaryManufacturerNo() {
		return secondaryManufacturerNo;
	}
	public void setSecondaryManufacturerNo(String secondaryManufacturerNo) {
		this.secondaryManufacturerNo = secondaryManufacturerNo;
	}
	public String getSecondaryManufacturerName() {
		return secondaryManufacturerName;
	}
	public void setSecondaryManufacturerName(String secondaryManufacturerName) {
		this.secondaryManufacturerName = secondaryManufacturerName;
	}
	public String getSecondaryManufacturerCountry() {
		return secondaryManufacturerCountry;
	}
	public void setSecondaryManufacturerCountry(String secondaryManufacturerCountry) {
		this.secondaryManufacturerCountry = secondaryManufacturerCountry;
	}
	public String getSecondaryManufacturerCity() {
		return secondaryManufacturerCity;
	}
	public void setSecondaryManufacturerCity(String secondaryManufacturerCity) {
		this.secondaryManufacturerCity = secondaryManufacturerCity;
	}
	public String getMaterialCategory() {
		return materialCategory;
	}
	public void setMaterialCategory(String materialCategory) {
		this.materialCategory = materialCategory;
	}
	public String getMaterialSubCategory() {
		return materialSubCategory;
	}
	public void setMaterialSubCategory(String materialSubCategory) {
		this.materialSubCategory = materialSubCategory;
	}
	public String getPurchasingUom() {
		return purchasingUom;
	}
	public void setPurchasingUom(String purchasingUom) {
		this.purchasingUom = purchasingUom;
	}
	public String getStockingUom() {
		return stockingUom;
	}
	public void setStockingUom(String stockingUom) {
		this.stockingUom = stockingUom;
	}
	public String getConversionFactor() {
		return conversionFactor;
	}
	public void setConversionFactor(String conversionFactor) {
		this.conversionFactor = conversionFactor;
	}
	public String getInvQuantity() {
		return invQuantity;
	}
	public void setInvQuantity(String invQuantity) {
		this.invQuantity = invQuantity;
	}
	public String getReceiptQuantity() {
		return receiptQuantity;
	}
	public void setReceiptQuantity(String receiptQuantity) {
		this.receiptQuantity = receiptQuantity;
	}
	public String getMoq() {
		return moq;
	}
	public void setMoq(String moq) {
		this.moq = moq;
	}
	public String getIoq() {
		return ioq;
	}
	public void setIoq(String ioq) {
		this.ioq = ioq;
	}
	public String getIncotermCode() {
		return incotermCode;
	}
	public void setIncotermCode(String incotermCode) {
		this.incotermCode = incotermCode;
	}
	public String getIncotermDesc() {
		return incotermDesc;
	}
	public void setIncotermDesc(String incotermDesc) {
		this.incotermDesc = incotermDesc;
	}
	public String getIncotermLocationCountry() {
		return incotermLocationCountry;
	}
	public void setIncotermLocationCountry(String incotermLocationCountry) {
		this.incotermLocationCountry = incotermLocationCountry;
	}
	public String getIncotermLocationCity() {
		return incotermLocationCity;
	}
	public void setIncotermLocationCity(String incotermLocationCity) {
		this.incotermLocationCity = incotermLocationCity;
	}
	public String getPaymentDays() {
		return paymentDays;
	}
	public void setPaymentDays(String paymentDays) {
		this.paymentDays = paymentDays;
	}
	public String getVmiFlag() {
		return vmiFlag;
	}
	public void setVmiFlag(String vmiFlag) {
		this.vmiFlag = vmiFlag;
	}
	public String getPurchaseCurrency() {
		return purchaseCurrency;
	}
	public void setPurchaseCurrency(String purchaseCurrency) {
		this.purchaseCurrency = purchaseCurrency;
	}
	public String getPriceValidityStartDate() {
		return priceValidityStartDate;
	}
	public void setPriceValidityStartDate(String priceValidityStartDate) {
		this.priceValidityStartDate = priceValidityStartDate;
	}
	public String getPriceValidityEndDate() {
		return priceValidityEndDate;
	}
	public void setPriceValidityEndDate(String priceValidityEndDate) {
		this.priceValidityEndDate = priceValidityEndDate;
	}
	public String getAmountDocCurrency() {
		return amountDocCurrency;
	}
	public void setAmountDocCurrency(String amountDocCurrency) {
		this.amountDocCurrency = amountDocCurrency;
	}
	public String getAmountBaseCurrency() {
		return amountBaseCurrency;
	}
	public void setAmountBaseCurrency(String amountBaseCurrency) {
		this.amountBaseCurrency = amountBaseCurrency;
	}
	public String getAmountEurActualrate() {
		return amountEurActualrate;
	}
	public void setAmountEurActualrate(String amountEurActualrate) {
		this.amountEurActualrate = amountEurActualrate;
	}
	public String getAmountEur15wahrRate() {
		return amountEur15wahrRate;
	}
	public void setAmountEur15wahrRate(String amountEur15wahrRate) {
		this.amountEur15wahrRate = amountEur15wahrRate;
	}
	public String getPounitpriceDocCurrency() {
		return pounitpriceDocCurrency;
	}
	public void setPounitpriceDocCurrency(String pounitpriceDocCurrency) {
		this.pounitpriceDocCurrency = pounitpriceDocCurrency;
	}
	public String getPounitpriceEurConvertedActualrate() {
		return pounitpriceEurConvertedActualrate;
	}
	public void setPounitpriceEurConvertedActualrate(
			String pounitpriceEurConvertedActualrate) {
		this.pounitpriceEurConvertedActualrate = pounitpriceEurConvertedActualrate;
	}
	public String getPounitpriceEurConverted15wahr() {
		return pounitpriceEurConverted15wahr;
	}
	public void setPounitpriceEurConverted15wahr(
			String pounitpriceEurConverted15wahr) {
		this.pounitpriceEurConverted15wahr = pounitpriceEurConverted15wahr;
	}
	public String getTotalpoEurActualrate() {
		return totalpoEurActualrate;
	}
	public void setTotalpoEurActualrate(String totalpoEurActualrate) {
		this.totalpoEurActualrate = totalpoEurActualrate;
	}
	public String getTotalpoEur15wahr() {
		return totalpoEur15wahr;
	}
	public void setTotalpoEur15wahr(String totalpoEur15wahr) {
		this.totalpoEur15wahr = totalpoEur15wahr;
	}
	public String getPurchasePriceUom() {
		return purchasePriceUom;
	}
	public void setPurchasePriceUom(String purchasePriceUom) {
		this.purchasePriceUom = purchasePriceUom;
	}
	public String getInvoicePriceDocCurrency() {
		return invoicePriceDocCurrency;
	}
	public void setInvoicePriceDocCurrency(String invoicePriceDocCurrency) {
		this.invoicePriceDocCurrency = invoicePriceDocCurrency;
	}
	public String getInvoicePriceEurActualrate() {
		return invoicePriceEurActualrate;
	}
	public void setInvoicePriceEurActualrate(String invoicePriceEurActualrate) {
		this.invoicePriceEurActualrate = invoicePriceEurActualrate;
	}
	public String getInvoicePriceEurIr() {
		return invoicePriceEurIr;
	}
	public void setInvoicePriceEurIr(String invoicePriceEurIr) {
		this.invoicePriceEurIr = invoicePriceEurIr;
	}
	public String getInvoicePriceEur15wahr() {
		return invoicePriceEur15wahr;
	}
	public void setInvoicePriceEur15wahr(String invoicePriceEur15wahr) {
		this.invoicePriceEur15wahr = invoicePriceEur15wahr;
	}
	public String getInvoicePriceEurIr15wahr() {
		return invoicePriceEurIr15wahr;
	}
	public void setInvoicePriceEurIr15wahr(String invoicePriceEurIr15wahr) {
		this.invoicePriceEurIr15wahr = invoicePriceEurIr15wahr;
	}
	public String getStandardCostperunitBaseCurrency() {
		return standardCostperunitBaseCurrency;
	}
	public void setStandardCostperunitBaseCurrency(
			String standardCostperunitBaseCurrency) {
		this.standardCostperunitBaseCurrency = standardCostperunitBaseCurrency;
	}
	public String getStandardCostperunitEurActualrate() {
		return standardCostperunitEurActualrate;
	}
	public void setStandardCostperunitEurActualrate(
			String standardCostperunitEurActualrate) {
		this.standardCostperunitEurActualrate = standardCostperunitEurActualrate;
	}
	public String getStandardCostperunitEur15wahr() {
		return standardCostperunitEur15wahr;
	}
	public void setStandardCostperunitEur15wahr(String standardCostperunitEur15wahr) {
		this.standardCostperunitEur15wahr = standardCostperunitEur15wahr;
	}
	public String getTotalStandardCost() {
		return totalStandardCost;
	}
	public void setTotalStandardCost(String totalStandardCost) {
		this.totalStandardCost = totalStandardCost;
	}
	public String getPpv2AmountDocCurrency() {
		return ppv2AmountDocCurrency;
	}
	public void setPpv2AmountDocCurrency(String ppv2AmountDocCurrency) {
		this.ppv2AmountDocCurrency = ppv2AmountDocCurrency;
	}
	public String getPpv2AmountEurActualrate() {
		return ppv2AmountEurActualrate;
	}
	public void setPpv2AmountEurActualrate(String ppv2AmountEurActualrate) {
		this.ppv2AmountEurActualrate = ppv2AmountEurActualrate;
	}
	public String getPpv2AmountEur15wahr() {
		return ppv2AmountEur15wahr;
	}
	public void setPpv2AmountEur15wahr(String ppv2AmountEur15wahr) {
		this.ppv2AmountEur15wahr = ppv2AmountEur15wahr;
	}
	public String getPpv1AmountDocCurrency() {
		return ppv1AmountDocCurrency;
	}
	public void setPpv1AmountDocCurrency(String ppv1AmountDocCurrency) {
		this.ppv1AmountDocCurrency = ppv1AmountDocCurrency;
	}
	public String getPpv1AmountEurActualrate() {
		return ppv1AmountEurActualrate;
	}
	public void setPpv1AmountEurActualrate(String ppv1AmountEurActualrate) {
		this.ppv1AmountEurActualrate = ppv1AmountEurActualrate;
	}
	public String getPpv1AmountEur15wahr() {
		return ppv1AmountEur15wahr;
	}
	public void setPpv1AmountEur15wahr(String ppv1AmountEur15wahr) {
		this.ppv1AmountEur15wahr = ppv1AmountEur15wahr;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getSseTransaction() {
		return sseTransaction;
	}
	public void setSseTransaction(String sseTransaction) {
		this.sseTransaction = sseTransaction;
	}
	public String getActualExchangeRate() {
		return actualExchangeRate;
	}
	public void setActualExchangeRate(String actualExchangeRate) {
		this.actualExchangeRate = actualExchangeRate;
	}
	public String getWeightedAverageHedgeRate15wahr() {
		return weightedAverageHedgeRate15wahr;
	}
	public void setWeightedAverageHedgeRate15wahr(
			String weightedAverageHedgeRate15wahr) {
		this.weightedAverageHedgeRate15wahr = weightedAverageHedgeRate15wahr;
	}
	public String getPoCostQuartile() {
		return poCostQuartile;
	}
	public void setPoCostQuartile(String poCostQuartile) {
		this.poCostQuartile = poCostQuartile;
	}	
	public String getNetvalueQuartile() {
		return netvalueQuartile;
	}
	public void setNetvalueQuartile(String netvalueQuartile) {
		this.netvalueQuartile = netvalueQuartile;
	}
	public String getNetvarianceQuartile() {
		return netvarianceQuartile;
	}
	public void setNetvarianceQuartile(String netvarianceQuartile) {
		this.netvarianceQuartile = netvarianceQuartile;
	}
	public String getPpvMismatch() {
		return ppvMismatch;
	}
	public void setPpvMismatch(String ppvMismatch) {
		this.ppvMismatch = ppvMismatch;
	}
	public String getDocumentCategory() {
		return documentCategory;
	}
	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}
	public String getDocumentCategoryDesc() {
		return documentCategoryDesc;
	}
	public void setDocumentCategoryDesc(String documentCategoryDesc) {
		this.documentCategoryDesc = documentCategoryDesc;
	}
	public String getFavourable() {
		return favourable;
	}
	public void setFavourable(String favourable) {
		this.favourable = favourable;
	}	
	public String getVariance() {
		return variance;
	}
	public void setVariance(String variance) {
		this.variance = variance;
	}
	public String getTotalInvoicePriceQuartile() {
		return totalInvoicePriceQuartile;
	}
	public void setTotalInvoicePriceQuartile(String totalInvoicePriceQuartile) {
		this.totalInvoicePriceQuartile = totalInvoicePriceQuartile;
	}
	public String getStandardCostQuartile() {
		return standardCostQuartile;
	}
	public void setStandardCostQuartile(String standardCostQuartile) {
		this.standardCostQuartile = standardCostQuartile;
	}
	public String getPpv2Quartile() {
		return ppv2Quartile;
	}
	public void setPpv2Quartile(String ppv2Quartile) {
		this.ppv2Quartile = ppv2Quartile;
	}
	public String getInvoicePriceBaseCurrency() {
		return invoicePriceBaseCurrency;
	}
	public void setInvoicePriceBaseCurrency(String invoicePriceBaseCurrency) {
		this.invoicePriceBaseCurrency = invoicePriceBaseCurrency;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getInvoiceLineNumber() {
		return invoiceLineNumber;
	}
	public void setInvoiceLineNumber(String invoiceLineNumber) {
		this.invoiceLineNumber = invoiceLineNumber;
	}
	public String getTotalpoAmtEurActualrate() {
		return totalpoAmtEurActualrate;
	}
	public void setTotalpoAmtEurActualrate(String totalpoAmtEurActualrate) {
		this.totalpoAmtEurActualrate = totalpoAmtEurActualrate;
	}
	public String getTotalpoAmtEur15wahr() {
		return totalpoAmtEur15wahr;
	}
	public void setTotalpoAmtEur15wahr(String totalpoAmtEur15wahr) {
		this.totalpoAmtEur15wahr = totalpoAmtEur15wahr;
	}
	public String getInvoiceSpendEurActualrate() {
		return invoiceSpendEurActualrate;
	}
	public void setInvoiceSpendEurActualrate(String invoiceSpendEurActualrate) {
		this.invoiceSpendEurActualrate = invoiceSpendEurActualrate;
	}
	public String getInvoiceSpendEur15wahr() {
		return invoiceSpendEur15wahr;
	}
	public void setInvoiceSpendEur15wahr(String invoiceSpendEur15wahr) {
		this.invoiceSpendEur15wahr = invoiceSpendEur15wahr;
	}
	public String getTotalAmountAtStandardCost() {
		return totalAmountAtStandardCost;
	}
	public void setTotalAmountAtStandardCost(String totalAmountAtStandardCost) {
		this.totalAmountAtStandardCost = totalAmountAtStandardCost;
	}
	public String getVendorFreqQuantityQuartile() {
		return vendorFreqQuantityQuartile;
	}
	public void setVendorFreqQuantityQuartile(String vendorFreqQuantityQuartile) {
		this.vendorFreqQuantityQuartile = vendorFreqQuantityQuartile;
	}
	public String getVendorFreqPosQuartile() {
		return vendorFreqPosQuartile;
	}
	public void setVendorFreqPosQuartile(String vendorFreqPosQuartile) {
		this.vendorFreqPosQuartile = vendorFreqPosQuartile;
	}
	public String getMaterialFreqQuantityQuartile() {
		return materialFreqQuantityQuartile;
	}
	public void setMaterialFreqQuantityQuartile(String materialFreqQuantityQuartile) {
		this.materialFreqQuantityQuartile = materialFreqQuantityQuartile;
	}
	public String getMaterialFreqPosQuartile() {
		return materialFreqPosQuartile;
	}
	public void setMaterialFreqPosQuartile(String materialFreqPosQuartile) {
		this.materialFreqPosQuartile = materialFreqPosQuartile;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	public String getEffectiveContract() {
		return effectiveContract;
	}
	public void setEffectiveContract(String effectiveContract) {
		this.effectiveContract = effectiveContract;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public int getTotalppvDocCurrency() {
		return totalppvDocCurrency;
	}
	public void setTotalppvDocCurrency(int totalppvDocCurrency) {
		this.totalppvDocCurrency = totalppvDocCurrency;
	}
	public String getGssStatus() {
		return gssStatus;
	}
	public void setGssStatus(String gssStatus) {
		this.gssStatus = gssStatus;
	}
	public String getQtVendorName() {
		return qtVendorName;
	}
	public void setQtVendorName(String qtVendorName) {
		this.qtVendorName = qtVendorName;
	}
	public String getQtMaterialDesc() {
		return qtMaterialDesc;
	}
	public void setQtMaterialDesc(String qtMaterialDesc) {
		this.qtMaterialDesc = qtMaterialDesc;
	}
	public String getTotalPpvDocCurrency() {
		return totalPpvDocCurrency;
	}
	public void setTotalPpvDocCurrency(String totalPpvDocCurrency) {
		this.totalPpvDocCurrency = totalPpvDocCurrency;
	}
	public String getTotalPpvEurActualrate() {
		return totalPpvEurActualrate;
	}
	public void setTotalPpvEurActualrate(String totalPpvEurActualrate) {
		this.totalPpvEurActualrate = totalPpvEurActualrate;
	}
	public String getTotalPpvEur15wahr() {
		return totalPpvEur15wahr;
	}
	public void setTotalPpvEur15wahr(String totalPpvEur15wahr) {
		this.totalPpvEur15wahr = totalPpvEur15wahr;
	}
	public String getTotalAmtAtStandardCostBaseCurrency() {
		return totalAmtAtStandardCostBaseCurrency;
	}
	public void setTotalAmtAtStandardCostBaseCurrency(
			String totalAmtAtStandardCostBaseCurrency) {
		this.totalAmtAtStandardCostBaseCurrency = totalAmtAtStandardCostBaseCurrency;
	}
	public String getTotalAmtAtStandardCostEurActualrate() {
		return totalAmtAtStandardCostEurActualrate;
	}
	public void setTotalAmtAtStandardCostEurActualrate(
			String totalAmtAtStandardCostEurActualrate) {
		this.totalAmtAtStandardCostEurActualrate = totalAmtAtStandardCostEurActualrate;
	}
	public String getTotalAmtAtStandardCostEur15wahr() {
		return totalAmtAtStandardCostEur15wahr;
	}
	public void setTotalAmtAtStandardCostEur15wahr(
			String totalAmtAtStandardCostEur15wahr) {
		this.totalAmtAtStandardCostEur15wahr = totalAmtAtStandardCostEur15wahr;
	}
	public String getTotalpoAmtDocCurrency() {
		return totalpoAmtDocCurrency;
	}
	public void setTotalpoAmtDocCurrency(String totalpoAmtDocCurrency) {
		this.totalpoAmtDocCurrency = totalpoAmtDocCurrency;
	}
	public String getSumInvoicespendEur() {
		return sumInvoicespendEur;
	}
	public void setSumInvoicespendEur(String sumInvoicespendEur) {
		this.sumInvoicespendEur = sumInvoicespendEur;
	}	
	public String getSumInvoicespendUsd() {
		return sumInvoicespendUsd;
	}
	public void setSumInvoicespendUsd(String sumInvoicespendUsd) {
		this.sumInvoicespendUsd = sumInvoicespendUsd;
	}
	public String getSumTotalppvEur() {
		return sumTotalppvEur;
	}
	public void setSumTotalppvEur(String sumTotalppvEur) {
		this.sumTotalppvEur = sumTotalppvEur;
	}
	public String getSumTotalppvUsd() {
		return sumTotalppvUsd;
	}
	public void setSumTotalppvUsd(String sumTotalppvUsd) {
		this.sumTotalppvUsd = sumTotalppvUsd;
	}
	public String getSapPlantCode() {
		return sapPlantCode;
	}
	public void setSapPlantCode(String sapPlantCode) {
		this.sapPlantCode = sapPlantCode;
	}
	public String getCommodityLevel1() {
		return commodityLevel1;
	}
	public void setCommodityLevel1(String commodityLevel1) {
		this.commodityLevel1 = commodityLevel1;
	}
	public String getTotalPpvNewEurActualrate() {
		return totalPpvNewEurActualrate;
	}
	public void setTotalPpvNewEurActualrate(String totalPpvNewEurActualrate) {
		this.totalPpvNewEurActualrate = totalPpvNewEurActualrate;
	}
	public String getStandardCostperunitDocCurrency() {
		return standardCostperunitDocCurrency;
	}
	public void setStandardCostperunitDocCurrency(
			String standardCostperunitDocCurrency) {
		this.standardCostperunitDocCurrency = standardCostperunitDocCurrency;
	}
	public String getVendorMismatch() {
		return vendorMismatch;
	}
	public void setVendorMismatch(String vendorMismatch) {
		this.vendorMismatch = vendorMismatch;
	}
	public String getAmountType() {
		return amountType;
	}
	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}
	public String getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}
	public int getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(int totalAmt) {
		this.totalAmt = totalAmt;
	}
	public int getWuppertalAmt() {
		return wuppertalAmt;
	}
	public void setWuppertalAmt(int wuppertalAmt) {
		this.wuppertalAmt = wuppertalAmt;
	}
	public int getSezanneAmt() {
		return sezanneAmt;
	}
	public void setSezanneAmt(int sezanneAmt) {
		this.sezanneAmt = sezanneAmt;
	}
	public int getPomeziaAmt() {
		return pomeziaAmt;
	}
	public void setPomeziaAmt(int pomeziaAmt) {
		this.pomeziaAmt = pomeziaAmt;
	}
	public int getCapeTownAmt() {
		return capeTownAmt;
	}
	public void setCapeTownAmt(int capeTownAmt) {
		this.capeTownAmt = capeTownAmt;
	}
	public int getEastLondonAmt() {
		return eastLondonAmt;
	}
	public void setEastLondonAmt(int eastLondonAmt) {
		this.eastLondonAmt = eastLondonAmt;
	}
	public int getValDeRuilAmt() {
		return valDeRuilAmt;
	}
	public void setValDeRuilAmt(int valDeRuilAmt) {
		this.valDeRuilAmt = valDeRuilAmt;
	}
	public int getHelsingborgAmt() {
		return helsingborgAmt;
	}
	public void setHelsingborgAmt(int helsingborgAmt) {
		this.helsingborgAmt = helsingborgAmt;
	}
	public int getMadridAmt() {
		return madridAmt;
	}
	public void setMadridAmt(int madridAmt) {
		this.madridAmt = madridAmt;
	}
	public int getMandraAmt() {
		return mandraAmt;
	}
	public void setMandraAmt(int mandraAmt) {
		this.mandraAmt = mandraAmt;
	}
	public int getSumInvoicespendActual() {
		return sumInvoicespendActual;
	}
	public void setSumInvoicespendActual(int sumInvoicespendActual) {
		this.sumInvoicespendActual = sumInvoicespendActual;
	}
	public int getSumInvoicespendWahr() {
		return sumInvoicespendWahr;
	}
	public void setSumInvoicespendWahr(int sumInvoicespendWahr) {
		this.sumInvoicespendWahr = sumInvoicespendWahr;
	}
	public int getSumInvoicespendBp() {
		return sumInvoicespendBp;
	}
	public void setSumInvoicespendBp(int sumInvoicespendBp) {
		this.sumInvoicespendBp = sumInvoicespendBp;
	}
	public int getTotalppvFcurActualrate() {
		return totalppvFcurActualrate;
	}
	public void setTotalppvFcurActualrate(int totalppvFcurActualrate) {
		this.totalppvFcurActualrate = totalppvFcurActualrate;
	}
	public int getTotalppvFcurWahr() {
		return totalppvFcurWahr;
	}
	public void setTotalppvFcurWahr(int totalppvFcurWahr) {
		this.totalppvFcurWahr = totalppvFcurWahr;
	}
	public int getTotalppvFcurBp() {
		return totalppvFcurBp;
	}
	public void setTotalppvFcurBp(int totalppvFcurBp) {
		this.totalppvFcurBp = totalppvFcurBp;
	}
	public String getFcur() {
		return fcur;
	}
	public void setFcur(String fcur) {
		this.fcur = fcur;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public int getOtherMismatchCount() {
		return otherMismatchCount;
	}
	public void setOtherMismatchCount(int otherMismatchCount) {
		this.otherMismatchCount = otherMismatchCount;
	}
	public int getCairoAmt() {
		return cairoAmt;
	}
	public void setCairoAmt(int cairoAmt) {
		this.cairoAmt = cairoAmt;
	}
	
	
}
