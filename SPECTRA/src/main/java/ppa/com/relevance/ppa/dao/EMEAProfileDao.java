package com.relevance.ppa.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.json.JSONArray;

import com.relevance.ppa.data.EMEAView;
import com.relevance.prism.dao.BaseDao;
import com.relevance.prism.data.DataTableView;
import com.relevance.prism.data.E2emfView;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismDbUtil;

public class EMEAProfileDao extends BaseDao{

	static String emeaPOProfileDefaultQuery = "SELECT pocount, currency, sapplantcode, plantdesc, companycode, companycodedesc, poreqdate, ssecontractindicator, compliantindicator, pricevariancepercentage, pricevariance, sumpricevariance, netvaluevariancepercentage, sumnetvalue, sseitemmatchindicator,  currencymismatch, leadtimemismatch, moqmismatch, unitmismatch, incotermmismatch, itemtype, subcategory, category, gss_status, vendormismatch, other_mismatch_count FROM ";
	
	static String emeaPOProfileFilterQuery = "SELECT count(*) as pocount,po_currency as currency, sapplantcode, plantdesc,company_code as companycode,'' as companycodedesc, po_req_date as poreqdate, sse_contract_found	 as ssecontractindicator,compliant_indicator as compliantindicator,price_variance_percentage as pricevariancepercentage, favourable as pricevariance, sum(total_netvalue_variance) as sumpricevariance,0 as netvaluevariancepercentage, sum(po_total_netvalue) as sumnetvalue,sse_match_found as sseitemmatchindicator, pricevariancepercentagequartile,currency_mismatch_ind as currencymismatch,lead_time_mismatch_ind as leadtimemismatch,moq_mismatch_ind as moqmismatch,unit_mismatch_ind as unitmismatch,incoterm_mismatch_ind as incotermmismatch,po_item_type as itemtype, pocostquartile,netvaluequartile,netvariancequartile,gss_status FROM ";
	static String groupByClausePOProfileFilter =" group by  currency, sapplantcode, plantdesc, companycode, companycodedesc, poreqdate, ssecontractindicator, compliantindicator, pricevariancepercentage, pricevariance, netvaluevariancepercentage, sseitemmatchindicator, pricevariancepercentagequartile, currencymismatch, leadtimemismatch, moqmismatch, unitmismatch, incotermmismatch, itemtype,pocostquartile,netvaluequartile,netvariancequartile,gss_status";
	
	static String emeaPOProfileL2Query = "SELECT sapplantcode, plantdesc, sse_plant, po_number, po_line_number, vendor_number, vendor_name, po_creation_date, po_req_date, po_currency, po_exchangerate, company_code, po_material_number, sse_material_number, po_material_desc, po_order_qty, po_uom, po_exp_cost, sse_price, price_variance, price_variance_percentage, po_total_netvalue, po_inco_term, inco_term_desc, po_lead_time, sse_contract_found, po_material_group, material_group_desc, match_category, compliant_indicator, sse_match_found, po_item_type, sse_moq, sse_currency, sse_incoterm, sse_leadtime, currency_mismatch_ind, lead_time_mismatch_ind, moq_mismatch_ind, unit_mismatch_ind, incoterm_mismatch_ind, gss_status, total_netvalue_variance FROM ";
	static String emeaPOProfilegroupByClauseFilter = " group by sapplantcode, plantdesc, sse_plant, po_number, po_line_number, vendor_number, vendor_name, po_creation_date, po_req_date, po_currency, po_exchangerate, company_code, po_material_number, sse_material_number, po_material_desc, po_order_qty, po_uom, po_exp_cost, sse_price, price_variance, price_variance_percentage, po_total_netvalue, po_inco_term, inco_term_desc, po_lead_time, sse_contract_found, po_material_group, material_group_desc, match_category, compliant_indicator, sse_match_found, po_item_type, sse_moq, sse_currency, sse_incoterm, sse_leadtime, currency_mismatch_ind, lead_time_mismatch_ind, moq_mismatch_ind, unit_mismatch_ind, incoterm_mismatch_ind, gss_status, total_netvalue_variance";
	
	static String emeaQuotesProfileDefaultQuery = "SELECT qtcount, currency, sapplantcode, plantdesc, contractstartdate, qt_year, ssecontractindicator, compliantindicator, pricevariancepercentage, pricevariance, netvaluevariancepercentage, sumnetvalue, sseitemmatchindicator, currencymismatch, leadtimemismatch, moqmismatch, unitmismatch, incotermmismatch, category, subcategory, direction, effectivecontract, vendormismatch, other_mismatch_count FROM ";
	
	static String emeaQuotesProfileL2Query = "SELECT sap_plant, qt_plant, qt_vendor_number, qt_vendor_name, qt_currency, qt_material_number, qt_material_desc, qt_uom, qt_inco_term, qt_lead_time, qt_price, qt_contract_start_date, sse_plant, sse_material_number, sse_price, sse_moq, sse_currency, sse_incoterm, sse_leadtime, price_variance, price_variance_percentage, match_category, compliant_indicator, sse_contract_found, sse_match_found, currency_mismatch_ind, lead_time_mismatch_ind, moq_mismatch_ind, unit_mismatch_ind, incoterm_mismatch_ind FROM ";
	static String emeaQuotesProfilegroupByClauseFilter = " group by sap_plant, qt_plant, qt_vendor_number, qt_vendor_name, qt_currency, qt_material_number, qt_material_desc, qt_uom, qt_inco_term, qt_lead_time, qt_price, qt_contract_start_date, sse_plant, sse_material_number, sse_price, sse_moq, sse_currency, sse_incoterm, sse_leadtime, price_variance, price_variance_percentage, match_category, compliant_indicator, sse_contract_found, sse_match_found, currency_mismatch_ind, lead_time_mismatch_ind, moq_mismatch_ind, unit_mismatch_ind, incoterm_mismatch_ind";  
	
	//static String emeaPPVProfileDefaultQuery = "SELECT ppvcount, posting_year, posting_month, plant_name, posting_date_doc, document_currency, incoterm_code, incoterm_desc, sum_invoicespend_eur, sum_invoicespend_usd, sum_totalppv_eur, sum_totalppv_usd, ssetransaction, document_category, variance, totalnvoicepricequartile, standardcostquartile, document_currency, commoditylevel1, vendor_freq_quantityquartile, vendor_freq_posquartile, material_freq_quantityquartile, material_freq_posquartile, gss_status,'' as material_code, '' as vendor_no, '' as material_spec_ref FROM "; 
	static String emeaPPVProfileDefaultQuery = "SELECT invoicecount, posting_year, posting_month, plant_name, posting_date_doc, document_currency, ssetransaction,  category, subcategory, transaction_type, sum_invoicespend_actual, sum_invoicespend_wahr, sum_invoicespend_bp, totalppv_fcur_actualrate, totalppv_fcur_wahr, totalppv_fcur_bp, fcur, variance, qty   FROM "; 
	static String emeaPPVProfileFilterQuery = "SELECT count(*) as ppvcount, posting_year, posting_month, plant_name, posting_date_doc, invoice_currency as document_currency, payment_terms, payment_termsdesc, incoterm_code, incoterm_desc, sum(invoice_spend_eur_actualrate) as sum_invoicespend_eur, '' as sum_invoicespend_usd, sum(totalppvnew_eur_actualrate) as sum_totalppv_eur, '' as sum_totalppv_usd, '' as sumamount_base_currency, ssetransaction, document_category, variance, totalnvoicepricequartile, standardcostquartile, ppv2quartile, ppv2percentagequartile, material_type, purchase_currency, purchasing_uom, stocking_uom, '' as commoditylevel1, vendor_freq_quantityquartile, vendor_freq_posquartile, material_freq_quantityquartile, material_freq_posquartile, gss_status, material_code, vendor_no, material_spec_ref, standard_costperunit_doc_currency FROM ";
	static String groupByClausePPVProfileFilter =" group by  posting_year, posting_month, plant_name, posting_date_doc, invoice_currency, payment_terms, payment_termsdesc, incoterm_code, incoterm_desc, ssetransaction, document_category, variance, totalnvoicepricequartile, standardcostquartile, ppv2quartile, ppv2percentagequartile, material_type, purchase_currency, purchasing_uom, stocking_uom, vendor_freq_quantityquartile, vendor_freq_posquartile, material_freq_quantityquartile, material_freq_posquartile, gss_status, material_code, vendor_no, material_spec_ref, standard_costperunit_doc_currency";
	
	static String emeaPPVProfileL2Query = "SELECT invoice_number, invoice_linenumber, posting_year, posting_month, plant_name, purchase_doc_no, posting_date_doc, gl_account, material_code, material_code_pred, material_desc, material_desc_pred, material_spec_ref, vendor_no, secondary_vendor_no, vendor_name, secondary_vendor_name, vendor_country, vendor_city, secondary_vendor_country, secondary_vendor_city, distributor_no, distributor_name, distributor_country, distributor_city, secondary_distributor_no, secondary_distributor_name, secondary_distributor_country, secondary_distributor_city, manufacturer_no, manufacturer_name, manufacturer_country, manufacturer_city, secondary_manufacturer_no, secondary_manufacturer_name, secondary_manufacturer_country, secondary_manufacturer_city, commoditylevel1, commoditylevel2, purchasing_uom, stocking_uom, conversion_factor, inv_quantity, receipt_quantity, moq, ioq, incoterm_code, incoterm_desc, incoterm_location_country, incoterm_location_city, payment_terms, payment_termsdesc, payment_days, vmi_flag, leadtime, purchase_currency, invoice_currency, price_validity_start_date, price_validity_end_date, amount_doc_currency, amount_base_currency, amount_eur_actualrate, amount_eur_15wahr_rate, pounitprice_doc_currency, pounitprice_eur_converted_actualrate, pounitprice_eur_converted_15wahr, totalpo_amt_doc_currency, totalpo_amt_eur_actualrate, totalpo_amt_eur_15wahr, purchase_price_uom, invoice_price_doc_currency, invoice_spend_base_currency, invoice_price_base_currency	, invoice_spend_eur_actualrate, invoice_price_eur_actual_rate, invoice_spend_eur_15wahr, invoice_price_eur_15wahr, standard_costperunit_base_currency, standard_costperunit_eur_actualrate, standard_costperunit_eur_15wahr, total_amount_at_standard_cost_base_currency, total_amount_at_standard_cost_eur_actualrate, total_amount_at_standard_cost_eur_15wahr, ppv2_amount_doc_currency, ppv2_amount_eur_actualrate, ppv2_amount_eur_15wahr, ppv1_amount_doc_currency, ppv1_amount_eur_actualrate, ppv1_amount_eur_15wahr, totalppv_doc_currency, totalppv_eur_actualrate, totalppv_eur_15wahr, ssetransaction, actual_exchange_rate, weighted_average_hedge_rate_15wahr, invoice_price_base_currency, sse_price, document_category, document_category_desc, variance, totalnvoicepricequartile, standardcostquartile, ppv2quartile, ppv2absquartile, ppv2percentage, ppv2percentagequartile, material_type, receipt_done, vendor_freq_quantityquartile, vendor_freq_posquartile, material_freq_quantityquartile, material_freq_posquartile, gss_status, sap_plantcode, totalppvnew_eur_actualrate, standard_costperunit_doc_currency FROM ";
	static String emeaPPVProfilegroupByClauseFilter = " group by invoice_number, invoice_linenumber, posting_year, posting_month, plant_name, purchase_doc_no, posting_date_doc, gl_account, material_code, material_code_pred, material_desc, material_desc_pred, material_spec_ref, vendor_no, secondary_vendor_no, vendor_name, secondary_vendor_name, vendor_country, vendor_city, secondary_vendor_country, secondary_vendor_city, distributor_no, distributor_name, distributor_country, distributor_city, secondary_distributor_no, secondary_distributor_name, secondary_distributor_country, secondary_distributor_city, manufacturer_no, manufacturer_name, manufacturer_country, manufacturer_city, secondary_manufacturer_no, secondary_manufacturer_name, secondary_manufacturer_country, secondary_manufacturer_city, commoditylevel1, commoditylevel2, purchasing_uom, stocking_uom, conversion_factor, inv_quantity, receipt_quantity, moq, ioq, incoterm_code, incoterm_desc, incoterm_location_country, incoterm_location_city, payment_terms, payment_termsdesc, payment_days, vmi_flag, leadtime, purchase_currency, invoice_currency, price_validity_start_date, price_validity_end_date, amount_doc_currency, amount_base_currency, amount_eur_actualrate, amount_eur_15wahr_rate, pounitprice_doc_currency, pounitprice_eur_converted_actualrate, pounitprice_eur_converted_15wahr, totalpo_amt_doc_currency, totalpo_amt_eur_actualrate, totalpo_amt_eur_15wahr, purchase_price_uom, invoice_price_doc_currency, invoice_spend_base_currency, invoice_price_base_currency	, invoice_spend_eur_actualrate, invoice_price_eur_actual_rate, invoice_spend_eur_15wahr, invoice_price_eur_15wahr, standard_costperunit_base_currency, standard_costperunit_eur_actualrate, standard_costperunit_eur_15wahr, total_amount_at_standard_cost_base_currency, total_amount_at_standard_cost_eur_actualrate, total_amount_at_standard_cost_eur_15wahr, ppv2_amount_doc_currency, ppv2_amount_eur_actualrate, ppv2_amount_eur_15wahr, ppv1_amount_doc_currency, ppv1_amount_eur_actualrate, ppv1_amount_eur_15wahr, totalppv_doc_currency, totalppv_eur_actualrate, totalppv_eur_15wahr, ssetransaction, actual_exchange_rate, weighted_average_hedge_rate_15wahr, invoice_price_base_currency, sse_price, document_category, document_category_desc, variance, totalnvoicepricequartile, standardcostquartile, ppv2quartile, ppv2absquartile, ppv2percentage, ppv2percentagequartile, material_type, receipt_done, vendor_freq_quantityquartile, vendor_freq_posquartile, material_freq_quantityquartile, material_freq_posquartile, gss_status, sap_plantcode, totalppvnew_eur_actualrate, standard_costperunit_doc_currency";
	
	static String emeaVarianceAnalysisProfileL2Query = "SELECT sapplantcode, plantdesc, sse_plant, po_number, po_line_number, vendor_number, vendor_name, po_creation_date, po_req_date, po_currency, po_exchangerate, company_code, po_material_number, sse_material_number, po_material_desc, po_order_qty, po_uom, po_exp_cost, sse_price, price_variance, price_variance_percentage, po_total_netvalue, po_inco_term, inco_term_desc, po_lead_time, sse_contract_found, po_material_group, material_group_desc, match_category, compliant_indicator, sse_match_found, po_item_type, sse_moq, sse_currency, sse_incoterm, sse_leadtime, currency_mismatch_ind, lead_time_mismatch_ind, moq_mismatch_ind, unit_mismatch_ind, incoterm_mismatch_ind, gss_status, sum(total_netvalue_variance) as totalnetvaluevariance FROM ";
	static String emeaVarianceAnalysisProfilegroupByClauseFilter = " group by sapplantcode, plantdesc, sse_plant, po_number, po_line_number, vendor_number, vendor_name, po_creation_date, po_req_date, po_currency, po_exchangerate, company_code, po_material_number, sse_material_number, po_material_desc, po_order_qty, po_uom, po_exp_cost, sse_price, price_variance, price_variance_percentage, po_total_netvalue, po_inco_term, inco_term_desc, po_lead_time, sse_contract_found, po_material_group, material_group_desc, match_category, compliant_indicator, sse_match_found, po_item_type, sse_moq, sse_currency, sse_incoterm, sse_leadtime, currency_mismatch_ind, lead_time_mismatch_ind, moq_mismatch_ind, unit_mismatch_ind, incoterm_mismatch_ind, gss_status";
	
	static String emeaPPVOverviewQuery = "SELECT * FROM ";
	

	Connection con = null;
	Statement stmt = null;
	
	String emea_db = null;
	
	public EMEAProfileDao(){
		emea_db = E2emfAppUtil.getAppProperty(E2emfConstants.emeaDB);
	}
	
	public ArrayList<EMEAView> getEMEAPOProfileViewDetails(Object... obj) throws AppException {
		String source = null;
		String materialid = null;
		String vendorid = null;
		ArrayList<EMEAView> emeaPOProfileViewList = new ArrayList<EMEAView>();
		StringBuilder queryToExecute = null;
		ResultSet rs = null;
		try {
			if (obj != null && obj.length >= 1) {
				source = (String) obj[0];
				materialid = (String) obj[1];
				//materialid="'908580','906289'"; //For Testing
				vendorid = (String) obj[2];
				Log.Info("Fetching the DefaultView for Slob Profile ... for Source " + source);
				String tableName = "EMEA_AG_PO_PROFILE";
				String filterTableName = "EMEA_L2_PO";
				String whereClause = "";
				
				con = dbUtil.getHiveConnection(E2emfConstants.emeaDB);
				Log.Info("Using SLOB HIVE Connection...");

				//queryToExecute = new StringBuilder(slbDefaultQuery).append(slob_db).append(".").append(tableName);
				if(materialid == null && vendorid== null)
					queryToExecute = new StringBuilder(emeaPOProfileDefaultQuery).append(tableName);
				else{

					if(materialid!= null && vendorid!=null)
						whereClause = " where po_material_number in ('"+materialid+"') and vendor_number in ('"+vendorid+"')"; 
					if(materialid == null && vendorid!=null)		
						whereClause = " where vendor_number in ('"+vendorid+"')";
					if(materialid != null && vendorid==null)	
						whereClause = " where po_material_number in ('"+materialid+"')";
					
					queryToExecute = new StringBuilder(emeaPOProfileFilterQuery).append(filterTableName).append(whereClause).append(groupByClausePOProfileFilter);
					
					
				}
				Log.Info("EMEA PO Profile Default View Query to Execute :"+queryToExecute.toString());

				stmt = con.createStatement();

				rs = stmt.executeQuery(queryToExecute.toString());
				if (rs != null) {
					
					while (rs.next()) {
						EMEAView emeaView = new EMEAView();
						emeaView.setPoCount(rs.getString("pocount"));
						emeaView.setCurrency(rs.getString("currency"));
						emeaView.setSapPC(rs.getString("sapplantcode"));
						emeaView.setPlantDesc(rs.getString("plantdesc"));
						emeaView.setCompanyCode(rs.getString("companycode"));
						emeaView.setCompanyCodeDesc(rs.getString("companycodedesc"));
						emeaView.setPoReqDate(rs.getString("poreqdate"));
						emeaView.setSseCI(rs.getString("ssecontractindicator"));
						emeaView.setcI(rs.getString("compliantindicator"));
						String pVP = rs.getString("pricevariancepercentage");
						if(pVP != null){
							double doubleValueToChange = Double.valueOf(pVP).doubleValue();
							emeaView.setpVP(roundDigits(doubleValueToChange));
						}else{
								emeaView.setpVP(pVP);
						}
						emeaView.setPriceVariance(rs.getString("pricevariance"));
						emeaView.setNetValueVP(rs.getString("netvaluevariancepercentage"));
						emeaView.setSumNetValue(rs.getString("sumnetvalue"));
						emeaView.setSseIMI(rs.getString("sseitemmatchindicator"));
//						emeaView.setpVPQuartile(rs.getString("pricevariancepercentagequartile"));
						emeaView.setCurrencyMisMatch(rs.getString("currencymismatch"));
						emeaView.setLeadTimeMisMatch(rs.getString("leadtimemismatch"));
						emeaView.setMoqMisMatch(rs.getString("moqmismatch"));
						emeaView.setUnitMisMatch(rs.getString("unitmismatch"));
						emeaView.setIncoTermMismatch(rs.getString("incotermmismatch"));
						emeaView.setItemType(rs.getString("itemtype"));
						emeaView.setSubCategory(rs.getString("subcategory"));
						emeaView.setCategory(rs.getString("category"));
						String sumPriceVariance = rs.getString("sumpricevariance");
						if(sumPriceVariance != null){
							double doubleValueToChange = Double.valueOf(sumPriceVariance).doubleValue();
							emeaView.setSumPriceVariance(roundDigits(doubleValueToChange));
						}else{
								emeaView.setSumPriceVariance(sumPriceVariance);
						}
//						emeaView.setPoCostQuartile(rs.getString("pocostquartile"));
//						emeaView.setNetvarianceQuartile(rs.getString("netvariancequartile"));
//						emeaView.setNetvalueQuartile(rs.getString("netvaluequartile"));
						emeaView.setGssStatus(rs.getString("gss_status"));
						emeaView.setVendorMismatch(rs.getString("vendormismatch"));
						emeaView.setOtherMismatchCount(rs.getInt("other_mismatch_count"));
						
						emeaPOProfileViewList.add(emeaView);						
					}
				}
			}
		}catch(SQLException sql){
			Log.Error("SQLException while Fetching Master Details for EMEA PO Profile." + sql.getStackTrace());
			Log.Error(sql);
			throw new AppException(sql.getMessage(),"Error while processing the request.", 1 ,sql.getCause(), true);
		}catch(Exception e){
			Log.Error("Exception fetching the Details for EMEA PO Profile. \n " + e.getMessage());
			Log.Error(e);
			e.printStackTrace();
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
		}finally{
			if(con != null)
				try {
					con.close();
					//stmt.close();
					//con = null;
					//stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
				
		}
		return emeaPOProfileViewList;
	}
	
	public ArrayList<EMEAView> getEMEAPOProfileL2ViewDetails(Object... obj) throws AppException {
		ArrayList<EMEAView> emeaPOProfileL2ViewList = new ArrayList<EMEAView>();
		StringBuilder queryToExecute = null;
		ResultSet rs = null;
		String plant = null;
		String companycode= null;
		String currency= null;
		String ssecontract= null;
		String compliant= null;
		String sseitemmatch= null;
		//String pricevariance= null;
		String netvaluevariance= null;
		String poreqdate= null;
		String netvaluequartile = null;
		String pocostquartile = null;
		String netvariancequartile = null;
		String itemtype = null;
		String currencymismatch = null;
		String leadtimemismatch = null;
		String moqmismatch = null;
		String unitmismatch = null;
		String incotermmismatch = null;
		String variance = null;
		String gssstatus = null;
		String pricevariancepercentage = null;
		//String netvalue= null;
		
		StringBuilder where = null;
		try {
				if (obj != null && obj.length >= 1) {
					plant = (String) obj[0];
					companycode = (String) obj[1];
					currency = (String) obj[2];
					ssecontract = (String) obj[3];
					compliant = (String) obj[4];
					sseitemmatch = (String) obj[5];
					//pricevariance = (String) obj[6];
					netvaluevariance = (String) obj[7];
					poreqdate = (String) obj[8];
					netvaluequartile = (String) obj[9];
					pocostquartile = (String) obj[10];
					netvariancequartile = (String) obj[11];
					itemtype = (String) obj[12];
					currencymismatch = (String) obj[13];
					leadtimemismatch = (String) obj[14];
					moqmismatch = (String) obj[15];
					unitmismatch = (String) obj[16];
					incotermmismatch = (String) obj[17];
					variance = (String) obj[18];
					gssstatus = (String) obj[19];
					pricevariancepercentage = (String) obj[20];
					//netvalue = (String) obj[21];
					Log.Info("Fetching the L2 View for EMEA PO Profile ...");
					String tableName = "EMEA_L2_PO";
					//if(poNumber!= null){
					//	whereClause = " where po_number in ('" + poNumber + "')";
					//}
					where = new StringBuilder(" where company_code = '").append(companycode)
							.append("'");
					
					if (plant == null || plant.equalsIgnoreCase(null) || plant.equalsIgnoreCase("NULL")) {
						where.append(" and (sapplantcode ='' or sapplantcode is null)");
					} else { 
						where.append(" and sapplantcode = '").append(plant).append("'");
					}
					
					if (currency == null || currency.equalsIgnoreCase(null) || currency.equalsIgnoreCase("NULL")) {
						where.append(" and (po_currency ='' or po_currency is null)");
					} else { 
						where.append(" and po_currency = '").append(currency).append("'");
					}
					
					if (ssecontract == null || ssecontract.equalsIgnoreCase(null) || ssecontract.equalsIgnoreCase("NULL")) {
						where.append(" and (sse_contract_found ='' or sse_contract_found is null)");
					} else { 
						where.append(" and sse_contract_found = '").append(ssecontract).append("'");
					}
					
					if (compliant == null || compliant.equalsIgnoreCase(null) || compliant.equalsIgnoreCase("NULL")) {
						where.append(" and (compliant_indicator ='' or compliant_indicator is null)");
					} else { 
						where.append(" and compliant_indicator = '").append(compliant).append("'");
					}
					
					if (sseitemmatch == null || sseitemmatch.equalsIgnoreCase(null) || sseitemmatch.equalsIgnoreCase("NULL")) {
						where.append(" and (sse_match_found ='' or sse_match_found is null)");
					} else { 
						where.append(" and sse_match_found = '").append(sseitemmatch).append("'");
					}
					
					/*if (pricevariance == null || pricevariance.equalsIgnoreCase(null) || pricevariance.equalsIgnoreCase("NULL")) {
						where.append(" and (price_variance = '' or price_variance is null)");
					} else { 
						where.append(" and price_variance = '").append(pricevariance).append("'");
					}*/
					
					if (netvaluevariance == null || netvaluevariance.equalsIgnoreCase(null) || netvaluevariance.equalsIgnoreCase("NULL")) {
						where.append(" and (total_netvalue_variance is null)");
					} else { 
						where.append(" and total_netvalue_variance = ").append(netvaluevariance);
					}
					
					if (poreqdate == null || poreqdate.equalsIgnoreCase(null) || poreqdate.equalsIgnoreCase("NULL")) {
						where.append(" and (po_req_date ='' or po_req_date is null)");
					} else { 
						where.append(" and po_req_date = '").append(poreqdate).append("'");
					}
					
					if (netvaluequartile == null || netvaluequartile.equalsIgnoreCase(null) || netvaluequartile.equalsIgnoreCase("NULL")) {
						where.append(" and (netvaluequartile ='' or netvaluequartile is null)");
					} else { 
						where.append(" and netvaluequartile = '").append(netvaluequartile).append("'");
					}
					
					if (pocostquartile == null || pocostquartile.equalsIgnoreCase(null) || pocostquartile.equalsIgnoreCase("NULL")) {
						where.append(" and (pocostquartile ='' or pocostquartile is null)");
					} else { 
						where.append(" and pocostquartile = '").append(pocostquartile).append("'");
					}
					
					if (netvariancequartile == null || netvariancequartile.equalsIgnoreCase(null) || netvariancequartile.equalsIgnoreCase("NULL")) {
						where.append(" and (netvariancequartile ='' or netvariancequartile is null)");
					} else { 
						where.append(" and netvariancequartile = '").append(netvariancequartile).append("'");
					}
					
					if (itemtype == null || itemtype.equalsIgnoreCase(null) || itemtype.equalsIgnoreCase("NULL")) {
						where.append(" and (po_item_type ='' or po_item_type is null)");
					} else { 
						where.append(" and po_item_type = '").append(itemtype).append("'");
					}
					
					if (currencymismatch == null || currencymismatch.equalsIgnoreCase(null) || currencymismatch.equalsIgnoreCase("NULL")) {
						where.append(" and (currency_mismatch_ind ='' or currency_mismatch_ind is null)");
					} else { 
						where.append(" and currency_mismatch_ind = '").append(currencymismatch).append("'");
					}
					
					if (leadtimemismatch == null || leadtimemismatch.equalsIgnoreCase(null) || leadtimemismatch.equalsIgnoreCase("NULL")) {
						where.append(" and (lead_time_mismatch_ind ='' or lead_time_mismatch_ind is null)");
					} else { 
						where.append(" and lead_time_mismatch_ind = '").append(leadtimemismatch).append("'");
					}
					
					if (moqmismatch == null || moqmismatch.equalsIgnoreCase(null) || moqmismatch.equalsIgnoreCase("NULL")) {
						where.append(" and (moq_mismatch_ind ='' or moq_mismatch_ind is null)");
					} else { 
						where.append(" and moq_mismatch_ind = '").append(moqmismatch).append("'");
					}
					
					if (unitmismatch == null || unitmismatch.equalsIgnoreCase(null) || unitmismatch.equalsIgnoreCase("NULL")) {
						where.append(" and (unit_mismatch_ind ='' or unit_mismatch_ind is null)");
					} else { 
						where.append(" and unit_mismatch_ind = '").append(unitmismatch).append("'");
					}
					
					if (incotermmismatch == null || incotermmismatch.equalsIgnoreCase(null) || incotermmismatch.equalsIgnoreCase("NULL")) {
						where.append(" and (incoterm_mismatch_ind ='' or incoterm_mismatch_ind is null)");
					} else { 
						where.append(" and incoterm_mismatch_ind = '").append(incotermmismatch).append("'");
					}
					
					if (variance == null || variance.equalsIgnoreCase(null) || variance.equalsIgnoreCase("NULL")) {
						where.append(" and (favourable ='' or favourable is null)");
					} else { 
						where.append(" and favourable = '").append(variance).append("'");
					}
					
					if (gssstatus == null || gssstatus.equalsIgnoreCase(null) || gssstatus.equalsIgnoreCase("NULL")) {
						where.append(" and (gss_status ='' or gss_status is null)");
					} else { 
						where.append(" and gss_status = '").append(gssstatus).append("'");
					}
					
					if (pricevariancepercentage == null || pricevariancepercentage.equalsIgnoreCase(null) || pricevariancepercentage.equalsIgnoreCase("NULL")) {
						where.append(" and (price_variance_percentage is null)");
					} else { 
						where.append(" and price_variance_percentage = ").append(pricevariancepercentage);
					}
					
					/*if (netvalue == null || netvalue.equalsIgnoreCase(null) || netvalue.equalsIgnoreCase("NULL")) {
						where.append(" and (po_total_netvalue = NULL or po_total_netvalue is null)");
					} else { 
						where.append(" and po_total_netvalue = ").append(netvalue);
					}*/
					
					con = dbUtil.getHiveConnection(E2emfConstants.emeaDB);
					Log.Info("Using EMEA HIVE Connection...");
					
					//queryToExecute = new StringBuilder(emeaPOProfileDefaultQuery).append(emea_db).append(".").append(tableName);
					//queryToExecute = new StringBuilder(emeaPOProfileL2Query).append(tableName).append(where.toString()).append(emeaPOProfilegroupByClauseFilter);
					queryToExecute = new StringBuilder(emeaPOProfileL2Query).append(tableName).append(where.toString());
					Log.Info("EMEA PO Profile L2 View Query to Execute :"+queryToExecute.toString());
	
					stmt = con.createStatement();
	
					rs = stmt.executeQuery(queryToExecute.toString());
					if (rs != null) {
						
						while (rs.next()) {
							
							EMEAView emeaView = new EMEAView();
							emeaView.setSapPC(rs.getString("sapplantcode"));
							emeaView.setSsePlant(rs.getString("sse_plant"));
							emeaView.setPoNumber(rs.getString("po_number"));
							emeaView.setPoLineNumber(rs.getString("po_line_number"));
							emeaView.setVendorNumber(rs.getString("vendor_number"));
							emeaView.setVendorName(rs.getString("vendor_name"));
							emeaView.setPoCreationDate(rs.getString("po_creation_date"));
							emeaView.setPoReqDate(rs.getString("po_req_date"));
							emeaView.setPoCurrency(rs.getString("po_currency"));
							emeaView.setPoExchangeRate(rs.getString("po_exchangerate"));
							emeaView.setCompanyCode(rs.getString("company_code"));
							emeaView.setPoMaterialNumber(rs.getString("po_material_number"));
							emeaView.setSseMaterialNumber(rs.getString("sse_material_number"));
							emeaView.setPoMaterialDesc(rs.getString("po_material_desc"));
							String poOrderQty =rs.getString("po_order_qty");
							if(poOrderQty != null){
								double doubleValueToChange = Double.valueOf(poOrderQty).doubleValue();
								emeaView.setPoOrderQty(roundDigits(doubleValueToChange));
							}else{
									emeaView.setPoOrderQty(poOrderQty);
							}
							emeaView.setPoUom(rs.getString("po_uom"));
							
							String poExpCost =rs.getString("po_exp_cost");
							if(poExpCost != null){
								double doubleValueToChange = Double.valueOf(poExpCost).doubleValue();
								emeaView.setPoExpCost(roundDigits(doubleValueToChange));
							}else{
									emeaView.setPoExpCost(poExpCost);
							}
							
							String ssePrice =rs.getString("sse_price");
							if(ssePrice != null){
								double doubleValueToChange = Double.valueOf(ssePrice).doubleValue();
								emeaView.setSsePrice(roundDigits(doubleValueToChange));
							}else{
									emeaView.setSsePrice(ssePrice);
							}
							
							String priceVariance =rs.getString("price_variance");
							if(priceVariance != null){
								double doubleValueToChange = Double.valueOf(priceVariance).doubleValue();
								emeaView.setPriceVariance(roundDigits(doubleValueToChange));
							}else{
									emeaView.setPriceVariance(priceVariance);
							}
							
							String pVP =rs.getString("price_variance_percentage");
							if(pVP != null){
								double doubleValueToChange = Double.valueOf(pVP).doubleValue();
								emeaView.setpVP(roundDigits(doubleValueToChange));
							}else{
									emeaView.setpVP(pVP);
							}
							
							String poTotalNetValue =rs.getString("po_total_netvalue");
							if(poTotalNetValue != null){
								double doubleValueToChange = Double.valueOf(poTotalNetValue).doubleValue();
								emeaView.setPoTotalNetValue(roundDigits(doubleValueToChange));
							}else{
									emeaView.setPoTotalNetValue(poTotalNetValue);
							}
							
							emeaView.setPoIncoTerm(rs.getString("po_inco_term"));
							emeaView.setIncoTermDesc(rs.getString("inco_term_desc"));
							emeaView.setPoLeadTime(rs.getString("po_lead_time"));
							emeaView.setSseContractFound(rs.getString("sse_contract_found"));
							emeaView.setPoMaterialGroup(rs.getString("po_material_group"));
							emeaView.setMaterialGroupDesc(rs.getString("material_group_desc"));
							emeaView.setMatchCategory(rs.getString("match_category"));
							emeaView.setcI(rs.getString("compliant_indicator"));
							emeaView.setSseMatchFound(rs.getString("sse_match_found"));
							emeaView.setPoItemType(rs.getString("po_item_type"));
							emeaView.setSseMoq(rs.getString("sse_moq"));
							emeaView.setSseCurrency(rs.getString("sse_currency"));
							emeaView.setSseIncoTerm(rs.getString("sse_incoterm"));
							emeaView.setSseLeadTime(rs.getString("sse_leadtime"));
							emeaView.setCurrencyMI(rs.getString("currency_mismatch_ind"));
							emeaView.setLeadTimeMI(rs.getString("lead_time_mismatch_ind"));
							emeaView.setMoqMI(rs.getString("moq_mismatch_ind"));
							emeaView.setUnitMI(rs.getString("unit_mismatch_ind"));
							emeaView.setIncoTermMI(rs.getString("incoterm_mismatch_ind"));
							emeaView.setGssStatus(rs.getString("gss_status"));
							
							String totalNetvalueVariance =rs.getString("total_netvalue_variance");
							if(totalNetvalueVariance != null){
								double doubleValueToChange = Double.valueOf(totalNetvalueVariance).doubleValue();
								emeaView.setTotalNetvalueVariance(roundDigits(doubleValueToChange));
							}else{
									emeaView.setTotalNetvalueVariance(totalNetvalueVariance);
							}

							emeaPOProfileL2ViewList.add(emeaView);						
						}
					}	
				}

		}catch(SQLException sql){
			Log.Error("SQLException while Fetching Master Details for EMEA PO Profile L2 View." + sql.getStackTrace());
			Log.Error(sql);
			throw new AppException(sql.getMessage(),"Error while processing the request.", 1 ,sql.getCause(), true);
		}catch(Exception e){
			Log.Error("Exception fetching the Details for EMEA PO Profile L2 View. \n " + e.getMessage());
			Log.Error(e);
			e.printStackTrace();
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
		}finally{
			if(con != null)
				try {
					con.close();
					//stmt.close();
					//con = null;
					//stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
				
		}
		return emeaPOProfileL2ViewList;
	}
	
	public ArrayList<EMEAView> getEMEAQuotesProfileViewDetails() throws AppException {
		ArrayList<EMEAView> emeaQuotesProfileViewList = new ArrayList<EMEAView>();
		StringBuilder queryToExecute = null;
		ResultSet rs = null;
		try {
				Log.Info("Fetching the Quotes Profile Default View for EMEA Quotes Profile ...");
				String tableName = "EMEA_AG_QT_PROFILE";
						
				con = dbUtil.getHiveConnection(E2emfConstants.emeaDB);
				Log.Info("Using EMEA HIVE Connection...");

				//queryToExecute = new StringBuilder(emeaPOProfileDefaultQuery).append(emea_db).append(".").append(tableName);
				queryToExecute = new StringBuilder(emeaQuotesProfileDefaultQuery).append(tableName);
				Log.Info("EMEA Quotes Profile Default View Query to Execute :"+queryToExecute.toString());

				stmt = con.createStatement();

				rs = stmt.executeQuery(queryToExecute.toString());
				if (rs != null) {
					
					while (rs.next()) {
						EMEAView emeaQuotesProfileView = new EMEAView();
						emeaQuotesProfileView.setQtCount(rs.getString("qtcount"));
						emeaQuotesProfileView.setCurrency(rs.getString("currency"));
						emeaQuotesProfileView.setSapPC(rs.getString("sapplantcode"));
						emeaQuotesProfileView.setPlantDesc(rs.getString("plantdesc"));
						emeaQuotesProfileView.setContractStartDate(rs.getString("contractstartdate"));
						emeaQuotesProfileView.setQtYear(rs.getString("qt_year"));
						emeaQuotesProfileView.setSseCI(rs.getString("ssecontractindicator"));
						emeaQuotesProfileView.setcI(rs.getString("compliantindicator"));
						emeaQuotesProfileView.setpVP(rs.getString("pricevariancepercentage"));
						emeaQuotesProfileView.setPriceVariance(rs.getString("pricevariance"));
						emeaQuotesProfileView.setNetValueVP(rs.getString("netvaluevariancepercentage"));
						emeaQuotesProfileView.setSumNetValue(rs.getString("sumnetvalue"));
						emeaQuotesProfileView.setSseIMI(rs.getString("sseitemmatchindicator"));
						emeaQuotesProfileView.setCurrencyMisMatch(rs.getString("currencymismatch"));
						emeaQuotesProfileView.setLeadTimeMisMatch(rs.getString("leadtimemismatch"));
						emeaQuotesProfileView.setMoqMisMatch(rs.getString("moqmismatch"));
						emeaQuotesProfileView.setUnitMisMatch(rs.getString("unitmismatch"));
						emeaQuotesProfileView.setIncoTermMismatch(rs.getString("incotermmismatch"));
						emeaQuotesProfileView.setCategory(rs.getString("category"));
						emeaQuotesProfileView.setSubCategory(rs.getString("subcategory"));
						emeaQuotesProfileView.setEffectiveContract(rs.getString("effectivecontract"));
						emeaQuotesProfileView.setDirection(rs.getString("direction"));
						emeaQuotesProfileView.setVendorMismatch(rs.getString("vendormismatch"));
						emeaQuotesProfileView.setOtherMismatchCount(rs.getInt("other_mismatch_count"));
			//			emeaQuotesProfileView.setpVPQuartile(rs.getString("pricevariancepercentagequartile"));
						//emeaQuotesProfileView.setSumPriceVariance(rs.getString("sumpricevariance"));
						
						emeaQuotesProfileViewList.add(emeaQuotesProfileView);						
					}
				}	

		}catch(SQLException sql){
			Log.Error("SQLException while Fetching Master Details for EMEA Quotes Profile." + sql.getStackTrace());
			Log.Error(sql);
			throw new AppException(sql.getMessage(),"Error while processing the request.", 1 ,sql.getCause(), true);
		}catch(Exception e){
			Log.Error("Exception fetching the Details for EMEA Quotes Profile. \n " + e.getMessage());
			Log.Error(e);
			e.printStackTrace();
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
		}finally{
			if(con != null)
				try {
					con.close();
					//stmt.close();
					//con = null;
					//stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
				
		}
		return emeaQuotesProfileViewList;
	}
	
	public ArrayList<EMEAView> getEMEAQuotesProfileL2ViewDetails(Object... obj) throws AppException {
		ArrayList<EMEAView> emeaQuotesProfileL2ViewList = new ArrayList<EMEAView>();
		StringBuilder queryToExecute = null;
		ResultSet rs = null;
		String plantcode = null;
		String currency= null;
		String ssecontract= null;
		String compliant= null;
		String sseitemmatch= null;
		//String pricevariance= null;
		//String netvaluevariance= null;
		String contractstartdate= null;
		String pricevariancepercentage = null;
		String currencymismatch = null;
		String leadtimemismatch = null;
		String moqmismatch = null;
		String unitmismatch = null;
		String incotermmismatch = null;
		//String netvalue= null;
		
		StringBuilder where = null;
		try {
				if (obj != null && obj.length >= 1) {
					plantcode = (String) obj[0];
					currency = (String) obj[1];
					ssecontract = (String) obj[2];
					compliant = (String) obj[3];
					sseitemmatch = (String) obj[4];
					//pricevariance = (String) obj[5];
					//netvaluevariance = (String) obj[6];
					contractstartdate = (String) obj[7];
					pricevariancepercentage = (String) obj[8];
					currencymismatch = (String) obj[9];
					leadtimemismatch = (String) obj[10];
					moqmismatch = (String) obj[11];
					unitmismatch = (String) obj[12];
					incotermmismatch = (String) obj[13];
					//netvalue = (String) obj[14];
					Log.Info("Fetching the L2 View for EMEA Quotes Profile ...");
					String tableName = "EMEA_L2_QT";
					//if(poNumber!= null){
					//	whereClause = " where po_number in ('" + poNumber + "')";
					//}
					where = new StringBuilder(" where sap_plant = '").append(plantcode)
							.append("'");
					
					if (currency == null || currency.equalsIgnoreCase(null) || currency.equalsIgnoreCase("NULL")) {
						where.append(" and (qt_currency ='' or qt_currency is null)");
					} else { 
						where.append(" and qt_currency = '").append(currency).append("'");
					}
					
					if (ssecontract == null || ssecontract.equalsIgnoreCase(null) || ssecontract.equalsIgnoreCase("NULL")) {
						where.append(" and (sse_contract_found ='' or sse_contract_found is null)");
					} else { 
						where.append(" and sse_contract_found = '").append(ssecontract).append("'");
					}
					
					if (compliant == null || compliant.equalsIgnoreCase(null) || compliant.equalsIgnoreCase("NULL")) {
						where.append(" and (compliant_indicator ='' or compliant_indicator is null)");
					} else { 
						where.append(" and compliant_indicator = '").append(compliant).append("'");
					}
					
					if (sseitemmatch == null || sseitemmatch.equalsIgnoreCase(null) || sseitemmatch.equalsIgnoreCase("NULL")) {
						where.append(" and (sse_match_found ='' or sse_match_found is null)");
					} else { 
						where.append(" and sse_match_found = '").append(sseitemmatch).append("'");
					}
					
					/*if (pricevariance == null || pricevariance.equalsIgnoreCase(null) || pricevariance.equalsIgnoreCase("NULL")) {
						where.append(" and (price_variance = NULL or price_variance is null)");
					} else { 
						where.append(" and price_variance = ").append(pricevariance);
					}*/
					
					/*if (netvaluevariance == null || netvaluevariance.equalsIgnoreCase(null) || netvaluevariance.equalsIgnoreCase("NULL")) {
						where.append(" and (po_currency ='' or po_currency is null)");
					} else { 
						where.append(" and po_currency = '").append(netvaluevariance).append("'");
					}*/
					
					if (contractstartdate == null || contractstartdate.equalsIgnoreCase(null) || contractstartdate.equalsIgnoreCase("NULL")) {
						where.append(" and (qt_contract_start_date ='' or qt_contract_start_date is null)");
					} else { 
						where.append(" and qt_contract_start_date = '").append(contractstartdate).append("'");
					}
					
					if (pricevariancepercentage == null || pricevariancepercentage.equalsIgnoreCase(null) || pricevariancepercentage.equalsIgnoreCase("NULL") || pricevariancepercentage.equalsIgnoreCase("0")) {
						where.append(" and (price_variance_percentage = '0' or price_variance_percentage is null)");
					} else { 
						where.append(" and price_variance_percentage = '").append(pricevariancepercentage).append("'");
					}
					
					if (currencymismatch == null || currencymismatch.equalsIgnoreCase(null) || currencymismatch.equalsIgnoreCase("NULL")) {
						where.append(" and (currency_mismatch_ind ='' or currency_mismatch_ind is null)");
					} else { 
						where.append(" and currency_mismatch_ind = '").append(currencymismatch).append("'");
					}
					
					if (leadtimemismatch == null || leadtimemismatch.equalsIgnoreCase(null) || leadtimemismatch.equalsIgnoreCase("NULL")) {
						where.append(" and (lead_time_mismatch_ind ='' or lead_time_mismatch_ind is null)");
					} else { 
						where.append(" and lead_time_mismatch_ind = '").append(leadtimemismatch).append("'");
					}
					
					if (moqmismatch == null || moqmismatch.equalsIgnoreCase(null) || moqmismatch.equalsIgnoreCase("NULL")) {
						where.append(" and (moq_mismatch_ind ='' or moq_mismatch_ind is null)");
					} else { 
						where.append(" and moq_mismatch_ind = '").append(moqmismatch).append("'");
					}
					
					if (unitmismatch == null || unitmismatch.equalsIgnoreCase(null) || unitmismatch.equalsIgnoreCase("NULL")) {
						where.append(" and (unit_mismatch_ind ='' or unit_mismatch_ind is null)");
					} else { 
						where.append(" and unit_mismatch_ind = '").append(unitmismatch).append("'");
					}
					
					if (incotermmismatch == null || incotermmismatch.equalsIgnoreCase(null) || incotermmismatch.equalsIgnoreCase("NULL")) {
						where.append(" and (incoterm_mismatch_ind ='' or incoterm_mismatch_ind is null)");
					} else { 
						where.append(" and incoterm_mismatch_ind = '").append(incotermmismatch).append("'");
					}
					
					/*if (netvalue == null || netvalue.equalsIgnoreCase(null) || netvalue.equalsIgnoreCase("NULL")) {
						where.append(" and (po_total_netvalue = NULL or po_total_netvalue is null)");
					} else { 
						where.append(" and po_total_netvalue = ").append(netvalue);
					}*/
					
					con = dbUtil.getHiveConnection(E2emfConstants.emeaDB);
					Log.Info("Using EMEA HIVE Connection...");
					
					//queryToExecute = new StringBuilder(emeaPOProfileDefaultQuery).append(emea_db).append(".").append(tableName);
					//queryToExecute = new StringBuilder(emeaQuotesProfileL2Query).append(tableName).append(where.toString()).append(emeaQuotesProfilegroupByClauseFilter);
					queryToExecute = new StringBuilder(emeaQuotesProfileL2Query).append(tableName).append(where.toString());
					Log.Info("EMEA Quotes Profile L2 View Query to Execute :"+queryToExecute.toString());
	
					stmt = con.createStatement();
	
					rs = stmt.executeQuery(queryToExecute.toString());
					if (rs != null) {
						
						while (rs.next()) {
							
							EMEAView emeaView = new EMEAView();
							emeaView.setSapPlant(rs.getString("sap_plant"));
							emeaView.setQtPlant(rs.getString("qt_plant"));
							emeaView.setQtVendorNumber(rs.getString("qt_vendor_number"));
							emeaView.setQtVendorName(rs.getString("qt_vendor_name"));
							emeaView.setQtCurrency(rs.getString("qt_currency"));
							emeaView.setQtMaterialNumber(rs.getString("qt_material_number"));
							emeaView.setQtMaterialDesc(rs.getString("qt_material_desc"));
							emeaView.setQtUom(rs.getString("qt_uom"));
							emeaView.setQtIncoTerm(rs.getString("qt_inco_term"));
							emeaView.setQtLeadTime(rs.getString("qt_lead_time"));
							String quotesPrice = rs.getString("qt_price");
							if(quotesPrice != null){
								double doubleValueToChange = Double.valueOf(quotesPrice).doubleValue();
								emeaView.setQtPrice(roundDigits(doubleValueToChange));
							}else{
									emeaView.setQtPrice(quotesPrice);
							}
							emeaView.setQtContractStartDate(rs.getString("qt_contract_start_date"));
							emeaView.setSsePlant(rs.getString("sse_plant"));
							emeaView.setSseMaterialNumber(rs.getString("sse_material_number"));
							String ssePrice =rs.getString("sse_price");
							if(ssePrice != null){
								double doubleValueToChange = Double.valueOf(ssePrice).doubleValue();
								emeaView.setSsePrice(roundDigits(doubleValueToChange));
							}else{
									emeaView.setSsePrice(ssePrice);
							}
							emeaView.setSseMoq(rs.getString("sse_moq"));
							emeaView.setSseCurrency(rs.getString("sse_currency"));
							emeaView.setSseIncoTerm(rs.getString("sse_incoterm"));
							emeaView.setSseLeadTime(rs.getString("sse_leadtime"));
							String priceVariance = rs.getString("price_variance");
							if(priceVariance != null && !priceVariance.equalsIgnoreCase("N/A")){
								double doubleValueToChange = Double.valueOf(priceVariance).doubleValue();
								emeaView.setPriceVariance(roundDigits(doubleValueToChange));
							}else{
									emeaView.setPriceVariance(priceVariance);
							}
							String pVP = rs.getString("price_variance_percentage");
							if(pVP != null && !pVP.equalsIgnoreCase("N/A")){
								double doubleValueToChange = Double.valueOf(pVP).doubleValue();
								emeaView.setpVP(roundDigits(doubleValueToChange));
							}else{
									emeaView.setpVP(pVP);
							}
							emeaView.setMatchCategory(rs.getString("match_category"));
							emeaView.setcI(rs.getString("compliant_indicator"));
							emeaView.setSseContractFound(rs.getString("sse_contract_found"));
							emeaView.setSseMatchFound(rs.getString("sse_match_found"));
							emeaView.setCurrencyMI(rs.getString("currency_mismatch_ind"));
							emeaView.setLeadTimeMI(rs.getString("lead_time_mismatch_ind"));
							emeaView.setMoqMI(rs.getString("moq_mismatch_ind"));
							emeaView.setUnitMI(rs.getString("unit_mismatch_ind"));
							emeaView.setIncoTermMI(rs.getString("incoterm_mismatch_ind"));
							
							emeaQuotesProfileL2ViewList.add(emeaView);						
						}
					}	
				}

		}catch(SQLException sql){
			Log.Error("SQLException while Fetching Master Details for EMEA Quotes Profile L2 View." + sql.getStackTrace());
			Log.Error(sql);
			throw new AppException(sql.getMessage(),"Error while processing the request.", 1 ,sql.getCause(), true);
		}catch(Exception e){
			Log.Error("Exception fetching the Details for EMEA Quotes Profile L2 View. \n " + e.getMessage());
			Log.Error(e);
			e.printStackTrace();
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
		}finally{
			if(con != null)
				try {
					con.close();
					//stmt.close();
					//con = null;
					//stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
				
		}
		return emeaQuotesProfileL2ViewList;
	}
	
	
	public E2emfView getEMEADataPagination(Object... obj) throws AppException {
		DataTableView datatableView = new DataTableView();
		String queryToExecute = null;
		String filterQuery = "";
		String columnConcat = "";
		ResultSet rs = null;
		
		String table = null;
		String columns = null;
		String searchValue = null;
		String filter = null;
		Integer orderBy = null;
		String orderType = null;
		String pagenumber = null;
		String pagesize = null;
		String groupBy = null;
		
		try {
				if (obj != null && obj.length >= 1) {
					table = (String) obj[0];
					columns = (String) obj[1];
					searchValue = (String) obj[2];
					filter = (String) obj[3];
					orderBy = (Integer) obj[4];
					orderType = (String) obj[5];
					pagenumber = (String) obj[6];
					pagesize = (String) obj[7];
					groupBy = (String) obj[8];
					Log.Info("Fetching the L2 View for EMEA  Profile ...");
				//	String tableName = "EMEA_L2_QT";
					
					con = dbUtil.getHiveConnection(E2emfConstants.emeaDB);
					Log.Info("Using EMEA HIVE Connection...");
					
					queryToExecute = "SELECT " + columns + " FROM " + table + " WHERE 1=1 ";
					if (filter != null && !filter.trim().equalsIgnoreCase("")
							&& !filter.equalsIgnoreCase("null")) {
						filterQuery = " AND (" + filter + ")";
						queryToExecute += filterQuery;
					}
					if (searchValue != null
							&& !searchValue.trim().equalsIgnoreCase("")
							&& !searchValue.equalsIgnoreCase("null")) {
						String[] columnList = columns.split(",");

						for (int i = 0; i < columnList.length; i++) {
							if (i == 0)
								columnConcat = "CAST(" + columnList[i]
										+ " AS String)";
							else
								columnConcat += ",' ',CAST(" + columnList[i]
										+ " AS String)";
						}
						columnConcat = " AND ( LOWER(CONCAT(" + columnConcat
								+ ")) like '%" + searchValue.toLowerCase() + "%')";
						queryToExecute += columnConcat;
					}
					if (groupBy != null) {
						queryToExecute += " GROUP BY " + groupBy +" ";
					}
					if (orderBy != null) {
						orderBy = orderBy + 1;
						queryToExecute += " ORDER BY " + Integer.toString(orderBy) + " "
								+ orderType;
					}
					if (pagesize != null) {
						queryToExecute += " LIMIT  " + pagesize;
					}
					if (pagesize != null) {
						queryToExecute += " OFFSET  " + pagenumber;
					}
					
				}
				
				if (con != null) {
					Statement stmt = con.createStatement();
					stmt = con.createStatement();
					 rs = stmt.executeQuery(queryToExecute);
					JSONArray convertedJson = dbUtil.convert(rs);
					datatableView.setData(convertedJson);
					ResultSet rsCount = stmt
							.executeQuery("SELECT COUNT(*) as count FROM " + table);
					if (rsCount != null) {
						while (rsCount.next()) {
							datatableView.setRecordsTotal(rsCount
									.getString("count"));
							datatableView.setRecordsFiltered(rsCount
									.getString("count"));
						}
					}
					if (!filterQuery.trim().equalsIgnoreCase("")
							|| !columnConcat.trim().equalsIgnoreCase("")) {
						String filterNumQuery = "SELECT COUNT(*) as count FROM "
								+ table + " WHERE 1=1 ";
						filterNumQuery += filterQuery;
						filterNumQuery += columnConcat;
						rsCount = stmt.executeQuery(filterNumQuery);
						if (rsCount != null) {
							while (rsCount.next()) {
								datatableView.setRecordsFiltered(rsCount
										.getString("count"));
							}
						}
					}

				}
				return datatableView;
		}catch(SQLException sql){
			Log.Error("SQLException while Fetching Master Details for EMEA  Profile L2 View." + sql.getStackTrace());
			Log.Error(sql);
			throw new AppException(sql.getMessage(),"Error while processing the request.", 1 ,sql.getCause(), true);
		}catch(Exception e){
			Log.Error("Exception fetching the Details for EMEA  Profile L2 View. \n " + e.getMessage());
			Log.Error(e);
			e.printStackTrace();
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
		}finally{
			if(con != null)
				try {
					con.close();
					//stmt.close();
					//con = null;
					//stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
				
		}
	}
	
	public ArrayList<EMEAView> getEMEAPPVProfileViewDetails(Object... obj) throws AppException {
		//String source = null;
		String materialid = null;
		String vendorid = null;
		String specid = null;
		ArrayList<EMEAView> emeaPPVProfileViewList = new ArrayList<EMEAView>();
		StringBuilder queryToExecute = null;
		ResultSet rs = null;
		try {
			if (obj != null && obj.length >= 1) {
				//source = (String) obj[0];
				materialid = (String) obj[1];
				//materialid="'908580','906289'"; //For Testing
				vendorid = (String) obj[2];
				specid = (String) obj[3];
				Log.Info("Fetching the DefaultView for EMEA PPV Profile ...");
				String tableName = "EMEA_AG_PPV_PROFILE";
				String filterTableName = "EMEA_L2_PPV";
				String whereClause = "";
						
				con = dbUtil.getHiveConnection(E2emfConstants.emeaDB);
				Log.Info("Using EMEA HIVE Connection...");

				//queryToExecute = new StringBuilder(emeaPOProfileDefaultQuery).append(emea_db).append(".").append(tableName);
				//queryToExecute = new StringBuilder(emeaPPVProfileDefaultQuery).append(tableName);
				if(materialid == null && vendorid== null && specid == null)
					queryToExecute = new StringBuilder(emeaPPVProfileDefaultQuery).append(tableName);
				else{

					if(materialid!= null && vendorid!=null && specid != null)
						whereClause = " where material_code in ('" + materialid + "') and vendor_no in ('" + vendorid + "') and material_spec_ref in ('" + specid + "')"; 
					if(materialid != null && vendorid != null && specid == null)		
						whereClause = "where material_code in ('" + materialid + "') and vendor_no in ('" + vendorid + "')";
					if(materialid != null && vendorid == null && specid != null)		
						whereClause = "where material_code in ('" + materialid + "') and material_spec_ref in ('" + specid + "')";
					if(materialid == null && vendorid != null && specid != null)		
						whereClause = "where vendor_no in ('" + vendorid + "') and material_spec_ref in ('" + vendorid + "')";
					if(materialid == null && vendorid != null && specid == null)		
						whereClause = " where vendor_no in ('" + vendorid + "')";
					if(materialid != null && vendorid == null && specid == null)	
						whereClause = " where material_code in ('" + materialid + "')";
					if(materialid == null && vendorid == null && specid != null)	
						whereClause = " where material_spec_ref in ('" + specid + "')";
					
					queryToExecute = new StringBuilder(emeaPPVProfileFilterQuery).append(filterTableName).append(whereClause).append(groupByClausePPVProfileFilter);
					
					
				}
				Log.Info("EMEA PPV Profile Default View Query to Execute :"+queryToExecute.toString());

				stmt = con.createStatement();

				rs = stmt.executeQuery(queryToExecute.toString());
				if (rs != null) {
					
					while (rs.next()) {
						EMEAView emeaView = new EMEAView();
						emeaView.setPpvCount(rs.getString("invoicecount"));
						emeaView.setPostingYear(rs.getString("posting_year"));
						emeaView.setPostingMonth(rs.getString("posting_month"));
						emeaView.setPlantName(rs.getString("plant_name"));
						emeaView.setPostingDateDoc(rs.getString("posting_date_doc"));
						emeaView.setTransactionType(rs.getString("transaction_type"));
						emeaView.setSseTransaction(rs.getString("ssetransaction"));
			//			emeaView.setDocumentCategory(rs.getString("document_category"));
			//			emeaView.setDocumentCategoryDesc(rs.getString("document_category_desc"));
						emeaView.setCategory(rs.getString("category"));
						emeaView.setSubCategory(rs.getString("subcategory"));
						emeaView.setVariance(rs.getString("variance"));
						emeaView.setInvoiceCurrency(rs.getString("document_currency"));
//						emeaView.setSumInvoicespendActual(Math.round(rs.getInt("sum_invoicespend_actual") * 100) / 100);
						emeaView.setSumInvoicespendActual(rs.getInt("sum_invoicespend_actual"));
						emeaView.setSumInvoicespendWahr(rs.getInt("sum_invoicespend_wahr"));
						emeaView.setSumInvoicespendBp(rs.getInt("sum_invoicespend_bp"));
						emeaView.setTotalppvFcurActualrate(rs.getInt("totalppv_fcur_actualrate"));
						emeaView.setTotalppvFcurWahr(rs.getInt("totalppv_fcur_wahr"));
						emeaView.setTotalppvFcurBp(rs.getInt("totalppv_fcur_bp"));
						emeaView.setFcur(rs.getString("fcur"));
						emeaView.setQty(rs.getInt("qty"));
						
//						int sumInvoiceSpendActual= rs.getInt("sum_invoicespend_actual");
//						
//						if(sumInvoiceSpendActual != undef ){
//							double doubleValueToChange = Double.valueOf(sumInvoiceSpendActual).doubleValue();
//							double absDoubleValueToChange = Math.abs(doubleValueToChange);
//							emeaView.setSumInvoicespendActual(roundDigits(absDoubleValueToChange));
//						}else{
//							emeaView.setSumInvoicespendActual(sumInvoiceSpendActual);
//						}
						
						emeaPPVProfileViewList.add(emeaView);						
					}
				}
			}

		}catch(SQLException sql){
			Log.Error("SQLException while Fetching Master Details for EMEA PPV Profile." + sql.getStackTrace());
			Log.Error(sql);
			throw new AppException(sql.getMessage(),"Error while processing the request.", 1 ,sql.getCause(), true);
		}catch(Exception e){
			Log.Error("Exception fetching the Details for EMEA PPV Profile. \n " + e.getMessage());
			Log.Error(e);
			e.printStackTrace();
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
		}finally{
			if(con != null)
				try {
					con.close();
					//stmt.close();
					//con = null;
					//stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
				
		}
		return emeaPPVProfileViewList;
	}
	
	public ArrayList<EMEAView> getEMEAPPVProfileL2ViewDetails(Object... obj) throws AppException {
		ArrayList<EMEAView> emeaPPVProfileL2ViewList = new ArrayList<EMEAView>();
		StringBuilder queryToExecute = null;
		ResultSet rs = null;
		String plantname = null;
		String posting_year= null;
		String posting_month= null;
		String posting_date_doc= null;
		String material_type= null;
		String purchase_currency= null;
		String invoice_currency = null;
		String purchasing_uom= null;
		String stocking_uom= null;
		String payment_terms = null;
		//String materialcategory = null;
		String documentcategory = null;
		String totalinvoicepricequartile = null;
		String standardcostquartile = null;
		String variance = null;
		String ppv2quartile = null;
		//String ppvmismatch = null;
		String incotermcode = null;
		String gssstatus = null;
		String vendorFreqQuantityQuartile = null;
		String vendorFreqPosQuartile = null;
		String materialFreqQuantityQuartile = null;
		String MaterialFreqPosQuartile = null;
		//String netvalue= null;
		String materialCode = null;
		String vendorNumber = null;
		String materialSpecRef = null;
		String agViewFlag = null;
		boolean isAGView = false;
		
		StringBuilder where = null;
		try {
				if (obj != null && obj.length >= 1) {
					plantname = (String) obj[0];
					posting_year = (String) obj[1];
					posting_month = (String) obj[2];
					posting_date_doc = (String) obj[3];
					material_type = (String) obj[4];
					purchase_currency = (String) obj[5];
					invoice_currency = (String) obj[6];
					purchasing_uom = (String) obj[7];
					stocking_uom = (String) obj[8];
					payment_terms = (String) obj[9];
					//materialcategory = (String) obj[10];
					documentcategory = (String) obj[11];
					totalinvoicepricequartile = (String) obj[12];
					standardcostquartile = (String) obj[13];
					variance = (String) obj[14];
					ppv2quartile = (String) obj[15];
					//ppvmismatch = (String) obj[16];
					incotermcode = (String) obj[17];
					gssstatus = (String) obj[18];
					vendorFreqQuantityQuartile = (String) obj[19];
					vendorFreqPosQuartile = (String) obj[20];
					materialFreqQuantityQuartile = (String) obj[21];
					MaterialFreqPosQuartile = (String) obj[22];
					//netvalue = (String) obj[23];
					materialCode = (String) obj[24];
					vendorNumber = (String) obj[25];
					materialSpecRef = (String) obj[26];
					agViewFlag = (String) obj[27];
					
					isAGView = Boolean.valueOf(agViewFlag).booleanValue();
					
					Log.Info("Fetching the L2 View for EMEA PPV Profile ...");
					String tableName = "EMEA_L2_PPV";
					//if(poNumber!= null){
					//	whereClause = " where po_number in ('" + poNumber + "')";
					//}
					where = new StringBuilder(" where plant_name = '").append(plantname)
							.append("'");
					
					if (posting_year == null || posting_year.equalsIgnoreCase(null) || posting_year.equalsIgnoreCase("NULL")) {
						where.append(" and (posting_year ='' or posting_year is null)");
					} else { 
						where.append(" and posting_year = '").append(posting_year).append("'");
					}
					
					if (posting_month == null || posting_year.equalsIgnoreCase(null) || posting_month.equalsIgnoreCase("NULL")) {
						where.append(" and (posting_month ='' or posting_month is null)");
					} else { 
						where.append(" and posting_month = '").append(posting_month).append("'");
					}
					
					if (posting_date_doc == null || posting_date_doc.equalsIgnoreCase(null) || posting_date_doc.equalsIgnoreCase("NULL")) {
						where.append(" and (posting_date_doc ='' or posting_date_doc is null)");
					} else { 
						where.append(" and posting_date_doc = '").append(posting_date_doc).append("'");
					}
					
					if (material_type == null || material_type.equalsIgnoreCase(null) || material_type.equalsIgnoreCase("NULL")) {
						where.append(" and (material_type ='' or material_type is null)");
					} else { 
						where.append(" and material_type = '").append(material_type).append("'");
					}
										
					if (purchase_currency == null || purchase_currency.equalsIgnoreCase(null) || purchase_currency.equalsIgnoreCase("NULL")) {
						where.append(" and (purchase_currency ='' or purchase_currency is null)");
					} else { 
						where.append(" and purchase_currency = '").append(purchase_currency).append("'");
					}
					
					if (invoice_currency == null || invoice_currency.equalsIgnoreCase(null) || invoice_currency.equalsIgnoreCase("NULL")) {
						where.append(" and (invoice_currency = NULL or invoice_currency is null)");
					} else { 
						where.append(" and invoice_currency = '").append(invoice_currency).append("'");
					}
					
					/*if (netvaluevariance == null || netvaluevariance.equalsIgnoreCase(null) || netvaluevariance.equalsIgnoreCase("NULL")) {
						where.append(" and (po_currency ='' or po_currency is null)");
					} else { 
						where.append(" and po_currency = '").append(netvaluevariance).append("'");
					}*/
					
					if (purchasing_uom == null || purchasing_uom.equalsIgnoreCase(null) || purchasing_uom.equalsIgnoreCase("NULL")) {
						where.append(" and (purchasing_uom ='' or purchasing_uom is null)");
					} else { 
						where.append(" and purchasing_uom = '").append(purchasing_uom).append("'");
					}
					
					if (stocking_uom == null || stocking_uom.equalsIgnoreCase(null) || stocking_uom.equalsIgnoreCase("NULL")) {
						where.append(" and (stocking_uom ='' or stocking_uom is null)");
					} else { 
						where.append(" and stocking_uom = '").append(stocking_uom).append("'");
					}
					
					if (payment_terms == null || payment_terms.equalsIgnoreCase(null) || payment_terms.equalsIgnoreCase("NULL")) {
						where.append(" and (payment_terms ='' or payment_terms is null)");
					} else { 
						where.append(" and payment_terms = '").append(payment_terms).append("'");
					}
					
					/*if (materialcategory == null || materialcategory.equalsIgnoreCase(null) || materialcategory.equalsIgnoreCase("NULL")) {
						where.append(" and (material_category ='' or material_category is null)");
					} else { 
						where.append(" and material_category = '").append(materialcategory).append("'");
					}*/
					
					if (documentcategory == null || documentcategory.equalsIgnoreCase(null) || documentcategory.equalsIgnoreCase("NULL")) {
						where.append(" and (document_category ='' or document_category is null)");
					} else { 
						where.append(" and document_category = '").append(documentcategory).append("'");
					}
					
					if (totalinvoicepricequartile == null || totalinvoicepricequartile.equalsIgnoreCase(null) || totalinvoicepricequartile.equalsIgnoreCase("NULL")) {
						where.append(" and (totalnvoicepricequartile ='' or totalnvoicepricequartile is null)");
					} else { 
						where.append(" and totalnvoicepricequartile = '").append(totalinvoicepricequartile).append("'");
					}
					
					if (standardcostquartile == null || standardcostquartile.equalsIgnoreCase(null) || standardcostquartile.equalsIgnoreCase("NULL")) {
						where.append(" and (standardcostquartile ='' or standardcostquartile is null)");
					} else { 
						where.append(" and standardcostquartile = '").append(standardcostquartile).append("'");
					}
					
					if (variance == null || variance.equalsIgnoreCase(null) || variance.equalsIgnoreCase("NULL")) {
						where.append(" and (variance ='' or variance is null)");
					} else { 
						where.append(" and variance = '").append(variance).append("'");
					}
					
					if (ppv2quartile == null || ppv2quartile.equalsIgnoreCase(null) || ppv2quartile.equalsIgnoreCase("NULL")) {
						where.append(" and (ppv2quartile ='' or ppv2quartile is null)");
					} else { 
						where.append(" and ppv2quartile = '").append(ppv2quartile).append("'");
					}
					
					/*if (ppvmismatch == null || ppvmismatch.equalsIgnoreCase(null) || ppvmismatch.equalsIgnoreCase("NULL")) {
						where.append(" and (ppvmismatch ='' or ppvmismatch is null)");
					} else { 
						where.append(" and ppvmismatch = '").append(ppvmismatch).append("'");
					}
					*/
					
					if (incotermcode == null || incotermcode.equalsIgnoreCase(null) || incotermcode.equalsIgnoreCase("NULL")) {
						where.append(" and (incoterm_code ='' or incoterm_code is null)");
					} else { 
						where.append(" and incoterm_code = '").append(incotermcode).append("'");
					}
					if (gssstatus == null || gssstatus.equalsIgnoreCase(null) || gssstatus.equalsIgnoreCase("NULL")) {
						where.append(" and (gss_status ='' or gss_status is null)");
					} else { 
						where.append(" and gss_status = '").append(gssstatus).append("'");
					}
					
					if (vendorFreqQuantityQuartile == null || vendorFreqQuantityQuartile.equalsIgnoreCase(null) || vendorFreqQuantityQuartile.equalsIgnoreCase("NULL")) {
						where.append(" and (vendor_freq_quantityquartile ='' or vendor_freq_quantityquartile is null)");
					} else { 
						where.append(" and vendor_freq_quantityquartile = '").append(vendorFreqQuantityQuartile).append("'");
					}
					
					if (vendorFreqPosQuartile == null || vendorFreqPosQuartile.equalsIgnoreCase(null) || vendorFreqPosQuartile.equalsIgnoreCase("NULL")) {
						where.append(" and (vendor_freq_posquartile ='' or vendor_freq_posquartile is null)");
					} else { 
						where.append(" and vendor_freq_posquartile = '").append(vendorFreqPosQuartile).append("'");
					}
					
					if (materialFreqQuantityQuartile == null || materialFreqQuantityQuartile.equalsIgnoreCase(null) || materialFreqQuantityQuartile.equalsIgnoreCase("NULL")) {
						where.append(" and (material_freq_quantityquartile ='' or material_freq_quantityquartile is null)");
					} else { 
						where.append(" and material_freq_quantityquartile = '").append(materialFreqQuantityQuartile).append("'");
					}
					
					if (MaterialFreqPosQuartile == null || MaterialFreqPosQuartile.equalsIgnoreCase(null) || MaterialFreqPosQuartile.equalsIgnoreCase("NULL")) {
						where.append(" and (material_freq_posquartile ='' or material_freq_posquartile is null)");
					} else { 
						where.append(" and material_freq_posquartile = '").append(MaterialFreqPosQuartile).append("'");
					}
					
					if (materialCode == null || materialCode.equalsIgnoreCase(null) || materialCode.equalsIgnoreCase("NULL")) {
						//where.append(" and (material_code ='' or material_code is null)");
					} else { 
						where.append(" and material_code = '").append(materialCode).append("'");
					}
					
					if (vendorNumber == null || vendorNumber.equalsIgnoreCase(null) || vendorNumber.equalsIgnoreCase("NULL")) {
						//where.append(" and (vendor_no ='' or vendor_no is null)");
					} else { 
						where.append(" and vendor_no = '").append(vendorNumber).append("'");
					}
					
					if (materialSpecRef == null || materialSpecRef.equalsIgnoreCase(null) || materialSpecRef.equalsIgnoreCase("NULL")) {
						//where.append(" and (material_spec_ref ='' or material_spec_ref is null)");
					} else { 
						where.append(" and material_spec_ref = '").append(materialSpecRef).append("'");
					}
					
					/*if (netvalue == null || netvalue.equalsIgnoreCase(null) || netvalue.equalsIgnoreCase("NULL")) {
						where.append(" and (po_total_netvalue = NULL or po_total_netvalue is null)");
					} else { 
						where.append(" and po_total_netvalue = ").append(netvalue);
					}*/
					
					con = dbUtil.getHiveConnection(E2emfConstants.emeaDB);
					Log.Info("Using EMEA HIVE Connection...");
					
					//queryToExecute = new StringBuilder(emeaPOProfileDefaultQuery).append(emea_db).append(".").append(tableName);
					//queryToExecute = new StringBuilder(emeaPPVProfileL2Query).append(tableName).append(where.toString()).append(emeaPPVProfilegroupByClauseFilter);
					if(isAGView){
						queryToExecute = new StringBuilder(emeaPPVProfileL2Query).append(tableName);
					}else{
						queryToExecute = new StringBuilder(emeaPPVProfileL2Query).append(tableName).append(where.toString());
					}
					
					Log.Info("EMEA PPV Profile L2 View Query to Execute :"+queryToExecute.toString());
	
					stmt = con.createStatement();
	
					rs = stmt.executeQuery(queryToExecute.toString());
					if (rs != null) {
						
						while (rs.next()) {
							
							EMEAView emeaView = new EMEAView();
							emeaView.setInvoiceNumber(rs.getString("invoice_number"));
							emeaView.setInvoiceLineNumber(rs.getString("invoice_linenumber"));
							emeaView.setPostingYear(rs.getString("posting_year"));
							emeaView.setPostingMonth(rs.getString("posting_month"));
							emeaView.setSapPC(rs.getString("sap_plantcode"));
							emeaView.setPlantName(rs.getString("plant_name"));
							emeaView.setPurchaseDocNo(rs.getString("purchase_doc_no"));
							emeaView.setPostingDateDoc(rs.getString("posting_date_doc"));
							emeaView.setGlAccount(rs.getString("gl_account"));
							emeaView.setMaterialCode(rs.getString("material_code"));
							emeaView.setMaterialCodePred(rs.getString("material_code_pred"));
							emeaView.setMaterialDesc(rs.getString("material_desc"));
							emeaView.setMaterialDesPred(rs.getString("material_desc_pred"));
							emeaView.setMaterialSpecRef(rs.getString("material_spec_ref"));
							String vendorNo = rs.getString("vendor_no");
							emeaView.setVendorNo(vendorNo);
							emeaView.setSecondaryVendorNo(rs.getString("secondary_vendor_no"));
							String vendorName = rs.getString("vendor_name");
							if(vendorName == null || vendorName.length() < 1 || vendorName == ""){
								emeaView.setVendorName(vendorNo);
							}
							else{
								emeaView.setVendorName(vendorName);
							}
							emeaView.setSecondaryVendorName(rs.getString("secondary_vendor_name"));
							emeaView.setVendorCountry(rs.getString("vendor_country"));
							emeaView.setVendorCity(rs.getString("vendor_city"));
							emeaView.setSecondaryVendorCountry(rs.getString("secondary_vendor_country"));
							emeaView.setSecondaryVendorCity(rs.getString("secondary_vendor_city"));
							emeaView.setDistributorNo(rs.getString("distributor_no"));
							emeaView.setDistributorName(rs.getString("distributor_name"));
							emeaView.setDistributorCountry(rs.getString("distributor_country"));
							emeaView.setDistributorCity(rs.getString("distributor_city"));
							emeaView.setSecondaryDistributorNo(rs.getString("secondary_distributor_no"));
							emeaView.setSecondaryDistributorName(rs.getString("secondary_distributor_name"));
							emeaView.setSecondaryDistributorCountry(rs.getString("secondary_distributor_country"));
							emeaView.setSecondaryDistributorCity(rs.getString("secondary_distributor_city"));
							emeaView.setManufacturerNo(rs.getString("manufacturer_no"));
							emeaView.setManufacturerName(rs.getString("manufacturer_name"));
							emeaView.setManufacturerCountry(rs.getString("manufacturer_country"));
							emeaView.setManufacturerCity(rs.getString("manufacturer_city"));
							emeaView.setSecondaryManufacturerNo(rs.getString("secondary_manufacturer_no"));
							emeaView.setSecondaryManufacturerName(rs.getString("secondary_manufacturer_name"));
							emeaView.setSecondaryManufacturerCountry(rs.getString("secondary_manufacturer_country"));
							emeaView.setSecondaryManufacturerCity(rs.getString("secondary_manufacturer_city"));
							//emeaView.setMaterialCategory(rs.getString("material_category"));
							emeaView.setMaterialSubCategory(rs.getString("commoditylevel2"));
							emeaView.setPurchasingUom(rs.getString("purchasing_uom"));
							emeaView.setStockingUom(rs.getString("stocking_uom"));
							emeaView.setPpv2AmountDocCurrency(rs.getString("ppv2_amount_doc_currency"));
							
							String stdCost = rs.getString("standard_costperunit_base_currency");
							if(stdCost != null){
								double doubleValueToChange = Double.valueOf(stdCost).doubleValue();
								emeaView.setStandardCostperunitBaseCurrency(roundDigits(doubleValueToChange));
							}else{
								emeaView.setStandardCostperunitBaseCurrency(stdCost);
							}
							
							String invQty = rs.getString("inv_quantity");
							if(invQty != null){
								double doubleValueToChange = Double.valueOf(invQty).doubleValue();
								emeaView.setInvQuantity(roundDigits(doubleValueToChange));
							}else{
								emeaView.setInvQuantity(invQty);
							}
							
							emeaView.setPurchasePriceUom(rs.getString("purchase_price_uom"));
							
							String totalAmountInBaseBaseCurrency = rs.getString("invoice_price_base_currency");
							if(totalAmountInBaseBaseCurrency != null){
								double doubleValueToChange = Double.valueOf(totalAmountInBaseBaseCurrency).doubleValue();
								emeaView.setInvoicePriceBaseCurrency(roundDigits(doubleValueToChange));
							}else{
								emeaView.setInvoicePriceBaseCurrency(totalAmountInBaseBaseCurrency);
							}
							
							String ssePrice =rs.getString("sse_price");
							if(ssePrice != null){
								double doubleValueToChange = Double.valueOf(ssePrice).doubleValue();
								emeaView.setSsePrice(roundDigits(doubleValueToChange));
							}else{
									emeaView.setSsePrice(ssePrice);
							}
							
							String amountBaseCurrency = rs.getString("amount_base_currency");
							if(amountBaseCurrency != null){
								double doubleValueToChange = Double.valueOf(amountBaseCurrency).doubleValue();
								emeaView.setAmountBaseCurrency(roundDigits(doubleValueToChange));
							}else{
								emeaView.setAmountBaseCurrency(amountBaseCurrency);
							}
							emeaView.setPaymentTermsDesc(rs.getString("payment_termsdesc"));
							//emeaView.setTotalpoAmtEurActualrate(rs.getString("totalpo_amt_eur_actualrate"));
							//emeaView.setTotalpoAmtEur15wahr(rs.getString("totalpo_amt_eur_15wahr"));
							emeaView.setInvoiceSpendEur15wahr(rs.getString("invoice_spend_eur_15wahr"));
							emeaView.setInvoiceSpendEurActualrate(rs.getString("invoice_spend_eur_actualrate"));
							//emeaView.setTotalAmountAtStandardCost(rs.getString("total_amount_at_standard_cost"));
							//emeaView.setPpvMismatch(rs.getString("ppvmismatch"));
							String ppv2AmountEurActualrate = rs.getString("ppv2_amount_eur_actualrate");
							if(ppv2AmountEurActualrate != null){
								double doubleValueToChange = Double.valueOf(ppv2AmountEurActualrate).doubleValue();
								emeaView.setPpv2AmountEurActualrate(roundDigits(doubleValueToChange));
							}else{
								emeaView.setPpv2AmountEurActualrate(ppv2AmountEurActualrate);
							}
							//emeaView.setDocumentCurrency(rs.getString("document_currency"));
							emeaView.setInvoiceCurrency(rs.getString("invoice_currency"));
							emeaView.setGssStatus(rs.getString("gss_status"));
							emeaView.setTotalPpvDocCurrency(rs.getString("totalppv_doc_currency"));
							emeaView.setTotalPpvEurActualrate(rs.getString("totalppv_eur_actualrate"));
							emeaView.setTotalPpvNewEurActualrate(rs.getString("totalppvnew_eur_actualrate"));
							emeaView.setTotalPpvEur15wahr(rs.getString("totalppv_eur_15wahr"));
							emeaView.setTotalAmtAtStandardCostBaseCurrency(rs.getString("total_amount_at_standard_cost_base_currency"));
							emeaView.setTotalAmtAtStandardCostEurActualrate(rs.getString("total_amount_at_standard_cost_eur_actualrate"));
							emeaView.setTotalAmtAtStandardCostEur15wahr(rs.getString("total_amount_at_standard_cost_eur_15wahr"));
							emeaView.setTotalpoAmtDocCurrency(rs.getString("totalpo_amt_doc_currency"));
							emeaView.setTotalpoAmtEurActualrate(rs.getString("totalpo_amt_eur_actualrate"));
							emeaView.setTotalpoAmtEur15wahr(rs.getString("totalpo_amt_eur_15wahr"));
							emeaView.setStandardCostperunitDocCurrency(rs.getString("standard_costperunit_doc_currency"));
							/*invoice_number, invoice_linenumber, posting_year, posting_month, plant_name, purchase_doc_no, 
							 * posting_date_doc, gl_account, material_code, material_code_pred, material_desc, material_desc_pred, 
							 * material_spec_ref, vendor_no, secondary_vendor_no, vendor_name, secondary_vendor_name, vendor_country, vendor_city, 
							 * secondary_vendor_country, secondary_vendor_city, distributor_no, distributor_name, distributor_country, distributor_city, 
							 * secondary_distributor_no, secondary_distributor_name, secondary_distributor_country, secondary_distributor_city, 
							 * manufacturer_no, manufacturer_name, manufacturer_country, manufacturer_city, secondary_manufacturer_no, secondary_manufacturer_name, secondary_manufacturer_country, secondary_manufacturer_city, 
							 * commoditylevel1, commoditylevel2, purchasing_uom, stocking_uom, conversion_factor, inv_quantity, receipt_quantity, 
							 * moq, ioq, incoterm_code, incoterm_desc, incoterm_location_country, incoterm_location_city, payment_terms, payment_termsdesc, 
							 * payment_days, vmi_flag, leadtime, purchase_currency, document_currency, price_validity_start_date, price_validity_end_date, 
							 * amount_doc_currency, amount_base_currency, amount_eur_actualrate, amount_eur_15wahr_rate, pounitprice_doc_currency, 
							 * pounitprice_eur_converted_actualrate, pounitprice_eur_converted_15wahr, totalpo_amt_doc_currency, totalpo_amt_eur_actualrate, 
							 * totalpo_amt_eur_15wahr, purchase_price_uom, invoice_price_doc_currency, invoice_spend_base_currency, invoice_price_base_currency	, 
							 * invoice_spend_eur_actualrate, invoice_price_eur_actual_rate, invoice_spend_eur_15wahr, invoice_price_eur_15wahr, 
							 * standard_costperunit_base_currency, standard_costperunit_eur_actualrate, standard_costperunit_eur_15wahr, 
							 * total_amount_at_standard_cost_base_currency, total_amount_at_standard_cost_eur_actualrate, total_amount_at_standard_cost_eur_15wahr, 
							 * ppv2_amount_doc_currency, ppv2_amount_eur_actualrate, ppv2_amount_eur_15wahr, ppv1_amount_doc_currency, ppv1_amount_eur_actualrate, 
							 * ppv1_amount_eur_15wahr, totalppv_doc_currency, totalppv_eur_actualrate, totalppv_eur_15wahr, ssetransaction, actual_exchange_rate, 
							 * weighted_average_hedge_rate_15wahr, ppvmismatch, invoice_price_base_currency, sse_price, document_category, document_category_desc, 
							 * variance, totalnvoicepricequartile, standardcostquartile, ppv2quartile, ppv2absquartile, ppv2percentage, ppv2percentagequartile,
							 * material_type, receipt_done, vendor_freq_quantityquartile, vendor_freq_posquartile, material_freq_quantityquartile, 
							 * material_freq_posquartile, gss_status
							 */
							
							emeaPPVProfileL2ViewList.add(emeaView);						
						}
					}	
				}

		}catch(SQLException sql){
			Log.Error("SQLException while Fetching Master Details for EMEA PPV Profile L2 View." + sql.getStackTrace());
			Log.Error(sql);
			throw new AppException(sql.getMessage(),"Error while processing the request.", 1 ,sql.getCause(), true);
		}catch(Exception e){
			Log.Error("Exception fetching the Details for EMEA PPV Profile L2 View. \n " + e.getMessage());
			Log.Error(e);
			e.printStackTrace();
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
		}finally{
			if(con != null)
				try {
					con.close();
					//stmt.close();
					//con = null;
					//stmt = null;
				} catch (SQLException e) {
					e.printStackTrace();
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
				
		}
		return emeaPPVProfileL2ViewList;
	}
	
	public ArrayList<EMEAView> getEMEAPOVarianceAnalysisViewDetails(Object... obj) throws AppException {
		ArrayList<EMEAView> emeaPPOVarianceAnalysisViewList = new ArrayList<EMEAView>();
		StringBuilder queryToExecute = null;
		ResultSet rs = null;
		/*String sliderstart = null;
		String sliderend = null;*/
		try {
				if (obj != null && obj.length >= 1) {
					
					/*sliderstart = (String) obj[0];
					sliderend = (String) obj[1];*/
					
					Log.Info("Fetching the PO Variance View for EMEA PPV Profile ...");
					String tableName = "EMEA_L2_PO";
					
					con = dbUtil.getHiveConnection(E2emfConstants.emeaDB);
					Log.Info("Using EMEA HIVE Connection...");
					
					StringBuilder where = new StringBuilder(" order by totalnetvaluevariance desc NULLS LAST");
					
					//queryToExecute = new StringBuilder(emeaPOProfileDefaultQuery).append(emea_db).append(".").append(tableName);
					//queryToExecute = new StringBuilder(emeaPPVProfileL2Query).append(tableName).append(where.toString()).append(emeaPPVProfilegroupByClauseFilter);
					queryToExecute = new StringBuilder(emeaVarianceAnalysisProfileL2Query).append(tableName).append(emeaVarianceAnalysisProfilegroupByClauseFilter).append(where.toString());
										
					Log.Info("EMEA PO Variance Analysis View Query to Execute :"+queryToExecute.toString());
	
					stmt = con.createStatement();
	
					rs = stmt.executeQuery(queryToExecute.toString());
					if (rs != null) {
						
						while (rs.next()) {
							
							EMEAView emeaView = new EMEAView();
							emeaView.setSapPC(rs.getString("sapplantcode"));
							emeaView.setPlantDesc(rs.getString("plantdesc"));
							emeaView.setSsePlant(rs.getString("sse_plant"));
							emeaView.setPoNumber(rs.getString("po_number"));
							emeaView.setPoLineNumber(rs.getString("po_line_number"));
							emeaView.setVendorNumber(rs.getString("vendor_number"));
							emeaView.setVendorName(rs.getString("vendor_name"));
							emeaView.setPoCreationDate(rs.getString("po_creation_date"));
							emeaView.setPoReqDate(rs.getString("po_req_date"));
							emeaView.setPoCurrency(rs.getString("po_currency"));
							emeaView.setPoExchangeRate(rs.getString("po_exchangerate"));
							emeaView.setCompanyCode(rs.getString("company_code"));
							emeaView.setPoMaterialNumber(rs.getString("po_material_number"));
							emeaView.setSseMaterialNumber(rs.getString("sse_material_number"));
							emeaView.setPoMaterialDesc(rs.getString("po_material_desc"));
							String poOrderQty =rs.getString("po_order_qty");
							if(poOrderQty != null){
								double doubleValueToChange = Double.valueOf(poOrderQty).doubleValue();
								emeaView.setPoOrderQty(roundDigits(doubleValueToChange));
							}else{
									emeaView.setPoOrderQty(poOrderQty);
							}
							emeaView.setPoUom(rs.getString("po_uom"));
							
							String poExpCost =rs.getString("po_exp_cost");
							if(poExpCost != null){
								double doubleValueToChange = Double.valueOf(poExpCost).doubleValue();
								emeaView.setPoExpCost(roundDigits(doubleValueToChange));
							}else{
									emeaView.setPoExpCost(poExpCost);
							}
							
							String ssePrice =rs.getString("sse_price");
							if(ssePrice != null){
								double doubleValueToChange = Double.valueOf(ssePrice).doubleValue();
								emeaView.setSsePrice(roundDigits(doubleValueToChange));
							}else{
									emeaView.setSsePrice(ssePrice);
							}
							
							String priceVariance =rs.getString("price_variance");
							if(priceVariance != null){
								double doubleValueToChange = Double.valueOf(priceVariance).doubleValue();
								emeaView.setPriceVariance(roundDigits(doubleValueToChange));
							}else{
									emeaView.setPriceVariance(priceVariance);
							}
							
							String pVP =rs.getString("price_variance_percentage");
							if(pVP != null){
								double doubleValueToChange = Double.valueOf(pVP).doubleValue();
								emeaView.setpVP(roundDigits(doubleValueToChange));
							}else{
									emeaView.setpVP(pVP);
							}
							
							String poTotalNetValue =rs.getString("po_total_netvalue");
							if(poTotalNetValue != null){
								double doubleValueToChange = Double.valueOf(poTotalNetValue).doubleValue();
								emeaView.setPoTotalNetValue(roundDigits(doubleValueToChange));
							}else{
									emeaView.setPoTotalNetValue(poTotalNetValue);
							}
							
							emeaView.setPoIncoTerm(rs.getString("po_inco_term"));
							emeaView.setIncoTermDesc(rs.getString("inco_term_desc"));
							emeaView.setPoLeadTime(rs.getString("po_lead_time"));
							emeaView.setSseContractFound(rs.getString("sse_contract_found"));
							emeaView.setPoMaterialGroup(rs.getString("po_material_group"));
							emeaView.setMaterialGroupDesc(rs.getString("material_group_desc"));
							emeaView.setMatchCategory(rs.getString("match_category"));
							emeaView.setcI(rs.getString("compliant_indicator"));
							emeaView.setSseMatchFound(rs.getString("sse_match_found"));
							emeaView.setPoItemType(rs.getString("po_item_type"));
							emeaView.setSseMoq(rs.getString("sse_moq"));
							emeaView.setSseCurrency(rs.getString("sse_currency"));
							emeaView.setSseIncoTerm(rs.getString("sse_incoterm"));
							emeaView.setSseLeadTime(rs.getString("sse_leadtime"));
							emeaView.setCurrencyMI(rs.getString("currency_mismatch_ind"));
							emeaView.setLeadTimeMI(rs.getString("lead_time_mismatch_ind"));
							emeaView.setMoqMI(rs.getString("moq_mismatch_ind"));
							emeaView.setUnitMI(rs.getString("unit_mismatch_ind"));
							emeaView.setIncoTermMI(rs.getString("incoterm_mismatch_ind"));
							emeaView.setGssStatus(rs.getString("gss_status"));
							
							String totalNetvalueVariance =rs.getString("totalnetvaluevariance");
							if(totalNetvalueVariance != null){
								double doubleValueToChange = Double.valueOf(totalNetvalueVariance).doubleValue();
								emeaView.setTotalNetvalueVariance(roundDigits(doubleValueToChange));
							}else{
									emeaView.setTotalNetvalueVariance(totalNetvalueVariance);
							}
							
							emeaPPOVarianceAnalysisViewList.add(emeaView);						
						}
					}	
				}
			}catch(SQLException sql){
				Log.Error("SQLException while Fetching Master Details for EMEA PO Variance Analysis View." + sql.getStackTrace());
				Log.Error(sql);
				throw new AppException(sql.getMessage(),"Error while processing the request.", 1 ,sql.getCause(), true);
			}catch(Exception e){
				Log.Error("Exception fetching the Details for EMEA PO Variance Analysis View. \n " + e.getMessage());
				Log.Error(e);
				e.printStackTrace();
				if(e instanceof NullPointerException){
					throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
				}
				throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
			}finally{
				if(con != null)
					try {
						con.close();
						//stmt.close();
						//con = null;
						//stmt = null;
					} catch (SQLException e) {
						e.printStackTrace();
						throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
					}
					
			}
			return emeaPPOVarianceAnalysisViewList;
	}
	
	public ArrayList<EMEAView> getEMEAPPVOverviewDetails(Object... obj) throws AppException {
		ArrayList<EMEAView> emeaPPVOverViewList = new ArrayList<EMEAView>();
		StringBuilder queryToExecute = null;
		ResultSet rs = null;
		try {
					Log.Info("Fetching the PPV Overview details ...");
					String tableName = "EMEA_PPV_AG_SUMMARY";
					
					con = dbUtil.getHiveConnection(E2emfConstants.emeaDB);
					Log.Info("Using EMEA HIVE Connection...");
					
					queryToExecute = new StringBuilder(emeaPPVOverviewQuery).append(tableName);
										
					Log.Info("EMEA PPV Overview Query to Execute :"+queryToExecute.toString());
	
					stmt = con.createStatement();
	
					rs = stmt.executeQuery(queryToExecute.toString());
					if (rs != null) {
						
						while (rs.next()) {
							
							EMEAView emeaView = new EMEAView();
							emeaView.setPostingYear(rs.getString("posting_year"));
							emeaView.setPostingMonth(rs.getString("posting_month"));
							emeaView.setCategory(rs.getString("category"));
							emeaView.setAmountType(rs.getString("amount_type"));
							emeaView.setTotalAmt(rs.getInt("total_amount"));
							emeaView.setCairoAmt(rs.getInt("cairo_amt"));
							emeaView.setWuppertalAmt(rs.getInt("wuppertal_amt"));
							emeaView.setSezanneAmt(rs.getInt("sezanne_amt"));
							emeaView.setPomeziaAmt(rs.getInt("pomezia_amt"));
							emeaView.setCapeTownAmt(rs.getInt("cape_town_amt"));
							emeaView.setEastLondonAmt(rs.getInt("east_london_amt"));
							emeaView.setValDeRuilAmt(rs.getInt("val_de_ruil_amt"));
							emeaView.setHelsingborgAmt(rs.getInt("helsingborg_amt"));
							emeaView.setMadridAmt(rs.getInt("madrid_amt"));
							emeaView.setMandraAmt(rs.getInt("mandra_amt"));
							emeaView.setCurrencyType(rs.getString("currency_type"));
							emeaView.setFcur(rs.getString("fcur"));

							emeaPPVOverViewList.add(emeaView);						
						}
					}	
			}catch(SQLException sql){
				Log.Error("SQLException while Fetching Master Details for EMEA PPV Overview View." + sql.getStackTrace());
				Log.Error(sql);
				throw new AppException(sql.getMessage(),"Error while processing the request.", 1 ,sql.getCause(), true);
			}catch(Exception e){
				Log.Error("Exception fetching the Details for EMEA PPV Overview View. \n " + e.getMessage());
				Log.Error(e);
				e.printStackTrace();
				if(e instanceof NullPointerException){
					throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
				}
				throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
			}finally{
				if(con != null)
					try {
						con.close();
						//stmt.close();
						//con = null;
						//stmt = null;
					} catch (SQLException e) {
						e.printStackTrace();
						throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
					}
					
			}
			return emeaPPVOverViewList;
	}
	
	public String roundDigits(double valueToChange){
		return String.format("%.2f", valueToChange);
	}
}
