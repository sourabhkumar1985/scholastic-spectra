package com.relevance.ppa.data;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"materialMapping"})
public class PlantPurchaseAnalyticsView {
	@JsonProperty("materialMapping")
	private List<PPAMaterialMapping> materialMapping = new ArrayList<PPAMaterialMapping>();
	
	public List<PPAMaterialMapping> getNGramAnalyser() {
		return materialMapping;
	}
	public void setMaterialMapping(List<PPAMaterialMapping> materialMapping) {
		this.materialMapping = materialMapping;
	}
	
}
