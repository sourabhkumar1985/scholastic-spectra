package com.relevance.ppa.dao;

import info.debatty.java.stringsimilarity.Levenshtein;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.relevance.ppa.data.Material;
import com.relevance.ppa.data.MaterialMappedObject;
import com.relevance.ppa.data.MaterialObject;
import com.relevance.prism.dao.BaseDao;
import com.relevance.prism.dao.ItemMatchingDao;
import com.relevance.prism.data.DistanceObject;
import com.relevance.prism.data.MatchColumn;
import com.relevance.prism.data.MatchLink;
import com.relevance.prism.data.Query;
import com.relevance.prism.data.Row;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.PrismDbUtil;
import com.relevance.prism.util.Log;

public class PPAItemMasterDao extends BaseDao{
	static String ppaSSEQuery = "select leadtime, currency, incoterm, min_order_qty as minorderquantity, award_start_date as contractstartdate, award_end_date as contractenddate, concat(concat(sse_place,', '),country) as plantdesc, spec as speccode, plantcode as materialid, material_group as materialname, material_group as materialgroup, final_category as finalcategory, supplier_name as supplier, '' as supplierid, sse_price as price, '' as latest,'' as purchasehistory,vendorno as galaxyvendorno ,vendorcity as galaxyvendorcity,'' as materialcreationdate,sse_uom as galaxyuom,'' as active,category from SSE";
	static String ppaBPCSQuery = "SELECT CASE WHEN env = 'ITM' THEN IGTEC ELSE IDRAW END AS speccode, '' as contractenddate, '' as contractstartdate, '' as currency, category as finalcategory, '' as incoterm, '' as latest, '' as leadtime, '' as minorderquantity, plantdesc as plantdesc, stprs as price, matnr as materialid, txtlg as materialname, '' as supplier, '' as supplierid, iioq as price,purchasehistory,'' as galaxyvendorno,'' as galaxyvendorcity,imdcrt as materialcreationdate,'' as galaxyuom,active,'' as category FROM MATERIAL";
	static String ppaPlantMappingQuery = "SELECT distinct plant_code as plantcode,sap_plantcode as sapplantcode,bpcs_plant_code as bpcsplantcode FROM PLANT";


	//E2emfAppUtil appUtil = null;
	Connection con = null;
	Statement stmt = null;

	String emea_db = null;
	String emea_wrk = null;
	String emea_stg = null;

	public PPAItemMasterDao() {
		emea_db = E2emfAppUtil.getAppProperty(E2emfConstants.emeaDB);
	}
	public void truncateMatchingTable() throws AppException{
		try
		{
			con = dbUtil.getPostGresConnection();
			stmt = con.createStatement();
			stmt.executeUpdate("delete from emea_sse_matched_items");
		} catch (SQLException sql) {
			Log.Error("SQLException while Fetching Master Details for PPA Material Matching."
					+ sql.getMessage());
			Log.Error(sql);
			throw new AppException(sql.getMessage(),
					"Error while processing the request.", 1, sql.getCause(),
					true);
		} catch (Exception e) {
			Log.Error(e);
			e.printStackTrace();
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (con != null)
				try {
					con.close();
					// stmt.close();
					// con = null;
					// stmt = null;
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}

		}
	};



	public Material convertToMaterial(Row row)
	{
			Material m = new Material();
			String materialId = row.getRow().get("materialid");
			if (materialId != null && materialId.trim() != "" && !materialId.equalsIgnoreCase("null")){
				materialId = materialId.toLowerCase().trim().replaceFirst("^0+(?!$)", "");
				String materialname = row.getRow().get("materialname");
				if (materialname != null && materialname.trim() != "" && !materialname.equalsIgnoreCase("null"))
					materialname = materialname.toLowerCase().trim();
				String specCode = row.getRow().get("speccode");
				m.setContractEndDate(row.getRow().get("contractenddate"));
				m.setContractStartDate(row.getRow().get("contractstartdate"));
				m.setCurrency(row.getRow().get("currency"));
				m.setCategory(row.getRow().get("category"));
				m.setFinalCategory(row.getRow().get("finalcategory"));
				m.setIncoterm(row.getRow().get("incoterm"));
				m.setLatest(row.getRow().get("latest"));
				m.setLeadTime(row.getRow().get("leadtime"));
				m.setMaterialId(materialId);
				m.setMaterialIdTemp(row.getRow().get("materialid"));
				m.setMaterialNameTemp(row.getRow().get("materialname"));
				m.setMaterialName(materialname);
				m.setMinOrderQuantity(row.getRow().get("minorderquantity"));
				m.setPlant(row.getRow().get("plant"));
				m.setPlantDesc(row.getRow().get("plantdesc"));
				m.setSapPlant(row.getRow().get("sapPlantCode"));
				m.setSpecCode(specCode);
				m.setPrice(Float.parseFloat(row.getRow().get("price")));
				m.setSupplier(row.getRow().get("supplier"));
				m.setSupplierId(row.getRow().get("supplierid"));
				m.setPurchaseHistory(row.getRow().get("purchasehistory"));
				m.setGalaxyVendorNo(row.getRow().get("galaxyvendorno"));
				m.setGalaxyVendorCity(row.getRow().get("galaxyvendorcity"));
				m.setMaterialCreationDate(row.getRow().get("materialcreationdate"));
				m.setGalaxyUOM(row.getRow().get("galaxyuom"));
				m.setActive(row.getRow().get("active"));
				m.setSystem(row.getRow().get("system"));
			}

			return m;
	}

	private List<MaterialMappedObject> convertToMMList(List<MatchLink> linkList)
	{
		Log.Debug("convertToMMList() started...");
		List<MaterialMappedObject> retList = new ArrayList<MaterialMappedObject>();
		for(MatchLink link : linkList)
		{
			if (link.getTgtRows() != null && link.getTgtRows().size() > 0)
			{
				for(Row tgtRow : link.getTgtRows())
				{
					MaterialMappedObject m = new MaterialMappedObject();
					m.setSourceMaterial(convertToMaterial(link.getSrcRow()));
					m.setTargetMaterial(convertToMaterial(tgtRow));
					m.setDistance(link.getDistance());

					retList.add(m);
				}
			}
			else
			{
				MaterialMappedObject m = new MaterialMappedObject();
				m.setSourceMaterial(convertToMaterial(link.getSrcRow()));
				m.setDistance(link.getDistance());
				m.setTargetMaterial(new Material());//empty object
				retList.add(m);
			}

		}
		Log.Debug("convertToMMList() completed");
		return retList;
	}

	private Query getQuery(String queryToExecute,String system,String plant,String sapPlantCode,String plantField)
	{
		Query p = new Query();

		if (plant != null && plant.trim() != "")
		{
			queryToExecute += " where "+plantField+" ='"+plant+ "'";
		}
		p.setQuery(queryToExecute);

		Row defaultValues = new Row();
		defaultValues.getRow().put("plant", plant);
		defaultValues.getRow().put("system", system);
		defaultValues.getRow().put("sapPlantCode", sapPlantCode);

		p.setDefaultValues(defaultValues);

		return p;
	}

	public void findPPAMatches(String plants, String source , String target) throws AppException
	{

		ResultSet rs = null;
		try
		{
			con = dbUtil.getHiveConnection(E2emfConstants.emeaSTG);//emeaSTG

			Log.Debug("Using EMEA HIVE Connection...findPPAMatches()");

			StringBuilder queryToExecute = new StringBuilder(ppaPlantMappingQuery);
			String whereClause = null;
			if (plants != null && plants != "") {
				String[] plantArr=plants.split(",");
				String plantsStr="";
				for(int i=0;i<plantArr.length;i++)
					plantsStr += "'" + plantArr[i] + "',";
				if (!plantsStr.equals("")){
					plantsStr =plantsStr.substring(0,plantsStr.length()-1);
					whereClause = " where bpcs_plant_code in (" + plantsStr + ")";
					queryToExecute.append(whereClause);
				}
			}
			else{
				whereClause = " where bpcs_plant_code is not null";
				queryToExecute.append(whereClause);
			}

			Log.Debug("Plant Query to Execute :" + queryToExecute.toString());
			stmt = con.createStatement();
			//stmt.executeUpdate("delete from emea_sse_matched_items");
			rs = stmt.executeQuery(queryToExecute.toString());

			ArrayList<Query> srcList = new ArrayList<Query>();
			ArrayList<Query> tgtList = new ArrayList<Query>();
			Query sp;
			Query tp;
			if (rs != null)
			{
				while (rs.next())
				{
					if (source.equalsIgnoreCase("sse"))
					{
						sp = getQuery(ppaSSEQuery,source,rs.getString("bpcsplantcode"),rs.getString("sapplantcode"),"sse_env");
						tp = getQuery(ppaBPCSQuery,target,rs.getString("bpcsplantcode"),rs.getString("sapplantcode"),"env");
					} else
					{
						sp = getQuery(ppaBPCSQuery,source,rs.getString("bpcsplantcode"),rs.getString("sapplantcode"),"env");
						tp = getQuery(ppaSSEQuery,target,rs.getString("bpcsplantcode"),rs.getString("sapplantcode"),"sse_env");
					}
					srcList.add(sp);
					tgtList.add(tp);

				}
				rs.close();

				Log.Debug("Plant Queries fetched successfully (No.of plants :" + srcList.size());

				ArrayList<MatchColumn> matchCols = new ArrayList<MatchColumn>();
				MatchColumn m = new MatchColumn();
				m.setName("materialid");
				m.setMatchType(MatchColumn.EXACT_MATCH);
				m.setMatchPriority(100);
				m.setCleanBeforeMatch(true);
				m.setExclusiveMatch(true);//Matched items will not be considered for next cycle of match
				DistanceObject d = ItemMatchingDao.getDistanceObject(1,0, "Exact", "Material Code");
				m.setDefaultDistance(d);
				matchCols.add(m);

				m = new MatchColumn();
				m.setName("speccode");
				m.setMatchType(MatchColumn.EXACT_MATCH);
				m.setMatchPriority(90);
				//m.setCleanBeforeMatch(true);
				d = ItemMatchingDao.getDistanceObject(2,0, "Tentative", "Spec Code");
				m.setDefaultDistance(d);
				matchCols.add(m);

				m = new MatchColumn();
				m.setName("materialname");
				m.setMatchType(MatchColumn.EXACT_MATCH);
				m.setMatchPriority(80);
				//m.setCleanBeforeMatch(true);
				d = ItemMatchingDao.getDistanceObject(3,0, "Tentative", "Exact Description");
				m.setDefaultDistance(d);
				matchCols.add(m);

				m = new MatchColumn();
				m.setName("materialname");
				m.setMatchType(MatchColumn.LEVIN_MATCH);
				m.setMatchPriority(70);
				//m.setCleanBeforeMatch(true);
				d = ItemMatchingDao.getDistanceObject(4,0, "Tentative", "Close Description");
				m.setDefaultDistance(d);
				matchCols.add(m);

				ItemMatchingDao itemMatchingDao = new ItemMatchingDao();
				for (int i = 0; i < srcList.size(); i++)
				{
						List<MatchLink> linkList = itemMatchingDao.findMatches(srcList.get(i), E2emfConstants.emeaWRK, tgtList.get(i), E2emfConstants.emeaWRK, matchCols);
						List<MaterialMappedObject> mList = convertToMMList(linkList);

						pushToDB(mList, source);
				}
			}

			Log.Debug("findPPAMatches() - Completed generation of item matches");
		}
		catch (Exception e) {
			Log.Error("Exception fetching the Details for PPA Material Matching."
					+ source + " \n " + e.getMessage());
			Log.Error(e);
			e.printStackTrace();
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}

		}
	//return null;
	}

	public ArrayList<MaterialMappedObject> itemMasterMapping (String plants,String source ,String target) throws AppException{
		MaterialObject sourceMaterialObject = new MaterialObject();
		MaterialObject targetMaterialObject = new MaterialObject();
		ArrayList<MaterialMappedObject> materialMappedObjectList = new ArrayList<MaterialMappedObject>();
		MaterialMappedObject materialMappedObject = new MaterialMappedObject();
		ResultSet rs = null;
		StringBuilder queryToExecute = null;
		String whereClause = null;
		Levenshtein levenshteinDistance = new Levenshtein();
		try {
			con = dbUtil.getHiveConnection(E2emfConstants.emeaSTG);//emeaSTG
			//con = dbUtil.getPostGresConnection();

			//con = dbUtil.getPostGresConnection();
			Log.Info("Using EMEA HIVE Connection...");

			queryToExecute = new StringBuilder(ppaPlantMappingQuery);
			if (plants != null && plants != "") {
				String[] plantArr=plants.split(",");
				String plantsStr="";
				for(int i=0;i<plantArr.length;i++)
					plantsStr += "'" + plantArr[i] + "',";
				if (!plantsStr.equals("")){
					plantsStr =plantsStr.substring(0,plantsStr.length()-1);
					whereClause = " where bpcs_plant_code in (" + plantsStr + ")";
					queryToExecute.append(whereClause);
				}
			}
			else{
				whereClause = " where bpcs_plant_code is not null";
				queryToExecute.append(whereClause);
			}

			Log.Debug("PPA Query to Execute :" + queryToExecute.toString());
			stmt = con.createStatement();
			//stmt.executeUpdate("delete from emea_sse_matched_items");
			rs = stmt.executeQuery(queryToExecute.toString());

			if (rs != null) {
				while (rs.next()) {
						materialMappedObjectList = new ArrayList<MaterialMappedObject>();
						if (source.equalsIgnoreCase("sse")){
							sourceMaterialObject = getPPAMaterialList(ppaSSEQuery,source,rs.getString("bpcsplantcode"),rs.getString("sapplantcode"),"sse_env");
							targetMaterialObject = getPPAMaterialList(ppaBPCSQuery,target,rs.getString("bpcsplantcode"),rs.getString("sapplantcode"),"env");
						} else{
							sourceMaterialObject = getPPAMaterialList(ppaBPCSQuery,source,rs.getString("bpcsplantcode"),rs.getString("sapplantcode"),"env");
							targetMaterialObject = getPPAMaterialList(ppaSSEQuery,target,rs.getString("bpcsplantcode"),rs.getString("sapplantcode"),"sse_env");
						}
						// Material Id Match
						Set<String> sourceMaterialMatched = new HashSet<String> ();
						for (Map.Entry<String, Material> sourceMaterial : sourceMaterialObject.getMaterialObject().entrySet())
						{
							String sourceMaterialID = sourceMaterial.getKey();
							Material sourceMaterialDetails = sourceMaterial.getValue();
							if (targetMaterialObject.getMaterialObject().containsKey(sourceMaterialID) && sourceMaterialID != null && sourceMaterialID.trim() != ""){
								materialMappedObject = new MaterialMappedObject();
								materialMappedObject.setSourceMaterial(sourceMaterialDetails);
								materialMappedObject.setTargetMaterial(targetMaterialObject.getMaterialObject().get(sourceMaterialID));
								materialMappedObject.setDistance(ItemMatchingDao.getDistanceObject(1,0d, "Exact", "Material Code"));
								sourceMaterialMatched.add(sourceMaterialID);
								materialMappedObjectList.add(materialMappedObject);
							}
						}
						//Removing matched set.
						targetMaterialObject.getMaterialObject().keySet().removeAll(sourceMaterialMatched);
						sourceMaterialObject.getMaterialObject().keySet().removeAll(sourceMaterialMatched);
						sourceMaterialObject.getSpecCodeHashmap().keySet().removeAll(sourceMaterialMatched);
						sourceMaterialObject.getMaterialIdHashMap().keySet().removeAll(sourceMaterialMatched);
						targetMaterialObject.getSpecCodeHashmap().keySet().removeAll(sourceMaterialMatched);
						targetMaterialObject.getMaterialIdHashMap().keySet().removeAll(sourceMaterialMatched);

						HashMap<String,String> targetSpecCodeHashmap = reverse(targetMaterialObject.getSpecCodeHashmap());
						//Spec Code Match
						sourceMaterialMatched = new HashSet<String> ();
						for (Map.Entry<String, Material> sourceMaterial : sourceMaterialObject.getMaterialObject().entrySet())
						{
							String sourceMaterialID = sourceMaterial.getKey();
							Material sourceMaterialDetails = sourceMaterial.getValue();
							if (sourceMaterialDetails.getSpecCode() != null && sourceMaterialDetails.getSpecCode().trim()!= "" && targetSpecCodeHashmap.containsKey(sourceMaterialDetails.getSpecCode()) && targetMaterialObject.getMaterialObject().get(targetSpecCodeHashmap.get(sourceMaterialDetails.getSpecCode())) != null ){
								materialMappedObject = new MaterialMappedObject();
								materialMappedObject.setSourceMaterial(sourceMaterialDetails);
								materialMappedObject.setTargetMaterial(targetMaterialObject.getMaterialObject().get(targetSpecCodeHashmap.get(sourceMaterialDetails.getSpecCode())));
								materialMappedObject.setDistance(ItemMatchingDao.getDistanceObject(2,0,"Tentative", "Spec Code"));
								sourceMaterialMatched.add(sourceMaterialID);
								materialMappedObjectList.add(materialMappedObject);
							}
						}

						targetMaterialObject.getMaterialObject().keySet().removeAll(sourceMaterialMatched);
						sourceMaterialObject.getMaterialObject().keySet().removeAll(sourceMaterialMatched);
						sourceMaterialObject.getSpecCodeHashmap().keySet().removeAll(sourceMaterialMatched);
						sourceMaterialObject.getMaterialIdHashMap().keySet().removeAll(sourceMaterialMatched);
						targetMaterialObject.getSpecCodeHashmap().keySet().removeAll(sourceMaterialMatched);
						targetMaterialObject.getMaterialIdHashMap().keySet().removeAll(sourceMaterialMatched);

					HashMap<String,String> targetMaterialNameHashmap = reverse(targetMaterialObject.getMaterialIdHashMap());
						//Exact material name match
					sourceMaterialMatched = new HashSet<String> ();
						for (Map.Entry<String, Material> sourceMaterial : sourceMaterialObject.getMaterialObject().entrySet())
						{
							String sourceMaterialID = sourceMaterial.getKey();
							Material sourceMaterialDetails = sourceMaterial.getValue();
							if (sourceMaterialDetails.getMaterialName()!= null && sourceMaterialDetails.getMaterialName().trim() != "" && targetMaterialNameHashmap.containsKey(sourceMaterialDetails.getMaterialName()) && targetMaterialObject.getMaterialObject().get(targetMaterialNameHashmap.get(sourceMaterialDetails.getMaterialName())) != null){
								materialMappedObject = new MaterialMappedObject();
								materialMappedObject.setSourceMaterial(sourceMaterialDetails);
								materialMappedObject.setTargetMaterial(targetMaterialObject.getMaterialObject().get(targetMaterialNameHashmap.get(sourceMaterialDetails.getMaterialName())));
								materialMappedObject.setDistance(ItemMatchingDao.getDistanceObject(3,0, "Tentative", "Exact Material Desc"));
								materialMappedObjectList.add(materialMappedObject);
								sourceMaterialMatched.add(sourceMaterialID);
							}
						};

						targetMaterialObject.getMaterialObject().keySet().removeAll(sourceMaterialMatched);
						sourceMaterialObject.getMaterialObject().keySet().removeAll(sourceMaterialMatched);
						sourceMaterialObject.getSpecCodeHashmap().keySet().removeAll(sourceMaterialMatched);
						sourceMaterialObject.getMaterialIdHashMap().keySet().removeAll(sourceMaterialMatched);
						targetMaterialObject.getSpecCodeHashmap().keySet().removeAll(sourceMaterialMatched);
						targetMaterialObject.getMaterialIdHashMap().keySet().removeAll(sourceMaterialMatched);

						for (Map.Entry<String, String> sourceMaterial : sourceMaterialObject.getMaterialIdHashMap().entrySet())
						{
							double objLVDistance  = 1000;
							String targetMaterialID = null;
							String sourceMaterialID = sourceMaterial.getKey();
							String sourcematerialName = sourceMaterial.getValue();
							for (Map.Entry<String, String> targetMaterial : targetMaterialNameHashmap.entrySet())
							{
								String targetMaterialName = targetMaterial.getKey();
								double tempLvDistance  = levenshteinDistance.distance(sourcematerialName, targetMaterialName);
								if  (objLVDistance > tempLvDistance && targetMaterialName!= null && targetMaterialName.trim()!= ""){
									targetMaterialID =  targetMaterial.getValue();
									objLVDistance = tempLvDistance;
								}
							}
							materialMappedObject = new MaterialMappedObject();
							materialMappedObject.setSourceMaterial(sourceMaterialObject.getMaterialObject().get(sourceMaterialID));
							if (objLVDistance < 7 && targetMaterialID != null && targetMaterialObject.getMaterialObject().get(targetMaterialID) != null){
								materialMappedObject.setTargetMaterial(targetMaterialObject.getMaterialObject().get(targetMaterialID));
								materialMappedObject.setDistance(ItemMatchingDao.getDistanceObject(4,objLVDistance, "Tentative", "Close Description"));
							}else{
								materialMappedObject.setDistance(ItemMatchingDao.getDistanceObject(10,0, "No Match", "No Match"));
							}
							materialMappedObjectList.add(materialMappedObject);
						};
						pushToDB(materialMappedObjectList,source);
					}
				}
			}catch (Exception e) {
				Log.Error("Exception fetching the Details for PPA Material Matching."
						+ source + " \n " + e.getMessage());
				Log.Error(e);
				e.printStackTrace();
				if (e instanceof NullPointerException) {
					throw new AppException("NullPointerException",
							e.getStackTrace(),
							"Error while processing the request.", 3, e.getCause(),
							true);
				}
				throw new AppException(e.getMessage(),
						"Error while processing the request.", 2, e.getCause(),
						true);
			} finally {
				if (con != null)
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
						throw new AppException(e.getMessage(),
								"Error while processing the request.", 1,
								e.getCause(), true);
					}

			}
		return null;
	}
	public static <K,V> HashMap<V,K> reverse(Map<K,V> map) {
	    HashMap<V,K> rev = new HashMap<V, K>();
	    for(Map.Entry<K,V> entry : map.entrySet())
	        rev.put(entry.getValue(), entry.getKey());
	    return rev;
	}

	private MaterialObject getPPAMaterialList(String queryToExecute,String system,String plant,String sapPlantCode,String plantField) throws AppException{
		Connection conMaterial = null;
		Statement stmtMaterial = null;
		MaterialObject materialObject = new MaterialObject();
		//ArrayList<HashMap<String,Material>> ppaMaterialList = new ArrayList<HashMap<String,Material>>();
		try{
			conMaterial = dbUtil.getHiveConnection(E2emfConstants.emeaWRK);//emeaWRK
			//conMaterial = dbUtil.getPostGresConnection();
			stmtMaterial = conMaterial.createStatement();
			ResultSet rs = null;
			if (plant != null && plant.trim() != ""){
				queryToExecute += " where "+plantField+" ='"+plant+ "'";
			}

			rs = stmtMaterial.executeQuery(queryToExecute.toString());
			if (rs != null) {
				HashMap<String,Material> ppaMaterialHashMap = null;
				HashMap<String,String> ppaMaterialNameHashMap = null;
				HashMap<String,String> ppaSpecCodeHashMap = null;
				ppaMaterialHashMap = new HashMap<String,Material>();
				ppaMaterialNameHashMap = new HashMap<String,String>();
				ppaSpecCodeHashMap = new HashMap<String,String>();
				while (rs.next()) {
					Material ppaMaterial = new Material();
					String materialId = rs.getString("materialid");
					if (materialId != null && materialId.trim() != "" && !materialId.equalsIgnoreCase("null")){
						materialId = materialId.toLowerCase().trim().replaceFirst("^0+(?!$)", "");
						String materialname = rs.getString("materialname");
						if (materialname != null && materialname.trim() != "" && !materialname.equalsIgnoreCase("null"))
							materialname = materialname.toLowerCase().trim();
						String specCode = rs.getString("speccode");
						ppaMaterial.setContractEndDate(rs.getString("contractenddate"));
						ppaMaterial.setContractStartDate(rs.getString("contractstartdate"));
						ppaMaterial.setCurrency(rs.getString("currency"));
						ppaMaterial.setCategory(rs.getString("category"));
						ppaMaterial.setFinalCategory(rs.getString("finalcategory"));
						ppaMaterial.setIncoterm(rs.getString("incoterm"));
						ppaMaterial.setLatest(rs.getString("latest"));
						ppaMaterial.setLeadTime(rs.getString("leadtime"));
						ppaMaterial.setMaterialId(materialId);
						ppaMaterial.setMaterialIdTemp(rs.getString("materialid"));
						ppaMaterial.setMaterialNameTemp(rs.getString("materialname"));
						ppaMaterial.setMaterialName(materialname);
						ppaMaterial.setMinOrderQuantity(rs.getString("minorderquantity"));
						ppaMaterial.setPlant(plant);
						ppaMaterial.setPlantDesc(rs.getString("plantdesc"));
						ppaMaterial.setSapPlant(sapPlantCode);
						ppaMaterial.setSpecCode(specCode);
						ppaMaterial.setPrice(rs.getFloat("price"));
						ppaMaterial.setSupplier(rs.getString("supplier"));
						ppaMaterial.setSupplierId(rs.getString("supplierid"));
						ppaMaterial.setPurchaseHistory(rs.getString("purchasehistory"));
						ppaMaterial.setGalaxyVendorNo(rs.getString("galaxyvendorno"));
						ppaMaterial.setGalaxyVendorCity(rs.getString("galaxyvendorcity"));
						ppaMaterial.setMaterialCreationDate(rs.getString("materialcreationdate"));
						ppaMaterial.setGalaxyUOM(rs.getString("galaxyuom"));
						ppaMaterial.setActive(rs.getString("active"));
						ppaMaterial.setSystem(system);
						ppaMaterialHashMap.put(materialId, ppaMaterial);
						if (materialname != null && materialname.trim() != "")
							ppaMaterialNameHashMap.put(materialId,materialname);
						if (specCode != null && specCode.trim() != "" && !specCode.equalsIgnoreCase("null"))
							ppaSpecCodeHashMap.put(materialId,specCode.trim());
					}
				}
				materialObject.setMaterialObject(ppaMaterialHashMap);
				materialObject.setMaterialIdHashMap(ppaMaterialNameHashMap);
				materialObject.setSpecCodeHashmap(ppaSpecCodeHashMap);
			}
		} catch (SQLException sql) {
			Log.Error("SQLException while Fetching Master Details for PPA Material Matching."
					+ sql.getStackTrace());
			Log.Error(sql);
			throw new AppException(sql.getMessage(),
					"Error while processing the request.", 1, sql.getCause(),
					true);
		} catch (Exception e) {
			Log.Error("Exception fetching the Details for PPA Material Matching. "
					+ system + " \n " + e.getMessage());
			Log.Error(e);
			e.printStackTrace();
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (con != null)
				try {
					conMaterial.close();
					// stmt.close();
					// con = null;
					// stmt = null;
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}

		}
		return materialObject;
	}

	private void pushToDB(List<MaterialMappedObject> materialMappedObjectList,String source) throws AppException{
		Connection conInsert = null;
		//Statement stmtInsert = null;
		PreparedStatement ps = null;

		Log.Debug("pushToDB() started...");
		long recordCount = 0;
		long batchCount = 0;
		long batchSize = 1000;

		if (materialMappedObjectList == null)
		{
			Log.Debug("pushToDB() Insert list is empty...");
			return;
		}
		else
		{
			Log.Debug("pushToDB() list size..." + materialMappedObjectList.size());
		}
		try{
			conInsert = dbUtil.getPostGresConnection();
			//stmtInsert = conInsert.createStatement();

			String insertQuery = "INSERT INTO public.emea_sse_matched_items( sourcesystem, sourceplant, sourcesapplant, sourceplantdesc, sourcespeccode, sourcematerialid, sourcematerialname, subcategory, "
					+"sourcecontractstartdate, sourcecontractenddate, category, sourcesupplier, contractstatus, sourceprice, sourcecurrency, sourceincoterm, sourceleadtime, sourceminorderquantity, "
					+"targetsystem, targetplant, targetsapplant, targetspeccode, targetmaterialid, targetmaterialname, targetsupplierid, targetsupplier, targetprice, targetpricebyprice, "
					+ "type, typedesc, typecategory, lvdistnacedesc, lvdistnacesupplier, pricediff, pricediffbyprice, weightage, direction, purchasehistory, galaxyvendorno, galaxyvendorcity, materialcreationdate, galaxyuom,active)"
					+"VALUES (?,?,?,?,?,?,?,?,?,?,"
					+"?,?,?,?,?,?,?,?,?,?,"
					+"?,?,?,?,?,?,?,?,?,?,"
					+"?,?,?,?,?,?,?,?,?,?,"
					+"?,?,?)";
			ps = conInsert.prepareStatement(insertQuery);

			for(MaterialMappedObject materialMappedObject: materialMappedObjectList){
				Material sourcePPAMaterial	= new Material();
				Material targetPPAMaterial	= new Material();
				if (materialMappedObject.getSourceMaterial() != null)
					sourcePPAMaterial	= materialMappedObject.getSourceMaterial();
				if (materialMappedObject.getTargetMaterial() != null)
					targetPPAMaterial	= materialMappedObject.getTargetMaterial();
				DistanceObject distanceObject = materialMappedObject.getDistance();
				String subCategory = null;
				String category = null;
				String contractStatus = null;
				String direction = "";
				String purchaseHistory = null;
				String galaxyvendorno = null;
				String galaxyvendorcity = null;
				String materialcreationdate = null;
				String galaxyuom = null;
				String active = null;
				if (source.equalsIgnoreCase("bpcs")){
					direction ="planttodm";
					category = targetPPAMaterial.getCategory();
					subCategory = targetPPAMaterial.getFinalCategory();
					purchaseHistory = sourcePPAMaterial.getPurchaseHistory();
					galaxyvendorno = targetPPAMaterial.getGalaxyVendorNo();
					galaxyvendorcity = targetPPAMaterial.getGalaxyVendorCity();
					materialcreationdate = sourcePPAMaterial.getMaterialCreationDate();
					galaxyuom = targetPPAMaterial.getGalaxyUOM();
					active = sourcePPAMaterial.getActive();
				}else{
					direction = "dmtoplant";
					category = sourcePPAMaterial.getCategory();
					subCategory = sourcePPAMaterial.getFinalCategory();
					purchaseHistory = targetPPAMaterial.getPurchaseHistory();
					galaxyvendorno = sourcePPAMaterial.getGalaxyVendorNo();
					galaxyvendorcity =  sourcePPAMaterial.getGalaxyVendorCity();
					materialcreationdate = targetPPAMaterial.getMaterialCreationDate();
					galaxyuom = sourcePPAMaterial.getGalaxyUOM();
					active = targetPPAMaterial.getActive();
				}

				ps.setString(1,sourcePPAMaterial.getSystem());
				ps.setString(2,sourcePPAMaterial.getPlant());
				ps.setString(3,sourcePPAMaterial.getSapPlant());
				ps.setString(4,sourcePPAMaterial.getPlantDesc());
				ps.setString(5,sourcePPAMaterial.getSpecCode());
				ps.setString(6,sourcePPAMaterial.getMaterialIdTemp());
				ps.setString(7,sourcePPAMaterial.getMaterialNameTemp());
				ps.setString(8,subCategory);
				ps.setString(9,sourcePPAMaterial.getContractStartDate());
				ps.setString(10,sourcePPAMaterial.getContractEndDate());
				ps.setString(11,category);
				ps.setString(12,sourcePPAMaterial.getSupplier());
				ps.setString(13,contractStatus);
				ps.setDouble(14,sourcePPAMaterial.getPrice());
				ps.setString(15,sourcePPAMaterial.getCurrency());
				ps.setString(16,sourcePPAMaterial.getIncoterm());
				ps.setString(17,sourcePPAMaterial.getLeadTime());
				ps.setString(18,sourcePPAMaterial.getMinOrderQuantity());
				ps.setString(19,targetPPAMaterial.getSystem());
				ps.setString(20,targetPPAMaterial.getPlant());
				ps.setString(21,targetPPAMaterial.getSapPlant());
				ps.setString(22,targetPPAMaterial.getSpecCode());
				ps.setString(23,targetPPAMaterial.getMaterialIdTemp());
				ps.setString(24,targetPPAMaterial.getMaterialNameTemp());
				ps.setString(25,targetPPAMaterial.getSupplierId());
				ps.setString(26,targetPPAMaterial.getSupplier());
				ps.setDouble(27,targetPPAMaterial.getPrice());
				ps.setDouble(28,targetPPAMaterial.getPrice());
				ps.setInt(29,distanceObject.getType());
				ps.setString(30,distanceObject.getTypeDesc());
				ps.setString(31,distanceObject.getTypeCategory());
				ps.setDouble(32,distanceObject.getDistance());
				ps.setString(33,distanceObject.getDistnaceSupplier());
				ps.setDouble(34,distanceObject.getPriceDiff());
				ps.setDouble(35,distanceObject.getPriceDiff());
				ps.setInt(36,distanceObject.getWeightage());
				ps.setString(37,direction);
				ps.setString(38,purchaseHistory);
				ps.setString(39,galaxyvendorno);
				ps.setString(40,galaxyvendorcity);
				ps.setString(41,materialcreationdate);
				ps.setString(42,galaxyuom);
				ps.setString(43,active);

				ps.addBatch();
				recordCount++;



				try{
			/*String insertQuery = "INSERT INTO public.emea_sse_matched_items( sourcesystem, sourceplant, sourcesapplant, sourceplantdesc, sourcespeccode, sourcematerialid, sourcematerialname, subcategory, "
					+"sourcecontractstartdate, sourcecontractenddate, category, sourcesupplier, contractstatus, sourceprice, sourcecurrency, sourceincoterm, sourceleadtime, sourceminorderquantity, "
					+"targetsystem, targetplant, targetsapplant, targetspeccode, targetmaterialid, targetmaterialname, targetsupplierid, targetsupplier, targetprice, targetpricebyprice, "
					+ "type, typedesc, typecategory, lvdistnacedesc, lvdistnacesupplier, pricediff, pricediffbyprice, weightage, direction, purchasehistory, galaxyvendorno, galaxyvendorcity, materialcreationdate, galaxyuom,active)"
					+"VALUES ('"
					+ sourcePPAMaterial.getSystem() + "','" +sourcePPAMaterial.getPlant() + "','"+ sourcePPAMaterial.getSapPlant()+ "','"+ sourcePPAMaterial.getPlantDesc() + "','"
					+ sourcePPAMaterial.getSpecCode() + "','" +sourcePPAMaterial.getMaterialIdTemp() + "','"+ sourcePPAMaterial.getMaterialNameTemp()+ "','"+ subCategory + "','"
					+ sourcePPAMaterial.getContractStartDate() + "','" +sourcePPAMaterial.getContractEndDate() + "','"+ category + "','"+ sourcePPAMaterial.getSupplier() + "','"
					+ contractStatus + "'," + sourcePPAMaterial.getPrice() + ",'"+sourcePPAMaterial.getCurrency()+"','"+ sourcePPAMaterial.getIncoterm() + "','"
					+ sourcePPAMaterial.getLeadTime()+ "','" +sourcePPAMaterial.getMinOrderQuantity() + "','"
					+ targetPPAMaterial.getSystem() + "','" +targetPPAMaterial.getPlant() + "','"+ targetPPAMaterial.getSapPlant()+ "','"+ targetPPAMaterial.getSpecCode() + "','"
					+ targetPPAMaterial.getMaterialIdTemp() + "','"+ targetPPAMaterial.getMaterialNameTemp()+ "','"+targetPPAMaterial.getSupplierId() + "','" +targetPPAMaterial.getSupplier() + "',"
					+ targetPPAMaterial.getPrice() + ",'"+ targetPPAMaterial.getPrice()+ "','"
					+ distanceObject.getType() + "','" + distanceObject.getTypeDesc() + "','" + distanceObject.getTypeCategory() + "','" + distanceObject.getDistance() + "','"+ distanceObject.getDistnaceSupplier() + "','"
					+ distanceObject.getPriceDiff() + "','" + distanceObject.getPriceDiff() + "'," + distanceObject.getWeightage() + ",'" + direction + "','"+purchaseHistory + "','"
					+ galaxyvendorno + "','" + galaxyvendorcity + "','" + materialcreationdate + "','" + galaxyuom + "','"+ active
					+ "')";
					stmtInsert.executeUpdate(insertQuery);*/

					if (recordCount % batchSize == 0 || recordCount == materialMappedObjectList.size())
					{
						ps.executeBatch();
						batchCount++;
						Log.Debug("Executed batch" + batchCount);
					}

				}catch (SQLException sql) {
					Log.Debug("Error while executing batch - batchCount " + batchCount + " - " + sql.getMessage());
					continue;
				}
			}

			Log.Debug("pushToDB() completed");
		}catch (SQLException sql) {
			Log.Error("SQLException while inserting Roles in table."
					+ sql.getStackTrace());
			Log.Error(sql);
			//String message = sql.getMessage();
			//String newmessage = message.replaceAll("\"", "\'").replaceAll(
			//		"\n", ".");
			/*throw new AppException(newmessage,
					"Error while processing the request.", 1,
					sql.getCause(), true);*/

		} catch (Exception e) {
			Log.Error("Exception while inserting Roles in table."
					+ e.getStackTrace());
			Log.Error(e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3,
						e.getCause(), true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll(
					"\n", ".");
			throw new AppException(newmessage,
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (conInsert != null) {
				try {
					conInsert.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}
			}
		}
	};
}
