package com.relevance.ppa.rest;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.relevance.ppa.data.TCurrView;
import com.relevance.ppa.service.TCurrProfileService;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.Log;
@Path("/tcurr")
public class TCurrProfileResource {
	@Context
	ServletContext context;

	public TCurrProfileResource(@Context ServletContext value) {
		this.context = value;
		//System.out.println("Conext:" + this.context);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getcurrresults")
	public ArrayList<TCurrView> getResults() throws AppException, SQLException{
		try{
			TCurrProfileService tcurfProfileService = new TCurrProfileService();
			ArrayList<TCurrView> tcurfViewList = tcurfProfileService.getResults();
			return tcurfViewList;
			}catch(Exception e){
				Log.Error("Exception fetching the Details for EMEA TCURR Profile. \n " + e.getMessage());
				Log.Error(e);
				e.printStackTrace();
				if(e instanceof NullPointerException){
					throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
		}
	}
}
