var defaultApps = function(){
	var init =function(){
		$.ajax({
			type : "POST",
			url : "rest/user/getroles",
			context : document.body
		}).done(function(response) {
			$("#defaultApps").empty();
			var appCounter = 0;
			var app = "";
			for (var i=0;i<response.length;i++){
				if (response[i].displayOrder && response[i].displayOrder>0){
					appCounter++;
					app = response[i].name;
					var logoDiv;
					if (response[i].icon && response[i].icon!=="")
						logoDiv = $("<i/>").addClass(response[i].icon);
					else
						logoDiv = $("<img/>").attr("src",response[i].logo);
					$("#defaultApps").append($("<div/>").addClass("widget_tile "+ response[i].widgetClass+" "+response[i].widgetColor).data("app",response[i].name)
						.append($("<a/>").addClass("widget_content").attr("href","login?app="+response[i].name)
							.append($("<div/>").addClass("main")
								.append($("<span/>").addClass("image-helper"))
								.append(logoDiv)
								.append($("<span/>").html(response[i].displayName)))
					));
				}
			}
			if (appCounter === 1)
				location.href = "login?app="+app;
			else if (appCounter === 0)
				$("#defaultApps").html(
				'<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" href="#">×</a><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No apps configured, please contact system admin.</p></div>');
		});
	};
	init();
};
defaultApps();