'use strict';
pageSetUp();
var imageCluster;
var poProfileInstance = function() {
	imageCluster = new imageClusterObject();
	//imageCluster.Load();
};

var imageClusterObject = function() {
	this.Load = function(obj) {
		var request = {
			"filters":"image_class='IMG_EXP_CODE_SYMBOL_0'",
			"table":"image_cluster",
			"columns":"full_name, image_file, path, fi, a, b, c, d, e, image_group, image_class",
			"source":"postgreSQL",
			"database":"AnalysisDBPostGres"
		};
		callAjaxService("getData", callbackSuccessPOProfile, callBackFailure,
				request, "POST");
	};
	
	var callbackSuccessPOProfile = function(response){
		enableLoading();
		console.log(response);
		$(".superbox").remove();
		var superBoxList = $("<div/>").addClass("superbox col-sm-12");
		var data = response.data;
		var clusterImagePath = "";
			if (lookUpTable.get("ClusterImagePath") != null)
				clusterImagePath = lookUpTable.get("ClusterImagePath");
		for (var i=0; i< data.length; i++){
			superBoxList.append($("<div/>").addClass("superbox-list").css({"width":"13.9%","border":"1px Solid #ccc","margin":"2px"}).append($("<img/>").addClass("superbox-img").attr({"src":clusterImagePath + data[i].full_name,"data-img":clusterImagePath + data[i].full_name})))
		};
		$("#widget-grid").append(superBoxList);
		$('.superbox').SuperBox();
	};
	var onFilterSelection = function(){
		var filter =" 1= 1 ";
		var imageGroup = $("#image_group").select2('val');
		var imageClass = $("#image_class").select2('val');
		if (imageGroup && imageGroup.length > 0){
			filter += " AND image_group in ('" + imageGroup.join("','") +"')"
		}
		if (imageGroup && imageClass.length > 0){
			filter += " AND image_class in ('" + imageClass.join("','") +"')"
		}
		if (imageGroup.length > 0 || imageClass.length > 0){
			var request = {
				"filters":filter,
				"table":"image_cluster",
				"columns":"full_name, image_file, path, fi, a, b, c, d, e, image_group, image_class",
				"source":"postgreSQL",
				"database":"AnalysisDBPostGres"
			};
			callAjaxService("getData", callbackSuccessPOProfile, callBackFailure,
					request, "POST");
		}
	};
	constructSelect2("#image_group","select distinct image_group,'' from image_cluster where lower(image_group) like '%<Q>%'",true,null,null,onFilterSelection,"Select Group","postgreSQL","E2EMFPostGres");
	constructSelect2("#image_class","select distinct image_class,'' from image_cluster where lower(image_class) like '%<Q>%'",true,null,null,onFilterSelection,"Select Class","postgreSQL","E2EMFPostGres");
};
poProfileInstance();