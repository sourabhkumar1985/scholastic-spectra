pageSetUp();
$("#searchterm").focus();
var searchTokenAnalysis;

var searchTokenAnalysisInstance =  function(){
	searchTokenAnalysis = new searchTokenAnalysisObj();
};
var searchTokenAnalysisObj = function(){
	var THIS = this;
	$("#searchForm").submit(function(e) {
	    e.preventDefault();
	});
	$("[id*=TokenAnalysiscount]").html(0);
	$("#search").click(function () { searchTokenAnalysis.Load();});
    $("#entities li").unbind('click').bind('click', function(){searchTokenAnalysis.entitySearch(this);});
    var displayFields = new Array("keywords","facet","source","rank","weight");
/*    var ekpoFields = new Array("poNum","material","matNum","itemNum");
    var vbapFields = new Array("soNum","itemNum","matNum","material","netVal","ordQty");
    var ebknFields = new Array("prNum","soNum","ordNum");
    var ebanFields = new Array("prNum","crtBy","stext","matNum");
    var kna1Fields = new Array("custNum","name","street","city","telephone");
    var knb1Fields = new Array("id","output","field14","field17","field48");
    var lfa1Fields = new Array("vendNum","name","city","street","telephone");
    var maktFields = new Array("matNum","desc");
    var maraFields = new Array("matNum","group","type");
    var marcFields = new Array("id","output");
    var tableColumns = {
    		"ebkn" : "banfn",
    		"kna1":"kunnr",
    		"lfa1":"lifnr",
    		"mara":"matnr",
    		"makt":"matnr",
    		"eban":"banfn"
    };*/
    THIS.datasource = "SAP";
    THIS.facet = "ekko";
    this.Load = function(){
    	
    	var tables = $.fn.dataTable.fnTables();
    	$("[id*=TokenAnalysiscount]").html(0);
    	$(tables).each(function () {
    	    $(this).dataTable().
    	    clear();
    	});
    	$(".dataTable tbody").html('<tr class="odd"><td valign="top" colspan="5" class="dataTables_empty">No data available in table</td></tr>');
    	enableLoading();
    	THIS.key = $("#searchterm").val();
    	$.ajaxQ.abortAll();
    	$("[id*=count]").html(0);
    	
    	$(".facets-tabs").show();
    	$(".searchkey").empty();
        $(".searchkey").append(THIS.key);
        $(".totalSearchCount").html('');
        $("#wid-id-data").removeClass("hide");
	    var request = {
	    		"key" : THIS.key 
	    };
	    callAjaxService("tokenAnalysisSearchService", callBackSearchResult,callBackFailure,request,"POST");
    };
    
    var callBackSearchResult = function(response){
    	disableLoading();
    	if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
    	$("#wid-id-20, #resultGrid").show();
    	$.each(response, function (key, data) {
    		key = key.toLowerCase();
    		$("#"+key+"TokenAnalysiscount").html(data);
    	});
    	if ($("#"+THIS.facet+"TokenAnalysiscount") != 0){
	    	var request = {
	    		"key": THIS.key,
	    		"facet": THIS.facet.toUpperCase()
	    	};
	    	enableLoading();
	    	callAjaxService("tokenAnalysisSearchService", callBackEntitySearchResult,callBackFailure,request,"POST");
    	}
    };
    this.entitySearch = function(obj){
    	THIS.datasource = $("#myTab").find("li.active").data("source");
    	THIS.facet = $(obj).data("facet");
    	var count = $("#"+THIS.facet+"TokenAnalysiscount").html();
    	if (count !="0"){
	    	enableLoading();
	    	var request = {
	    			"key": THIS.key,
	    			"facet": THIS.facet.toUpperCase()
	    			
	    	};
	    	callAjaxService("tokenAnalysisSearchService", callBackEntitySearchResult,callBackFailure,request,"POST");
    	} else {
    		$("#"+THIS.facet+"TokenAnalysisTable")
    		.dataTable({
    			"data" : null,
    			"destroy" : true,
    			"language" : {
					"zeroRecords" : "No records to display"
				},
				"filter" : false
    		});
    	}
    };
    
    var callBackEntitySearchResult= function(response){
    	disableLoading();
    	if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
    	try{
	    	//var coloum_config = eval(THIS.facet+"Fields");
	    	var coloum_config = displayFields;
	    	var colosFunction = [];
			for ( var i = 0, idx = coloum_config.length; i < idx; i++) {
				var fun = new Function('oObj', 'return oObj.aData.'
						+ coloum_config[i].trim());
				colosFunction.push({
					"data" :  coloum_config[i].trim(),
					"defaultContent" : "",
					"sortable" : true
				});
			}
			var facetTable = $("#"+THIS.facet+"TokenAnalysisTable")
			.dataTable(
					{
						"language" : {
							"zeroRecords" : "No records to display"
						},
						"dom" : "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'><'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
						"filter" : false,
						"info" : true,
						"data" : response,
						"tableTools" : {
							"aButtons" : [ {
								"sExtends" : "collection",
								"sButtonText" : 'Export <span class="caret" />',
								"aButtons" : [ "csv", "pdf" ]
							} ],
							"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
						},
						"rowCallback" : function(nRow, aData,
								iDisplayIndex, iDisplayIndexFull) {
							console.log(aData);
							$(nRow).html("");
							for ( var i = 0; i < coloum_config.length; i++) {
								if (i == 0) {
									$(nRow)
											.append(
													$("<td/>")
															.html(
																	$(
																			"<a/>")
																			.attr(
																					{
																						
																						"onClick" : "javascript:StoreTokenAnalysisValues('"
																								+ aData[coloum_config[i]]
																								+ "')"})
																			.html(
																					aData[coloum_config[i]])));
								} else if (i==3){
									if (aData[i] == "1")
										$(nRow).append(
												$("<td/>").html("Master"));
									else 
										$(nRow).append(
												$("<td/>").html("Transaction"));
								} 
								else {
									$(nRow).append(
											$("<td/>").html(aData[coloum_config[i]]));
								}
							}
						},
						"destroy" : true,
						"columns" : colosFunction
					});
			/*if (THIS.facet != "ekko" &&  THIS.facet != "ekpo" && THIS.facet != "vbap")
				addTableClickListenr("#"+THIS.facet+"Table", facetTable, THIS.facet, [tableColumns[THIS.facet]], THIS.facet.toUpperCase());*/
    	} catch(err){
    		
    	}
    };
};
function StoreTokenAnalysisValues(key) {
	if (!TokenAnalysisSearchValues)
		TokenAnalysisSearchValues = {};
	TokenAnalysisSearchValues["key"] =key;
	location.href='#TokenAnalysisDiagram';
};
searchTokenAnalysisInstance();