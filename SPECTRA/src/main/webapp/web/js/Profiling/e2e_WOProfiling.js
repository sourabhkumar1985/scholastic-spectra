'use strict';
pageSetUp();
var WOProfiling;
var plantProfileInstance = function() {
	WOProfiling = new woProfilingObject();
	WOProfiling.Load();
};

var woProfilingObject = function() {
	var THIS = this;
	var dateFormat = d3.timeFormat("%m/%d/%Y");
	var numberFormat = d3.format(",f");
	THIS.type == "materialCount";
	constructTabs(THIS);
	this.Load = function(obj) {
		enableLoading();
		var source = $(obj).data("source");
		THIS.source = source || $("#myTab").find("li.active").data("source");
		var request = {
				
			"source" : THIS.source,
			"viewType" : "default"
		};
		callAjaxService("woProfiling", callbackSuccessWOProfile, callBackFailure,
				request, "POST");
		
	};
	
	this.loadViewType = function(viewType){
		enableLoading();
		THIS.type = viewType;
		setTimeout(function() { drawChart(); disableLoading();}, 50);
		
	};

	var callbackSuccessWOProfile = function(data) {
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		if (data != undefined && data != null && data["aggregatedWorkOrderList"] != undefined && data["aggregatedWorkOrderList"] != null && data["aggregatedWorkOrderList"].length > 0) {
			THIS.data = data["aggregatedWorkOrderList"];
			drawChart();
		}
		disableLoading();
	};
	var drawChart = function(){
		var data = THIS.data;
			data.forEach(function(d) {
				d.dd = dateFormat.parse(d.actualreleasedate);
				d.month = d3.time.month(d.dd);
				d.wocount = +d.wocount;
				d.totalscrap = +d.totalscrap;
				d.zz = (THIS.type == "netvalue") ?  isNaN(d.totalscrap) ? 0 : d.totalscrap : isNaN(d.wocount) ? 0 : d.wocount;
			});
			var ndx = crossfilter(data);
			var all = ndx.groupAll();
			
			THIS.pieChartMaterialType = dc.pieChart("#pie-chart");
			var pieMaterialTypeDimension = ndx.dimension(function(d) {
				return (d.ordertype || "Others") +" - " +(d.ordertypedesc || "Not Available");
			});
			var pieMaterialType = pieMaterialTypeDimension.group();
			if (pieMaterialType.size() > 1
					|| pieMaterialType.all()[0].key.split("-")[0] != "Others") {
				pieMaterialType = pieMaterialType
					.reduceSum(function(d) {
						return d.zz;
					});
			} else {
				$("#pie-chart-purchaseorg h4").removeClass("hide");
				pieMaterialType = pieMaterialType
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartVendor = dc.pieChart("#pie-chart-Vendor");
			var pieChartVendorGroupDimention = ndx.dimension(function(d) {
				return (d.companycode ||"Others") +" - " +(d.companycodedesc || "Not Available");
			});
			var pieVendorGroup = pieChartVendorGroupDimention.group();
			if (pieVendorGroup.size() > 1
					|| pieVendorGroup.all()[0].key.split("-")[0] != "Others") {
				pieVendorGroup = pieVendorGroup
					.reduceSum(function(d) {
						return d.zz;
					});
			} else {
				$("#pie-chart-purchaseorg h4").removeClass("hide");
				pieVendorGroup = pieVendorGroup
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartCompanyCode = dc.pieChart("#pie-chart-Companycode");
			var pieChartCompanyGroupDimention = ndx.dimension(function(d) {
				return (d.orderstatus || "Others") +" - " +(d.companycodename || "Not Available");
			});
			var pieCompanyGroup = pieChartCompanyGroupDimention.group();
			if (pieCompanyGroup.size() > 1
					|| pieCompanyGroup.all()[0].key.split("-")[0] != "Others") {
				pieCompanyGroup = pieCompanyGroup
					.reduceSum(function(d) {
						return d.zz;
					});
			} else {
				$("#pie-chart-purchaseorg h4").removeClass("hide");
				pieCompanyGroup = pieCompanyGroup
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartPurchaseGrp = dc.pieChart("#pie-chart-prchasegroup");
			var pieChartPurchaseGroupDimention = ndx.dimension(function(d) {
				return d.durquart +"(" +d.durquartrange+")"; 
			});
			var piePurchaseGroup = pieChartPurchaseGroupDimention.group();
			console.log(piePurchaseGroup.all()[0].key);
			//if (piePurchaseGroup.size() > 1) {
				piePurchaseGroup = piePurchaseGroup
					.reduceSum(function(d) {
						return d.zz;
					});
				/*} else {
				$("#pie-chart-prchasegroup h4").removeClass("hide");
				piePurchaseGroup = piePurchaseGroup
				.reduceSum(function() {
					return 0;
				});
			}*/
			
			THIS.pieChartPlant = dc.pieChart("#pie-chart-plant");
			var pieChartPlantDimention = ndx.dimension(function(d) {
				return (d.businessarea || "Others") +" - "+(d.businessareadesc || "Not Available");
			});
			var piePlantGroup = pieChartPlantDimention.group();
			if (piePlantGroup.size() > 1
					|| piePlantGroup.all()[0].key.split("-")[0] != "Others") {
				piePlantGroup = piePlantGroup
					.reduceSum(function(d) {
						return d.zz;
					});
			} else {
				$("#pie-chart-purchaseorg h4").removeClass("hide");
				piePlantGroup = piePlantGroup
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartPurchaseorg = dc.pieChart("#pie-chart-purchaseorg");
			var pieChartPurchaseorgDimention = ndx.dimension(function(d) {
				return d.totscrapquart +"(" +d.totscrapquartrange +")";
			});
			var piePurchaseOrgGroup = pieChartPurchaseorgDimention.group();
			//if (piePurchaseOrgGroup.size() > 1) {
				piePurchaseOrgGroup = piePurchaseOrgGroup
					.reduceSum(function(d) {
						return d.zz;
					});
			/*} else {
				$("#pie-chart-purchaseorg h4").removeClass("hide");
				piePurchaseOrgGroup = piePurchaseOrgGroup
				.reduceSum(function() {
					return 0;
				});
			}*/

			THIS.horzBarChart = dc.rowChart("#row-chart");
			THIS.timelineAreaChart = dc.lineChart("#timeline-area-chart");
			THIS.timelineChart = dc.barChart("#timeline-chart");
			
			var timelineDimension = ndx.dimension(function(d) {
				return d.month;
			});
			var timelineGroup = timelineDimension.group().reduceSum(
					function(d) {
						d.zz;

					});
			var timelineAreaGroup = timelineDimension.group().reduce(
					function(p, v) {
						++p.count;
						v.wocount = v.wocount || 0;
						p.total += v.wocount;// (v.open + v.close) / 2;
						// p.avg = numberFormat(p.total / p.count);
						return p;
					}, function(p, v) {
						--p.count;
						v.wocount = v.wocount || 0;
						p.total -= v.wocount;// (v.open + v.close) / 2;
						// p.avg = p.count ? numberFormat(p.total / p.count) : 0;
						return p;
					}, function() {
						return {
							count : 0,
							total : 0,
						};
					});
			// counts per weekday
			var horzBarDimension = ndx.dimension(function(d) {
				return (d.plant || "") + '-' + (d.plantdesc || "Not Available");;
			});
			var horzBarGrp = horzBarDimension.group().reduceSum(function(d) {
				return d.zz;
			});

			THIS.pieChartMaterialType.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(pieMaterialTypeDimension).group(pieMaterialType).title(function(d) {
				return THIS.type == "netvalue" ?
						 d.key + ': $' + numberFormat(d.value)
					:
						 d.key + ': ' + numberFormat(d.value);
			}).on(
					'filtered', function(chart) {
						THIS.refreshTable(pieMaterialTypeDimension);
					}).label(function(d) {
					    var name = d.key.split("-");
					    return name[0];
				});

			THIS.pieChartVendor.width(250).height(200).radius(80).innerRadius(
					40).dimension(pieChartVendorGroupDimention).group(
					pieVendorGroup).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
				THIS.refreshTable(pieChartVendorGroupDimention);
			}).label(function(d) {
			    var name = d.key.split("-");
			    return name[0];
		});

			THIS.pieChartCompanyCode.width(250).height(200).radius(80)
			//.colors(colorScale)
			// .innerRadius(30)
			.dimension(pieChartCompanyGroupDimention).group(pieCompanyGroup)
					.label(function(d) {
						var b = d.key;
						return b;
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
						THIS.refreshTable(pieChartCompanyGroupDimention);
					}).label(function(d) {
					    var name = d.key.split("-");
					    return name[0];
				});

			THIS.pieChartPurchaseGrp.width(250).height(200).radius(80)
					.innerRadius(40).dimension(pieChartPurchaseGroupDimention)
					.group(piePurchaseGroup).label(function(d) {
						var purchaseGrp = d.key;
						return purchaseGrp;
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
						THIS.refreshTable(pieChartPurchaseGroupDimention);
					});

			THIS.pieChartPlant.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(pieChartPlantDimention).group(piePlantGroup).label(
					function(d) {
						var h = d.key;
						return h;
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
				THIS.refreshTable(pieChartPlantDimention);
			});

			THIS.pieChartPurchaseorg.width(250).height(200).radius(80)
					.innerRadius(40).dimension(pieChartPurchaseorgDimention)
					.group(piePurchaseOrgGroup).label(function(d) {
						var z = d.key;
						return z;
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
						THIS.refreshTable(pieChartPurchaseorgDimention);
					}).label(function(d) {
					    return d.key;
				});

			// #### Row Chart
			THIS.horzBarChart.width(180).height(535).margins({
				top : 20,
				left : 10,
				right : 10,
				bottom : 20
			}).group(horzBarGrp).dimension(horzBarDimension).on('filtered',
					function(chart) {
						THIS.refreshTable(horzBarDimension);
					}).label(function(d) {
					    var name = d.key.split("-");
					    return name[0];
				})
			// assign colors to each value in the x scale domain
			.ordinalColors(
					[ '#3182bd', '#6baed6', '#9e5ae1', '#c64bef', '#da8aab' ])
					.label(function(d) {

						return d.key;// .split(".")[1];
					})
					// title sets the row text
					.title(function(d) {
				return THIS.type == "netvalue" ?
						 d.key + ': $' + numberFormat(d.value)
					:
						 d.key + ': ' + numberFormat(d.value);
			}).elasticX(true).xAxis().ticks(4);

			THIS.timelineAreaChart.renderArea(true).width($('#content').width()).height(150)
					.transitionDuration(1000).margins({
						top : 30,
						right : 70,
						bottom : 25,
						left : 80
					}).dimension(timelineDimension).mouseZoomable(true).on(
							'filtered', function(chart) {
								THIS.refreshTable(timelineDimension);
							})
					.rangeChart(THIS.timelineChart).x(
							d3.time.scale().domain(
									[ new Date(1999, 0, 1),
											new Date(2015, 11, 31) ])).round(
							d3.time.month.round).xUnits(d3.time.months)
					.elasticY(true).renderHorizontalGridLines(true)
					// .legend(dc.legend().x(800).y(10).itemHeight(13).gap(5))
					.brushOn(false).group(timelineAreaGroup, "Net Value")
					.valueAccessor(function(d) {
						return d.value.total;
					})
					.title(function(d) {
						var value = d.value.total;
						if (isNaN(value))
							value = 0;
						return dateFormat(d.key) + "\n $" + numberFormat(value);
					});

			THIS.timelineChart.width($('#content').width()).height(80).margins({
				top : 40,
				right : 70,
				bottom : 20,
				left : 80
			}).dimension(timelineDimension).group(timelineGroup)
					.centerBar(true).gap(1).x(
							d3.time.scale().domain(
									[ new Date(1999, 0, 1),
											new Date(2015, 11, 31) ])).round(
							d3.time.month.round).alwaysUseRounding(true)
					.xUnits(d3.time.months);

			dc.dataCount(".dc-data-count").dimension(ndx).group(all);

			var dataTableData = data.slice(0, 100);

			THIS.dataTable = $(".dc-data-table")
					.DataTable(
							{
								"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
								"t"+
								"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
								"processing" : true,
								"lengthChange" : true,
								"sort" : true,
								"info" : true,
								"jQueryUI" : false,
								"scrollX" : true,
								"data" : dataTableData,
								"tableTools" : {
									"aButtons" : [ {
										"sExtends" : "collection",
										"sButtonText" : 'Export <span class="caret" />',
										"aButtons" : [ "csv", "pdf" ]
									} ],
									"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
								},
								"destroy" : true,
								"columns" : [
										{
											"data" : null,
											"defaultContent" : '<img src="./img/details_open.png">'
										},
										{
											"data" : "wocount",
											"defaultContent" : "",
											
										},{
											"data" : "companycode",
											"defaultContent" : ""
										},
										{
											"data" : "plant",
											"defaultContent" : "",
											
										},
										{
											"data" : "matnr",
											"defaultContent" : "",
											
										},
										{
											"data" : "businessarea",
											"defaultContent" : ""
										},
										/*{
											"data" : "costcenter",
											"defaultContent" : ""
										},
										{
											"data" : "functionalarea",
											"defaultContent" : "",											
										},*/										
										{
											"data" : "totalscrap",
											"defaultContent" : "",
											"mRender" : function(data, type,
													full) {
												return numberFormat(data);
											},
											"class" : "numbercolumn"
										},
										{
											"data" : "actualscrap",
											"defaultContent" : "",											
										},
										{
											"data" : "actualyield",
											"defaultContent" : "",											
										},
										{
											"data" : "poq",
											"defaultContent" : "",											
										},
										{
											"data" : "pas",
											"defaultContent" : "",											
										},
										{
											"data" : "ordertype",
											"defaultContent" : ""
										},
										{
											"data" : "orderstatus",
											"defaultContent" : ""
										},
										{
											"data" : "ordersettlement",
											"defaultContent" : ""
										},
										{
											"data" : "actualreleasedate",
											"defaultContent" : ""
										}],
										"rowCallback" : function(nRow, aData,
												iDisplayIndex, iDisplayIndexFull) {
											var request = {
												"viewType" : "level2",
												"source" : aData.source || "null",
												"companycode" : aData.companycode || "null",
												"plant" : aData.plant || "null",
												"material" : aData.matnr || "null",
												"businessarea" : aData.businessarea || "null",
												"ordertype" : aData.ordertype || "null",
												"actualreleasedate" : aData.actualreleasedate || "null"												
											};
											$(nRow).attr({
												"id" : "row" + iDisplayIndex
											}).unbind('click').bind('click', request,
													WOProfiling.callLevel2PoProfile);
										}
							});
		dc.renderAll();
	};

	this.callLevel2PoProfile = function(request) {
		var tr = $(this).closest('tr');
		THIS.childId = "child" + $(tr).attr("id");
		if ($(tr).find('td:eq(0)').hasClass("expandRow")) {
			$($(tr).find('td:eq(0)').find('img')).attr('src',
					'./img/details_open.png');
			$("#" + THIS.childId).hide(50);
		} else {

			$($(tr).find('td:eq(0)').find('img')).attr('src',
					'./img/details_close.png');
			if (!$(tr).next().hasClass("childRowtable")) {
				$(tr).after($("<tr/>").addClass("childRowtable").attr({
					"id" : THIS.childId
				}).append($("<td/>")).append($("<td/>").attr({
					"colspan" : "13",
					"id" : THIS.childId + "td"
				}).addClass("carousel")));

				enableLoading(THIS.childId + "td","50%","50%");
				request = request.data;
				callAjaxService(
						"woProfiling",
						function (response){callbackSucessLevel2POProfile(response, request);}, callBackFailure, request, "POST");
			} else {
				$("#" + THIS.childId).show(50);
			}
		}
		$(tr).find('td:eq(0)').toggleClass("expandRow");
	};

	var callbackSucessLevel2POProfile = function(response, request) {
		disableLoading(THIS.childId + "td");
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		var childTabledata = null;
		if (response != null)
			childTabledata = response["level2WorkOrderList"];
		
		$("#" + THIS.childId + "td")
				.show()
				.html("")
				.append(
						$("<table/>").addClass(
								"table  table-striped table-hover dc-data-table").attr({
							"id" : THIS.childId + "Table"
						}).append(
								$("<thead/>").append(
										$("<tr/>").append(
												$("<th/>").html(
														"Work Order"))
												.append(
														$("<th/>").html(
																"Item Number"))
												.append(
														$("<th/>").html(
																"Schedule Start Date"))
												.append(
														$("<th/>")
																.html("Schedule Finish Date"))
												.append(
														$("<th/>").html(
																"Actual Start Date"))
												.append(
														$("<th/>").html(
																"Actual Finish Date"))
												.append(
														$("<th/>").html(
																"Actual Confirm Date"))				
								)));

		$("#" + THIS.childId + "Table")
				.DataTable(
						{
							"dom":
								"t"+
								"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"processing" : true,
							"lengthChange" : true,
							// "bServerSide": true,
							"info" : true,
							"filter" : false,
							"jQueryUI" : false,
							"data" : childTabledata,
							"scrollX" : true,
							"tableTools" : {
								"aButtons" : [ {
									"sExtends" : "collection",
									"sButtonText" : 'Export <span class="caret" />',
									"aButtons" : [ "csv", "pdf" ]
								} ],
								"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
							},
							"columns" : [ {
								"data" : "workorder",
								"defaultContent" : ""
							}, {
								"data" : "workorderline",
								"defaultContent" : ""
							}, {
								"data" : "schedulestartdate",
								"defaultContent" : ""
							}, {
								"data" : "schedulefinishdate",
								"defaultContent" : ""
							}, {
								"data" : "actualstartdate",
								"defaultContent" : ""
							}, {
								"data" : "actualfinishdate",
								"defaultContent" : ""
							}, {
								"data" : "actualconfirmdate",
								"defaultContent" : ""
							} ],
							"rowCallback" : function(nRow, aData,
									iDisplayIndex, iDisplayIndexFull) {
								$(nRow)
										.attr(
												{
													'onClick' : "javascript:WOProfiling.thirdLevelDrillDownWOProfile('e2e_"+THIS.source+"_l3_wo','"+ aData.workorder +"')",
													'data-target' : '#myModalthredlevel',
													'data-toggle' : 'modal'
												});
							}
						});
	};
	
	this.thirdLevelDrillDownWOProfile = function(entitytype, criteria) {
		$("#poDocView").empty();
		enableLoading("poDocView");
		var CallbackSuccessLevel3Details = function(l3Data) {
			if (l3Data && l3Data.isException){
				showNotification("error",l3Data.customMessage);
				return;
			}
			if (l3Data && l3Data[0]) {
				l3Data = l3Data[0];
				var keys = Object.keys(l3Data);
				var onAjaxLoadSuccess = function(html) {
					if (html && html.isException){
						showNotification("error",html.customMessage);
						return;
					}
					$('#myModalthredlevel #myModalLabel').text(entitytype);
					$("#poDocView").html(html);
					$("#entity").text(entitytype);
					$("#criteria").text(criteria);
					$("#btnPrintL3").unbind('click').bind('click', function(e) {
						printElement('#poDocView');
					});
					$("#btnSaveL3").unbind('click').bind('click',function(e) {
						savePDF('#poDocView');
					});
					/*
					 * keys.forEach(function(key){ console.log(l3Data[key]);
					 * $("#tbDynamic tbody").append($("<tr>\n").append("<th>"+key+"</th>\n").append("<td>"+l3Data[key]+"</td>\n")); })
					 */
					// $("<tr/>").append($("<td/>").html(i+1)).append($("<td/>").html("po[i].ekpo_matnr"))
					var i = 0, idx = keys.length, lvalue, lkey, rvalue, rkey;
					while (i < idx) {
						lvalue = l3Data[keys[i]] !== undefined
								&& l3Data[keys[i]] !== null ? l3Data[keys[i]]
								: '';
						while (lvalue == '' && i < idx) {
							i++;
							lvalue = l3Data[keys[i]] !== undefined
									&& l3Data[keys[i]] !== null ? l3Data[keys[i]]
									: '';
						}
						lkey = keys[i] !== undefined && keys[i] !== null ? keys[i]
								: '';
						i++;
						rvalue = l3Data[keys[i]] !== undefined
								&& l3Data[keys[i]] !== null ? l3Data[keys[i]]
								: '';
						while (rvalue == '' && i < idx) {
							i++;
							rvalue = l3Data[keys[i]] !== undefined
									&& l3Data[keys[i]] !== null ? l3Data[keys[i]]
									: '';
						}
						rkey = keys[i] !== undefined && keys[i] !== null ? keys[i]
								: '';
						if (rvalue == '') {
							rkey = '';
						}
						var colTitle = lookUpTable.get(lkey.toUpperCase());
						if (colTitle != null){
							lkey = colTitle;
						}
						 colTitle = lookUpTable.get(rkey.toUpperCase());
							if (colTitle != null){
								rkey = colTitle;
							}
						if (rvalue != '' || lvalue != '') {
							$("#tbDynamic tbody")
									.append(
											$("<tr>\n")
													.append(
															"<th>" + lkey
																	+ "</th>\n")
													.append(
															"<td>" + lvalue
																	+ "</td>\n")
													.append(
															"<th>" + rkey
																	+ "</th>\n")
													.append(
															"<td>" + rvalue
																	+ "</td>\n"));
						}

						i++;
					}
					// for(var i=0, idx= keys.length; i<idx; i++){
					// var lkey = keys[i]!==undefined && keys[i] !==
					// null?keys[i]:'';
					// var lvalue = l3Data[keys[i]] !== undefined &&
					// l3Data[keys[i]] !== null? l3Data[keys[i]] :'';
					// var rkey = keys[i+1]!==undefined && keys[i+1] !==
					// null?keys[i+1]:'';
					// var rvalue = l3Data[keys[i+1]] !== undefined &&
					// l3Data[keys[i+1]] !== null? l3Data[keys[i+1]] :'';
					// if(lvalue !== '' || rvalue != ''){
					// $("#tbDynamic tbody").append($("<tr>\n")
					// .append("<th>"+lkey+"</th>\n")
					// .append("<td>"+lvalue+"</td>\n")
					// .append("<th>"+rkey+"</th>\n")
					// .append("<td>"+rvalue+"</td>\n"));
					// }
					// }
			       
			        
				};
				callAjaxService("SearchL3View", onAjaxLoadSuccess, function() {
					alert("error");
				}, null, "GET", "html");
			} else {
				$("#poDocView").html('<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" href="#">×</a><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No matching results found.</p></div>');;
			}
		};
		var request = {
			"entitytype" : System.dbName +"."+entitytype,
			"criteria" : "aufnr:" + criteria
		};
		callAjaxService("Level3Details", CallbackSuccessLevel3Details,
				callBackFailure, request, "POST");
	};
	
	
	this.refreshTable = function(dim) {

		if (THIS.dataTable !== null && dim !== null) {
			THIS.dataTable.clear();
			THIS.dataTable.rows.add(dim.top(1000));
			THIS.dataTable.columns.adjust().draw();
		} else
			console
					.log('[While Refreshing Data Table] This should never happen..');
	};
	// #### Version
	// Determine the current version of dc with `dc.version`
	d3.selectAll("#version").text(dc.version);
};
plantProfileInstance();