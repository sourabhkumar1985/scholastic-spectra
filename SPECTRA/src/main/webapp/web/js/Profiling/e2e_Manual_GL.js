'use strict';
pageSetUp();
var manualGLProfiling;
var manualGLProfileInstance = function() {
	manualGLProfiling = new manualGLProfilingObject();
	manualGLProfiling.Load();
};

var manualGLProfilingObject = function() {
	var THIS = this;
	var numberFormat = d3.format(",f");
	THIS.type = "netvalue";
	constructTabs(THIS);
	this.Load = function(obj) {
		enableLoading();
		var source = $(obj).data("source");
		THIS.source = source || $("#myTab").find("li.active").data("source");
		var request = {
			"source" : THIS.source,
			"viewType" : "default"
		};
		callAjaxService("manualGL", callbackSuccessManualGL, callBackFailure,
				request, "POST");
	};
	
	this.loadViewType = function(viewType){
		enableLoading();
		THIS.type = viewType;
		setTimeout(function() { drawChart(); disableLoading();}, 50);
	};
	
	var callbackSuccessManualGL = function(data) {
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		if (data != undefined && data != null
				&& data["aggregatedManualGlList"] != undefined
				&& data["aggregatedManualGlList"] != null && data["aggregatedManualGlList"].length > 0) {
			THIS.data = data['aggregatedManualGlList'];
			drawChart();
		}
		disableLoading();
	};
	
	var drawChart = function(){
		var data = THIS.data;
		$("[id^=pie-chart] h4").addClass("hide");
			var dateFormat = d3.timeFormat("%m/%Y");
			data.forEach(function(d) {
				d.dd = dateFormat.parse(d.postingdate);
				d.month = d3.time.month(d.dd);
				d.glamount = +d.glamount;
				d.zz = (THIS.type == "netvalue") ?  d.glamount ? d.glamount : 0 : isNaN(d.glcount) ? 0 : d.glcount;
			});

			var ndx = crossfilter(data);

			var all = ndx.groupAll();
			var timelineDimension = ndx.dimension(function(d) {
				return d.month;
			});
			var timelineGroup = timelineDimension.group().reduceSum(
					function(d) {
						return d.zz;
					});
			var timelineAreaGroup = timelineDimension.group().reduce(
					function(p, v) {
						++p.count;
						p.total += v.glamount;
						return p;
					}, function(p, v) {
						--p.count;
						p.total -= v.glamount;// (v.open + v.close) / 2;
						return p;
					}, function() {
						return {
							count : 0,
							total : 0,
						};
					});

			
			THIS.pieChartCompanyCode = dc.pieChart("#pie-chart-CompanyCode");
			var pieChartCompanyCodeDimension = ndx.dimension(function(d) {
				return (d.companycode || "Others") + "-" + (d.companycodename || "Not Available");
			});
			var pieCompanyCode = pieChartCompanyCodeDimension.group();
			if (pieCompanyCode.size() > 1
					|| pieCompanyCode.all()[0].key.split("-")[0] != "Others") {
				pieCompanyCode = pieCompanyCode.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-CompanyCode h4").removeClass("hide");
				pieCompanyCode = pieCompanyCode
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartAccGroup = dc.pieChart("#pie-chart-AccountGroup");
			var pieChartAccGroupDimension = ndx.dimension(function(d) {
				return d.accgroup || "Others";
			});
			var pieAccGroup = pieChartAccGroupDimension.group();
			if (pieAccGroup.size() > 1
					|| pieAccGroup.all()[0].key.split("-")[0] != "Others") {
				pieAccGroup = pieAccGroup.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-AccountGroup h4").removeClass("hide");
				pieAccGroup = pieAccGroup
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartReversal = dc.pieChart("#pie-chart-Reversal");
			var pieChartReversalDimension = ndx.dimension(function(d) {
				return d.reversal ||  "Others";
			});
			var pieReversal = pieChartReversalDimension.group();
			if (pieReversal.size() > 1
					|| pieReversal.all()[0].key.split("-")[0] != "Others") {
				pieReversal = pieReversal.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-Reversal h4").removeClass("hide");
				pieReversal = pieReversal
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartTradPartner = dc.pieChart("#pie-chart-TradingPartner");
			var pieChartTradPartnerDimension = ndx.dimension(function(d) {
				return (d.tradpartner ||  "Others") +"-" +(d.tradpartnername || "Not Available");
			});
			var pieTradPartner = pieChartTradPartnerDimension.group();
			if (pieTradPartner.size() > 1
					|| pieTradPartner.all()[0].key.split("-")[0] != "Others") {
				pieTradPartner = pieTradPartner.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-TradingPartner h4").removeClass("hide");
				pieTradPartner = pieTradPartner
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartUserId = dc.pieChart("#pie-chart-UserId");
			var pieChartUserIdDimension = ndx.dimension(function(d) {
				return d.userid ||  "Others";
			});
			var pieUserId = pieChartUserIdDimension.group();
			if (pieUserId.size() > 1
					|| pieUserId.all()[0].key.split("-")[0] != "Others") {
				pieUserId = pieUserId.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-UserId h4").removeClass("hide");
				pieUserId = pieUserId
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartTCode = dc.pieChart("#pie-chart-TCode");
			var pieChartTCodeDimension = ndx.dimension(function(d) {
				return (d.tcode ||  "Others")+ "-" + (d.tcodename||"Not Available");
			});
			var pieTCode = pieChartTCodeDimension.group();
			if (pieTCode.size() > 1
					|| pieTCode.all()[0].key.split("-")[0] != "Others") {
				pieTCode = pieTCode.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-TCode h4").removeClass("hide");
				pieTCode = pieTCode
				.reduceSum(function() {
					return 0;
				});
			}

			THIS.horzBarChart = dc.rowChart("#row-chart");
			THIS.timelineAreaChart = dc.lineChart("#timeline-area-chart");
			THIS.timelineChart = dc.barChart("#timeline-chart");
			var horzBarDimension = ndx.dimension(function(d) {
				return (d.doctype || "Others") + "-" + (d.doctypedesc || "Not Available");
						
			});
			var horzBarGrp = horzBarDimension.group().reduceSum(function(d) {
				return d.zz;
			});

			THIS.pieChartCompanyCode.width(250).height(200).radius(80)
					.dimension(pieChartCompanyCodeDimension).group(
							pieCompanyCode).label(function(d) {
						var b = d.key.split("-");
						return b[0];
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
						THIS.refreshTable(pieChartCompanyCodeDimension);
					});
			THIS.pieChartAccGroup.width(250).height(200).radius(80)
					.innerRadius(40).dimension(pieChartAccGroupDimension)
					.group(pieAccGroup).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
						THIS.refreshTable(pieChartCompanyCodeDimension);
					});
			THIS.pieChartReversal.width(250).height(200).radius(80).dimension(
					pieChartReversalDimension).group(pieReversal).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
				THIS.refreshTable(pieChartCompanyCodeDimension);
			});
			THIS.pieChartTradPartner.width(200).height(200).radius(80)
					.innerRadius(40).dimension(pieChartTradPartnerDimension)
					.group(pieTradPartner).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
						THIS.refreshTable(pieChartCompanyCodeDimension);
					}).label(function(d) {
						var b = d.key.split("-");
						return b[0];
					});
			THIS.pieChartUserId.width(200).height(200).radius(80).dimension(
					pieChartUserIdDimension).group(pieUserId).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
				THIS.refreshTable(pieChartCompanyCodeDimension);
			});
			THIS.pieChartTCode.width(200).height(200).radius(80)
					.innerRadius(40).dimension(pieChartTCodeDimension).group(
							pieTCode).label(function(d) {
						var b = d.key.split("-");
						return b[0];
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
						THIS.refreshTable(pieChartCompanyCodeDimension);
					});
			// pieChartreversal.width(200).height(200).radius(80).innerRadius(40).dimension(pieChartreversalDimension).group(pieReversal);
			// #### Row Chart
			THIS.horzBarChart.width(180).height(520).margins({
				top : 20,
				left : 10,
				right : 10,
				bottom : 20
			}).group(horzBarGrp).dimension(horzBarDimension).on('filtered',
					function(chart) {
						THIS.refreshTable(horzBarDimension);
					}).label(function(d) {
					    var name = d.key.split("-");
					    return name[0];
				})
			// assign colors to each value in the x scale domain
			.ordinalColors(
					[ '#3182bd', '#6baed6', '#9e5ae1', '#c64bef', '#da8aab' ])
					/*.label(function(d) {

						return d.key;// .split(".")[1];
					})*/
					// title sets the row text
					.title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).elasticX(true).xAxis().ticks(4);

			THIS.timelineAreaChart.renderArea(true)
					.width($('#content').width()).height(150)
					.transitionDuration(1000).margins({
						top : 30,
						right : 70,
						bottom : 25,
						left : 100
					}).dimension(timelineDimension).mouseZoomable(true).on(
							'filtered', function(chart) {
								THIS.refreshTable(timelineDimension);
							})
					// Specify a range chart to link the brush extent of the
					// range
					// with the zoom focue of the current chart.
					.rangeChart(THIS.timelineChart).x(
							d3.time.scale().domain(
									[ new Date(2011, 0, 1),
											new Date(2015, 11, 31) ])).round(
							d3.time.month.round).xUnits(d3.time.months)
					.elasticY(true).renderHorizontalGridLines(true)
					// .legend(dc.legend().x(800).y(10).itemHeight(13).gap(5))
					.brushOn(false)
					// Add the base layer of the stack with group. The second
					// parameter specifies a series name for use in the legend
					// The `.valueAccessor` will be used for the base layer
					.group(timelineAreaGroup, "Vendor Count").valueAccessor(
							function(d) {
								return d.value.total;
							}).title(
							function(d) {
								var value = d.value.total ? d.value.total
										: d.value;
								if (isNaN(value))
									value = 0;
								return dateFormat(d.key) + "\n $"
										+ numberFormat(value);
							});

			THIS.timelineChart.width($('#content').width()).height(80).margins(
					{
						top : 40,
						right : 70,
						bottom : 20,
						left : 100
					}).dimension(timelineDimension).group(timelineGroup)
					.centerBar(true).gap(1).x(
							d3.time.scale().domain(
									[ new Date(2011, 0, 1),
											new Date(2015, 11, 31) ])).round(
							d3.time.month.round).alwaysUseRounding(true)
					.xUnits(d3.time.months);
			dc.dataCount(".dc-data-count").dimension(ndx).group(all);

			var dataTableData = data.slice(0, 100);

			THIS.dataTable = $(".dc-data-table")
					.DataTable(
							{
								"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
								"t"+
								"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
								"processing" : true,
								"lengthChange" : true,
								"bSort" : true,
								"info" : true,
								"jQueryUI" : false,
								"scrollX" : true,
								"data" : dataTableData,
								 "bAutoWidth": false,
								"tableTools" : {
									"aButtons" : [ {
										"sExtends" : "collection",
										"sButtonText" : 'Export <span class="caret" />',
										"aButtons" : [ "csv", "pdf" ]
									} ],
									"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
								},
								"bDestroy" : true,
								"columns" : [
										{
											"data" : null,
											"defaultContent" : '<img src="./img/details_open.png">'
										},
										{
											"data" : "glcount",
											"defaultContent" : "0",
											"class" : "numbercolumn"
										},
										{
											"data" : "companycode",
											"defaultContent" : ""
										},
										{
											"data" : "doctype",
											"defaultContent" : ""
										},
										{
											"data" : "accgroup",
											"defaultContent" : ""
										},
										{
											"data" : "tradpartner",
											"defaultContent" : ""
										},
										{
											"data" : "userid",
											"defaultContent" : ""
										},
										{
											"data" : "tcode",
											"defaultContent" : ""
										},
										{
											"data" : "reversal",
											"defaultContent" : ""
										},
										{

											"data" : "glamount",
											"defaultContent" : "",
											"render" : function(data, type,
													full) {
												return numberFormat(data);
											},
											"class" : "numbercolumn"
										}, {

											"data" : "postingdate",
											"defaultContent" : ""
										}, ],
								"rowCallback" : function(nRow, aData,
										iDisplayIndex, iDisplayIndexFull) {
									var request = {
										"viewType" : "level2",
										"source" : aData.source || "null",
										"companyCode" : aData.companycode
												|| "null",
										"doctType" : aData.doctype || "null",
										"accGroup" : aData.accgroup || "null",
										"tradePartner" : aData.tradpartner
												|| "null",
										"userID" : aData.userid || "null",
										"transactionCode" : aData.tcode
												|| "null",
										"reversal" : aData.reversal || "null",
										"glAmount" : aData.glamount || "null",
										"region" : aData.region || "null",
										"postingDate" : aData.postingdate
												|| "null"
									};
									$(nRow)
											.attr({
												"id" : "row" + iDisplayIndex
											})
											.unbind('click')
											.bind(
													'click',
													request,
													manualGLProfiling.callLevel2manualGLProfiling);
								}
							});
			dc.renderAll();
	};
	this.callLevel2manualGLProfiling = function(request) {
		var tr = $(this).closest('tr');
		THIS.childId = "child" + $(tr).attr("id");
		if ($(tr).find('td:eq(0)').hasClass("expandRow")) {
			$($(tr).find('td:eq(0)').find('img')).attr('src',
					'./img/details_open.png');
			$("#" + THIS.childId).hide(50);
		} else {

			$($(tr).find('td:eq(0)').find('img')).attr('src',
					'./img/details_close.png');
			if (!$(tr).next().hasClass("childRowtable")) {
				$(tr).after($("<tr/>").addClass("childRowtable").attr({
					"id" : THIS.childId
				}).append($("<td/>")).append($("<td/>").attr({
					"colspan" : "13",
					"id" : THIS.childId + "td"
				}).addClass("carousel")));

				enableLoading(THIS.childId + "td", "50%", "45%");
				request = request.data;
				callAjaxService("manualGL", function(response) {
					callbackSucessLevel2ManualGL(response, request);
				}, callBackFailure, request, "POST");
			} else {
				$("#" + THIS.childId).show(50);
			}
		}
		$(tr).find('td:eq(0)').toggleClass("expandRow");
	};
	var callbackSucessLevel2ManualGL = function(response, request) {
		disableLoading(THIS.childId + "td");
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		var childTabledata = null;
		var payload = null;
		if(THIS.source == 'SAP'){
		 payload = "entitytype="+System.dbName+".e2e_"+ THIS.source +"_l3_gl&criteria=belnr:";
		} else if(THIS.source == 'JDE'){
		 payload = "entitytype="+System.dbName+".F0911&criteria=gldoc:";
		}
		if (response && response["level2ManualGlList"])
			childTabledata = response["level2ManualGlList"];
		$("#" + THIS.childId + "td")
				.show()
				.html("")
				.append(
						$("<table/>").addClass(
								"table table-striped table-hover dc-data-table").attr({
							"id" : THIS.childId + "Table"
						}).append(
								$("<thead/>").append(
										$("<tr/>").append(
												$("<th/>").html(
														"Document Number"))
												.append(
														$("<th/>").html(
																"Transaction Code Desc"))
												.append(
														$("<th/>").html(
																"Company Name"))
												.append(
														$("<th/>")
																.html("Account Name"))
												.append(
														$("<th/>").html(
																"Amount"))
												.append(
														$("<th/>").html(
																"Posting Date")))));

		$("#" + THIS.childId + "Table")
				.DataTable(
						{
							"dom":
								"t"+
								"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"processing" : true,
							"lengthChange" : true,
							// "bServerSide": true,
							"info" : true,
							"filter" : false,
							"jQueryUI" : false,
							"data" : childTabledata,
							"scrollX" : true,
							"tableTools" : {
								"aButtons" : [ {
									"sExtends" : "collection",
									"sButtonText" : 'Export <span class="caret" />',
									"aButtons" : [ "csv", "pdf" ]
								} ],
								"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
							},
							"columns" : [ {
								"data" : "docNumber",
								"defaultContent" : ""
							}, {
								"data" : "tcodename",
								"defaultContent" : ""
							}, {
								"data" : "companycodename",
								"defaultContent" : ""
							}, {
								"data" : "accname",
								"defaultContent" : ""
							}, {
								"data" : "glamount",
								"defaultContent" : "",
								"render" : function(data, type,
										full) {
									return numberFormat(data);
								},
								"class" : "numbercolumn"
							}, {
								"data" : "postingdate",
								"defaultContent" : ""
							} ],
							"rowCallback" : function(nRow, aData,
									iDisplayIndex, iDisplayIndexFull) {
								var query = payload+aData.docNumber;
								$(nRow)
										.attr(
												{
													'onClick' : "javascript:searchL3DrillDown('"+query+"','Manual GL','"+aData.docNumber+"','e2e_"+THIS.source+"_l3_gl')",
													'data-target' : '#myModalthredlevel',
													'data-toggle' : 'modal'
												});
							}
						});
	};
	this.refreshTable = function(dim) {

		if (THIS.dataTable !== null && dim !== null) {
			THIS.dataTable.clear();
			THIS.dataTable.rows.add(dim.top(1000));
			THIS.dataTable.columns.adjust().draw();
		} else
			console
					.log('[While Refreshing Data Table] This should never happen..');
	};
	// #### Version
	// Determine the current version of dc with `dc.version`
	d3.selectAll("#version").text(dc.version);

	/*var config = {
		endpoint : 'manualGL',
		dataKey : 'aggregatedManualGlList',
		dataType : 'json',
		dataPreProcess : function(d) {
			var dateFormat = d3.timeFormat("%Y-%m");
			d.dd = dateFormat.parse(d.postingdate);
			d.month = d3.time.month(d.dd); // pre-calculate month for better
											// performance
			d.glamount = +numberFormat(d.glamount * 100) / 100;
		},
		mappings : [
				{
					chartType : 'pie',
					htmlElement : '#pie-chart-CompanyCode',
					field : 'companycode',
					resetHandler : "#resetPlant"
				},
				{
					chartType : 'pie',
					htmlElement : '#pie-chart-AccountGroup',
					field : 'accgroup',
					groupon : 'glamount',
					groupRound : true,
					resetHandler : "#resetPlant"
				},
				{
					chartType : 'pie',
					htmlElement : '#pie-chart-TradingPartner',
					field : 'tradpartner',
					groupon : 'glamount',
					groupRound : true,
					resetHandler : "#resetPlant"
				},
				{
					chartType : 'pie',
					htmlElement : '#pie-chart-UserId',
					field : 'userid',
					groupon : 'glamount',
					groupRound : true,
					resetHandler : "#resetPlant"
				},
				{
					chartType : 'pie',
					htmlElement : '#pie-chart-TCode',
					field : 'tcode',
					groupon : 'glamount',
					groupRound : true,
					resetHandler : "#resetPlant"
				},
				{
					chartType : 'horbar',
					htmlElement : '#row-chart',
					field : 'doctype',
					resetHandler : ''
				},
				{
					chartType : 'timeline',
					htmlElement : '#timeline-area-chart',
					subHtmlElement : '#timeline-chart',
					field : 'month',
					group : 'glamount',
					resetHandler : '#resetTAC',
					mapFunction : function(p, v) {
						++p.count;
						p.total += numberFormat(v.glamount * 100) / 100;// (v.open
																		// +
																		// v.close)
																		// / 2;
						return p;
					},
					reduceFunction : function(p, v) {
						--p.count;
						p.total -= v.netprice;// (v.open + v.close) / 2;
						return p;
					},
					countFunction : function() {
						return {
							count : 0,
							total : 0,
							avg : 0,
							materialMap : [],
							vendorMap : []
						}
					}
				},
				{
					chartType : 'table',
					htmlElement : '.dc-data-table',
					field : 'month',
					groupon : 'companycode',
					coloums : 'postingdate, companycode, doctype, accgroup, accname, tradpartner, userid, tcode, headertext, reversal, glamount',
					sortby : 'companycode',
					summarydiv : '.dc-data-count',
					renderlet : 'dc-table-group',
					resetHandler : '#resetRC'
				} ]
	};*/

	/*
	 * var MGLProf = new Profiling(config); MGLProf.init(function(){
	 * $('#loading').hide(); });
	 */

};
manualGLProfileInstance();