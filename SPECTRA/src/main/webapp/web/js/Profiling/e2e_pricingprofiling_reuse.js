'use strict';
pageSetUp();

$('#loading').show();

var config = {
	endpoint : 'pricingProfile',
	dataKey : 'aggregatedPricingList',
	dataType : 'json',
	mappings : [
			{
				chartType : 'pie',
				htmlElement : '#pie-chart-Companycode',
				field : 'companycode, companycodename',
				resetHandler : '#resetCC'
			},
			{
				chartType : 'pie',
				htmlElement : '#pie-chart-plant',
				field : 'plantgroup, plantname',
				resetHandler : "#resetPlant"
			},
			{
				chartType : 'pie',
				htmlElement : '#pie-chart-salesorg',
				field : 'salesorg',
				resetHandler : '#resetSalesOrg'
			},
			{
				chartType : 'pie',
				htmlElement : '#pie-chart-SalesGroup',
				field : 'salesorg',
				resetHandler : '#resetSG'
			},
			{
				chartType : 'pie',
				htmlElement : '#pie-chart-MaterialType',
				field : 'materialtype',
				grouponcount : true,
				resetHandler : '#resetMT'
			},
			{
				chartType : 'pie',
				htmlElement : '#pie-chart-SOType',
				field : 'sotype',
				grouponcount : true,
				resetHandler : '#resetSOT'
			},
			{
				chartType : 'pie',
				htmlElement : '#pie-chart-PricingConditon',
				field : 'salesdoctype',
				grouponcount : true,
				resetHandler : '#resetSDT'
			},
			{
				chartType : 'pie',
				htmlElement : '#pie-chart-DistChannel',
				field : 'distributionchannel',
				resetHandler : '#resetDT'
			},
			{
				chartType : 'pie',
				htmlElement : '#pie-chart-Division',
				field : 'division',
				resetHandler : '#resetD'
			},
			{
				chartType : 'horbar',
				htmlElement : '#row-chart',
				field : 'plantname',
				resetHandler : ''
			},
			{
				chartType : 'table',
				htmlElement : '.dc-data-table',
				field : 'companycode',
				groupon : 'companycode',
				coloums : 'socount, companycode, plantname, salesorg, materialtype, sotype, pricingcondition, distributionchannel, division',
				sortby : 'companycode',
				summarydiv : '.dc-data-count',
				renderlet : 'dc-table-group',
				resetHandler : '#resetRC'
			}, {
				chartType : 'timeline',
				htmlElement : '#timeline-area-chart',
				subHtmlElement : '#timeline-chart',
				field : 'month',
				group : 'socount',
				resetHandler : '#resetTAC',
				mapFunction : function(p, v) {
					++p.count;
					v.netvalue = v.netvalue || 0;
					p.total += v.netvalue;
					return p;
				},
				reduceFunction : function(p, v) {
					--p.count;
					v.netvalue = v.netvalue || 0;
					p.total -= v.netvalue;// (v.open + v.close) / 2;
					// p.avg = p.count ? Math.round(p.total / p.count) : 0;
					return p;
				},
				countFunction : function() {
					return {
						count : 0,
						total : 0,
					}
				}
			} ]
}
var Profiling = new Profiling(config);
Profiling.init(function() {
	$('#loading').hide();
});

var callBackfailureDropShip = function(response) {
	alert("ok");
};
// #### Version
// Determine the current version of dc with `dc.version`
d3.selectAll("#version").text(dc.version);

var opts = {
	lines : 15, // The number of lines to draw
	length : 8, // The length of each line
	width : 5, // The line thickness
	radius : 12, // The radius of the inner circle
	corners : 0.7, // Corner roundness (0..1)
	rotate : 0, // The rotation offset
	direction : 1, // 1: clockwise, -1: counterclockwise
	color : '#000', // #rgb or #rrggbb or array of colors
	speed : 1, // Rounds per second
	trail : 60, // Afterglow percentage
	shadow : false, // Whether to render a shadow
	hwaccel : false, // Whether to use hardware acceleration
	className : 'spinner', // The CSS class to assign to the spinner
	zIndex : 2e9, // The z-index (defaults to 2000000000)
	top : '10', // Top position relative to parent in px
	left : 'auto' // Left position relative to parent in px
};
var target = document.getElementById('loading');
var spinner = new Spinner(opts).spin(target);