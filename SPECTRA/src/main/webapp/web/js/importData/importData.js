pageSetUp();
var importData = function() {
	var THIS = this;
	$("#frmExportImport").validate({

		rules : {
			datasource:{
				required : true
			},
			database : {
				required : true
			},
			datatable : {
				required: true
			}
		},
		messages : {
			datasource : {
				required : 'Please select source'
			},
			database : {
				required : 'Please select database'
			},
			datatable : {
				required : 'Please select table'
			}
		},
		errorPlacement : function(error, element) {
			error.insertAfter(element);
		}
	});
	
	$("#csvImportModelForm").validate({

		rules : {
			selectFile:{
				required : true
			},
			table:{
				required : true
			},
			separator:{
				required : true
			}
		},
		messages : {
			selectFile : {
				required : 'Please select File'
			},
			table : {
				required : 'Please Enter Table Name'
			},
			separator : {
				required : 'Please Enter Seperator'
			}
		},
		errorPlacement : function(error, element) {
			error.insertAfter(element);
		}
	});
	$("#datasource").select2({
		placeholder : "Choose Source",
		data :  [{
			"id":"hive","text":"HIVE"},{
				"id":"postgreSQL","text":"PostgreSQL"}]
	}).on("change", function(e) {
		sourceSelect(e.val);
	});
	$("#frmModalExportImport,#csvImportModelForm").submit(function(e) {
	    e.preventDefault();
	});
	$('#importFieldSet,#exportFieldSet').hide();
	$("#database").select2({
		placeholder : "Choose Database",
		data :  []
	});
	$("#datatable").select2({
		placeholder : "Choose Table",
		data :  []
	});
	$('.fileUpload').change(function(event){
		$(this).parent().next().val($(this).val().replace(/^C:\\fakepath\\/i, ''));
	});
	$('#import').click(function(){
		if(!$("#frmExportImport").valid()){
			return;
		}
		$("#myModalUpload").modal("show");
		$('#exportImportButton').html('Import').unbind('click').bind('click',function(){
			enableLoading();
			var formData = new FormData(document.getElementById("frmModalExportImport"));
			formData.append("env", $("#datasource").select2('val'));
			formData.append("db", $("#database").select2('val'));
			formData.append("table", $("#datatable").select2('val'));
		    $.ajax({
	              url:serviceCallMethodNames["fileUpload"].url,
	              type: "POST",
	              data: formData,
	              enctype: 'multipart/form-data',
	              processData: false,
	              contentType: false
	            }).done(function( data ) {
	            	disableLoading();
	            	showNotification("success","File Uploaded successfully..");
	            });
		   // callAjaxService("fileUpload",  callBackfileUpload,callBackFailure,request, "POST");
		});
		$('#importFieldSet').show().find('em').remove();
		$('#exportFieldSet').hide();
		$('#modalHeaderExportImport').html('Import');
	});
	
	$('#importCSV').click(function(){
		if(!$("#frmExportImport").valid()){
			return;
		}
		
		$("#table").val($("#datatable").select2('val'));
		$("#selectFile,#selectFilePlaceholder").val("");
		$("#separator").val("");
		
		$("#csvImportModel").modal("show");
		$('#btnCSVImport').html('Import').unbind('click').bind('click',function(){
			if(!$("#csvImportModelForm").valid()){
				return;
			}
			enableLoading();
			var formData = new FormData(document.getElementById("csvImportModelForm"));
			formData.append("env", $("#datasource").select2('val'));
			formData.append("db", $("#database").select2('val'));
//			formData.append("separator", $("#separator").val());
//			formData.append("table",$("#table").val());
		    $.ajax({
	              url:serviceCallMethodNames["uploadCSV"].url,
	              type: "POST",
	              data: formData,
	              enctype: 'multipart/form-data',
	              processData: false,
	              contentType: false
	            }).done(function( data ) {
	            	disableLoading();
	            	$("#csvImportModel").modal("hide");
	            	showNotification("success","File Uploaded successfully..");
	            });
		   // callAjaxService("fileUpload",  callBackfileUpload,callBackFailure,request, "POST");
		});
	
	});
	
	$('#export').click(function(){
		if(!$("#frmExportImport").valid()){
			return;
		}
		$("#myModalUpload").modal("show");
		$('#importFieldSet').hide();
		$('#exportFieldSet').show();
		$('#modalHeaderExportImport').html('Export');
		$('#exportImportButton').html('Export').unbind('click').bind('click',function(){
			var url ='rest/user/reportbyfilter?env='+$("#datasource").select2('val')+'&db='+$("#database").select2('val')+'&table='+$("#datatable").select2('val');
				$('#exportFieldSet').find('input').val()?url = url+'&wherecondition='+ $('#exportFieldSet').find('input').val():'';
				$.fileDownload(url);	
		});
		
	});
	var sourceSelect = function(source){
		if (source  === "hive" || source  === "postgreSQL"){
			var request ={
					"source":source
			};
			enableLoading();
			callAjaxService("getDatabaseList",  function(response){callBackGetDatabaseList(response);},
					callBackFailure, request, "POST");
		}
	};
	var callBackGetDatabaseList = function(response){
		disableLoading();
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		if(response !== undefined 
			&& response !== null 
			){
			var databaseList = new Array();
			for (var i=0;i<response.length;i++){
				databaseList.push({"id":response[i],"text":response[i]});
			}
			$("#database").select2({
				placeholder : "Choose Database",
				data :  databaseList
			}).on("change", function(e) {
				databaseSelect(e.val);
			});			
		}
	};
	var databaseSelect = function(database){
		THIS.database = database;
		var request ={
				"source":$("#datasource").select2('val'),
				"database":database
		};
		enableLoading();
		callAjaxService("getTableList", callBackGetTableList,callBackFailure, request, "POST");
	};
	var callBackGetTableList = function(response){
		disableLoading();
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		if(response !== undefined 
			&& response !== null 
			){
			THIS.tableList = new Array();
			for (var i=0;i<response.length;i++){
				THIS.tableList.push({"id":response[i],"text":response[i]});
			}
			$("#datatable").select2({
				placeholder : "Choose table",
				data :  THIS.tableList
			});
		}
	};
}();