 var queryBuildConfig = {};
 var getQueryBuilder = function(cfg, obj, type) {
     //if (!!queryBuildConfig) {
     queryBuildConfig = new queryBuilder(cfg, obj, type);
     //}
     return queryBuildConfig;
 }

 var queryBuilder = function(cfg, obj, type) {

     var searchConfigTemplate = {
         title: '',
         advancedSearchs: [],
         data: [],
         dataViewConfig: {
             grid: false,
             card: false,
             content: false
         },
         defaultView: 'card',
         dataButtons: {
             edit: false,
             expBtn: false,
             info: false,
             compare: false,
             shortList: false,
             print: false,
             mail: false,
             download: false,
             obsolete: false
         },
         filters: {},
         findAndReplace: false,
         sortData: [],
         paginationStyle: 0,
         searchSettings: {
             schema: '',
             searchType: '',
             searchSource: '',
             searchDatabase: '',
             searchTable: ''
         },
         entities: [],
         searchFields: {},
         searchTypes: {},
         displayQuery: '',
         indexConfigObj: {},
         solrConfigFieldMap: {},
         key: '',
         description: '',
         id: 0,
         queryMap: {}
     };
     var aSearchUID = 0;
     var solrObject = {
         displaySettings: {
             queryIndex: aSearchUID,
             queryName: '',
             queryType: '',
             querySchema: [],
             queryResultView: ''
         },
         'q': '*:*',
         'qt': '/select',
         'fq': '', //[]
         'sort': '',
         'start': 0,
         'rows': 10,
         'fl': '',
         'df': '',
         'rawQuery': '',
         'wt': 'json',
         'indent': 'true',
         'isDismax': false,
         // edismax: {
         'q.alt': '',
         'qf': '',
         'mm': '',
         'pf': '',
         'ps': '',
         'qs': '',
         'tie': '',
         'bq': '',
         'bf': '',
         'uf': '',
         'pf2': '',
         'pf3': '',
         'ps2': '',
         'ps3': '',
         'boost': '',
         'stopwords': false,
         'lowercaseOperators': false,
         // },
         'isHl': false,
         // hl: {
         "hl.simple.pre": "",
         "hl.simple.post": "",
         "hl.fl": "",
         "hl.highlightMultiTerm": false,
         "hl.requireFieldMatch": false,
         "hl.usePhraseHighlighter": false,
         // },
         'defType': '',
         'isFacet': false,
         // facet: {
         "facet.field": '',
         "facet.query": '',
         "facet.prefix": '',
         // },
         'isSpatial': false,
         // spatial: {
         "pt": '',
         "sfield": '',
         "d": '',
         // },
         'isSpellCheck': false,
         // spellCheck: {
         "spellcheck.build": false,
         "spellcheck.reload": false,
         "spellcheck.dictionary": '',
         "spellcheck.count": '',
         "spellcheck.onlyMorePopular": false,
         "spellcheck.collate": false,
         "spellcheck.maxCollations": '',
         "spellcheck.maxCollationTries": '',
         "spellcheck.accuracy": '',
         "spellcheck.q": '',
         "spellcheck.extendedResults": false
             // }
     };

     var searchConfigObject = {};
     var config, data;
     var mode = 'View'; //Config & Preview
     var searchData = [];

     var defSearchTypes = {
         1: { all: 'All these Words' },
         2: { any: 'Any of these Words' },
         3: { exact: 'This exact word or phase' },
         4: { none: 'None of these words' }
     };

     var solrObjectsIndexMap = {},
         solrObjects = [],
         solrIndexNameMap = {},
         solrIndexTypeMap = {};

     var solrCollections = [];
     var currentSearch = {};
     var queryObject = {};

     activate();

     function activate() {
         if (!!!cfg) {
             searchConfigObject = JSON.parse(JSON.stringify(searchConfigTemplate)); //Deep Copy             
         } else {
             searchConfigObject = JSON.parse(JSON.stringify(cfg)); //Deep Copy....
         }

         $('#btnExecuteSolr').unbind('click').bind('click', postQuery);
     }

     function getQueryById(id) {
         var request = {
             "id": id
         };
         searchConfigObject.id = id;
         enableLoading();
         callAjaxService("getProfileConfig", callBackGetQueryById, callBackFailure, request, "POST");
     }

     function callBackGetQueryById(response) {
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             disableLoading();
             return;
         }
         if (response) {
             if (response.config) {
                 var search = JSON.parse(response.config);
                 if (search.length > 0) {
                     search = search[0];
                 }
                 if (search && search.config) {
                     searchConfigObject = JSON.parse(search.config);
                     searchConfigObject.id = obj.id;
                     $('#profilename').val(searchConfigObject.title);
                     $('#profileappname').val(searchConfigObject.appName);
                     $('#description').val(searchConfigObject.description);
                     if (searchConfigObject.solrConfigFieldMap) {
                         solrObjectsIndexMap = JSON.parse(JSON.stringify(searchConfigObject.solrConfigFieldMap));
                     }
                     console.log(solrObjectsIndexMap);
                     // if (solrObjectsIndexMap && Object.keys(solrObjectsIndexMap).length > 0) {
                     //     aSearchUID = Math.max.apply(Math, Object.keys(solrObjectsIndexMap)) + 1;
                     // }
                     $('#sourceTableDesc').text('Search : ' + searchConfigObject.title);
                     $('#solrSchema').val(solrObjectsIndexMap[0].displaySettings.querySchema);
                     bindQueryFields(aSearchUID);
                     $('#resultJson').text('');
                     $('resultJson').hide();
                     postQuery();
                     // segrigateSolrObjects('name');
                     // segrigateSolrObjects('type');
                     // bindQueryConfigurations();
                     // constructTabs();
                     //getSolrColumnList();
                 }
             }
         }
         disableLoading();
         if (mode.toLowerCase() === 'config' || mode.toLowerCase() === 'id') {
             getSolrCollections();
         }
     }

     function bindQueryFields(index) {
         if (index > -1 && !solrObjectsIndexMap[index]) {
             return;
         }
         var obj;
         if (index === -1) {
             obj = JSON.parse(JSON.stringify(solrObject));
             solrObjectsIndexMap[aSearchUID] = obj;
             aSearchUID += 1;
         } else {
             obj = solrObjectsIndexMap[index];
         }
         for (var i in obj) {
             var element = $('#frmQueryBuilder').find("[data-map='" + i + "']");
             if ($(element).is(':text') || $(element).is('textarea') || $(element).is(':input[type="number"]')) {
                 $(element).val(obj[i]);
             }
             if ($(element).is(':checkbox')) {
                 if (obj[i] === true) {
                     $(element).prop('checked', true);
                     if (i.toString().startsWith('is')) {
                         var ele = i.toString().replace('is', '').toLowerCase();
                         if ($('#' + ele).length > 0) {
                             $('#' + ele).fadeIn('fast');
                         }
                     }
                 } else {
                     $(element).prop('checked', false);
                     if (i.toString().startsWith('is')) {
                         var ele = i.toString().replace('is', '').toLowerCase();
                         if ($('#' + ele).length > 0) {
                             $('#' + ele).fadeOut('fast');
                         }
                     }
                 }
             }
         }
     }

     function postQuery() {
         enableLoading();
         if ($('#solrSchema').val() === '') {
             showNotification('error', 'Please select atleast one Solr Schema');
             return;
         }
         searchConfigObject.appName = $('#solrSchema').val().split(',')[0];
         solrObjectsIndexMap[0].displaySettings.querySchema = $('#solrSchema').val().split(',');
         solrObjectsIndexMap[0] = updateObject(JSON.parse(JSON.stringify(solrObjectsIndexMap[0])));
         var query = queryBuilder(solrObjectsIndexMap[0]);
         if (query.length === 0) {
             showNotification('error', 'Query could not be constructed.');
             return;
         }
         var request = {
             solrcollectionname: searchConfigObject.appName,
             query: JSON.stringify(queryObject)
         };

         $('#queryDiv').html('<h5>Query</h5>' + query);
         callAjaxService("solrSearch", callBackPostQuery, callBackFailure, request, "POST", null, true);
     }

     function callBackPostQuery(response) {
         disableLoading();
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             return;
         }
         $('#resultJson').show();
         response = JSON.parse(response);
         console.log(response);
         $('#resultJson').text(JSON.stringify(response, undefined, 2));
     }

     function getSolrCollections() {
         var request = {
             'source': 'solr'
         };
         enableLoading();
         callAjaxService("getDatabaseList", callBackGetSolrCollections, callBackFailure, request, "POST");
     }

     function callBackGetSolrCollections(response) {
         disableLoading();
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             return;
         }
         solrCollections = response;
         sourceSelect(solrCollections, 'solrSchema', 'Choose a Solr Schema', true);
         if (mode !== 'id') {
             bindQueryFields(-1);
             $('#solrSchema').select2('val', solrObjectsIndexMap[0].displaySettings.querySchema)
         }

     }

     function sourceSelect(source, selectId, selectText, isMultiple) {
         var databaseList = [];
         if (source && source.length > 0) {
             for (var i = 0; i < source.length; i++) {
                 databaseList.push({ "id": source[i], "text": source[i] });
             }
         }

         $("#" + selectId).select2({
             placeholder: selectText,
             data: databaseList,
             multiple: !!isMultiple
         });
     }

     function updateObject(obj) {
         //Build Object...
         $('#frmQueryBuilder :text, textarea, :checkbox, :input[type="number"]').each(function() {
             //console.log($(this).attr('data-map'));
             if (Object.keys(obj).indexOf($(this).attr('data-map')) !== -1) {
                 if ($(this).is(':checkbox')) {
                     obj[$(this).attr('data-map')] = $(this).is(':checked') ? true : false;
                 } else {
                     obj[$(this).attr('data-map')] = $(this).val();
                 }
             }
         });
         return obj;
     }

     function queryBuilder(obj) {
         if (!obj) {
             return '';
         }
         var str = '';
         queryObject = {};
         str += obj.qt;
         queryObject['qt'] = obj.qt;
         var str1 = '';
         if (obj.isDismax) {
             str1 += 'defType=edismax' + '&';
             queryObject['defType'] = 'edismax';
         }
         for (var i in obj) {
             if (i.toString() === 'displaySettings' || i.toString() === 'qt') {
                 continue;
             }
             if (obj[i] && (obj[i] !== '' || obj[i] !== false)) {
                 if (i.toString() === 'isDismax') {
                     continue;
                 } else if (i.toString().startsWith('is')) {
                     var s = i.toString().replace('is', '').toLowerCase();
                     str1 += s + '=' + obj[i].toString() + '&';
                     queryObject[s] = obj[i].toString();
                 } else {
                     str1 += i.toString() + '=' + obj[i].toString() + '&';
                     queryObject[i.toString()] = obj[i].toString();
                 }

             }
             // console.log(queryObject);
         }
         if (obj.displaySettings || obj.displaySettings.querySchema) {
             str1 += 'collection=';
             var s = '';
             for (var i = 0; i < obj.displaySettings.querySchema.length; i += 1) {
                 if (i === (obj.displaySettings.querySchema.length - 1)) {
                     str1 += obj.displaySettings.querySchema[i];
                     s += obj.displaySettings.querySchema[i];
                 } else {
                     str1 += obj.displaySettings.querySchema[i] + ',';
                     s += obj.displaySettings.querySchema[i] + ',';
                 }
                 queryObject['collection'] = s;
             }
         }
         str = str + '?' + str1;
         // console.log(obj.displaySettings.queryName + ' - ' + str);
         return str;
     }

     function saveQueryBuilder() {

         var query = queryBuilder(solrObjectsIndexMap[0]);
         queryObject['appName'] = searchConfigObject.appName;
         var request = {
             "source": searchConfigObject.searchSettings.schema.toString(),
             "database": searchConfigObject.searchSettings.schema.toString(),
             "table": '',
             "fact": 'queryBuilder',
             "customfacts": '',
             "config": JSON.stringify(searchConfigObject),
             "filters": JSON.stringify(queryObject),
             "summary": JSON.stringify(solrObjectsIndexMap[0]),
             "remarks": searchConfigObject.description,
             "displayname": searchConfigObject.title,
             "additionalconfig": '',//searchConfigObject.searchSettings.schema.toString(),
             "type": "search",
             "id": searchConfigObject.id
         };
         enableLoading;
         callAjaxService("saveProfile", callBackSaveQueryBuilder, callBackFailure, request, "POST");
     }

     function callBackSaveQueryBuilder(response) {
         disableLoading();
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             return;
         }
         showNotification('success', 'Query saved Successfully');
         var request = {
        		 id: response,
        		 name: $('#profilename').val(),
        		 query: JSON.stringify(queryObject)
         }
         callAjaxService("updateQueryCache", function(){}, callBackFailure, request, "POST");
         
         $('#myModalSaveSearch').modal('hide');
     }

     return {
         initConfig: function(id) {
             $('#frmQueryBuilder :checkbox').unbind('click').bind('click', function() {
                 if ($(this).attr('data-map') && $(this).attr('data-map').startsWith('is')) {
                     if ($('#' + $(this).attr('data-map').replace('is', '').toLowerCase()).length > 0) {
                         $('#' + $(this).attr('data-map').replace('is', '').toLowerCase()).toggle('fade');
                     }
                 }
             });
             if (!id || id === 0) {
                 getSolrCollections();
             } else {
                 mode = 'id';
                 getQueryById(id);
             }
             $('#btnSaveQuery').unbind('click').bind('click', function() {
                 if ($('#solrSchema').val() === '') {
                     showNotification('error', 'Select a Solr Schema');
                     return;
                 }
                 $('#myModalSaveSearch').modal('show');
             });
             $('#btnSaveSearch').unbind('click').bind('click', function(e) {
                 e.preventDefault();
                 if (mode !== 'view') {
                     if (!$('#profilename').val()) {
                         showNotification("error", 'Enter profile name');
                         return;
                     }
                     searchConfigObject.title = $('#profilename').val();
                     searchConfigObject.description = $('#description').val();
                     searchConfigObject.solrConfigFieldMap = solrObjectsIndexMap;
                     searchConfigObject.searchSettings.schema = $('#solrSchema').val().toString();
                     searchConfigObject.appName = $('#solrSchema').val().split(',')[0];
                     solrObjectsIndexMap[0].displaySettings.querySchema = $('#solrSchema').val().split(',');
                     solrObjectsIndexMap[0] = updateObject(JSON.parse(JSON.stringify(solrObjectsIndexMap[0])));
                     queryBuilder(solrObjectsIndexMap[0]);
                     searchConfigObject.queryMap = queryObject;
                     saveQueryBuilder();
                 }
             })
         },
         initView: function(id) {
             $('#frmQueryBuilder :checkbox').unbind('click').bind('click', function() {
                 if ($(this).attr('data-map') && $(this).attr('data-map').startsWith('is')) {
                     if ($('#' + $(this).attr('data-map').replace('is', '').toLowerCase()).length > 0) {
                         $('#' + $(this).attr('data-map').replace('is', '').toLowerCase()).toggle('fade');
                     }
                 }
             });
         }
     }
 }
