/*
 __                   .__
 |__|____ __________  _|__| ______
 |  \__  \\_  __ \  \/ /  |/  ___/
 |  |/ __ \|  | \/\   /|  |\___ \
 /\__|  (____  /__|    \_/ |__/____  >
 \______|    \/                    \/

 Copyright 2013 - SmartAdmin Template

 * This script is part of an item on wrapbootstrap.com
 * https://wrapbootstrap.com/user/myorange
 *
 * Date 	 : Jan 2014
 * Updated	 : 12/12/2013
 * Dependency: jQuery UI core, json2(ie7)
 *
 * ************************************************************* *
 *
 * Jarvis Widgets (AKA Power Widgets), is originally created by
 * Mark (www.creativemilk.net) and Sunny (bootstraphunter.com).
 * This script may NOT be RESOLD or REDISTRUBUTED under any
 * circumstances, and is only to be used with this purchased
 * copy of SmartAdmin Template.
 *
 * ************************************************************* */
;
(function ($, window, document, undefined) {

    //"use strict"; // jshint ;_;

    var pluginName = 'jarvisWidgets';

    function Plugin(element, options) {
        /**
         * Variables.
         **/
        this.obj = $(element);
        this.o = $.extend({}, $.fn[pluginName].defaults, options);
        this.objId = this.obj.attr('id');
        this.pwCtrls = '.jarviswidget-ctrls'
        this.widget = this.obj.find(this.o.widgets);
        this.toggleClass = this.o.toggleClass.split('|');
        this.editClass = this.o.editClass.split('|');
        this.fullscreenClass = this.o.fullscreenClass.split('|');
        this.customClass = this.o.customClass.split('|');

        this.init();
    };

    Plugin.prototype = {

        /**
         * Important settings like storage and touch support.
         *
         * @param:
         **/
        _settings: function () {

            var self = this;
           // console.log(self);

            //*****************************************************************//
            //////////////////////// LOCALSTORAGE CHECK /////////////////////////
            //*****************************************************************//

            storage = !! function () {
                var result, uid = +new Date;
                try {
                    localStorage.setItem(uid, uid);
                    result = localStorage.getItem(uid) == uid;
                    localStorage.removeItem(uid);
                    return result;
                } catch (e) {}
            }() && localStorage;

            //*****************************************************************//
            /////////////////////////// SET/GET KEYS ////////////////////////////
            //*****************************************************************//

            // TODO : Push state does not work on IE9, try to find a way to detect IE and use a seperate filter


            if (storage && self.o.localStorage) {

                if (self.o.ajaxnav === true) {
                    widget_url = location.hash.replace(/^#/, '')

                    keySettings = 'Plugin_settings_' + widget_url + '_' + self.objId;
                    getKeySettings = localStorage.getItem(keySettings);

                    keyPosition = 'Plugin_position_' + widget_url + '_' + self.objId;
                    getKeyPosition = localStorage.getItem(keyPosition);

                    //console.log("from jarvis widget " + widget_url);
                    //console.log(self.o.ajaxnav + " if")

                } else {

                    keySettings = 'jarvisWidgets_settings_' + location.pathname + '_' + self.objId;
                    getKeySettings = localStorage.getItem(keySettings);

                    keyPosition = 'jarvisWidgets_position_' + location.pathname + '_' + self.objId;
                    getKeyPosition = localStorage.getItem(keyPosition);
                    //console.log(self.o.ajaxnav + " else")

                } // end else

            } // end if



            //*****************************************************************//
            ////////////////////////// TOUCH SUPPORT ////////////////////////////
            //*****************************************************************//

            /**
             * Check for touch support and set right click events.
             **/
            if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
                clickEvent = 'touchstart';
                //click tap
            } else {
                clickEvent = 'click';
            }

        },

        /**
         * Function for the indicator image.
         *
         * @param:
         **/
        _runLoaderWidget: function (elm) {
            var self = this;
            if (self.o.indicator === true) {
                $($(elm.parents(self.o.widgets).first())
                    .find('.jarviswidget-loader').first())
                    .stop(true, true)
                    .fadeIn(100)
                    .delay(self.o.indicatorTime)
                    .fadeOut(100);
            }
        },

        /**
         * Create a fixed timestamp.
         *
         * @param: t | date | Current date.
         **/
        _getPastTimestamp: function (t) {

            var self = this;

            var da = new Date(t);

            /**
             * Get and set the date and time.
             **/
            tsMonth = da.getMonth() + 1;
            // index based
            tsDay = da.getDate();
            tsYear = da.getFullYear();
            tsHours = da.getHours();
            tsMinutes = da.getMinutes();
            tsSeconds = da.getUTCSeconds();

            /**
             * Checking for one digit values, if so add an zero.
             **/
            if (tsMonth < 10) {
                var tsMonth = '0' + tsMonth
            }
            if (tsDay < 10) {
                var tsDay = '0' + tsDay
            }
            if (tsHours < 10) {
                var tsHours = '0' + tsHours
            }
            if (tsMinutes < 10) {
                var tsMinutes = '0' + tsMinutes
            }
            if (tsSeconds < 10) {
                var tsSeconds = '0' + tsSeconds
            }

            /**
             * The output, how you want it.
             **/
            var format = self.o.timestampFormat.replace(/%d%/g, tsDay)
                .replace(/%m%/g, tsMonth)
                .replace(/%y%/g, tsYear)
                .replace(/%h%/g, tsHours)
                .replace(/%i%/g, tsMinutes)
                .replace(/%s%/g, tsSeconds);

            return format;
        },

        /**
         * AJAX load File, which get and shows the .
         *
         * @param: awidget | object  | The widget.
         * @param: file    | file    | The file thats beeing loaded.
         * @param: loader  | object  | The widget.
         **/
        _loadAjaxFile: function (awidget, file, loader) {

            var self = this

            awidget.find('.widget-body')
                .load(file, function (response, status, xhr) {

                    $this = $(this);

                    /**
                     * If action runs into an error display an error msg.
                     **/
                    if (status == "error") {
                        $this.html('<h4 class="alert alert-danger">' + self.o.labelError + '<b> ' +
                            xhr.status + " " + xhr.statusText + '</b></h4>');
                    }

                    /**
                     * Run if there are no errors.
                     **/
                    if (status == "success") {

                        /**
                         * Show a timestamp.
                         **/
                        var aPalceholder = awidget.find(self.o.timestampPlaceholder);

                        if (aPalceholder.length) {

                            aPalceholder.html(self._getPastTimestamp(new Date()));
                        }

                        /**
                         * Run the callback function.
                         **/
                        if (typeof self.o.afterLoad == 'function') {
                            self.o.afterLoad.call(this, awidget);
                        }
                    }
                });

            /**
             * Run function for the indicator image.
             **/
            self._runLoaderWidget(loader);

        },

        /**
         * Save all settings to the localStorage.
         *
         * @param:
         **/
        _saveSettingsWidget: function () {

            var self = this;

            self._settings();

            if (storage && self.o.localStorage) {
                var storeSettings = [];

                self.obj.find(self.o.widgets)
                    .each(function () {
                        var storeSettingsStr = {};
                        storeSettingsStr['id'] = $(this)
                            .attr('id');
                        storeSettingsStr['style'] = $(this)
                            .attr('data-widget-attstyle');
                        storeSettingsStr['title'] = $(this)
                            .children('header')
                            .children('h2')
                            .text();
                        storeSettingsStr['hidden'] = ($(this)
                            .is(':hidden') ? 1 : 0);
                        storeSettingsStr['collapsed'] = ($(this)
                            .hasClass('jarviswidget-collapsed') ? 1 : 0);
                        storeSettings.push(storeSettingsStr);
                    });

                var storeSettingsObj = JSON.stringify({
                    'widget': storeSettings
                });

                /* Place it in the storage(only if needed) */
                if (getKeySettings != storeSettingsObj) {
                    localStorage.setItem(keySettings, storeSettingsObj);
                }
            }

            /**
             * Run the callback function.
             **/
            if (typeof self.o.onSave == 'function') {
                self.o.onSave.call(this, null, storeSettingsObj);
            }
        },

        /**
         * Save positions to the localStorage.
         *
         * @param:
         **/
        _savePositionWidget: function () {

            var self = this;

            self._settings();

            if (storage && self.o.localStorage) {
                var mainArr = [];

                self.obj.find(self.o.grid + '.sortable-grid')
                    .each(function () {
                        var subArr = [];
                        $(this)
                            .children(self.o.widgets)
                            .each(function () {
                                var subObj = {};
                                subObj['id'] = $(this)
                                    .attr('id');
                                subArr.push(subObj);
                            });
                        var out = {
                            'section': subArr
                        }
                        mainArr.push(out);
                    });

                var storePositionObj = JSON.stringify({
                    'grid': mainArr
                });

                /* Place it in the storage(only if needed) */
                if (getKeyPosition != storePositionObj) {
                    localStorage.setItem(keyPosition, storePositionObj, null);
                }
            }

            /**
             * Run the callback function.
             **/
            if (typeof self.o.onSave == 'function') {
                self.o.onSave.call(this, storePositionObj);
            }
        },

        /**
         * Code that we run at the start.
         *
         * @param:
         **/
        init: function () {

            var self = this;

            self._settings();

            /**
             * Force users to use an id(it's needed for the local storage).
             **/
            if (!$('#' + self.objId)
                .length) {
                alert('It looks like your using a class instead of an ID, dont do that!')
            }

            /**
             * Add RTL support.
             **/
            if (self.o.rtl === true) {
                $('body')
                    .addClass('rtl');
            }

            /**
             * This will add an extra class that we use to store the
             * widgets in the right order.(savety)
             **/

            $(self.o.grid)
                .each(function () {
                    if ($(this)
                        .find(self.o.widgets)
                        .length) {
                        $(this)
                            .addClass('sortable-grid');
                    }
                });

            //*****************************************************************//
            //////////////////////// SET POSITION WIDGET ////////////////////////
            //*****************************************************************//

            /**
             * Run if data is present.
             **/
            if (storage && self.o.localStorage && getKeyPosition) {

                var jsonPosition = JSON.parse(getKeyPosition);

                /**
                 * Loop the data, and put every widget on the right place.
                 **/
                for (var key in jsonPosition.grid) {
                    var changeOrder = self.obj.find(self.o.grid + '.sortable-grid')
                        .eq(key);
                    for (var key2 in jsonPosition.grid[key].section) {
                        changeOrder.append($('#' + jsonPosition.grid[key].section[key2].id));
                    }
                }

            }

            //*****************************************************************//
            /////////////////////// SET SETTINGS WIDGET /////////////////////////
            //*****************************************************************//

            /**
             * Run if data is present.
             **/
            if (storage && self.o.localStorage && getKeySettings) {

                var jsonSettings = JSON.parse(getKeySettings);

                /**
                 * Loop the data and hide/show the widgets and set the inputs in
                 * panel to checked(if hidden) and add an indicator class to the div.
                 * Loop all labels and update the widget titles.
                 **/
                for (var key in jsonSettings.widget) {
                    var widgetId = $('#' + jsonSettings.widget[key].id);

                    /**
                     * Set a style(if present).
                     **/
                    if (jsonSettings.widget[key].style) {
                        //console.log("test");
                        widgetId.removeClassPrefix('jarviswidget-color-')
                            .addClass(jsonSettings.widget[key].style)
                            .attr('data-widget-attstyle', '' + jsonSettings.widget[key].style + '');
                    }

                    /**
                     * Hide/show widget.
                     **/
                    if (jsonSettings.widget[key].hidden == 1) {
                        widgetId.hide(1);
                    } else {
                        widgetId.show(1)
                            .removeAttr('data-widget-hidden');
                    }

                    /**
                     * Toggle content widget.
                     **/
                    if (jsonSettings.widget[key].collapsed == 1) {
                        widgetId.addClass('jarviswidget-collapsed')
                            .children('div')
                            .hide(1);
                    }

                    /**
                     * Update title widget (if needed).
                     **/
                    if (widgetId.children('header')
                        .children('h2')
                        .text() != jsonSettings.widget[key].title) {
                        widgetId.children('header')
                            .children('h2')
                            .text(jsonSettings.widget[key].title);
                    }
                }
            }

            //*****************************************************************//
            ////////////////////////// LOOP AL WIDGETS //////////////////////////
            //*****************************************************************//

            /**
             * This will add/edit/remove the settings to all widgets
             **/
            self.widget.each(function () {

                var tWidget = $(this);
                var thisHeader = $(this)
                    .children('header');

                /**
                 * Dont double wrap(check).
                 **/
                if (!thisHeader.parent()
                    .attr('role')) {

                    /**
                     * Hide the widget if the dataset 'widget-hidden' is set to true.
                     **/
                    if (tWidget.data('widget-hidden') === true) {

                        tWidget.hide();
                    }

                    /**
					 * Hide the content of the widget if the dataset
					 * 'widget-collapsed' is set to true.

					 **/
                    if (tWidget.data('widget-collapsed') === true) {
                        tWidget.addClass('jarviswidget-collapsed')
                            .children('div')
                            .hide();
                    }

                    /**
                     * Check for the dataset 'widget-icon' if so get the icon
                     * and attach it to the widget header.
                     * NOTE: MOVED THIS TO PHYSICAL for more control
                     **/
                    //if(tWidget.data('widget-icon')){
                    //	thisHeader.prepend('<i class="jarviswidget-icon '+tWidget.data('widget-icon')+'"></i>');
                    //}

                    /**
                     * Add a delete button to the widget header (if set to true).
                     **/
                    if (self.o.customButton === true && tWidget.data('widget-custombutton') ===
                        undefined && self.customClass[0].length != 0) {
                        var customBtn =
                            '<a href="javascript:void(0);" class="button-icon jarviswidget-custom-btn"><i class="' +
                            self.customClass[0] + '"></i></a>';
                    } else {
                        customBtn = '';
                    }

                    /**
                     * Add a delete button to the widget header (if set to true).
                     **/
                    if (self.o.deleteButton === true && tWidget.data('widget-deletebutton') ===
                        undefined) {
                        var deleteBtn =
                            '<a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="Delete" data-placement="bottom"><i class="' +
                            self.o.deleteClass + '"></i></a>';
                    } else {
                        deleteBtn = '';
                    }

                    /**
                     * Add a delete button to the widget header (if set to true).
                     **/
                    if (self.o.editButton === true && tWidget.data('widget-editbutton') === undefined) {
                        var editBtn =
                            '<a href="javascript:void(0);" class="button-icon jarviswidget-edit-btn" rel="tooltip" title="Edit Title" data-placement="bottom"><i class="' +
                            self.editClass[0] + '"></i></a>';
                    } else {
                        editBtn = '';
                    }

                    /**
                     * Add a delete button to the widget header (if set to true).
                     **/
                    if (self.o.fullscreenButton === true && tWidget.data('widget-fullscreenbutton') ===
                        undefined) {
                        var fullscreenBtn =
                            '<a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="Fullscreen" data-placement="bottom"><i class="' +
                            self.fullscreenClass[0] + '"></i></a>';
                    } else {
                        fullscreenBtn = '';
                    }

                    /**
                     * Add a delete button to the widget header (if set to true).
                     **/
                    if (self.o.colorButton === true && tWidget.data('widget-colorbutton') ===
                        undefined) {
                        var widgetcolorBtn =
                            '<a data-toggle="dropdown" class="dropdown-toggle color-box selector" href="javascript:void(0);"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span class="bg-color-green" data-widget-setstyle="jarviswidget-color-green" rel="tooltip" data-placement="left" data-original-title="Green Grass"></span></li><li><span class="bg-color-greenDark" data-widget-setstyle="jarviswidget-color-greenDark" rel="tooltip" data-placement="top" data-original-title="Dark Green"></span></li><li><span class="bg-color-greenLight" data-widget-setstyle="jarviswidget-color-greenLight" rel="tooltip" data-placement="top" data-original-title="Light Green"></span></li><li><span class="bg-color-purple" data-widget-setstyle="jarviswidget-color-purple" rel="tooltip" data-placement="top" data-original-title="Purple"></span></li><li><span class="bg-color-magenta" data-widget-setstyle="jarviswidget-color-magenta" rel="tooltip" data-placement="top" data-original-title="Magenta"></span></li><li><span class="bg-color-pink" data-widget-setstyle="jarviswidget-color-pink" rel="tooltip" data-placement="right" data-original-title="Pink"></span></li><li><span class="bg-color-pinkDark" data-widget-setstyle="jarviswidget-color-pinkDark" rel="tooltip" data-placement="left" data-original-title="Fade Pink"></span></li><li><span class="bg-color-blueLight" data-widget-setstyle="jarviswidget-color-blueLight" rel="tooltip" data-placement="top" data-original-title="Light Blue"></span></li><li><span class="bg-color-teal" data-widget-setstyle="jarviswidget-color-teal" rel="tooltip" data-placement="top" data-original-title="Teal"></span></li><li><span class="bg-color-blue" data-widget-setstyle="jarviswidget-color-blue" rel="tooltip" data-placement="top" data-original-title="Ocean Blue"></span></li><li><span class="bg-color-blueDark" data-widget-setstyle="jarviswidget-color-blueDark" rel="tooltip" data-placement="top" data-original-title="Night Sky"></span></li><li><span class="bg-color-darken" data-widget-setstyle="jarviswidget-color-darken" rel="tooltip" data-placement="right" data-original-title="Night"></span></li><li><span class="bg-color-yellow" data-widget-setstyle="jarviswidget-color-yellow" rel="tooltip" data-placement="left" data-original-title="Day Light"></span></li><li><span class="bg-color-orange" data-widget-setstyle="jarviswidget-color-orange" rel="tooltip" data-placement="bottom" data-original-title="Orange"></span></li><li><span class="bg-color-orangeDark" data-widget-setstyle="jarviswidget-color-orangeDark" rel="tooltip" data-placement="bottom" data-original-title="Dark Orange"></span></li><li><span class="bg-color-red" data-widget-setstyle="jarviswidget-color-red" rel="tooltip" data-placement="bottom" data-original-title="Red Rose"></span></li><li><span class="bg-color-redLight" data-widget-setstyle="jarviswidget-color-redLight" rel="tooltip" data-placement="bottom" data-original-title="Light Red"></span></li><li><span class="bg-color-white" data-widget-setstyle="jarviswidget-color-white" rel="tooltip" data-placement="right" data-original-title="Purity"></span></li><li><a href="javascript:void(0);" class="jarviswidget-remove-colors" data-widget-setstyle="" rel="tooltip" data-placement="bottom" data-original-title="Reset widget color to default">Remove</a></li></ul>';
                        thisHeader.prepend('<div class="widget-toolbar">' + widgetcolorBtn + '</div>');

                    } else {
                        widgetcolorBtn = '';
                    }

                    /**
                     * Add a toggle button to the widget header (if set to true).
                     **/
                    if (self.o.toggleButton === true && tWidget.data('widget-togglebutton') ===
                        undefined) {
                        if (tWidget.data('widget-collapsed') === true || tWidget.hasClass(
                            'jarviswidget-collapsed')) {
                            var toggleSettings = self.toggleClass[1];
                        } else {
                            toggleSettings = self.toggleClass[0];
                        }
                        var toggleBtn =
                            '<a href="#" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="Collapse" data-placement="bottom"><i class="' +
                            toggleSettings + '"></i></a>';
                    } else {
                        toggleBtn = '';
                    }

                    /**
                     * Add a refresh button to the widget header (if set to true).
                     **/
                    if (self.o.refreshButton === true && tWidget.data('widget-refreshbutton') !=
                        false && tWidget.data('widget-load')) {
                        var refreshBtn =
                            '<a href="#" class="button-icon jarviswidget-refresh-btn" data-loading-text="&nbsp;&nbsp;Loading...&nbsp;" rel="tooltip" title="Refresh" data-placement="bottom"><i class="' +
                            self.o.refreshButtonClass + '"></i></a>';
                    } else {
                        refreshBtn = '';
                    }

                    /**
                     * Set the buttons order.
                     **/
                    var formatButtons = self.o.buttonOrder.replace(/%refresh%/g, refreshBtn)
                        .replace(/%delete%/g, deleteBtn)
                        .replace(/%custom%/g, customBtn)
                        .replace(/%fullscreen%/g, fullscreenBtn)
                        .replace(/%edit%/g, editBtn)
                        .replace(/%toggle%/g, toggleBtn);

                    /**
                     * Add a button wrapper to the header.
                     **/
                    if (refreshBtn != '' || deleteBtn != '' || customBtn != '' || fullscreenBtn != '' ||
                        editBtn != '' || toggleBtn != '') {
                        thisHeader.prepend('<div class="jarviswidget-ctrls">' + formatButtons +
                            '</div>');
                    }

                    /**
                     * Adding a helper class to all sortable widgets, this will be
                     * used to find the widgets that are sortable, it will skip the widgets
                     * that have the dataset 'widget-sortable="false"' set to false.
                     **/
                    if (self.o.sortable === true && tWidget.data('widget-sortable') === undefined) {
                        tWidget.addClass('jarviswidget-sortable');
                    }

                    /**
                     * If the edit box is present copy the title to the input.
                     **/
                    if (tWidget.find(self.o.editPlaceholder)
                        .length) {
                        tWidget.find(self.o.editPlaceholder)
                            .find('input:first')
                            .val($.trim(thisHeader.children('h2')
                                .text()));
                    }

                    /**
                     * Prepend the image to the widget header.
                     **/
                    thisHeader.append(
                        '<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>'
                    );

                    /**
                     * Adding roles to some parts.
                     **/
                    tWidget.attr('role', 'widget')
                        .children('div')
                        .attr('role', 'content')
                        .prev('header')
                        .attr('role', 'heading')
                        .children('div')
                        .attr('role', 'menu');
                }
            });

            /**
             * Hide all buttons if option is set to true.
             **/
            if (self.o.buttonsHidden === true) {
                $(self.o.pwCtrls)
                    .hide();
            }

            /* activate all tooltips */
            $(".jarviswidget header [rel=tooltip]")
                .tooltip();

            //******************************************************************//
            //////////////////////////////// AJAX ////////////////////////////////
            //******************************************************************//

            /**
             * Loop all ajax widgets.
             **/
            self.obj.find('[data-widget-load]')
                .each(function () {

                    /**
                     * Variables.
                     **/
                    var thisItem = $(this),
                        thisItemHeader = thisItem.children(),
                        pathToFile = thisItem.data('widget-load'),
                        reloadTime = thisItem.data('widget-refresh') * 1000,
                        ajaxLoader = thisItem.children();

                    if (!thisItem.find('.jarviswidget-ajax-placeholder')
                        .length) {

                        /**
                         * Append a AJAX placeholder.
                         **/
                        thisItem.children('widget-body')
                            .append('<div class="jarviswidget-ajax-placeholder">' + self.o.loadingLabel +
                                '</div>');

                        /**
                         * If widget has a reload time refresh the widget, if the value
                         * has been set to 0 dont reload.
                         **/
                        if (thisItem.data('widget-refresh') > 0) {

                            /**
                             * Load file on start.
                             **/
                            self._loadAjaxFile(thisItem, pathToFile, thisItemHeader);

                            /**
                             * Set an interval to reload the content every XXX seconds.
                             **/
                            setInterval(function () {

                                self._loadAjaxFile(thisItem, pathToFile, thisItemHeader);
                            }, reloadTime);
                        } else {

                            /**
                             * Load the content just once.
                             **/
                            self._loadAjaxFile(thisItem, pathToFile, thisItemHeader);

                        }
                    }
                });

            //******************************************************************//
            ////////////////////////////// SORTABLE //////////////////////////////
            //******************************************************************//

            /**
             * jQuery UI soratble, this allows users to sort the widgets.
             * Notice that this part needs the jquery-ui core to work.
             **/
            if (self.o.sortable === true && jQuery.ui) {
                var sortItem = self.obj.find('.sortable-grid')
                    .not('[data-widget-excludegrid]');
                sortItem.sortable({
                    items: $(sortItem.find('.jarviswidget-sortable')).parent("div"),
                    connectWith: sortItem,
                    placeholder: self.o.placeholderClass,
                    cursor: 'move',
                    revert: true,
                    opacity: self.o.opacity,
                    delay: 200,
                    cancel: '.button-icon, #jarviswidget-fullscreen-mode > div',
                    zIndex: 10000,
                    handle: sortItem.find('.jarviswidget-sortable > header'),
                    forcePlaceholderSize: true,
                    forceHelperSize: true,
                    update: function (event, ui) {
                        /* run pre-loader in the widget */
                        self._runLoaderWidget(ui.item.children());
                        /* store the positions of the plugins */
                        self._savePositionWidget();
                        /**
                         * Run the callback function.
                         **/
                        if (typeof self.o.onChange == 'function') {
                            self.o.onChange.call(this, ui.item);
                        }
                    }
                });
            }

            //*****************************************************************//
            ////////////////////////// BUTTONS VISIBLE //////////////////////////
            //*****************************************************************//

            /**
             * Show and hide the widget control buttons, the buttons will be
             * visible if the users hover over the widgets header. At default the
             * buttons are always visible.
             **/
            if (self.o.buttonsHidden === true) {

                /**
                 * Show and hide the buttons.
                 **/
                self.widget.children('header')
                    .hover(function () {
                        $(this)
                            .children(self.o.pwCtrls)
                            .stop(true, true)
                            .fadeTo(100, 1.0);
                    }, function () {
                        $(this)
                            .children(self.o.pwCtrls)
                            .stop(true, true)
                            .fadeTo(100, 0.0);
                    });
            }

            //*****************************************************************//
            ///////////////////////// CLICKEVENTS //////////////////////////
            //*****************************************************************//

            self._clickEvents();

            //*****************************************************************//
            ///////////////////// DELETE LOCAL STORAGE KEYS /////////////////////
            //*****************************************************************//

            /**
             * Delete the settings key.
             **/
            $(self.o.deleteSettingsKey)
                .on(clickEvent, this, function (e) {
                    if (storage && self.o.localStorage) {
                        var cleared = confirm(self.o.settingsKeyLabel);
                        if (cleared) {
                            localStorage.removeItem(keySettings);
                        }
                    }
                    e.preventDefault();
                });

            /**
             * Delete the position key.
             **/
            $(self.o.deletePositionKey)
                .on(clickEvent, this, function (e) {
                    if (storage && self.o.localStorage) {
                        var cleared = confirm(self.o.positionKeyLabel);
                        if (cleared) {
                            localStorage.removeItem(keyPosition);
                        }
                    }
                    e.preventDefault();
                });

            //*****************************************************************//
            ///////////////////////// CREATE NEW KEYS  //////////////////////////
            //*****************************************************************//

            /**
             * Create new keys if non are present.
             **/
            if (storage && self.o.localStorage) {

                /**
                 * If the local storage key (keySettings) is empty or
                 * does not excite, create one and fill it.
                 **/
                if (getKeySettings === null || getKeySettings.length < 1) {
                    self._saveSettingsWidget();
                }

                /**
                 * If the local storage key (keyPosition) is empty or
                 * does not excite, create one and fill it.
                 **/
                if (getKeyPosition === null || getKeyPosition.length < 1) {
                    self._savePositionWidget();
                }
            }

        },

        /**
         * All of the click events.
         *
         * @param:
         **/
        _clickEvents: function () {

            var self = this;

            self._settings();

            //*****************************************************************//
            /////////////////////////// TOGGLE WIDGETS //////////////////////////
            //*****************************************************************//

            /**
             * Allow users to toggle the content of the widgets.
             **/
           // console.log(self.widget);
            self.widget.off('click').on(clickEvent, '.jarviswidget-toggle-btn', function (e) {

                var tWidget = $(this);
                var pWidget = tWidget.parents(self.o.widgets).first();

                /**
                 * Run function for the indicator image.
                 **/
                self._runLoaderWidget(tWidget);

                /**
                 * Change the class and hide/show the widgets content.
                 **/
                if (pWidget.hasClass('jarviswidget-collapsed')) {
                    $(tWidget.children()
                        .removeClass(self.toggleClass[1])
                        .addClass(self.toggleClass[0])
                        .parents(self.o.widgets).first())
                        .removeClass('jarviswidget-collapsed')
                        .children('[role=content]')
                        .slideDown(self.o.toggleSpeed, function () {
                            self._saveSettingsWidget();
                        });
                } else {
                    $(tWidget.children()
                        .removeClass(self.toggleClass[0])
                        .addClass(self.toggleClass[1])
                        .parents(self.o.widgets).first())
                        .addClass('jarviswidget-collapsed')
                        .children('[role=content]')
                        .slideUp(self.o.toggleSpeed, function () {
                            self._saveSettingsWidget();
                        });
                }

                /**
                 * Run the callback function.
                 **/
                if (typeof self.o.onToggle == 'function') {
                    self.o.onToggle.call(this, pWidget);
                }

                e.preventDefault();
            });

            //*****************************************************************//
            ///////////////////////// FULLSCREEN WIDGETS ////////////////////////
            //*****************************************************************//

            /**
             * Set fullscreen height function.
             **/
            function heightFullscreen() {
                if ($('#jarviswidget-fullscreen-mode')
                    .length) {

                    /**
                     * Setting height variables.
                     **/
                    var heightWindow = $(window)
                        .height();
                    var heightHeader = $('#jarviswidget-fullscreen-mode')
                        .find(self.o.widgets)
                        .children('header')
                        .height();

                    /**
                     * Setting the height to the right widget.
                     **/
                    $('#jarviswidget-fullscreen-mode')
                        .find(self.o.widgets)
                        .children('div:first')
                        .height(heightWindow - heightHeader - 15);
                }
            }

            /**
             * On click go to fullscreen mode.
             **/
            self.widget.on(clickEvent, '.jarviswidget-fullscreen-btn', function (e) {
            	
                var thisWidget = $(this)
                    .parents(self.o.widgets).first();
                var thisWidgetContent = thisWidget.children('div');

                /**
                 * Run function for the indicator image.
                 **/
                self._runLoaderWidget($(this));

                /**
                 * Wrap the widget and go fullsize.
                 **/
                if ($('#jarviswidget-fullscreen-mode')
                    .length) {

                    /**
                     * Remove class from the body.
                     **/
                    $('.nooverflow')
                        .removeClass('nooverflow');

                    /**
                     * Unwrap the widget, remove the height, set the right
                     * fulscreen button back, and show all other buttons.
                     **/
                   $( thisWidget.unwrap('<div>')
                        .children('div')
                        .removeAttr('style')
                        .end()
                        .find('.jarviswidget-fullscreen-btn')
                        .children()
                        .removeClass(self.fullscreenClass[1])
                        .addClass(self.fullscreenClass[0])
                        .parents(self.pwCtrls).first())
                        .children('a')
                        .show();

                    /**
                     * Reset collapsed widgets.
                     **/
                    if (thisWidgetContent.hasClass('jarviswidget-visible')) {
                        thisWidgetContent.hide()
                            .removeClass('jarviswidget-visible');
                    }

                } else {

                    /**
                     * Prevent the body from scrolling.
                     **/
                    $('body')
                        .addClass('nooverflow');

                    /**
					 * Wrap, append it to the body, show the right button

					 * and hide all other buttons.
					 **/
                    $(thisWidget.wrap('<div id="jarviswidget-fullscreen-mode"/>')
                        .parent()
                        .find('.jarviswidget-fullscreen-btn')
                        .children()
                        .removeClass(self.fullscreenClass[0])
                        .addClass(self.fullscreenClass[1])
                        .parents(self.pwCtrls).first())
                        .children('a:not(.jarviswidget-fullscreen-btn)')
                        .hide();

                    /**
                     * Show collapsed widgets.
                     **/
                    if (thisWidgetContent.is(':hidden')) {
                        thisWidgetContent.show()
                            .addClass('jarviswidget-visible');
                    }
                }

                /**
                 * Run the set height function.
                 **/
                heightFullscreen();

                /**
                 * Run the callback function.
                 **/
                if (typeof self.o.onFullscreen == 'function') {
                    self.o.onFullscreen.call(this, thisWidget);
                }

                e.preventDefault();
            });

            /**
             * Run the set fullscreen height function when the screen resizes.
             **/
            $(window)
                .resize(function () {

                    /**
                     * Run the set height function.
                     **/
                    heightFullscreen();
                });

            //*****************************************************************//
            //////////////////////////// EDIT WIDGETS ///////////////////////////
            //*****************************************************************//

            /**
             * Allow users to show/hide a edit box.
             **/
            self.widget.on(clickEvent, '.jarviswidget-edit-btn', function (e) {

                var tWidget = $(this)
                    .parents(self.o.widgets).first();

                /**
                 * Run function for the indicator image.
                 **/
                self._runLoaderWidget($(this));

                /**
                 * Show/hide the edit box.
                 **/
                if (tWidget.find(self.o.editPlaceholder)
                    .is(':visible')) {
                    $($(this)
                        .children()
                        .removeClass(self.editClass[1])
                        .addClass(self.editClass[0])
                        .parents(self.o.widgets).first())
                        .find(self.o.editPlaceholder)
                        .slideUp(self.o.editSpeed, function () {
                            self._saveSettingsWidget();
                        });
                } else {
                    $($(this)
                        .children()
                        .removeClass(self.editClass[0])
                        .addClass(self.editClass[1])
                        .parents(self.o.widgets).first())
                        .find(self.o.editPlaceholder)
                        .slideDown(self.o.editSpeed);
                }

                /**
                 * Run the callback function.
                 **/
                if (typeof self.o.onEdit == 'function') {
                    self.o.onEdit.call(this, tWidget);
                }

                e.preventDefault();
            });

            /**
             * Update the widgets title by using the edit input.
             **/
            /*var axisLabelClick = function(obj){
        		var popover = $(obj).siblings('div.popover');
        		var chartType = $(obj).data("charttype");
        		var popoverContent = $(".popover-content > .smart-form",popover);
        	
        		var tempOption = $("<div/>");
        		tempOption.append($("<option/>").attr("value","").html("Choose"));
        		if (THIS.MeasureList && THIS.MeasureList.length > 0){
        			for (var i=0; i< THIS.MeasureList.length; i++){
        				tempOption.append($("<option/>").attr("value",THIS.MeasureList[i]).html(THIS.MeasureList[i]));
        			}
        		}
        		if (chartType !==  "chart-bubble" && chartType !== "chart-composite"  && chartType !== "chart-bar" && chartType !== "chart-grouped-bar"){
        			popoverContent.append($("<div/>").addClass("note").append($("<strong/>").html("Measure"))).append($("<label/>").addClass("input").append($("<select/>").addClass("aggregate-select").append(tempOption.html()).attr({type:"text",name:"group"}).addClass("input-xs").val($(obj).data("group"))));
        			var popoverMeasure= $("<div/>").addClass("div-3").appendTo(popoverContent);
        			popoverMeasure
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Order By"))).append($("<label/>").addClass("width-100per").append($("<select/>").addClass("aggregate-select")
        					.append($("<option/>").attr("value","0").html("None"))
        					.append($("<option/>").attr("value","-1").html("DESC"))
        					.append($("<option/>").attr("value","1").html("ASC"))
        					.attr({name:"orderby"}).addClass("input-xs").val($(obj).data("orderby"))).append($("<i/>"))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Format"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"numformat"}).addClass("input-xs").val($(obj).data("numformat")))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Aggregate"))).append($("<label/>").addClass("width-100per").append($("<select/>").addClass("aggregate-select")
        					.append($("<option/>").attr("value","sum").html("SUM"))
        					.append($("<option/>").attr("value","count").html("COUNT"))
        					.append($("<option/>").attr("value","avg").html("AVG"))
        					.append($("<option/>").attr("value","min").html("MIN"))
        					.append($("<option/>").attr("value","max").html("MAX"))
        					.append($("<option/>").attr("value","median").html("MEDIAN"))
        					.append($("<option/>").attr("value","distinct").html("DISTINCT"))
        					.append($("<option/>").attr("value","Ratio").html("RATIO"))
        					.attr({name:"aggregate"}).addClass("input-xs").val($(obj).data("aggregate"))).append($("<i/>"))));
        		}
        		if ( chartType === "chart-bar" || chartType === "chart-grouped-bar"){
        			popoverContent.append($("<div/>").addClass("note").append($("<strong/>").html("Measure"))).append($("<label/>").addClass("input").append($("<select/>").addClass("aggregate-select").append(tempOption.html()).attr({type:"text",name:"group"}).addClass("input-xs").val($(obj).data("group"))));
        			var popoverMeasure= $("<div/>").addClass("div-4").appendTo(popoverContent);
        			popoverMeasure
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Order By"))).append($("<label/>").addClass("width-100per").append($("<select/>").addClass("aggregate-select")
        					.append($("<option/>").attr("value","0").html("None"))
        					.append($("<option/>").attr("value","-1").html("DESC"))
        					.append($("<option/>").attr("value","1").html("ASC"))
        					.attr({name:"orderby"}).addClass("input-xs").val($(obj).data("orderby"))).append($("<i/>"))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Format"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"numformat"}).addClass("input-xs").val($(obj).data("numformat")))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Aggregate"))).append($("<label/>").addClass("width-100per").append($("<select/>").addClass("aggregate-select")
        					.append($("<option/>").attr("value","sum").html("SUM"))
        					.append($("<option/>").attr("value","count").html("COUNT"))
        					.append($("<option/>").attr("value","avg").html("AVG"))
        					.append($("<option/>").attr("value","min").html("MIN"))
        					.append($("<option/>").attr("value","max").html("MAX"))
        					.append($("<option/>").attr("value","median").html("MEDIAN"))
        					.append($("<option/>").attr("value","distinct").html("DISTINCT"))
        					.append($("<option/>").attr("value","Ratio").html("RATIO"))
        					.attr({name:"aggregate"}).addClass("input-xs").val($(obj).data("aggregate"))).append($("<i/>"))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Show Total"))).append($("<label/>").addClass("width-100per").append($("<select/>").addClass("aggregate-select")
        					.append($("<option/>").attr("value","yes").html("Yes"))
        					.attr({name:"showtotal"}).addClass("input-xs").val($(obj).data("showtotal"))).append($("<i/>"))));
        		}
        		if(chartType !== 'chart-pie' && chartType !== 'chart-donut' && chartType.charttype !== "chart-geo" && chartType.charttype !==  "chart-bubble"){
        			var popoverOrientation= $("<div/>").addClass("div-4").appendTo(popoverContent);
        			popoverOrientation
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("X-Axis"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"xaxislabel"}).addClass("input-xs").val($(obj).data("xaxislabel")))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Y-Orient."))).append($("<label/>").addClass("width-100per").append($("<select/>").addClass("aggregate-select").append($("<option/>").attr("value","").html("0")).append($("<option/>").attr("value","y-axis-rotate-15-deg").html("15"))
        					.append($("<option/>").attr("value","y-axis-rotate-30-deg").html("30")).append($("<option/>").attr("value","y-axis-rotate-45-deg").html("45")).append($("<option/>").attr("value","y-axis-rotate-60-deg").html("60"))
        					.append($("<option/>").attr("value","y-axis-rotate-90-deg").html("90")).attr({name:"yaxisorientation"}).addClass("input-xs").val($(obj).data("yaxisorientation"))).append($("<i/>"))))			
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Y-Axis"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"yaxislabel"}).addClass("input-xs").val($(obj).data("yaxislabel")))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("x-Orient."))).append($("<label/>").addClass("width-100per").append($("<select/>").addClass("aggregate-select").append($("<option/>").attr("value","").html("0")).append($("<option/>").attr("value","x-axis-rotate-15-deg").html("15"))
        					.append($("<option/>").attr("value","x-axis-rotate-30-deg").html("30")).append($("<option/>").attr("value","x-axis-rotate-45-deg").html("45")).append($("<option/>").attr("value","x-axis-rotate-60-deg").html("60"))
        					.append($("<option/>").attr("value","x-axis-rotate-90-deg").html("90")).attr({name:"xaxisorientation"}).addClass("input-xs").val($(obj).data("xaxisorientation"))).append($("<i/>"))));
        		}
        		var popoverDimension = $("<div/>").addClass("div-2").appendTo(popoverContent);
        		popoverDimension
        		.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Width"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"chartwidth"}).addClass("input-xs").val($(obj).data("chartwidth")))))
        		.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Height"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"chartheight"}).addClass("input-xs").val($(obj).data("chartheight")))));
        		if (chartType === "chart-line")
        			popoverContent.append($("<div/>").addClass("note").append($("<strong/>").html("Date Format"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"dateformat"}).addClass("input-xs").val($(obj).data("dateformat"))));
        		else if (chartType === "chart-grouped-bar" || chartType === "chart-stacked")
        			popoverContent.append($("<div/>").addClass("note").append($("<strong/>").html("Groups"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"stack"}).addClass("input-xs").val($(obj).data("stack"))));
        		else if (chartType === "chart-geo"){
        			var popoverBubble = $("<div/>").addClass("div-3").appendTo(popoverContent);
        			popoverBubble
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Bubble"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"bubble"}).addClass("input-xs").val($(obj).data("bubble")))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Latitude"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"latitude"}).addClass("input-xs").val($(obj).data("latitude")))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Longitude"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"longitude"}).addClass("input-xs").val($(obj).data("longitude")))))
        			.append($("<div/>").addClass("div-2")
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Label"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"label"}).addClass("input-xs").val($(obj).data("label")))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Prefix"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"prefix"}).addClass("input-xs").val($(obj).data("prefix"))))));
        		}
        		else if (chartType === "chart-bubble"){
        			popoverContent.append($("<div/>").addClass("note").append($("<strong/>").html("X-Axis Measure"))).append($("<label/>").addClass("input").append($("<select/>").addClass("aggregate-select").append(tempOption.html()).attr({type:"text",name:"xaxis"}).addClass("input-xs").val($(obj).data("xaxis"))));
        			var popoverxAxis= $("<div/>").addClass("div-4").appendTo(popoverContent);
        			popoverxAxis
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("X-Suffix"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"xaxis-suffix"}).addClass("input-xs").val($(obj).data("xaxis-suffix")))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("X-Aggr."))).append($("<label/>").addClass("width-100per").append($("<select/>").addClass("aggregate-select").append($("<option/>").attr("value","sum").html("SUM")).append($("<option/>").attr("value","count").html("COUNT")).append($("<option/>").attr("value","avg").html("AVG")).attr({name:"xaxis-aggre"}).addClass("input-xs").val($(obj).data("aggregate"))).append($("<i/>"))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("X-Label"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"xaxis-label"}).addClass("input-xs").val($(obj).data("xaxis-label")))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("X-Prefix"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"xaxis-prefix"}).addClass("input-xs").val($(obj).data("xaxis-prefix")))));
        			
        			popoverContent.append($("<div/>").addClass("note").append($("<strong/>").html("Y-Axis Measure"))).append($("<label/>").addClass("input").append($("<select/>").addClass("aggregate-select").append(tempOption.html()).attr({type:"text",name:"yaxis"}).addClass("input-xs").val($(obj).data("yaxis"))));
        			var popoveryAxis= $("<div/>").addClass("div-4").appendTo(popoverContent);
        			popoveryAxis
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Y-Suffix"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"yaxis-suffix"}).addClass("input-xs").val($(obj).data("yaxis-suffix")))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Y-Aggr."))).append($("<label/>").addClass("width-100per").append($("<select/>").addClass("aggregate-select").append($("<option/>").attr("value","sum").html("SUM")).append($("<option/>").attr("value","count").html("COUNT")).append($("<option/>").attr("value","avg").html("AVG")).attr({name:"yaxis-aggre"}).addClass("input-xs").val($(obj).data("yaxis-aggre"))).append($("<i/>"))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Y-Label"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"yaxis-label"}).addClass("input-xs").val($(obj).data("yaxis-label")))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Y-Prefix"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"yaxis-prefix"}).addClass("input-xs").val($(obj).data("yaxis-prefix")))));
        			
        			popoverContent.append($("<div/>").addClass("note").append($("<strong/>").html("Radius Measure"))).append($("<label/>").addClass("input").append($("<select/>").addClass("aggregate-select").append(tempOption.html()).attr({type:"text",name:"radius"}).addClass("input-xs").val($(obj).data("radius"))));
        			var popoverrAxis= $("<div/>").addClass("div-4").appendTo(popoverContent);
        			popoverrAxis
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("R-Suffix"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"radius-suffix"}).addClass("input-xs").val($(obj).data("radius-suffix")))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("R-Aggr."))).append($("<label/>").addClass("width-100per").append($("<select/>").addClass("aggregate-select").append($("<option/>").attr("value","sum").html("SUM")).append($("<option/>").attr("value","count").html("COUNT")).append($("<option/>").attr("value","avg").html("AVG")).attr({name:"radius-aggre"}).addClass("input-xs").val($(obj).data("radius-aggre"))).append($("<i/>"))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("R-Label"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"radius-label"}).addClass("input-xs").val($(obj).data("radius-label")))))
        			.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("R-Prefix"))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"radius-prefix"}).addClass("input-xs").val($(obj).data("radius-prefix")))));
        		} else if (chartType === "chart-composite"){
        			for (var i=1; i< 6; i++){
        				var popoverxAxis= $("<div/>").addClass("div-4").appendTo(popoverContent);
        				popoverxAxis
        				.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Chart "+i))).append($("<label/>").addClass("width-100per").append($("<select/>").addClass("aggregate-select").append($("<option/>").attr("value","bar").html("Bar")).append($("<option/>").attr("value","line").html("Line")).append($("<option/>").attr("value","avg").html("AVG")).attr({name:"chart-type-"+i}).addClass("input-xs").val($(obj).data("chart-type-"+i))).append($("<i/>"))))
        				.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Color "+i))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"color",name:"color-"+i}).addClass("input-xs").val($(obj).data("color-"+i)))))
        				.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Meas. "+i))).append($("<label/>").addClass("input").append($("<select/>").addClass("aggregate-select").append(tempOption.html()).attr({type:"text",name:"measure-"+i}).addClass("input-xs").val($(obj).data("measure-"+i)))))
        				//.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Meas. "+i))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"measure-"+i}).addClass("input-xs").val($(obj).data("measure-"+i)))))
        				.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Aggr. "+i))).append($("<label/>").addClass("width-100per").append($("<select/>").addClass("aggregate-select").append($("<option/>").attr("value","sum").html("SUM")).append($("<option/>").attr("value","count").html("COUNT")).append($("<option/>").attr("value","avg").html("AVG")).attr({name:"aggre-"+i}).addClass("input-xs").val($(obj).data("aggre-"+i))).append($("<i/>"))));
        				
        				var popoverxAxis= $("<div/>").addClass("div-2").appendTo(popoverContent);
        				popoverxAxis
        				.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Display Name "+i))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"name-"+i}).addClass("input-xs").val($(obj).data("name-"+i)))))
        				.append($("<div/>").append($("<div/>").addClass("note").append($("<strong/>").html("Dash Style"+i))).append($("<label/>").addClass("input").append($("<input/>").attr({type:"text",name:"dash-"+i}).addClass("input-xs").val($(obj).data("dash-"+i)))));
        			}
        		}
        		$("input.input-xs",popover).each(function(i,j){
        			$("[name="+$(this).attr("name")+"]",popover).val($(obj).data($(this).attr("name")));
        		});
        		$("input.input-xs",popover).keyup(function(){
        				$(obj).data($(this).attr("name"),$(this).val());
        				if ($(this).attr("name") === "displayname")
        					$(obj).html($(this).val());
        		});
        		$("input[type=color]",popover).change(function(){
        				$(obj).data($(this).attr("name"),$(this).val());
        		});
        		$("select",popover).each(function(i,j){
        			$("[name="+$(this).attr("name")+"]",popover).val($(obj).data($(this).attr("name")));
        		});
        		$("select",popover).change(function() {
        			if ($( this ).attr("name")=="charttype"){
        				$(obj).data("charttype",$( this ).val());
        				$("[class^=chart-]",$(obj).parent()).hide();
        				$("."+$( this ).val(),$(obj).parent()).show();
        				if ($( this ).val() == "chart-geo"){
        					$(".chart-geo",$(obj).parent()).empty();
        					var map = new Datamap({
        				        scope: 'usa',
        				        element: $(".chart-geo",$(obj).parent())[0],
        				       	projection: 'mercator',width:250,fills: {
        				       	defaultFill: 'grey',height:250}});
        				}
        			} else{
        				$(obj).data($(this).attr("name"),$(this).val());
        			}
        		});
        	};*/
            $(self.o.editPlaceholder)
                .find('input:first')
                .keyup(function () {
                    $($(this)
                        .parents(self.o.widgets).first())
                        .children('header')
                        .children('h2')
                        .text($(this)
                            .val());
                });
            /* added to select width dynamically*/
            $(self.o.editPlaceholder)
            .find('.select-width')
            .change(function () {
                $($($(this)
                    .parents(self.o.widgets).first())
                    .parent('div')).removeClass()
                    .addClass($(this).find(":selected").val())
                    .addClass("chart-parent-div");
            });
            /* added to select no. of charts*/
            $(self.o.editPlaceholder)
            .find('.select-charts').unbind("change").bind("change"
            ,function () {
            	var selectedClass = $(this).find(":selected").val();
            	var chartCount = $(this).find(":selected").text();
            	var widgetBody = $($(this)
                        .parents(self.o.widgets).first())
                        .children('div')
                        .children('.widget-body');
            	var chartDiff = parseInt(chartCount);
            	if ($(widgetBody).find("div[class^=col-md]") && $(widgetBody).find("div[class^=col-md]").length > 0)
            		chartDiff = chartCount-$(widgetBody).find("div[class^=col-md]").length;
            	if (chartDiff > 0){
            		for (var i=0;i<chartDiff;i++){
            			$(widgetBody).append($("<div/>").addClass(selectedClass + " chart-div").append($("<span/>").addClass("dimension-placeholder")));
            			 $(".chart-div").droppable({
           			      activeClass: "ui-state-default",
           			      hoverClass: "ui-state-hover",
           			      accept: ":not(.btn-fact,.chart-parent-div,.dd-item.ui-sortable-handle)",
           			      drop: function( event, ui ) {
           			    	 $(this).find(".dimension-placeholder").remove();
           			    	  if ($(this).is(":empty")){
           			    			 if (!ui.draggable.hasClass('chart-div'))
           			    				 chartDrop(this,ui);
           			    			 else{
           			    				 $("[rel=popover]").popover('destroy');
           			    				 $(this).html(ui.draggable.html());
           			    				 ui.draggable.find(".axisHeader").removeData("bs.popover");
           			    				 $(this).find(".axisHeader").data(ui.draggable.find(".axisHeader").data());
           			    				 $("[rel=popover]").popover();
           			    				 $(".axisHeader",$(this)).bind("click",function(){dataAnalyser.axisLabelClick(this);});
           			    				 //$(".axisHeader",$(this)).bind("click",function(){dataAnalyser.axisLabelClick(this);});
           			    				 $(".chart-remove", $(this)).unbind("click").bind("click", function() {dataAnalyser.removeChart(this);});
           			    				 ui.draggable.empty().append($("<span/>").addClass("dimension-placeholder"));
           			    			 }
           			    	  } else{
           			    		  $("[rel=popover]").popover('destroy');
           			    		  var tempHTML =$(this).html();
           			    		  $(this).find(".axisHeader").removeData("bs.popover");
           			    		  var tempData =$(this).find(".axisHeader").data();
           			    		  $(this).empty();
           			    		  $(this).html(ui.draggable.html());
           			    		  ui.draggable.find(".axisHeader").removeData("bs.popover");
           			    		  $(this).find(".axisHeader").data(ui.draggable.find(".axisHeader").data());
           			    		  ui.draggable.empty().html(tempHTML);
           			    		  ui.draggable.find(".axisHeader").data(tempData);
           			    		  $("[rel=popover]").popover();
           			    		  $(".axisHeader",$(this)).bind("click",function(){dataAnalyser.axisLabelClick(this);});
           			    		  $(".axisHeader",ui.draggable).bind("click",function(){dataAnalyser.axisLabelClick(this);});
           			    		  $(".chart-remove", $(this)).unbind("click").bind("click", function() {dataAnalyser.removeChart(this);});
           			    		$(".chart-remove", ui.draggable).unbind("click").bind("click", function() {dataAnalyser.removeChart(this);});
           			    	  }
           			      }
           			    });
          			  $( ".chart-div" ).draggable({
          			      appendTo: "body",
          			      helper: "clone"
          			    });
            		};
            	} else if (chartDiff<0){
            		if ($(widgetBody).find("[class^=dimension-placeholder]") && $(widgetBody).find("[class^=dimension-placeholder]").length > 0){
            			if (Math.abs(chartDiff) <= $(widgetBody).find("[class^=dimension-placeholder]").length){
            				for(var j=Math.abs(chartDiff)-1; j>=0;j--){
            					$($(widgetBody).find("[class^=dimension-placeholder]")[j]).parent().remove();
                    		}
            			}
            		};
            	}
            	$(widgetBody).find("div[class^=col-md]").each(function(){
        			$(this).removeClass();
        			$(this).addClass(selectedClass + " chart-div");
        		});
            });
            
            var chartDrop = function(obj,ui){
      		  var THIS = $(obj);
      		  var options = {
      				  "name":ui.draggable.text(),
      				  "piestyle": "width:"+$(THIS).height()*80/100+"px;height:"+$(THIS).height()*80/100+"px"
      		  };
      		  var callBackPie = function(response){
      			  $(THIS).html(response);
      			$("[rel=popover]").popover();
      			$(".axisHeader").bind("click",function(){dataAnalyser.axisLabelClick(this);});
      		  };
      		  templates.getPie(options,callBackPie);
            };
            /**
             * Set a custom style.
             **/
            self.widget.on(clickEvent, '[data-widget-setstyle]', function (e) {

                var val = $(this)
                    .data('widget-setstyle');
                var styles = '';

                /**
                 * Get all other styles, in order to remove it.
                 **/
                $($(this)
                    .parents(self.o.editPlaceholder).first())
                    .find('[data-widget-setstyle]')
                    .each(function () {
                        styles += $(this)
                            .data('widget-setstyle') + ' ';
                    });

                /**
                 * Set the new style.
                 **/
               $( $(this)
                    .parents(self.o.widgets).first())
                    .attr('data-widget-attstyle', '' + val + '')
                    .removeClassPrefix('jarviswidget-color-')
                    .addClass(val);

                /**
                 * Run function for the indicator image.
                 **/
                self._runLoaderWidget($(this));

                /**
                 * Lets save the setings.
                 **/
                self._saveSettingsWidget();

                e.preventDefault();
            });

            //*****************************************************************//
            /////////////////////////// CUSTOM ACTION ///////////////////////////
            //*****************************************************************//

            /**
             * Allow users to show/hide a edit box.
             **/
            self.widget.on(clickEvent, '.jarviswidget-custom-btn', function (e) {

                var w = $(this)
                    .parents(self.o.widgets).first();

                /**
                 * Run function for the indicator image.
                 **/
                self._runLoaderWidget($(this));

                /**
                 * Start and end custom action.
                 **/
                if ($(this)
                    .children('.' + self.customClass[0])
                    .length) {
                    $(this)
                        .children()
                        .removeClass(self.customClass[0])
                        .addClass(self.customClass[1]);

                    /**
                     * Run the callback function.
                     **/
                    if (typeof self.o.customStart == 'function') {
                        self.o.customStart.call(this, w);
                    }
                } else {
                    $(this)
                        .children()
                        .removeClass(self.customClass[1])
                        .addClass(self.customClass[0]);

                    /**
                     * Run the callback function.
                     **/
                    if (typeof self.o.customEnd == 'function') {
                        self.o.customEnd.call(this, w);
                    }
                }

                /**
                 * Lets save the setings.
                 **/
                self._saveSettingsWidget();

                e.preventDefault();
            });

            //*****************************************************************//
            /////////////////////////// DELETE WIDGETS //////////////////////////
            //*****************************************************************//

            /**
             * Allow users to delete the widgets.
             **/
            self.widget.on(clickEvent, '.jarviswidget-delete-btn', function (e) {

                var tWidget = $(this)
                    .parents(self.o.widgets).first();
                var removeId = tWidget.attr('id');
                var widTitle = tWidget.children('header')
                    .children('h2')
                    .text();

                /**
                 * Delete the widgets with a confirm popup.
                 **/
                $.SmartMessageBox({
                    title: "<i class='fa fa-times' style='color:#ed1c24'></i> " + self.o.labelDelete +
                        ' "' + widTitle + '"',
                    content: "Warning: This action cannot be undone",
                    buttons: '[No][Yes]'
                }, function (ButtonPressed) {
                    //console.log(ButtonPressed);
                    if (ButtonPressed == "Yes") {
                        /**
                         * Run function for the indicator image.
                         **/
                        self._runLoaderWidget($(this));

                        /**
                         * Delete the right widget.
                         **/
                        $('#' + removeId)
                            .fadeOut(self.o.deleteSpeed, function () {
                            	   $(this).parent()
                                   .remove();
                                $(this)
                                    .remove();
                             

                                /**
                                 * Run the callback function.
                                 **/
                                if (typeof self.o.onDelete == 'function') {
                                    self.o.onDelete.call(this, tWidget);
                                }
                            });
                    }

                });

                e.preventDefault();
            });

            //******************************************************************//
            /////////////////////////// REFRESH BUTTON ///////////////////////////
            //******************************************************************//

            /**
             * Refresh ajax upon clicking refresh link.
             **/
            self.widget.on(clickEvent, '.jarviswidget-refresh-btn', function (e) {

                /**
                 * Variables.
                 **/
                var rItem = $(this)
                    .parents(self.o.widgets).first(),
                    pathToFile = rItem.data('widget-load'),
                    ajaxLoader = rItem.children(),
                    btn = $(this);

                /**
                 * Run the ajax function.
                 **/
                btn.button('loading');
                ajaxLoader.addClass("widget-body-ajax-loading");
                setTimeout(function () {
                    btn.button('reset');
                    ajaxLoader.removeClass("widget-body-ajax-loading");
                    self._loadAjaxFile(rItem, pathToFile, ajaxLoader);

                }, 1000)

                e.preventDefault();
            });
        },

        /**
         * Destroy.
         *
         * @param:
         **/
        destroy: function () {
            var self = this;
            self.widget.off('click', self._clickEvents());
            self.obj.removeData(pluginName);
        }
    };

    $.fn[pluginName] = function (option) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data(pluginName);
            var options = typeof option == 'object' && option;
            if (!data) {
                $this.data(pluginName, (data = new Plugin(this, options)))
            }
            if (typeof option == 'string') {
                data[option]();
            }
        });
    };

    /**
     * Default settings(dont change).
     * You can globally override these options
     * by using $.fn.pluginName.key = 'value';
     **/

    $.fn[pluginName].defaults = {
        grid: 'section',
        widgets: '.jarviswidget',
        localStorage: true,
        deleteSettingsKey: '',
        settingsKeyLabel: 'Reset settings?',
        deletePositionKey: '',
        positionKeyLabel: 'Reset position?',
        sortable: true,
        buttonsHidden: false,
        toggleButton: true,
        toggleClass: 'fa fa-minus | fa fa-plus',
        toggleSpeed: 200,
        onToggle: function () {},
        deleteButton: true,
        deleteClass: 'trashcan-10',
        deleteSpeed: 200,
        onDelete: function () {},
        editButton: true,
        editPlaceholder: '.jarviswidget-editbox',
        editClass: 'pencil-10 | delete-10',
        editSpeed: 200,
        onEdit: function () {},
        colorButton: true,
        fullscreenButton: true,
        fullscreenClass: 'fullscreen-10 | normalscreen-10',
        fullscreenDiff: 3,
        onFullscreen: function () {},
        customButton: true,
        customClass: '',
        customStart: function () {},
        customEnd: function () {},
        buttonOrder: '%refresh% %delete% %custom% %edit% %fullscreen% %toggle%',
        opacity: 1.0,
        dragHandle: '> header',
        placeholderClass: 'jarviswidget-placeholder',
        indicator: true,
        indicatorTime: 600,
        ajax: true,
        loadingLabel: 'loading...',
        timestampPlaceholder: '.jarviswidget-timestamp',
        timestampFormat: 'Last update: %m%/%d%/%y% %h%:%i%:%s%',
        refreshButton: true,
        refreshButtonClass: 'refresh-10',
        labelError: 'Sorry but there was a error:',
        labelUpdated: 'Last Update:',
        labelRefresh: 'Refresh',
        labelDelete: 'Delete widget:',
        afterLoad: function () {},
        rtl: false,
        onChange: function () {},
        onSave: function () {},
        ajaxnav: true
    };

    /*
     * REMOVE CSS CLASS WITH PREFIX
     * Description: Remove classes that have given prefix. You have an element with classes
     * 				"widget widget-color-red"
     * Usage: $elem.removeClassPrefix('widget-color-');
     */

    $.fn.removeClassPrefix = function (prefix) {

        this.each(function (i, it) {
            var classes = it.className.split(" ")
                .map(function (item) {
                    return item.indexOf(prefix) === 0 ? "" : item;
                });
            //it.className = classes.join(" ");
            it.className = $.trim(classes.join(" "));

        });

        return this;
    }
})(jQuery, window, document);