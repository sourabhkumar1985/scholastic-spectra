(function(){
	var THIS = {};
	THIS.queryEditor ={};
	$(".col-sm-3", "#selectWidget").unbind("click").bind("click", function() {
		widgetonClick(this);
	});
	$("#saveProfile").unbind("click").bind("click", function() {
		saveProfile();
	});
	$("#resetProfile").unbind("click").bind("click", function() {
		getProfileTemplate();
	});
	$("#tableBack").unbind("click").bind("click", function() {
		$("#selectWidget").show(300);
		$("#selectTemplate").hide(300);
	});
	$("#profileBack").unbind("click").bind("click", function() {
		$("#profileTemplateTable").hide();
		$("#selectWidget").show();
	});
	$("#proceed").unbind("click").bind("click", function() {
		proceed(this);
	});
	$("#excuteQuery").unbind("click").bind("click", function() {
		excuteQuery();
	});
	$("#btnSelectTable").unbind("click").bind("click", function() {
		onClickGoOfSelectTable(this);
	});
	$("#frmSaveProfile").submit(function(e) {
		e.preventDefault();
	});
	$("#frmSaveProfile").validate({
		ignore: [],
		rules : {
			profilename : {
				required : true
			},
			description : {
				required : true
			},
			appname:{
				required : true
			}
		},
		messages : {
			profilename : {
				required : 'Please enter profile name'
			},
			description : {
				required : 'Please describe profile'
			},
			appname : {
				required : 'Please select app name'
			}
		},
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	var widgetonClick = function(obj) {
		var widget = $(obj).data("source");
		if (widget === "myProfiles") {
			request = {
					"type" : "templateview"
				};
				enableLoading();
				callAjaxService("getMyProfiles", callBackGetMyProfiles,
						callBackFailure, request, "POST");
		} else {
			$("#selectTemplate").show(300);
			$("#selectWidget").hide();
			constructSelect2("#profileTemplate","select id,displayname from profile_analyser where type in ('template') and lower(displayname) like '%<Q>%'",false,null,"name");
			THIS.sourceType = widget;
			THIS.profileId = 0;
			selectSource();
		}
	};
	var callBackGetMyProfiles = function(response) {
		disableLoading();
		$("#myprofiles").show(300);
		$("#selectWidget").hide(300);
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			// response = JSON.parse(response);
			var data = response;
			// data = JSON.parse(data);
			$("#myProfileList").empty();
			for ( var i = 0; i < data.length; i++) {
				$("#myProfileList").append(
						$("<li/>").addClass("list-group-item").data({
							"refid" : data[i].id
						}).append(
								$("<a/>").append($("<span/>").addClass("text-left col-md-3").html(data[i].displayname))
										 .append($("<span/>").addClass("text-left col-md-3").html(data[i].source))
										 .append($("<span/>").addClass("text-left col-md-3").html(data[i].lastmodifiedby))
										 .append($("<span/>").addClass("badge text-center col-md-3").html(getManipulatedDate("day",0,data[i].lastmodifiedon,"%d-%m-%Y %H:%M:%S")))
										 ));
			}
			$("#myProfileList li").unbind("click").bind("click", function() {
				myProfileTemplateSelect(this);
			});
		}
		$("[data-target^=#profilePublishModal]").unbind("click").bind("click",
				function(e) {
					e.stopPropagation();
					previewPublishProfile(this);
				});
	};
	var myProfileTemplateSelect = function(obj) {
		var id = $(obj).data("refid");
		var request = {
			"id" : id
		};
		enableLoading();
		callAjaxService("getProfileConfig", callBackGetProfileConfig,
				callBackFailure, request, "POST");
	};
	var callBackGetProfileConfig = function(response) {
		$("#profileTemplateTable").show(300);
		$("#selectWidget").hide(300);
		$("#myprofiles").hide(300);
		$("#btnSelectTable").hide();
		$("#configQuery").parent().parent().show();
		THIS.queryEditor = {};
		$("#divScriptEditor").find("table").remove();
		var data = JSON.parse(response["config"])[0];
		if (data) {
			THIS.profileId = data.id;
			THIS.app = data.app;
			THIS.displayname = data.displayname;
			$("#frmTemplateConf").find("[name='profilename']").val(data.displayname);
			if(data.source){
				THIS.sourceType = data.source;
			}
			THIS.database = data.database;
			getTableByDataBase(data.database,true);
			$("#frmTemplateConf").find("[name='table']").val(data.configtable);
			if (data.config) {
				THIS.config = JSON.parse(data.config.replace(/@/g, "'"));
				constructLevelQuery();
			}
			if(data.additional_config){
				$("#configTable").find("tbody").html("");
				THIS.additionalConfig = JSON.parse(data.additional_config);
			}
			$("#sourceTableDesc").html(THIS.displayname +":"+THIS.database);
		}
		
	};
	var selectSource = function(){
		request = {
				"source" : THIS.sourceType
			};
		enableLoading();
		callAjaxService("getDatabaseList", callBackGetDatabaseList,callBackFailure, request, "POST");
	};
	var callBackGetDatabaseList = function(response){
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			var databaseList = [];
			for ( var i = 0; i < response.length; i++) {
				databaseList.push({
					"id" : response[i],
					"text" : response[i]
				});
			}
			$("#database").select2({
				placeholder : "Choose Database",
				data : databaseList
			}).on("change", function(e) {
				getTableByDataBase(e.val);
			});
		}
	};
	var getTableByDataBase = function(database,isEdit) {
		THIS.database = database;
		var request = {
			"source" : THIS.sourceType,
			"database" : database
		};
		enableLoading();
		callAjaxService("getTableList", function(response){callBackGetTableList(response,isEdit)}, callBackFailure,
				request, "POST");
	};
	var callBackGetTableList = function(response,isEdit) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			THIS.tableList = [];
			for ( var i = 0; i < response.length; i++) {
				THIS.tableList.push({
					"id" : response[i],
					"text" : response[i]
				});
			}
			if(isEdit){
				constructTable();
			}
		}
	};
	var getConfig =function(){
		var config ={};
		if(THIS.queryEditor && Object.keys(THIS.queryEditor).length>0){
			for(key in THIS.queryEditor){
				config[key] = THIS.queryEditor[key].getValue();
			}
		}
		return config;
	};
	var getTable =function(){
		var tables = [];
		$("#configTable").find("[name='mappingtable']").each(function(){
			tables.push({
				key:$(this).attr("key"),
				value:$(this).attr("keyname"),
				table:$(this).select2("data")
			});
		});
		return tables;
	};
	var saveProfile = function(isClone) {
		var saveDataProfile = function() {
			var config = getConfig();
			var tables = getTable();
			var callBackSuccessHandler = callBackSaveProfile;
			var displayName;
			var remarks;
			var appName;
			if(!isClone){
				var profileID = THIS.profileId||0;
				if((profileID ===0)){ // Update values if saving on first time
					var app = $("#appname").select2("data");
					THIS.displayname = $("#myModalthredlevel #profilename").val();
					$("#saveProfile").data("app",app.name);
					appName = app.id;
					displayName = $("#myModalthredlevel #profilename").val();
					remarks = $("#myModalthredlevel #description").val();
				} else {
					displayName = THIS.displayname;
					appName = THIS.app;
					remarks = THIS.remarks;
				}
				$("#wid-id-profiling > header > h2").html(displayName);
				callBackSuccessHandler = callBackSaveProfile;
			} else {
				callBackSuccessHandler = function() {
					disableLoading();
					$('#myModalthredlevel').modal('hide');
					showNotification("success",
							"Data Profile cloned successfully..");
				};
				remarks = $("#myModalthredlevel #description").val();
				displayName = $("#myModalthredlevel #profilename").val();
				appName = $("#appname").select2("data").name;
			}
			var additionalconfig = {};
			if(tables && tables.length>0){
				additionalconfig.tables = tables;
			}
			//additionalconfig.templateName = THIS.profiletemplate;
			additionalconfig.templateId = THIS.additionalConfig.templateId;
			additionalconfig = JSON.stringify(additionalconfig);
			var request = {
				"source" : THIS.sourceType,
				"database" : THIS.database,
				/*"table" : THIS.table,
				"fact" : JSON.stringify(fact),
				"customfacts" : JSON.stringify(THIS.customFacts),*/
				"config" : JSON.stringify(config),
				/*"filters" : JSON.stringify(filters),
				"summary" : JSON.stringify(summary),*/
				"remarks" : remarks,
				"displayname" : displayName,
				"additionalconfig" : additionalconfig,
				"type" : "templateview",
				"app":appName,
				"id" : isClone === true ? 0 : THIS.profileId
			};
			if ($(".notification-holder a") && $(".notification-holder a") !== null &&$(".notification-holder a").length > 0){
				request.notificationFilter = $(".notification-holder a").text();
			}
			enableLoading();
			callAjaxService("saveProfile", callBackSuccessHandler,
					callBackFailure, request, "POST");
		};
		var profileID = THIS.profileId ||0;
		if (isClone || profileID ===0) {
			$("#myModalthredlevel #description").val("");
			$("#myModalthredlevel #profilename").val("");
			constructSelect2("#appname","select name,displayname from roles where lower(displayname) like '%<Q>%'",false,null,"name");
			$("#myModalthredlevel").modal('show');
			$("#myModalthredlevel #btnCreate").unbind("click").bind("click",
					function() {
						if (!$("#frmSaveProfile").valid())
							return;
						saveDataProfile();
					});
		} else {
			saveDataProfile();
		}
	};
	var callBackSaveProfile = function(response) {
		if ((response && response.isException)|| (response.message && response.message ==="failure")) {
			showNotification("error", response.customMessage||response.message);
			return;
		}
		if (response !== undefined && response !== null) {
			disableLoading();
			$('#myModalthredlevel').modal('hide');
			showNotification("success", "Profile saved successfully.");
			$("#saveProfile").data("id", response);
			$("#cloneProfile").show();
		}
	};
	var excuteQuery = function(){
		var request = {
			"source":THIS.sourceType,
			"database":THIS.database,
			"id" : THIS.profileId
		};
		enableLoading();
		callAjaxService("excutequery", callBackGetExcuteQuery,callBackFailure, request, "POST");
	};
	var callBackGetExcuteQuery = function(response){
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if(response && response.length>0){
			for(var i=0;i<response.length;i++){
				if(response[i].indexOf("Excuted Succesfully")>-1){
					showNotification("success", response[i]);
				}else{
					showNotification("error", response[i]);
				}
			}
		}
		
	};
	var proceed = function(){
		THIS.database = $("#database").select2("val");
		$("#btnSelectTable").show();
		THIS.profiletemplate = $("#profileTemplate").select2("val");
		var request = {
				"id" : $("#profileTemplate").select2("val")
			};
		enableLoading();
		callAjaxService("getProfileConfig", callBackGetTemplateProfile,callBackFailure, request, "POST");
	};
	var callBackGetTemplateProfile = function(response){
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			$("#profileTemplateTable").show();
			$("#selectTemplate").hide();
			var data = JSON.parse(response["config"].replace(/@/g, "'"));
			if(data && data.length>0){
				THIS.config = JSON.parse(data[0]["config"].replace(/@/g, "'"));
				THIS.additionalConfig = JSON.parse(data[0]["additional_config"].replace(/@/g, "'"));
				constructTable();
				constructLevelQuery();
			}
			$("#sourceTableDesc").html("New Profile ," +THIS.database+",Template :"+$("#profileTemplate").select2("data").name);
		}
	};
	var constructTable = function(tables){
		var configTables = tables || THIS.additionalConfig.tables;
		if(configTables && configTables.length>0){
			$("#configTable").empty();
			for(var i=0;i<configTables.length;i++){
				var tableCombo = $("<input/>").addClass("width-100per").attr({"name":"mappingtable","key":configTables[i].key,"keyname":configTables[i].value});
				var tableData = $("<div/>").addClass("col col-4").append( $("<div/>").addClass("note padding-2")
							.append($("<strong/>").html(configTables[i].value)))
							.append($("<label/>").addClass("input").append(tableCombo));
				$("#configTable").append(tableData);
				$(tableCombo).select2({
					placeholder : "Select table",
					data : THIS.tableList
				});
				if(configTables[i].table && Object.keys(configTables[i].table).length>0){
					$(tableCombo).select2("data",configTables[i].table);
				}
			}
		}
	};
	var getProfileTemplate = function(){
		// to get template query
		var callBackGetTemplate = function(response){
			disableLoading();
			if (response && response.isException) {
				showNotification("error", response.customMessage);
				return;
			}
			if (response !== undefined && response !== null) {
				var data = JSON.parse(response["config"])[0];
				var config = JSON.parse(data.config.replace(/@/g, "'"));
				var additionalConfig = JSON.parse(data.additional_config.replace(/@/g, "'"));
				if(config && Object.keys(config).length>0){
					constructLevelQuery(config);
				}
				if(additionalConfig && additionalConfig.tables && additionalConfig.tables.length>0){
					constructTable(additionalConfig.tables);
				}
				$("#configTable").find("[name='mappingtable']").select2("val","");
				$("#btnSelectTable").show();
				$("#sourceTableDesc").html(THIS.displayname +"," +THIS.database+",Template :"+data.displayname);
			}
		};
		if(THIS.additionalConfig && THIS.additionalConfig.templateId){
			var request = {
				"id" : THIS.additionalConfig.templateId
			};
			enableLoading();
			callAjaxService("getProfileConfig", callBackGetTemplate,callBackFailure, request, "POST");
		}
	};
	
	var onClickGoOfSelectTable = function(){
		//$("#tableSection").hide();
		var tables = getTable();
		var selectedTable = tables.filter(function(obj){return (obj.table && obj.table.id?obj.table:'')});
		if(selectedTable.length !== tables.length){
			showNotification("notify","Please Select all the tables");
			return;
		}
		$("#configQuery").parent().parent().show();
		$("#btnSelectTable").hide();
		for (var key in THIS.queryEditor) {
			var query = THIS.queryEditor[key].getValue();
			if(tables.length>0){
				for(var i=0;i<tables.length;i++){
					if(query.indexOf(tables[i].key)>-1){
						query = query.replaceAll(tables[i].key,tables[i].table.id);
					}
				}
			}
			THIS.queryEditor[key].setValue(query);
		}
	};
	var constructLevelQuery = function(config){
		var configQuery = config || THIS.config;
		if(configQuery && Object.keys(configQuery).length>0){
			THIS.queryEditor = {};
			$("#configQuery").find("ul").html("");
			$("#configQuery").find("[name='querytabs']").html("");
			var i =0;
			for (var key in configQuery) {
				$("#configQuery").find("ul").append($("<li/>").addClass(i===0?"active":'').css({"width":(100/Object.keys(configQuery).length)+"%"})
						.append($("<a/>").unbind('click').bind('click',function(){
							var key = $(this).attr("href").replace("#","");
							if(THIS.queryEditor && THIS.queryEditor[key]){
								setTimeout(function() {
									THIS.queryEditor[key].refresh();
								}, 100);
							}
						}).attr({"data-toggle":"tab","href":"#"+key}).append($("<span/>").addClass("step").html(key))
								.append($("<span/>").addClass("title").html())));
				var textArea = $("<textarea/>").attr({"name":key});
				$("#configQuery").find("[name='querytabs']").append($("<div/>").attr({"id":key}).addClass(i===0?"active tab-pane margin-top-20 well":"tab-pane margin-top-20 well")
						.append($("<table>").addClass("table table-bordered margin-bottom-10 padding-bottom-10").append($("<tr/>").append($("<td/>").append(textArea)))));
				createQueryEditor(textArea[0],configQuery[key]);
				i++;
			}
		}
	};
	function createQueryEditor(ele,data){
		var queryEditor = CodeMirror.fromTextArea(ele, {
			lineNumbers : true,
			mode : "text/x-sql",
			autofocus : true,
			matchBrackets : true,
			styleActiveLine : true,
			showCursorWhenSelecting : true
		});
		if(data){
			queryEditor.setValue(data.replace(/@/g, "'"));
		}
		setTimeout(function() {
			queryEditor.refresh();
		}, 100);
		THIS.queryEditor[$(ele).attr("name")] = queryEditor;
	};
})();