'use strict';
pageSetUp();
var LookupReport;
var LookupReportInstance = function() {
	LookupReport = new LookupReportObject();
	LookupReport.Load();
};
var LookupReportObject = function() {
	var THIS = this;
	$("#btnPrintL3").click(function(e) {
		printElement('#poDocView');
	});
	$("#btnSaveL3").click(function(e) {
		savePDF('#poDocView');
	});
	$("#btnCreate").unbind('click').bind('click', function() {
		LookupReport.Create();
	});
	var $registerForm = $("#frmSankeyFilter").validate({

		// Rules for form validation
		rules : {
			key : {
				required : true
			},
			displayname : {
				required: true
			}
		},

		// Messages for form validation
		messages : {
			login : {
				key : 'Please enter key'
			},
			displayname : {
				required : 'Please enter Display Name'
			}
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	$("#frmSankeyFilter").submit(function(e) {
	    e.preventDefault();
	});
	this.Load = function(source) {
		enableLoading();
		source = source || "JDE";
		// var request = "viewType=userTypes&source="+source
		var request = {
			"source" : source
		};
		// console.log(request);
		callAjaxService("LookupReportPost", LookupReportPostSuccessCallback,
				callBackFailure, request, "GET");
	};

	var LookupReportPostSuccessCallback = function(data) {
		disableLoading();
		if (data && data.isException) {
			showNotification("error", data.customMessage);
			return;
		}
		if (data !== undefined && data !== null) {

			// data = data['userTypeList'].splice(0,100);
			// console.log(data);
			var deleteDiv = $("#deleteDiv").html();
			var dataTable = $(".dc-data-table")
					.DataTable(
							{
								"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
								"t"+
								"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
								"processing" : true,
								"lengthChange" : true,
								"sort" : true,
								"info" : true,
								"jQueryUI" : false,
								"scrollX" : true,
								"data" : data,
								"tableTools" : {
									"aButtons" : [ {
										"sExtends" : "collection",
										"sButtonText" : 'Export <span class="caret" />',
										"aButtons" : [ "csv", "pdf" ]
									} ],
									"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
								},
								"destroy" : true,
								"columns" : [ {

									"data" : "key",
									"defaultContent" : ""
								},{
									"data" : "displayName",
									"defaultContent" : ""
								},{
									"data" : "key",
									"defaultContent" :"",
									"render" : function(data, type,
											full) {
										return deleteDiv.replace("$","'"+data+"'").replace("#","LookupReport");
									}
								} ],
								"rowCallback" : function(nRow, aData,
										iDisplayIndex, iDisplayIndexFull) {
									$(nRow)
									.attr(
											{
												'onClick' : "javascript:LookupReport.Edit( '"+ aData.key+ "','"+aData.displayName+"')",
												'data-target' : '#myModalthredlevel',
												'data-toggle' : 'modal'
											});
								}
							/*
							 * "rowCallback" : function(nRow, aData,
							 * iDisplayIndex, iDisplayIndexFull) };
							 */
							});
		}
	};
	this.CreateView = function(){
		$("#smart-form-register input").val("");
		$("#btnCreate").html("").append($("<i/>").addClass("fa fa-plus")).append(" Create").unbind('click').bind('click',function(){LookupReport.Create();});
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		$("[name=key]").removeAttr("readonly");
	};
	this.Create = function() {
		if (!$("#frmSankeyFilter").valid())
			return;
		var request = {
			"key" : $("[name=key]").val(),
			"displayname" : $("[name=displayname]").val()
		// $("[name=enabled]").val(),
		};
		callAjaxService("CreateLookup", callbackSucessCreateLookUp, callBackFailure,
				request, "POST");
	};
	var callbackSucessCreateLookUp = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.Load();
			$('#myModalthredlevel').modal('hide');
			showNotification("success", "Lookup value Created Sucessfully..");
		}
	};
	
	THIS.Edit = function(key,displayName){
		$("#smart-form-register input").val("");
		$("#btnCreate").html("").append($("<i/>").addClass("fa fa-pencil")).append(" Update").unbind('click').bind('click',function(){LookupReport.Update();});
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		
		
		$("[name=key]").val(key).attr("readonly","true");
		$("[name=displayname]").val(displayName);
		
	};
	this.Update = function(){
		if (!$("#frmSankeyFilter").valid())
			return;
		
		var request = {
				"key":$("[name=key]").val(),
				"displayname": $("[name=displayname]").val()
		};
		callAjaxService("UpdateLookUp",callbackSucessUpdateUser,callBackFailure,request,"POST");
	};
	var callbackSucessUpdateUser = function(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		} else {
			$('#myModalthredlevel').modal('hide');
			$("#smart-form-register input").val("");
			$("#type").select2("val",null);
			THIS.Load();
			showNotification("success","User updated Sucessfully..");
		};
	};
	this.Delete = function(key){
		$.confirm({
		    text: "Are you sure you want to delete that LookUp Value?",
		    confirm: function() {
		    	var request = {
						"key":key
				};
				callAjaxService("DeleteLookup",callbackDeleteCreateLookUp, callBackFailure,
						request, "POST");
		    },
		    cancel: function() {
		        // nothing to do
		    }
		});
	};
	var callbackDeleteCreateLookUp = function(response){
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.Load();
			showNotification("success", "Lookup value Deleted Sucessfully..");
		}
	};
};
LookupReportInstance();