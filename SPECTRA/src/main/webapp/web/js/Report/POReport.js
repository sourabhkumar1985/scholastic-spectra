'use strict';
pageSetUp();
var POReportInstance = function() {
	POReportObject = new POReportObject();
	POReportObject.Load();
};
var POReportObject = function(){

	$("#btnPrintL3").click(function(e){
    	printElement('#poDocView');
    });
    $("#btnSaveL3").click(function(e){
        savePDF('#poDocView');
    });

	this.Load = function(source) {
		enableLoading();
		source = source || "JDE";
		//var request = "viewType=poTypes&source="+source
		var request = {
			"source" : source,
			"viewType" : "poTypes"
		};
		console.log(request);
		callAjaxService("POReportPost", POReportPostSuccessCallback, callBackFailure,
				request, "POST");
	};

	var POReportPostSuccessCallback = function(data){
		disableLoading();
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		if(data !== undefined 
			&& data !== null 
			&& data['poTypeList'] !==undefined 
			&& data['poTypeList'] !== null){

			data = data['poTypeList'].splice(0,100);
			console.log(data);

			var dataTable = $(".dc-data-table")
					.DataTable(
							{
								"sDom" : "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
								"bProcessing" : true,
								"bLengthChange" : true,
								"bSort" : true,
								"bInfo" : true,
								"bJQueryUI" : false,
								"scrollX" : true,
								"aaData" : data,
								"oTableTools" : {
									"aButtons" : [ {
										"sExtends" : "collection",
										"sButtonText" : 'Export <span class="caret" />',
										"aButtons" : [ "csv", "pdf" ]
									} ],
									"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
								},
								"bDestroy" : true,
								"processing" : true,
								"aoColumns" : [{										

											"mData" : "purchaseOrderType",
											"sDefaultContent" : ""
										},{

											"mData" : "purchaseOrderTypeDesc",
											"sDefaultContent" : ""
										},{

											"mData" : "relatedPurchaseOrderType",
											"sDefaultContent" : ""
										},{

											"mData" : "relatedPurchaseOrderTypeDesc",
											"sDefaultContent" : ""
										},{
											
											"mData" : "count",
											"sDefaultContent" : ""
										}],
										
								/*"fnRowCallback" : function(nRow, aData,
										iDisplayIndex, iDisplayIndexFull)
									};
								*/	
							});





		}
	}

}
POReportInstance();