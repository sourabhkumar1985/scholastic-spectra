'use strict';
pageSetUp();
var FAQReport;
var FCKeditor;
var FAQReportInstance = function() {
	FAQReport = new FAQReportObject();
	FAQReport.Load();
};
var FAQReportObject = function(){
	var THIS = this;
	$("#btnPrintL3").click(function(e){
		printElement('#poDocView');
	});
	$("#btnSaveL3").click(function(e){
		savePDF('#poDocView');
	});

	$('#myModalthredlevel').on('shown', function () {
		$('[name="id"]').focus();
	});
	$("#backToDesign").unbind('click').bind('click',function(){$("#faqGridDiv,#faqEditDiv").toggleClass('hide');});
	/* $("[name^=password]").show();*/
	$("#btnCreateFAQ,#btnCancel").unbind("click").bind("click", function(){FAQReport.CreateView();});
	setTimeout(function(){ 
		CKEDITOR.replace( 'question', { 
			extraPlugins: 'base64image',
			removeButtons: 'Image',
			// Upload images to a CKFinder connector (note that the response type is set to JSON).
			uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
			startupFocus : true,
			enterMode : CKEDITOR.ENTER_BR,
	        shiftEnterMode: CKEDITOR.ENTER_P
			} );
		CKEDITOR.replace( 'answer', { extraPlugins: 'base64image',
			removeButtons: 'Image',
			uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
			enterMode : CKEDITOR.ENTER_BR,
	        shiftEnterMode: CKEDITOR.ENTER_P
			} ); 
	}, 1000);

	$("#frmSankeyFilter").submit(function(e) {
		e.preventDefault();
	});

	var $registerForm = $("#frmSankeyFilter").validate({

		// Rules for form validation
		rules : {
			question : {
				required : true
			},
			answer : {
				required : true
			},

			type : {
				required: true
			}
		},

		// Messages for form validation
		messages : {
			question : {
				required : 'Please enter question'
			},
			answer : {
				required : 'Please enter answer'
			},

			type : {
				required : 'Please select your Role'
			}
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});


	this.Load = function(source) {
		enableLoading();
		source = source || "JDE";
		//var request = "viewType=userTypes&source="+source
		var request = {
				"source" : source,
				"viewType" : "userTypes"
		};
		//console.log(request);
		callAjaxService("FAQReportPost", FAQReportPostSuccessCallback, callBackFailure,
				request,"POST");
	};

	var FAQReportPostSuccessCallback = function(data){
		var screens = [];
		disableLoading();
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		if(data !== undefined 
				&& data !== null 
		){
			if(data && data.length>0){
				for(var i=0;i<data.length;i++){
					if(data[i].screen){
						try{
							screens = JSON.parse(data[i].screen);
							data[i].screen = screens.map(function(e) { return e.name; }).toString();
						} catch (e) {
					    }
					}
				}
			}
			var dataTable = $("#faqTable")
			.DataTable(
					{
						"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
						"processing" : true,
						"sort" : true,
						"info" : true,
						"jQueryUI" : false,
						"scrollX" : true,
						"stateSave": true,
						"data" : data,/*,
								"tableTools" : {
									"aButtons" : [ {
										"sExtends" : "collection",
										"sButtonText" : 'Export <span class="caret" />',
										"aButtons" : [ "csv", "pdf" ]
									} ],
									"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
								},*/
						"destroy" : true,
						"columns" : [{											

							"data" : "id",
							"defaultContent" : "",
							"title":"ID"
						},{											

							"data" : "question",
							"defaultContent" : "",
							"title":"Question"
						},

						{

							"data" : "role",
							"defaultContent" : "",
							"title":"Role",
							/*"render" : function(data, type, full)
												{
													var i = 0, roleStr = '';
													for(i = 0; i < data.length; i++)
														{
															if (i < (data.length - 1))
																roleStr = roleStr + data[i] + ', ';
															else
																roleStr = roleStr + data[i];
														}
													//console.log(rdata);
													return roleStr;
												},*/
						},/*{

											"data" : "roles",
											"defaultContent" : ""
										},{

											"data" : "relatedWorkOrderTypeDesc",
											"defaultContent" : ""
										},*/{
							"data" : "enabled",
							"defaultContent" : "",
							"title":"Enabled",
							"render" : function(data, type, full)
							{
								if (data)
									return '<span class="label label-success btn-sm">Active</span>';

								return '<span class="label label-danger btn-sm">Disabled</span>';
							}
						},{
							"data" : "screen",
							"defaultContent" : "",
							"title":"Screen"
						}],
						"rowCallback" : function(nRow, aData,
								iDisplayIndex, iDisplayIndexFull) {
							$(nRow)
							.attr(
									{
										'onClick' : "javascript:FAQReport.Edit( '"+ aData.id+ "')"
									});
						}
					/*"fnRowCallback" : function(nRow, aData,
										iDisplayIndex, iDisplayIndexFull)
									};
					 */	
					});
			callAjaxService("GetAllRoles",callBackSuccessGetRole,callBackFailure,null,"POST");
		}
	};
	var callBackSuccessGetRole = function(data){
		THIS.Roles = [];
		disableLoading();
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		if(data !== undefined 
				&& data !== null 
		){
			for (var i=0; i<data.length ;i++){
				THIS.Roles.push({"id":data[i].name,"text":data[i].displayName});
			}
			$("#type").select2({
				placeholder : "Chose Roles",
				data : THIS.Roles,
				multiple: true,
			});
			/* $('#screen').select2({
					data : THIS.Roles
				});*/

			var masterURL = directoryName + '/rest/MasterData/searchMasterDataFilter';

			function formatResult(resp) {
				return resp.name;
			}

			function formatSelection(resp) {
				return  resp.name;
			}

			$("#screen").select2({
				placeholder : "Screen",
				minimumInputLength : 1,
				maximumSelectionSize: 4,
				formatSelectionTooBig: function (limit) {

					// Callback

					return 'You have already selected action';
				},
				multiple : true,
				initSelection: function (element, callback) {
	                var data = { "id" :"name" };
	                callback(data);
	            },
				ajax : { // instead of writing the function to execute the request we
					// use
					// Select2's convenient helper
					url :  masterURL,
					dataType : 'json',
					type : 'GET',
					data : function(term) {
						return {
							q : term,
							type : 'A',
							source : THIS.datasource
						};
					},
					results : function(data) { // parse the results into the format
						// expected by Select2.
						// since we are using custom formatting functions we do not need
						// to alter remote JSON data
						// console.log(data);
						  /*var results = [index];
			                $.each(data, function (index, item) {
			                    results.push({
			                        id: item['id'],
			                        text: item['name']
			                    });
			                });*/
						return {
							results : data
						};
					}
				},
				formatResult : formatResult, // omitted for brevity, see the source
				// of
				// this page
				formatSelection : formatResult, // omitted for brevity, see the
				// source of this page
				dropdownCsclass : "bigdrop", // apply css that makes the dropdown
				// taller
				escapeMarkup : function(m) {
					return m;
				} // we do not want to escape
				// markup since we are
				// displaying html in
				// results
			});

		}
	};
	this.CreateView = function(){
		$("#searchForm").submit(function(e) {
			e.preventDefault();
		});
		$("#faqGridDiv,#faqEditDiv").toggleClass('hide');		
		$("#btnCreate").html("Create").unbind('click').bind('click',function(){FAQReport.Create();});
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");

		/*var masterURL = directoryName + '/rest/MasterData/searchMasterDataFilter';*/

		function formatResult(resp) {
			return resp.name;
		}

		function formatSelection(resp) {
			return  resp.name;
		}

	};
	this.Create = function(){
		if (!$("#frmSankeyFilter").valid())
			return;
		var enable =0;

		if ($("#FAQEnabled").is(':checked'))
			enable = 1;	

		var question = CKEDITOR.instances['question'].getData();
		var answer = CKEDITOR.instances['answer'].getData();
		var roles = $("#type").val();
		var screen = JSON.stringify($("#screen").select2('data'));
		var request = {
				"question":question,
				"answer":answer,
				"role":roles,
				"screen":screen,
				"enabled": enable,

		};
		callAjaxService("CreateFAQ",callbackSucessCreateUser,callBackFailure,request,"POST");
	};
	var callbackSucessCreateUser = function(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		} else {
			$("#smart-form-register input").val("");
			$("#type").select2("val",null);
			$("#faqGridDiv,#faqEditDiv").toggleClass('hide');
			THIS.Load();
			showNotification("success","FAQ created Sucessfully..");
		};
	};
	this.Edit = function(id){
		$("#faqGridDiv,#faqEditDiv").toggleClass('hide');
		var request = {
				"id":id
		};		
		callAjaxService("GetFAQ", function (response){callbackSucessGetFAQ(response);}, callBackFailure, request, "POST");
	};

	var callbackSucessGetFAQ = function(response){
		var screens;
		if(response.screen){
			 try{
				 screens = JSON.parse(response.screen);
			} catch (e) {
		    }
		}
		var roles = THIS.Roles.filter(function(obj) {
		    return response.role.split(',').indexOf(obj.id)>-1;
		});
		$("#btnCreate").html("Update").unbind('click').bind('click',function(){FAQReport.Update(response.id);});
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		CKEDITOR.instances['question'].setData(response.question);
		CKEDITOR.instances['answer'].setData(response.answer);
		$("#screen").select2('data',screens);
		$("#type").select2('data',roles);
		if (response.enabled == 1)
			$("#FAQEnabled").attr("checked","true");
		else 
			$("#FAQEnabled").removeAttr("checked");
	};
	this.Update = function(id){
		if (!$("#frmSankeyFilter").valid())
			return;
		var enable =0;
		if ($("#FAQEnabled").is(':checked'))
			enable = 1;


		var role = $("#type").val();
		var question = CKEDITOR.instances['question'].getData();
		var answer = CKEDITOR.instances['answer'].getData();
		var screen =  JSON.stringify($("#screen").select2('data'));

		var request = {
				"id":id,
				"question": question,
				"answer":answer,
				"role": role,
				"enabled": enable,
				"screen":screen,
		};
		callAjaxService("UpdateFAQ",callbackSucessUpdateFAQ,callBackFailure,request,"POST");
	};
	var callbackSucessUpdateFAQ = function(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		} else {
			$("#smart-form-register input").val("");
			$("#type").select2("val",null);
			$("#faqGridDiv,#faqEditDiv").toggleClass('hide');
			THIS.Load();
			showNotification("success","FAQ updated Sucessfully..");
		};
	};
};
FAQReportInstance();