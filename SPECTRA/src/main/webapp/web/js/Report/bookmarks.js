var bookmarks;
var bookmarksInstance = function() {
    bookmarks = new bookmarksObject();
    bookmarks.Load();
};
var bookmarksObject = function() {
    var THIS = this;
    THIS.currOrder = "desc";
    
    var location = localStorage.getItem("searchLocation");
    if(location) {
    	location = location.replace('#', '');
    }
    var request = {
            "type": "Bookmark",
            "moduleName": location,
            "orderBy" : "logdate",
            "orderStyle": THIS.currOrder
        };
    
    this.ChageOrder = function() {
    	THIS.currOrder = (THIS.currOrder === "desc") ? "asc" : "desc";
    	if(	THIS.currOrder === 'asc') {
    		$('.desc').show();
    		$('.asc').hide();
    	} else {
    		$('.desc').hide();
    		$('.asc').show();
    	}
    	 if(localStorage.getItem("searchLocation")){
         	callAjaxService("getSearchLogs", UserReportPostSuccessCallback, callBackFailure,
                     request, "POST");
         } else {
         	if (System.secondaryRoles && System.secondaryRoles.length > 0) {
                 callAjaxService("integraSearhcHistory", UserReportPostSuccessCallback, callBackFailure,
                     request, "POST");
             } else {
                 callAjaxService("bookmarksPost", UserReportPostSuccessCallback, callBackFailure,
                     request, "POST");
             }
         }
    }
    
    this.Load = function(source) {
        enableLoading();
        //var request = "viewType=userTypes&source="+source
        if(localStorage.getItem("searchLocation")){
        	callAjaxService("getSearchLogs", UserReportPostSuccessCallback, callBackFailure,
                    request, "POST");
        } else {
        	if (System.secondaryRoles && System.secondaryRoles.length > 0) {
                callAjaxService("integraSearhcHistory", UserReportPostSuccessCallback, callBackFailure,
                    request, "POST");
            } else {
                callAjaxService("bookmarksPost", UserReportPostSuccessCallback, callBackFailure,
                    request, "POST");
            }
        }
    };
    
    var deleteSerchLog = function(id, trElement) {
    	if(id) {
    		var request = {id: id};
    		enableLoading();
    		callAjaxService("deleteSearchLog", function(response) {
    			 disableLoading();
    			 if (response && response.isException) {
    		            showNotification("error", response.customMessage);
    		            return;
    		      }
    			  if(response) {
    				  showNotification("success", "Search Strategy Removed Successfully");
    				  $(trElement).remove();
    			  }
    		}, callBackFailure, request, "POST");
    	}
    }
    
    var UserReportPostSuccessCallback = function(data) {
        disableLoading();
        if (data && data.isException) {
            showNotification("error", data.customMessage);
            return;
        }
        $("#searchHistoryTable tbody").empty()
        var searchHistoryTable = $("#searchHistoryTable");
        if (data !== undefined && data !== null && data.length > 0) {
            $("#bookmarkcount").html(" (" + data.length + ")");
            var canDelete = (lookUpTable.get('canDeleteBookMark') === 'true') ? true : false;
            for (var i = 0; i < data.length; i++) {
            	var str = '';            	
            	if(canDelete){
            		str = '<i style="color: #646464" data-id = "'+data[i].id+'" class="fa fa-trash fa-lg bookMarkDelete pull-right icon-hover" aria-hidden="true"></i>';
            	}
            	var isSame = (data[i].displayQuery === data[i].jsonQuery) ? '' : data[i].jsonQuery;
                $("<tr/>")
                    //.append($("<td/>").css({ "width": "2px" }))
                    .append($("<td/>").append($("<h4/>").attr({ "data-searchkey": data[i].displayQuery }).append($("<a/>").html(data[i].bookmarkName))))
                    .append($("<td/>").html(data[i].jsonQuery))
                    .append($("<td/>").html(data[i].displayQuery))
                    .append($("<td/>").append($("<i/>").html(data[i].logDate)).append(str))
                    .appendTo(searchHistoryTable);
            }
            $('.bookMarkDelete').on('click', function(){
            	if($(this).attr('data-id')) {
            		deleteSerchLog($(this).attr('data-id'), $(this).closest('tr'));
            	}
            });
        } else {
            $(".well").empty();
            /*$("<tr/>")
            .append($("<td/>").
            		append(*/
            $("<div/>").html("No Search Strategies found.").addClass("alert alert-warning fade in")
                .prepend($("<button/>").addClass("close").data("dismiss", "alert").html("×"))
                .prepend($("<i/>").addClass("fa-fw fa fa-warning")).appendTo($(".well"));
            /*<div class="alert alert-warning fade in">
			<button class="close" data-dismiss="alert">
				×
			</button>
			<i class="fa-fw fa fa-warning"></i>
			<strong>Warning</strong> Your monthly traffic is reaching limit.
		</div>*/
        }
        $(".table-forum h4").addClass("cursor-pointer").unbind("click").bind("click", function() {
            searchKey = $(this).data("searchkey");
            if(localStorage.getItem("searchLocation")) {
            	$('#searchconfigterm').val(searchKey);
            	$('#search').trigger('click');
            	window.location.hash = localStorage.getItem("searchLocation");
            } else {
            	if (System.secondaryRoles && System.secondaryRoles.length > 0) {
                    window.location.hash = "#integrasearch";
                } else {
                    window.location.hash = "#catalogSearch";
                }
            }            
        });
    };
};
bookmarksInstance();
