'use strict';
pageSetUp();
(function(){
	var THIS = {};
	var reportId;
	var textarea = $("#queryDataSource");
	$("#btnCreate").unbind('click').bind('click', function() {
		
		Create();
	});
	$("#smart-mod-eg1").unbind('click').bind('click', function() {
		reportId = 0;
		$("#updateReport").html('Create');
		CreateView();
	});
	$("#updateReport").unbind('click').bind('click', function() {
	if(reportId >0)
		{
		Update(reportId);
		}else{
			Create();
		}
	});
	$("#cancelReport").unbind('click').bind('click', function() {
	$('#dataTableContent').show();
	$("#myModalsave").hide();
	});
	var $registerForm = $("#frmSankeyFilter").validate({

		// Rules for form validation
		rules : {
			name : {
				required : true
			},
			description : {
				required: true
			},
			queryDataSource: {
				required: true
			},
			param: {
				required: true
			},
			dbName: {
				required: true
			}
		},
		// Messages for form validation
		messages : {
			name : {
				displayname : 'Please enter the name'
			},
			description : {
				required : 'Please enter description'
			},
			queryDataSource:{
				required : 'Please enter query'
			},
			param:{
				required : 'Please enter param'
			},
			dbName:{
				required : 'Please enter dbName'
			}
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	$("#frmSankeyFilter").submit(function(e) {
		e.preventDefault();
	});
	
	var Load = function() {
		enableLoading();
		var request = {

		};
		callAjaxService("GetAdhocReports", AdhocReportsCallback,  callBackFailure,
				request, "POST");
	};
	
	THIS.queryEditor = CodeMirror.fromTextArea(textarea[0], {
		lineNumbers : true,
		mode : "text/x-sql",
		autofocus : true,
		matchBrackets : true,
		styleActiveLine : true,
		showCursorWhenSelecting : true
	});
	var CreateView = function(){
		$('#dataTableContent').hide();
		$("#myModalsave").show();
		$("#smart-form-register input").val("");
		$("#dbName").select2("val",null);
		$("#source").select2("val",null);
		$("#description").val("");
		THIS.queryEditor.setValue("");
		$("#frmSankeyFilter").find(".state-success").removeClass("state-success");
		$("#btnCreate").html("").append($("<i/>").addClass("fa fa-plus")).append(" Create").unbind('click').bind('click',function(){Create();});
		
		setTimeout(function() {
			THIS.queryEditor.refresh();
		}, 300);
		};	
	var AdhocReportsCallback = function(data){
		disableLoading();
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		if(data !== undefined && data !== null ){
			var dataTableData = data;

			var dataTable = $(".dc-data-table")
			.DataTable(
					{

						"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
						"processing" : true,
						"lengthChange" : true,
						"bSort" : true,
						"info" : true,
						"jQueryUI" : false,
						"scrollX" : true,
						"data" : dataTableData,
						"bAutoWidth": false,
						"tableTools" : {
							"aButtons" : [ {
								"sExtends" : "collection",
								"sButtonText" : 'Export <span class="caret" />',
								"aButtons" : [ "csv", "pdf" ]
							} ],
							"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
						},
						"destroy" : true,
						"columns" :[{											

							"data" : "name",
							"defaultContent" : ""
						},{											

							"data" : "description",
							"defaultContent" : ""
						},{

							"data" : "query",
							"defaultContent" : ""
						},{

							"data" : "param",
							"defaultContent" : ""
						},{

							"data" : "source",
							"defaultContent" : ""
						},{

							"data" : "roleId",
							"defaultContent" : ""
						},{

							"data" : "dbName",
							"defaultContent" : ""
						}

						],
						"rowCallback" : function(nRow, aData) {
							$(nRow).unbind('click').bind('click',function(){Edit(aData.id);});
						}
					});

		}
	};
	$('#dbName').select2({
		data : dbName
	});

	var sourceName = [ {
		"id" : "hive",
		"text" : "HIVE"
	}, {
		"id" : "postgreSQL",
		"text" : "PostgreSQL"
	}];
	var callBackGetDatabaseList = function(response,dbName){
		disableLoading();
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		if(response !== undefined 
				&& response !== null 
		){
			var databaseList = [];
			for (var i=0;i<response.length;i++){
				databaseList.push({"id":response[i],"text":response[i]});
			}
			$("#dbName").select2({
				placeholder : "Choose Database",
				allowClear: true,
				data :  databaseList
			});
			if(dbName){
				$("#dbName").select2("val",dbName);
			}
		}
	};
	var sourceSelect = function(source,dbName){
		if (source  === "hive" || source  === "postgreSQL"){
			var request ={
					"source":source
			};
			enableLoading();
			callAjaxService("getDatabaseList",  function(response){callBackGetDatabaseList(response,dbName);},
					callBackFailure, request, "POST");
		}
	};
	
	//constructDropDown("#source","select source as id, source as name from report  where lower(source)  like '%<Q>%' group by source");
$("#source").select2({
		data : sourceName,
		placeholder : "Choose Source",
	}).on("change", function(e) {
		sourceSelect(e.val);
	});


	var callbackSucessCreateAdhocReport = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			Load();
			$("#myModalsave").hide();
			$('#dataTableContent').show();
			showNotification("success", "AdhocReports Created Sucessfully..");
		}
	};
	var Create = function() {
	
		if (!$("#frmSankeyFilter").valid())
			return;
		var source = '';
		if ( $("#source").select2("val") !== "")
			source = $("#source").select2("val");
		var dbName = '';
		if ( $("#dbName").select2("val") !== "")
			dbName = $("#dbName").select2("val");
		var param = $("[name=param]").val() || "";
		var adhocReport = {
				"name" : $("[name=profilename]").val(),
				"description" : $("#description").val(),
				"query" : THIS.queryEditor.getValue(),
				"param":  param,
				"roleId":  $("[name=roleId]").val(),
				"source" : source,
				"dbName" : dbName,
		};
		var request = {};
		request.adhocReportRequest = JSON.stringify(adhocReport);
		callAjaxService("CreateAdhocReports", callbackSucessCreateAdhocReport, callBackFailure,
				request, "POST");
	};

	var Update = function(id){
		if (!$("#frmSankeyFilter").valid())
			return;
		var source = '';
		if ( $("#source").select2("val") !== "")
			source = $("#source").select2("val");
		var dbName = '';
		if ( $("#dbName").select2("val") !== "")
			dbName = $("#dbName").select2("val");
		var param = $("[name=param]").val() || "";
		var adhocReport = {
				"id" : id,
				"name" : $("[name=profilename]").val(),
				"description": $("#description").val(),
				"query" : THIS.queryEditor.getValue(),
				"param":  param,
				"roleId":  $("[name=roleId]").val(),
				"source" : source,
				"dbName" : dbName,
		};
		var callbackSucessUpdateAdhocReport = function(response){
			if (response && response.isException){
				showNotification("error",response.customMessage);
				return;
			} else {
				$('#myModalsave').hide();
				$('#dataTableContent').show();
				$("#smart-form-register input").val("");
				$("#type").select2("val",null);
				Load();
				showNotification("success","AdhocReport updated Sucessfully..");
			};
		};
		var request = {};
		request.adhocReportRequest = JSON.stringify(adhocReport);
		callAjaxService("UpdateAdhocReports", callbackSucessUpdateAdhocReport, callBackFailure,
				request, "POST");
	};

	var callbackSucessGetAdhocReport = function(response){
		disableLoading();
		$('#dataTableContent').hide();
		$("#myModalsave").show();
		$("#updateReport").html('Update');
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {;
			$("[name=profilename]").val(response.name);
			$("[name=description]").val(response.description);
			THIS.queryEditor.setValue(response.query);
			
			$("#source").select2("val",response.source);
			sourceSelect(response.source,response.dbName);
			$("#dbName").select2("val",response.dbName);
			
			setTimeout(function() {
				THIS.queryEditor.refresh();
			}, 300);
		}
	};
	var Edit = function(id){
		enableLoading();
		reportId = id;
		var request = {
				"id":id
		};
		callAjaxService("GetAdhocReport", function(response) {callbackSucessGetAdhocReport(response);}, callBackFailure, request, "POST");	

	};
	Load();
})();