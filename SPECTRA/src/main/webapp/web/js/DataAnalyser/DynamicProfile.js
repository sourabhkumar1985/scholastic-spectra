var dataAnalyser = null;
pageSetUp();
var enableWidget = function() {
	var action = window.location.hash.split('#')[1]
	var roles = System.roles
	roles.push(System.businessRole)
	role =''
	roles.forEach(function(data){
		role+='\''+ data+  '\','; 
	})
	role = role.slice(0,-1)
	var filterQuery = role
	var request ={
		 source:'postgreSQL',
		 database:'E2EMFPostGres',
		 //table:'widget_config',
		 query:'select widget from widget_config where id in (SELECT group_id FROM widget_config_logs where role in (<FILTER>) and enabled="1") and action = "'+action+'"',
		 columns:'widget',
		 filters:filterQuery,
		 groupByEnabled:false,
		 cacheDisabled:true
		 
	};
callAjaxService("getData", callBackSuccessGetWidgetData, callBackFailure,
		 request, "POST",null,true);
}

var callBackSuccessGetWidgetData = function(response){
	System.EnabledWidget = response;
	dynamicProfling();
	
}
var dynamicProfling = function() {
	$("#actionDiv").empty();
	$("#actionDiv").prepend(
			$("<ul/>").addClass("nav nav-tabs pull-right in no-border").attr(
					"id", "myTab"));
	var THIS = this;
	var callBackActionHandler = function(respose, profileConfigKey,
			profileViewId) {
		var constructTabs = function(tabsData, type) {
			var item;
			var source;
			var template;
			var tabHtml;
			var sourcesInfo;
			if (type === 'profile') {
				if (tabsData.source && tabsData.source.length > 30) {
					sourcesInfo = jQuery.parseJSON(tabsData.source);
					for (var i = 0; i < sourcesInfo.length; i++) {
						item = sourcesInfo[i];
						item.active = i === 0 ? "active" : "";
						source = $("#tab-Template").html();
						template = Handlebars.compile(source);
						tabHtml = template(item);
						$('#actionDiv #myTab').removeClass("no-border")
								.prepend(tabHtml);
					}
					$('#actionDiv #myTab li.tabinfoTitle')
							.off("click")
							.on(
									"click",
									function(e) {

										var profileKey = $(e.currentTarget)
												.attr("data-profilekey");
										var request = {
											"profileKey" : profileKey
										};
										$(
												"#dataAnalyserProfile #widget-grid .widget-body.row")
												.empty();
										$("#divSankeyFilter").empty();
										$(".dc-data-count").empty();
										enableLoading();
										callAjaxService("getProfileConfig",
												callBackGetProfileConfig,
												callBackFailure, request,
												"POST");
									});
				}
			} else if (tabsData.source && tabsData.source.length > 30) {
				sourcesInfo = jQuery.parseJSON(tabsData.source);
				for (var j = 0; j < sourcesInfo.length; j++) {
					item = sourcesInfo[j];
					item.active = j === 0 ? "active" : "";
					source = $("#tab-Template").html();
					template = Handlebars.compile(source);
					tabHtml = template(item);

					$('#actionDiv #myTab').append(tabHtml);
				}

				$('#actionDiv #myTab li.tabinfoTitle').off("click").on(
						"click",
						function(e) {
							var profileKey = $(e.currentTarget).attr(
									"data-profilekey");
							var request = {
								"profileKey" : profileKey
							};
							enableLoading();
							$("#sankey").empty();
							$("#divSankeyFilter").empty();
							$(".dc-data-count").empty();
							callAjaxService("getProfileConfig",
									callBackGetProfileConfig, callBackFailure,
									request, "POST");
						});
			}
		};

		var deleteView = function(obj) {
			var profileId = $(obj).parent().parent().data("profileId");
			var viewId = $(obj).parent().parent().data("viewId");
			var query = 'delete from profile_views where id =' + viewId;
			var updateRequest = {
				"source" : 'postgreSQL',
				"dataBase" : 'E2EMFPostGres',
				"queries" : btoa(query)
			}
			callAjaxService("integraGenericUpdate", function(response) {
				deleteCallBack(response, profileId, viewId);
			}, callBackFailure, updateRequest, "POST", "json", true);
		};

		var deleteCallBack = function(response, profileId, viewId) {
			if (response && response.isException) {
				showNotification("error", response.customMessage);
				return;
			}/*
				 * else if (views === undefined || views === null) {
				 * showNotification("error", "No Data avaiable.."); return; }
				 */else {
				showNotification("success", "view deleted successfully.");
				var request = {
					"profileKey" : profileKey,
					"viewId" : profileViewId
				};
				$("#dataAnalyserProfile #widget-grid .widget-body.row").empty();
				$("#divSankeyFilter").empty();
				$(".dc-data-count").empty();
				callAjaxService("getProfileConfig", callBackGetProfileConfig,
						callBackFailure, request, "POST");
			}
		}

		var viewClick = function() {
			var viewName = $($(this).find('label')[0]).text();
			var profileId = $(this).data("profileId");
			var viewId = $(this).data("viewId");
			var request = {
				"id" : profileId,
				"viewId" : viewId
			};
			$("#dataAnalyserProfile #widget-grid .widget-body.row").empty();
			$("#divSankeyFilter").empty();
			$(".dc-data-count").empty();
			enableLoading();
			callAjaxService("getProfileConfig", function(response) {
				callBackGetProfileConfig(response, viewName);
			}, callBackFailure, request, "POST");
		};

		var callBackSaveView = function(views) {
			if (views && views.isException) {
				showNotification("error", views.customMessage);
				return;
			} else if (views === undefined || views === null) {
				showNotification("error", "No Data avaiable..");
				return;
			} else {
				showNotification("success", "view saved successfully.");
				var myViews = $("[name=myViews]", ".profile-settings").empty();
				var publicViews = $("[name=publicViews]", ".profile-settings")
						.empty();
				publicViews.append($("<article/>").addClass("row").data({
					"viewId" : -1,
					"profileId" : dataAnalyser.profileId
				}).click(viewClick).append(
						$("<section/>").addClass("col col-6").append(
								$("<label/>").addClass("label").html(
										"Default View"))).append(
						$("<section/>").addClass("col col-6").append(
								$("<label/>").addClass("label").html(
										"Administrator"))));
				var noViewsMessage = $("<section>").addClass("col col-12")
						.append(
								$("<label/>").addClass("label").html(
										"No Views Available.."));
				var myViewsAvaiable = false;
				for (var v = 0; v < views.length; v++) {
					var defaultI = views[v].isDefault
							&& views[v].isDefault !== "f" ? $("<i/>").addClass(
							"fa fa-check txt-color-green").attr({
						"title" : "Default view"
					}) : $("<i/>");
					var publicI = views[v].isPublic
							&& views[v].isPublic !== "f" ? $("<i/>").addClass(
							"fa fa-users").attr({
						"title" : "Public"
					}) : $("<i/>").addClass("fa fa-user").attr({
						"title" : "Private"
					});
					if (views[v].viewType) {
						myViewsAvaiable = true;
						myViews.append($("<article/>").addClass("row").data({
							"viewId" : views[v].id,
							"profileId" : views[v].profileId
						}).click(viewClick).append(
								$("<section/>").addClass("col col-3").append(
										$("<label/>").addClass("label").html(
												views[v].name))).append(
								$("<section/>").addClass("col col-1").append(
										publicI)).append(
								$("<section/>").addClass("col col-5").append(
										$("<label/>").addClass("label").html(
												views[v].createdOn))).append(
								$("<section/>").addClass("col col-1").append(
										defaultI)).append(
								$("<section/>").addClass("col col-2").append(
										$("<i/>").addClass("fa fa-trash").attr(
												"title", "delete view").unbind(
												"click").bind("click",
												function(e) {// e.preventDefault();
													e.stopPropagation();
													deleteView(this);
												}))));
					} else {
						publicViews
								.append($("<article/>")
										.addClass("row")
										.data({
											"viewId" : views[v].id,
											"profileId" : views[v].profileId
										})
										.click(viewClick)
										.append(
												$("<section/>").addClass(
														"col col-5").append(
														$("<label/>").addClass(
																"label").html(
																views[v].name)))
										.append(
												$("<section/>")
														.addClass("col col-2")
														.append(
																$("<label/>")
																		.addClass(
																				"label")
																		.html(
																				views[v].createdBy)))
										.append(
												$("<section/>")
														.addClass("col col-5")
														.append(
																$("<label/>")
																		.addClass(
																				"label")
																		.html(
																				views[v].createdOn)))
										.append(
												$("<section/>")
														.addClass("col col-2")
														.append(
																$("<i/>")
																		.addClass(
																				"fa fa-trash")
																		.attr(
																				"title",
																				"delete view")
																		.unbind(
																				"click")
																		.bind(
																				"click",
																				function(
																						e) {// e.preventDefault();
																					e
																							.stopPropagation();
																					deleteView(this);
																				}))));
					}
				}
				if (!myViewsAvaiable)
					myViews.append(noViewsMessage);

				$(".profile-settings [name=emailNotification]").unbind("click")
						.bind(
								"click",
								function() {
									drawEmailNotification(views,
											THIS.emailNotification);
								});
			}
		};
		var saveView = function() {
			var name = $("[name=viewname]", "#actionDiv").val();
			if (name === "")
				return;
			var isDefault = $("[name=default]", "#actionDiv").is(":checked");
			var isPublic = $("[name=public]", "#actionDiv").is(":checked");
			if (isDefault)
				isDefault = true;
			else
				isDefault = false;

			if (isPublic)
				isPublic = true;
			else
				isPublic = false;

			var viewFilters = dataAnalyser.getFiltersFromProfile();
			var config = {
				widget:{}	
			};
			$("#widget-grid").find(".jarviswidget").each(function(index,ele){
				var id = $(ele).attr("id");
				if(!$(ele+" > div").is(":visible")){
					config.widget[id].hidden = true;
				}
			});
			var request = {};
			var view = {
				"id" : 0,
				"profileId" : dataAnalyser.profileId,
				"name" : name,
				"viewFilters" : JSON.stringify(viewFilters),
				"viewConfig":JSON.stringify(config),
				"isDefault" : isDefault,
				"isPublic" : isPublic,
				"viewSetting" : JSON.stringify(dataAnalyser.profiling.grapher
						.getChartFilters()),
				"filterQuery" : dataAnalyser.profiling.grapher
						.constructFilterQuery()
			};
			request.view = JSON.stringify(view);
			callAjaxService("saveView", callBackSaveView, callBackFailure,
					request, "POST");
		};

		var drawViews = function(views, profileConfig, viewName) {
			profileConfig.viewname = viewName || profileConfig.viewname;
			$(".profile-settings").remove();
			$("<div/>").addClass("profile-settings pull-right").append(
					$("<a/>").addClass("views").attr("title", "Views").append(
							$("<i/>").addClass("fa fa-ellipsis-v"))).append(
					$("<ul/>").css({"width":"350px"}).addClass("dropdown-menu dropdown-menu-right")
							.attr("id", "settingsOption")).prependTo(
					"#actionDiv");
			var dropdownDiv = $("<div/>").addClass("smart-form");
			$("#settingsOption").append($("<li/>").prepend(dropdownDiv));
			var noViewsMessage = $("<section>").addClass("col col-12").append(
					$("<label/>").addClass("label")
							.html("No Views Available.."));
			var myViews = $("<fieldset/>").attr({
				"name" : "myViews"
			});
			var publicViews = $("<fieldset/>").attr({
				"name" : "publicViews"
			});
			var myViewsAvaiable = false;
			var isdefaultInput;
			if (profileConfig.isdefault && profileConfig.isdefault !== "f") {
				isdefaultInput = $("<input/>").addClass("input-xs").attr({
					"type" : "checkbox",
					"name" : "default",
					"checked" : true
				});
			} else {
				isdefaultInput = $("<input/>").addClass("input-xs").attr({
					"type" : "checkbox",
					"name" : "default"
				});
			}
			var ispublicInput;
			if (profileConfig.ispublic && profileConfig.ispublic !== "f") {
				ispublicInput = $("<input/>").addClass("input-xs").attr({
					"type" : "checkbox",
					"name" : "public",
					"checked" : true
				});
			} else {
				ispublicInput = $("<input/>").addClass("input-xs").attr({
					"type" : "checkbox",
					"name" : "public"
				});
			}
			publicViews.append($("<article/>").addClass("row").data({
				"viewId" : -1,
				"profileId" : profileConfig.id
			}).click(viewClick).append(
					$("<section/>").addClass("col col-6").append(
							$("<label/>").addClass("label")
									.html("Default View"))).append(
					$("<section/>").addClass("col col-6").append(
							$("<label/>").addClass("label").html(
									"Administrator"))));
			var saveButtonDesc;
			if (profileConfig.viewtype && profileConfig.viewtype !== "t") {
				saveButtonDesc = " Save As";
			} else {
				saveButtonDesc = " Save";
			}
			$(".breadcrumb span.currentView").remove();
			var viewname = profileConfig.viewname || "Default";
			$(".breadcrumb li:last")
					.append(
							$("<span/>").addClass("currentView").html(
									" - " + viewname));
			dropdownDiv
					.append($("<fieldset/>")
							.addClass("padding-top-20")
							.append(
									$("<section/>")
											.addClass("col col-9")
											.append(
													$("<label/>")
															.addClass("input")
															.append(
																	$(
																			"<input/>")
																			.addClass(
																					"input-xs")
																			.attr(
																					{
																						"type" : "text",
																						"name" : "viewname"
																					})
																			.val(
																					profileConfig.viewname))))
							.append(
									$("<section/>")
											.addClass("col col-3")
											.append(
													$("<button/>")
															.addClass(
																	"btn btn-primary btn-xs")
															.append(
																	$("<i/>")
																			.addClass(
																					"fa fa-floppy-o")
																			.html(
																					saveButtonDesc)
																			.click(
																					function() {
																						saveView();
																					}))))
							.append(
									$("<section/>")
											.addClass(
													"col col-6 padding-left-40")
											.append(
													$("<label/>")
															.addClass(
																	"checkbox")
															.append(
																	isdefaultInput)
															.append($("<i/>"))
															.append(
																	$("<span/>")
																			.addClass(
																					"font-xs")
																			.html(
																					"Make it my default"))))
							.append(
									$("<section/>")
											.addClass("col col-6")
											.append(
													$("<label/>")
															.addClass(
																	"checkbox")
															.append(
																	ispublicInput)
															.append($("<i/>"))
															.append(
																	$("<span/>")
																			.addClass(
																					"font-xs")
																			.html(
																					"Make it public")))));
			for (var v = 0; v < views.length; v++) {
				var defaultI = views[v].isDefault && views[v].isDefault !== "f" ? $(
						"<i/>").addClass("fa fa-check txt-color-green").attr({
					"title" : "Default view"
				})
						: $("<i/>");
				var publicI = views[v].isPublic && views[v].isPublic !== "f" ? $(
						"<i/>").addClass("fa fa-users").attr({
					"title" : "Public"
				})
						: $("<i/>").addClass("fa fa-user").attr({
							"title" : "Private"
						});
				if (views[v].viewType) {
					myViewsAvaiable = true;
					myViews.append($("<article/>").addClass("row").data({
						"viewId" : views[v].id,
						"profileId" : views[v].profileId
					}).click(viewClick).append(
							$("<section/>").addClass("col col-3").append(
									$("<label/>").addClass("label").html(
											views[v].name))).append(
							$("<section/>").addClass("col col-1").append(
									publicI)).append(
							$("<section/>").addClass("col col-5").append(
									$("<label/>").addClass("label").html(
											views[v].createdOn))).append(
							$("<section/>").addClass("col col-1").append(
									defaultI)).append(
							$("<section/>").addClass("col col-2").append(
									$("<i/>").addClass("fa fa-trash").attr(
											"title", "delete view").unbind(
											"click").bind("click", function(e) {// e.preventDefault();
										e.stopPropagation();
										deleteView(this);
									}))));
				} else {
					publicViews.append($("<article/>").addClass("row").data({
						"viewId" : views[v].id,
						"profileId" : views[v].profileId
					}).click(viewClick).append(
							$("<section/>").addClass("col col-3").append(
									$("<label/>").addClass("label").html(
											views[v].name))).append(
							$("<section/>").addClass("col col-2").append(
									$("<label/>").addClass("label").html(
											views[v].createdBy))).append(
							$("<section/>").addClass("col col-5").append(
									$("<label/>").addClass("label").html(
											views[v].createdOn))).append(
							$("<section/>").addClass("col col-2").append(
									$("<i/>").addClass("fa fa-trash").attr(
											"title", "delete view").unbind(
											"click").bind("click", function(e) {
										e.stopPropagation();
										deleteView(this);
										;
									}))));
				}
			}
			if (!myViewsAvaiable)
				myViews.append(noViewsMessage);

			dropdownDiv.append($("<header/>").html("My Views"));
			dropdownDiv.append(myViews);
			dropdownDiv.append($("<header/>").html("Public Views"));
			dropdownDiv.append(publicViews);
			dropdownDiv.append($("<hr/>"));
		};
		var callBackSaveEmailNotification = function(response, views,
				emailNotification) {
			disableLoading();
			if (response && response.isException) {
				showNotification("error", response.customMessage);
				return;
			} else if (response === undefined || response === null) {
				showNotification("error", "No Data avaiable..");
				return;
			} else {
				$("#myModalDrilldown").modal("hide");
				showNotification("success", response.message);
				$(".profile-settings [name=emailNotification]").unbind("click")
						.bind("click", function() {
							drawEmailNotification(views, emailNotification);
						});
				THIS.emailNotification = emailNotification;
			}
		};
		var saveEmailNotificationForm = function(views) {
			var request = {};
			if (!$("#emailNotificationForm").valid())
				return;
			enableLoading();
			var emailFrequency = $("#emailFrequency").val();
			var notification = {
				"profileId" : dataAnalyser.profileId,
				"viewId" : $("#emailViewSelect").val(),
				"subject" : $("#subject").val(),
				"distEmail" : $("#emailmulti").val(),
				"frequency" : emailFrequency,
				"content" : $("#message").val(),
				"name" : $("#emailViewSelect  option:selected").text(),
				"enabled" : $("#notificationEnabled").is(':checked'),
				"fileformat" : $("#attachmentFormatSelect").val()
			};
			if (emailFrequency === "weekly") {
				notification.frequencyDay = $("#dayWeek .active").data("value");
			} else if (emailFrequency === "monthly") {
				notification.frequencyDay = $("#dayMonth select").val();
			}
			var url = window.location.href;
			var profileName = url.split('#')[url.split('#').length - 1];
			// alert(address);
			request.notification = JSON.stringify(notification);
			request.profileName = profileName;
			callAjaxService("saveEmailNotification", function(response) {
				callBackSaveEmailNotification(response, views, notification);
			}, callBackFailure, request, "POST");
		};
		var setEmailNotification = function(emailNotification) {
			emailNotification.subject = emailNotification.subject || "Default";
			$("#subject").val(emailNotification.subject);
			$("#myModalLabel", "#myModalDrilldown").html(
					"Email Notification: " + emailNotification.subject);
			$("#emailViewSelect").val(emailNotification.viewId);
			$("#attachmentFormatSelect").val(emailNotification.fileFormat);
			if (emailNotification.frequency) {
				var valueSelected = emailNotification.frequency;
				$("#emailmulti").val(emailNotification.distEmail);
				$("#emailFrequency").val(valueSelected);
				if (valueSelected === "weekly") {
					$("#dayWeek").show();
					$("#dayWeek .btn").removeClass("active");
					$(
							"#dayWeek .btn[data-value="
									+ emailNotification.frequencyDay + "]")
							.addClass("active");
					$("#dayMonth").hide();
				} else if (valueSelected === "monthly") {
					$("#dayMonth").show();
					$("#dayMonth select").val(emailNotification.frequencyDay);
					$("#dayWeek").hide();

				} else {
					$("#dayMonth,#dayWeek").hide();
				}
				$("#message").val(emailNotification.content);
				if (emailNotification.enabled)
					$("#notificationEnabled").attr("checked", "true");
				else
					$("#notificationEnabled").removeAttr("checked");
			}
		};
		var drawEmailNotification = function(views, emailNotification) {
			var callBackEmailTemplate = function(emailTemplate) {
				$("#myModalDrilldown .modal-dialog").addClass("width-500");
				$("#myModalDrilldown").modal("show");
				$("#poDocView").empty().append(emailTemplate);
				$("#emailNotificationForm").submit(function(e) {
					e.preventDefault();
				});
				$("#setEmailNotification").click(function() {
					saveEmailNotificationForm(views);
				});
				$("#emailNotificationForm")
						.validate(
								{
									rules : {
										emailmulti : {
											required : true,
											multiemail : true,
											limitto : 10
										},
										subject : {
											required : true,
										},
										message : {
											required : true
										}
									},
									messages : {
										emailmulti : {
											multiemail : "Please enter valid email addresses (separate multiple email addresses using a semicolon)",
											required : 'Please enter your email',
											limitto : "Only maximum of {0} email can be added at a time."
										},
										subject : {
											required : 'Please enter your subject'
										},
										message : {
											required : 'Please enter your message'
										}
									},
									errorPlacement : function(error, element) {
										error.insertAfter(element.parent());
									}
								});
				var daySelect = $("#dayMonth").find("select").empty();

				$("#dayWeek").show();
				$("#dayMonth").hide();
				$("#emailFrequency").change(function() {
					var valueSelected = $(this).find("option:selected").val();
					if (valueSelected === "weekly") {
						$("#dayWeek").show();
						$("#dayMonth").hide();
					} else if (valueSelected === "monthly") {
						$("#dayMonth").show();
						$("#dayWeek").hide();
					} else {
						$("#dayMonth,#dayWeek").hide();
					}
				});
				for (var day = 1; day < 29; day++) {
					var dayPrefix;
					if (day % 10 === 1) {
						dayPrefix = "st";
					} else if (day % 10 === 2) {
						dayPrefix = "nd";
					} else if (day % 10 === 3) {
						dayPrefix = "rd";
					} else {
						dayPrefix = "th";
					}
					daySelect.append($("<option/>").attr("value", day).html(
							day.toString().concat(dayPrefix)));
				}
				daySelect.append($("<option/>").attr("value", "-1").html(
						"Last Day"));

				var viewSelect = $("#emailViewSelect").empty();
				viewSelect.append($("<option/>").attr("value", 0).html(
						"Default"));
				for (var v = 0; v < views.length; v++) {
					viewSelect.append($("<option/>").attr("value", views[v].id)
							.html(views[v].name));
				}
				setEmailNotification(emailNotification);
			};
			callAjaxService("getEmailNotificationTemplate",
					callBackEmailTemplate, callBackFailure, null, "GET", "html");
		};
		var enableEmailNotifiation = function(enable, views, emailNotification) {
			if (enable) {
				$(".profile-settings").prepend(
						$("<a/>").attr("name", "emailNotification").prepend(
								$("<i/>").addClass("fa fa-envelope")).attr(
								"title", "Email Notification").click(
								function() {
									drawEmailNotification(views,
											emailNotification);
								}));
			}
		};
		var downloadPDF = function() {
			var $preparingFileModal = $("#preparing-file-modal");
			$preparingFileModal.dialog({
				modal : true
			});
			$.fileDownload('rest/filedownload/converthtmltopdf', {
				successCallback : function() {
					$preparingFileModal.dialog('close');
				},
				failCallback : function(responseHtml) {
					$preparingFileModal.dialog('close');
					$("#error-modal").html($(responseHtml).text());
					$("#error-modal").dialog({
						modal : true
					});
				},
				httpMethod : "POST",
				data : {
					"content" : $("#widget-grid").html()
				}
			});
		};
		var drawDownload = function() {
			$(".profile-settings").prepend(
					$("<a/>").attr("name", "downloadPDF").prepend(
							$("<i/>").addClass("fa fa-download")).attr("title",
							"Download PDF").click(function() {
						downloadPDF();
					}));
		};
		var callBackGetProfileConfig = function(profileConfig, viewName) {
			if (profileConfig && profileConfig.isException) {
				showNotification("error", profileConfig.customMessage);
				return;
			} else if (profileConfig === undefined || profileConfig === null) {
				showNotification("error", "No Data avaiable..");
				return;
			}
			var configData = JSON.parse(profileConfig.config);		
			// This piece of code is added to show the demo for CELGENE
			if(profileKey === "ProtocolPatientInfo"){
		        var closeIcon =  $("<i/>").addClass("botClose fa fa-times"),
	            notificationText = $('<div/>').addClass("bigBox bigBoxextend animated fadeIn fast").css({"background-color":"rgb(50, 118, 177)"})
	                .append( $("<div/>").append(closeIcon).append( $("<span/>").html("Alert Notification!"))
	                .append( $("<p/>").html("The patient enrolment rate for this Study is below the expected enrolement rate. Please take corrective action."))
	                .append( $("<div/>").addClass("bigboxicon").append($("<i/>").addClass("fa fa-bell swing animated"))));
		        	$(".smart-style-3").append(notificationText); 
		        $('.botClose').click(function(){
		        	$('.bigBox').remove();
		        });
			}
			//***************
			$("#nGramAnalyser").hide();
			if (configData && configData != null && configData.length > 0
					&& configData[0].type === "sankey") {
				if ($('#actionDiv #myTab li.tabinfoTitle').length === 0) // Contsruct
																			// Tabs
																			// on
																			// First
																			// time
																			// Only
					loadScript("js/DataAnalyser/SankeyGenerator.js",
							function() {
								constructTabs(respose, 'sankey');
								var sankey = new sankeyGenerator();
								sankey.getSankeyJSON(configData[0]);
							}, "#dataAnalyserProfile");
			} else if (configData && configData != null
					&& configData.length > 0 && configData[0].type === "plotly") {
				loadScript("js/plotly-latest.min.js", function() {
					loadScript("js/plotly/plotlyAnalyser.js", function() {
					});
				}, "#plotlyAnalyser");
			} else if (configData && configData != null
					&& configData.length > 0 && configData[0].type === "ngram") {
				loadScript("js/nGram/nGramAnalyser.js", function() {
					nGramAnalyser.dynamicProfiling(configData[0]);
				}, "#nGramAnalyser");
			} else if (configData && configData != null
					&& configData.length > 0 && configData[0].type === "search") {
				if (configData[0].fact === "searchIndexer") {
					loadScript("js/SearchConfig/solrIndexer.js", function() {
						var indexer = getsolrIndexer(null, {
							id : configData[0].id
						}, 'view');
						indexer.initView(configData[0].id);
					}, "#dataAnalyserProfile");
				} else if (configData[0].fact === "search") {
					loadScript("js/SearchConfig/searchConfigurator.js",
							function() {
								var confgurator = getConfigurator(null, {
									id : configData[0].id
								}, 'preview');
								confgurator.initView(configData[0].id);
							}, "#dataAnalyserProfile");
				} else {
					loadScript("js/DataAnalyser/searchProfile.js", function() {
						loadScript("js/DataAnalyser/searchEntityDetails.js",
								function() {
									var searchConfig = new searchProfile();
									searchConfig.Load(configData);
								}, "#dataAnalyserProfile");
					}, "#dataAnalyserProfile");
				}
			} else if (configData && configData.length > 0
					&& configData[0].type === "stringMatching") {
				loadScript("js/StringMatching/StringMatching.js", function() {
					var config = JSON.parse(configData[0].config.replace(/@/g,
							"'"));
					var filters = JSON.parse(configData[0].filters.replace(
							/@/g, "'"));
					StringMatching(config, 0, 'view', filters);
				}, "#dataAnalyserProfile");
			} else if (configData && configData != null
					&& configData.length > 0
					&& configData[0].type === "dashboard") {
				dc.deregisterAllCharts();
				dc.chartRegistry.clear();
				loadPageElements();
				allPageSupportNavbarItems();
				loadScript("js/profileAnalyser/profileAnalyser.js", function() {
					if (!profileAnalyser) {
						profileAnalyser = new ProfileAnalyser();
					}
					profileAnalyser.dynamicDashboard(profileConfig,
							configData[0]);
				}, "#dataAnalyserProfile");
			} else if (configData && configData != null
					&& configData.length > 0) {
				
//				Changes to activate the bookmark, search history and favorite icons for all pages.
				
//				$("#shortlist").css({"display":"none"});
//				$("#searchHistory").css({"display":"none"});
//				$("#navagate_bookmarks").css({"display":"none"});
				loadPageElements();
				allPageSupportNavbarItems();
				
//				-------------------------
				
				if ($('#actionDiv #myTab li.tabinfoTitle').length === 0) {
					constructTabs(respose, 'profile');
				} // Contsruct Tabs on First time Only
				dc.deregisterAllCharts();
				dc.chartRegistry.clear();
				loadScript(
						"js/DataAnalyser/DataAnalyser.js",
						function() {
							if (localStorage["status"]) {
								$("#actionDiv").find("li")
										.removeClass("active");
								$("#actionDiv").find(
										"[data-id='" + configData[0].id + "']")
										.addClass("active");
								var filters = Jsonparse(configData[0].filters), lookupquery;
								if (filters && filters.length > 0) {
									for (var i = 0; i < filters.length; i++) {
										if (filters[i].fieldName === 'status') {
											lookupquery = Jsonparse(
													filters[i].lookupquery)
													.filter(
															function(d) {
																return d.id === localStorage["status"];
															});
											filters[i].defaultvalue
													.push(lookupquery[0]);
											break;
										}
									}
								}
								delete localStorage.status;
								configData[0].filters = JSON.stringify(filters);
							}
							if (localStorage["filtersForwaded"]) {
								var filtersForwadedObj =  Jsonparse(localStorage["filtersForwaded"]);
								var filters = Jsonparse(configData[0].filters), lookupquery;
								if (filters && filters.length > 0) {
									for (var i = 0; i < filters.length; i++) {
										if (filtersForwadedObj[filters[i].fieldName]) {	
											var filterObj = filtersForwadedObj[filters[i].fieldName];
											//if(Object.keys(filterObj).length>0){
												if(typeof filters[i].defaultvalue === 'object' && filters[i].defaultvalue !== null){
													if(Array.isArray(filters[i].defaultvalue)){
														filters[i].defaultvalue.push(filterObj);
													}
													else
														filters[i].defaultvalue = [filterObj];												
												}else if(filters[i].defaultvalue && !filters[i].defaultvalue.length>0)
													filters[i].defaultvalue = [filterObj];
												else{
													filters[i].defaultvalue = [filterObj];
												}
											//}
										}
									}
								}
								delete localStorage.filtersForwaded;
								configData[0].filters = JSON.stringify(filters);
							}
							if (profileConfig.dataQuery)
								configData[0].dataQuery = profileConfig.dataQuery;
							profileAnalyser = null;
							dataAnalyser = new DataAnalyser();
							dataAnalyser.dynamicProfiling(configData[0],
									profileConfig.profileActions);
							drawViews(profileConfig.views, configData[0],
									viewName);
							var emailNotification = profileConfig.emailNotification
									|| {
										"subject" : configData[0].viewname,
										"viewId" : configData[0].viewid
									};
							THIS.emailNotification = emailNotification;
							enableEmailNotifiation(true, profileConfig.views,
									emailNotification);
						}, "#dataAnalyserProfile");
			}
		};
		var profileKey = profileConfigKey;
		var ribanName = lookUpTable.get(profileKey);
		if(ribanName){
			$("#ribbonname").html(ribanName);
		}else{
			$("#ribbonname").html("");
		}
		if (localStorage.activeTab && localStorage.activeTab !== "0"
				&& respose.source) {
			var source = Jsonparse(respose.source);
			source = source.filter(function(d) {
				return d.order === localStorage.activeTab;
			});
			profileKey = source[0].profileKey;
			delete localStorage.activeTab;
		}
		var request = {
			"profileKey" : profileKey,
			"viewId" : profileViewId
		};
		callAjaxService("getProfileConfig", callBackGetProfileConfig,
				callBackFailure, request, "POST");
	};
	
	var init = function() {
		$("#content").show();
		$("#otherPagesHeader").hide();
		$("#previewOthers").hide();	
		var urlParam = location.hash.replace(/^#/, '').split("?");
		var viewId = 0;
		localStorage["from"] = "";
		localStorage["viewSetting"] = "";
		localStorage["viewFilter"] = "";

		if (urlParam.length > 1) {
			var queryParam = urlParam[1].split("$&$");
			$.each(queryParam, function() {
				var queryParamKeyValue = this.split("$=$");
				if (!queryParamKeyValue || queryParamKeyValue.length < 2)
					return;
				if (queryParamKeyValue[0] === "viewId") {
					viewId = queryParamKeyValue[1];
				} else if (queryParamKeyValue[0] === "viewSetting") {
					localStorage["viewSetting"] = queryParamKeyValue[1];
				} else if (queryParamKeyValue[0] === "viewFilter") {
					localStorage["viewFilter"] = queryParamKeyValue[1];
				} else if (queryParamKeyValue[0] === "from") {
					localStorage["from"] = queryParamKeyValue[1];
				}
			});
		}
		var request = {
			"action" : urlParam[0]
		};
		enableLoading();
		callAjaxService("getAction", function(response) {
			callBackActionHandler(response, urlParam[0], viewId);
		}, callBackFailure, request, "POST");
	};
	init();
};

var pdfpagination = function() {
	var buttonvalue = $(this).attr("value");
	if (buttonvalue === previous) {
		$('#pdfModal')
				.attr(
						'src',
						'http://localhost:8080/SPECTRA/web/pdfviewer/viewer.html?file=/SPECTRA/web/data/269CV.pdf?#search=cleaning instruments');
	}

};
enableWidget()


	
var loadPageElements = function() {
	if($('#otherPagesHeader').length == 0){
        var str = '';
    str += '<div class="row" id="otherPagesHeader" style="display: none; background-color: #828282; max-height: 40px; height: 40px"></div>';
    str += '<div id="previewOthers" style="display: none; margin-top: 30px;">';
    	str += '<div class="row">';
    		str += '<div class="col-md-12" style="margin-top: -30px;">';
    			str += '<div class="well">';
    				str += '<table class="table table-forum" id="recentlyViewedTable">';
    				str += '<caption id="recentlyViewedTCaption"></caption>';
    					str += '<thead id="recentlyViewedTHead"></thead>';
    					str += '<tbody id="recentlyViewedTBody"></tbody>';
    				str += '</table>';
    			str += '</div>';
    		str += '</div>';
    	str += '</div>';
    str += '</div>';
    $('#main').append(str);
  }
}
var allPageSupportNavbarItems = function(){	
	loadScript("js/SearchConfig/searchConfigurator.js",function(){
		loadScript("js/SearchConfig/searchCallBackHandler.js",function(){ 
			
			var locationTemp = "celgene_doc_search";		// Temporary value for location
			searchActionCallbackHandler.backPageStr = '<div class="col-sm-3 col-md-3" onclick="searchActionCallbackHandler.searchBackToResults()" style="color: white;height:100%"><div class="cursor-pointer" style="margin-right: 35px;margin-top: 10px;float:right"><i class="fa fa-reply"></i><strong style="font-size:19px;">&nbsp;Back</strong></div></div>';
			searchActionCallbackHandler.viewMode = 'external';


			$('#searchConfigBookmark').on('click', function() {
			    $('#bookmarkModal').modal("show");
			});

			$('#saveConfigBookmark').off('click').on('click', function(){
			         enableLoading();
			         var bookMarkName = $.trim($("#configBookMarkName").val());
			         var currentSearchText = $('#searchconfigterm').val();
			         var request = {};
			         request.key = $("#configBookMarkDesc").val();
			         request.displayQuery = $("#configBookMarkDesc").val();
			         request.bookmarkDesc = (!currentSearchText || currentSearchText === '') ? '*' : currentSearchText;
			         request.bookmarkname = bookMarkName;
			         request.type = "Bookmark";
			         request.moduleName = locationTemp;		// window.location.hash.replace('#', '')
			         enableLoading();
			        $("#saveConfigBookmark").addClass("disabled");
 
			         callAjaxService("markSearch", function(response){
			             
			         $($("#searchConfigBookmark").parent()).removeClass("open");
			         $("#saveConfigBookmark").removeClass("disabled");
			         $("#configBookMarkName").val('');
			         $("#configBookMarkDesc").val('');
			         disableLoading();
			         if (response && response.isException) {
			             showNotification("error", response.customMessage);
			             return;
			         } else {
			        	 if($("#saveConfigBookmark").attr("data-app") == 'saveSearchStrategy') {
			        		 showNotification("success", "Save Search Strategy Added Successfully");
			        	 } else {
			        		 showNotification("success", "Save Search Strategy Added Successfully");
			        	 }
			         }
			    
			         },
			             callBackFailure, request, "POST");
			});

			$("#navagate_bookmarks").off('click').on("click", function() {
			    // $('.smart-style-3').scrollTop(0);
			    enableLoading();

			    $('#previewOthers').show();
			    $('#recentlyViewedTCaption').empty();
			    $('#recentlyViewedTBody').empty();
			    $('#recentlyViewedTHead').empty();
			    $('#otherPagesHeader').html('<div class="col-sm-9 col-md-9 admin-link-list">Saved Search Strategy</div>'+ searchActionCallbackHandler.backPageStr);
			    $('#otherPagesHeader').show();
			    $('#content').hide();

			    var location = localStorage.getItem("searchLocation");
			    location = location.replace('#', '');
			    var request = {
			        "type": "Bookmark",
			        "moduleName": locationTemp,
			        "orderBy": "logdate",
			        "orderStyle": "desc"
			    };
			    
			    callAjaxService("getSearchLogs", function(data) {
			         if (data && data.isException) {
			            showNotification("error", data.customMessage);
			            return;
			        }
			        if (data !== undefined && data !== null && data.length > 0) {

			            $('#recentlyViewedTCaption').html('<p style="text-align: left;color: #828282;padding: 10px;font-size: 14px;">Save your search strategy to run it again at a later time.</p>');
			            var header = '<tr>'
			                +'<th><strong>Name</strong></th>'
			        			+'<th><strong>Description</strong></th>'
			        			+'<th class=""><strong>Parameters</strong></th>'
			        			+'<th><strong>Date Created <i class="fa fa-sort cursor-pointer sortByDate" data-sort="asc" aria-hidden="true"></i></strong></th>'
			        			+'<th><div class="pull-right">'
			        				+'<button class="btn btn-default btn-xs admin-btn deleteAllBookmark">Delete All</button>'
			        			+'</div></th>'
			        		+'</tr>';
			            $('#recentlyViewedTHead').html(header);
			            var str = '';
			            for (var i = 0; i < data.length; i++) {
			                str += '<tr class="cursor-pointer">';
			                str += '<td style="width: 20%"><a class="cursor-pointer rlDataNav" data-obj="' + data[i].jsonQuery + '">' + data[i].bookmarkName + '</a></td>';
			                str += '<td style="width: 30%"><a class="cursor-pointer rlDataNav" data-obj="' + data[i].jsonQuery + '">' + data[i].displayQuery + '</a></td>';
			                str += '<td style="width: 20%"><a class="cursor-pointer rlDataNav" data-obj="' + data[i].jsonQuery + '">' + data[i].jsonQuery + '</a></td>';
			                str += '<td style="width: 20%" class="date"><a class="cursor-pointer rlDataNav" data-obj="' + data[i].jsonQuery + '">'+ data[i].logDate + '</a></td>';
			                str += '<td style="width: 10%"><span class="pull-right search-action-icon"><span class="fa fa-lg fa-fw fa-trash deleteBookmark margin-3 txt-color-darken" data-toggle="tooltip" data-placement="bottom" title="Delete" data-id="'+data[i].id+'"></span></span></td>';
			                str += '</tr>';
			            }
			            $('#recentlyViewedTBody').html(str);
			            
			            $('.deleteAllBookmark').on('click', function(){
			                var ids = [];
			                $('.deleteBookmark').each(function(){
			                    var id = $(this).attr('data-id');
			                    if(id) {
			                        ids.push(id);
			                    }
			                });
			                if(ids.length > 0){
			                    var query = 'delete from search_log where id in('+ids.join(',')+')';
			                    var updateRequest = {
			    					"source" : 'postgreSQL',
			    					"dataBase" : 'E2EMFPostGres',
			    					"queries" : btoa(query)
			    				}
			    				callAjaxService("integraGenericUpdate", function(response) {
			    					if (response && response.isException) {
			    						showNotification('error',
			    								response.customMessage);
			    						return;
			    					}
			    					showNotification('success', 'Saved Search Strategy Removed Successfully');
			                        $('#recentlyViewedTCaption').empty();
			    					$('#recentlyViewedTHead').empty();
			    					$("#recentlyViewedTBody").html('<tr><td class="non-action-class" style="border-top-style: none !important">No Saved Search Strategy Available.</td></tr>');
			    				}, callBackFailure, updateRequest, "POST", "json", true);
			                }
			            });
			            $('.deleteBookmark').on('click', function(){
			                var id = $(this).attr('data-id');
			                if(!id) {
			                    return;
			                }
			                var trElement = $(this).closest('tr');
			                if(id) {
			        		var request = {id: id};
			        		enableLoading();
			        		callAjaxService("deleteSearchLog", function(response) {
			    			    disableLoading();
			    			    if (response && response.isException) {
			        		            showNotification("error", response.customMessage);
			        		            return;
			    		        }
			    			    if(response) {
			    				    showNotification("success", "Saved Search Strategy Removed Successfully");
			    				    if(data.length === 1) {
			                            $('#recentlyViewedTCaption').empty();
			    					    $('#recentlyViewedTHead').empty();
			        					$("#recentlyViewedTBody").html('<tr><td class="non-action-class" style="border-top-style: none !important">No Saved Search Strategy Available.</td></tr>');
			    				    }
			    				    $(trElement).remove();
			    			    }
			        		  }, callBackFailure, request, "POST");
			            	}
			            });
//			            $('.rlDataNav').off('click').on('click', function() {
//			                var str = $(this).attr('data-obj') || '';
//			                var obj = str;
//			                $('#previewOthers').hide();
//			                $('#otherPagesHeader').hide();
//			                $('#recentlyViewedTCaption').empty();
//			                $('#recentlyViewedTBody').empty();
//			                $('#recentlyViewedTHead').empty();
//			                $('#previewSearchConfig').show();
//
//			                $('#searchconfigterm').val(str);
//			                searchConfig.overRideSearch('searchAll', str);
//			            });
			            
			            $('.sortByDate').on('click', function() {
			                $(this).attr('data-sort') == "asc"? $(this).attr('data-sort', "dec"): $(this).attr('data-sort', "asc");
			                var sort = $(this).attr('data-sort');
			                searchActionCallbackHandler.sortingBasedOnDate($('#recentlyViewedTBody'), "tr", "td.date", sort);   
			            });
			       
			        } else {
			             //showNotification("error", 'No Save Search Strategy Available');
			             $("#recentlyViewedTBody").html('<tr><td class="non-action-class" style="border-top-style: none !important">No Saved Search Strategy Available.</td></tr>');
			        }
			        try {
			            $("[data-toggle='tooltip']").tooltip();
			        } catch (e) {
			            
			        }
			        
			    }, callBackFailure, request, "POST");
			});

			$("#shortlist").off('click').on('click', function() { 
			    
			    enableLoading();
			    $('#previewOthers').show();
			    $('#recentlyViewedTCaption').empty();
			    $('#recentlyViewedTBody').empty();
			    $('#recentlyViewedTHead').empty();
			    $('#otherPagesHeader').html('<div class="admin-link-list col-sm-9">My Favorites</div>' + searchActionCallbackHandler.backPageStr);
			    $('#otherPagesHeader').show();
			    $('#content').hide();

			    var location = localStorage.getItem("searchLocation");
			    location = location.replace('#', '');


			    callAjaxService("searchGetShortLists", function(data) {
			        if (data && data.isException) {
			            showNotification("error", data.customMessage);
			            return;
			        }
			        if (data !== undefined && data !== null && data.length > 0) {
			            var header = '<tr>'
			                    +'<th style="min-width: 40%"><strong>Type</strong></th>'
			                    +'<th style="min-width: 40%"><strong>Title</strong></th>'
			    				+'<th style="min-width: 20%">'
			    					+'<div class="pull-right">'
			    					    +'<button class="btn btn-default btn-xs admin-btn marginRight10 selectAllFavourites">Select All</button>'
			    				// 		+'<button class="btn btn-default btn-xs admin-btn marginRight10 downloadAllFavourites">Download All</button>'
			    						+'<button class="btn btn-default btn-xs admin-btn marginRight10 deleteAllFavourites">Delete All</button>'
			    					+'</div>'
			    				+'</th>'
			    			+'</tr>';
			            $('#recentlyViewedTHead').html(header);
			            var str = '';
			            var link = window.location.origin + directoryName + '/';
			            for (var i = 0; i < data.length; i++) {
			                try{
			                    var obj = JSON.parse(JSON.stringify(decodeURIComponent(escape(window.atob(data[i].refid))))); //;
			                    obj = JSON.parse(obj);
			                    for(var _ob in obj){
			                        obj[_ob] = obj[_ob]? obj[_ob]: '';
			                    }
			                    
			                    str += '<tr id="myFavourateHR'+i+'">';
			                    str += '<td style="width: 40%; text-transform:capitalize;"><input class="rfPnlChkBox myFavouriteCheckbox" type="checkbox" id="myFavourateCheckbox'+i+'" data-obj="'+data[i].refid+'" data-id="'+data[i].id+'" data-oripath="'+lookUpTable.get('evidenceURL')+''+ obj.oripath +'" data-key="'+ obj.key +'"/> '+obj.type+'</td>';
			                    if(obj.type === 'compound'){
			                        str += '<td class="t" style="width: 40%;text-transform: none;"><a style="height: 0px; padding-top: 12px;" onclick="searchActionCallbackHandler.compundToFilter(\''+obj.oripath+'\');"  target="_blank" data-key="'+obj.oripath+'" data-obj="' +  obj.oripath + '">' + obj.title + '</a></td>';
			                    }
			                    if(obj.type === 'crd'){
			                        str += '<td class="t" style="width: 40%;text-transform: none;"><a style="height: 0px; padding-top: 12px;" onclick="searchActionCallbackHandler.protocolToFilter(\''+obj.oripath+'\');"  target="_blank" data-key="'+obj.oripath+'" data-obj="' +  obj.oripath + '">' + obj.title + '</a></td>';

			                    }
			                    if(obj.type === 'file'){
			                        str += '<td class="t" style="width: 40%;text-transform: none;"><label style="height: 0px; padding-top: 12px;" for="myFavourateCheckbox'+i+'"></label><a class="cursor-pointer" href="'+lookUpTable.get('celgenePPTURL')+'/pdfs/'+ obj.oripath +'" target="_blank" data-key="'+obj.oripath+'" data-obj="' +  obj.oripath + '">' + obj.title + '</a></td>';
			                     }
			                    str += '<td style="width: 20%">';
			                    str += '<span class=" shortListItem search-action-icon" data-solrid="'+ data[i].refid +'">';
			                    str += '<span class="fa fa-lg fa-fw fa-trash margin-3 favsBtn" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick="searchActionCallbackHandler.favActivityIconsAction(\'fav\', \'myFavourateHR'+i+'\')"></span></span>';
			                    if(obj.type == 'file'){
			                        str += '<a class=" shortListItem search-action-icon downloadFavoutites" download target ="_blank" href="'+lookUpTable.get('celgenePPTURL')+'/pdfs/'+ obj.oripath +'" data-solrid="'+ data[i].refid +'">';
			                        str += '<span class="fa fa-lg fa-fw fa-download margin-3 downloadsBtn" data-toggle="tooltip" data-placement="bottom" title="Download" onclick=""></span></a>';
			                    }
			                    /*str += '<span class="pull-right shortListItem search-action-icon" data-solrid="'+ data[i].refid +'">';
			                    str += '<span class="fa fa-lg fa-fw fa-star-o margin-3 favsBtn" title="Favorite" onclick="searchActionCallbackHandler.favActivityIconsAction(\'fav\', \'myFavourateHR'+i+'\')"></span></span>';*/
			                    str += '</td>';
			                    str += '</tr>';
			                } catch(e) {
			                    //$('#recentlyViewedTHead').empty();
			                    //str = '<tr><td class="non-action-class" style="border-top-style: none !important">No My Favourates Available.</td></tr>';
			                }
			            }
			            $('#recentlyViewedTBody').html(str);

			            $('.deleteAllFavourites').on('click', function() {
			                var ids = [], trElements = [];
			                $('.myFavouriteCheckbox').each(function() {
			                    var id = $(this).attr('data-id');
			                    if($(this).prop('checked')==true && id) {
			                        ids.push(id);
			                        trElements.push($(this).closest('tr'));
			                    }
			                });
			                if(ids.length > 0){
			                    var query = 'delete from sf_shortlist where "Id" in('+ids.join(',')+')';
			                    var updateRequest = {
			    					"source" : 'postgreSQL',
			    					"dataBase" : 'E2EMFPostGres',
			    					"queries" : btoa(query)
			    				}
			    				callAjaxService("integraGenericUpdate", function(response) {
			    					if (response && response.isException) {
			    						showNotification('error',
			    								response.customMessage);
			    						return;
			    					}
			    					showNotification('success', 'My Favorites Removed Successfully');
			    					trElements.forEach(function(element) {
			                             $(element).remove();
			                        });
			                        searchConfig.clearAllShortlists();
			    				}, callBackFailure, updateRequest, "POST", "json", true);
			                } else {
			                    showNotification('error', 'Please select at least one document');
			                }
			            });
			            $('.downloadAllFavourites').on('click', function() {
			                searchActionCallbackHandler.downloadURLs = [];
			                $('.myFavouriteCheckbox').each(function() {
			                    var url = $(this).attr('data-oripath');
			                    if($(this).prop('checked')==true && url) {
			                        searchActionCallbackHandler.downloadURLs.push(url);
			                    }
			                });
			                if (searchActionCallbackHandler.downloadURLs.length > 0) {
			                    searchActionCallbackHandler.evidencePresentationLogic.downloadURL();
			                } else {
			                    showNotification('error', 'Please select at least one document');
			                }
			            });
			            $('.selectAllFavourites').on('click', function() {
			                var parent = $(this);
			                var text = $(this).text();
			                ($(this).text() === 'Select All') ? $(this).text('Unselect All') : $(this).text('Select All');
			                $('.myFavouriteCheckbox').each(function() {
			                    if(text === 'Select All') {
			                        $(this).prop('checked', true);
			                    } else {
			                       $(this).prop('checked', false); 
			                    }
			                });
			            });
//			            $('.rlDataNav').off('click').on('click', function() {
//			                var str = $(this).attr('data-obj') || '';
//			                var key = $(this).attr('data-key');
//			                str = str.replaceAll("<em>","").replaceAll("</em>","");
//			                str = str.split(',');
//			                var fStr = '';
//			                str.map(function(currentValue, index, arr) {
//			                    if (index === 0) {
//			                        fStr += ' key:"' + currentValue + '"';
//			                    } else {
//			                        fStr += ' OR ' + ' key:"' + currentValue + '"';
//			                    }
//			                });
//			                $('#searchconfigterm').val('');
//			                // localStorage.setItem('searchKey', key)
//			                //System.searchKey = key;
//			                var href = window.location.href + '?key=key:"' + key + '"';
//			                var win = window.open(href, '_blank');
//			                win.focus();
//			                //searchConfig.overRideSearch('searchAll', key, '');
//			            });
			        } else {
			            //showNotification("error", 'No My Favourates Available');
			            $("#recentlyViewedTBody").html('<tr><td class="non-action-class" style="border-top-style: none !important">No Favorites Available.</td></tr>');
			        }
			        try {
			            $("[data-toggle='tooltip']").tooltip();
			        } catch (e) {
			            
			        }
			    }, callBackFailure, { "module": locationTemp }, "POST");
			});

			$("#searchHistory").off('click').on("click", function() {
			    
			    enableLoading();

			    $('#previewOthers').show();
			    $('#recentlyViewedTCaption').empty();
			    $('#recentlyViewedTBody').empty();
			    $('#recentlyViewedTHead').empty();
			    $('#otherPagesHeader').html('<div class="admin-link-list col-sm-9">Recently Viewed</div>' + searchActionCallbackHandler.backPageStr);
			    $('#otherPagesHeader').show();
			    $('#content').hide();
			    
			    searchActionCallbackHandler.viewMode = 'external';
			    var location = localStorage.getItem("searchLocation");
			    location = location.replace('#', '');
			    var request = {
			        "type": "History",
			        "moduleName": locationTemp,
			        "orderBy": "logdate",
			        "orderStyle": "desc"
			    };
			    callAjaxService("getSearchLogs", function(data) {
			        disableLoading();
			        if (data && data.isException) {
			            showNotification("error", data.customMessage);
			            return;
			        }
			        if (data !== undefined && data !== null && data.length > 0) {
			            $('#recentlyViewedTCaption').html('<p style="text-align: left;color: #828282;padding: 10px;font-size: 14px;">View your recently opened documents (up to 50)</p>');
			            var header = '<tr>'
			                    +'<th></th>'
			                    +'<th><strong>Type</strong></th>'
			                    +'<th><strong>Title</strong></th>'
			                    +'<th><strong>Viewed On <i class="fa fa-sort cursor-pointer sortByDate" data-sort="asc" aria-hidden="true"></i></strong></th>'
			    				+'<th>'
			    					+'<div class="">'
			    					    +'<button class="btn btn-default btn-xs admin-btn selectAllRecentlyViewed">Select All</button>'
			    					    +'<button class="btn btn-default btn-xs admin-btn downloadAllRecentlyViewed" style="margin-left:2px;">Download</button>'
			    					   // +'<button class="btn btn-default btn-xs admin-btn shareAllRecentlyViewed" style="margin-left:2px;">Share</button>'
			    						+'<button class="btn btn-default btn-xs admin-btn clearAllRecentlyViewed" style="margin-left:2px;">Clear</button>'
			    					+'</div>'
			    				+'</th>'
			    			+'</tr>';
			            $('#recentlyViewedTHead').html(header);
			            var str = '';
			            
			            for (var i = 0; i < data.length; i++) {
			                var obj = JSON.parse(JSON.stringify(decodeURIComponent(escape(window.atob(data[i].jsonQuery))))); //;
			                obj = JSON.parse(obj);

			                str += '<tr class="cursor-pointer">';
			                str += '<td style="width: 3%"><input class="myHistoryCheckbox" type="checkbox" id="myHistoryCheckbox'+i+'" data-obj="'+data[i].jsonQuery+'" data-id="'+data[i].id+'" data-oripath="'+ data[i].jsonQuery +'" data-key="'+ data[i].jsonQuery +'"/><label style="height: 0px; padding-top: 12px;" for="myHistoryCheckbox'+i+'"></label></td>';
			                str += '<td style="width: 26.5%;text-transform: capitalize;"><a class="cursor-pointer" data-obj="' + data[i].jsonQuery + '">'+ obj.type+ '</a></td>';
			                if(obj.type == 'file'){
			                    str += '<td style="width: 26.5%;text-transform: none;"><a class="cursor-pointer" target="_blank" href="'+obj.oripath+'" data-obj="' + data[i].jsonQuery + '">' + data[i].displayQuery + '</a></td>';
			                }
			                if(obj.type == 'compound'){
			                        str += '<td style="width: 26.5%;text-transform: none;"><a class="cursor-pointer" target="_blank" onclick="searchActionCallbackHandler.compundToFilter(\''+obj.oripath+'\');"  data-obj="' + data[i].jsonQuery + '">' + data[i].displayQuery + '</a></td>';
			                }
			                if(obj.type == 'crd'){
			                        str += '<td style="width: 26.5%;text-transform: none;"><a class="cursor-pointer" target="_blank" onclick="searchActionCallbackHandler.protocolToFilter(\''+obj.oripath+'\');"  data-obj="' + data[i].jsonQuery + '">' + data[i].displayQuery + '</a></td>';
			                }
			                str += '<td style="width: 10%" class="date"><a class="cursor-pointer" data-obj="' + data[i].jsonQuery + '">'+ data[i].logDate + '</a></td>';
			                str += '<td style="width: 17%"><span class="search-action-icon">';
			                str += '<span class="fa fa-lg fa-fw fa-trash margin-3 txt-color-darken deleteRecentlyViewed" data-toggle="tooltip" data-placement="bottom" title="Delete" data-id="'+data[i].id+'"></span></span>';
			                if(obj.type == 'file'){
			                    str += '<a class="shortListItem search-action-icon downloadFavoutites" download target ="_blank" href="'+ obj.oripath +'" data-solrid="'+ data[i].refid +'"> <span class="fa fa-lg fa-fw fa-download margin-3 txt-color-darken" data-toggle="tooltip" data-placement="bottom" title="Download"></span> </a>';
			                }
			                str += '</td>';
			                str += '</tr>';
			            }
			            $('#recentlyViewedTBody').html(str);
			            
			            $('.selectAllRecentlyViewed').on('click', function() {
			                var parent = $(this);
			                var text = $(this).text();
			                ($(this).text() === 'Select All') ? $(this).text('Unselect All') : $(this).text('Select All');
			                $('.myHistoryCheckbox').each(function() {
			                    if(text === 'Select All') {
			                        $(this).prop('checked', true);
			                    } else {
			                       $(this).prop('checked', false); 
			                    }
			                });
			            });
			            
			            $('.downloadAllRecentlyViewed').on('click', function() {
			                searchActionCallbackHandler.downloadURLs = [];
			                $('.myHistoryCheckbox').each(function() {
			                    var url = $(this).attr('data-oripath');
			                    if($(this).prop('checked')==true && url) {
			                        searchActionCallbackHandler.downloadURLs.push(url);
			                    }
			                });
			                if (searchActionCallbackHandler.downloadURLs.length > 0) {
			                    searchActionCallbackHandler.evidencePresentationLogic.downloadURL();
			                } else {
			                    showNotification('error', 'Please select at least one document');
			                }
			            });
			            

			            
			            $('.clearAllRecentlyViewed').on('click', function() {
			                
			                var ids = [];
			                $('.myHistoryCheckbox').each(function() {
			                    
			                    var id = $(this).attr('data-id');
			                    if($(this).prop('checked')==true && id) {
			                        //
			                      $($('#'+$(this).attr('id')).parent()).parent().remove();
			                        //
			                        ids.push(id);
			                    }
			                    
			                });
			                if(ids.length > 0){
			                    var query = 'delete from search_log where id in('+ids.join(',')+')';
			                    var updateRequest = {
			    					"source" : 'postgreSQL',
			    					"dataBase" : 'E2EMFPostGres',
			    					"queries" : btoa(query)
			    				}
			    				callAjaxService("integraGenericUpdate", function(response) {
			    				    
			    					if (response && response.isException) {
			    					    
			    						showNotification('error',
			    								response.customMessage);
			    						return;
			    					}
			    					showNotification('success', 'Recently Viewed Removed Successfully');
			    					//
			    					 //if(data.length === 1)
			    					 
			                    if(($('tbody#recentlyViewedTBody').find('tr').length)==0){
							            
			    				        $("#recentlyViewedTHead").empty();
					        	        $("#recentlyViewedTBody").html('<tr><td class="non-action-class" style="border-top-style: none !important">No Recently Viewed Available.</td></tr>');
			        				}

			    				}, callBackFailure, updateRequest, "POST", "json", true);
			                } else {
			                    showNotification('error', 'Please select at least one document');
			                }
			              	  
			            });
			            //---
			            $('.deleteRecentlyViewed').on('click', function(){
			                
			                var id = $(this).attr('data-id');
			                if(!id) {
			                    return;
			                }
			                var trElement = $(this).closest('tr');
			                if(id) {
			        		var request = {id: id};
			        		enableLoading();
			        		callAjaxService("deleteSearchLog", function(response) {
			    			    disableLoading();
			    			    if (response && response.isException) {
			    		            showNotification("error", response.customMessage);
			    		            return;
			    		        }
			    			    if(response) {
			    			        
							        showNotification("success", "Recently Viewed Removed Successfully");
							       // if(($('tbody#recentlyViewedTBody').find('tr').length)==0){
							        if(data.length === 1) {
							            
			    				        $("#recentlyViewedTHead").empty();
					        	        $("#recentlyViewedTBody").html('<tr><td class="non-action-class" style="border-top-style: none !important">No Recently Viewed Available.</td></tr>');
			        				}
			    				    $(trElement).remove();
			        			  }
			        		  }, callBackFailure, request, "POST");
			            	}
			            });
			            
			            $('.rlDataNav').off('click').on('click', function() {
			                var str = $(this).attr('data-obj') || '';
			                str = str.replaceAll("<em>","").replaceAll("</em>","");
			                str = str.split(',');
			                var fStr = '';
			                str.map(function(currentValue, index, arr) {
			                    if (index === 0) {
			                        fStr += ' "' + currentValue + '"';
			                    } else {
			                        fStr += ' OR ' + ' "' + currentValue + '"';
			                    }

			                });
			                
			                var href = window.location.href + '?key=key:"' + str + '"';
			                var win = window.open(href, '_blank');
			                win.focus();
			                
			                // $('#searchconfigterm').val(str);
			                // searchConfig.overRideSearch('searchAll', fStr, '');
			            });
			            
			            $('.sortByDate').on('click', function() {
			                $(this).attr('data-sort') == "asc"? $(this).attr('data-sort', "dec"): $(this).attr('data-sort', "asc");
			                var sort = $(this).attr('data-sort');
			                searchActionCallbackHandler.sortingBasedOnDate($('#recentlyViewedTBody'), "tr", "td.date", sort);   
			            });
			        } else {
			            //showNotification("error", 'No Recently Viewed Available');
			            $("#recentlyViewedTBody").html('<tr><td class="non-action-class" style="border-top-style: none !important">No Recently Viewed Available.</td></tr>');
			        }
			        try {
			            $("[data-toggle='tooltip']").tooltip();
			        } catch (e) {
			            
			        }
			    }, callBackFailure, request, "POST");

			});
			
			searchActionCallbackHandler.insertSearchHistory = function(type, title, datasource) {
	        var request = {};
	        request.key = title;
	        request.displayQuery = datasource;
	        request.bookmarkDesc = btoa(unescape(encodeURIComponent(JSON.stringify({ 
	            'type': type,
	            'title': title,
	            'oripath': datasource}))));
	        request.bookmarkname = "";
	        request.type = "History";
	        request.moduleName = locationTemp;		// window.location.hash.replace('#', '')
	        callAjaxService("markSearch", function(response) {}, callBackFailure, request, "POST",null,true);
	}

	searchActionCallbackHandler.callFavs = function(event, type,  title, datasource, isRemove) {
	    
	    var element = event.target;
	    if(element.nodeName.toLowerCase() === 'span') {
	    	element = element.parentElement;
	    }
	    var shortlists = localStorage.getItem('shortLists');
	    shortlists = JSON.parse(shortlists);
	    shortlists = shortlists.shortlist;
	    var obj= {
	        'type': type,
	        'title' : title,
	        'oripath': datasource
	    };
	    var solrId = btoa(unescape(encodeURIComponent(JSON.stringify(obj))));

	    if(solrId){
	        if(shortlists.indexOf(solrId) > -1){
	            shortlists.splice(shortlists.indexOf(solrId), 1);
	            $(element).find('.margin-3').removeClass('fa-star');
	    		$(element).find('.margin-3').addClass('fa-star-o');
	    		$(element).find('.margin-3').css('cssText', 'color: #828282 !important');
	    		$(element).removeClass('shortlisted');
	        } else {
	            shortlists.push(solrId);
	            $(element).addClass('shortlisted');
	    		$(element).find('.margin-3').removeClass('fa-star-o');
				$(element).find('.margin-3').addClass('fa-star');
				$(element).find('.margin-3').css('cssText', 'color:gold !important');
	        }
	    } else {
	       //Intentionally do nothing...
	    }
	  
	    localStorage.setItem('shortLists', JSON.stringify({shortlist: shortlists}));
	 
	    var shortListTemplate = {
	    		 "solrId": solrId,
	    		 "moduleName": window.location.hash.replace('#','')
	     };
	     
	    enableLoading();
	    callAjaxService('shortListSearch', function(response) {
	    		 if(response && response.isException) {
	    			 showNotification("error", response.customMessage);                 
	    		 }
	    		 disableLoading();
	    }, callBackFailure, shortListTemplate,"POST");
	    
	/*    if(key) {
	        if(isRemove !== 'false') {
	           
	            $('.favourite-' + key).removeClass('fa-star');
	    		$('.favourite-' + key).addClass('fa-star-o');
	    		$('.favourite-' + key).css('cssText', 'color: #828282 !important');
	    		$('.favourite-' + key).parent().removeClass('shortlisted');
	        } else {
	          
	            $('.favourite-' + key).addClass('shortlisted');
	    		$('.favourite-' + key).removeClass('fa-star-o');
				$('.favourite-' + key).addClass('fa-star');
				$('.favourite-' + key).css('cssText', 'color:gold !important');
	        }
	    }*/
	}

	searchActionCallbackHandler.favActivityIconsAction = function(param, elm) {
	    if(param == 'share') {
	        
	    } else if(param == 'download') {
	        
	    } else {
	        var deleteEml = $('#'+elm);
	        searchConfig.alterShortListItem(event, deleteEml);
	    } 
	};

//	searchConfig.setDistributedCollection(false);
	Handlebars.registerHelper('chopCharacters', function(key) {
		if(!key || key.length === 0) {
			return '';
		}
		if(key.length <= 200) {
		    return key;
		}
		return key.substr(0, 200);
	});

	Handlebars.registerHelper('replacePipe', function(key) {
		if(key.indexOf('|')>-1){
		    var str = '';
		    var keys = key.split('|');
		    keys.forEach(function(key){
		        str+='<a class="protocol" onclick="searchActionCallbackHandler.protocolToFilter(&quot;'+key+'&quot;);"  style="cursor: pointer;" >'+key+'</a>';
		        str+=' , ';
		    });
		    return str.substring(0,str.length-3);
		}
		else{
		    return '<a class="protocol" onclick="searchActionCallbackHandler.protocolToFilter(&quot;'+key+'&quot;);"  style="cursor: pointer;" >'+key+'</a>';
		}
	});

	Handlebars.registerHelper('checkFavs', function(obj){
	    
	    if(obj.compund_name){
	        var key= btoa(unescape(encodeURIComponent(JSON.stringify({ 
	            'type': 'compound',
	            'title': obj.compund_name,
	            'oripath':obj.compund_name}))));
	    }else if(obj.protocol_num){
	        var key = btoa(unescape(encodeURIComponent(JSON.stringify({ 
	            'type': 'crd',
	            'title': obj.protocol_num,
	            'oripath':obj.protocol_num}))));
	    } else if(obj.document_name){    
	        var key= btoa(unescape(encodeURIComponent(JSON.stringify({ 
	            'type': 'file',
	            'title': obj.document_name,
	            'oripath':obj.filepath}))));
	    }
	    if(localStorage.getItem('shortLists')) {
	        try{
	            var json = JSON.parse(localStorage.getItem('shortLists'));
	            var arr = json.shortlist || [];
	            if(arr.indexOf(key) > -1  ) {
	                return true;
	            }
	            return false;
	        } catch (ex) {
	            //intentionally do nothing.
	        }
	    }
	});

	searchActionCallbackHandler.protocolToFilter = function (protocol){
	    // localStorage['celgene_doc_search_crd_prt_info']="protocol_num= '"+protocol+"'"; 
	    // localStorage['filtersForwaded'] =  JSON.stringify({'protocol_num':protocol});
	    localStorage['filtersForwaded'] =  JSON.stringify({'protocol_num':JSON.stringify({'id':protocol})});
	    var windowurl = '/SPECTRA/index#crd_prt_info?from$=$celgene_doc_search';
	    window.open(windowurl, '_self');
	}

	searchActionCallbackHandler.compundToFilter = function (compound){
	    // localStorage['celgene_doc_search_CompoundInfo']="compound= '"+compound+"'"; 
	    // localStorage['filtersForwaded'] =  JSON.stringify({'compound':compound});
	   localStorage['filtersForwaded'] =  JSON.stringify({'compound':JSON.stringify({'id':compound})});
	    var windowurl = '/SPECTRA/index#CompoundInfo?from$=$celgene_doc_search';
	    window.open(windowurl, '_self');
	}

	searchActionCallbackHandler.matIdToFilter = function (matId){
	    // localStorage['celgene_doc_search_crd_prt_info']="protocol_num= '"+protocol+"'"; 
	    // localStorage['filtersForwaded'] =  JSON.stringify({'matid':matId});
	    localStorage['filtersForwaded'] =  JSON.stringify({'matid':JSON.stringify({'id':matId})});
	    var windowurl = '/SPECTRA/index#cel_prime_prod_demand?from$=$CompoundInfo';
	    window.open(windowurl, '_self');
	}

	searchActionCallbackHandler.searchBackToResults = function(){
	    $('#previewOthers').hide();
	    $('#otherPagesHeader').hide();
	    $('#content').show();
	};
	
		});
	});
}
