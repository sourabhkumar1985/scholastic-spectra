var testSuite = function() {
	var THIS = {};
	var init = function() {
		getAllProfiles();
	};
	$("#createProfile").unbind('click').bind(
			'click',
			function() {
				THIS.profile = {};
				$("#myModalsave").modal('show');
				$('#frmSaveDataSource').find('label').removeClass('state-success state-error');
				$("[name='displayname']").val("");
				$("[name='emails']").val("");
				$('#configTable').find('tbody').empty();
				addTable();
				//$('#myModalsave').find('.checkbox').show();				
			});
	$("#frmSaveDataSource").submit(function(e) {
		e.preventDefault();
	});
	$("#frmSaveDataSource").validate({
		ignore : [],
		rules : {
			displayname : {
				required : true
			},
			emails : {
				required : true
			}
		},
		messages : {
			displayname : {
				required : 'Please Enter Suite Name'
			},
			emails : {
				required : 'Please Enter Email '
			}
		},
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});

	var getAllProfiles = function(){
		$('#myprofilediv').show();
		$('#allprofilediv').hide();
		var requiredCol = [ {
			"sWidth": '20%',
			"mDataProp" : "displayname",
			"title":"Name",
			"sDefaultContent" : ""
		},  {
			"mDataProp" : "createdby",
			"sWidth": '10%',
			"title":"Created By",
			"sDefaultContent" : ""
		},  {
			"mDataProp" : "lastmodifiedby",
			"sWidth": '10%',
			"title":"Modified By",
			"sDefaultContent" : ""
		},{
			"mDataProp" : "actionid",
			"sWidth" : "5%",
			"title" : "Execute",
			"sDefaultContent" : ""
		},{
			"mDataProp" : "actionid",
			"sWidth" : "5%",
			"title" : "Download",
			"sDefaultContent" : ""
		}];
		
		var executeButton = [ {
			"targets" :  3,
			"width" : '5%',
			"createdCell" : function(td, cellData, rowData) {
				// actionId = rowData.actionid;
				var button = $("<button/>").addClass("btn-outline-success").html('Execute');
				button.attr({
					//'data-target' : '#profilePublishModal',
					//'data-toggle' : 'modal',
					'name' : 'excuteQuery',
					'data-refid' : rowData.id,
					'data-displayname' : rowData.displayname,
					'data-actionid' : rowData.actionId,
					'data-action' : rowData.profilekey
				});
				button.unbind('click').bind('click', function(e) {
					e.preventDefault();
					e.stopPropagation();
					mydataSourceSelect(this);
				});
				$(td).html(button);
			}
		},{
			"targets" :  4,
			"width" : '5%',
			"createdCell" : function(td, cellData, rowData) {
				// actionId = rowData.actionid;
				var button = $("<button/>").addClass("btn-outline-success").html('Download');
				button.attr({
					//'data-target' : '#profilePublishModal',
					//'data-toggle' : 'modal',
					'name' : 'downloadQuery',
					'data-refid' : rowData.id,
					'data-displayname' : rowData.displayname,
					'data-actionid' : rowData.actionId,
					'data-action' : rowData.profilekey
				});
				button.unbind('click').bind('click', function(e) {
					e.preventDefault();
					e.stopPropagation();
					mydataSourceSelect(this);
				});
				$(td).html(button);
			}
		} ];
		
		var profileParam = {
				"getAll" : true,
				"profileType" : "queryExecutor",
		        "columns" : requiredCol,
		        "otherButton" :executeButton 
		};
		enableLoading();
		var profileUtitlyObj = new profileUtiltyClass(profileParam,function(e){mydataSourceSelect(e,this);});
		profileUtitlyObj.getMyProfile();
		
	};
	
	var mydataSourceSelect = function(obj) {
		var id = $(obj).data("refid");
		var request = {
			"id" : id
		};
		enableLoading();
		callAjaxService("getProfileConfig", function(response) {
			callBackGetProfileConfig(response,obj);
		},
				callBackFailure, request, "POST");
	};

	var callBackGetProfileConfig = function(response,obj) {
		disableLoading();
		var data = JSON.parse(response["config"])[0];
		var allIds=[];
		if($(obj).attr('name') === 'downloadQuery'){
			excuteQuery(data,true);
			return;
		}
		if($(obj).attr('name') === 'excuteQuery'){
			excuteQuery(data);
			return;
		}
		$("#myModalsave").modal('show');
		$("#configTable").find("tbody").html("");
		if (data) {
			THIS.profile = {};
			THIS.profile.id = data.id;
			$("[name='displayname']").val(data.displayname);
			if (data.config) {
				var config = JSON.parse(data.config);
				for (var i = 0; i < config.query.length; i++) {
					var ids=config.query[i].id;
					allIds.push(ids);
				}
				$("[name='emails']").val(config.users);
				var req={
						"ids":allIds.join(",")	
					};
					callAjaxService("getProfileConfig", function(response) {
						callBackGetProfile(response,config);
					},callBackFailure, req, "POST");
			}
		}

	};

	var callBackGetProfile = function(response,configData){
		var data = JSON.parse(response.config);
		var tableList = [];
		for (var i=0;i<data.length;i++){
			tableList.push({"id":data[i].id,"name":data[i].displayname,"criticality":configData.query[i].order});
		}
		for (var i = 0; i < data.length; i++) {
			var remarks = data[i].remarks;
			addTable(tableList[i],remarks);
		}
	}
	$("#addTable").unbind('click').bind('click', function() {
		addTable();
	});

	$("#profileBack").unbind("click").bind("click", function() {
		$("#testSuite").hide(300);
		$("#selectSource").show(300);
	});
	var addTable = function(data,desc) {
		var removeButton = $("<a/>").addClass(
				"button-icon pull-right cursor-pointer").append(
				$("<i/>").addClass("fa fa-fw fa-lg fa-remove"));
		removeButton.unbind('click').bind('click', function() {
			$(this).parents("tr").remove();
		});
		var selectOption = $("<input/>").css("width", "100%").attr({
			"name" : "queryName",
			"placeholder" : "Select Query"
		});
		var id = $('[name="criticality"]').length? $('[name="criticality"]').length:0;
		var options = $("<input/>").css("width", "100%").attr({
			"name" : "criticality",
			"placeholder" : "Select option",
			"id" :"order"+id
		});
		var description='<td><div name="selectedOpt">'+desc+'</div></td>';
		var tableRow = $("<tr/>").append(
				$("<td/>").css("width","25%").append(
						$("<label/>").addClass("input width-100per").append(
								$("<div/>").addClass("col-md-11").append(
										selectOption)))).append(description).append(
												$("<td/>").css("width","25%").append(
														$("<label/>").addClass("input width-100per").append(
																$("<div/>").addClass("col-md-11").append(
																		options)))).append($("<td/>").css("width","2%").append(
												$("<div/>").append(removeButton)));
		$("#configTable").find("tbody").append(tableRow);
		constructSelect2(
				selectOption,
				"select id,displayname,remarks from profile_analyser where type in ('savedQuery') and lower(displayname) like '%<Q>%'",
				false, data ? data : null, "name",function(e){onChangeOfOpt(e)});
		$('[name="criticality"]').select2({
			placeholder : "Select Source",
			data : criticalityData
		})
		$('#order'+id).select2("val", data ? data.criticality : "" );
   };
	
	var criticalityData = [ {
		"id" : "low",
		"text" : "Low"
	}, {
		"id" : "medium",
		"text" : "Medium"
	}, {
		"id" : "high",
		"text" : "High"
	} ];
	
	
	function onChangeOfOpt(e){
		if(e.added){
			$(e.target).parents('tr').find('[name="selectedOpt"]').text(e.added.count);
			return;
		}
		
		$(e.target).parents('tr').find('[name="selectedOpt"]').text('');
	};

	$('#saveTest').unbind('click').bind('click', function() {
		saveProfile();
		$("#myModalsave").modal('hide');
	});

	var saveProfile = function() {
		var saveDataProfile = function() { 
			var profileIds = [];
			var id = 0;
			for (var i = 0; i < $("[name='queryName']").length; i++) {
				var queryId = $("[name='queryName']").eq(i).select2('data');
				var priority = $("[name='criticality']").eq(i).select2('data')?$("[name='criticality']").eq(i).select2('data').id:"low";
				queryId.order=priority;
				profileIds.push(queryId);
			}
			var config = {
				query : profileIds,
				users : $("[name='emails']").val()
			};
			if (THIS.profile) {
				id = THIS.profile.id;
			}
			var request = {
				"config" : JSON.stringify(config),
				"displayname" : $("[name='displayname']").val(),
				"type" : "queryExecutor",
				"id" : id

			};
			enableLoading();
			callAjaxService("saveProfile", callBackSuccessHandler,
					callBackFailure, request, "POST");
		};
		if (THIS.profile && THIS.profile.id) {
			saveDataProfile();
		} else {
			if (!$("#frmSaveDataSource").valid())
				return;
			saveDataProfile();
		}
	};
	var callBackSuccessHandler = function() {
		disableLoading();
		showNotification("success", "Query saved successfully..");
		getAllProfiles();
	};

   var excuteQuery = function(data,flag) {
	if(data && data !=null && data!=undefined){
		var profileName = data.displayname;
		var configData = Jsonparse(data.config);
		var profileIds = [];
		var criticality =[];
		var obj = {};
		
		for (var i = 0; i < configData.query.length; i++) {
			var queryId = configData.query[i].id;
			criticality.push(configData.query[i].order != undefined ? configData.query[i].order :"Low");
			profileIds.push(queryId);
		}
		var request = {
			"profileIds" : profileIds.join(","),
			"users" : configData.users,
			"profileName" : profileName,
			"criticality": criticality.join(','),
            "sendMail" : true
		};

		enableLoading();
		if(flag){
			disableLoading();
			downloadURL = 'rest/dataAnalyser/downloadQueryResult';
			$.fileDownload(downloadURL, {
				successCallback: function() {
					showNotification("success", profileName+" is downloaded sucessfully ");
	            },
	            failCallback: function(responseHtml) {
	            	showNotification("error", profileName+" is not downloaded sucessfully ");
	                $preparingFileModal.dialog('close');
	                $("#error-modal").html($(responseHtml).text());
	                $("#error-modal").dialog({ modal: true });
	            },
		        httpMethod: "POST",
		        data: {"profileIds":request.profileIds, "users":request.users, "profileName":request.profileName, "criticality":request.criticality, "isEncoded" : "true"}
		    });	
		}else{
			callAjaxService("executeTestSuite", callBackGetQueryExecutor, function(
					erer) {
			}, request, "POST");
		}
		
	  }
	};

	var callBackGetQueryExecutor = function(response) {
		disableLoading();
		if (!response) {
			return;
		}
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}else if(response !="true"){
			showNotification("error", "There is some error while sending mail ");
		}else{

			showNotification("success", "Mail has been sent sucessfully");
		}
	};

	init();
};
var testSuite = new testSuite();