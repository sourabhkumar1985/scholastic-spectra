var dataTableDrillDownObj = function() {
	var currentFocusedObject;
	var isEndOfWorkFlow;
	var disableWorkFlowAfterComplete;
	this.addCrdNotes=function(obj){
		actionCallbackHandler.addCrdNotes(obj);
	}
	this.drillDownSO = function(obj) {
		$("#myModalDrilldown").modal("show");
		var request = JSON.parse($(obj).parent("tr").data("request"));
		thirdLevelDrillDown('po', '', request.ebeln, request.source);
	};
	this.showDetails = function(obj) {
		if ($(obj).find(":checkbox").length > 0) {
			$(obj).find(":checkbox").prop('checked',
					!$(obj).find(":checkbox").is(":checked"));
		} else if (!$(event.target).parent().hasClass("checkbox")) {
			var request = JSON.parse($(obj).parent("tr").data("request"));
			loadScript("js/DataAnalyser/calendarDrilldown.js", function() {
				calendarDrillDown.showDetails("eventdetails", {
					"eventid" : request.id
				});
			}, "#dataAnalyserProfile");
		}
	};
	this.showSmartFindModal = function(obj) {
		var request = JSON.parse($(obj).parent("tr").data("request"));
		var oldCategory = '', newCategory = '', message = 'Old Category', message1 = 'New Category';
		if (request.cs_approved || request.cat_approved) {
			if (request.cs_approved) {
				message = 'Old CSpecialty';
				message1 = 'New CSpecialty';
				newCategory = request.cs_approved;
			}
			if (request.cat_approved) {
				newCategory = request.cat_approved;
			}
		}

		if (request.source || request.cspeciality) {
			if (request.source) {
				oldCategory = request.source;
			}
			if (request.cspeciality) {
				message = 'Old CSpecialty';
				message1 = 'New CSpecialty';
				oldCategory = request.cspeciality;
			}
		}

		var queryItems = [ {
			"key" : request.key,
			"type" : "exact",
			"field" : "id"
		} ];
		var req = {
			queryItems : queryItems
		};
		req.key = JSON.stringify(req);
		enableLoading();
		callAjaxService(
				"catalogSearchPost",
				function(response) {
					if (response) {
						disableLoading();
						if (!!response.isException) {
							showNotification("error", response.customMessage);
							return;
						}
						if (response.search && response.search.result
								&& response.search.result.length > 0) {
							response = response.search.result[0];
							// var category =
							// newCategory;//response.categoryl0.split(';');
							// var subCategory = oldCategory;//(category[1])?
							// category[1] : '';
							// category = category[0];
							$(".zoomContainer").addClass("hide");
							$("#frmSankeyFilter").empty();
							$("#tableConfigTab").hide();
							$("#myModalDrilldown #myModalLabel")
									.html(response.original)
									.append($("<br/>"))
									.append(
											$("<small/>")
													.html("Company :<b> "
																	+ response.manufacturer
																	+ "<b/>"))
									.append(
											$("<small/>")
													.addClass('margin-10')
													.html(
															"Doc num :<b style='display:inline-block'> "
																	+ response.docnum
																	+ "<b/>"))
									.append(
											$("<small/>")
													.addClass('margin-10')
													.html(
															"Material/Components:<b> "
																	+ response.components
																	+ "<b/>"))
									.append(
											$("<small/>")
													.addClass('margin-10')
													.html(
															message
																	+ ":<b> "
																	+ oldCategory
																	+ "<b/>"))
									.append(
											$("<small/>")
													.addClass('margin-10')
													.html(
															message1
																	+ ":<b> "
																	+ newCategory
																	+ "<b/>"));
							var images = response.jpgimage.split(','), imageDiv, sfJpgURL = lookUpTable
									.get("sfJpgURL"), sfPngURL = lookUpTable
									.get("sfPngURL");
							var bigImages = (response.pngimage) ? response.pngimage
									.split(',')
									: [ "" ];
							var imagepath = sfPngURL + response.imagepath
									+ bigImages[0];
							if (images.length > 0) {
								imageDiv = $('<div/>');
								for (var imgIndex = 0; imgIndex < bigImages.length; imgIndex++) {
									var imageUrl = sfPngURL
											+ response.imagepath
											+ bigImages[imgIndex];
									// console.log(imageUrl);
									// var imageUrl =
									// sfJpgURL+response.smallimagepath+images[imgIndex];
									// imageDiv.append($('<div/>').addClass('padding-2').append($('<small/>').css({'margin':'5px
									// 10px','font-weight':'bold'}).html(imgIndex+1))
									// .append($('<img/>').addClass('img-thumbnail').css({'width':'100px','background-color':response.currentImageIndex
									// == imgIndex?'#3276b1':''})
									// .attr({'src':imageUrl}).unbind('click').bind('click',function(){
									// var imageIndex =
									// parseInt($(this).parent().find('small').html())-1;
									// $("#poDocView
									// img").css('background-color','');
									// $(this).css('background-color','#3276b1');
									// $("#poDocView
									// img:last").attr({'src':sfPngURL+response.imagepath+bigImages[imageIndex]});
									// setTimeout(function() {$("#poDocView
									// img:last").elevateZoom({ zoomType :
									// "lens",minZoomLevel:0.5, lensShape :
									// "round", lensSize : 300,scrollZoom :
									// true,customClass:"detailImg" });}, 500);
									// })));
								}
							}
						}
						$("#poDocView")
								.css({
									"height" : $(window).height() - 150 + "px"
								})
								.empty()
								.append(
										$("<div/>")
												.addClass("row")
												.append(
														$("<div/>")
																.addClass(
																		"col-sm-2")
																.append(
																		imageDiv ? imageDiv
																				: ''))
												.append(
														$("<div/>")
																.addClass(
																		"col-sm-9")
																.append(
																		$(
																				"<div/>")
																				.addClass(
																						"text-center padding-10")
																				.append(
																						$(
																								"<img/>")
																								.attr(
																										{
																											"src" : imagepath,
																											"alt" : title,
																											"height" : $(
																													window)
																													.height()
																													- 150
																													+ "px"
																										})))));
						$('.detailImg').remove();
						$('#poDocView').find('.col-sm-2').css({
							'max-height' : $('#poDocView').height(),
							"max-width" : "200px",
							"overflow" : "auto"
						});
						setTimeout(function() {
							$("#poDocView img:last").elevateZoom({
								zoomType : "lens",
								minZoomLevel : 0.5,
								lensShape : "round",
								lensSize : 300,
								scrollZoom : true,
								customClass : "detailImg"
							});
						}, 500);
						// getReleatedDocments(title);
					}
					$("#myModalDrilldown").modal("show");
				}, callBackFailure, req, "POST");
	};
	this.urlExists = function(obj){
		event.preventDefault();
		var url = $(obj).attr("url");
        $.ajax(
        		{
        			url: url,
        			type:"GET", 
        			async: false,
        		    dataType: "json",
        			success: function(data, textStatus) {
        				//console.log(textStatus);
        			},
        			complete:function(jqXHR, textStatus, errorThrown){
        				if(jqXHR.status == 404 || jqXHR.status == "404"){
        					showNotification("error", "File does not exist.");
        				}else{
        					var win = window.open(url, '_blank');
              		      	win.focus();
        				}
        			},
        			error: function(jqXHR, textStatus, errorThrown) {
        		    	  console.log(textStatus);
        		    }
            
	      });
	};	
	this.editConsolidatedMatch = function(obj,event){
		event.preventDefault();
		event.stopPropagation();
		$("#myModalDrilldown").modal("show");
		$("#poDocView").parents(".modal-dialog").removeClass("width-500");
		var rowData = Jsonparse($(obj).parents("tr").data("request"));
		//var columnSettings = Jsonparse($(obj).parent("tr").data("columnSettings"));
		$("#poDocView").empty();
		var override ="";
		if(rowData.user_status === "A" && System.roles.indexOf("RA_SuperUser")>-1){
			override = $("<a/>").addClass("btn marginRight10 pull-right btn-warning col-sm-1").bind("click",function(){
				$("#updateraattribute").find("input").removeAttr("readonly");
				$("#poDocView").find("a").show();
			}).html("Override");
		}
		var headerInfo = $("<div/>").addClass("col-sm-8")
		.append($("<strong/>").html("RA Information Details")).append("</br>")
		.append($("<strong/>").html("Item# :")).append(rowData.item_no).append("</br>")
		.append($("<strong/>").html("Product Name :")).append((rowData.product_name && rowData.product_name!== "null")?rowData.product_name:"").append("</br>")
		.append($("<strong/>").html("Item Status :")).append((rowData.item_status && rowData.item_status !== "null")?rowData.item_status:"");
		$("#printView #myModalLabel").addClass("row").html(headerInfo).append(override);
		var request = {
				source:"postgreSQL",
				database:"QFIND_RA",
				table:'l1_source_consolidation',
				columns:"approved,fda_product_code,fda_class,submission_type,fda_submission_number,submission_date,fda_medical_device_listing_number,cat_no_on_label," +
						"item_status,item_type,site_rollup,division,submission_status,product_name,product_name_on_the_label,legal_manufacturer,gmdn_codes," +
						"device_exempt_from_premarket_submission_requirements,fda_premarket_submission_numbers,item_in_gudid,model_version,cat_no," +
						"labeler_duns_in_gudid,fda_submission_supplement_number,fda_medical_device_listing_number_dl,fda_product_codes,approval_date," +
						"dl_in_furls_based_on_submission_in_gudid,product_code_in_furls_based_on_submission_in_gudid,submission__exception_in_furls_based_on_dl_in_gudid," +
						"product_code_in_furls_based_on_dl_in_gudid_,ils_fda_21,ils_dev_6,dl_in_furls_based_on_submission_in_oracle,product_code_in_furls_based_on_submission_in_oracle," +
						"submission_exception_in_furls_based_on_dl_in_oracle,fda_regulatory_status,k510_dt_if_used_to_bring_sku_to_market,fda_submission_supplement_numbers," +
						"is_item_in_oracle,is_item_in_agile,distribution_status,gmdn_codes1,submission_exception_in_furls_based_on_dl_in_oracle1," +
						"product_code_in_furls_based_on_dl_in_oracle,fda_class1,fda_product_code1,ils_est_reg_22,dl_in_furls_based_on_submission_in_oracle_,ils_510k_20," +
						"make_buy,intg_mfg_11,design_specification_owner,intg_site_12,vend_est_reg_25,vend_dev_23,gmdn_28",
				//columns:"",
				filters:" item_no in ('"+rowData.item_no+"')",
				//filters:" oracle.itemno in ('"+rowData.item_no+"') and gudid.version_model_number in ('"+rowData.item_no+"') and agile.item_number in ('"+rowData.item_no+"') "
		};
		enableLoading();
		callAjaxService("getData", function(response){
			disableLoading();
			if (response && response.isException) {
				showNotification("error", response.customMessage);
				return;
			}
			if(response && response.data && response.data.length>0){
				var data = response.data[0];
				if(Object.keys(data).length>0){
					//var content = $("<div/>").addClass("row smart-form");
					var table = $("<table/>").addClass("table table-bordered table-striped");
					table.append($("<tr/>")
							.append($("<th/>").css({"width":"35%"}).html("Attribute Name"))
							.append($("<th/>").css({"width":"65%"}).html("Attribute Value")));
					var editableFields = ['site_rollup','division','approved','fda_submission_supplement_number','submission_status','submission_date','gmdn_codes','approval_date','cat_no_on_label','item_status','item_type','product_name','product_name_on_the_label','legal_manufacturer','k510_dt_if_used_to_bring_sku_to_market'];
					//var editableFields = ['site_rollup','division','fda_product_code','fda_class','submission_type','fda_submission_number','fda_medical_device_listing_number'];
					var mapLabelObj = [
					                   {"key":"is_item_in_oracle","label":"Is item in Oracle?","info":"Data source is Oracle"},							
					                   {"key":"is_item_in_agile","label":"Is item in Agile?","info":"Data source is Agile"},
					                   {"key":"product_name","label":"Product Name","info":"Product name rollup based on following heirarchy: Oracle, Agile, or other system/source"},							
					                   {"key":"product_name_on_the_label","label":"Product name on the label","info":"Based on Kallik, Prisym, Bartender, Site Label Template Spreadsheets and other available labels. Note: label data is not being automically refreshed."},							
					                   {"key":"item_status","label":"Item Status","info":"Item status rollup based on following heirarchy: Oracle, Agile, or other system/source"},							
										{"key":"item_type","label":"Item Type","info":"Item type rollup based on following heirarchy: Oracle, Agile, or other system/source"},
										{"key":"cat_no_on_label","label":"Cat # on Label","info":"Based on Kallik, Prisym, Bartender, Site Label Template Spreadsheets and other available labels. Note: label data is not being automically refreshed."},
										{"key":"division","label":"Division","info":"Division rollup based on GUDID labeler and a combination of other sources if item is not in GUDID, including label information and Agile / Oracle site info"},
										{"key":"site_rollup","label":"SITE ROLLUP","info":"Site rollup based on GUDID labeler and a combination of other sources if item is not in GUDID, including label information and Agile / Oracle site info"},
										{"key":"legal_manufacturer","label":"Legal manufacturer name on the label","info":"Based on Kallik, Prisym, Bartender, Site Label Template Spreadsheets and other available labels. Note: label data is not being automically refreshed."},
										{"key":"approved","label":"Approved/ Cleared?","info":"Based on system calculation if compared RA data matches FURLS or based on values entered by RA SME during verification in case of a data discrepancy or gap"},
										{"key":"fda_product_code","label":"FDA Product Code","info":"Based on system calculation if compared RA data matches FURLS or based on values entered by RA SME during verification in case of a data discrepancy or gap"},
										{"key":"fda_class","label":"FDA Class","info":"Based on system calculation if compared RA data matches FURLS or based on values entered by RA SME during verification in case of a data discrepancy or gap"},
										{"key":"submission_type","label":"Submission Type","info":"Based on system calculation if compared RA data matches FURLS or based on values entered by RA SME during verification in case of a data discrepancy or gap"},
										{"key":"fda_submission_number","label":"FDA Submission Number","info":"Based on system calculation if compared RA data matches FURLS or based on values entered by RA SME during verification in case of a data discrepancy or gap"},
										{"key":"fda_submission_supplement_number","label":"FDA Submission Supplement Number","info":'Supplement # only when PMA or HDE Submission #(s) is entered.Otherwise,defaults to "N/A".'},
										{"key":"submission_status","label":"Submission Status","info":"Based on submission #(s) entered. 'N/A' if no submission #(s) is entered."},
										{"key":"submission_date","label":"Submission Date","info":"This is the FDA received date based on the final submission #(s) entered. 'N/A' if no submission #(s) is entered."},
										{"key":"approval_date","label":"Approval/Clearance Date","info":"Based on submission #(s) entered. 'N/A' if no submission #(s) is entered."},
										{"key":"k510_dt_if_used_to_bring_sku_to_market","label":"510k DT# (if used to bring SKU to market)","info":"Defaults to 'N/A' but is editable. If needed, contact Corporate RA to enter value."},
										{"key":"fda_medical_device_listing_number","label":"FDA Medical Device Listing Number","info":"Based on system calculation if compared RA data matches FURLS or based on values entered by RA SME during verification in case of a data discrepancy or gap"},
										{"key":"gmdn_codes","label":"GMDN Code(s)","info":"Based on GUDID value or Oracle value if item is not in GUDID. Otherwise, defaults to 'TBD'"},
										{"key":"item_in_gudid","label":"Item in GUDID","info":"Data source is GUDID"},
										{"key":"model_version","label":"Model/ Version #","info":"Data source is GUDID"},
										{"key":"cat_no","label":"Cat #","info":"Data source is GUDID"},
										{"key":"labeler_duns_in_gudid","label":"Labeler Company Name + City DUNS #","info":"Data source is GUDID"},
										{"key":"distribution_status","label":"Device Commercial Distribution Status (GUDID)","info":"Data source is GUDID"},
										{"key":"fda_product_codes","label":"FDA Product Codes","info":"Data source is GUDID"},
										{"key":"device_exempt_from_premarket_submission_requirements","label":"Device Exempt from Premarket Submission Requirements","info":"Data source is GUDID"},
										{"key":"fda_premarket_submission_numbers","label":"FDA Premarket Submission Numbers","info":"Data source is GUDID"},
										{"key":"fda_submission_supplement_numbers","label":"FDA Submission Supplement Numbers","info":"Data source is GUDID"},
										{"key":"fda_medical_device_listing_number_dl","label":"FDA Medical Device Listing Number (DL #)","info":"Data source is GUDID"},
										{"key":"gmdn_codes1","label":"GMDN Code(s) (GUDID)","info":"Data source is GUDID"},
										{"key":"dl_in_furls_based_on_submission_in_gudid","label":"DL # in FURLS based on Submission # in GUDID","info":"Data source is FURLS"},
										{"key":"product_code_in_furls_based_on_submission_in_gudid","label":"Product Code in FURLS based on Submission # in GUDID ","info":"Data source is FURLS"},
										{"key":"submission__exception_in_furls_based_on_dl_in_gudid","label":"Submission  #/ exception in FURLS based on DL # in GUDID","info":"Data source is FURLS"},
										{"key":"product_code_in_furls_based_on_dl_in_gudid_","label":"Product Code in FURLS based on DL # in GUDID","info":"Data source is FURLS"},
										{"key":"ils_510k_20","label":"510k # /PMA/Exempt (Oracle)","info":"Data source is Oracle."},
										{"key":"ils_fda_21","label":"ILS_FDA_21 (FDA ProCode)","info":"Data source is Oracle"},
										{"key":"ils_dev_6","label":"ILS_DEV_6 ","info":"Data source is Oracle"},
										{"key":"dl_in_furls_based_on_submission_in_oracle","label":"DL # in FURLS based on Submission # in Oracle","info":"Data source is FURLS"},
										{"key":"product_code_in_furls_based_on_submission_in_oracle","label":"Product Code in FURLS based on Submission # in Oracle","info":"Data source is FURLS"},
										{"key":"submission_exception_in_furls_based_on_dl_in_oracle1","label":"Submission # / Exception in FURLS based on DL # in Oracle","info":"Data source is FURLS"},
										{"key":"product_code_in_furls_based_on_dl_in_oracle","label":"Product Code in FURLS based on DL # in Oracle","info":"Data source is FURLS"},
										{"key":"fda_class1","label":"FDA Class (Agile)","info":"Data source is Agile"},
										{"key":"fda_product_code1","label":"FDA Product Code (Agile)","info":"Data source is Agile"},
										{"key":"fda_regulatory_status","label":"FDA Regulatory Status","info":"Data source is Agile"},
										{"key":"dl_in_furls_based_on_submission_in_oracle_","label":"DL # in FURLS based on product code in Agile","info":"Data source is FURLS"},
										{"key":"submission_exception_in_furls_based_on_dl_in_oracle","label":"Submission  #/ exception in FURLS based on Product Code in Agile","info":"Data source is FURLS"},
										{"key":"make_buy","label":"MAKE_BUY (Oracle)","info":"Data source is Oracle"},
										{"key":"intg_mfg_11","label":"INTG_MFG_11 (Integra Manufactured?) (Oracle)","info":"Data source is Oracle"},
										{"key":"design_specification_owner","label":"Design/Specification Owner (Agile)","info":"Data source is Agile"},
										{"key":"intg_site_12","label":"INTG_SITE_12 (Integra Mfr Name) (Oracle)","info":"Data source is Oracle"},
										{"key":"ils_est_reg_22","label":"ILS_EST_REG_22 (ILS Establishment Reg #) (Oracle)","info":"Data source is Oracle"},
										{"key":"vend_est_reg_25","label":"VEND_EST_REG_25 (Vendor FDA Establishment Reg #) (Oracle)","info":"Data source is Oracle"},
										{"key":"vend_dev_23","label":"VEND_DEV_23 (Vendor Device #) (Oracle)","info":"Data source is Oracle"},
										{"key":"gmdn_28","label":"GMDN_28 (GMDN Code) (Oracle)","info":"Data source is Oracle"},
							];
					var isEditable = false;
					if(System.roles.indexOf("RA_SuperUser")>-1 || System.roles.indexOf("RA_SMEs")>-1){
						isEditable = true;
					}else{
						editableFields.length = 0;
					}
					for(var i=0;i<mapLabelObj.length;i++){
						var key = mapLabelObj[i].key;
						var value = data[key] || "";
						var valueContent = $("<div/>").html(value),color = "#DFF1D6";
						var	attribute = {};
						attribute["data-content"] = (mapLabelObj[i] && mapLabelObj[i].info)?mapLabelObj[i].info:""
						attribute["data-placement"] = "top";
						attribute["data-toggle"] = "popover";
						attribute["data-html"] = true;
						if(editableFields.indexOf(key)>-1){
							color = "#FFFF66";
							valueContent = $("<input/>").data("oldvalue",value).attr({"name":key}).addClass("form-control").val(value);
						}
						if(rowData.user_status === "A"){
							valueContent.attr({"readonly":"readonly"});
						}
						table.append($("<tr/>")
								.append($("<td/>").css({"background-color":color})
									.append(mapLabelObj[i].label)
									.append($("<i/>").attr(attribute).addClass("fa fa-info-circle fa-lg pull-right cursor-pointer txt-color-blue")))
									.append($("<td/>").css({"background-color":color}).html(valueContent)));
					}
					table.find("i").popover({
			    		  placement: 'auto',
			    		  delay :  { "show": 100, "hide": 100 },
			    		  trigger : 'hover'
			    	  });
					$("#poDocView").append($("<form/>").attr("id","updateraattribute").append(table));					
					$("#poDocView").append($("<footer/>").addClass("rightalign").append(isEditable===true?$("<a/>").unbind("click").bind("click",function(){
						var query1 = "update l1_source_consolidation set ";
						var query2 = "INSERT INTO source_consolidation_audit ( ";
						var insertedColums = ["item_no","updated_by","version","updated_on"];
						var insertedValue = ["'"+rowData.item_no+"'","'"+System.userName+"'","(select coalesce(max(version),0)+1 from source_consolidation_audit)"," now() "];
						var updatedData = $("#updateraattribute").serializeArray(),values = [],isValidate = false;
						if(updatedData && updatedData.length>0){
							for(var i=0;i<updatedData.length;i++){;
								values.push(updatedData[i].name+" = '"+updatedData[i].value+"'");
								insertedColums.push(updatedData[i].name);
								insertedValue.push("'"+updatedData[i].value+"'");
								var oldValue = $("#updateraattribute").find("[name='"+updatedData[i].name+"']").data("oldvalue");
								if(oldValue !== updatedData[i].value)
									isValidate =  true;
							}
						}
						if(!isValidate){
							showNotification('notify', "There is no change in existing data.");
							return;
						}
						query1+=values.toString();
						query1+="  where item_no = '"+rowData.item_no+"' ";
						query2+=insertedColums.toString()+") VALUES ("+ insertedValue.toString()+")";
						//var query = [btoa(query1)];//,btoa(query2)
						var query = btoa(unescape(encodeURIComponent(query1)));
						var updateRequest = {
							"source" : 'postgreSQL',
							"dataBase" : 'QFIND_RA',
							"queries" : query.toString()
						};
						callAjaxService("integraGenericUpdate",
									function(response) {
										if (response && response.isException) {
											showNotification('error', response.customMessage);
											return;
										}
										$('#myModalDrilldown').modal('hide');
										showNotification('success','Saved Successfully.');
										function callbackSucessClearCache() {
											$("#applyFilter").trigger("click");//dataAnalyser.profiling.grapher.refreshTable();
										}
										callAjaxService("clearCache", callbackSucessClearCache,
												callBackFailure, null, "POST");
						}, callBackFailure, updateRequest, "POST", "json", true);
						
					}).addClass("btn btn-primary").html("Update"):""));
					if(rowData.user_status === "A"){
						$("#poDocView").find("footer").find("a").hide();	
					}
				}
				
			}
		}, callBackFailure,request, "POST",null,true);
		console.log(obj);
	};
	this.showConsolidatedMatch = function(obj){
		$("#myModalDrilldown").modal("show");
		$("#poDocView").parents(".modal-dialog").removeClass("width-500");
		var rowData = JSON.parse($(obj).parents("tr").data("request")),isFinishRequired = false;
		if((System.roles.indexOf("RA_SuperUser")>-1 || System.roles.indexOf("RA_SMEs")>-1) && rowData.status !== 'Matched'){
			isFinishRequired = true;
		}
		var columnSettings = Jsonparse($(obj).parents("tr").data("columnSettings"));
		var allColumns = ["fda_product_code", "fda_class", "submission_type", "fda_submission_number", "fda_medical_device_listing_number"];
		var headerInfo = $("<div/>").addClass("col-sm-8")
							.append($("<strong/>").html("RA Match Results")).append("</br>")
							.append($("<strong/>").html("Item# :")).append(rowData.item_no).append("</br>")
							.append($("<strong/>").html("Product Name :")).append((rowData.product_name && rowData.product_name!== "null")?rowData.product_name:"").append("</br>")
							.append($("<strong/>").html("Item Status :")).append((rowData.item_status && rowData.item_status !== "null")?rowData.item_status:"");
		$("#printView #myModalLabel").addClass("row").html(headerInfo)
		.append($("<a/>").addClass("btn marginRight10 pull-right  btn-primary col-sm-1").html("Save").data("status","S"))
		.append(isFinishRequired === true?$("<a/>").addClass("btn marginRight10 pull-right btn-success col-sm-1").html("Finish").data("status","A"):'');
		if(rowData.user_status && rowData.user_status.toLowerCase() === 'a'){
			$("#poDocView").parents(".modal-content").find("a").attr({"disabled":"disabled"});
		}
		if(rowData.user_status && rowData.user_status.toLowerCase() === 'a' && System.roles.indexOf("RA_SuperUser")>-1){
			$("#printView #myModalLabel").append($("<a/>").addClass("btn marginRight10 pull-right btn-warning col-sm-1").html("Override"));
			$("#printView #myModalLabel").append($("<div/>").addClass("smart-form col-sm-4 pull-right hide").css({"padding-left":"40px"})
					.append($("<label/>").addClass("checkbox").append($("<input/>").attr({"type":"checkbox","name":"overrideRAAttributeaction"}))
							.append($("<i/>")).append("Set Action as Open")));
		}
		$("#poDocView").empty();
		$("#tableConfigTab").hide();
		var table = $("<table/>").addClass("table table-bordered");
		table.append($("<thead/>").append($("<tr/>")
				.append( $("<th/>").attr({"rowspan":"2"}).css({"width":"24%","background-color":"#3276b1","color":"#fff","border-bottom":"1px solid #3276b1","vertical-align": "top","text-align": "left"}).append("RA Attribute"))
				.append( $("<th/>").attr({"colspan":"3"}).css({"width":"24%","background-color":"#3276b1","color":"#fff","vertical-align": "top","text-align": "left"}).append("GUDID vs FURLS ")
						.append($("<i/>").attr({"data-content":"Qfind compares GUDID vs FURLS data if possible, then Oracle vs FURLS data if GUDID vs FURLS comparison is not possible, and lastly Agile vs FURLS data if prior two comparisons are not possible.","data-placement":"top","data-toggle":"popover","data-html":true}).addClass('fa fa-info-circle fa-lg cursor-pointer')))
				.append( $("<th/>").attr({"colspan":"3"}).css({"width":"24%","background-color":"#3276b1","color":"#fff","vertical-align": "top","text-align": "left"}).append("Oracle vs.FURLS ")
						.append($("<i/>").attr({"data-content":"Qfind compares GUDID vs FURLS data if possible, then Oracle vs FURLS data if GUDID vs FURLS comparison is not possible, and lastly Agile vs FURLS data if prior two comparisons are not possible.","data-placement":"top","data-toggle":"popover","data-html":true}).addClass('fa fa-info-circle fa-lg cursor-pointer')))
				.append( $("<th/>").attr({"colspan":"2"}).css({"width":"16%","background-color":"#3276b1","color":"#fff","vertical-align": "top","text-align": "left"}).append("Agile vs.FURLS ")
						.append($("<i/>").attr({"data-content":"Qfind compares GUDID vs FURLS data if possible, then Oracle vs FURLS data if GUDID vs FURLS comparison is not possible, and lastly Agile vs FURLS data if prior two comparisons are not possible.","data-placement":"top","data-toggle":"popover","data-html":true}).addClass('fa fa-info-circle fa-lg cursor-pointer')))
				.append(rowData.status !== 'Matched'?$("<th/>").attr({"rowspan":"2"}).css({"width":"12%","background-color":"#3276b1","color":"#fff","border-bottom":"1px solid #3276b1","vertical-align": "top","text-align": "left"}).append("RA Verification ")
						.append($("<i/>").attr({"data-content":"These values represent the final/correct RA values. Enter comments if needed.","data-placement":"top","data-toggle":"popover","data-html":true}).addClass('fa fa-info-circle fa-lg cursor-pointer')):"")
				));
		table.append($("<thead/>").append($("<tr/>")
				.append($("<th/>").attr({"rowspan":"0"}).css({"width":"24%","background-color":"#3276b1","color":"#fff"}).html(""))
				.append( $("<th/>").css({"width":"8%","background-color":"#3276b1","color":"#fff","vertical-align": "top","text-align": "left"}).append("GUDID ")
						.append($("<i/>").attr({"data-content":"Data source is GUDID. Follow GSOP-933 if a correction or update is needed to GUDID data.","data-placement":"top","data-toggle":"popover","data-html":true}).addClass('fa fa-info-circle fa-lg cursor-pointer')))
				.append($("<th/>").css({"width":"8%","background-color":"#3276b1","color":"#fff","vertical-align": "top","text-align": "left"}).append("FURLS ")
						.append($("<i/>").attr({"data-content":"Data source is FURLS. Follow GSOP-905 if FURLS data needs to be updated.","data-placement":"top","data-toggle":"popover","data-html":true}).addClass('fa fa-info-circle fa-lg cursor-pointer')).append("</br><span style='font-size:x-small'>(Based on Submission # in GUDID)</span>"))		
				.append($("<th/>").css({"width":"8%","background-color":"#3276b1","color":"#fff","vertical-align": "top","text-align": "left"}).append("FURLS ")
						.append($("<i/>").attr({"data-content":"Data source is FURLS. Follow GSOP-905 if FURLS data needs to be updated.","data-placement":"top","data-toggle":"popover","data-html":true}).addClass('fa fa-info-circle fa-lg cursor-pointer')).append("</br><span style='font-size:x-small'>(Based on DL # in GUDID)<span>"))
						
				.append($("<th/>").css({"width":"8%","background-color":"#3276b1","color":"#fff","vertical-align": "top","text-align": "left"}).append("Oracle ")
						.append($("<i/>").attr({"data-content":"Data source is Oracle. Follow GSOP-424 and/or site/local procedures if a correction or update is needed to Oracle data.","data-placement":"top","data-toggle":"popover","data-html":true}).addClass('fa fa-info-circle fa-lg cursor-pointer')))
				.append($("<th/>").css({"width":"8%","background-color":"#3276b1","color":"#fff","vertical-align": "top","text-align": "left"}).append("FURLS ")
						.append($("<i/>").attr({"data-content":"Data source is FURLS. Follow GSOP-905 if FURLS data needs to be updated..","data-placement":"top","data-toggle":"popover","data-html":true}).addClass('fa fa-info-circle fa-lg cursor-pointer'))
						.append("</br><span style='font-size:x-small'>(Based on Submission # in Oracle)</span>"))
				.append($("<th/>").css({"width":"8%","background-color":"#3276b1","color":"#fff","vertical-align": "top","text-align": "left"}).append("FURLS ")
						.append($("<i/>").attr({"data-content":"Data source is FURLS. Follow GSOP-905 if FURLS data needs to be updated.","data-placement":"top","data-toggle":"popover","data-html":true}).addClass('fa fa-info-circle fa-lg cursor-pointer')).append("</br><span style='font-size:x-small'>(Based on DL # in Oracle)</span>"))
						
				.append($("<th/>").css({"width":"8%","background-color":"#3276b1","color":"#fff","vertical-align": "top","text-align": "left"}).append("Agile ")
						.append($("<i/>").attr({"data-content":"Data source is Agile. Follow GSOP-742 and/or site/local procedures if a correction or update is needed to Agile data.","data-placement":"top","data-toggle":"popover","data-html":true}).addClass('fa fa-info-circle fa-lg cursor-pointer')))
				.append($("<th/>").css({"width":"8%","background-color":"#3276b1","color":"#fff","vertical-align": "top","text-align": "left"}).append("FURLS ")
						.append($("<i/>").attr({"data-content":"Data source is FURLS. Follow GSOP-905 if FURLS data needs to be updated.","data-placement":"top","data-toggle":"popover","data-html":true}).addClass('fa fa-info-circle fa-lg cursor-pointer')).append("</br><span style='font-size:x-small'>(Based on Product code in Agile)</span>"))
						
				/*.append($("<th/>").css({"width":"15%","background-color":"#3276b1","color":"#fff"}).append("FURLS ")
						.append($("<i/>").attr({"data-content":"Data source is FURLS. Follow GSOP-905 if FURLS data needs to be updated.</br><b>Product Code :</b> Based On DL In GUDID</br><b>Submission Number:</b>Based On DL In GUDID</br><b>Medical Device Listing Number : </b>Based On Submission In GUDID","data-placement":"top","data-toggle":"popover","data-html":true}).addClass('fa fa-info-circle fa-lg cursor-pointer')))
				.append($("<th/>").css({"width":"15%","background-color":"#3276b1","color":"#fff"}).append("FURLS ")
						.append($("<i/>").attr({"data-content":"Data source is FURLS. Follow GSOP-905 if FURLS data needs to be updated.</br><b>Product Code :</b> Based On Submission In GUDID</br><b>Submission Number:</b>Based On DL In Oracle</br><b>Medical Device Listing Number : </b>Based On Submission In Oracle","data-placement":"top","data-toggle":"popover","data-html":true}).addClass('fa fa-info-circle fa-lg cursor-pointer')))*/		
				
				.append(rowData.status !== 'Matched'?$("<th/>").attr({"rowspan":"0"}).css({"width":"12%","background-color":"#3276b1","color":"#fff"}):"")));
		if(columnSettings && columnSettings.length>0){
			for(var i=0;i<columnSettings.length;i++){
				if(allColumns.indexOf(columnSettings[i].data)>-1){
					var bgColor = "#DFF1D6";
					table.append($("<tr/>").append($("<th/>").css({"background-color":"rgb(120, 191, 105)","color":"white"}).html(columnSettings[i].sTitle))
							.append($("<td/>").css({"background-color":(columnSettings[i].data === 'fda_class')?"#ccc":"#DFF1D6"})
									.attr({"name":"gudid_"+columnSettings[i].data}))
							.append($("<td/>").css({"background-color":(columnSettings[i].data === 'fda_class' || columnSettings[i].data === 'submission_type')?"#ccc":"#DFF1D6","font-weight":(columnSettings[i].data === 'fda_submission_number')?"bold":""})
									.attr({"name":"furls_gudid_"+columnSettings[i].data}))
							.append($("<td/>").css({"background-color":(columnSettings[i].data === 'fda_class' || columnSettings[i].data === 'submission_type')?"#ccc":"#DFF1D6","font-weight":(columnSettings[i].data === 'fda_medical_device_listing_number')?"bold":""})
									.attr({"name":"furls1_gudid_"+columnSettings[i].data}))
									
							.append($("<td/>").css({"background-color":(columnSettings[i].data === 'fda_class' || columnSettings[i].data === 'submission_type')?"#ccc":"#DFF1D6"})
									.attr({"name":"oracle_"+columnSettings[i].data}))
							.append($("<td/>").css({"background-color":(columnSettings[i].data === 'fda_class' || columnSettings[i].data === 'submission_type')?"#ccc":"#DFF1D6","font-weight":(columnSettings[i].data === 'fda_submission_number')?"bold":""})
									.attr({"name":"furls_oracle_"+columnSettings[i].data}))
							.append($("<td/>").css({"background-color":(columnSettings[i].data === 'fda_class' || columnSettings[i].data === 'submission_type')?"#ccc":"#DFF1D6","font-weight":(columnSettings[i].data === 'fda_medical_device_listing_number')?"bold":""})
									.attr({"name":"furls1_oracle_"+columnSettings[i].data}))
									
							.append($("<td/>").css({"background-color":(columnSettings[i].data === 'fda_submission_number' || columnSettings[i].data === 'fda_medical_device_listing_number')?"#ccc":"#DFF1D6"})
									.attr({"name":"agile_"+columnSettings[i].data}))
							.append($("<td/>").css({"background-color":(columnSettings[i].data === 'fda_class' || columnSettings[i].data === 'submission_type')?"#ccc":"#DFF1D6","font-weight":(columnSettings[i].data === 'fda_product_code')?"bold":""})
									.attr({"name":"furls_agile_"+columnSettings[i].data}))
									
							/*.append($("<td/>").css({"background-color":(columnSettings[i].data === 'fda_class')?"#ccc":"#DFF1D6"})
									.attr({"name":"furls_"+columnSettings[i].data}))
							.append($("<td/>").css({"background-color":(columnSettings[i].data === 'fda_class')?"#ccc":"#DFF1D6"})
									.attr({"name":"furls1_"+columnSettings[i].data}))*/		
							.append(rowData.status !== 'Matched'?$("<td/>").append($("<input/>").attr({"name":"sco_"+columnSettings[i].data}).addClass("form-control")):'')
							);
					//rowData.user_status==='a'
				}
			}
		}
		var commentSection = $("<table/>").attr({"name":"tableComments"}).css({"table-layout":"fixed"}).addClass("table table-bordered margin-bottom-10 padding-bottom-10").append($("<thead/>")
				.append($("<tr/>").append($("<th/>").append($("<span/>").html("Comment")))))
				.append($("<tbody/>"));
		
		$("#poDocView").append(table);
		$("#poDocView").append($("<div/>").append($("<strong/>").html("Note : ")).append("The bold text indicates the lookup value that was used to get the FURLS values"));
		$("#poDocView").append(commentSection);
		if(rowData.user_status && rowData.user_status.toLowerCase() === 'a'){
			$("#poDocView").find("input,textarea").attr("readonly",true);
		}
		$("#poDocView").find("i").popover({
	  		  placement: 'auto',
	  		  container: '#poDocView',
	  		  delay :  { "show": 100, "hide": 100 },
	  		  trigger : 'hover'
  	  	});
		var query = "select fda_product_codes as gudid_fda_product_code,product_code_in_furls_based_on_submission_in_gudid as furls_gudid_fda_product_code,ils_fda_21 as oracle_fda_product_code," +
				"product_code_in_furls_based_on_dl_in_gudid_ as furls1_gudid_fda_product_code,product_code_in_furls_based_on_submission_in_oracle as furls_oracle_fda_product_code,fda_regulatory_status as agile_submission_type," +
				"product_code_in_furls_based_on_dl_in_oracle as furls1_oracle_fda_product_code,fda_product_code1 as agile_fda_product_code,fda_product_code1 as furls_agile_fda_product_code,fda_class1 as agile_fda_class,submission__exception_in_furls_based_on_dl_in_gudid as furls1_gudid_fda_submission_number," +
				"device_exempt_from_premarket_submission_requirements  as gudid_submission_type,furls_submission_type,fda_premarket_submission_numbers as gudid_fda_submission_number,fda_premarket_submission_numbers as furls_gudid_fda_submission_number," +
				"ils_510k_20 as oracle_fda_submission_number,ils_510k_20 as furls_oracle_fda_submission_number,submission_exception_in_furls_based_on_dl_in_oracle1 as  furls1_oracle_fda_submission_number,submission_exception_in_furls_based_on_dl_in_oracle as furls_agile_fda_submission_number," +
				"fda_medical_device_listing_number_dl as gudid_fda_medical_device_listing_number,fda_medical_device_listing_number_dl as furls1_gudid_fda_medical_device_listing_number,dl_in_furls_based_on_submission_in_gudid as furls_gudid_fda_medical_device_listing_number," +
				"dl_in_furls_based_on_submission_in_oracle as furls_oracle_fda_medical_device_listing_number,dl_in_furls_based_on_submission_in_oracle_  as furls_agile_fda_medical_device_listing_number,ils_dev_6 as oracle_fda_medical_device_listing_number,ils_dev_6 as furls1_oracle_fda_medical_device_listing_number," +
				"fda_product_code as sco_fda_product_code,fda_class as sco_fda_class,submission_type as sco_submission_type,fda_submission_number as sco_fda_submission_number,fda_medical_device_listing_number as sco_fda_medical_device_listing_number,comments as sco_comment from l1_source_consolidation where <FILTER>";
		var request = {
				source:"postgreSQL",
				database:"QFIND_RA",
				query:query,
				groupByEnabled:true,
				filters:" item_no = '"+rowData.item_no+"'"
		};
		enableLoading();
		callAjaxService("getData", function(response){
			disableLoading();
			if (response && response.isException) {
				showNotification("error", response.customMessage);
				return;
			}
			if(response && response.data && response.data.length>0){
				var data = response.data[0];
				if(data && Object.keys(data).length>0){
					for (var key in data) {
						$("#poDocView").find("[name='"+key+"']").html(data[key]);
						if(rowData && rowData.user_status)
							$("#poDocView").find("[name='"+key+"']").val(data[key]);
						if(key === "sco_comment"){
							var commnets = Jsonparse(data[key]);
							if(commnets && Object.keys(commnets).length>0){
								for(var i=0;i<commnets.length;i++){
									$("[name='tableComments']").append($("<tr/>").append($("<td/>").attr({"user":commnets[i].user,"date":commnets[i].datetime})
											.html("<b>"+commnets[i].user+"</b> Added comment on "+commnets[i].datetime+"</br><pre>"+commnets[i].comment+"</pre>")));
								}
							}
						}
					}
					$("[name='tableComments']").append($("<tr/>").append($("<td/>").append($("<textarea/>").attr({"name":"sco_comment"})
							.css({"min-height":"100px","max-width":"950px"}).addClass("width-100per"))));
					if($("[name='furls_submission_type']").html() === ""){
						$("[name='furls_submission_type'],[name='furls1_submission_type']").css("background-color","#ccc");
					}
				}
			}
		}, callBackFailure,request, "POST",null,true);
		$("#printView #myModalLabel").find("a").unbind("click").bind("click",function(){
			var status = $(this).data("status");
			var action = (status === 'S')?"OPEN":"CLOSED";
			var updatedField = {};
			if(status){
				$("#poDocView").find("input").each(function(){
					if($(this).val()){
						updatedField[$(this).attr("name")] = $(this).val();
					}else{
						updatedField[$(this).attr("name")] = "";
					}
				});
				var comments = [];
				$("[name='tableComments']").find("tbody").find("tr").each(function(){
					var user = $(this).find("td").attr("user");
					var comment = $(this).find("pre").html();
					var date = $(this).find("td").attr("date");
					if(user && comment){
						comments.push({
							user:user,
							datetime:date,
							comment:comment
						});
					}
				});
				var scoComment = $("[name='sco_comment']").val()|| '';
				if(scoComment && $.trim(scoComment)){
					comments.push({
						user:System.userDisplayName,
						datetime:getManipulatedDate(null,null,null,"%m/%d/%Y %H:%M:%S"),
						comment:scoComment
					});
				}
				updatedField.sco_comment = JSON.stringify(comments);
				if($("[name='overrideRAAttributeaction']").length>0){
					action = $("[name='overrideRAAttributeaction']").is(":checked")?"OPEN":"CLOSED";
				}
				var isMatch = ($("[name='sco_fda_product_code']").length === 0);
				var query = "update l1_source_consolidation set fda_product_code='"+updatedField.sco_fda_product_code+"',fda_class='"+updatedField.sco_fda_class+"',submission_type='"+updatedField.sco_submission_type+"'," +
				"fda_submission_number='"+updatedField.sco_fda_submission_number+"',fda_medical_device_listing_number='"+updatedField.sco_fda_medical_device_listing_number+"',comments='"+updatedField.sco_comment+"',user_status = '"+status+"',updated_by = '"+System.userName+"',action='"+action+"',updated_on = now()";
				if(isMatch)
					query = " update l1_source_consolidation set comments='"+updatedField.sco_comment+"',updated_by = '"+System.userName+"',updated_on = now()";
				
				if(status === 'A'){
					var isValidate = true;
					$("#poDocView").find("input").each(function(){
						if($(this).val() === "" || $(this).val().trim() === ""){
							isValidate = false;
						}
					});
					if(!isValidate){
						showNotification("error", "Please enter data in RA Verification fields.");
						return;
					}
				}
				query+=" where item_no = '"+rowData.item_no+"'";
				var queries = [btoa(query)];
				var queries = [btoa(unescape(encodeURIComponent(query)))];
				/*queries.push(btoa("INSERT INTO source_consolidation_audit (item_no,updated_by,version,updated_on,fda_product_code,fda_class,submission_type,fda_submission_number,fda_medical_device_listing_number,comments) " +
				"VALUES('"+rowData.item_no+"','"+System.userName+"',(select coalesce(max(version),0)+1 from source_consolidation_audit),now(),'"+updatedField.sco_fda_product_code+"','"+updatedField.sco_fda_class+"','"+updatedField.sco_submission_type+"','"+updatedField.sco_fda_submission_number+"','"+updatedField.sco_fda_medical_device_listing_number+"','"+updatedField.sco_comment+"')"));*/
				var updateRequest = {
					"source" : 'postgreSQL',
					"dataBase" : 'QFIND_RA',
					"queries" : queries.toString()
				};
				callAjaxService("integraGenericUpdate",
							function(response) {
								if (response && response.isException) {
									showNotification('error', response.customMessage);
									return;
								}
								$('#myModalDrilldown').modal('hide');
								showNotification('success',((status === 'S')?"Saved":"Approved")+' Successfully.');
								function callbackSucessClearCache() {
									$("#applyFilter").trigger("click");//dataAnalyser.profiling.grapher.refreshTable();
								}
								callAjaxService("clearCache", callbackSucessClearCache,
										callBackFailure, null, "POST");
				}, callBackFailure, updateRequest, "POST", "json", true);
			}else{
				$("#poDocView").find("input,textarea").removeAttr("readonly");
				$("#poDocView").parents(".modal-content").find("a").removeAttr("disabled");
				$("[name='overrideRAAttributeaction']").parents("div").removeClass("hide");
			}
		});
	}; 
	this.showAuditConsolidated = function(obj){
		var filters= "1=1",columnConfig = [{"data":"division","title":"Division"},{"data":"site_rollup","title":"SITE ROLLUP"},
		                                   {"data":"approved","title":"Approved/ Cleared?"},{"data":"fda_product_code","title":"FDA Product Code"},
		                                   {"data":"fda_class","title":"FDA Class"},{"data":"submission_type","title":"Submission Type"},
		                                   {"data":"fda_submission_number","title":"FDA Submission Number"},{"data":"fda_medical_device_listing_number","title":"FDA Medical Device Listing Number"},
		                                   {"data":"fda_submission_supplement_number","title":"FDA Submission Supplement Number"},{"data":"approval_date","title":"Approval Date"},
		                                   {"data":"submission_status","title":"Submission Status"},{"data":"submission_date","title":"Submission Date"},
		                                   {"data":"k510_dt_if_used_to_bring_sku_to_market","title":"510k DT#(SKU to Mkt.)"},{"data":"gmdn_codes","title":"GMDN Code(s)"},
		                                   {"data":"cat_no_on_label","title":"Cat # On Label"},{"data":"item_status","title":"Item Status"},
		                                   {"data":"item_type","title":"Item Type"},{"data":"product_name","title":"Product Name"},
		                                   {"data":"product_name_on_the_label","title":"Product Name On The Label"},{"data":"legal_manufacturer","title":"Legal Manufacturer"},		                                   
		                                   {"data":"item_in_gudid","title":"Item in GUDID"},{"data":"model_version","title":"Model/ Version #"},{"data":"device_exempt_from_premarket_submission_requirements","title":"Device Exempt from Premarket Submission Requirements"},
		                                   {"data":"fda_premarket_submission_numbers","title":"FDA Premarket Submission Numbers"},{"data":"dl_in_furls_based_on_submission_in_gudid","title":"DL # in FURLS based on Submission # in GUDID"},
		                                   {"data":"submission__exception_in_furls_based_on_dl_in_gudid","title":"Submission  #/ exception in FURLS based on DL # in GUDID"},{"data":"product_code_in_furls_based_on_submission_in_gudid","title":"Product Code in FURLS based on Submission # in GUDID"},
		                                   //{"data":"approval_clearance_letter_filed_internally","title":"Approval Clearance Letter Filled Internally"},
		                                   //{"data":"permission_to_use_letter_filed_internally","title":"Permission To Use Letter Filled Internally"},
		                                   {"data":"fda_product_codes","title":"FDA Product Codes"},{"data":"fda_submission_supplement_numbers","title":"FDA Submission Supplement Numbers"},
		                                   {"data":"fda_medical_device_listing_number_dl","title":"FDA Device Listing Numbers"},
		                                   {"data":"product_code_in_furls_based_on_dl_in_gudid_","title":"Product Code in FURLS based on DL # in GUDID"},
		                                   {"data":"ils_510k_20","title":"510k # /PMA/Exempt (Oracle)"},{"data":"ils_fda_21","title":"ILS_FDA_21 (FDA ProCode)"},
		                                   {"data":"ils_dev_6","title":"ILS_DEV_6"},{"data":"dl_in_furls_based_on_submission_in_oracle","title":"DL # in FURLS based on Submission # in Oracle"},
		                                   {"data":"product_code_in_furls_based_on_submission_in_oracle","title":"Product Code in FURLS based on Submission # in Oracle"},{"data":"submission_exception_in_furls_based_on_dl_in_oracle1","title":"Submission # / Exception in FURLS based on Product code in Agile"},
		                                   {"data":"product_code_in_furls_based_on_dl_in_oracle","title":"Product Code in FURLS based on DL # in Oracle"},{"data":"fda_class1","title":"FDA Class (Agile)"},
		                                   {"data":"fda_product_code1","title":"FDA Product Code (Agile)"},{"data":"fda_regulatory_status","title":"FDA Regulatory Status"},
		                                   {"data":"dl_in_furls_based_on_submission_in_oracle_","title":"DL # in FURLS based on product code in Agile"},
		                                   {"data":"submission_exception_in_furls_based_on_dl_in_oracle","title":"Submission # / Exception in FURLS based on product code in Agile"},		                                   
		                                   {"data":"action","title":"Action"},
		                                   {"data":"comments","title":"Comments"},{"data":"updated_by","title":"Verified By","render":function (data){return (data && data !== 'systemgenerated')?data:''}},
		                                   {"data":"updated_on","title":"Verified On","render":function (data){return data?getManipulatedDate(null,0,new Date(data)):''}}];
		var itemNo = $(obj).attr("item");
		if(itemNo){
			filters = " item_no in ('"+itemNo+"') ";
		}
		$("#myModalDrilldown").modal("show");
		$("#poDocView").parents(".modal-dialog").removeClass("width-500");
		var headerInfo = $("<div/>").addClass("col-sm-8").html("<strong>Audit Table Hisory</strong></br><strong>Item# :</strong> "+itemNo);
		$("#printView #myModalLabel").addClass("row").html(headerInfo);
		$("#poDocView").addClass("panel-body").empty();
		var table = $("<table/>").attr({"id":"sourceConsolidationAudit"}).addClass("table table-bordered");
		$("#poDocView").append(table);
		var config = {
			html: '#sourceConsolidationAudit',
			url: serviceCallMethodNames["getDataPagination"].url,
			source : 'postgreSQL',
			database : 'QFIND_RA',
			table:"source_consolidation_audit",
			groupbyenabled:true,
			data:{orderby:46},
			dataColumns:columnConfig.map(function(o){return o.data}).toString(),
			columns:columnConfig,
			filter:filters
		};
		constructGrid(config);
		$("#tableOutput_wrapper").find("th").removeClass("rightalign");
	};
	this.changeManagement = function(obj) {
		$("#modalDrilldown-additionalFeature").empty();
		var data = JSON.parse($(obj).parent("tr").data("request")), configColumns = [];
		// var pKeys = $(obj).parents("tr").data("primarykeys");
		var historyObj = JSON.parse($(obj).parents(".dataTables_wrapper").data(
				"history"));
		if (historyObj.mappingcolumns
				&& Object.keys(historyObj.mappingcolumns).length > 0) {
			for ( var key in historyObj.mappingcolumns) {
				if (historyObj.mappingcolumns[key]
						&& historyObj.mappingcolumns[key].column) {
					configColumns.push({
						"data" : historyObj.mappingcolumns[key].column,
						"title" : historyObj.mappingcolumns[key].label
					});
				}
			}
		}
		if (historyObj.primarykey) {
			configColumns.push({
				"data" : historyObj.primarykey,
				"title" : historyObj.primarykey
			});
		}
		if (historyObj.versionColumn) {
			configColumns.push({
				"data" : historyObj.versionColumn,
				"title" : historyObj.versionColumn
			});
		}
		if (historyObj.hsourcelink) {
			configColumns.push({
				"data" : historyObj.hsourcelink,
				"title" : historyObj.hsourcelink
			});
		}
		var tableDetails = Jsonparse($(obj).parents(".dataTables_wrapper")
				.data("tabledetail"));
		var request = {
			source : tableDetails.source || "postgreSQL",
			database : tableDetails.database || "QFINDSPELL",
			table : historyObj.table,
			columnConfigurations : JSON.stringify(configColumns),
			columns : configColumns.map(function(o) {
				return o.data
			}).toString(),
			start : 0,
			length : 10,
			filter : historyObj.foregincolumn + " in ('"
					+ data[historyObj.foregincolumn] + "') and "
					+ historyObj.versionColumn + " in (select max ("
					+ historyObj.versionColumn + ") from " + historyObj.table
					+ " where " + historyObj.foregincolumn + " in('"
					+ data[historyObj.foregincolumn] + "'))"
		};
		enableLoading();
		callAjaxService("getDataPagination", function(response) {
			callBackSuccessHistoryData(response, obj)
		}, callBackFailure, request, "POST", null, true);
	};
	var callBackSuccessHistoryData = function(response, obj) {
		
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		var masterdata = JSON.parse($(obj).parent("tr").data("request"));
		var historyObj = JSON.parse($(obj).parents(".dataTables_wrapper").data(
				"history"));
		var historyData = Jsonparse(response.data);
		if (historyData && historyData.length > 0) {
			historyData = historyData[0];
		}
		$("#modalDrilldown-additionalFeature").css({
			"max-height" : "500px",
			"overflow" : "auto"
		}).empty();
		var columnDiv = $("<div/>").addClass("col col-md-4");
		var table = $("<table/>").addClass('table table-bordered'), isHistoryRequired = false;
		if (historyData && historyData[historyObj.primarykey]) {
			table.data("primarykeyvalue", historyData[historyObj.primarykey]);
		}
		table.data("history", historyObj);
		table.data("tabledetail", Jsonparse($(obj).parents(
				".dataTables_wrapper").data("tabledetail")));
		if (historyObj.versionColumn && historyData[historyObj.versionColumn]
				&& historyData[historyObj.versionColumn] !== "0" && masterdata
				&& (masterdata[historyObj.statusColumn] === 'U')) {
			isHistoryRequired = true;
		}
		var isHistoryDeleted = false;
		if (historyObj.statusColumn
				&& masterdata[historyObj.statusColumn] === 'U'
				&& historyData[historyObj.statusColumn] === 'D') {
			table.data("historystatus", historyData[historyObj.statusColumn]);
			isHistoryDeleted = true;
		}
		if(historyData.item_status && historyObj.table === 'bom_oracle_data_history') {
			table.data("item_status", historyData["item_status"]);
		}
		if(historyData.web_access){
			table.data("webaccess", historyData["web_access"]);
		}
		table
				.append($("<tr/>")
						.append($("<th/>").css({
							"width" : "20%"
						}).addClass("alert alert-info").html("Columns"))
						.append(
								$("<th/>").css({
									"width" : "20%"
								}).addClass("alert alert-info").html(
										historyObj.tableMasterAlias
												|| "Existing"))
						.append(
								historyObj.msourcelink ? $("<th/>").css({
									"width" : "20%"
								}).addClass("alert alert-info").html(
										"Existing Source") : '')
						.append(
								isHistoryRequired ? $("<th/>")
										.css({
											"width" : "20%"
										})
										.addClass(
												"alert "
														+ (isHistoryDeleted ? "alert-danger"
																: "alert-info"))
										.html(
												isHistoryDeleted ? "Deleted"
														: (historyObj.tableHistoryAlias || "Updated"))
										: "")
						.append(
								(isHistoryRequired && historyObj.hsourcelink) ? $(
										"<th/>").css({
									"width" : "20%"
								}).addClass("alert alert-info").html(
										"Updated Source")
										: ""));
		var sortedHistoryData = Object.keys(historyObj.mappingcolumns).sort(
				function(a, b) {
					return historyObj.mappingcolumns[a].orderby
							- historyObj.mappingcolumns[b].orderby
				});
		for (var i = 0; i < sortedHistoryData.length; i++) {
			var key = sortedHistoryData[i];
			var hkey = (historyObj.mappingcolumns[key] && historyObj.mappingcolumns[key].column) ? historyObj.mappingcolumns[key].column
					: key;
			var className = '', columnName = historyObj.mappingcolumns[key].label
					|| key;
			var masterSourceLink, historySourceLink;
			masterdata[key] = masterdata[key] || "";
			historyData[hkey] = historyData[hkey] || "";
			if (historyObj.msourcelink && masterdata[historyObj.msourcelink]) {
				var jsonValue = Jsonparse(masterdata[historyObj.msourcelink]);
				masterSourceLink = jsonValue[key] ? jsonValue[key]
						: jsonValue["default"] ? jsonValue["default"] : "";
			}
			if (historyObj.hsourcelink && historyData[historyObj.hsourcelink]) {
				var jsonValue = Jsonparse(historyData[historyObj.hsourcelink]);
				historySourceLink = jsonValue[hkey] ? jsonValue[hkey]
						: jsonValue["default"] ? jsonValue["default"] : "";
			}
			if (masterdata[key] !== historyData[hkey])
				className = 'bg-color-yellow';
			table.append($("<tr/>").attr({
				"masterdatacolumn" : key,
				"masterdatavalue" : historyData[hkey]
			}).append($("<th/>").html(columnName)).append(
					$("<td/>").html(masterdata[key])).append(
					historyObj.msourcelink ? $("<td/>").html(masterSourceLink)
							: "").append(
					isHistoryRequired ? $("<td/>").addClass(className).html(
							historyData[hkey]) : "").append(
					(isHistoryRequired && historyObj.hsourcelink) ? $("<td/>")
							.html(historySourceLink) : ""));
		}
		$("#modalDrilldown-additionalFeature").append(table);
		if ($("#modalDrilldown").find("header").find(
				"[name='actionPlaceHolder']").length === 0) {
			$("#modalDrilldown").find("header").append(
					$("<div/>").addClass("btn-group floatRight").attr({
						"name" : "actionPlaceHolder"
					}));
		}
		if (historyObj.statusColumn
				&& masterdata[historyObj.statusColumn] !== 'A'
				&& masterdata[historyObj.statusColumn] !== 'D') {
			if ($("#modalDrilldownExtras").find("[name='approve']").length === 0) {
				var rejectButton =  $("<a/>").addClass("pull-right").attr({"name" : "reject"})
				.append($("<i/>").addClass("fa-fw fa fa-times")).append("Cancel").addClass("btn marginRight10 btn-danger");
				$("#modalDrilldownExtras").append(
						$("<a/>").addClass("pull-right").attr({
							"name" : "approve"
						}).append($("<i/>").addClass("fa-fw fa fa-check"))
								.append("Approve").addClass(
										"btn marginRight10 btn-success")
								).append(rejectButton);
			 }
			if(masterdata[historyObj.statusColumn]==='U'){
				$("#modalDrilldownExtras").find("[name='reject']").unbind("click").bind("click",obj,
						function(e) {
							updateMasterdataByHistory(e.data,"Rejected");
						}).show();	
			}else{
				$("#modalDrilldownExtras").find("[name='reject']").unbind("click").hide();
			}
		} else {
			$("#modalDrilldownExtras").find("[name='approve']").remove();
			$("#modalDrilldownExtras").find("[name='reject']").remove();
		}
		$("#modalDrilldownExtras").find("[name='approve']").unbind("click").bind("click",obj,
				function(e) {
					updateMasterdataByHistory(e.data,"Approved");
				});
				
			
		
		$('#goBack').addClass("marginRight10").unbind('click').bind('click',
				function() {
					$("#modalDrilldown").fadeOut('slow');
					$('.chart-parent-div').fadeIn('fast');
					$("#dataAnalyserProfile").show()
					if ($('#drawProfile').length > 0) {
						$('#drawProfile').fadeIn('fast');
					}
				});
		$('.chart-parent-div').fadeOut('slow');
		if ($('#drawProfile').length > 0) {
			$('#drawProfile').fadeOut('fast');
		}
		$("#dataAnalyserProfile").hide();
		$("#modalDrilldown").fadeIn('fast');
	};
	var updateMasterdataByHistory = function(obj, status) {
		
		var updateHistoryStatus = function() {
			var req = {
				source : tableDetails.source,
				database : tableDetails.database,
				table : historyObj.table,
				primaryKey : [ {
					fieldName : historyObj.primarykey,
					newValue : historyPkeyValue,
					fieldType : "number"
				} ],
				values : [
						{
							fieldName : historyObj.statusColumn,
							newValue : 'A',
							type : "U"
						},
						{
							fieldName : 'wf_status',
							newValue : 'Approved',
							type : "U"
						},
						{
							fieldName : 'wf_date',
							fieldType : "timestamp",
							newValue : getManipulatedDate(null, null, null,
									"%Y-%m-%d %H:%M:%S"),
							type : "U"
						}, {
							fieldName : 'wf_user',
							newValue : System.userName,
							type : "U"
						} ]
			};
			var request = {
				"request" : JSON.stringify(req)
			};
			callAjaxService("updateToDB", function(response) {
				if (response && response.isException) {
					showNotification("error", response.customMessage);
					return;
				}
				function callbackSucessClearCache() {
					$("#applyFilter").trigger("click");
				}
				callAjaxService("clearCache", callbackSucessClearCache,
						callBackFailure, null, "POST");

				$("#goBack").trigger("click");
			}, callBackFailure, request, "POST", null, true);
		};

		var tableDetails = $("#modalDrilldown-additionalFeature").find("table")
				.data("tabledetail");
		var historyObj = $("#modalDrilldown-additionalFeature").find("table")
				.data("history");
		var historyStatus = $("#modalDrilldown-additionalFeature")
				.find("table").data("historystatus");
		var webaccess = $("#modalDrilldown-additionalFeature")
				.find("table").data("webaccess");
		
		var item_status = $("#modalDrilldown-additionalFeature").find("table").data("item_status");
		
		
		var historyPkeyValue = $("#modalDrilldown-additionalFeature").find(
				"table").data("primarykeyvalue");
		var req = {
			source : tableDetails.source,
			database : tableDetails.database,
			table : tableDetails.table,
			primaryKey : Jsonparse($(obj).parents("tr").data("primarykeys")),
			values : []
		};
		if (status && status.toLowerCase() === 'approved') {
			$("#modalDrilldown-additionalFeature")
					.find("tr")
					.each(
							function() {
								if ($(this).attr("masterdatacolumn")) {
									var fieldName = $(this).attr(
											"masterdatacolumn"), newValue = $(this).attr("masterdatavalue");
									if (fieldName === historyObj.statusColumn) {
										newValue = (historyStatus && historyStatus === 'D') ? 'D'
												: 'A';
										if(webaccess && webaccess === "0"){
											newValue = "D";
										}
										if(historyObj.table === 'bom_oracle_data_history' && item_status) {
											switch(item_status.toLowerCase()) {
											  case 'pilot' : newValue = ""; break;
											  case 'production': newValue = "A"; break;
											  case 'prototype' : newValue = ""; break;
											  case 'obsolete' : newValue = "D"; break;
											  case 'inactive' : newValue = "D"; break;
											}
										}
									}
									if(fieldName === 'wf_user') {
										newValue = System.userName;
									}
									req.values.push({
										fieldName : fieldName,
										newValue : newValue,
										type : "U"
									});
								}
							});
			var request = {
				"request" : JSON.stringify(req)
			};
			callAjaxService("updateToDB", function(response) {
				if (response && response.isException) {
					showNotification("error", response.customMessage);
					return;
				} else {
					showNotification("success", "Updated Successfully");
					if(historyStatus && historyStatus === 'D'){
						$("#goBack").trigger("click");
						$("#applyFilter").trigger("click");
					}
					else{
						updateHistoryStatus();
					}
				}
			}, callBackFailure, request, "POST", null, true);
		} else {
			req.values.push({
				fieldName :'status' ,
				newValue : 'A',
				type : "U"
			});
			var request = {
					"request" : JSON.stringify(req)
				};
			callAjaxService("updateToDB",null, callBackFailure, request, "POST", null, true);
			
			req.table = historyObj.table;
			req.primaryKey = [ {
				fieldName : historyObj.primarykey,
				newValue : historyPkeyValue,
				fieldType : "number"
			} ];
			req.values = [
					{
						fieldName : 'wf_status',
						newValue : 'Rejected',
						type : "U"
					},
					{
						fieldName : 'wf_date',
						fieldType : "timestamp",
						newValue : getManipulatedDate(null, null, null,
								"%Y-%m-%d %H:%M:%S"),
						type : "U"
					}, {
						fieldName : 'wf_user',
						newValue : System.userName,
						type : "U"
					} ];
			if (historyObj.statusColumn) {
				req.values.push({
					fieldName : historyObj.statusColumn,
					newValue : (historyStatus && historyStatus === 'R') ? 'A'
							: 'R',
					type : "U"
				});
			}
			var request = {
				"request" : JSON.stringify(req)
			};
			callAjaxService("updateToDB", function(response) {
				if (response && response.isException) {
					showNotification("error", response.customMessage);
					return;
				} else {
					showNotification("success", "Updated Successfully");
					$("#goBack").trigger("click");
					$("#applyFilter").trigger("click");
				}
			}, callBackFailure, request, "POST",null,true);
		}
	};

	this.updatePlatformSpeciality = function(obj) {
		if ($(obj).hasClass('audit-log') || $(obj).hasClass('checkbox')
				|| $(obj).hasClass('dt-body-center')) {
			return; // As Audit log is not required for drill down.
		}
		var request = JSON.parse($(obj).parent("tr").data("request"));
		var dataSettings = JSON.parse($(obj).parent("tr").data("dataSettings"));
		var active = '';
		request.synonyms = request.synonyms?request.synonyms:'';
		$("#myModalDrilldown").addClass('plain-modal-theme');
		var isChecked = (request.status === 'Active') ? 'checked' : '';
		var str = '';
		str += '<form id="frmPlatformUpdate" class="" novalidate="novalidate"><div class="row"> <section>';
		str += '<label class="label">Platform & Specialty</label>';
		str += '<label class="input"><input type="text" class="platformSpeciality" name="platformSpeciality" value="'
				+ request.platform_and_specialty + '"></label>';
		str += '</section></div>';
		str += '<div class="row"> <section>';
		str += '<label class="label">Synonyms</label>';
		str += '<label class="input"><input type="text" class="platformSyn" value="'
				+ request.synonyms + '"></label>';
		str += '</section></div>';
		
		var modifiedOn = (request.modified_on) ? request.modified_on : '';
		var modifiedBy = (request.modified_by) ? request.modified_by : '';
		
		str += '<div class="row"> <section>';
		str += '<label class="label">Modified On</label>';
		str += '<label class="input state-disabled"><input disabled type="text" class="modifiedOn" value="'
				+ modifiedOn+ '"></label>';
		str += '</section></div>';
		
		str += '<div class="row"> <section>';
		str += '<label class="label">Modified By</label>';
		str += '<label class="input state-disabled"><input disabled type="text" class="modifiedBy" value="'
				+ modifiedBy + '"></label>';
		str += '</section></div>';
		
		str += '<div class="row"> <section>';
		str += '<label class="checkbox enable-checkbox"><input type="checkbox" class="platformStatus" '
				+ isChecked + ' value="' + request.status + '"><i></i>Active</label>';
		str += '</section></div></form>';
		str += '<footer class="modal-footer">';
		str += '<button type="button" class="pull-right btn btn-warning btnPSUpdate" aria-hidden="true">Update</button>';
		str += '</footer>';

		$("#myModalLabel", "#myModalDrilldown").html("");
		var modalTitle = (request.platform_and_specialty) ? request.platform_and_specialty
				: "";
		$("#myModalLabel", '#myModalDrilldown').html(
				"<div> Update : " + modalTitle + "</div>");
		$(".modal-dialog", "#myModalDrilldown").addClass("width-50p");
		$(".modal-body", "#myModalDrilldown").addClass(
				"no-padding-top no-padding-bottom");
		$('.modal-body').css("max-height", "20em");
		$("#poDocView").addClass('smart-form').html(str);
		$('#myModalDrilldown').modal('show');
		 $("#frmPlatformUpdate").validate({
				rules : {
					platformSpeciality : {
						required : true
					}
				},
				messages : {
					platformSpeciality : {
						required : 'Please enter Platform & specialty'
					}
				},
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
	        
		$('.btnPSUpdate')
				.on(
						'click',
						function() {
							if (!$("#frmPlatformUpdate").valid())
								return;
							if ($('.platformStatus').is(":checked"))
								active = 'Active';
							else
								active = 'Inactive';
							var query = "update evidence_platform_specialty set status='"
									+ active
									+ "', modified_on = now(), modified_by = '"
									+ $('#username').text()
									+ "' , synonyms = '"
									+ $('.platformSyn').val()
									+ "', platform_and_specialty = '"
									+ $('.platformSpeciality').val()
									+ "' where platform_and_specialty = '"
									+ request.platform_and_specialty + "'";

							var updateRequest = {
								"source" : 'postgreSQL',
								"dataBase" : dataSettings.database,
								"queries" : btoa(query)
							}
							callAjaxService(
									"integraGenericUpdate",
									function(response) {
										if (response && response.isException) {
											if (response.message
													.indexOf("ERROR: duplicate key") > -1) {
												showNotification('error',
														"This Platform & Specialty already Exists");
											} else {
												showNotification('error',
														response.customMessage);
											}
											return;
										}
										$('#myModalDrilldown').modal('hide');
										showNotification('success',
												'Platform & Specialty updated Successfully');
										$("#applyFilter").trigger("click");
									}, callBackFailure, updateRequest, "POST",
									"json", true);
						});
	}

	this.updateProductFamily = function(obj) {
		if ($(obj).hasClass('audit-log') || $(obj).hasClass('checkbox')
				|| $(obj).hasClass('dt-body-center')) {
			return; // As Audit log is not required for drill down.
		}
		var request = JSON.parse($(obj).parent("tr").data("request"));
		var dataSettings = JSON.parse($(obj).parent("tr").data("dataSettings"));
		$("#myModalDrilldown").addClass('plain-modal-theme');
		var selectRequest = {
			"source" : 'postgreSQL',
			"dbName" : dataSettings.database,
			"tableName" : "evidence_platform_specialty where status = 'Active' order by platform_and_specialty",
			"columnName" : 'platform_and_specialty'
		}
		callAjaxService(
				"getIntegraCategories",
				function(response) {
					if (response && response.isException) {
						showNotification('error', response.customMessage);
						return;
					}
					if(response.indexOf(request.platform_and_specialty) === -1) {
						response.push(request.platform_and_specialty);
					}
					var drpDwnStr = '';
					response.map(function(a) {
						drpDwnStr += '<option value="' + a + '">' + a
								+ '</option>';
					});
					var str = '';

					var isChecked = (request.pr_status === 'Active') ? 'checked'
							: '';
					request.modified_by = request.modified_by? request.modified_by=request.modified_by: request.modified_by="";
					
					str += '<form id="frmProductFamily" class="" novalidate="novalidate"><div class="row"> <section>';
					str += '<label class="label">Platform & Specialty</label>';
					str += '<label class="select"><select class="width-100per platformSpecialty" placeholder="Select Platform & Specialty">'
							+ drpDwnStr + '</select></label>';
					str += '</section></div>';

					str += '<div class="row"> <section>';
					str += '<label class="label">Product Family</label>';
					str += '<label class="input"><input type="text" class="productFamily" name="productFamily" value="'
							+ request.product_family + '"></label>';
					str += '</section></div>';

					str += '<div class="row"> <section>';
					str += '<label class="label">Synonyms</label>';
					str += '<label class="input"><input type="text" class="productFamilySyn" value="'
							+ request.synonyms + '" ></label>';
					str += '</section></div>';
					
					var modifiedOn = (request.modified_on) ? request.modified_on : '';
					var modifiedBy = (request.modified_by) ? request.modified_by : '';
					
					str += '<div class="row"> <section>';
					str += '<label class="label">Modified On</label>';
					str += '<label class="input state-disabled"><input disabled type="text" class="modifiedOn" value="'
							+ modifiedOn+ '"></label>';
					str += '</section></div>';
					
					str += '<div class="row"> <section>';
					str += '<label class="label">Modified By</label>';
					str += '<label class="input state-disabled"><input disabled type="text" class="modifiedBy" value="'
							+ modifiedBy + '"></label>';
					str += '</section></div>';

					str += '<div class="row"> <section>';
					str += '<label class="checkbox enable-checkbox"><input type="checkbox" class="productStatus" '
							+ isChecked
							+ ' value="'
							+ request.pr_status
							+ '"><i></i>Active</label>';
					str += '</section></div></form>';

					str += '<footer class="modal-footer">';
					str += '<button type="button" class="pull-right btn btn-warning btnPFUpdate"  aria-hidden="true">Update</button>';
					str += '</footer>';

					$("#myModalLabel", "#myModalDrilldown").html("");
					var modalTitle = (request.product_family) ? request.product_family
							: "";
					$("#myModalLabel", '#myModalDrilldown').html(
							"<div> Update : " + modalTitle + "</div>");
					$(".modal-dialog", "#myModalDrilldown").addClass(
							"width-50p");
					$(".modal-body", "#myModalDrilldown").addClass(
							"no-padding-top no-padding-bottom");
					$('.modal-body').css("max-height", "50em");
					$("#poDocView").addClass('smart-form').html(str);
					$('.platformSpecialty').val(request.platform_and_specialty);
					$('.platformSpecialty').select2();
					$('#myModalDrilldown').modal('show');
					$("#frmProductFamily").validate({
						rules : {
							productFamily : {
								required : true
							}
						},
						messages : {
							productFamily : {
								required : 'Please enter Product Family'
							}
						},
						errorPlacement : function(error, element) {
							error.insertAfter(element.parent());
						}
					});
					$('.btnPFUpdate')
							.on(
									'click',
									function() {
										if (!$("#frmProductFamily").valid())
											return;
										var active = $('.productStatus').is(
												':checked') ? 'Active'
												: 'Inactive';
										var query = "update evidence_product_family_syn set pr_status='"
												+ active
												+ "', modified_on = now(), modified_by = '"
												+ $('#username').text()
												+ "' ,platform_and_specialty= '"
												+ $('.platformSpecialty')
														.select2('val')
												+ "', product_family = '"
												+ $('.productFamily').val()
												+ "' , synonyms = '"
												+ $('.productFamilySyn').val()
												+ "' where product_family = '"
												+ request.product_family + "'";
										// +"' , specialty_name =
										// '"+$('.platformSpecialty').val()+"' ,
										// pr_status =
										// '"+$('.platformStatus').val()

										var updateRequest = {
											"source" : 'postgreSQL',
											"dataBase" : dataSettings.database,
											"queries" : btoa(query)
										}
										callAjaxService(
												"integraGenericUpdate",
												function(response) {
													if (response
															&& response.isException) {
														if (response.message
																.indexOf("ERROR: duplicate key") > -1) {
															showNotification('error',
																	"This Product Family already Exists");
														} else {
															showNotification('error',
																	response.customMessage);
														}
														return;
													}
													$('#myModalDrilldown').modal('hide');
													showNotification('success',
															'Product Family updated Successfully');
													$("#applyFilter").trigger(
															"click");
												}, callBackFailure,
												updateRequest, "POST", "json",
												true);
									});
				}, callBackFailure, selectRequest, "POST", "json", true);
	}

	this.updateProductBrand = function(obj) {
		if ($(obj).hasClass('audit-log') || $(obj).hasClass('checkbox')
				|| $(obj).hasClass('dt-body-center')) {
			return; // As Audit log is not required for drill down.
		}
		var request = JSON.parse($(obj).parent("tr").data("request"));
		var dataSettings = JSON.parse($(obj).parent("tr").data("dataSettings"));
		$("#myModalDrilldown").addClass('plain-modal-theme');
		var selectRequest = {
			"source" : 'postgreSQL',
			"dbName" : dataSettings.database,
			"tableName" : "evidence_platform_specialty where status = 'Active' order by platform_and_specialty",
			"columnName" : 'platform_and_specialty'
		}

		callAjaxService(
				"getIntegraCategories",
				function(response) {
					var psDrpdwnStr = '';
					var pfDrpdwnStr = '';
					var pfSDrpdwnStr = '';
					if (response && response.isException) {
						showNotification('error', response.customMessage);
						return;
					}
					if(response.indexOf(request.platform_and_specialty) === -1) {
						response.push(request.platform_and_specialty);
					}
					response.map(function(a) {
						psDrpdwnStr += '<option value="' + a + '">' + a
								+ '</option>';
					});
					// Get Brands
					selectRequest = {
						"source" : 'postgreSQL',
						"dbName" : dataSettings.database,
						"tableName" : 'evidence_product_family_syn',
						"columnName" : 'product_family'
					}
					callAjaxService(
							"getIntegraCategories",
							function(response) {
								if (response && response.isException) {
									showNotification('error',
											response.customMessage);
									return;
								}

								response.map(function(a) {
									pfDrpdwnStr += '<option value="' + a + '">'
											+ a + '</option>';
								});
								// Get Brands
								selectRequest = {
									"source" : 'postgreSQL',
									"dbName" : dataSettings.database,
									"tableName" : 'evidence_product_family_syn',
									"columnName" : 'synonyms'
								}
								callAjaxService(
										"getIntegraCategories",
										function(response) {
											if (response
													&& response.isException) {
												showNotification('error',
														response.customMessage);
												return;
											}
											response
													.map(function(a) {
														pfSDrpdwnStr += '<option value="'+ a+ '">'+ a + '</option>';
													});
											var str = '';
											var isChecked = (request.status === 'Active') ? 'checked': '';
											request.brand_synonyms? request.brand_synonyms=request.brand_synonyms:request.brand_synonyms="";
											request.created_by? request.created_by=request.created_by:request.created_by="";
											request.modified_by? request.modified_by=request.modified_by:request.modified_by="";
											
											str += '<form id="frmProductBrand" class="" novalidate="novalidate"><div class="row"> <section>';
											str += '<label class="label">Platform & Specialty</label>';
											str += '<label class="select"><select class="width-100per platformSpecialty" placeholder="Select Platform & Specialty">'
													+ psDrpdwnStr
													+ '</select></label>';
											str += '</section></div>';

											str += '<div class="row"> <section>';
											str += '<label class="label">Product Family</label>';
											str += '<label class="select"><select class="width-100per productFamily" placeholder="Select Product Family">'
													+ pfDrpdwnStr
													+ '</select></label>';
											str += '</section></div>';

											// str += '<div class="row">
											// <section>';
											// str += '<label
											// class="label">Product Family
											// Synonyms</label>';
											// str += '<label
											// class="select"><select
											// class="productFamilySyn
											// width-100per">'+pfSDrpdwnStr+'</select></label>';
											// str += '</section></div>';

											str += '<div class="row"> <section>';
											str += '<label class="label">Brand</label>';
											str += '<label class="input"><input type="text" class="productBrand" name="productBrand" value="'
													+ request.product_brand_family
													+ '"></label>';
											str += '</section></div>';

											str += '<div class="row"> <section>';
											str += '<label class="label">Synonyms</label>';
											str += '<label class="input"><input type="text" class="brandSyn" value="'
													+ request.brand_synonyms
													+ '"></label>';
											str += '</section></div>';

											request.keyword = (request.keyword) ? request.keyword
													: '';
											str += '<div class="row"> <section>';
											str += '<label class="label">Keywords</label>';
											str += '<label class="input"><input type="text" class="brandKeyword" value="'
													+ request.keyword
													+ '" /></label>';
											str += '</section></div>';
											
											/*str += '<div class="row"> <section>';
											str += '<label class="label">Created By</label>';
											str += '<label class="input state-disabled""><input type="text" class="brandSyn" value="'
													+ request.created_by
													+ '" disabled></label>';
											str += '</section></div>';*/
											
											var modifiedOn = (request.modified_on) ? request.modified_on : '';
											var modifiedBy = (request.modified_by) ? request.modified_by : '';
											
											str += '<div class="row"> <section>';
											str += '<label class="label">Modified On</label>';
											str += '<label class="input state-disabled"><input disabled type="text" class="modifiedOn" value="'
													+ modifiedOn+ '"></label>';
											str += '</section></div>';
											
											str += '<div class="row"> <section>';
											str += '<label class="label">Modified By</label>';
											str += '<label class="input state-disabled"><input disabled type="text" class="modifiedBy" value="'
													+ modifiedBy + '"></label>';
											str += '</section></div>';

											str += '<div class="row"> <section>';
											str += '<label class="checkbox enable-checkbox"><input type="checkbox" '
													+ isChecked
													+ ' class="platformStatus" value="'
													+ request.status
													+ '"><i></i>Active</label>';
											str += '</section></div></form>';

											str += '<footer class="modal-footer">';
											str += '<button type="button" class="pull-right btn btn-warning btnPBUpdate" aria-hidden="true">Update</button>';
											str += '</footer>';

											$("#myModalLabel",
													"#myModalDrilldown").html(
													"");
											var modalTitle = (request.product_brand_family) ? request.product_brand_family
													: "";
											$("#myModalLabel",
													'#myModalDrilldown').html(
													"<div> Update : "
															+ modalTitle
															+ "</div>");
											$(".modal-dialog",
													"#myModalDrilldown")
													.addClass("width-50p");
											$(".modal-body",
													"#myModalDrilldown")
													.addClass(
															"no-padding-top no-padding-bottom");
											$('.modal-body').css("max-height",
													"50em");
											$("#poDocView").addClass(
													'smart-form').html(str);
											$('.platformSpecialty').val(
													request.platform_specialty);
											$('.productFamily').val(
													request.product_family);
											$('.productFamilySyn').val(
													request.family_synonyms);

											$('.platformSpecialty').select2();
											$('.productFamily').select2();
											$('.productFamilySyn').select2();

											$('#myModalDrilldown')
													.modal('show');
											$("#frmProductBrand").validate({
												rules : {
													productBrand : {
														required : true
													}
												},
												messages : {
													productBrand : {
														required : 'Please enter Brand Name'
													}
												},
												errorPlacement : function(error, element) {
													error.insertAfter(element.parent());
												}
											});
											$('.btnPBUpdate')
													.on(
															'click',
															function() {
																if (!$("#frmProductBrand").valid())
																	return;
																var active = $(
																		'.platformStatus')
																		.is(
																				':checked') ? 'Active'
																		: 'Inactive';
																var query = "update evidence_product_synomyms set keyword = '"
																		+ $(
																				'.brandKeyword')
																				.val()
																		+ "', modified_on = now(), modified_by = '"
																		+ $(
																				'#username')
																				.text()
																		+ "', status = '"
																		+ active
																		+ "', family_synonyms = '"
																		+ $(
																				'.productFamilySyn')
																				.select2(
																						'val')
																		+ "' , platform_specialty = '"
																		+ $(
																				'.platformSpecialty')
																				.select2(
																						'val')
																		+ "', product_family = '"
																		+ $(
																				'.productFamily')
																				.select2(
																						'val')
																		+ "', product_brand_family = '"
																		+ $(
																				'.productBrand')
																				.val()
																		+ "' , brand_synonyms = '"
																		+ $(
																				'.brandSyn')
																				.val()
																		+ "' where product_brand_family = '"
																		+ request.product_brand_family
																		+ "'";
																var updateRequest = {
																	"source" : 'postgreSQL',
																	"dataBase" : dataSettings.database,
																	"queries" : btoa(query)
																}
																callAjaxService(
																		"integraGenericUpdate",
																		function(
																				response) {
																			if (response
																					&& response.isException) {
																				if (response.message
																						.indexOf("ERROR: duplicate key") > -1) {
																					showNotification('error',
																							"This JNJ Brand already Exists");
																				} else {
																					showNotification('error',
																							response.customMessage);
																				}
																				return;
																			}
																			 $('#myModalDrilldown').modal('hide');
																			showNotification(
																					'success',
																					'JNJ Brand updated Successfully');
																			$(
																					"#applyFilter")
																					.trigger(
																							"click");
																		},
																		callBackFailure,
																		updateRequest,
																		"POST",
																		"json",
																		true);
															});

										}, callBackFailure, selectRequest,
										"POST", "json", true);

							}, callBackFailure, selectRequest, "POST", "json",
							true);
				}, callBackFailure, selectRequest, "POST", "json", true);

	}

	this.updateCompetitorBrand = function(obj) {
		if ($(obj).hasClass('audit-log') || $(obj).hasClass('checkbox')
				|| $(obj).hasClass('dt-body-center')) {
			return; // As Audit log is not required for drill down.
		}
		var request = JSON.parse($(obj).parent("tr").data("request"));
		var dataSettings = JSON.parse($(obj).parent("tr").data("dataSettings"));
		$("#myModalDrilldown").addClass('plain-modal-theme');
		var selectRequest = {
			"source" : 'postgreSQL',
			"dbName" : dataSettings.database,
			"tableName" : "evidence_platform_specialty where status = 'Active' order by platform_and_specialty",
			"columnName" : 'platform_and_specialty'
		}

		callAjaxService(
				"getIntegraCategories",
				function(response) {

					var str = '';
					var drpDwnStr = '';
					if(response.indexOf(request.platform_and_specialty) === -1) {
						response.push(request.platform_and_specialty);
					}
					response.map(function(a) {
						drpDwnStr += '<option value="' + a + '">' + a
								+ '</option>';
					});
					var isChecked = (request.status === 'Active') ? 'checked' : '';
					
					request.modified_by = request.modified_by? request.modified_by=request.modified_by: request.modified_by='';
					
					str += '<form id="frmAddComptr" class="" novalidate="novalidate"><div class="row"> <section>';
					str += '<label class="label">Platform & Specialty</label>';
					str += '<label class="select"><select class="platformSpecialty width-100per" placeholder="Select Platform & Specialty">'
							+ drpDwnStr + '</select></label>';
					str += '</section></div>';

					str += '<div class="row"> <section>';
					str += '<label class="label">Product </label>';
					str += '<label class="input"><input type="text" class="productName" name="productName" value="'
							+ request.product_name + '"></label>';
					str += '</section></div>';

					str += '<div class="row"> <section>';
					str += '<label class="label">Synonyms</label>';
					str += '<label class="input"><input type="text" class="competitorSyn" value="'
							+ request.synonyms + '"></label>';
					str += '</section></div>';

					str += '<div class="row"><section>';
					str += '<label class="label">Keywords</label>';
					str += '<label class="input"><input type="text" class="competitorTerm" value="'
							+ request.key_term + '"></label>';
					str += '</section></div>';

					str += '<div class="row"><section>';
					str += '<label class="label">Competitor</label>';
					str += '<label class="input"><input type="text" class="competitorName" name="competitorName" value="'
							+ request.competitor_name + '"></label>';
					str += '</section></div>';
					
					var modifiedOn = (request.modified_on) ? request.modified_on : '';
					var modifiedBy = (request.modified_by) ? request.modified_by : '';
					
					str += '<div class="row"> <section>';
					str += '<label class="label">Modified On</label>';
					str += '<label class="input state-disabled"><input disabled type="text" class="modifiedOn" value="'
							+ modifiedOn+ '"></label>';
					str += '</section></div>';
					
					str += '<div class="row"> <section>';
					str += '<label class="label">Modified By</label>';
					str += '<label class="input state-disabled"><input disabled type="text" class="modifiedBy" value="'
							+ modifiedBy + '"></label>';
					str += '</section></div>';

					str += '<div class="row"> <section>';
					str += '<label class="checkbox enable-checkbox"><input type="checkbox" '
							+ isChecked + ' class="platformStatus" value="'
							+ request.status + '"><i></i>Active</label>';
					str += '</section></div></form>';

					str += '<footer class="modal-footer">';
					str += '<button type="button" class="pull-right btn btn-warning btnPSUpdate"  aria-hidden="true">Update</button>';
					str += '</footer>';

					$("#myModalLabel", "#myModalDrilldown").html("");
					var modalTitle = (request.product_name) ? request.product_name
							: "";
					$("#myModalLabel", '#myModalDrilldown').html(
							"<div> Update : " + modalTitle + "</div>");
					$(".modal-dialog", "#myModalDrilldown").addClass(
							"width-50p");
					$(".modal-body", "#myModalDrilldown").addClass(
							"no-padding-top no-padding-bottom");
					$('.modal-body').css("max-height", "50em");
					$("#poDocView").addClass('smart-form').html(str);
					$('.platformSpecialty').val(request.platform_and_specialty);
					$('.platformSpecialty').select2();
					$('#myModalDrilldown').modal('show');
					$("#frmAddComptr").validate({
						rules : {
							productName : {
								required : true
							},
							competitorName : {
								required : true
							}
						},
						messages : {
							productName : {
								required : 'Please enter Product'
							},
							competitorName : {
								required : 'Please enter Competitor'
							}
						},
						errorPlacement : function(error, element) {
							error.insertAfter(element.parent());
						}
					});
					$('.btnPSUpdate')
							.on(
									'click',
									function() {
										if (!$("#frmAddComptr").valid())
											return;
										var active = $('.platformStatus').is(
												':checked') ? 'Active'
												: 'Inactive';
										var query = "update evidence_product_family_competitor set status = '"
												+ active
												+ "', product_name = '"
												+ $('.productName').val()
												+ "', modified_on = now(), modified_by = '"
												+ $('#username').text()
												+ "' , synonyms = '"
												+ $('.competitorSyn').val()
												+ "' , key_term = '"
												+ $('.competitorTerm').val()
												+ "', platform_and_specialty = '"
												+ $('.platformSpecialty')
														.select2('val')
												+ "' , competitor_name  = '"
												+ $('.competitorName').val()
												+ "' where product_name = '"
												+ request.product_name 
												+"' and competitor_name = '"
												+ request.competitor_name + "'";
										// pr_status =
										// '"+$('.platformStatus').val()

										var updateRequest = {
											"source" : 'postgreSQL',
											"dataBase" : dataSettings.database,
											"queries" : btoa(query)
										}
										callAjaxService(
												"integraGenericUpdate",
												function(response) {
													if (response
															&& response.isException) {
														if (response.message
																.indexOf("ERROR: duplicate key") > -1) {
															showNotification('error',
																	"This Competitor Product already Exists");
														} else {
															showNotification('error',
																	response.customMessage);
														}
														return;
													}
													$('#myModalDrilldown').modal('hide');
													showNotification('success',
															'Competitor Product updated Successfully');
													$("#applyFilter").trigger(
															"click");
												}, callBackFailure,
												updateRequest, "POST", "json",
												true);
									});
				}, callBackFailure, selectRequest, "POST", "json", true);

	}

	this.updateDocumentType = function(obj) {
		if ($(obj).hasClass('audit-log') || $(obj).hasClass('checkbox')
				|| $(obj).hasClass('dt-body-center')) {
			return; // As Audit log is not required for drill down.
		}
		var request = JSON.parse($(obj).parent("tr").data("request"));
		var dataSettings = JSON.parse($(obj).parent("tr").data("dataSettings"));
		
		$("#myModalDrilldown").addClass('plain-modal-theme');
		var str = '';
		var isChecked = (request.is_active === 'Active') ? 'checked' : '';
		var isComChecked = (request.commercial === 'Active') ? 'checked' : '';
		var isRdChecked = (request.rd === 'Active') ? 'checked' : '';
		request.last_updated_at = request.last_updated_at ? request.last_updated_at:''
		// str += '<div class="row"> <section>';
		// str += '<label class="label">ID </label>';
		// str += '<label class="input state-diabled"><input type="text"
		// class="docTypeId" value="'+request.doc_type_id+'" disabled></label>';
		// str += '</section></div>';

		str += '<form id="frmDocumentType" class="" novalidate="novalidate">';
		str += '<div class="row"> <section>';
		str += '<label class="label">Type</label>';
		str += '<label class="input"><input type="text" class="docTypeName" name="docTypeName" value="'
				+ request.doc_type_name + '"></label>';
		str += '</section></div>';
		
		
		
		/*str += '<div class="row"><section>';
		str += '<label class="label">Created By</label>';
		str += '<label class="input state-disabled"><input type="text" disabled class="createdBy" value="'
				+ request.created_by + '"></label>';
		str += '</section></div>';

		str += '<div class="row"> <section>';
		str += '<label class="label">Created On</label>';
		str += '<label class="input state-disabled"><input disabled type="text" class="createdAt" value="'
				+ request.created_at + '"></label>';
		str += '</section></div>';*/

		str += '<div class="row"> <section>';
		str += '<label class="label">Modified By</label>';
		str += '<label class="input state-disabled"><input type="text" disabled class="lastUpdatedBy" value="'
				+ request.last_updated_by + '"></label>';
		str += '</section></div>';

		str += '<div class="row"> <section>';
		str += '<label class="label">Modified On</label>';
		str += '<label class="input state-disabled"><input type="text" disabled class="lastUpdatedAt" value="'
				+ request.last_updated_at + '"></label>';
		str += '</section></div>';
		
		
		str += '<div class="row"><section>R&D <label class="checkbox"><input type="checkbox" class="isRdChecked" '
				+ isRdChecked + ' value="' + request.rd + '"><i></i></label>';
		str += '</section></div>';
		
		str += '<div class="row"><section>';
		str += 'Commercial Non Field<label class="checkbox"><input type="checkbox" class="isComChecked" '
				+ isComChecked
				+ ' value="'
				+ request.commercial
				+ '"><i></i></label>';
		str += '</section></div>';

		
		str += '<div class="row" style="display: none;"><section>';
		str += '<label class="checkbox enable-checkbox"><input type="checkbox" class="isActive" '
				+ isChecked
				+ ' value="'
				+ request.is_active
				+ '"><i></i>Active</label>';
		str += '</section></div></form>';

		str += '<footer class="modal-footer">';
		str += '<button type="button" class="pull-right btn btn-warning btnPSUpdate" aria-hidden="true">Update</button>';
		str += '</footer>';

		$("#myModalLabel", "#myModalDrilldown").html("");
		var modalTitle = (request.doc_type_name) ? request.doc_type_name : "";
		$("#myModalLabel", '#myModalDrilldown').html(
				"<div> Update : " + modalTitle + "</div>");
		$(".modal-dialog", "#myModalDrilldown").addClass("width-50p");
		$(".modal-body", "#myModalDrilldown").addClass(
				"no-padding-top no-padding-bottom");
		$('.modal-body').css("max-height", "50em");
		$("#poDocView").addClass('smart-form').html(str);
		$('#myModalDrilldown').modal('show');
		 $("#frmDocumentType").validate({
				rules : {
					docTypeName : {
						required : true
					}
				},
				messages : {
					docTypeName : {
						required : 'Please enter Type'
					}
				},
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
		$('.btnPSUpdate')
				.on(
						'click',
						function() {
							if (!$("#frmDocumentType").valid())
								return;
							var active = $('.isActive').is(':checked') ? 'Active'
									: 'Inactive';
							var isComChecked = $('.isComChecked')
									.is(':checked') ? 'Active' : 'Inactive';
							var isRdChecked = $('.isRdChecked').is(':checked') ? 'Active'
									: 'Inactive';

							if (!$('.docTypeName').val()) {
								showNotification('error',
										'Please enter Document Type');
							}

							var query = "update document_type set doc_type_name = '"
									+ $('.docTypeName').val()
									+ "' , commercial='"
									+ isComChecked
									+ "', rd = '"
									+ isRdChecked
									+ "', is_active = '"
									+ active
									+ "'  , last_updated_by  = '"
									+ $('#username').text()
									+ "' , last_updated_at  = now() where doc_type_id = '"
									+ request.doc_type_id + "'";
							// +"' , specialty_name =
							// '"+$('.platformSpecialty').val()+"' , pr_status =
							// '"+$('.platformStatus').val()

							var updateRequest = {
								"source" : 'postgreSQL',
								"dataBase" : dataSettings.database,
								"queries" : btoa(query)
							}
							callAjaxService(
									"integraGenericUpdate",
									function(response) {
										if (response && response.isException) {
											if (response.message
													.indexOf("ERROR: duplicate key") > -1) {
												showNotification('error',
														"This type already Exists");
											} else {
												showNotification('error',
														response.customMessage);
											}
											return;
										}
										$('#myModalDrilldown').modal('hide');
										showNotification('success',
												'Document Type updated Successfully');
										$("#applyFilter").trigger("click");
									}, callBackFailure, updateRequest, "POST",
									"json", true);
						});
	}

	this.updateDocumentSubtype = function(obj) {
		if ($(obj).hasClass('audit-log') || $(obj).hasClass('checkbox')
				|| $(obj).hasClass('dt-body-center')) {
			return; // As Audit log is not required for drill down.
		}
		var request = JSON.parse($(obj).parent("tr").data("request"));
		var dataSettings = JSON.parse($(obj).parent("tr").data("dataSettings"));
		var str = '';
		$("#myModalDrilldown").addClass('plain-modal-theme');
		var isChecked = (request.is_active === 'Active') ? 'checked' : '';

		var str = '';
		str += '<form>';
		str += '<div class="row"> <section>';
		str += '<label class="label">ID </label>';
		str += '<label class="input state-disabled"><input type="text" class="docSubTypeId" value="'
				+ request.doc_subtype_id + '" disabled></label>';
		str += '</section></div>';

		str += '<div class="row"> <section>';
		str += '<label class="label">Name</label>';
		str += '<label class="input"><input type="text" class="docSubTypeName" value="'
				+ request.doc_subtype_name + '"></label>';
		str += '</section></div>';
		
		// str += '<div class="row"><section>';
		// str += '<label class="label">Status</label>';
		// str += '<label class="input"><input type="text" class="isActive"
		// value="'+request.is_active+'"></label>';
		// str += '</section></div>';

		str += '<div class="row"><section>';
		str += '<label class="label">Created By</label>';
		str += '<label class="input"><input type="text" class="createdBy" value="'
				+ request.created_by + '"></label>';
		str += '</section></div>';

		str += '<div class="row"> <section>';
		str += '<label class="label">Created On</label>';
		str += '<label class="input"><input type="text" class="createdAt" value="'
				+ request.created_at + '"></label>';
		str += '</section></div>';

		str += '<div class="row"> <section>';
		str += '<label class="label">Last Updated By</label>';
		str += '<label class="input"><input type="text" class="lastUpdatedBy" value="'
				+ request.last_updated_by + '"></label>';
		str += '</section></div>';

		str += '<div class="row"> <section>';
		str += '<label class="label">Updated On</label>';
		str += '<label class="input"><input type="text" class="lastUpdatedAt" value="'
				+ request.last_updated_at + '"></label>';
		str += '</section></div>';
		
		str += '<div class="row"><section>';
		str += '<label class="checkbox enable-checkbox"><input type="checkbox" class="isActive" '
				+ isChecked
				+ ' value="'
				+ request.is_active
				+ '"><i></i>Active</label>';
		str += '</section></div>';
		
		str += '</form>';
		
		str += '<footer class="modal-footer">';
		str += '<button type="button" class="pull-right btn btn-warning btnPSUpdate" data-dismiss="modal" aria-hidden="true">Update</button>';
		str += '</footer>';

		$("#myModalLabel", "#myModalDrilldown").html("");
		var modalTitle = (request.doc_subtype_name) ? request.doc_subtype_name
				: "";
		$("#myModalLabel", '#myModalDrilldown').html(
				"<div> Update : " + modalTitle + "</div>");
		$(".modal-dialog", "#myModalDrilldown").addClass("width-50p");
		$(".modal-body", "#myModalDrilldown").addClass(
				"no-padding-top no-padding-bottom");
		$('.modal-body').css("max-height", "50em");
		$("#poDocView").addClass('smart-form').html(str);
		$('#myModalDrilldown').modal('show');

		$('.btnPSUpdate')
				.on(
						'click',
						function() {
							var active = $('.isActive').is(':checked') ? 'Active'
									: 'Inactive';
							var query = "update document_subtype set "// doc_subtype_id
							// =
									// '"+$('docSubTypeId').val()
									+ "  doc_subtype_name = '"
									+ $('.docSubTypeName').val()
									+ "' , is_active = '"
									+ active
									+ "' , last_updated_by  = '"
									+ $('#username').text()
									+ "' , last_updated_at  = now() where doc_subtype_id = '"
									+ request.doc_subtype_id + "'";

							var updateRequest = {
								"source" : 'postgreSQL',
								"dataBase" : dataSettings.database,
								"queries" : btoa(query)
							}
							callAjaxService(
									"integraGenericUpdate",
									function(response) {
										if (response && response.isException) {
											if (response.message
													.indexOf("ERROR: duplicate key") > -1) {
												showNotification('error',
														"This Document Subtype already Exists");
											} else {
												showNotification('error',
														response.customMessage);
											}
											return;
										}
										showNotification('success',
												'Document Subtype updated Successfully');
										$("#applyFilter").trigger("click");
									}, callBackFailure, updateRequest, "POST",
									"json", true);
						});
	}

	this.updateDocumentClassification = function(obj) {
		if ($(obj).hasClass('audit-log') || $(obj).hasClass('checkbox')
				|| $(obj).hasClass('dt-body-center')) {
			return; // As Audit log is not required for drill down.
		}
		var request = JSON.parse($(obj).parent("tr").data("request"));
		var dataSettings = JSON.parse($(obj).parent("tr").data("dataSettings"));
		$("#myModalDrilldown").addClass('plain-modal-theme');
		var str = '';
		var isChecked = (request.is_active === 'Active') ? 'checked' : '';
		str += '<form>';
		str += '<div class="row"> <section>';
		str += '<label class="label">ID </label>';
		str += '<label class="input state-disabled"><input type="text" class="docClassificationId" value="'
				+ request.doc_classification_id + '" disabled></label>';
		str += '</section></div>';

		str += '<div class="row"> <section>';
		str += '<label class="label">Name</label>';
		str += '<label class="input"><input type="text" class="docClassificationName" value="'
				+ request.doc_classification_name + '"></label>';
		str += '</section></div>';
		
		// str += '<div class="row"><section>';
		// str += '<label class="label">Status</label>';
		// str += '<label class="input"><input type="text" class="isActive"
		// value="'+request.is_active+'"></label>';
		// str += '</section></div>';

		str += '<div class="row"><section>';
		str += '<label class="label">Created By</label>';
		str += '<label class="input"><input type="text" class="createdBy" value="'
				+ request.created_by + '"></label>';
		str += '</section></div>';

		str += '<div class="row"> <section>';
		str += '<label class="label">Created On</label>';
		str += '<label class="input"><input type="text" class="createdAt" value="'
				+ request.created_at + '"></label>';
		str += '</section></div>';

		str += '<div class="row"> <section>';
		str += '<label class="label">Last Updated By</label>';
		str += '<label class="input"><input type="text" class="lastUpdatedBy" value="'
				+ request.last_updated_by + '"></label>';
		str += '</section></div>';

		str += '<div class="row"> <section>';
		str += '<label class="label">Updated On</label>';
		str += '<label class="input"><input type="text" class="lastUpdatedAt" value="'
				+ request.last_updated_at + '"></label>';
		str += '</section></div>';
		
		str += '<div class="row"><section>';
		str += '<label class="checkbox enable-checkbox"><input type="checkbox" class="isActive" '
				+ isChecked
				+ ' value="'
				+ request.is_active
				+ '"><i></i>Active</label>';
		str += '</section></div>';
		str += '</form>';

		str += '<footer class="modal-footer">';
		str += '<button type="button" class="pull-right btn btn-warning btnPSUpdate" data-dismiss="modal" aria-hidden="true">Update</button>';
		str += '</footer>';

		$("#myModalLabel", "#myModalDrilldown").html("");
		var modalTitle = (request.doc_classification_name) ? request.doc_classification_name
				: "";
		$("#myModalLabel", '#myModalDrilldown').html(
				"<div> Update : " + modalTitle + "</div>");
		$(".modal-dialog", "#myModalDrilldown").addClass("width-50p");
		$(".modal-body", "#myModalDrilldown").addClass(
				"no-padding-top no-padding-bottom");
		$('.modal-body').css("max-height", "50em");
		$("#poDocView").addClass('smart-form').html(str);
		$('#myModalDrilldown').modal('show');

		$('.btnPSUpdate')
				.on(
						'click',
						function() {
							var active = $('.isActive').is(':checked') ? 'Active'
									: 'Inactive';
							var query = "update document_classification set "// doc_subtype_id
							// =
									// '"+$('docSubTypeId').val()
									+ "  doc_classification_name = '"
									+ $('.docClassificationName').val()
									+ "' , is_active = '"
									+ active
									+ "' , last_updated_by  = '"
									+ $('#username').text()
									+ "' , last_updated_at  = now() where doc_classification_id = '"
									+ request.doc_classification_id + "'";
							var updateRequest = {
								"source" : 'postgreSQL',
								"dataBase" : dataSettings.database,
								"queries" : btoa(query)
							}
							callAjaxService(
									"integraGenericUpdate",
									function(response) {
										if (response && response.isException) {
											if (response.message
													.indexOf("ERROR: duplicate key") > -1) {
												showNotification('error',
														"This Document Classification already Exists");
											} else {
												showNotification('error',
														response.customMessage);
											}
											return;
										}
										showNotification('success',
												'Document Classification updated Successfully');
										$("#applyFilter").trigger("click");
									}, callBackFailure, updateRequest, "POST",
									"json", true);
						});
	}

	this.updateFranchise = function(obj) {
		if ($(obj).hasClass('audit-log') || $(obj).hasClass('checkbox')
				|| $(obj).hasClass('dt-body-center')) {
			return; // As Audit log is not required for drill down.
		}
		var request = JSON.parse($(obj).parent("tr").data("request"));
		var dataSettings = JSON.parse($(obj).parent("tr").data("dataSettings"));
		$("#myModalDrilldown").addClass('plain-modal-theme');
		
		var psDrpdwnStr = '', selectRequest = {
			"source" : 'postgreSQL',
			"dbName" : 'EVDSEARCH',
			"tableName" : "evidence_platform_specialty where status = 'Active' order by platform_and_specialty",
			"columnName" : 'platform_and_specialty'
		};
		callAjaxService("getIntegraCategories",function(response){
			if(response && response.isException) {
				showNotification('error', response.customMessage);
				return;
			}
			response.map(function(a) {
				psDrpdwnStr += '<option value="'+a+'">'+a+'</option>';
			});
			var str = '';
			var isChecked = (request.status === 'Active') ? 'checked' : '';
			request.last_updated_at = request.last_updated_at?request.last_updated_at:'';
			str += '<form id="frmFranchiseUpdate" class="" novalidate="novalidate"><div class="row"> <section>';
			str += '<label class="label">JNJ Franchise</label>';
			str += '<label class="input"><input type="text" class="franchise" name="franchise" value="'
					+ request.franchise + '"></label>';
			str += '</section></div>';
			
			str += '<div class="row"> <section>';
			str += '<label class="label">JNJ Company Name</label>';
			str += '<label class="input"><input type="text" class="businessUnit" name="businessUnit" value="'
					+ request.business_unit + '"></label>';
			str += '</section></div>';

			str += '<div class="row"> <section>';
			str += '<label class="label">JNJ Company Synonyms</label>';
			str += '<label class="input"><input type="text" class="synonyms" value="'
					+ request.synonyms + '"></label>';
			str += '</section></div>';

			str += '<div class="row"> <section>';
			str += '<label class="label">Platform And Specialty</label>';
			str += '<label class="select"><select class="width-100per platformSpecialty" placeholder="Select Platform & Specialty">'+psDrpdwnStr+'</select></label>';
			/*str += '<label class="input"><input type="text" class="platformSpecialty" name="platformSpecialty" value="'
					+ request.platform_and_specialty + '"></label>';*/
			str += '</section></div>';

			
			//			
			// str += '<div class="row"><section>';
			// str += '<label class="label">Created By</label>';
			// str += '<label class="input"><input type="text" class="createdBy"
			// value="'+request.created_by+'"></label>';
			// str += '</section></div>';
			//
			// str += '<div class="row"> <section>';
			// str += '<label class="label">Created On</label>';
			// str += '<label class="input"><input type="text" class="createdAt"
			// value="'+request.created_at+'"></label>';
			// str += '</section></div>';
			//
			str += '<div class="row"> <section>';
			str += '<label class="label">Modified By</label>';
			str += '<label class="input state-disabled"><input type="text" disabled class="lastUpdatedBy state-disabled" value="'
					+ request.last_updated_by + '"></label>';
			str += '</section></div>';

			str += '<div class="row"> <section>';
			str += '<label class="label">Modified On</label>';
			str += '<label class="input state-disabled"><input type="text" disabled class="lastUpdatedAt" value="'
					+ request.last_updated_at + '"></label>';
			str += '</section></div>';
			
			str += '<div class="row"><section>';
			str += '<label class="checkbox enable-checkbox"><input type="checkbox" class="status" '
					+ isChecked + ' value="' + request.status + '"><i></i>Active</label>';
			str += '</section></div></form>';
			
			str += '<footer class="modal-footer">';
			str += '<button type="button" class="pull-right btn btn-warning btnPSUpdate" aria-hidden="true">Update</button>';
			str += '</footer>';

			$("#myModalLabel", "#myModalDrilldown").html("");
			var modalTitle = (request.franchise) ? request.franchise : "";
			$("#myModalLabel", '#myModalDrilldown').html(
					"<div> Update : " + modalTitle + "</div>");
			$(".modal-dialog", "#myModalDrilldown").addClass("width-50p");
			$(".modal-body", "#myModalDrilldown").addClass(
					"no-padding-top no-padding-bottom");
			$('.modal-body').css("max-height", "50em");
			$("#poDocView").addClass('smart-form').html(str);

			$('.platformSpecialty').val(request.platform_and_specialty);
			$('.platformSpecialty').select2();
			
			$('#myModalDrilldown').modal('show');
			//constructSelect2("[name='franchise']","select distinct franchise,'' from evidence_franchise where lower(franchise) like '%<Q>%'",false, {id:request.franchise,text:request.franchise}, null,null, "Select ","postgreSQL", "EVDSEARCH");
			$("#frmFranchiseUpdate").validate({
				rules : {
					franchise : {
						required : true
					},
					businessUnit : {
						required : true
					},
					platformSpecialty : {
						required : true
					}
				},
				messages : {
					franchise : {
						required : 'Please enter JNJ Franchise'
					},
					businessUnit : {
						required : 'Please enter JNJ Company Name'
					},
					platformSpecialty : {
						required : 'Please enter Platform & Specialty'
					}
				},
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
			$('.btnPSUpdate') .on( 'click', function() {
				
				if (!$("#frmFranchiseUpdate").valid())
					return;
				var franchise = $("[name='franchise']").val();
				franchise = franchise.trim();
				var active = $('.status').is(':checked') ? 'Active'
						: 'Inactive';
				var query = "update evidence_franchise set "
						+ " franchise = '"
						+ franchise
						+ " ',business_unit = '"
						+ $('.businessUnit').val()
						+ "',synonyms = '"
						+ $('.synonyms').val()
						+ "',platform_and_specialty = '"
						+ $(".platformSpecialty").select2('val')
						+ "' , status = '"
						+ active
						+ "' , last_updated_by  = '"
						+ $('#username').text()
						+ "' , last_updated_at  = now() where franchise = '"
						+ request.franchise + "'";
				var updateRequest = {
					"source" : 'postgreSQL',
					"dataBase" : dataSettings.database,
					"queries" : btoa(query)
				}
				callAjaxService("integraGenericUpdate", function(
						response) {
					if (response && response.isException) {
						if (response.message
								.indexOf("ERROR: duplicate key") > -1) {
							showNotification('error',
									"This Franchise already Exists");
						} else {
							showNotification('error',
									response.customMessage);
						}
						return;
					}
					$('#myModalDrilldown').modal('hide');
					showNotification('success',
							'Franchise updated Successfully');
					dataAnalyser.profiling.grapher.dataTable[0].ajax.reload();
				}, callBackFailure, updateRequest, "POST", "json",
						true);
			});
		}, callBackFailure, selectRequest, "POST", "json", true);
		
	}

	this.getUser = function(obj) {
		if ($(obj).hasClass('audit-log') || $(obj).hasClass('checkbox')
				|| $(obj).hasClass('dt-body-center')) {
			return; // As Audit log is not required for drill down.
		}
		
		var selectRequest = {
	       "source" : 'postgreSQL',
	       "dbName" : 'EVDSEARCH',
	       "tableName" : "evidence_platform_specialty where status = 'Active' order by platform_and_specialty",
	       "columnName" : 'platform_and_specialty'
	     };
		callAjaxService("getIntegraCategories",function(response){
			if(response && response.isException) {
				showNotification('error', response.customMessage);
				return;
			}
			
			var optStr = '';
			response.map(function(a) {
				optStr += '<option value="'+a+'">'+a+'</option>';
			});
			
			$("#myModalDrilldown").addClass('plain-modal-theme');
			var req = JSON.parse($(obj).parent("tr").data("request"));
			var additionalDetails = {
				"title" : req.title,
				"commonName" : req.common_name || '',
				"companyName" : req.company_name || '',
				"department" : req.department || '',
				"username" : req.username || '',
				"statusRemarks" : req.status_remarks || '',
				"firstName" : req.common_name || ''
			};
			var request = {
				"username" : req.username || '',
				"businessrole" : req.business_role || '',
				"firstname" : req.common_name || '',
				"emailId" : req.emailid || '',
				"enabled" : req.enabled || '0',
				"region" : req.region || '',
				"status" : req.status || '',
				"remarks" : req.remarks || '',
				"speciality" : req.speciality || ''
			};
			var isPending = (req.status === 'Pending') ? true : false;
			var enabledAttr = (req.enabled == '1') ? 'checked' : (isPending) ? 'checked' : '';
			var disabledText = (request.status === 'Pending' || request.status === 'Approved') ? "" : "disabled";
			var disabledBusinessFunction = (request.status === 'Rejected') ? "disabled" : "";
			var disabledBusinessFunctionDrpdwn = (request.status === 'Rejected') ? "state-disabled" : "";
			var str = '';
			str += '<div class="row"> <section class="col col-6">';
			str += '<label class="label">Active Directory ID</label>';
			str += '<label class="input state-disabled"><input type="text" value="'
					+ request.username + '" disabled="disabled"></label>';
			str += '</section>';
			str += '<section class="col col-6" style="width: 48%">';
			str += '<label class="label">Email Address</label>';
			str += '<label class="input state-disabled"><input type="text" value="'
					+ request.emailId
					+ '" id="txtUsrEmailId" disabled="disabled" /></label>';
			str += '</section></div>';
			str += '<div class="row"> <section class="col col-6">';
			str += '<label class="label">Common Name</label>';
			str += '<label class="input state-disabled"><input value="'
					+ additionalDetails.commonName
					+ '" type="text" class="txtUsrCmnNme" disabled="disabled" /></label>';
			str += '</section>';
			str += '<section class="col col-6" style="width: 48%">';
			str += '<label class="label">Title</label>';
			str += '<label class="input state-disabled"><input value="'
					+ additionalDetails.title
					+ '" type="text" class="txtUsrTitle" disabled="disabled" /></label>';
			str += '</section></div>';
			str += '<div class="row" style="border-bottom : 1px dahsed"><section class="col col-6">';
			str += '<label class="label">Department</label>';
			str += '<label class="input state-disabled"><input value="'
					+ additionalDetails.department
					+ '" type="text" class="txtUsrDept" disabled="disabled" /></label>';
			str += '</section>';
			str += '<section class="col col-6" style="width: 48%">';
			str += '<label class="label">Company Name</label>';
			str += '<label class="input state-disabled"><input type="text" value="'
					+ additionalDetails.companyName
					+ '" class="txtUsrCpyNme" disabled="disabled" /></label>';
			str += '</section></div>';
			str += '<section class="dropDown-field odd">';
			str += '<label class="label">Business Function</label>';
			str += '<label class="select '+disabledBusinessFunctionDrpdwn+'"><select '+disabledBusinessFunction+' class="txtUsrBrole width-100per" value="'
					+ request.businessrole
					+ '"><option value="Commercial Non Field">Commercial Non Field</option><option value="R&D">R&D</option></select></label>';
			str += '</section>';
			str += '<section class="dropDown-field even">';
			str += '<label class="label">Region</label>';
			str += '<label class="select  state-disabled"><select '
					+ disabledText
					+ ' disabled class="txtUsrRgn width-100per" value="'
					+ request.region
					+ '"><option value="ASPAC">ASPAC</option><option value="LATAM">LATAM</option><option value="EMEA">EMEA</option><option value="NA">NA</option></select></label>';
			str += '</section>';
			str += '<section class="dropDown-field odd">';
			str += '<label class="label">Platform & Specialty</label>';
			str += '<label class="select  state-disabled"><select '
					+ disabledText
					+ ' disabled placeholder="Select Platform & Specialty" multiple class="txtUsrSpeciality width-100per" value="'
					+ request.speciality + '">' + optStr
					+ '</select></label>';
			str += '</section>';
			if (request.status !== 'Approved') {
				str += '<section class="dropDown-field even">';
				str += '<label class="label">Rejection Reason</label>';
				str += '<label class="select"><select '
						+ disabledText
						+ ' placeholder="Select Rejection Remarks" class="txtUsrRmks width-100per" value="'
						+ request.remarks + '">'
						+ lookUpTable.get('evdRejectionFields')
						+ '</select></label>';
				str += '</section>';
			}
			str += '<section>';
			str += '<label class="label">Remarks</label>';
			str += '<label class="textarea"><textarea ' + disabledText
					+ ' class="txtRmks">' + additionalDetails.statusRemarks
					+ '</textarea></label>';
			str += '</section>';
			str += '<section>';
			str += '<label class="checkbox enable-checkbox"><input type="checkbox" ' + disabledText
					+ ' class="txtUsrEnabled" ' + enabledAttr
					+ '/><i></i>Enabled</label>';
			str += '</section>';
			str += '<footer class="row modal-footer">';
			if (request.status !== 'Pending') {
				str += '<button type="button" class="btn btn-warning pull-right" data-dismiss="modal" id="btnUsrUpdate" aria-hidden="true">Update</button>';
			}
			if (request.status === 'Pending') {
				str += '<button type="submit" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="btnUsrReject" id="btnReject">Reject</button>';
				str += '<button type="submit" class="btn btn-success" data-dismiss="modal" aria-hidden="true" id="btnUsrApprove" id="btnApprove">Approve</button>';
			}
			str += '</footer>';
			$("#myModalDrilldown").addClass('plain-modal-theme');
			$("#myModalLabel", "#myModalDrilldown").html("");
			var labelStr = '';
			if (request.status === 'Pending') {
				labelStr = '<span class="badge margin-right-13" style="background-color: orange">Pending</span>';
			} else if (request.status === 'Approved') {
				labelStr = '<span class="badge margin-right-13" style="background-color: #96bf48">Approved</span>';
			} else if (request.status === 'Rejected') {
				labelStr = '<span class="badge margin-right-13" style="background-color: #DB4A67">Rejected</span>';
			}

			var modalTitle = (additionalDetails.commonName) ? additionalDetails.commonName
					: request.username;
			$("#myModalLabel", '#myModalDrilldown').html(
					"<div> " + modalTitle + " " + labelStr + " </div>");
			$(".modal-dialog", "#myModalDrilldown").addClass("width-50p");
			$(".modal-body", "#myModalDrilldown").addClass(
					"no-padding-top no-padding-bottom");
			$("#poDocView").addClass('smart-form').html(str);
			$('#myModalDrilldown').modal('show');

			$('.txtUsrRgn').val(request.region);
			$('.txtUsrBrole').val(request.businessrole);
			$('.txtUsrTitle').val(additionalDetails.title);
			$('.txtUsrSpeciality').val(
					(request.speciality) ? request.speciality.split(',') : []);
			$('.txtUsrRmks').val(request.remarks);

			$('.txtUsrRgn').select2();
			$('.txtUsrBrole').select2();
			$('.txtUsrSpeciality').select2();
			$('.txtUsrRmks').select2();

			$('#btnUsrUpdate').on('click', function() {
				request = getUserFields(request, additionalDetails);
				callAjaxService("UpdateUser", function(response) {
					callbackSucessUpdateUser(response, null, null);
				}, callBackFailure, request, "POST");
			});

			$('#btnUsrApprove').on(
					'click',
					function() {
						request = getUserFields(request, additionalDetails);
						request.status = 'Approved';
						callAjaxService("UpdateUser", function(response) {
							callbackSucessUpdateUser(response, 'Approved',
									request.emailId, null,
									$('.txtUsrCmnNme').val(), $('.txtUsrBrole')
											.select2('val'));
						}, callBackFailure, request, "POST");
					});

			$('#btnUsrReject').on(
					'click',
					function() {
						if (!$('.txtUsrRmks').select2('val')) {
							showNotification('error',
									'Please enter remarks to Reject the user');
							return false;
						}
						request = getUserFields(request, additionalDetails);
						request.status = 'Rejected';
						callAjaxService("UpdateUser", function(response) {
							callbackSucessUpdateUser(response, 'Rejected',
									request.emailId, $('.txtUsrRmks')
											.select2('val'), $('.txtUsrCmnNme')
											.val(), $('.txtUsrBrole')
											.select2('val'));
						}, callBackFailure, request, "POST");
					});
		}, callBackFailure, selectRequest, "POST", "json", true);
	}

	function getUserFields(req, additional) {
		additional.commonName = $('.txtUsrCmnNme').val() || "";
		additional.companyName = $('.txtUsrCpyNme').val() || "";
		additional.department = $('.txtUsrDept').val() || "";
		additional.title = $('.txtUsrTitle').val() || "";
		additional.speciality = $('.txtUsrSpeciality').select2('val');
		additional.speciality = (additional.speciality) ? additional.speciality
				.join(',') : '';
		additional.statusRemarks = $('.txtRmks').val() || "";

		req.businessrole = $('.txtUsrBrole').select2('val') || "";
		req.region = $('.txtUsrRgn').select2('val') || "";
		req.remarks = ($('.txtUsrRmks').length > 0 && $('.txtUsrRmks').select2(
				'val')) ? $('.txtUsrRmks').select2('val') : "";
		req.enabled = $('.txtUsrEnabled').is(':checked') ? '1' : '0';
		req["menuontop"] = '0';
		req["roles"] = "";
		req["firstname"] = additional.commonName;
		req["lastname"] = "";
		req["timezone"] = "";
		req["datename"] = "";
		req["password"] = req.username;

		if (req.businessrole === 'System Admin') {
			req["roles"] = "ROLE_Evidence_Governance,ROLE_Evidence_Admin,ROLE_Evidence";
			req["primaryrole"] = "ROLE_Evidence_Admin";
		} else if (req.businessrole === 'Business Admin') {
			req["roles"] = "ROLE_Evidence_Governance,ROLE_Evidence";
			req["primaryrole"] = "ROLE_Evidence_Governance";
		} else {
			req["roles"] = "ROLE_Evidence";
			req["primaryrole"] = "ROLE_Evidence";
		}
		
		for(var k in req) {
			if(typeof req[k] === 'string' && req[k]) {
				req[k] = req[k].replaceAll("'", "''");
			}
		}
		for(var k in additional) {
			if(typeof additional[k] === 'string' && additional[k]) {
				additional[k] = additional[k].replaceAll("'", "''");
			}
		}
		
		req['additionalDetails'] = JSON.stringify(additional);
		return req;
	}

	function callbackSucessUpdateUser(response, message, emailId, remarks,
			userName, businessRole) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			showNotification("success", "User updated Sucessfully");
			if (message && emailId) {
				var emailContent = (message === 'Approved') ? lookUpTable
						.get("evdApprovedMail") : lookUpTable
						.get("evdRejectedMail");
				var subject = (message === 'Approved') ? lookUpTable
						.get("evdApprovedSubject") : lookUpTable
						.get("evdRejectedSubject");
				emailContent = emailContent.replaceAll("{{rejectRemarks}}",
						remarks);
				emailContent = emailContent
						.replaceAll("{{userName}}", userName);
				emailContent = emailContent.replaceAll("{{businessRole}}",
						businessRole);

				$.post(directoryName + "/rest/email/sendGenericEmail", {
					"from" : "MDEvidencePortal@its.jnj.com",
					"to" : emailId,
					"subject" : subject,
					"content" : emailContent
				}, function() {
				});
			}
			$("#applyFilter").trigger("click");
		}
	}

	this.mdmDrillDownCallBack = function(obj) {
		if ($(obj).hasClass('audit-log')) {
			return; // As Audit log is not required for drill down.
		}
		var req = JSON.parse($(obj).parent("tr").data("request"));
		var columnSettings = JSON.parse($(obj).parent("tr").data(
				"columnSettings"));
		var dataSettings = JSON.parse($(obj).parent("tr").data("dataSettings"));
		var pKeys = $(obj).parents("tr").data("primarykeys");
		if (pKeys !== null && !$.isEmptyObject(pKeys)) {
			pKeys = JSON.parse(pKeys);
		} else {
			showNotification("error", "Primary Key not configured");
			return;
		}
		var primaryKey = '';
		var primaryColumn = '';
		var primaryKeys = [];
		var primaryColumns = [];
		var filter = '';
		var columns = [];
		var displayColumns = [];
		var columnConfigurations = [];
		var title = '';
		var rule = {};
		if (dataSettings.rule) {
			rule = dataSettings.rule;
		}
		columnSettings.map(function(o) {
			if (!o.data) {
				return false;
			}
			if (o.primarykey) {
				primaryColumn = o.data;
				primaryKey = req[primaryColumn] || '';
				primaryKeys.push(req[primaryColumn] || '');
				primaryColumns.push(o.data);
				if (req[primaryColumn]) {
					if (filter) {
						filter += " AND 1=1 AND LOWER(COALESCE(NULLIF(CAST("
								+ o.data + " AS text ),''),'')) = '"
								+ req[primaryColumn].toLowerCase() + "'";
					} else {
						filter += " 1=1 AND LOWER(COALESCE(NULLIF(CAST("
								+ o.data + " AS text ),''),'')) = '"
								+ req[primaryColumn].toLowerCase() + "'";
					}
				}
			}
			if (dataSettings.pivotSettings
					&& dataSettings.pivotSettings.headerCols
					&& o.data === dataSettings.pivotSettings.headerCols) {
				title = '' + o.title + ' : ' + req[o.data];
			}
			columns.push(o.data);
			if (o.bVisible) {
				displayColumns[o.data] = o.data;
			}
		});
		columns.map(function(a) {
			columnConfigurations.push({
				'columnName' : a
			});
		});
		var request = [ {
			"source" : dataSettings.source,
			"database" : dataSettings.database,
			"table" : dataSettings.table,
			"columns" : columns.join(','),
			"order[0][column]" : "0",
			"order[0][dir]" : "asc",
			"start" : '0',
			"length" : '10',
			"search[value]" : null,
			"search[regex]" : "false",
			"filter" : filter,
			"columnConfigurations" : JSON.stringify(columnConfigurations)
		} ];
		if (dataSettings.pivotSettings && dataSettings.pivotSettings.tables
				&& dataSettings.pivotSettings.tables.length > 0) {
			dataSettings.pivotSettings.tables
					.map(function(table) {
						var cols = table.columns;
						var colConfigs = [];
						var fltrs = '';
						var tableReq = {
							"source" : dataSettings.source,
							"database" : dataSettings.database,
							"table" : table.table,
							"columns" : table.columns.join(','),
							"order[0][column]" : "0",
							"order[0][dir]" : "asc",
							"start" : '0',
							"length" : '10',
							"search[value]" : null,
							"search[regex]" : "false",
							"filter" : "",
							"columnConfigurations" : ""
						};
						if (cols && cols.length > 0) {
							cols.map(function(c) {
								colConfigs.push({
									'columnName' : c
								});
							});
							tableReq.columnConfigurations = JSON
									.stringify(colConfigs);
							if (table.pKey && table.pKey.length > 0) {
								table.pKey
										.map(function(a) {
											if (req[a]) {
												if (fltrs) {
													fltrs += " AND 1=1 AND LOWER(COALESCE(NULLIF(CAST("
															+ a
															+ " AS text ),''),'')) = '"
															+ req[a]
																	.toLowerCase()
															+ "'";
												} else {
													fltrs += " 1=1 AND LOWER(COALESCE(NULLIF(CAST("
															+ a
															+ " AS text ),''),'')) = '"
															+ req[a]
																	.toLowerCase()
															+ "'";
												}
											}
										});
								tableReq.filter = fltrs;
							}
							request.push(tableReq);
						}
					})
		}
		if (!title) {
			title = "" + primaryColumn + " : " + primaryKey;
		}
		enableLoading();
		callAjaxService("getMultipleDataPagination", function(response) {
			disableLoading();
			callBackGetDataPagination(response, dataSettings.pivotSettings,
					columns, title, pKeys, dataSettings, req, columnSettings,
					rule);
		}, callBackFailure, {
			"profiles" : JSON.stringify(request)
		}, "POST");
	};

	function callBackGetDataPagination(response, pivotSettings, columns, title,
			primaryKeySettings, dataSettings, selectedRowData, columnSettings,
			rule) {
		if (pivotSettings && response && Object.keys(response).length > 0) {
			$('#modalDrilldownLabel').html(title);
			$("#modalDrilldown-additionalFeature").empty();
			var workFlowStr = '<div style="display: flex;" class="row"><div class="col-sm-8 margin-10" id="workFlowContent"></div><div class="col-sm-2" style="display: none;" id="workFlowButton"></div><div class="col-sm-4" id="workflow-log"><span class="pull-right" style="line-height: 5em;"></span></div></div>';
			$('#modalDrilldown-additionalFeature').append(workFlowStr);
			if ($('.rule-run').length > 0) {
				$('.rule-run').remove();
			}
			if (rule && Object.keys(rule).length > 0 && rule.isRule) {
				var data = selectedRowData[rule.column];
				var link = rule.ruleLink
						|| '/SPECTRA/rest/rule-engine/runrules';
				var project = rule.ruleProject
						|| localStorage.getItem('lastusedapp').replace('ROLE_',
								'');
				var ruleStr = '<a class="btn btn-info rule-run" data-project="'
						+ project
						+ '" data-link="'
						+ link
						+ '" data-id="'
						+ data
						+ '" href="javascript:void(0);" title="Rule Rule"> <i class="fa fa-lg fa-tasks"></i> Run Rule</a>';
				$('#modalDrilldownExtras').prepend(ruleStr);
				$('.rule-run')
						.on(
								'click',
								function() {
									var $preparingFileModal = $("#preparing-rule-modal");
									var r = confirm(lookUpTable
											.get('mdmRunRuleConfirm') ? lookUpTable
											.get('mdmRunRuleConfirm')
											: 'Are you sure to run rules?');
									if (r) {
										if ($(this).attr('data-link')
												&& $(this).attr('data-id')
												&& $(this).attr('data-project')) {
											var request = [ {
												ids : $(this).attr('data-id'),
												project : $(this).attr(
														'data-project')
											} ];
											$preparingFileModal.dialog({
												modal : true
											});
											callAjaxServices(
													[ $(this).attr('data-link') ],
													function(response) {
														if (response
																&& response.length > 0
																&& response[0].isException) {
															showNotification(
																	'error',
																	response[0].customMessage);
														}
														$preparingFileModal
																.dialog('close');
														if ($('#goBack').length > 0) {
															$('#goBack')
																	.trigger(
																			'click');
														}
														if ($("#applyFilter").length > 0) {
															$("#applyFilter")
																	.trigger(
																			"click")
														}
													}, callBackFailure,
													request, "POST");
										}
									} else {
										$preparingFileModal.dialog('close');
									}
								})
			}

			var pivotColumn = pivotSettings.column;
			var processedData = [];
			var pivotElements = [];
			var multiTableKey = '__pvt_pivotal_Multi_tbl_Nme';
			var height = $(window).height() - 50 + "px";
			var combinedTableResponse = [];
			for ( var i in response) {
				var arr = response[i];
				arr = Jsonparse(arr);
				if (arr.data) {
					var dta = JSON.parse(arr.data);
					if (pivotSettings.multiTable) {
						dta.map(function(o) {
							o[multiTableKey] = i;
						})
					}
					combinedTableResponse = combinedTableResponse.concat(dta);
				}
			}

			combinedTableResponse.map(function(res) {
				if (pivotElements.indexOf(res[pivotColumn]) === -1) {
					pivotElements.push(res[pivotColumn]);
				}
				if (pivotSettings.multiTable) {
					var obj = {};
					var pvt = res[pivotColumn];
					if (processedData[res[multiTableKey]]) {
						var o = processedData[res[multiTableKey]];
						if (!o[pvt]) {
							o[pvt] = [];
						}
						o[pvt].push(res);
						processedData[res[multiTableKey]] = o;
					} else {
						if (!obj[pvt]) {
							obj[pvt] = [];
						}
						obj[pvt].push(res);
						processedData[res[multiTableKey]] = obj;
					}
				} else {
					if (!processedData[res[pivotColumn]]) {
						processedData[res[pivotColumn]] = [];
					}
					processedData[res[pivotColumn]].push(res);
				}
			});
			if ((!pivotSettings.colOrder || (pivotSettings.colOrder && pivotSettings.colOrder.length === 0))
					&& pivotSettings.editCols
					&& pivotSettings.editCols.length > 0) {
				pivotSettings.editCols.map(function(ele) {
					if (ele && pivotElements.indexOf(ele) > -1) {
						pivotElements.move(pivotElements.indexOf(ele),
								(pivotElements.length - 1));
					}
				})
			} else if (pivotSettings.colOrder
					&& pivotSettings.colOrder.length > 0) {
				pivotElements = pivotSettings.colOrder;
			}
			var str = '';
			var pkeys = [];
			if (pivotSettings.multiTable) {
				var tableConfig = {};
				var columnConfigs = {};
				tableConfig[dataSettings.table] = pivotSettings.tableAs
						|| dataSettings.table.replaceAll('_', ' ');
				var obj = {};
				columnSettings
						.map(function(o) {
							obj[o.data] = {
								columnAlias : o.title,
								editable : o.editable,
								hidden : !o.enabledetail,
								render : o.renderfunction,
								edtType : o.edittype,
								typeOpt : o.typeoptions,
								wraptext : o.wraptext,
								headdingSource : pivotElements.join(','),
								pivotHeader : (pivotSettings.headerCols === o.data) ? true
										: false
							};
						});
				obj.headerCols
				columnConfigs[dataSettings.table] = obj;
				pkeys = {};
				primaryKeySettings.map(function(i) {
					if (!pkeys[dataSettings.table]) {
						pkeys[dataSettings.table] = [];
					}
					pkeys[dataSettings.table].push(i.fieldName);
				});
				pkeys[dataSettings.table].push(pivotColumn);

				pivotSettings.tables.map(function(table) {
					tableConfig[table.table] = table.tableAlias || table;
					columnConfigs[table.table] = table.columnConfigs || {};
					if (!table.pKey) {
						table.pKey = [];
					}
					if (table.pKey.indexOf(pivotColumn) === -1) {
						table.pKey.push(pivotColumn);
					}
					pkeys[table.table] = table.pKey;
				});
				str = '<ul class="rpd-feature-set-list" style="list-style-type: none;" id="modalDrilldown-content">';
				for ( var i in tableConfig) {
					var innerStr = getMDMViewStr(pivotElements,
							processedData[i], i, height, pivotSettings,
							multiTableKey, pkeys[i], columnConfigs[i])
					str += '<li>';
					str += '<div class="rpd-feature-set">';
					str += '<div class="rpd-feature-set-header line"><div data-obj="'
							+ i
							+ '" class="drillCollapse expand rpd-feature-circle fk-float-left js-rpd-feature-circle">-</div>';
					str += '<h3 class="rpd-feature-set-title" style="text-transform: capitalize;">'
							+ tableConfig[i] + '</h3></div>';
					str += '<div class="rpd-feature-set-body bpadding10 ' + i
							+ '-expand">';
					str += '<div  class="js-rpd-feature-set-innerbody line '
							+ i + '-expand1">';
					// Content-BEGIN
					str += '<div class="row">';
					str += '<div>';
					str += innerStr;
					str += '</div>';
					str += '</div>';
					str += '</div>';
					// Content-END
					str += '</div>';
					str += '</div>';
					str += '</li>';
				}
				str += '</ul>';

			} else {
				var columnConfigs = {};
				var obj = {};

				primaryKeySettings.map(function(i) {
					pkeys.push(i.fieldName);
				});

				if (pkeys.indexOf(pivotColumn) === -1) {
					pkeys.push(pivotColumn);
				}
				columnSettings.map(function(o) {
					obj[o.data] = {
						columnAlias : o.title,
						editable : o.editable,
						hidden : !o.enabledetail,
						render : o.renderfunction,
						edtType : o.edittype,
						typeOpt : o.typeoptions,
						wraptext : o.wraptext
					};
				});
				columnConfigs = obj;
				str += getMDMViewStr(pivotElements, processedData,
						dataSettings.table, height, pivotSettings,
						multiTableKey, pkeys.join(','), columnConfigs);
			}

			$("#modalDrilldown-additionalFeature").append(
					'<div>' + str + '</div>');
			$('.td-filter')
					.on(
							'click',
							function() {
								if (disableWorkFlowAfterComplete
										&& $('#workflow .steps li').last()
												.hasClass('active')) {
									showNotification('error', 'Can not update '
											+ $('#workflow .steps li').last()
													.find('.span-text').html()
											+ ' item.');
									return;
								}
								if (currentFocusedObject) {
									$(currentFocusedObject).hide();
									$(currentFocusedObject).closest('td').find(
											'.lbl-filter').show();
								}
								if (!$(this).find('.ip-filter')) {
									return;
								}
								$(this).find('.ip-filter').val(
										$(this).find('.lbl-filter').text());
								$(this).find('.lbl-filter').hide();
								$(this).find('.ip-filter').show();
								$(this)
										.find('.ip-filter')
										.on(
												'keyup',
												function(e) {
													if (e.keyCode == 13) {
														var pkeys = $(this)
																.attr(
																		'data-pKeys');
														if (!pkeys) {
															showNotification(
																	'error',
																	'Primary key not configured.');
															return;
														}
														var primaryKeyObject = JSON
																.parse(decodeURIComponent(escape(atob($(
																		this)
																		.closest(
																				'tbody')
																		.find(
																				'.hdn-obj')
																		.val()))));
														var primaryKeyFiltered = [];
														pkeys = pkeys
																.split(',');
														pkeys
																.map(function(h) {
																	primaryKeyFiltered
																			.push({
																				enableAudit : true,
																				fieldName : h,
																				newValue : primaryKeyObject[h]
																			});
																});
														updateToDb(
																$(this).val(),
																$(this)
																		.attr(
																				'data-oldVal'),
																primaryKeyFiltered,
																$(this)
																		.attr(
																				'data-fieldName'),
																$(this)
																		.attr(
																				'data-displayName'),
																dataSettings,
																$(this)
																		.attr(
																				'data-table'));
														$(this).hide();
														$(this)
																.closest('td')
																.find(
																		'.lbl-filter')
																.text(
																		$(this)
																				.val());
														$(this)
																.closest('td')
																.find(
																		'.lbl-filter')
																.show();
													}
												});
								$(this)
										.find('.ip-filter-chkbox')
										.on(
												'click',
												function() {
													var pkeys = $(this)
															.closest(
																	'.smart-form')
															.attr('data-pKeys');
													if (!pkeys) {
														showNotification(
																'error',
																'Primary key not configured.');
														return;
													}
													var primaryKeyObject = JSON
															.parse(decodeURIComponent(escape(atob($(
																	this)
																	.closest(
																			'tbody')
																	.find(
																			'.hdn-obj')
																	.val()))));
													var primaryKeyFiltered = [];
													pkeys = pkeys.split(',');
													pkeys
															.map(function(h) {
																primaryKeyFiltered
																		.push({
																			enableAudit : true,
																			fieldName : h,
																			newValue : primaryKeyObject[h]
																		});
															});
													updateToDb(
															$(this).prop(
																	'checked'),
															$(this)
																	.closest(
																			'.smart-form')
																	.attr(
																			'data-oldVal'),
															primaryKeyFiltered,
															$(this)
																	.closest(
																			'.smart-form')
																	.attr(
																			'data-fieldName'),
															$(this)
																	.closest(
																			'.smart-form')
																	.attr(
																			'data-displayName'),
															dataSettings,
															$(this)
																	.closest(
																			'.smart-form')
																	.attr(
																			'data-table'));
												});
								currentFocusedObject = $(this).find(
										'.ip-filter');
							});
			$('.expand').on('click', function() {
				var type = '' + $(this).attr('data-obj');
				if ($(this).html() === '+') {
					$(this).html('-');
					$('.' + type + '-expand').removeClass('minhieght200');
					$('.' + type + '-expand1').fadeIn('fast');
				} else {
					$(this).html('+');
					$('.' + type + '-expand').removeClass('feature-folded');
					$('.' + type + '-expand1').fadeOut('fast');
				}
			});
			$('.expand').each(function(index) {
				if (index > 0) {
					$(this).trigger('click');
				}
			});
			$('.collapsable-table-row')
					.on(
							'click',
							function() {
								var attr = $(this).attr('data-record');
								if (attr) {
									if ($(this)
											.find('i')
											.hasClass(
													'fa fa-fw fa-plus-circle txt-color-green')) {
										$(this)
												.find('i')
												.removeClass(
														'fa fa-fw fa-plus-circle txt-color-green');
										$(this)
												.find('i')
												.addClass(
														'fa fa-fw fa-minus-circle txt-color-red');
									} else {
										$(this)
												.find('i')
												.removeClass(
														'fa fa-fw fa-minus-circle txt-color-red');
										$(this)
												.find('i')
												.addClass(
														'fa fa-fw fa-plus-circle txt-color-green');
									}
									$('.' + attr).toggle('show');
								}
							});
			$('#goBack').unbind('click').bind('click', function() {
				$("#modalDrilldown").fadeOut('slow');
				$('.chart-parent-div').fadeIn('fast');
				if ($('#drawProfile').length > 0) {
					$('#drawProfile').fadeIn('fast');
				}
			});

			$('.chart-parent-div').fadeOut('slow');
			if ($('#drawProfile').length > 0) {
				$('#drawProfile').fadeOut('fast');
			}
			$("#modalDrilldown").fadeIn('fast');

		}
		if (selectedRowData && dataSettings.workflow
				&& dataSettings.workflow.column
				&& selectedRowData[dataSettings.workflow.column]) {
			enableLoading();
			disableWorkFlowAfterComplete = !!dataSettings.workflow.disableOnComplete;
			var requests = [ {
				"entityid" : selectedRowData[dataSettings.workflow.column]
			}, null ];
			callAjaxServices([ "getWorkFlowStatus", "getAllWorkFlowStatus" ],
					callBackSuccessGetWorkFlowStatus, callBackFailure,
					requests, [ "POST", "GET" ], "json", true);
		}
	}
	var callBackSuccessGetWorkFlowStatus = function(response) {
		disableLoading();
		if (response && response.length > 0) {
			var currentWorkFlowStatus = response[0];
			constructWorkFlows(response[1], currentWorkFlowStatus);
		}
	};
	var constructWorkFlows = function(response, currentWorkFlowStatus) {
		if (response) {
			$("#workflow").remove();
			var actionDiv = $("<span/>");// $("<div/>").addClass("pull-right");
			var steps = $("<ul/>").addClass("steps");
			for (var i = 0; i < response.length; i++) {
				var workFlowli = $("<li/>")
						.addClass(
								currentWorkFlowStatus.state.toLowerCase() === response[i].currentState
										.toLowerCase() ? 'active' : '');
				workFlowli.append($("<span/>").addClass("badge badge-info")
						.html(i + 1));
				workFlowli.append($("<span/>").addClass('span-text').html(
						response[i].currentState));
				workFlowli.append($("<span/>").addClass("chevron"));
				steps.append(workFlowli);
				if (response[i].action
						&& currentWorkFlowStatus.state.toLowerCase() === response[i].currentState
								.toLowerCase()) {
					actionDiv.append($("<a/>").addClass(
							"btn btn-xs btn-success margin-2").html(
							response[i].action).data("state",
							response[i].targetState).unbind("click").bind(
							"click",
							function() {
								updateWorkFlowStatus(
										currentWorkFlowStatus.entityId, this);
							}));
				}
			}
			;
			actionDiv.append($("<a/>").attr({
				"title" : "View Audit Logs"
			}).addClass('btn btn-xs btn-primary').html('Audit Logs')).unbind(
					'click').bind('click', function() {
				if (!$("#workflow").find("[name='auditlogs']").is(":visible"))
					auditLogs(currentWorkFlowStatus.entityId);
				$("#workflow").find("[name='auditlogs']").toggle();
			});
			$("#workFlowContent").prepend(
					$("<div/>").addClass("fuelux row").attr({
						"id" : "workflow"
					}).append(
							$("<div/>").addClass("wizard col-sm-12").append(
									steps)).append(
							$("<div/>").addClass("col-sm-11").attr({
								"name" : "auditlogs"
							}).css({
								"display" : "none"
							})));
			$("#workflow-log").find('span').html(actionDiv);
		}
	};
	var auditLogs = function(entityId) {
		var request = {
			"entityid" : entityId
		};
		callAjaxService("getworkflowauditlist", callBackAuditLogs,
				callBackFailure, request, "POST", "json", true);
	};
	var callBackAuditLogs = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response && response.length > 0) {
			var table = $("<table/>").addClass(
					"table table-bordered table-striped").append(
					$("<thead/>").append($("<tr/>").append($("<th/>").attr({
						"colspan" : "4"
					}).html("Audit Logs"))).append(
							$("<tr/>").append($("<th/>").html("Status"))
									.append($("<th/>").html("User")).append(
											$("<th/>").html("Updated On"))
									.append($("<th/>").html("Comments"))));
			for (var i = 0; i < response.length; i++) {
				table
						.append($("<tr/>")
								.append($("<td/>").html(response[i].state))
								.append($("<td/>").html(response[i].user))
								.append(
										$("<td/>")
												.html(response[i].lastUpdated))
								.append(
										$("<td/>")
												.html(
														(response[i].comments && response[i].comments !== "null") ? response[i].comments
																: '')));
			}
			$("#workflow").find("[name='auditlogs']").addClass("margin-10")
					.html(table);
		} else {
			$("#workflow").find("[name='auditlogs']").html(
					$("<h1>").addClass("well circle-tile").append(
							$("<strong/>").html("NO DATA")));
		}
	};
	var updateWorkFlowStatus = function(entityId, obj) {
		enableLoading();
		var status = $(obj).data("state");
		var request = {
			"entityid" : entityId,
			"status" : status,
			"updatedby" : System.userName
		// "comments":""
		};
		callAjaxService("updateworkflowstatus", function(response) {
			callBackUpdateWorkFlowStatus(response, entityId)
		}, callBackFailure, request, "POST", "json", true);
	};
	var callBackUpdateWorkFlowStatus = function(response, entityId) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response && response.isUpdated) {
			showNotification("success", "Status has been updated successfully.");
			var requests = [ {
				"entityid" : entityId
			}, null ];
			callAjaxServices([ "getWorkFlowStatus", "getAllWorkFlowStatus" ],
					callBackSuccessGetWorkFlowStatus, callBackFailure,
					requests, [ "POST", "GET" ], "json", true);
			/*
			 * auditLogs(entityId);
			 * $("#workflow").find("[name='auditlogs']").show();
			 */
		}
	};

	var getMDMViewStr = function(pivotElements, processedData, tableName,
			height, pivotSettings, multiTableKey, primaryKeySettings,
			columnConfigs) {
		if (!processedData) {
			return '<div class="alert alert-warning fade in col-sm-9; width: 99%" style="float: left; width: 100%;"><h3 class="no-margin">No data found</h3></div>';
		}
		var tdPercent = 90 / (pivotElements.length);
		var str = '<div>';
		var pivotalObject = [];
		var maxCount = 0;
		for (var a = 0; a < pivotElements.length; a += 1) {
			if (processedData[pivotElements[a]]
					&& processedData[pivotElements[a]].length > maxCount) {
				pivotalObject = processedData[pivotElements[a]];
				maxCount = pivotalObject.length;
			}
		}
		str += '<span>TOTAL Records : <i><b>' + maxCount + '</b></i> </span>';
		str += '<div class="content width-100per"><div class="width-100per" style="float: left;">';
		str += '<div class="table-fixed-wrapper"><div class="table-fixed-container" style="min-height-200px;height: auto;max-height:550px;">';
		str += '<table class="pivotTable table-fixed" style="width: 100%;">'; // table-layout:
		// fixed;display:block;max-height:
		// '+height+';height:
		// '+height+';
		str += '<thead><tr>';
		str += '<th style="width: 10%; max-width: 10%;"><div></div></th>';
		for (var i = 0; i < pivotElements.length; i += 1) {
			str += '<th style="width: ' + tdPercent + '%">' + pivotElements[i]
					+ '<div >' + pivotElements[i] + '</div></th>'; //
		}
		str += '</tr></thead>';

		var primaryKeyStr = '';
		for (var k = 0; k < pivotalObject.length; k += 1) {
			var vertCols = Object.keys(pivotalObject[k]);
			str += '<tbody>';
			primaryKeyStr = '';
			for (var p = 0; p < vertCols.length; p += 1) {
				if (columnConfigs
						&& columnConfigs[vertCols[p]]
						&& columnConfigs[vertCols[p]].pivotHeader
						&& columnConfigs[vertCols[p]].headdingSource
						&& pivotElements
								.indexOf(columnConfigs[vertCols[p]].headdingSource) > -1) {
					// primaryKeyStr += (columnConfigs[vertCols[p]].columnAlias)
					// ? columnConfigs[vertCols[p]].columnAlias : vertCols[p];
					var embed = (pivotalObject[k][vertCols[p]]) ? pivotalObject[k][vertCols[p]]
							: vertCols[p];
					primaryKeyStr += (primaryKeyStr && embed) ? embed + ' ; '
							: embed;
				}
			}
			var customClass = (k > 0) ? "fa fa-fw fa-plus-circle txt-color-green"
					: "fa fa-fw fa-minus-circle txt-color-red";
			str += '<tr data-record="table-'
					+ tableName
					+ '-record'
					+ k
					+ '" class="collapsable-table-row" style="cursor: pointer;"><td style="background-color: #ededed;" colspan="'
					+ (pivotElements.length + 1)
					+ '" style="text-align: center;"><input class="hdn-obj" type="hidden" value="'
					+ btoa(unescape(encodeURIComponent(JSON
							.stringify(pivotalObject[k])))) + '"/><i class="'
					+ customClass + '"></i> ' + (k + 1) + '. ' + primaryKeyStr
					+ ' </td></tr>'
			for (var i = 0; i < vertCols.length; i += 1) {
				var config;
				if (columnConfigs && columnConfigs[vertCols[i]]) {
					config = columnConfigs[vertCols[i]];
				} else {
					config = null;
				}
				if (vertCols[i] === multiTableKey || (config && config.hidden)) {
					continue;
				}
				var vertText = (config && config.columnAlias) ? config.columnAlias
						: vertCols[i];
				customClass = (k > 0) ? "display: none;" : "";
				str += '<tr class="table-' + tableName + '-record' + k
						+ '" style="' + customClass + '">';
				str += '<td style=" min-width: 10%; max-width: 10%;"><b>'
						+ vertText + '</b></td>';
				for (var j = 0; j < pivotElements.length; j += 1) {
					var data;
					if (!processedData[pivotElements[j]]
							|| !processedData[pivotElements[j]][k]) {
						str += '<td class="td-filter" style="width: '
								+ tdPercent + '%; min-width: ' + tdPercent
								+ '%; max-width: ' + tdPercent + '%;"></td>';
						continue;
					}
					data = processedData[pivotElements[j]][k][vertCols[i]];
					data = data || '';
					if (config && config.render) {
						try {
							data = eval(config.render);
						} catch (e) {
							// Intentionally do notihing...
						}
					}
					if ((config && config.editable)
							&& pivotSettings.editCols
							&& pivotSettings.editCols.indexOf(pivotElements[j]) > -1) {
						str += '<td class="td-filter table-background" style="width: '
								+ tdPercent
								+ '%; min-width: '
								+ tdPercent
								+ '%; max-width: '
								+ tdPercent
								+ '%;background-color: lemonchiffon;cursor: pointer;">';
						if (config.edtType
								&& config.edtType.toLowerCase() === 'txtarea') {
							str += '<textarea data-pKeys="'
									+ primaryKeySettings
									+ '" data-table="'
									+ escapeHtml(tableName)
									+ '" data-oldVal="'
									+ escapeHtml(data)
									+ '" data-displayName="'
									+ vertCols[i]
									+ '" style="width: 100%;display: none;" data-fieldName="'
									+ vertCols[i] + '" class="ip-filter" >'
									+ escapeHtml(data) + '</textarea>';
						} else if (config.edtType
								&& config.edtType.toLowerCase() === 'select'
								&& config.typeOpt) {
							var arr = config.typeOpt.split(',');
							var opts = '';
							arr
									.map(function(e) {
										var selcted = (escapeHtml(data) === escapeHtml(e)) ? 'selected'
												: '';
										opts += '<option value="'
												+ escapeHtml(e) + '" '
												+ selcted + '>' + escapeHtml(e)
												+ '</option>';
									});
							str += '<select data-table="'
									+ escapeHtml(tableName)
									+ '" data-oldVal="'
									+ escapeHtml(data)
									+ '" data-displayName="'
									+ vertCols[i]
									+ '" style="width: 100%;display: none;" data-fieldName="'
									+ vertCols[i]
									+ '" class="ip-filter-select">' + opts
									+ '</select>';
						} else if (config.edtType
								&& config.edtType.toLowerCase() === 'number') {
							str += '<input data-pKeys="'
									+ primaryKeySettings
									+ '" data-table="'
									+ escapeHtml(tableName)
									+ '" data-oldVal="'
									+ escapeHtml(data)
									+ '" data-displayName="'
									+ vertCols[i]
									+ '" value="'
									+ escapeHtml(data)
									+ '" style="width: 100%;display: none;" type="number" data-fieldName="'
									+ vertCols[i] + '" class="ip-filter" />';
						} else if (config.edtType
								&& config.edtType.toLowerCase() === 'lookup'
								&& typeOpt) {
							// str += '<input
							// data-table="'+escapeHtml(tableName)+'"
							// data-oldVal="'+escapeHtml(data)+'"
							// data-displayName="'+vertCols[i]+'"
							// value="'+escapeHtml(data)+'" style="width:
							// 100%;display: none;" type="number"
							// data-fieldName="'+vertCols[i]+'"
							// class="ip-filter" />';
						} else if (config.edtType
								&& config.edtType.toLowerCase() === 'date') {
							str += '<input data-pKeys="'
									+ primaryKeySettings
									+ '" data-table="'
									+ escapeHtml(tableName)
									+ '" data-oldVal="'
									+ escapeHtml(data)
									+ '" data-displayName="'
									+ vertCols[i]
									+ '" value="'
									+ escapeHtml(data)
									+ '" style="width: 100%;display: none;" type="date" data-fieldName="'
									+ vertCols[i]
									+ '" class="ip-filter-date" />';
						} else if (config.edtType
								&& config.edtType.toLowerCase() === 'toggle') {
							var checked = '';
							if (escapeHtml(data) === 't') {
								checked = 'checked';
							}
							data = '';
							str += '<label class="smart-form" style="position: initial; display: initial" data-pKeys="'
									+ primaryKeySettings
									+ '" data-table="'
									+ escapeHtml(tableName)
									+ '" data-oldVal="'
									+ escapeHtml(data)
									+ '" data-displayName="'
									+ vertCols[i]
									+ '" data-fieldName="'
									+ vertCols[i]
									+ '" style="width: 100%;"><label class="toggle"><input type="checkbox" class="ip-filter-chkbox" '
									+ checked + '>';
							str += '<i data-swchon-text="Yes" data-swchoff-text="No"></i> </label></label>';
						} else if (config.edtType
								&& config.edtType.toLowerCase() === 'checkbox') {
							var checked = '';
							data = '';
							if (escapeHtml(data) === 't') {
								checked = 'checked';
							}
							str += '<label style="position: initial; display: initial" class="smart-form" data-pKeys="'
									+ primaryKeySettings
									+ '" data-table="'
									+ escapeHtml(tableName)
									+ '" data-oldVal="'
									+ escapeHtml(data)
									+ '" data-displayName="'
									+ vertCols[i]
									+ '" data-fieldName="'
									+ vertCols[i]
									+ '" style="width: 100%;"><label class="checkbox"><input type="checkbox" class="ip-filter-chkbox" '
									+ checked + '>';
							str += '<i data-swchon-text="Yes" data-swchoff-text="No"></i> </label></label>';
						} else {
							str += '<input data-pKeys="'
									+ primaryKeySettings
									+ '" data-table="'
									+ escapeHtml(tableName)
									+ '" data-oldVal="'
									+ escapeHtml(data)
									+ '" data-displayName="'
									+ vertCols[i]
									+ '" value="'
									+ escapeHtml(data)
									+ '" style="width: 100%;display: none;" type="text" data-fieldName="'
									+ vertCols[i] + '" class="ip-filter" />';
						}
						if (config && config.wraptext) {
							str += '<label style="word-break: break-all;" class="lbl-filter">'
									+ data + '</label>';
						} else {
							str += '<label class="lbl-filter">' + data
									+ '</label>';
						}
						str += '</td>';
					} else {
						str += '<td class="td-filter " style="width: '
								+ tdPercent + '%; min-width: ' + tdPercent
								+ '%; max-width: ' + tdPercent + '%;">';
						if (config && config.wraptext) {
							str += '<label style="word-break: break-all;">'
									+ data + '</label>';
						} else {
							str += '<label>' + data + '</label>';
						}
						str += '</td>';
					}
				}
				str += "</tr>";
			}
			str += "</tbody>";
		}
		str += "</table>";
		str += '</div></div>';
		str += '</div></div></div>';
		return str;
	}
	
	this.getHirarchicalDataHurricaneBO =  function (obj){
		var req = JSON.parse($(obj).parent("tr").data("request"));
		var columnSettings = JSON.parse($(obj).parent("tr").data("columnSettings"));
		var dataSettings = JSON.parse($(obj).parent("tr").data("dataSettings"));
		var pKeys = $(obj).parents("tr").data("primarykeys");
		if (pKeys !== null && !$.isEmptyObject(pKeys)) {
			pKeys = JSON.parse(pKeys);
		} else {
			showNotification("error", "Primary Key not configured");
			return;
		}
		//datatable_2
		var serverSideDrillDownColumns = ["SO", "REGION", "BRAND",  "CUSTOMER", "SUPPLYING_DC", "SO_LINE", "REQ_DATE", "DELIVERY_DATE"];
		var columnConfigurations = [];
		var columnName=" ";
		//Example ['bite', 'jaw'] -> [{columnName: bite}, {columnName: jaw}]
		for(var i=0;i<serverSideDrillDownColumns.length;i++){ 
			columnName=serverSideDrillDownColumns[i]; 
			var columnObj={"columnName":columnName};
			columnConfigurations.push(columnObj);
			
		}
		var request = {
				"source" : dataSettings.source,
				"database" : dataSettings.database,
				"table" : 'hrc_bo',
				"columns" : serverSideDrillDownColumns.join(','),
				"order[0][column]": "0",
				"order[0][dir]": "asc",
				"start": '' + 0,
				"length": '' + 1500,
				"search[value]": null,
				"search[regex]":"false",
				"filter": "SO = '" + req["so"] +"'",
				"columnConfigurations" : JSON.stringify(columnConfigurations) 
			};
		
		
		
			callAjaxService("getDataPagination", function(response){
				$("#datatable_2").dataTable().fnClearTable();
				$('#datatable_2').dataTable().fnAddData(JSON.parse(response.data));
			}, callBackFailure, request, "POST");	
	}
	
	function updateToDb(newValue, oldValue, primaryKey, fieldName, displayName,
			dataSettings, table) {
		var fieldType = 'text';
		var req = {
			"source" : dataSettings.source,
			"database" : dataSettings.database,
			"table" : table,
			"values" : [ {
				"fieldName" : fieldName,
				"displayName" : displayName,
				"newValue" : newValue,
				"oldValue" : oldValue,
				"fieldType" : fieldType,
				"type" : "U"
			} ],
			"primaryKey" : primaryKey
		};
		var request = {
			"request" : JSON.stringify(req)
		};
		enableLoading();
		callAjaxService("updateToDB", callBackUpdateSuccess, callBackFailure,
				request, "POST");
	}

	var callBackUpdateSuccess = function(response) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		showNotification("success", "Saved Successfully");
		// getScenario();
	}

	$('#myModalDrilldown').on('hidden.bs.modal', function() {
		$(".zoomContainer").remove();
	});

	this.callback = function(obj) {
		obj = obj || $("#docsPeraDataGrid > tbody > tr:first >td");

		$("#myModalDrilldown").modal("show");
		var request = JSON.parse($(obj).parent("tr").data("request"));
		if (!$("#btnShippingL3").hasClass("disabled"))
			$("#btnShippingL3").addClass("disabled");
		if (!$("#btnCompleteL3").hasClass("disabled"))
			$("#btnCompleteL3").addClass("disabled");
		if (!$("#btnTemplateL3").hasClass("disabled"))
			$("#btnTemplateL3").addClass("disabled");
		if (request.event_id === "5743") {
			$("#btnShippingL3").removeClass("disabled");
		} else if (request.event_id === "5742") {
			$("#btnCompleteL3").removeClass("disabled");
		} else {
			$("#btnTemplateL3").removeClass("disabled");
		}
		var onHtmlLoadPO = function(HTMLresponse) {
			if (HTMLresponse && HTMLresponse.isException) {
				showNotification("error", HTMLresponse.customMessage);
				return;
			}
			$("#poDocView").html(HTMLresponse);
			$('#event_details').find('td:eq(0)').html(request.event_id);
			$('#event_details').find('td:eq(1)').html(request.event_start);
			$('#event_details').find('td:eq(2)').html(request.event_case);
			$('#event_details').find('td:eq(3)').html(request.event_details);
			$('#event_details').find('td:eq(4)').html(
					request.event_physicians_name);
			$('#patientDetails').find('td:eq(0)').html(request.patient_id);
			$('#patientDetails').find('td:eq(1)').html(request.patient_gender);
			$('#patientDetails').find('td:eq(2)').html(request.patient_age);
			$('#patientDetails').find('td:eq(3)').html(request.patient_height);
			$('#patientDetails').find('td:eq(4)').html(request.patient_weight);
			$('#physiciansDetails').find('td:eq(0)').html(
					request.event_physicians_id);
			$('#physiciansDetails').find('td:eq(1)').html(
					request.event_physicians_name);
			$('.rpd-feature-circle').bind('click', function() {
				if ($(this).html() === '-') {
					$(this).html('+');
				} else {
					$(this).html('-');
				}
				$(this).parent().next().toggle();
			});
		};
		callAjaxService("DocSperaView", onHtmlLoadPO, function() {
		}, null, "GET", "html");
	};
	this.caseView = function() {
		$("#myModalDrilldown").modal("show");
		$("#myModalDrilldown").find("h4").html("Shipment Details");
		$("#myModalDrilldown #poDocView").append(
				'<img src="img/caseview.png" width="950" height="500">');

		var caseViewCallBack = function() {
			alert("hh");
		};
		var request = {};
		request.partName = "150410103";
		request.partNumber = "4";
		request.partDesc = "ATTUNE PS FEM LT SZ 3 CEM";
		request.cases = [ "4159152", "4159153" ];
		request.grossQty = 2;
		callAjaxService("getPartView", caseViewCallBack, callBackFailure,
				request, "POST");

	};

	var callbackOrderDetails = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			$('#myModalDrilldown').modal('show');
			$("#myModalLabel", "#myModalDrilldown").html("");
			$("#myModalLabel", '#myModalDrilldown').append(
					$("<div/>").append($("<strong/>").html("Order No:"))
							.append(
									$("<span/>").addClass("text-right").html(
											response.id))).append(
					$("<div>").addClass("row").append(
							$("<strong/>").addClass("col-md-1 text-right")
									.html("Location:")).append(
							$("<span/>").addClass("col-md-2").html(
									response.location)).append(
							$("<strong/>").addClass("col-md-2 text-right")
									.html("Sales Rep:")).append(
							$("<span/>").addClass("col-md-2").html(
									response.salesRep)).append(
							$("<strong/>").addClass("col-md-2 text-right")
									.html("Created Date:")).append(
							$("<span/>").addClass("col-md-3").html(
									response.createdDate)));
			var resTable = $("<table/>").addClass(
					"table table-bordered table-striped");
			var resDiv = $("<div/>").addClass("table-responsive").append(
					resTable);
			$("#poDocView").addClass("no-min-height").empty().append(resDiv);
			$(".modal-footer").find("a").remove();
			resTable.append($("<thead>").append(
					$("<tr/>").append($("<th/>").html("#")).append(
							$("<th/>").html("Part Number")).append(
							$("<th/>").html("Part Description")).append(
							$("<th/>").html("Cases")).append(
							$("<th/>").html("Gross Qty")).append(
							$("<th/>").html("Trunk Stock")).append(
							$("<th/>").html("Net Qty"))));
			for (var i = 0; i < response.parts.length; i++) {
				resTable.append($("<tr/>").append($("<td/>").html(i + 1))
						.append($("<td/>").html(response.parts[i].partNumber))
						.append(
								$("<td/>").html(
										response.parts[i].partDescription))
						.append($("<td/>").html(response.parts[i].cases))
						.append($("<td/>").html(response.parts[i].grossQty))
						.append($("<td/>").html(response.parts[i].trunkQty))
						.append($("<td/>").html(response.parts[i].netQty)));
			}
			;
		}
		;
	};
	this.orderDetails = function(obj) {
		var request = {};
		if ($(obj).parent().data("request")) {
			request.orderno = JSON.parse($(obj).parent().data("request")).id;
		}
		callAjaxService("getorderdetails", callbackOrderDetails,
				callBackFailure, request, "POST");
	};
	this.eventStatus = function(data) {
		var temp = '';
		if (data) {
			if (data.toLowerCase() === "shipped") {
				temp = '<span class="gray-bg btn-xs txt-color-white text-transform-capitalize">'
						+ data + '</span>';
			} else if (data.toLowerCase() === 'confirmed') {
				temp = '<span class="navy-bg btn-xs txt-color-white text-transform-capitalize">'
						+ data + '</span>';
			} else if (data.toLowerCase() === 'cancelled') {
				temp = '<span class="red-bg btn-xs txt-color-white text-transform-capitalize">'
						+ data + '</span>';
			} else {
				temp = '<span class="blue-bg btn-xs txt-color-white text-transform-capitalize">'
						+ data + '</span>';
			}
		}
		return temp;
	};
	var callBackSuccessDetailsByPartNumber = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else if (!response || response.length === 0) {
			showNotification("info", "No record Found in SmartFind");
			return;
		}
		var data = response[0];
		$("#myModalDrilldown").modal("show");
		$(".modal-body", "#myModalDrilldown").addClass("padding-top-10");
		$(".modal-title", "#myModalDrilldown").html(
				"SmartFind Data - <b>" + data.partnumber);
		var mainDiv = $("#poDocView").empty().addClass("row");
		var title = $("<div/>").addClass("col-sm-12");
		title.append($("<h1>").addClass("semi-bold").html(data.title));

		var subDiv = $("<div/>").addClass("col-md-12 padding-top-10");
		subDiv.append(
				$("<div>").addClass("col-md-2 text-right text-bold").html(
						"Document No:")).append(
				$("<div>").addClass("col-md-2").html(
						data.docnum || 'Not Avaiable'));
		subDiv.append(
				$("<div>").addClass("col-md-2 text-right text-bold").html(
						"Company:")).append(
				$("<div>").addClass("col-md-2").html(
						data.manufacturer || 'Not Avaiable'));
		subDiv.append(
				$("<div>").addClass("col-md-2 text-right text-bold").html(
						"Clinical specialty:")).append(
				$("<div>").addClass("col-md-2").html(
						data.cspeciality || 'Not Avaiable'));
		subDiv.append(
				$("<div>").addClass("col-md-2 text-right text-bold").html(
						"Manufacturer Loc:")).append(
				$("<div>").addClass("col-md-2").html(
						data.mLocation || 'Not Avaiable'));
		subDiv.append(
				$("<div>").addClass("col-md-2 text-right text-bold").html(
						"Component No:")).append(
				$("<div>").addClass("col-md-2").html(
						data.componentNumber || 'Not Avaiable'));
		subDiv.append(
				$("<div>").addClass("col-md-2 text-right text-bold").html(
						"Finished Goods:")).append(
				$("<div>").addClass("col-md-2").html(
						data.finishedGoodsNumber || 'Not Avaiable'));
		subDiv.append(
				$("<div>").addClass("col-md-2 text-right text-bold").html(
						"Category/Sub:")).append(
				$("<div>").addClass("col-md-10").html(
						data.categoryl0.replace(";", ", ") || 'Not Avaiable'));
		subDiv.append(
				$("<div>").addClass("col-md-2 text-right text-bold").html(
						"Material/Comp:")).append(
				$("<div>").addClass("col-md-10").html(data.components)
						|| 'Not Avaiable');

		var imageSet = data.imageset.split(",");
		var sfPngURL = lookUpTable.get("sfPngURL");
		var imagePath = sfPngURL + data.imagepath;
		var imageDiv = $("<div/>").addClass(
				"col-sm-12 margin-top-10 galaxie-image");
		for (var i = 0; i < imageSet.length; i++) {
			imageDiv.append($("<div/>").addClass("text-center padding-10")
					.append($("<img/>").attr({
						"src" : imagePath + imageSet[i],
						"alt" : title,
						"height" : $(window).height() - 150 + "px"
					})));
		}
		mainDiv.append(title).append(subDiv).append(imageDiv);
		var calback = function() {
			$('.detailImg').remove();
			setTimeout(function() {
				$("#poDocView img").elevateZoom({
					zoomType : "lens",
					minZoomLevel : 0.5,
					lensShape : "round",
					lensSize : 300,
					scrollZoom : true,
					customClass : "detailImg"
				});
			}, 500);
		};
		loadScript("js/plugin/elevatezoom-master/jquery.elevatezoom.js",
				calback, "poDocView");
		$('#myModalDrilldown').on('hidden.bs.modal', function() {
			$('.zoomContainer').removeClass('hide');
			$('.detailImg').remove();
		});
	};
	this.linkGalaxieSmartFind = function(obj) {
		var request = {
			"partNumber" : $(obj).text()
		};
		callAjaxService("getDetailsByPartNumber",
				callBackSuccessDetailsByPartNumber, callBackFailure, request,
				"POST");
	};
	this.dummyFunction = function() {
	};
	this.galaxieDrawingLink = function(data) {
		if (data && data !== 'null' && data !== '#N/A')
			return '<a onclick="dataTableDrillDown.galaxieDrawing(this)">'
					+ data + '</a>';
		else
			return data;
	};
	this.galaxieDrawing = function(obj) {
		var trRequest = $(obj).parents("tr").data();
		trRequest = JSON.parse(trRequest.request);
		var filePath = trRequest.dwg_path;
		filePath = filePath
				.replace(
						"\\\\na.jnj.com\\jnjdfsroot\\na\\dpyna\\West Chester\\Everyone\\HCL Projects\\Instruments Compendium",
						"http://itsusral00904/PNGANDJPG/").replace(/\\/g, "/");
		var win = window.open(filePath, '_blank');
		win.focus();
	};
	this.constructMasterDataDrilldown = function(data) {
		return '<i class="fa fa-plus" refid="' + data
				+ '" onclick="dataTableDrillDown.getRelatedData(this)"></span>';
	};
	var isLoaded = false;
	this.getRelatedData = function(obj) {
		if (isLoaded || !$(obj).attr('refid')) {
			return;
		}
		isLoaded = true;
		var key = $(obj).attr('refid');
		if ($(obj).hasClass('fa-minus')) {
			isLoaded = false;
			$(obj).removeClass('fa-minus');
			$(obj).addClass('fa-plus');
			$('.' + $(obj).attr('refid') + '_tr').hide();
			return;
		}
		if ($('.' + $(obj).attr('refid') + '_tr').length > 0) {
			$(obj).removeClass('fa-plus');
			$(obj).addClass('fa-minus');
			isLoaded = false;
			$('.' + $(obj).attr('refid') + '_tr').show();
			return;
		}
		var request = {
			"key" : key,
			"dbName" : "QFINDSPELL"
		}
		enableLoading();
		callAjaxService(
				'getMasterData',
				function(response) {
					disableLoading();
					isLoaded = false;
					if (!response) {
						return;
					}
					if (response.isException) {
						showNotification('error', response.customMessage);
					}
					var tr = obj.closest('tr');
					$(obj).removeClass('fa-plus');
					$(obj).addClass('fa-minus');
					var str = '';
					for ( var key in response) {
						str += '<tr class="' + $(obj).attr('refid')
								+ '_tr table-background">';
						for (var i = 0; i < response[key].length; i += 1) {
							if (i === 0) {
								str += '<td class="non-editable"><label style="padding: 5px;" class="label label-info non-editable">'
										+ response[key][i] + '</label></td>';
							} else {
								str += '<td class="non-editable">'
										+ response[key][i] + '</td>';
							}
						}
						str += '</tr>';
					}
					$(tr).after(str);
				}, callBackFailure, request, "POST", null);
	};

};
var dataTableDrillDown = new dataTableDrillDownObj();