var DataTableEditor = function(){
	
	var getHeaderName = function(ele){
		var tabel = $(ele).parents("table");
		var th = $(tabel).find(' tr:not(.columnSearch) th').eq($(ele).index());
		return th.text();
	};
	this.isColumnEditable = function(colConfig){
		var editable = false;
		if (colConfig.title === getHeaderName(this.ele))
			return true;
		
		return editable;
	};
	var callBackSuccessUpdateRecords = function(response){
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}else if ($("input.ui-autocomplete-loading").length === 0){
			showNotification("success", "Saved Successfully");
			if($("#applyFilter").length > 0) {
				$("#applyFilter").trigger("click");
			}
			$("#myModalDrilldown").modal("hide");
		} else {
			$("input.ui-autocomplete-loading").each(function(){
				$(this).parents("td").removeClass("smart-form active").html($(this).val());
			});
		}
		dataAnalyser.profiling.grapher.dataTable.forEach(function(_datatable) {
			var filter = dataAnalyser.profiling.grapher.constructFilterQuery(_datatable.ajax.params("data").fieldMapping,_datatable.ajax.params("data").defaultFilter);
			localStorage["datatablefilter_"+_datatable.context[0].sTableId.replace("#",'')] = filter;
			_datatable.ajax.reload();
		});
		
		function callbackSucessClearCache() {
			dataAnalyser.profiling.grapher.refreshTable();
		}
		callAjaxService("clearCache", callbackSucessClearCache,
				callBackFailure, null, "POST");
	};
	this.updateRecords = function(values){
		this.tableDetails.values = values;
		var primarykeys = $(this.ele).parents("tr").data("primarykeys");
		if (primarykeys !== null && !$.isEmptyObject(primarykeys)){
			this.tableDetails.primaryKey =JSON.parse(primarykeys);
		}else{
			showNotification("error", "Primary Key not configured");
			return;
		}
		var request = {
				"request":JSON.stringify(this.tableDetails)
		};
		callAjaxService("updateToDB",callBackSuccessUpdateRecords,callBackFailure,request,"POST");
		
	};
	this.saveForm = function(primaryKeys, isCreate){
		if (!$("#saveForm").valid())
			return;
		var updateObj = [];
		$("#saveForm section").each(function(){
			var fieldType = $(this).attr("fieldtype");			
			var ele = "";
			var newValue = "";
			if (fieldType === "text" || fieldType === "number" || fieldType === "date" || fieldType === "datestring"){
				ele = $(this).find("input");
				if(ele.prop('disabled') && $(this).find("input").attr("defaultvalue") === undefined && $(this).find("input").val() == "") {
					return;
				}
				var defaultData=$(this).find("input").attr("defaultvalue") != undefined ? $(this).find("input").attr("defaultvalue").indexOf("function") : -1;
				if(defaultData > -1){
					 newValue = $(this).find("input").attr("defaultvalue") != undefined ? eval($(this).find("input").attr("defaultvalue")):ele.val();
				}else{
                     newValue = $(this).find("input").attr("defaultvalue") != undefined ? $(this).find("input").attr("defaultvalue"):ele.val();
				}
				newValue = newValue.replaceAll("'","`");
			}else if (fieldType === "select"){
				ele = $(this).find("select");
				if(ele.prop('disabled')) {
					return;
				}
				newValue = ele.val();
				fieldType = "text";
			}else if (fieldType === "checkbox" || fieldType === "toggle"){
				ele = $(this).find("input");
				if(ele.prop('disabled')) {2
					return;
				}
				newValue = ele.is(":checked")?"t":"f";
				fieldType = "boolean";
			}else if (fieldType === "lookup"){
				ele = $(this).find("input[tabindex=-1]");
				if(ele.prop('disabled')) {
					return;
				}
				newValue = ele.select2('val');
				fieldType = "text";
			}if (fieldType === "txtarea"){
				ele = $(this).find("textarea");
				if(ele.prop('disabled')) {
					return;
				}
				newValue = ele.val();
			}
			var oldValue = $(ele).attr("oldvalue");
			if (newValue === oldValue)
				return;
			if (fieldType === "boolean")
				newValue = newValue==="t"?true:false;
			var fieldTitle = $(ele).attr("fieldTitle");
			var fieldName = $(ele).attr("field");
			var columnconfig = $(this).data('colconfig');
			if(!fieldName){
				fieldName = columnconfig.data;
			}
			if(fieldType === 'datestring'){
				fieldType = 'text';
			}
			var colObj = {
					"fieldName":fieldName,
					"displayName":fieldTitle,
					"newValue":newValue,
					"oldValue":oldValue,
					"fieldType":fieldType,
					"type":"U"
			};
			updateObj.push(colObj);
		});
		if (updateObj.length === 0){
			$("#myModalDrilldown").modal("hide");
			return;
		}
			
		this.tableDetails.values  = updateObj;
		this.tableDetails.primaryKey = primaryKeys;
		this.tableDetails.create = !!isCreate;
		if (this.tableDetails.source === "hive" && primaryKeys && primaryKeys.length > 0){
			for (var c = 0; c< primaryKeys.length; c++){
				var colObj = {
						"fieldName":primaryKeys[c].fieldName,
						"displayName":primaryKeys[c].fieldName,
						"newValue":primaryKeys[c].newValue,
						"oldValue":primaryKeys[c].newValue,
						"fieldType":primaryKeys[c].fieldType || "text",
						"type":"P"
				};
				if (primaryKeys[c].enableAudit)
					this.tableDetails.enableAudit = primaryKeys[c].enableAudit;
				
				updateObj.push(colObj);
			}
			this.tableDetails.primaryKey = null;
		}
		var request = {
				"request":JSON.stringify(this.tableDetails)
		};
		callAjaxService("updateToDB",callBackSuccessUpdateRecords,callBackFailure,request,"POST");
	};
};

DataTableEditor.prototype.checkForDirtyFields = function(ele,enableBulkUpdate){
	var _self = this;
	var tabel = $(ele).parents("table");
	var isUpdated=true;
	if ($(tabel).find("input[type=checkbox]").length > 0 && $(ele).find("input[type=checkbox]").length === 0){
		var isUpdated=true;
		$(tabel).find("input[type=checkbox]").each(function(){
			if (enableBulkUpdate === undefined || enableBulkUpdate === false)
			{	
				var newValue = $(this).is(":checked");
				var oldValue = $(this).attr("oldvalue");
				if (newValue === oldValue)
					return;
				$(this).parents("td").removeClass("smart-form").html(newValue?"t":"f");
				var updateObj = {
						"fieldName":$(this).attr("field"),
						"displayName":$(this).attr("fieldTitle"),
						"newValue":newValue,
						"oldValue":oldValue,
						"fieldType":"boolean",
						"type":"U"
				};
				_self.updateRecords([updateObj]);
			}else
				{	
					var newValue = $(this).is(":checked");
					var oldValue = $(this).attr("oldvalue");
					if (newValue !== oldValue){
						$($(this).parent().parent().parent()).attr('edited','true');
					}
					$(this).parents("td").removeClass("smart-form").html(newValue?"t":"f");
					isUpdated=false;
					
				}
		});
		return isUpdated;
	}
	if ($(tabel).find("input[class*=hasDatepicker]").length > 0 && $(ele).find("input[class*=hasDatepicker]").length === 0){
		var isUpdated=true;
		$(tabel).find("input[class*=hasDatepicker]").each(function(){
			if (enableBulkUpdate === undefined || enableBulkUpdate === false){
				var newValue = $(this).val();
				var oldValue = $(this).attr("oldvalue");
				if (newValue === oldValue)
					return;
				$(this).parents("td").removeClass("active").html(newValue);
				var updateObj = {
						"fieldName":$(this).attr("field"),
						"displayName":$(this).attr("fieldTitle"),
						"newValue":newValue,
						"oldValue":oldValue,
						"fieldType":"date",
						"type":"U"
				};
				_self.updateRecords([updateObj]);
			}else{
				var newValue = $(this).val();
				var oldValue = $(this).attr("oldvalue");
				if (newValue !== oldValue){
					$($(this).parent().parent()).attr('edited','true');
				}
				$(this).parents("td").removeClass("active").html(newValue);
				isUpdated=false;
			}
		});
		return isUpdated;
	}
	if ($(tabel).find("input[tabindex=-1]").length > 0 && $(ele).find("input[tabindex=-1]").length === 0){
		$(tabel).find("input[tabindex=-1]").each(function(){
			var newValue = $(this).select2('val');
			var oldValue = $(this).attr("oldvalue");
			$(this).parents("td").removeClass("active").html(newValue);
			if (newValue === oldValue)
				return;
			var updateObj = {
					"fieldName":$(this).attr("field"),
					"displayName":$(this).attr("fieldTitle"),
					"newValue":newValue,
					"oldValue":oldValue,
					"fieldType":"text",
					"type":"U"
			};
			_self.updateRecords([updateObj]);
		});
		return true;
	}
	
	return false;
};

// in-line input type text
DataTableEditor.prototype.textEdit = function(config,enableBulkUpdate,defaultUpdateColumns){
	var _self = this;
	var ele = this.ele;
	if($(this.ele).hasClass('non-editable')) {
		return;
	}
	var oldValue = $(this.ele).hasClass( "smart-form active" ) ? $(this.ele).find('input').val() : $(ele).text();
	// var oldValue = $(ele).text();
	var tdWidth = $(ele).width() -32;
	$(ele).addClass("smart-form active").empty().append($("<input/>").attr({"type":config.type||"text","defaultvalue":config.defaultvalue}).css("width",tdWidth+"px").addClass("form-control").val(oldValue));
	if(enableBulkUpdate === undefined || enableBulkUpdate === false)
	{
		$(ele).find("input").focus().blur(function() {
			$(ele).removeClass("smart-form active").html(oldValue);
		}).on("keypress", function(e) {
			$(ele).removeClass("state-error");
	        if (e.keyCode === 13) {
	        	var newValue = $(this).val();
	        	if(config.defaultvalue != undefined){
	        		newValue = config.defaultvalue;
	        	}
	        	if (newValue === oldValue){
	        		$(ele).removeClass("smart-form active").html(oldValue);
	        		return;
	        	}
	        		
	        	if (newValue === "" && config.mandatory){
	        		showNotification("error","this field cannot be left blank");
	        		$(ele).addClass("state-error");
	        		$(this).focus();
	        		return;
	        	}
	        	var updateObj = [{
        			"fieldName":config.field,
        			"displayName":config.title,
    				"newValue":newValue,
    				"oldValue":oldValue,
    				"fieldType":config.type||"text",
    				"type":"U"
            	}];
	        	if(defaultUpdateColumns && defaultUpdateColumns.length > 0) {
	        		for(var i=0;i<defaultUpdateColumns.length;i++){
	        			var defaultvalue = "";
	        			if(defaultUpdateColumns[i].defaultvalue){
	        				try {
		        				var fun = new Function("return "+defaultUpdateColumns[i].defaultvalue);//eval(defaultUpdateColumns[i].defaultvalue);
		        				defaultvalue = fun();	        					
	        				}catch(ex){
	        					console.log(ex);
	        				}
	        			}
	        			updateObj.push({
	        				"fieldName":defaultUpdateColumns[i].field,
	        				"displayName":defaultUpdateColumns[i].title,
	        				"newValue":defaultvalue,
	        				"oldValue":defaultvalue,
	        				"fieldType":defaultUpdateColumns[i].type||"text",
	        				"type":"U"
	        			});
	        		}
	        	}
        		$(ele).html($(ele).find('input').val());
        		$(ele).attr('class','');
        		_self.updateRecords(updateObj);
	        	
	        	$(this).addClass("ui-autocomplete-loading");
	            return false; // prevent the button click from happening
	        }else if (e.keyCode === 27) {
	        	$(ele).removeClass("smart-form no-padding").html(oldValue);
	        }
		});
	} else
		{
			$(ele).find("input").focus().blur(function() {
				var newValue = $(this).val();
				
				if (newValue === "" && config.mandatory){
	        		showNotification("error","this field cannot be left blank");
	        		$(ele).addClass("state-error");
	        		$(this).focus();
	        		return;
				}
				if (newValue !== oldValue)
					$($(ele).parent()).attr('edited','true');
				$(ele).removeClass("smart-form active").html(newValue);
			});
		}
};

//inline Date type
DataTableEditor.prototype.dateEdit = function(config){
	var ele = this.ele;
	var oldValue = $(ele).text();
	var tdWidth = $(ele).width() -32;
	$(ele).addClass("active").empty().append($("<input/>").attr({"type":"text","field":config.field,"fieldTitle":config.title,"defaultvalue":config.defaultvalue}).css("width",tdWidth+"px").addClass("form-control").val(oldValue));
	
	$(ele).find('input')
		.datepicker({
			defaultDate : "+1w",
			changeMonth : true,
			dateFormat: 'mm/dd/yy'//'yy-mm-dd'
		}).focus();
};

//in-line select type text
DataTableEditor.prototype.selectEdit = function(config){
	var ele = this.ele;
	var _self = this;
	if (!config.options || config.options.length === 0){
		return;
	}
	var oldValue = $(ele).text();
	var tdWidth = $(ele).width() -5;
	var tempDiv = $("<div/>");
	var configOptions = config.options;
	for (var i=0; i< configOptions.length;i++){
		tempDiv.append($("<option/>").attr("value",configOptions[i].id).html(configOptions[i].text));
	};
	$(ele).addClass("smart-form active").empty().append($("<select/>").append($(tempDiv).html()).css("width",tdWidth+"px").addClass("form-control").val(oldValue));
	$(ele).find("select").focus().blur(function() {
		var newValue = $(this).val();
		$(ele).removeClass("smart-form active").html(newValue);
		if (newValue === oldValue)
    		return;
		if (newValue === "" && config.mandatory){
    		showNotification("error","this field cannot be left blank");
    		$(ele).addClass("state-error");
    		$(this).focus();
    		return;
    	}
    	var updateObj = {
			"fieldName":config.field,
			"displayName":config.title,
			"newValue":newValue,
			"oldValue":oldValue,
			"fieldType":"text",
			"type":"U"
    	};
		_self.updateRecords([updateObj]);
	}).click(function(e){
		 e.stopPropagation();
	});
};
DataTableEditor.prototype.lookUpEdit = function(config,ele,oldValue){ 
	var _self = this;
	ele = ele || this.ele;
	var lookupQuery = "";
	if(config.options){
		lookupQuery = config.options;
	}else if(config.typeoptions){
		lookupQuery = config.typeoptions
	}
	var formatResult = function(resp) {
		return resp.id + (resp.name?' - ' + resp.name:'');
	};

	var formatSelection = function(resp) { 
		return resp.id + (resp.name?' - ' + resp.name:'');
	};
	oldValue = oldValue || $(ele).text();
	var tdWidth = $(ele).width() -32;
	if (tdWidth <= 0)
		tdWidth =  "100%";
	$(ele).addClass("smart-form active").empty().append($("<input/>").attr({"type":"text","field":config.field,"fieldTitle":config.title,"required":config.mandatory,"oldValue":oldValue}).css("width",tdWidth+"px").addClass("form-control").val(oldValue));
	$(ele).find('input')
	.select2({
				placeholder : "Enter "
						+ config.title,
				allowClear: true,		
				minimumInputLength : 1,
				multiple : false,
				width: 'resolve',
				ajax : {
					url : serviceCallMethodNames["suggestionsLookUp"].url,
					dataType : 'json',
					type : 'GET',
					data : function(
							term) {
						return {
							q : term,
							source : config.sourceconfig?config.sourceconfig:_self.tableDetails.source,
							database : config.databaseconfig?config.databaseconfig:_self.tableDetails.database,
							lookupQuery : lookupQuery.replace(/@/g,"'")
							
						};
					},
					results : function(
							data) {
						return {
							results : data
						};
					}
				},
				formatResult : formatResult,
				formatSelection : formatSelection,
				dropdownCsclass : "bigdrop",
				initSelection : function(
						element,
						callback) {
					callback($
							.map(element.val().split(','),
									function(id) {id = id.split(":");
										return {
											id : id[0],
											name : id[1]
										};
									}));
				},
				escapeMarkup : function(m) {
					return m;
				}
			});

	if(config.disable){
		$(ele).find('input').select2("disable");
	}
	if (oldValue !== "")
		$(ele).find('input').select2('data', {"id":oldValue,"text":oldValue}, true);
	$(ele).click(function(e){
		 e.stopPropagation();
	});
};

DataTableEditor.prototype.toggleEdit = function(config){
	var ele = this.ele;
	var oldValue = $(ele).text();
	var check = {
		"type":"checkbox",
		"field":config.field,
		"fieldTitle":config.title
		};
	if (oldValue === "t"){
		check.checked = "checked";
	}
	$(ele).addClass("smart-form").empty().append($("<label/>").addClass("toggle").append($("<input/>").attr(check))
			.append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"})));
	$(ele).find("i").focus().click(function(e){
		 e.stopPropagation();
	});
	$(ele).find("input").click(function(e){
		 e.stopPropagation();
	});
};
DataTableEditor.prototype.checkBoxEdit = function(config,enableBulkUpdate){
	var ele = this.ele;
	var oldValue = $(ele).text();
	var check = {
		"type":"checkbox",
		"field":config.field,
		"fieldTitle":config.title
		};
	if (oldValue === "t"){
		check.checked = "checked";
	}
	$(ele).addClass("smart-form").empty().append($("<label/>").addClass("checkbox").append($("<input/>").attr(check)).append($("<i/>")));
	
		$(ele).find("i").focus().click(function(e){
			 e.stopPropagation();
		});
		$(ele).find("input").click(function(e){
			 e.stopPropagation();
		});
};

//in-line Editor
DataTableEditor.prototype.inline = function(config,ele,tableDetails){
	this.tableDetails = tableDetails;	
	var enableBulkUpdate=config.enableBulkUpdate;
	if (this.checkForDirtyFields(ele,enableBulkUpdate))
		return;
	this.ele = ele;
	var defaultEditableColumns = config.filter(function(column){ return column.defaultvalue});
	for (var i = 0; i < config.length; i++){
		var editable = this.isColumnEditable(config[i]);		
		var colConfig = config[i];
		if (!editable || !colConfig.editable){
			continue;
		}
		var editType = colConfig.type || "text";
		switch (editType) {
		case 'select':
			this.selectEdit(colConfig);
			break;
		case 'checkbox':
			this.checkBoxEdit(colConfig);
			break;
		case 'toggle':
			this.toggleEdit(colConfig);
			break;
		case 'date':
			this.dateEdit(colConfig);
			break;
		case 'lookup':
			this.lookUpEdit(colConfig);
			break;
		case 'number':
		default:
			this.textEdit(colConfig,enableBulkUpdate,defaultEditableColumns);
			break;
		}
	};
};
DataTableEditor.prototype.form = function(config,ele,tableDetails){ 
	var _self = this;
	this.tableDetails = tableDetails;
	if($(ele).children().length > 0) {
		var attr = $($(ele).children()[0]);
		if($(attr).attr('data-type') === 'no-popup') {
			return;
		}
	}
	$("#myModalDrilldown").modal("show");
	$(".modal-title","#myModalDrilldown").html("Edit");
	$(".modal-dialog","#myModalDrilldown").addClass("width-50p");
	$(".modal-body","#myModalDrilldown").addClass("no-padding-top no-padding-bottom");
	var smartForm = $("<form/>").addClass("smart-form").attr({"novalidate":"novalidate","id":"saveForm"});
	$("#poDocView").addClass("row no-min-height").empty().append(smartForm);
	var colFieldSet = $("<fieldset/>").appendTo(smartForm);
	var colRow = null;
	var trRequest = $(ele).parents("tr").data("request");
	trRequest =JSON.parse(trRequest);
	var primaryKeys = $(ele).parents("tr").data("primarykeys");
	if (primaryKeys !== null && !$.isEmptyObject(primaryKeys)){
		primaryKeys =JSON.parse(primaryKeys);
	}else{
		showNotification("error", "Primary Key not configured");
		return;
	}
	for (var i = 0; i < config.length; i++){
		//if(config[i].visible){
			var colConfig = config[i];
			var editType = colConfig.type || "text";
			var oldValue = trRequest[colConfig.field];
			var fieldName = colConfig.field;
			var fieldTitle = colConfig.title;
			var defaultvalue = colConfig.defaultvalue;
			var isMandatory = colConfig.mandatory;
			var disable = colConfig.disable;
			var isEditable = (colConfig.editable) ? !colConfig.editable : true;
			var colSection = $("<section/>").addClass("col col-6 padding-bottom-10").attr("fieldtype",editType);
			if (i%2 === 0 && i !== config.length-1){
				colRow = $("<div/>").css({"width":"650px"}).addClass("row");
			} else{
				colRow.appendTo(colFieldSet);
			}
			if(!colConfig.visible){
				colSection.addClass("hide");
			}
			colRow.append(colSection);
			colSection.data("colconfig",colConfig);
			if (editType === 'text' || editType === 'number'){
				colSection.append($("<label/>").addClass("label").html(fieldTitle));
				colSection.append($("<span/>").css('padding', '0px').append($("<input/>").addClass("form-control padding-left-10").attr({"type":editType,"field":fieldName,"fieldTitle":fieldTitle,"defaultvalue":defaultvalue,"oldValue":oldValue,"required":isMandatory}).prop('disabled', isEditable).val(oldValue)));
				if(disable){
					colSection.find("input").attr("disabled","disabled");
				}
			}
			else if (editType === 'select'){
				colSection.append($("<label/>").addClass("label").html(fieldTitle));
				var tempDiv = $("<div/>");
				var configOptions = colConfig.options;
				for (var j=0; j< configOptions.length;j++){
					tempDiv.append($("<option/>").attr("value",configOptions[j].id).html(configOptions[j].text));
				};
				colSection.append($("<span/>").css('padding', '0px').append($("<select/>").attr({"type":editType,"field":fieldName,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory}).prop('disabled', isEditable).append($(tempDiv).html()).val(oldValue)).append($("<i/>")));
				if(disable){
					colSection.find("select").attr("disabled","disabled");
				}
			}
			else if (editType === 'toggle'){
				var checkboxOptions = {
						"type":"checkbox",
						"oldValue":oldValue,
						"field":fieldName,
						"fieldTitle":fieldTitle
				};
				if (oldValue === "t")
					checkboxOptions.checked = true;
				colSection.append($("<label/>").addClass("toggle").append($("<input/>").attr(checkboxOptions).prop('disabled', isEditable))
				.append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"})));
			}
			else if (editType === 'checkbox'){
				var checkboxOptions = {
						"type":"checkbox",
						"oldValue":oldValue,
						"field":fieldName,
						"fieldTitle":fieldTitle
				};
				if (oldValue === "t")
					checkboxOptions.checked = true;
				colSection.append($("<label/>").addClass("checkbox").html(fieldTitle).prepend($("<i/>")).prepend($("<input/>").attr(checkboxOptions).prop('disabled', isEditable)));
			}
			else if (editType === 'date' || editType === "datestring"){
				if(config[i].defaultvalue != undefined)
				isEditable=true;
				var dateEle = $("<input/>").attr({"type":"text","field":fieldName,"defaultvalue":config[i].defaultvalue,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory}).prop('disabled', isEditable).val(oldValue);
				colSection.append($("<label/>").addClass("label").html(fieldTitle));
				colSection.append($("<label/>").addClass("input").append(dateEle));
				
				if(config[i].defaultvalue === undefined){
				dateEle
					.datepicker({
						defaultDate : "+1w",
						changeMonth : true,
						dateFormat: 'mm/dd/yy'//'yy-mm-dd'
					}).focus();
				}
				if(disable){
					colSection.find("input").attr("disabled","disabled");
				}
			}
			else if (editType === 'lookup'){
				var lookUpEle = $("<span/>").css('padding', '0px').append($("<input/>").addClass("form-control").attr({"type":"text","field":fieldName,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory}));
				colSection.append($("<label/>").addClass("label").html(fieldTitle));
				colSection.append($("<label/>").addClass("input").append(lookUpEle));
				this.lookUpEdit(colConfig,lookUpEle,oldValue);
			}else if (editType === 'txtarea'){
				colSection.append($("<label/>").addClass("label").html(fieldTitle));
				colSection.append($("<span/>").css('padding', '0px').append($("<textarea/>").css('resize', 'none').addClass("form-control").attr({"type":editType,"field":fieldName,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory, rows:'5', cols:'40'}).prop('disabled', isEditable).val(oldValue)));
				if(disable){
					colSection.find("textarea").attr("disabled","disabled");
				}
			}
		//}
	};
	var colFooter = $("<footer/>").appendTo(smartForm);
	colFooter.append($("<button/>").attr({"type":"button","data-dismiss":"modal","area-hidden":"true"}).addClass("btn btn-warning").html("Cancel"));
	colFooter.append($("<button/>").attr({"type":"submit","id":"saveForm"}).addClass("btn btn-primary").html("Save"));
	
	$('#saveForm').validate();
	$('#saveForm').unbind('submit').submit(
			function() {
				_self.saveForm(primaryKeys);
				return false;
			});
};

DataTableEditor.prototype.actionForm = function(config,ele,tableDetails,header){
var _self = this;
this.tableDetails = tableDetails;
if($(ele).children().length > 0) {
	var attr = $($(ele).children()[0]);
	if($(attr).attr('data-type') === 'no-popup') {
		return;
	}
}
var x= config.length;
/*$(".editColumn").click(function(){*/
$("#myModalDrilldown").modal("show");
$(".modal-title","#myModalDrilldown").html("Edit "+header);
$(".modal-dialog","#myModalDrilldown").addClass("width-50p");
$(".modal-body","#myModalDrilldown").addClass("no-padding-top no-padding-bottom");
var smartForm = $("<form/>").addClass("smart-form").attr({"novalidate":"novalidate","id":"saveForm"});
$("#poDocView").addClass("row no-min-height").empty().append(smartForm);
var colFieldSet = $("<fieldset/>").appendTo(smartForm);
var colRow = null;
var trRequest = $(ele).parents("tr").data("request");
trRequest =JSON.parse(trRequest);
var primaryKeys = $(ele).parents("tr").data("primarykeys");
if (primaryKeys !== null && !$.isEmptyObject(primaryKeys)){
	primaryKeys =JSON.parse(primaryKeys);
}else{
	showNotification("error", "Primary Key not configured");
	return;
}
for (var i = 0; i < config.length; i++){
	//if(config[i].visible){
		var colConfig = config[i];
		var editType = colConfig.type || "text";
		var oldValue = trRequest[colConfig.field];
		var fieldName = colConfig.field;
		var fieldTitle = colConfig.title;
		var defaultvalue = colConfig.defaultvalue;
		var isMandatory = colConfig.mandatory;
		var disable = colConfig.disable;
		var isEditable = (colConfig.editable) ? !colConfig.editable : true;
		var colSection = $("<section/>").addClass("col col-6 padding-bottom-10").attr("fieldtype",editType);
		if (i%2 === 0 && i !== config.length-1){
			colRow = $("<div/>").css({"width":"650px"}).addClass("row");
		} else{
			colRow.appendTo(colFieldSet);
		}
		if(!colConfig.visible){
			colSection.addClass("hide");
		}
		colRow.append(colSection);
		colSection.data("colconfig",colConfig);
		if (editType === 'text' || editType === 'number'){
			colSection.append($("<label/>").addClass("label").html(fieldTitle));
			colSection.append($("<span/>").css('padding', '0px').append($("<input/>").addClass("form-control padding-left-10").attr({"type":editType,"field":fieldName,"fieldTitle":fieldTitle,"defaultvalue":defaultvalue,"oldValue":oldValue,"required":isMandatory}).val(oldValue)));
			if(disable){
				colSection.find("input").attr("disabled","disabled");
			}
		}
		else if (editType === 'select'){
			colSection.append($("<label/>").addClass("label").html(fieldTitle));
			var tempDiv = $("<div/>");
			var configOptions = colConfig.options;
			for (var j=0; j< configOptions.length;j++){
				tempDiv.append($("<option/>").attr("value",configOptions[j].id).html(configOptions[j].text));
			};
			colSection.append($("<span/>").css('padding', '0px').append($("<select/>").attr({"type":editType,"field":fieldName,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory}).prop('disabled', isEditable).append($(tempDiv).html()).val(oldValue)).append($("<i/>")));
			if(disable){
				colSection.find("select").attr("disabled","disabled");
			}
		}
		else if (editType === 'toggle'){
			var checkboxOptions = {
					"type":"checkbox",
					"oldValue":oldValue,
					"field":fieldName,
					"fieldTitle":fieldTitle
			};
			if (oldValue === "t")
				checkboxOptions.checked = true;
			colSection.append($("<label/>").addClass("toggle").append($("<input/>").attr(checkboxOptions).prop('disabled', isEditable))
			.append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"})));
		}
		else if (editType === 'checkbox'){
			var checkboxOptions = {
					"type":"checkbox",
					"oldValue":oldValue,
					"field":fieldName,
					"fieldTitle":fieldTitle
			};
			if (oldValue === "t")
				checkboxOptions.checked = true;
			colSection.append($("<label/>").addClass("checkbox").html(fieldTitle).prepend($("<i/>")).prepend($("<input/>").attr(checkboxOptions).prop('disabled', isEditable)));
		}
		else if (editType === 'date' || editType === "datestring"){
			if(config[i].defaultvalue != undefined)
			isEditable=true;
			var dateEle = $("<input/>").attr({"type":"text","field":fieldName,"defaultvalue":config[i].defaultvalue,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory}).prop('disabled', isEditable).val(oldValue);
			colSection.append($("<label/>").addClass("label").html(fieldTitle));
			colSection.append($("<label/>").addClass("input").append(dateEle));
			
			if(config[i].defaultvalue === undefined){
			dateEle
				.datepicker({
					defaultDate : "+1w",
					changeMonth : true,
					dateFormat: 'mm/dd/yy'//'yy-mm-dd'
				}).focus();
			}
			if(disable){
				colSection.find("input").attr("disabled","disabled");
			}
		}
		else if (editType === 'lookup'){ 
			var lookUpEle = $("<span/>").css('padding', '0px').append($("<input/>").addClass("form-control").attr({"type":"text","field":fieldName,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory}));
			colSection.append($("<label/>").addClass("label").html(fieldTitle));
			colSection.append($("<label/>").addClass("input").append(lookUpEle));
			DataTableEditor.prototype.lookUpEdit(colConfig,lookUpEle,oldValue);
		}else if (editType === 'txtarea'){
			colSection.append($("<label/>").addClass("label").html(fieldTitle));
			colSection.append($("<span/>").css('padding', '0px').append($("<textarea/>").css('resize', 'none').addClass("form-control").attr({"type":editType,"field":fieldName,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory, rows:'5', cols:'40'}).prop('disabled', isEditable).val(oldValue)));
			if(disable){
				colSection.find("textarea").attr("disabled","disabled");
			}
		}
	//}
};

var colFooter = $("<footer/>").appendTo(smartForm);
colFooter.append($("<button/>").attr({"type":"button","data-dismiss":"modal","area-hidden":"true"}).addClass("btn btn-warning").html("Cancel"));
colFooter.append($("<button/>").attr({"type":"submit","id":"saveForm"}).addClass("btn btn-primary").html("Save"));
//});
$('#saveForm').validate();
$('#saveForm').unbind('submit').submit( 
		function() {
		_self.saveForm(primaryKeys);
			return false;
		});
//});
};
DataTableEditor.prototype.showAuditLog = function(ele,tableDetails){
	var config = {};
	event.preventDefault();
	var primaryKeys = $(ele).parents("tr").data("primarykeys");
	if (primaryKeys !== null && !$.isEmptyObject(primaryKeys)){
		primaryKeys =JSON.parse(primaryKeys);
	}else{
		showNotification("error", "Primary Key not configured");
		return;
	}
	var primaryKey= "";
	for (var i=0;i<primaryKeys.length;i++){
		primaryKey+=primaryKeys[i].newValue;
	}
	$(".modal-dialog","#myModalDrilldown").removeClass().addClass("modal-dialog");
	$("#myModalDrilldown").modal("show");
	$(".modal-title","#myModalDrilldown").html("Audit Log");
	$("#poDocView").addClass("row no-min-height").empty().append($("<table/>").attr("id","auditTable").addClass("col-md-12 table table-striped table-hover dc-data-table dataTable no-footer"));
	config.url =serviceCallMethodNames["getDataPagination"].url,
	config.source = tableDetails.source;
	config.database = tableDetails.database;
	config.table = "audit";
	config.filter = '';
	config.html = "#auditTable";
	config.dataColumns = "updatedate,username,displayname,oldvalue,newvalue";
	config.columns =  [{
		"data": "updatedate",
		"defaultContent": "",
		"title": "Updated Date"
	}, {
		"data": "username",
		"defaultContent": "",
		"title": "User"
	}, {
		"data": "displayname",
		"defaultContent": "",
		"title": "Field"
	}, {
		"data": "oldvalue",
		"defaultContent": "",
		"title": "Old Value"
	}, {
		"data": "newvalue",
		"defaultContent": "",
		"title": "Updated Value"
	}];
	setTimeout(function() {
		constructGrid(config,"primarykey = '"+primaryKey+"'");
	}, 200);
};
DataTableEditor.prototype.addRow = function(ele,header,edittype){
	var config,tableDetails,defaultvalue;
	config = profileTableMap[$(ele).attr('data-sequence')].columns;
	tableDetails = {};
	if(profileTableMap[$(ele).attr('data-sequence')].data) {
		tableDetails.source = profileTableMap[$(ele).attr('data-sequence')].data.source;
		tableDetails.database = profileTableMap[$(ele).attr('data-sequence')].data.database;
		tableDetails.table = profileTableMap[$(ele).attr('data-sequence')].data.table;
	}
	var _self = this;
	this.tableDetails = tableDetails;
	$("#myModalDrilldown").modal("show");
	$(".modal-title","#myModalDrilldown").html("Create "+ header);
	$(".modal-dialog","#myModalDrilldown").addClass("width-50p");
	$(".modal-body","#myModalDrilldown").addClass("no-padding-top no-padding-bottom");
	var smartForm = $("<form/>").addClass("smart-form").attr({"novalidate":"novalidate","id":"saveForm"});
	$("#poDocView").addClass("row no-min-height").empty().append(smartForm);
	var colFieldSet = $("<fieldset/>").appendTo(smartForm);
	var colRow = null;
	var trRequest = {};
	config.map(function(obj) {
		trRequest[obj.data] = obj.defaultContent
	});
	if(Object.keys(trRequest).length === 0) {
		showNotification("error", "No rows present");
		return;
	}
	
	for (var i = 0; i < config.length; i++){
	 if(config[i].data!=null){
	
		var colConfig = config[i];
		var editType = colConfig.edittype || "text";
		var defaultValue = '';
		var oldValue = trRequest[colConfig.field];
		var fieldName = colConfig.data;
		var fieldTitle = colConfig.title;
		var isMandatory = colConfig.mandatory;
		var disable = colConfig.disable;
		var isEditable = false;//(colConfig.editable) ? !colConfig.editable : true;
		if(!fieldTitle) {
			continue;
		}
		var colSection = $("<section/>").addClass("col col-6 padding-bottom-10").attr("fieldtype",editType);
		if(edittype=="actionForm"){
			if (i%2 === 1 && i !== config.length-1){
			colRow = $("<div/>").css({"width":"650px"}).addClass("row");
			}
			else{
			colRow.appendTo(colFieldSet);
			}
		}
		else{
			if (i%2 === 0 && i !== config.length-1){
				colRow = $("<div/>").css({"width":"650px"}).addClass("row");
			}
			else{
				colRow.appendTo(colFieldSet);
			}
		}
		colSection.data("colconfig",colConfig);
		/*if(!colConfig.visible){
			colSection.addClass("hide");
		}*/
		colRow.append(colSection);
		if(colConfig.editvalue){
			try{
				defaultValue = eval(colConfig.editvalue);
			}catch (e) {
				defaultValue = colConfig.editvaluel;
				console.log('Static value assigned to ' + fieldTitle);
			}
		}
		if (editType === 'text' || editType === 'number'){
			colSection.append($("<label/>").addClass("label").html(fieldTitle));
			colSection.append($("<span/>").css('padding', '0px').append($("<input/>").addClass("form-control padding-left-10").attr({"type":editType,"field":fieldName,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory}).val(oldValue)));
			colSection.find("input").val(defaultValue);
			if(colConfig.disable){
				colSection.find("input").prop('disabled', "disabled");
			}
		}
		else if (editType === 'select'){
			colSection.append($("<label/>").addClass("label").html(fieldTitle));
			var tempDiv = $("<div/>");
			var configOptions = colConfig.options;
			for (var j=0; j< configOptions.length;j++){
				tempDiv.append($("<option/>").attr("value",configOptions[j].id).html(configOptions[j].text));
			};
			colSection.append($("<span/>").css('padding', '0px').append($("<select/>").attr({"type":editType,"field":fieldName,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory}).prop('disabled', isEditable).append($(tempDiv).html()).val(oldValue)).append($("<i/>")));
			if(colConfig.disable){
				colSection.find("select").prop('disabled', "disabled");
			}
		}
		else if (editType === 'checkbox'){
			var checkboxOptions = {
					"type":"checkbox",
					"oldValue":oldValue,
					"field":fieldName,
					"fieldTitle":fieldTitle
			};
			if (oldValue === "t")
				checkboxOptions.checked = true;
			colSection.append($("<label/>").addClass("checkbox").html(fieldTitle).prepend($("<i/>")).prepend($("<input/>").attr(checkboxOptions).prop('disabled', isEditable)));
		}
		else if (editType === 'date' || editType === 'datestring'){
			var dateEle = $("<input/>").attr({"type":"text","field":fieldName,"defaultvalue":defaultvalue,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory}).val(oldValue);
			colSection.append($("<label/>").addClass("label").html(fieldTitle));
			colSection.append($("<label/>").addClass("input").append(dateEle));
			
			dateEle
				.datepicker({
					defaultDate : "+1w",
					changeMonth : true,
					dateFormat: 'mm/dd/yy'//'yy-mm-dd'
				}).focus();
			if(colConfig.disable){
				colSection.find("input").prop('disabled', "disabled");
			}
		}
		else if (editType === 'lookup'){
			var lookUpEle = $("<span/>").css('padding', '0px').append($("<input/>").addClass("form-control").attr({"type":"text","field":fieldName,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory}));
			colSection.append($("<label/>").addClass("label").html(fieldTitle));
			colSection.append($("<label/>").addClass("input").append(lookUpEle));
			this.lookUpEdit(colConfig,lookUpEle,oldValue);
		}else if (editType === 'txtarea'){
			colSection.append($("<label/>").addClass("label").html(fieldTitle));
			colSection.append($("<span/>").css('padding', '0px').append($("<textarea/>").css('resize', 'none').addClass("form-control").attr({"type":editType,"field":fieldName,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory, rows:'5', cols:'40'}).prop('disabled', isEditable).val(oldValue)));
			if(colConfig.disable){
				colSection.find("textarea").prop('disabled', "disabled");
			}
		}
	 }
	};
	var colFooter = $("<footer/>").appendTo(smartForm);
	colFooter.append($("<button/>").attr({"type":"button","data-dismiss":"modal","area-hidden":"true"}).addClass("btn btn-warning").html("Cancel"));
	colFooter.append($("<button/>").attr({"type":"submit","id":"saveForm"}).addClass("btn btn-primary").html("Create"));
	
	$('#saveForm').validate();
	$('#saveForm').unbind('submit');
	$('#saveForm').submit(function(e){
		e.preventDefault();
		_self.saveForm(null, true);
	});	
}

DataTableEditor.prototype.Create = function(ele,header){
var config,tableDetails,defaultvalue;
config = profileTableMap[$(ele).attr('data-sequence')].columns;
tableDetails = {};
if(profileTableMap[$(ele).attr('data-sequence')].data) {
	tableDetails.source = profileTableMap[$(ele).attr('data-sequence')].data.source;
	tableDetails.database = profileTableMap[$(ele).attr('data-sequence')].data.database;
	tableDetails.table = profileTableMap[$(ele).attr('data-sequence')].data.table;
}
var _self = this;
this.tableDetails = tableDetails;
$("#myModalDrilldown").modal("show");
$(".modal-title","#myModalDrilldown").html("Create "+ header);
$(".modal-dialog","#myModalDrilldown").addClass("width-50p");
$(".modal-body","#myModalDrilldown").addClass("no-padding-top no-padding-bottom");
var smartForm = $("<form/>").addClass("smart-form").attr({"novalidate":"novalidate","id":"saveForm"});
$("#poDocView").addClass("row no-min-height").empty().append(smartForm);
var colFieldSet = $("<fieldset/>").appendTo(smartForm);
var colRow = null;
var trRequest = {};
config.map(function(obj) {
	trRequest[obj.data] = obj.defaultContent
});
if(Object.keys(trRequest).length === 0) {
	showNotification("error", "No rows present");
	return;
}

for (var i = 0; i < config.length; i++){
 if(config[i].data!=null){

	var colConfig = config[i];
	var editType = colConfig.edittype || "text";
	var defaultValue = '';
	var oldValue = trRequest[colConfig.field];
	var fieldName = colConfig.data;
	var fieldTitle = colConfig.title;
	var isMandatory = colConfig.mandatory;
	var disable = colConfig.disable;
	var isEditable = false;//(colConfig.editable) ? !colConfig.editable : true;
	if(!fieldTitle) {
		continue;
	}
	var colSection = $("<section/>").addClass("col col-6 padding-bottom-10").attr("fieldtype",editType);
	if (i%2 === 1 && i !== config.length-1){
		colRow = $("<div/>").css({"width":"650px"}).addClass("row");
	}
	else{
		colRow.appendTo(colFieldSet);
	}
	colSection.data("colconfig",colConfig);
	/*if(!colConfig.visible){
		colSection.addClass("hide");
	}*/
	colRow.append(colSection);
	if(colConfig.editvalue){
		try{
			defaultValue = eval(colConfig.editvalue);
		}catch (e) {
			defaultValue = colConfig.editvaluel;
			console.log('Static value assigned to ' + fieldTitle);
		}
	}
	if (editType === 'text' || editType === 'number'){
		colSection.append($("<label/>").addClass("label").html(fieldTitle));
		colSection.append($("<span/>").css('padding', '0px').append($("<input/>").addClass("form-control padding-left-10").attr({"type":editType,"field":fieldName,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory}).val(oldValue)));
		colSection.find("input").val(defaultValue);
		if(colConfig.disable){
			colSection.find("input").prop('disabled', "disabled");
		}
	}
	else if (editType === 'select'){
		colSection.append($("<label/>").addClass("label").html(fieldTitle));
		var tempDiv = $("<div/>");
		var configOptions = colConfig.options;
		for (var j=0; j< configOptions.length;j++){
			tempDiv.append($("<option/>").attr("value",configOptions[j].id).html(configOptions[j].text));
		};
		colSection.append($("<span/>").css('padding', '0px').append($("<select/>").attr({"type":editType,"field":fieldName,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory}).prop('disabled', isEditable).append($(tempDiv).html()).val(oldValue)).append($("<i/>")));
		if(colConfig.disable){
			colSection.find("select").prop('disabled', "disabled");
		}
	}
	else if (editType === 'checkbox'){
		var checkboxOptions = {
				"type":"checkbox",
				"oldValue":oldValue,
				"field":fieldName,
				"fieldTitle":fieldTitle
		};
		if (oldValue === "t")
			checkboxOptions.checked = true;
		colSection.append($("<label/>").addClass("checkbox").html(fieldTitle).prepend($("<i/>")).prepend($("<input/>").attr(checkboxOptions).prop('disabled', isEditable)));
	}
	else if (editType === 'date' || editType === 'datestring'){
		var dateEle = $("<input/>").attr({"type":"text","field":fieldName,"defaultvalue":defaultvalue,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory}).val(oldValue);
		colSection.append($("<label/>").addClass("label").html(fieldTitle));
		colSection.append($("<label/>").addClass("input").append(dateEle));
		
		dateEle
			.datepicker({
				defaultDate : "+1w",
				changeMonth : true,
				dateFormat: 'mm/dd/yy'//'yy-mm-dd'
			}).focus();
		if(colConfig.disable){
			colSection.find("input").prop('disabled', "disabled");
		}
	}
	else if (editType === 'lookup'){
		var lookUpEle = $("<span/>").css('padding', '0px').append($("<input/>").addClass("form-control").attr({"type":"text","field":fieldName,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory}));
		colSection.append($("<label/>").addClass("label").html(fieldTitle));
		colSection.append($("<label/>").addClass("input").append(lookUpEle));
		this.lookUpEdit(colConfig,lookUpEle,oldValue);
	}else if (editType === 'txtarea'){
		colSection.append($("<label/>").addClass("label").html(fieldTitle));
		colSection.append($("<span/>").css('padding', '0px').append($("<textarea/>").css('resize', 'none').addClass("form-control").attr({"type":editType,"field":fieldName,"fieldTitle":fieldTitle,"oldValue":oldValue,"required":isMandatory, rows:'5', cols:'40'}).prop('disabled', isEditable).val(oldValue)));
		if(colConfig.disable){
			colSection.find("textarea").prop('disabled', "disabled");
		}
	}
 }
};
var colFooter = $("<footer/>").appendTo(smartForm);
colFooter.append($("<button/>").attr({"type":"button","data-dismiss":"modal","area-hidden":"true"}).addClass("btn btn-warning").html("Cancel"));
colFooter.append($("<button/>").attr({"type":"submit","id":"saveForm"}).addClass("btn btn-primary").html("Save"));

$('#saveForm').validate();
$('#saveForm').unbind('submit');
$('#saveForm').submit(function(e){
	e.preventDefault();
	_self.saveForm(null, true);
});	
}