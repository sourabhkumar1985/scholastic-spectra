function searchEntityDetailsObj(){
	var THIS =this;
	enableLoading();
	this.getEventDetails =function(eventid){
		callAjaxService("eventDetailsView", function(response){callBackSuccessEventDetailsView(eventid,response);},
				function() {
					alert("error");
				}, null, "GET", "html");
	};
	var callBackSuccessEventDetailsView = function(eventid,response){
		$("#wid-id-formView").removeClass("hide");
		$("#searchProfile").addClass("hide");
		$("#formView").empty();
		$("#formView").html(response);
		var request = {
				"eventid":eventid
		};
		callAjaxService("getEventByID", callBackSuccessGetEventByID,callBackFailure,request,"POST");
	};
	function callBackSuccessGetEventByID(response){
		response = response[0];
		 THIS.eventID =response["eventId"];
		 $("#workflow").addClass("padding-left-200");
		$('#formView').find('[name="eventId"]').html(response["eventId"]);
		$('#formView').find('[name="eventCase"]').html(response["caseName"]);
		$('#formView').find('[name="eventDetails"]').html(response["eventDetails"]);
		//$('#formView').find('[name="eventPhysiciansName"]').html(response["physicians"][0]["name"]);
		$('#formView').find('[name="eventStart"]').html(response["startDate"]);
		$('#formView').find('[name="eventEnd"]').html(response["endDate"]);
		if (response["locations"] && response["locations"]["id"] && response["locations"]["id"]!= null){
			$('#formView').find('[name="locationId"]').html(response["locations"]["id"]);
			$('#formView').find('[name="locationAddress"]').html(response["locations"]["address"]);
			$('#formView').find('[name="locationPractice"]').html(response["locations"]["practice"]);
		}
		if (response["physicians"] && response["physicians"].length > 0){
			$('#formView').find('[name="physiciansId"]').html(response["physicians"][0]["id"]);
			$('#formView').find('[name="physiciansName"]').html(response["physicians"][0]["name"]);
		}
		if (response["patients"] && response["patients"].length > 0){
			$('#formView').find('[name="patientId"]').html(response["patients"][0]["id"]);
			$('#formView').find('[name="patientGender"]').html(response["patients"][0]["gender"]);
			$('#formView').find('[name="patientAge"]').html(response["patients"][0]["age"]);
			$('#formView').find('[name="patientHeight"]').html(response["patients"][0]["height"]);
			$('#formView').find('[name="patientWeight"]').html(response["patients"][0]["weight"]);
		}
		if (response["equipment"] && response["equipment"] != null){
			if (response["equipment"]["supplemental"] && response["equipment"]["supplemental"] != null){
				$('#formView').find('[name="implantsOrientation"]').html(response["equipment"]["supplemental"]["orientation"]);
				$('#formView').find('[name="patientWeight"]').html(response["equipment"]["supplemental"]["size"]);
			}
			if (response["equipment"]["implants"] && response["equipment"]["implants"] != null && response["equipment"]["implants"].length >  0){
				$('#formView').find('[name="implantsPart"]').html(response["equipment"]["implants"][0]["part"]);
				$('#formView').find('[name="implantsDescription"]').html(response["equipment"]["implants"][0]["desc"]);
			}
		}
		callAjaxService("getAllWorkFlowStatus", callBackSuccessGetAllWorkFlowStatus,callBackFailure);
	};
	var callBackSuccessGetAllWorkFlowStatus = function(response){	
		var steps = $("<ul/>").addClass("steps");
		for (var i=0; i< response.length ;i++){
			var workFlowli = $("<li/>");
			workFlowli.append($("<span/>").addClass("badge badge-info").html(i+1));
			workFlowli.append($("<span/>").html(response[i].currentState));
			//if (i != (response.length-1))
				workFlowli.append($("<span/>").addClass("chevron"));
			steps.append(workFlowli);
		};
		var wizardDiv = $("<div/>").addClass("wizard").append(steps);
		var workFlowDiv = $("<div/>").addClass("fuelux").append(wizardDiv);
		$("#workflow").append(workFlowDiv);	
		var request = {
				"entityid":THIS.eventID
		};
		callAjaxService("getWorkFlowStatus", callBackSuccessGetWorkFlowStatus,callBackFailure,request,"POST");
	};
	var callBackSuccessGetWorkFlowStatus = function(response){
		$(".wizard").find("li:contains('"+response["state"]+"')").addClass("active");
		var request = {
				"entityid":THIS.eventID,
				"role":"admin"
		};
		callAjaxService("getActionButton", callBackSuccessGetActionButton,callBackFailure,request,"POST");
	};
	var callBackSuccessGetActionButton = function(response){
		$("#actionButtons").empty();
		for(var i=0; i< response.length;i++){
			$("#actionButtons").append($("<button/>").addClass("btn btn-primary btn-sm").html(response[i].action).data("entity-id",THIS.eventID).unbind("click").bind("click",function(){
				procedeAction(this);
			}));
		}
		$("#actionButtons").append($("<button/>").addClass("btn btn-danger btn-sm").html("Cancel").unbind("click").bind("click",function(){
			$("#wid-id-formView").toggleClass("hide");
			$("#searchProfile").toggleClass("hide");
		}));
		disableLoading();
	};
	var procedeAction = function(obj){
		$("#myModalDrilldown").modal('show');
		$("#poDocView").css("min-height","auto");
		$("#myModalDrilldown > .modal-dialog").addClass("width-500");
		$("#btnCreate").data({"entity-id":$(obj).data("entity-id"),"entity-status":$(obj).html()}).unbind("click").bind("click",function(){
			submitProcedeAction(this);
		});
	};
	var submitProcedeAction = function(obj){
		var request = {
				"entityid":$(obj).data("entity-id"),
				"user":"mani",
				"role": "admin",
				"action":$(obj).data("entity-status"),
				"comments":$("[name=remarks]").val()
		};
		callAjaxService("applyWorkFlowAction", callBackSuccessApplyWorkFlowAction,callBackFailure,request,"POST");
	};
	var callBackSuccessApplyWorkFlowAction = function(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		} else {
			$('#myModalDrilldown').modal('hide');
			$("#frmSankeyFilter input").val("");
			showNotification("success",response["message"]);
			THIS.getEventDetails(THIS.eventID);
		};
	};
};
var searchEntityDetails =  new searchEntityDetailsObj();