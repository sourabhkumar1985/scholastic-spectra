var actionCallbackHandlerObj = function() {
	var THIS = this;

	/**
	 * Downloads CSV Data based on 'name' of Report
	 */
	THIS.downloadCSV = function(name, tableId) {
		var filter = "1=1";
		if (tableId) {
			var datatableConfig = dataAnalyser.config.filter(function(d) {
				return d.id.indexOf(tableId) > -1;
			})[0];
			if (datatableConfig !== undefined) {
				datatableConfig = datatableConfig.charts.filter(function(a) {
					return a.alias.indexOf(tableId);
				})[0];
				if (datatableConfig.data.filter) {
					filter += ' And ' + datatableConfig.data.filter;
				}
			}
			if (localStorage["datatablefilter_" + tableId]) {
				filter += ' And ' + localStorage["datatablefilter_" + tableId];
			}
			if (localStorage["datatablecolumnfilter_" + tableId]) {
				filter += ' And '
						+ localStorage["datatablecolumnfilter_" + tableId];
			}
		}
		var searchValue = $('.dataTables_filter input').val();
		if (searchValue)
			searchValue = searchValue.toLowerCase();
		var request = {
			"searchValue" : "'%" + searchValue + "'%",
			"filter" : filter,
			"name" : name,
			"fileType" : "CSV"
		};

		var $preparingFileModal = $("#preparing-file-modal");
		$preparingFileModal.dialog({
			modal : true
		});

		$.fileDownload('rest/dataAnalyser/adHocReportDownload', {
			successCallback : function() {
				$preparingFileModal.dialog('close');
			},
			failCallback : function(responseHtml) {
				$preparingFileModal.dialog('close');
				$("#error-modal").html($(responseHtml).text());
				$("#error-modal").dialog({
					modal : true
				});
			},
			httpMethod : "POST",
			data : {
				"data" : JSON.stringify(request)
			}
		});
	};

	/**
	 * Downloads Excel Data based on 'name' of Report
	 */
	THIS.downloadExcel = function(name, tableId) {
		var filter = " 1=1";
		if (localStorage["datatablefilter"]) {
			filter += ' And ' + localStorage["datatablefilter"];
		}
		if (tableId) {

			var datatableConfig = dataAnalyser.config.filter(function(d) {
				return d.id.indexOf(tableId) > -1;
			})[0];
			if (datatableConfig !== undefined) {
				datatableConfig = datatableConfig.charts.filter(function(a) {
					return a.alias.indexOf(tableId);
				})[0];
				var profileId = datatableConfig.alias.split('#');
				tableId = profileId[1];
				if (datatableConfig.data.filter) {
					filter += ' And ' + datatableConfig.data.filter;
				}
			}

			if (localStorage["datatablefilter_" + tableId]) {
				filter += ' And ' + localStorage["datatablefilter_" + tableId];
			}
			if (localStorage["datatablecolumnfilter_" + tableId]) {
				filter += ' And '
						+ localStorage["datatablecolumnfilter_" + tableId];
			}
		}
		// changes for report download without data table
		else {
			var graphing = null;
			graphing = new Graphing();
			filter = graphing.constructFilterQuery();
			filter += ' And ' + localStorage["reportFilter"];
		}
		var searchValue = $('.dataTables_filter input').val();
		if (searchValue)
			searchValue = searchValue.toLowerCase();
		var request = {
			"searchValue" : searchValue === undefined ? "" : "'%" + searchValue
					+ "'%",
			"filter" : filter,
			"name" : name,
			"fileType" : "EXCEL"
		};

		var $preparingFileModal = $("#preparing-file-modal");
		$preparingFileModal.dialog({
			modal : true
		});

		$.fileDownload('rest/dataAnalyser/adHocReportDownload', {
			successCallback : function() {
				$preparingFileModal.dialog('close');
			},
			failCallback : function(responseHtml) {
				$preparingFileModal.dialog('close');
				$("#error-modal").html($(responseHtml).text());
				$("#error-modal").dialog({
					modal : true
				});
			},
			httpMethod : "POST",
			data : {
				"data" : JSON.stringify(request)
			}
		});
	};

	THIS.extractMatchingText = function(event) {
		var regExp = $('textarea[name="Rawfilter"]').val().split('~')[1];
		if (regExp && regExp.trim()) {
			enableLoading();
			$("#myModalDrilldown").modal("show");
			var excelBtn = $('<a title="Download Excel" class="btn btn-success btn-sm pull-right download-excel-file" style="margin-right: 20px"> <i class="fa fa-lg fa-download"></i> Excel </a>');
			var inputText = $('<input type="text" class="pull-right extractTextField" placeholder="Enter your type name" style="width: 225px; padding-left: 10px; margin-right: 15px;">')
			var button = $('<input title:"Save Extracted Text" class="btn btn-sm btn-success pull-right saveExtractedText" style="margin-right: 15px" type="button" value="Save" disabled/>');
			$(".modal-header #myModalLabel").text(
					'Extracted: ' + dataAnalyser.table).css({
				'display' : 'inline',
				'height' : '50px'
			}).append(excelBtn, button, inputText);
			$(".modal-body").css({
				'position' : 'relative',
				'overflow-y' : 'auto'
			});
			$(".modal-header").css({
				'height' : '50px'
			});
			$("#poDocView").addClass("no-min-height").empty();
			$("#poDocView").append($("<table/>").attr({
				"id" : "materialDatatable"
			}).addClass("table table-striped table-hover dc-data-table"));

			var config = {
				html : '#materialDatatable',
				url : serviceCallMethodNames["getDataPagination"].url,
				source : dataAnalyser.sourceType
			};

			columnConfiguration = [ {
				data : "file_name",
				title : "File Name"
			}, {
				data : "raw_text",
				title : "Raw Text"
			}, {
				data : "regexp_matches",
				title : "Reg Exp"
			} ];
			config["filter"] = "1 = 1 ";
			config["database"] = dataAnalyser.database;
			config["table"] = dataAnalyser.table;
			config["source"] = dataAnalyser.sourceType;
			config["columns"] = columnConfiguration;
			if ($('textarea[name="Rawfilter"]').val().split('~')[0]
					.indexOf('lower') != -1) {
				config["query"] = "SELECT file_name, raw_text, regexp_matches(lower(raw_text), "
						+ regExp
						+ ") FROM "
						+ dataAnalyser.table
						+ " where <FILTER>";
			} else {
				config["query"] = "SELECT file_name, raw_text, regexp_matches(raw_text, "
						+ regExp
						+ ") FROM "
						+ dataAnalyser.table
						+ " where <FILTER>";
			}
			config.dataColumns = $.map(columnConfiguration, function(val) {
				if (val.data != 'regexp_matches')
					return val.data;
			}).join(',');
			constructGrid(config);

			$(".extractTextField").on('input', function() {
				THIS.extractTextField = "";
				if ($(this).val().trim().length > 0) {
					$(".saveExtractedText").prop('disabled', false);
					THIS.extractTextField = $(this).val().trim();
				} else {
					$(".saveExtractedText").prop('disabled', true);
				}
			});

			$(".saveExtractedText").on('click', function() {
				saveExtractedText();
			});

			$(".download-excel-file").unbind('click').bind('click', {
				"tableconfig" : config
			}, function(e) {
				downloadExcelFile(e);
			});

			function saveExtractedText() {
				var request = {
					"source" : dataAnalyser.sourceType,
					"database" : dataAnalyser.database,
					"table" : dataAnalyser.table,
					"type" : THIS.extractTextField,
					"regexp" : regExp
				};
				enableLoading();
				callAjaxService("saveExtractedText", callBackSaveExtractedText,
						callBackFailure, request, "POST");

				function callBackSaveExtractedText(response) {
					showNotification("success", response.message);
					$("#myModalDrilldown").modal("hide");
				}
			}

			function downloadExcelFile(event) {
				var config = event.data.tableconfig;
				var tableSorting = $(config.html).dataTable().fnSettings().aaSorting;
				var columns = "";
				if (config.columns && config.columns.length > 0) {
					for (var i = 0; i < config.columns.length; i++) {
						(i > 0) ? columns += "," : "";
						columns += config.columns[i].data + ' AS "'
								+ config.columns[i].title + '"';
					}
				}
				var request = {
					"columns" : columns,
					"databaseColumns" : config.dataColumns,
					"orderField" : tableSorting[0][0],
					"orderType" : tableSorting[0][1],
					"searchValue" : $('#poDocView .dataTables_filter input')
							.val(),
					"source" : config.source,
					"database" : config.database,
					"table" : config.table,
					"filter" : config.filter,
					"displayName" : "Extracted RegExp"
				};
				if ($('textarea[name="Rawfilter"]').val().split('~')[0]
						.indexOf('lower') != -1) {
					request["query"] = "SELECT file_name, raw_text, regexp_matches(lower(raw_text), "
							+ regExp + ") FROM " + dataAnalyser.table + "";
				} else {
					request["query"] = "SELECT file_name, raw_text, regexp_matches(raw_text, "
							+ regExp + ") FROM " + dataAnalyser.table + "";
				}
				enableLoading();
				$.fileDownload('rest/dataAnalyser/datatableExcelDownload',
						{
							successCallback : function() {
								showNotification("success",
										"Downloaded successfully.");
							},
							failCallback : function(responseHtml) {
								disableLoading();
								showNotification("error", $(responseHtml)
										.text());
							},
							httpMethod : "POST",
							data : {
								"data" : encodeURI(JSON.stringify(request)),
								"isEncoded" : "true"
							}
						});
			}
		} else {
			showNotification("error", "Please enter your filter expression");
		}
	};

	var callbacksendmail = function() {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			showNotification("success", "Email sent successfully");
		}
	};

	var emailNotification = function(orderno) {
		var request = {
			templatename : "order_confirmation",
			datajson : JSON.stringify({
				orderno : orderno,
				orderjson : THIS.orderjson
			})
		};
		callAjaxService("sendmail", callbacksendmail, callBackFailure, request,
				"POST");
	};

	var callbackAddorder = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			showNotification("success", "Order added successfully");
			emailNotification(response.orderno);
			$("#myModalLabel", '#myModalDrilldown').find("span:eq(0)").html(
					response.orderno);
			$("#poDocView").find("table").find("input").hide();
			$("#poDocView").find("table").find("span").show();
			$("#printView").find("a").remove();
		}
	}

	var addOrder = function() {
		var orderjson = {
			location : THIS.PartView.orderInfo.location,
			salesRep : THIS.PartView.orderInfo.repName,
			createdBy : System.userName,
			// startDate: THIS.PartView.orderInfo.startDate,
			// endDate: THIS.PartView.orderInfo.endDate,
			parts : []
		}, request = {};
		$("#poDocView").find("tbody").find("tr").each(function(index) {
			orderjson.parts.push({
				partNumber : THIS.PartView.parts[index].partNumber,
				grossQty : THIS.PartView.parts[index].grossQty,
				trunkQty : $(this).find("input").val(),
				netQty : $(this).find('td:last').html(),
				cases : THIS.PartView.parts[index].cases.toString(),
				partDescription : THIS.PartView.parts[index].partDesc,
			});
		});
		THIS.orderjson = orderjson;
		request.orderjson = JSON.stringify(orderjson);
		callAjaxService("addorder", callbackAddorder, callBackFailure, request,
				"POST");
	};

	var callBackSuccessEventDetailsView = function(response) {
		$("#formView").empty().html(response);
		eventDetailsObj.Load();
	};
	var caseViewCallBack = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.PartView = response;
			$('#myModalDrilldown').modal('show');
			$("#myModalLabel", "#myModalDrilldown").html("");
			$("#myModalLabel", '#myModalDrilldown')
					.append(
							$("<div/>")
									.append($("<strong/>").html("Order No:"))
									.append($("<span/>").addClass("text-right")))
					.append(
							$("<div>")
									.addClass("row")
									.append(
											$("<strong/>").addClass(
													"col-md-1 text-right")
													.html("Location:"))
									.append(
											$("<span/>")
													.addClass("col-md-2")
													.html(
															THIS.PartView.orderInfo.location))
									.append(
											$("<strong/>").addClass(
													"col-md-2 text-right")
													.html("Sales Rep:"))
									.append(
											$("<span/>")
													.addClass("col-md-2")
													.html(
															THIS.PartView.orderInfo.repName))
									.append(
											$("<strong/>").addClass(
													"col-md-2 text-right")
													.html("Date Range:"))
									.append(
											$("<span/>")
													.addClass("col-md-3")
													.html(
															THIS.PartView.orderInfo.startDate
																	.replace(
																			/-/g,
																			"/")
																	+ " - "
																	+ THIS.PartView.orderInfo.endDate
																			.replace(
																					/-/g,
																					"/"))));
			var resTable = $("<table/>").addClass(
					"table table-bordered table-striped");
			var resDiv = $("<div/>").addClass("table-responsive").append(
					resTable);
			$("#poDocView").addClass("no-min-height").empty().append(resDiv);
			$(".modal-footer").remove();
			resTable.append($("<thead>").append(
					$("<tr/>").append($("<th/>").html("#")).append(
							$("<th/>").html("Part Number")).append(
							$("<th/>").html("Part Description")).append(
							$("<th/>").html("Cases")).append(
							$("<th/>").html("Gross Qty")).append(
							$("<th/>").html("Enter Trunk Stock")).append(
							$("<th/>").html("Net Qty"))));
			for (var i = 0; i < response.parts.length; i++) {
				resTable
						.append($("<tr/>")
								.append($("<td/>").html(i + 1))
								.append(
										$("<td/>").html(
												response.parts[i].partNumber))
								.append(
										$("<td/>").html(
												response.parts[i].partDesc))
								.append(
										$("<td/>").html(
												response.parts[i].cases
														.join(",")))
								.append(
										$("<td/>").html(
												response.parts[i].grossQty))
								.append(
										$("<td/>")
												.append(
														$("<span/>")
																.html(
																		response.parts[i].trunkStock)
																.hide())
												.append(
														$("<input/>")
																.val(
																		response.parts[i].trunkStock)
																.on(
																		"blur",
																		function() {
																			$(
																					this)
																					.siblings()
																					.html(
																							$(
																									this)
																									.val());
																			var trunkStock = parseInt($(
																					this)
																					.parent()
																					.prev()
																					.html())
																					- parseInt($(
																							this)
																							.val());
																			if (trunkStock > 0) {
																				$(
																						this)
																						.parent()
																						.next()
																						.html(
																								trunkStock);
																			} else {
																				$(
																						this)
																						.parent()
																						.next()
																						.html(
																								0);
																			}
																		})))
								.append(
										$("<td/>")
												.html(
														response.parts[i].grossQty
																- response.parts[i].trunkStock)));
			}
			;
			$("#printView").append(
					$("<div/>").addClass("modal-footer").append(
							$("<a/>").unbind("click").bind("click", function() {
								addOrder();
							}).addClass("btn btn-success btn-lg pull-right")
									.html("Finalize Planned Shipment")));
		}
		;
	};

	THIS.generateShipment = function() {
		var selectedID = [];
		$("#wid-id-datatable_1-temp").find("input:checkbox").each(function() {
			if ($(this).is(":checked")) {
				selectedID.push($(this).parents("tr").find("td:first").html());
			}
		});
		if (selectedID.length === 0) {
			alert("Select at least one row."); // margin-left-25 margin-top-10
			return;
		}
		var request = {
			"caseids" : selectedID.join(",")
		};
		callAjaxService("getPartView", caseViewCallBack, callBackFailure,
				request, "POST");
	};
	THIS.createCase = function() {
		callAjaxService("eventDetailsView", callBackSuccessEventDetailsView,
				callBackFailure, null, "GET", "html");
	};
	THIS.runCategoryRule = function() {
		var selectedID = [];
		$("#wid-id-datatable_1-temp").find("input:checkbox").each(function() {
			if ($(this).is(":checked")) {
				selectedID.push($(this).parents("tr").find("td:first").html());
			}
		});
		if (selectedID.length === 0) {
			alert("Select at least one row."); // margin-left-25 margin-top-10
			return;
		}
		var condition = dataAnalyser.profiling.grapher.constructFilterQuery({
			"categoryl0" : "categoryl0"
		}) || null;
		var request = {
			"rules" : selectedID.join("','"),
			"condition" : condition
		};
		callAjaxService("updateCategories", callBackSuccessUpdateCategories,
				callBackFailure, request, "POST");
	};
	THIS.runCSRule = function() {
		var selectedID = [];
		$("#wid-id-datatable_1-temp").find("input:checkbox").each(function() {
			if ($(this).is(":checked")) {
				selectedID.push($(this).parents("tr").find("td:first").html());
			}
		});
		if (selectedID.length === 0) {
			alert("Select at least one row."); // margin-left-25 margin-top-10
			return;
		}
		var condition = dataAnalyser.profiling.grapher.constructFilterQuery({
			"cspeciality" : "cspeciality"
		}) || null;
		var request = {
			"rules" : selectedID.join("','"),
			"condition" : condition
		};
		callAjaxService("updateCS", callBackSuccessUpdateCategories,
				callBackFailure, request, "POST");
	};
	THIS.exportRegulatoryAffairs = function(reportName, tableId) {
		event.preventDefault();
		event.stopPropagation();
		$("#myModalDrilldown").modal("show");
		$("#poDocView").css({
			"min-height" : "100px"
		}).empty();
		var headerInfo = $("<div/>").addClass("col-sm-5").html("Download ");
		$("#printView #myModalLabel").addClass("row").html(headerInfo);
		var section = $("<div/>").addClass("row smart-form col-sm-11").append(
				$("<section/>").addClass("col col-6 col-sm-6").append(
						$("<div/>").addClass("note").append(
								$("<strong/>").html("ERP Item#"))).append(
						$("<label/>").addClass("input width-100per").attr({
							"id" : "itemNoSourceConsolidation"
						}))).append(
				$("<section/>").addClass("col col-6 col-sm-6").append(
						$("<div/>").addClass("note").append(
								$("<strong/>").html("Status"))).append(
						$("<label/>").attr({
							"id" : "statusSourceConsolidation"
						}).addClass("input width-100per")));
		// localStorage["datatablefilter"];
		$("#poDocView").append(section);
		constructDropDown(
				"#itemNoSourceConsolidation",
				"select distinct item_no,'' from l1_source_consolidation where item_no like '%<Q>%'",
				true, null, null, "Select ERP Item#", "postgreSQL", "QFIND_RA",
				{
					buttonWidth : "350px"
				});
		constructDropDown(
				"#statusSourceConsolidation",
				"select distinct status ,'' from l1_source_consolidation where status  like '%<Q>%'",
				false, null, null, "Select Status", "postgreSQL", "QFIND_RA", {
					buttonWidth : "350px"
				});
		$("#poDocView").append(
				$("<footer/>").addClass("rightalign").append(
						$("<a/>").attr({
							"name" : "exceldownload"
						}).addClass("btn marginRight10 btn-success").append(
								$("<i>").addClass("fa fa-download")).append(
								" Excel").data("downloadtype", "Excel"))
						.append(
								$("<a/>").attr({
									"name" : "csvdownload"
								}).addClass("btn marginRight10 btn-success")
										.append(
												$("<i>").addClass(
														"fa fa-download"))
										.append(" CSV").data("downloadtype",
												"CSV")));
		$("#poDocView")
				.find("a[name='exceldownload'],a[name='csvdownload']")
				.unbind("click")
				.bind(
						"click",
						function() {
							localStorage["datatablefilter"] = "";
							var type = $(this).attr("name"), filter = " 1=1 ";
							var selectedStatus = $("#statusSourceConsolidation")
									.find("select").val();
							var selectedItem = $("#itemNoSourceConsolidation")
									.find("select").val();
							if (selectedItem && selectedItem.length > 0) {
								filter = " item_no in("
										+ selectedItem.map(function(o) {
											return "'" + o + "'"
										}).toString() + ")"
							}
							if (selectedStatus) {
								filter += " and  status in ('" + selectedStatus
										+ "')";
							}
							localStorage["datatablefilter"] = filter;
							if (type === "exceldownload") {
								THIS.downloadExcel(reportName, tableId);
							} else {
								THIS.downloadCSV(reportName, tableId);
							}
						});
	};
	THIS.importExcel = function(primaryKey, roles) {
		if (roles && roles.length > 0) {
			var isExist = false;
			for (var i = 0; i < roles.length; i++) {
				if (System.roles.indexOf(roles[i]) > -1) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				showNotification("error",
						"You are not authorised to import data.");
				return;
			}
		}
		var defaultValueConfig = [];
		if (!primaryKey) {
			showNotification("error", "Primary Key not configured.");
		}
		var columns = $(event.currentTarget).data("columns"), connfigColumns = [], isValid = false;
		var defaultValueColumns = columns.filter(function(obj) { 
			return (obj.editvalue)
		});

		columns = columns.filter(function(obj) {
			return (obj.editable || primaryKey.indexOf(obj.data) > -1)
		});
		if (defaultValueColumns && defaultValueColumns.length > 0) {
			for (var i = 0; i < defaultValueColumns.length; i++) {
				if (defaultValueColumns[i].data) {
					var defaultColVal;
					if (defaultValueColumns[i].editvalue.indexOf("function") > -1) {
						defaultColVal = eval(defaultValueColumns[i].editvalue);
					} else {
						defaultColVal = defaultValueColumns[i].editvalue;
					}
					defaultValueConfig.push({
						key : defaultValueColumns[i].data,
						value : defaultValueColumns[i].title,
						defaultvalue : defaultColVal
					});
				}
			}
		}
		if (columns && columns.length > 0) {
			for (var i = 0; i < columns.length; i++) {
				if (columns[i].data) {
					connfigColumns.push({
						key : columns[i].data,
						value : columns[i].title,
					});
				}
			}
		} else {
			showNotification("notify",
					"Editable columns not exist in the table.");
			return;
		}
		$("#myModalUpload").modal('show');
		$('.fileUpload')
				.change(
						function() {
							if ($(this).val()) {
								var extension = $(this).val().split('.').pop()
										.toLowerCase();
								if ([ "xls", "xlt", "xlm", "xlsx" ]
										.indexOf(extension) === -1) {
									showNotification("notify",
											"Upload only excel file.");
									return;
								} else {
									isValid = true;
									$(this).parent().next().val(
											$(this).val().replace(
													/^C:\\fakepath\\/i, ''));
								}
							}
						});
		$("#frmModalExportImport")
				.unbind('submit')
				.submit(
						function(e) {
							e.preventDefault();
							if (!isValid)
								return;
							enableLoading();
							var formData = new FormData(document
									.getElementById("frmModalExportImport"));
							formData.append("env", dataAnalyser.sourceType);
							formData.append("db", dataAnalyser.database);
							formData.append("table", dataAnalyser.table);
							formData.append("configcolumns", JSON
									.stringify(connfigColumns));
							formData.append("defaultValueConfig", JSON
									.stringify(defaultValueConfig));
							var primarykeyslist = {};
							primarykeyslist.primaryKeyNamesList = primaryKey
									.split(",");
							formData.append("primarykeyslist", JSON
									.stringify(primarykeyslist));
							$
									.ajax(
											{
												url : serviceCallMethodNames["fileUpload"].url,
												type : "POST",
												data : formData,
												enctype : 'multipart/form-data',
												processData : false,
												contentType : false
											})
									.done(
											function(response) {
												disableLoading();
												$("#myModalUpload").modal(
														'hide');
												if (!response)
													showNotification("error",
															"Error while importing file");
												else if (response
														&& response.isException) {
													showNotification(
															"error",
															response.customMessage);
													return;
												} else {
													showNotification("success",
															response.message);
													function callbackSucessClearCache() {
														dataAnalyser.profiling.grapher
																.refreshTable();
													}
													callAjaxService(
															"clearCache",
															callbackSucessClearCache,
															callBackFailure,
															null, "POST");
												}
											});
							return false;
						});
	};
	this.clearDraftCS = function() {
		callAjaxService("clearDraftCS", callBackSuccessUpdateCategories,
				callBackFailure, null, "GET");
	};
	this.clearDraftCategories = function() {
		callAjaxService("clearDraftCategory", callBackSuccessUpdateCategories,
				callBackFailure, null, "GET");
	};
	var callBackSuccessUpdateCategories = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			showNotification("success", response.message);
			$("#applyFilter").trigger("click");
		}
	};
	this.applyCategoryRules = function() {
		callAjaxService("applyCategoryRules", callBackSuccessUpdateCategories,
				callBackFailure, null, "GET");
	};
	this.applyCSRules = function() {
		callAjaxService("applyCSRules", callBackSuccessUpdateCategories,
				callBackFailure, null, "GET");
	};

	this.updateIntegraCategories = function(categories, tableName, dbName,
			primaryKeyName, columnName, categoryRequest) {
		var request = JSON.parse(categoryRequest);
		enableLoading();
		callAjaxService(
				"getIntegraCategories",
				function(response) {
					if (response && response.isException) {
						showNotification("error", response.customMessage);
						disableLoading();
						return;
					}
					disableLoading();
					categories = response;

					var selectedID = [];
					$("#wid-id-datatable_1-temp").find("input:checkbox").each(
							function() {
								if ($(this).is(":checked")) {
									selectedID.push($(this).attr('refid'));
								}
							});
					if (selectedID.length === 0) {
						alert("Select at least one row."); // margin-left-25
															// margin-top-10
						return;
					}
					$("#myModalDrilldown #myModalLabel").html('Categories')
							.append($("<br/>"));
					var dropDown = $('<select id="selectedItem" style="margin:5px" />');
					var categoriesLength = categories.length;
					for (var i = 0; i < categoriesLength; i++) {
						var data = '<option>' + categories[i] + '</option>';
						dropDown.append(data);
					}
					$("#poDocView")
							.html('Select Category')
							.append(dropDown)
							.append(
									$("<button class='btn btn-sm btn-primary integra-select-value'> Save </button>"));
					$('#poDocView').css('min-height', '0px');
					$("#myModalDrilldown").modal("show");
					$('.integra-select-value')
							.click(
									function() {
										var selectedOption = $('#selectedItem')
												.val();
										var postRequest = {
											"primaryKeys" : selectedID
													.join(","),
											"value" : selectedOption,
											"columnName" : columnName,
											"primaryKeyColumnName" : primaryKeyName,
											"tableName" : tableName,
											"dbName" : dbName
										};
										enableLoading();
										callAjaxService(
												"updateIntegraCatalogs",
												function(response) {
													$("#myModalDrilldown")
															.modal("hide");
													if (response
															&& response.isException) {
														showNotification(
																"error",
																response.customMessage);
														disableLoading();
														return;
													}
													disableLoading();
													showNotification("success",
															"Categories Updated Successfully");
													if ($("#applyFilter").length > 0) {
														$("#applyFilter")
																.trigger(
																		"click")
													}
												}, callBackFailure,
												postRequest, "POST")
									});
				}, callBackFailure, request, "POST")

	};

	this.indexSolr = function(id) {
		if (!id) {
			return;
		}
		enableLoading();
		callAjaxService("getProfileConfig", function(response) {
			if (response && response.isException) {
				showNotification("error", response.customMessage);
				disableLoading();
				return;
			}
			disableLoading();
			if (response) {
				if (response.config) {
					var search = JSON.parse(response.config);
					if (search.length > 0) {
						search = search[0];
					}
					if (search && search.config) {
						var searchConfigObject = JSON.parse(search.config);
						var configObj = searchConfigObject.indexConfigObj;
						if (configObj) {
							enableLoading();
							callAjaxService("indexSolr", function(response) {
								disableLoading();
								if (response && response.isException) {
									showNotification("error",
											response.customMessage);
									return;
								} else {
									showNotification("success",
											response.message);
								}
							}, callBackFailure, {
								config : JSON.stringify(configObj),
								filterColumn : JSON
										.stringify(configObj.filterColumn)
							}, "POST");
						}
					}
				}
			}
		}, callBackFailure, {
			"id" : id
		}, "POST");
	};

	this.openImageInPopUp = function(headding, link, lookUpName) {
		$("#myModalDrilldown #myModalLabel").html(headding).append($("<br/>"));
		link = lookUpTable.get(lookUpName) + link;
		$("#poDocView")
				.html(
						'<div class="col-sm-12 margin-top-10"><div class="text-center padding-10"><img style="height:400px"  src="'
								+ link + '" /></div></div>');
		$('#poDocView img').elevateZoom({
			zoomType : "lens",
			minZoomLevel : 0.5,
			lensShape : "round",
			lensSize : 300,
			scrollZoom : true,
			customClass : "detailImg"
		});
		$("#poDocView").css("min-height", "550px");
		$("#myModalDrilldown").modal("show");
		$('#btnSaveL3').hide();
		$('#btnPrintL3').hide();
		$('#tableConfigTab').hide();
		$('#myModalDrilldown').on('hidden.bs.modal', function() {
			$('.zoomContainer').removeClass('hide');
			$('.detailImg').remove();
		});

	};

	this.evidenceApproveUser = function() {
		var query = "update users set status = 'Approved', enabled = '1' where status ='Pending' and ";
		var wheres = [];
		var checkboxCount = 0;
		var emails = [];
		var emailSubject = lookUpTable.get("evdApprovedSubject");
		var emailContent = lookUpTable.get("evdApprovedMail");
		var emailFrom = 'MDEvidencePortal@its.jnj.com';
		$('[name="checkboxinline"]').each(
				function() {
					if ($(this).is(':checked')) {
						try {
							var data = $(this).closest('tr').data();
							var email = {};
							if (data.request) {
								var request = JSON.parse(data.request);
								email.to = request.emailid;
								email.from = 'MDEvidencePortal@its.jnj.com';
								email.subject = lookUpTable
										.get("evdApprovedSubject");
								;
								var emailContent = lookUpTable
										.get("evdApprovedMail");
								;
								emailContent = emailContent.replaceAll(
										"{{rejectRemarks}}",
										request.status_remarks);
								emailContent = emailContent.replaceAll(
										"{{userName}}", request.common_name);
								emailContent = emailContent.replaceAll(
										"{{businessRole}}",
										request.business_role);
								email.content = emailContent;

								emails.push(btoa(JSON.stringify(email)));
							}
							data = JSON.parse(data.primarykeys);
							data = data[0].newValue;
							wheres.push("username = '" + data + "' ");
							checkboxCount++;
						} catch (e) {

						}
					}
				});
		if (checkboxCount > 0) {
			query += wheres.join(' OR ');
			query = btoa(query);
		} else {
			return;
		}
		var updateRequest = {
			"source" : 'postgreSQL',
			"dataBase" : 'E2EMFPostGres',
			"queries" : query
		};
		callAjaxService("integraGenericUpdate", function(response) {
			if (response && response.isException) {
				showNotification('error', response.customMessage);
				return;
			}
			showNotification('success', 'Users Approved Successfully');
			$.post(directoryName + "/rest/email/sendGenericEmails", {
				"requestParam" : emails.join(",")
			}, function() {
				$("#applyFilter").trigger("click");
			});
		}, callBackFailure, updateRequest, "POST", "json", true);
	}

	this.evidenceAddPlatform = function() {
		$("#myModalDrilldown").addClass('plain-modal-theme');
		var str = '';
		str += '<form id="frmPlatform" class="" novalidate="novalidate"><div class="row"> <section>';
		str += '<label class="label">Platform & Specialty</label>';
		str += '<label class="input"><input type="text" class="platformSpeciality" name="platformSpeciality" value=""></label>';
		str += '</section></div>';
		str += '<div class="row" > <section>';
		str += '<label class="label">Synonyms</label>';
		str += '<label class="input"><input type="text" class="platformSyn" value=""></label>';
		str += '</section></div>';
		str += '<div class="row" style="display: none;"> <section>';
		str += '<label class="checkbox enable-checkbox"><input type="checkbox" class="platformStatus" value=""><i></i>Active</label>';
		str += '</section></div></form>';
		str += '<footer class="modal-footer">';
		str += '<button type="button" class="pull-right btn btn-warning btnPSAdd"  aria-hidden="true">Add</button>';
		str += '</footer>';

		$("#myModalLabel", "#myModalDrilldown").html("");
		$("#myModalLabel", '#myModalDrilldown').html(
				"<div> Add Platform & Specialty</div>");
		$(".modal-dialog", "#myModalDrilldown").addClass("width-50p");
		$(".modal-body", "#myModalDrilldown").addClass(
				"no-padding-top no-padding-bottom");
		$('.modal-body').css("max-height", "16em");
		$("#poDocView").addClass('smart-form').html(str);
		$('#myModalDrilldown').modal('show');
		$("#frmPlatform").validate({
			rules : {
				platformSpeciality : {
					required : true
				}
			},
			messages : {
				platformSpeciality : {
					required : 'Please enter Platform & specialty'
				}
			},
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
		});

		$('.btnPSAdd')
				.on(
						'click',
						function() {
							if (!$("#frmPlatform").valid())
								return;
							var x = 0;
							if ($('.platformStatus').is(":checked"))
								x = 1;
							else
								x = 0;
							var query = "insert into evidence_platform_specialty (platform_and_specialty, status, synonyms, created_on, created_by) values ('"
									+ $('.platformSpeciality').val()
									+ "', 'Active', '"
									+ $('.platformSyn').val()
									+ "', now(), '"
									+ $('#username').text() + "');";

							var updateRequest = {
								"source" : 'postgreSQL',
								"dataBase" : 'EVDSEARCH',
								"queries" : btoa(query)
							}
							callAjaxService(
									"integraGenericUpdate",
									function(response) {
										if (response && response.isException) {
											if (response.message
													.indexOf("ERROR: duplicate key") > -1) {
												showNotification('error',
														"This Platform & Specialty already Exists");
											} else {
												showNotification('error',
														response.customMessage);
											}
											return;
										}
										$('#myModalDrilldown').modal('hide');
										showNotification('success',
												'Platform & Specialty Added Successfully');
										$("#applyFilter").trigger("click");
									}, callBackFailure, updateRequest, "POST",
									"json", true);
						});
	}

	this.evidenceAddComptr = function() {
		var selectRequest = {
			"source" : 'postgreSQL',
			"dbName" : 'EVDSEARCH',
			"tableName" : "evidence_platform_specialty where status = 'Active' order by platform_and_specialty",
			"columnName" : 'platform_and_specialty'
		}
		callAjaxService(
				"getIntegraCategories",
				function(response) {
					if (response && response.isException) {
						showNotification('error', response.customMessage);
						return;
						s
					}
					var drpDwnStr = '';
					response.map(function(a) {
						drpDwnStr += '<option value="' + a + '">' + a
								+ '</option>';
					});
					$("#myModalDrilldown").addClass('plain-modal-theme');
					var str = '';

					var isChecked = 'checked';
					str += '<form id="frmAddComptr" class="" novalidate="novalidate"><div class="row"> <section>';
					str += '<label class="label">Platform & Specialty</label>';
					str += '<label class="select"><select class="platformSpecialty width-100per" placeholder="Select Platform & Specialty">'
							+ drpDwnStr + '</select></label>';
					str += '</section></div>';

					str += '<div class="row"> <section>';
					str += '<label class="label">Product </label>';
					str += '<label class="input"><input type="text" class="productName" name="productName" value=""></label>';
					str += '</section></div>';

					str += '<div class="row"> <section>';
					str += '<label class="label">Synonyms</label>';
					str += '<label class="input"><input type="text" class="competitorSyn" value=""></label>';
					str += '</section></div>';

					str += '<div class="row"><section>';
					str += '<label class="label">Keywords</label>';
					str += '<label class="input"><input type="text" class="competitorTerm" value=""></label>';
					str += '</section></div>';

					str += '<div class="row"><section>';
					str += '<label class="label">Competitor</label>';
					str += '<label class="input"><input type="text" class="competitorName"  name="competitorName" value=""></label>';
					str += '</section></div>';

					str += '<div class="row" style="display:none;"> <section>';
					str += '<label class="checkbox enable-checkbox"><input type="checkbox" '
							+ isChecked
							+ ' class="platformStatus" ><i></i>Status</label>';
					str += '</section></div></form>';

					str += '<footer class="modal-footer">';
					str += '<button type="button" class="pull-right btn btn-warning btnPCAdd" aria-hidden="true">Add</button>';
					str += '</footer>';

					$("#myModalLabel", "#myModalDrilldown").html("");
					$("#myModalLabel", '#myModalDrilldown').html(
							"<div> Add Competitor </div>");
					$(".modal-dialog", "#myModalDrilldown").addClass(
							"width-50p");
					$(".modal-body", "#myModalDrilldown").addClass(
							"no-padding-top no-padding-bottom");
					$('.modal-body').css("max-height", "50em");
					$("#poDocView").addClass('smart-form').html(str);
					$('.platformSpecialty').select2();
					$('#myModalDrilldown').modal('show');
					$("#frmAddComptr").validate({
						rules : {
							productName : {
								required : true
							},
							competitorName : {
								required : true
							}
						},
						messages : {
							productName : {
								required : 'Please enter Product'
							},
							competitorName : {
								required : 'Please enter Competitor'
							}
						},
						errorPlacement : function(error, element) {
							error.insertAfter(element.parent());
						}
					});
					$('.btnPCAdd')
							.on(
									'click',
									function() {
										if (!$("#frmAddComptr").valid())
											return;
										var active = $('.platformStatus').is(
												':checked') ? 'Active'
												: 'In Active';
										var query = "insert into evidence_product_family_competitor (status,product_name,created_on,created_by,synonyms,key_term,platform_and_specialty,competitor_name) values ('"
												+ active
												+ "', '"
												+ $('.productName').val()
												+ "', now(), '"
												+ $('#username').text()
												+ "' , '"
												+ $('.competitorSyn').val()
												+ "', '"
												+ $('.competitorTerm').val()
												+ "' , '"
												+ $('.platformSpecialty')
														.select2('val')
												+ "', '"
												+ $('.competitorName').val()
												+ "')";

										var updateRequest = {
											"source" : 'postgreSQL',
											"dataBase" : 'EVDSEARCH',
											"queries" : btoa(query)
										}
										callAjaxService(
												"integraGenericUpdate",
												function(response) {
													if (response
															&& response.isException) {
														if (response.message
																.indexOf("ERROR: duplicate key") > -1) {
															showNotification(
																	'error',
																	"This Competitor already Exists");
														} else {
															showNotification(
																	'error',
																	response.customMessage);
														}
														return;
													}
													$('#myModalDrilldown')
															.modal('hide');
													showNotification('success',
															'Competitor Brand Added Successfully');
													$("#applyFilter").trigger(
															"click");
												}, callBackFailure,
												updateRequest, "POST", "json",
												true);
									});
				}, callBackFailure, selectRequest, "POST", "json", true);
	}

	this.evidenceAddProductFamily = function() {
		var selectRequest = {
			"source" : 'postgreSQL',
			"dbName" : 'EVDSEARCH',
			"tableName" : "evidence_platform_specialty where status = 'Active' order by platform_and_specialty",
			"columnName" : 'platform_and_specialty'
		}
		callAjaxService(
				"getIntegraCategories",
				function(response) {
					if (response && response.isException) {
						showNotification('error', response.customMessage);
						return;
						s
					}
					var drpDwnStr = '<option value="">Select Platform & Specialty</option>';
					response.map(function(a) {
						if (a) {
							drpDwnStr += '<option value="' + a + '">' + a
									+ '</option>';
						}
					});
					var str = '';
					$("#myModalDrilldown").addClass('plain-modal-theme');
					var isChecked = 'checked';

					str += '<form id="frmProductFamily" class="" novalidate="novalidate"><div class="row"> <section>';
					str += '<label class="label">Platform & Specialty</label>';
					str += '<label class="select"><select class="width-100per platformSpecialty" placeholder="Select Platform & Specialty">'
							+ drpDwnStr + '</select></label>';
					str += '</section></div>';

					str += '<div class="row"> <section>';
					str += '<label class="label">Product Family</label>';
					str += '<label class="input"><input type="text" class="productFamily" name="productFamily" value=""></label>';
					str += '</section></div>';

					str += '<div class="row"> <section>';
					str += '<label class="label">Synonyms</label>';
					str += '<label class="input"><input type="text" class="productFamilySyn" value="" ></label>';
					str += '</section></div>';

					str += '<div class="row" style="display: none;"> <section>';
					str += '<label class="checkbox enable-checkbox"><input type="checkbox" class="productStatus" '
							+ isChecked + ' value=""><i></i>Active</label>';
					str += '</section></div></form>';

					str += '<footer class="modal-footer">';
					str += '<button type="button" class="pull-right btn btn-warning btnPFAdd" aria-hidden="true">Add</button>';
					str += '</footer>';

					$("#myModalLabel", "#myModalDrilldown").html("");
					$("#myModalLabel", '#myModalDrilldown').html(
							"<div> Add Product Family </div>");
					$(".modal-dialog", "#myModalDrilldown").addClass(
							"width-50p");
					$(".modal-body", "#myModalDrilldown").addClass(
							"no-padding-top no-padding-bottom");
					$('.modal-body').css("max-height", "50em");
					$("#poDocView").addClass('smart-form').html(str);
					$('.platformSpecialty').select2();
					$('#myModalDrilldown').modal('show');

					$("#frmProductFamily").validate({
						rules : {
							productFamily : {
								required : true
							}
						},
						messages : {
							productFamily : {
								required : 'Please enter Product Family'
							}
						},
						errorPlacement : function(error, element) {
							error.insertAfter(element.parent());
						}
					});

					$('.btnPFAdd')
							.on(
									'click',
									function() {
										if (!$("#frmProductFamily").valid())
											return;
										var active = $('.productStatus').is(
												':checked') ? 'Active'
												: 'In Active';
										var query = "insert into evidence_product_family_syn (pr_status,platform_and_specialty,product_family,synonyms,created_on, created_by) values ('"
												+ active
												+ "','"
												+ $('.platformSpecialty')
														.select2('val')
												+ "','"
												+ $('.productFamily').val()
												+ "','"
												+ $('.productFamilySyn').val()
												+ "', now(), '"
												+ $('#username').text() + "')"
										// +"' , specialty_name =
										// '"+$('.platformSpecialty').val()+"' ,
										// pr_status =
										// '"+$('.platformStatus').val()

										var updateRequest = {
											"source" : 'postgreSQL',
											"dataBase" : 'EVDSEARCH',
											"queries" : btoa(query)
										}
										callAjaxService(
												"integraGenericUpdate",
												function(response) {
													if (response
															&& response.isException) {
														if (response.message
																.indexOf("ERROR: duplicate key") > -1) {
															showNotification(
																	'error',
																	"This family already Exists");
														} else {
															showNotification(
																	'error',
																	response.customMessage);
														}
														return;
													}
													$('#myModalDrilldown')
															.modal('hide');
													showNotification('success',
															'Product Family Added Successfully');
													$("#applyFilter").trigger(
															"click");
												}, callBackFailure,
												updateRequest, "POST", "json",
												true);
									});
				}, callBackFailure, selectRequest, "POST", "json", true);
	}

	this.evidenceAddProductBrand = function() {

		var selectRequest = {
			"source" : 'postgreSQL',
			"dbName" : 'EVDSEARCH',
			"tableName" : "evidence_platform_specialty where status = 'Active' order by platform_and_specialty",
			"columnName" : 'platform_and_specialty'
		}

		callAjaxService(
				"getIntegraCategories",
				function(response) {
					var psDrpdwnStr = '<option value="">Select Platform & Specialty</option>';
					var pfDrpdwnStr = '<option value="">Select Product Family</option>';
					var pfSDrpdwnStr = '';
					if (response && response.isException) {
						showNotification('error', response.customMessage);
						return;
					}
					response.map(function(a) {
						if (a) {
							psDrpdwnStr += '<option value="' + a + '">' + a
									+ '</option>';
						}
					});
					// Get Brands
					selectRequest = {
						"source" : 'postgreSQL',
						"dbName" : 'EVDSEARCH',
						"tableName" : 'evidence_product_family_syn',
						"columnName" : 'product_family'
					}
					callAjaxService(
							"getIntegraCategories",
							function(response) {
								if (response && response.isException) {
									showNotification('error',
											response.customMessage);
									return;
								}

								response.map(function(a) {
									if (a) {
										pfDrpdwnStr += '<option value="' + a
												+ '">' + a + '</option>';
									}
								});
								// Get Brands
								selectRequest = {
									"source" : 'postgreSQL',
									"dbName" : 'EVDSEARCH',
									"tableName" : 'evidence_product_family_syn',
									"columnName" : 'synonyms'
								}
								callAjaxService(
										"getIntegraCategories",
										function(response) {
											if (response
													&& response.isException) {
												showNotification('error',
														response.customMessage);
												return;
											}
											response
													.map(function(a) {
														pfSDrpdwnStr += '<option value="'
																+ a
																+ '">'
																+ a
																+ '</option>';
													});
											$("#myModalDrilldown").addClass(
													'plain-modal-theme');
											var str = '';
											var isChecked = 'checked';
											str += '<form id="frmProductBrand" class="" novalidate="novalidate"><div class="row"> <section>';
											str += '<label class="label">Platform & Specialty</label>';
											str += '<label class="select"><select class="width-100per platformSpecialty" placeholder="Select Platform & Specialty">'
													+ psDrpdwnStr
													+ '</select></label>';
											str += '</section></div>';

											str += '<div class="row"> <section>';
											str += '<label class="label">Product Family</label>';
											str += '<label class="select"><select class="width-100per productFamily" placeholder="Select Product Family">'
													+ pfDrpdwnStr
													+ '</select></label>';
											str += '</section></div>';

											/*
											 * str += '<div class="row">
											 * <section>'; str += '<label
											 * class="label">Product Family
											 * Synonyms</label>'; str += '<label
											 * class="input"><input type="text"
											 * class="productFamilySyn" value="" /></label>';
											 * str += '</section></div>';
											 */

											str += '<div class="row"> <section>';
											str += '<label class="label">Brand</label>';
											str += '<label class="input"><input type="text" class="productBrand"  name="productBrand" value=""></label>';
											str += '</section></div>';

											str += '<div class="row"> <section>';
											str += '<label class="label"> Synonyms</label>';
											str += '<label class="input"><input type="text" class="brandSyn" value=""></label>';
											str += '</section></div>';

											str += '<div class="row"> <section>';
											str += '<label class="label">Keywords</label>';
											str += '<label class="input"><input type="text" class="brandKeyword" /></label>';
											str += '</section></div></form>';

											str += '<div class="row" style="display: none;"> <section>';
											str += '<label class="checkbox enable-checkbox"><input type="checkbox" '
													+ isChecked
													+ ' class="platformStatus" value=""><i></i>Status</label>';
											str += '</section></div>';

											str += '<footer class="modal-footer">';
											str += '<button type="button" class="pull-right btn btn-warning btnPBAdd" aria-hidden="true">Add</button>';
											str += '</footer>';

											$("#myModalLabel",
													"#myModalDrilldown").html(
													"");
											$("#myModalLabel",
													'#myModalDrilldown')
													.html(
															"<div> Add JNJ Brand </div>");
											$(".modal-dialog",
													"#myModalDrilldown")
													.addClass("width-50p");
											$(".modal-body",
													"#myModalDrilldown")
													.addClass(
															"no-padding-top no-padding-bottom");
											$('.modal-body').css("max-height",
													"50em");
											$("#poDocView").addClass(
													'smart-form').html(str);

											$('.platformSpecialty').val('');
											$('.productFamily').val('');

											$('.platformSpecialty').select2();
											$('.productFamily').select2();
											// $('.productFamilySyn').select2();

											$('#myModalDrilldown')
													.modal('show');

											$("#frmProductBrand")
													.validate(
															{
																rules : {
																	productBrand : {
																		required : true
																	},
																	platformSpecialty : {
																		required : true
																	},
																	productFamily : {
																		required : true
																	}
																},
																messages : {
																	productBrand : {
																		required : 'Please enter Brand Name'
																	},
																	platformSpecialty : {
																		required : 'Please enter Platform & Specialty'
																	},
																	productFamily : {
																		required : 'Please enter Product Family'
																	}
																},
																errorPlacement : function(
																		error,
																		element) {
																	error
																			.insertAfter(element
																					.parent());
																}
															});

											$('.btnPBAdd')
													.on(
															'click',
															function() {
																if (!$(
																		"#frmProductBrand")
																		.valid())
																	return;

																if (!$(
																		'.platformSpecialty')
																		.select2(
																				'val')) {
																	showNotification(
																			'error',
																			"Please enter Platform & Specialty");
																	return;
																}
																if (!$(
																		'.productFamily')
																		.select2(
																				'val')) {
																	showNotification(
																			'error',
																			"Please enter Product Family");
																	return;
																}
																var active = $(
																		'.platformStatus')
																		.is(
																				':checked') ? 'Active'
																		: 'In Active';
																var query = "insert into evidence_product_synomyms (keyword,status,family_synonyms,platform_specialty,product_family,product_brand_family,brand_synonyms,created_on, created_by) values ('"
																		+ $(
																				'.brandKeyword')
																				.val()
																		+ "','"
																		+ active
																		+ "','"
																		+ $(
																				'.productFamilySyn')
																				.val()
																		+ "','"
																		+ $(
																				'.platformSpecialty')
																				.select2(
																						'val')
																		+ "','"
																		+ $(
																				'.productFamily')
																				.select2(
																						'val')
																		+ "','"
																		+ $(
																				'.productBrand')
																				.val()
																		+ "','"
																		+ $(
																				'.brandSyn')
																				.val()
																		+ "', now(), '"
																		+ $(
																				'#username')
																				.text()
																		+ "' )";
																var updateRequest = {
																	"source" : 'postgreSQL',
																	"dataBase" : 'EVDSEARCH',
																	"queries" : btoa(query)
																}
																callAjaxService(
																		"integraGenericUpdate",
																		function(
																				response) {
																			if (response
																					&& response.isException) {
																				if (response.message
																						.indexOf("ERROR: duplicate key") > -1) {
																					showNotification(
																							'error',
																							"This Brand already Exists");
																				} else {
																					showNotification(
																							'error',
																							response.customMessage);
																				}
																				return;
																			}
																			$(
																					'#myModalDrilldown')
																					.modal(
																							'hide');
																			showNotification(
																					'success',
																					'JNJ Brand Added Successfully');
																			$(
																					"#applyFilter")
																					.trigger(
																							"click");
																		},
																		callBackFailure,
																		updateRequest,
																		"POST",
																		"json",
																		true);
															});

										}, callBackFailure, selectRequest,
										"POST", "json", true);

							}, callBackFailure, selectRequest, "POST", "json",
							true);
				}, callBackFailure, selectRequest, "POST", "json", true);

	}

	this.evidenceAddDocumentType = function() {
		$("#myModalDrilldown").addClass('plain-modal-theme');
		var str = '';
		str += '<form id="frmDocumentType" class="" novalidate="novalidate"><div class="row"> <section>';
		str += '<label class="label">Type</label>';
		str += '<label class="input"><input type="text" class="docTypeName" name="docTypeName" value=""></label>';
		str += '</section></div>';
		str += '<div class="row"> <section>';
		str += 'R&D <label class="checkbox"><input type="checkbox" class="isRdChecked" ><i></i></label>';
		str += '</section></div>';
		str += '<div class="row"> <section>';
		str += 'Commercial Non Field <label class="checkbox"><input type="checkbox" class="isComChecked" ><i></i></label>';
		str += '</section></div>';
		str += '<div class="row" style="display: none;"> <section>';
		str += '<label class="checkbox enable-checkbox"><input type="checkbox" class="isActive" value=""><i></i>Active</label>';
		str += '</section></div></form>';

		str += '<footer class="modal-footer">';
		str += '<button type="button" class="pull-right btn btn-warning btnPSAdd" aria-hidden="true">Add</button>';
		str += '</footer>';

		$("#myModalLabel", "#myModalDrilldown").html("");
		$("#myModalLabel", '#myModalDrilldown').html(
				"<div> Add Document Type</div>");
		$(".modal-dialog", "#myModalDrilldown").addClass("width-50p");
		$(".modal-body", "#myModalDrilldown").addClass(
				"no-padding-top no-padding-bottom");
		// $('.modal-body').css("min-height", "200px");
		$("#poDocView").css("min-height", "200px").addClass('smart-form').html(
				str);
		$('#myModalDrilldown').modal('show');
		$("#frmDocumentType").validate({
			rules : {
				docTypeName : {
					required : true
				}
			},
			messages : {
				docTypeName : {
					required : 'Please enter Type'
				}
			},
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
		});

		$('.btnPSAdd')
				.on(
						'click',
						function() {
							if (!$("#frmDocumentType").valid())
								return;
							var x = 0;
							if ($('.isActive').is(":checked"))
								x = 1;
							else
								x = 0;
							var isComChecked = $('.isComChecked')
									.is(':checked') ? 'Active' : 'Inactive';
							var isRdChecked = $('.isRdChecked').is(':checked') ? 'Active'
									: 'Inactive';

							if (!$('.docTypeName').val()) {
								showNotification('error',
										'Please enter Document Type');
							}

							var query = "insert into document_type (doc_type_name, is_active, created_by, created_at,commercial,rd) values ('"
									+ $('.docTypeName').val()
									+ "', 'Active', '"
									+ $('#username').text()
									+ "', now(), '"
									+ isComChecked
									+ "', '"
									+ isRdChecked + "');";

							var updateRequest = {
								"source" : 'postgreSQL',
								"dataBase" : 'EVDSEARCH',
								"queries" : btoa(query)
							}
							callAjaxService(
									"integraGenericUpdate",
									function(response) {
										if (response && response.isException) {
											if (response.message
													.indexOf("ERROR: duplicate key") > -1) {
												showNotification('error',
														"This type already Exists");
											} else {
												showNotification('error',
														response.customMessage);
											}
											return;
										}
										$('#myModalDrilldown').modal('hide');
										showNotification('success',
												'Document Type Added Successfully');
										$("#applyFilter").trigger("click");
									}, callBackFailure, updateRequest, "POST",
									"json", true);
						});
	}
	
	
	this.createJobs = function(ele){
	var buttonName = "Create"
	if(ele){
		var primaryKey = Jsonparse($(ele).parents("tr").data("primarykeys"))[0].newValue;
		var columnData = Jsonparse($(ele).parents("tr").data("request"))
		buttonName = "Save"
		}
	else
		var primaryKey = null

	$("#myModalDrilldown #poDocView").html("");
	var headerTest = ele?"Edit Job":"Create Job"
	$(".modal-title","#myModalDrilldown").html(headerTest);
	var str=''

	str+='<div class="row smart-form">'
	str+='<section class="col col-xs-12 padding-bottom-10" fieldtype="text"><label style="font-size:100% !imprtant,color:black">Job Name</label><span style="padding: 0px;"><input id="name" class="form-control padding-left-10" type="text" field="id" ></span></section>'
	//str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label style="font-size:100% !imprtant,color:black">Connection</label><span style="padding: 0px;"><div name="connection" ></div></span></section>'
	str+='</div>'

	str+='<div class="row">'
	str+='<section id="timesection" class="col col-xs-6 padding-bottom-10" fieldtype="text"><label style="font-size:100% !imprtant;color:black; margin-left: 10px;">Time</label><span style="padding: 0px;"><input id="time1" name="time1" class="form-control padding-left-10" field="id" ></span></section>'

	//str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label style="font-size:100% !imprtant,color:black">Enabled</label><span style="padding: 0px;"><input id="enabled" class="form-control padding-left-10" type="checkbox" field="id" ></span></section>'
/*	str+='<section id="timesection" class="col col-xs-1 padding-bottom-10" fieldtype="text" style="margin-top: 23px">'
	str+='<input type="checkbox" id="enabled" name="enabled" value="enabled" style="width: 20px;height: 20px;"></section><section id="timesection" class="col col-xs-5 padding-bottom-10" fieldtype="text" style="margin-top: 27px;margin-left: -48px;"><label for="enabled"> Active</label></section>'
	str+='</div>'*/
	
	str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label style="font-size:100% !imprtant,color:black">End Date</label><span style="padding: 0px;"><input id="date" class="form-control padding-left-10" placeholder="DD-MM-YYYY" type="date" field="id" ></span></section>'
	str+='</div>'


	str+='<div class="row smart-form">'
	str+='<section id="dyas" class="col col-xs-6 padding-bottom-10" fieldtype="text"><label style="font-size:100% !imprtant,color:black">Days</label><div style="padding: 0px;left: 10px;" class="row smart-form">';
	str+='<span class="col-xs-1"><label class="checkbox"> <input type="checkbox" id="sun" name="sun" value="1"> <i></i>S</label></span>'
	str+='<span class="col-xs-1"><label class="checkbox"> <input type="checkbox" id="mon" name="mon" value="2"> <i></i>M</label></span>'
	str+='<span class="col-xs-1"><label class="checkbox"> <input type="checkbox" id="tue" name="tue" value="3"> <i></i>T</label></span>'
	str+='<span class="col-xs-1"><label class="checkbox"> <input type="checkbox" id="wed" name="wed" value="4"> <i></i>W</label></span>'	
	str+='<span class="col-xs-1"><label class="checkbox"> <input type="checkbox" id="thur" name="thur" value="5"> <i></i>TH</label></span>'
	str+='<span class="col-xs-1"><label class="checkbox"> <input type="checkbox" id="fri" name="fri" value="6"> <i></i>F</label></span>'
	str+='<span class="col-xs-1"><label class="checkbox"> <input type="checkbox" id="sat" name="sat" value="7"> <i></i>S</label></span>'
	
		
	/*str+='<input type="checkbox" class="customCheckBox large" id="sun" name="sun" value="1" style="width: 20px;height: 16px;"><label for="enabled" style="margin-right: 5px;font-size: 17px;"> S</label>'
	str+='<input type="checkbox" id="mon" name="mon" value="2" style="width: 20px;height: 16px;"><label for="enabled" style="margin-right: 5px;font-size: 17px;"> M</label>'
	str+='<input type="checkbox" id="tue" name="tue" value="3" style="width: 20px;height: 16px;"><label for="enabled" style="margin-right: 5px;font-size: 17px;"> T</label>'
	str+='<input type="checkbox" id="wed" name="wed" value="4" style="width: 20px;height: 16px;"><label for="enabled" style="margin-right: 5px;font-size: 17px;"> W</label>'
	str+='<input type="checkbox" id="thur" name="thur" value="5" style="width: 20px;height: 16px;"><label for="enabled" style="margin-right: 5px;font-size: 17px;"> TH</label>'
	str+='<input type="checkbox" id="tue" name="fri" value="6" style="width: 20px;height: 16px;"><label for="enabled" style="margin-right: 5px;font-size: 17px;"> F</label>'
	str+='<input type="checkbox" id="tue" name="sat" value="7" style="width: 20px;height: 16px;"><label for="enabled" style="margin-right: 5px;font-size: 17px;"> S</label>'*/

	// str+=' <label class="input" style="width:100%">';
	// str+='<div class="form-group">';
	// str+=' <div>';
	// str+=' <div id="daysDiv" class="btn-group" data-toggle="buttons">';
	// str+=' <label id="rad" class="btn btn-default" title="Sunday" data-value="1"> <input id="rad" type="radio" name="day" data-value="1"> S';
	// str+=' </label> <label id="rad" class="btn btn-default active" title="Monday" data-value="2"> <input id="rad" type="radio" name="day" data-value="2"> M';
	// str+=' </label> <label id="rad" class="btn btn-default" title="Tuesday" data-value="3"> <input id="rad" type="radio" name="day" data-value="3"> T';
	// str+='</label> <label id="rad" class="btn btn-default" title="Webnesday" data-value="4"> <input id="rad" type="radio" name="day" data-value="4"> W';
	// str+='</label> <label id="rad" class="btn btn-default" title="Thursday" data-value="5"> <input id="rad" type="radio" name="day" data-value="5"> T';
	// str+='</label> <label id="rad" class="btn btn-default" title="Friday" data-value="6"> <input id="rad" type="radio" name="day" data-value="6"> F';
	// str+='</label> <label id="rad" class="btn btn-default" title="Saturday" data-value="7"> <input id="rad" type="radio" name="day"> S';
	// str+='</label>';
	// str+='</div>';
	// str+='</div>';
	// str+='</div>';
	// str+='</label>';
	str+='</div></section>';
	/*str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label style="font-size:100% !imprtant,color:black">End Date</label><span style="padding: 0px;"><input id="date" class="form-control padding-left-10" type="date" field="id" ></span></section>'
	str+='</div>'*/

	str+='<div class="row">'
	str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label style="font-size:100% !imprtant,color:black">Job Types</label><span style="padding: 0px;">';
	str+='<select id="jobtypes" class="form-control" id="example-select">';
	str+='<option>Spectra Job</option>';
	str+='<option>Oozie Job</option>';
	str+='</select></span></section>';
	str+='</div>'
	str+='</div>'
	
	

	str+='<div class="row smart-form">'
	str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label style="font-size:100% !imprtant,color:black">Connection</label><span style="padding: 0px;"><div name="connection" id="connection1" ></div></span></section>'
	str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label style="font-size:100% !imprtant,color:black">Target Object</label><span style="padding: 0px;"><div name="targetObject" id="source1" ></div></span></section>'
	str+='</div>'
	str+='<div class="row smart-form">'
	str+='<section id="timesection" class="col col-xs-1 padding-bottom-10" fieldtype="text" style="margin-top: 23px;left: -2px;">'
	str+='<label class="checkbox"> <input type="checkbox" id="enabled" name="enabled" value="enabled"> <i></i>Active</label></section>'
	str+='</div>'
	str+='<footer style="margin-bottom: -14px;width: 101%;background: aliceblue;margin-left: -20px;"><button type="button" data-dismiss="modal" area-hidden="true" class="btn btn-warning floatRight">Cancel</button><button type="submit" id="createJobs" class="btn btn-primary " style="width: 65px;margin-left: 574px;">'+buttonName+'</button></footer>'
	$("#poDocView").html(str);
	$('#myModalDrilldown').modal('show');

	var onChangeFun = function(){

	var selectedVal = $("[name='connection']").find("select").val()
	var filterQuery = " 1=1 ";
	if(selectedVal)
	filterQuery = " connection = '"+selectedVal+"'";
	constructDropDown("[name='targetObject']","SELECT distinct name,'' FROM di_cfg_target_obj where name like '%<Q>%' and "+filterQuery+"",true, '', "", "Select Target","postgreSQL","E2EMFPostGres",object);

	}

	$("select2-choices").css("border-radius","0px !important")


	//
	// $("#staging").select2({
	// allowClear: true,
	// placeholder : "Choose mapping",
	// tags : urdata,
	// maximumSelectionSize: 1
	// });

	$('input[name="time1"]').select2({
		allowClear: true,
		placeholder : "Choose Time",
		tags:true,
		tags : ["5pm"],
		maximumSelectionSize: 10000
	}).on('change', function(e){
		var regexp = /^(0?[1-9]|1[012])(:[0-5]\d)[APap][mM]$/;
	 	var res = regexp.test(e.added.text);
	 	var regexp1 = /^(0?[1-9]|1[012])[APap][mM]$/;
	 	var res1 = regexp1.test(e.added.text);
	 	if(res===false && res1===false){
	 		e.preventDefault();
	 		$(this).select2('val', '')
	 		showNotification("error", "Time format should be hh:mm(AM|PM) or hh(AM|PM)");
	 	}

	});

	$(".select2-drop").css({"left":"199.5px !important"})
	

	if(primaryKey){
		var object={};
		object.buttonWidth = '461px';
		object.disAbledRound = true

		$('#name').val(columnData.name)

		var datatarget = []
		Jsonparse(columnData.target_obj_list).forEach(function(target){
			datatarget.push(target.name)

		})
		constructDropDown("[name='connection']","SELECT distinct name,'' FROM di_cfg_connection where name like '%<Q>%'",false, columnData.connection, onChangeFun, "","postgreSQL","E2EMFPostGres",{"buttonWidth":"462px","disAbledRound":true});
		constructDropDown("[name='targetObject']","SELECT distinct name,'' FROM di_cfg_target_obj where name like '%<Q>%'",true, datatarget, "", "Select Target","postgreSQL","E2EMFPostGres",object);
		$("#date").val(columnData.end_date)



		var timelist = []
		Jsonparse(columnData.time).forEach(function(time){
			timelist.push(time.name)

		})

		$('input[name="time1"]').select2({
			allowClear: true,
			placeholder : "Choose Time",
			tags:true,
			tags : ["5pm"],
			maximumSelectionSize: 10000
		}).select2('val', timelist);
	
		$(".select2-drop").css({"left":"199.5px !important"})

		var enabled = columnData.enable;
		if(enabled == "1"){
			$("#enabled").prop('checked',true)
		}else{
			$("#enabled").prop('checked',false)
		}

		var daysList = []
		Jsonparse(columnData.days).forEach(function(day){
			daysList.push(day.name)

		})

		var allinput = $("#dyas").find('input')
		for(var s=0;s<daysList.length;s++){
			for(var q=0;q<allinput.length;q++){
				if($(allinput[q]).val() ==daysList[s] ){
					$(allinput[q]).prop('checked',true)
				}
			}
		}

		$("#jobtypes").val(columnData.type)
	}
		var object={};
		object.buttonWidth = '468px';
		object.disAbledRound =true

		if(!primaryKey){
			constructDropDown("[name='connection']","SELECT distinct name,'' FROM di_cfg_connection where name like '%<Q>%'",false, '', onChangeFun, "Select Connection","postgreSQL","E2EMFPostGres",{"buttonWidth":"462px","disAbledRound":true});
			constructDropDown("[name='targetObject']","SELECT distinct name,'' FROM di_cfg_target_obj where name like '%<Q>%'",true, '', "", "Select Target","postgreSQL","E2EMFPostGres",object);
		}

	$("#createJobs").unbind().bind('click',function(){
		var columnList = [ 'name', 'time', 'endDate', 'days', 'jobtype',"conncetion","targetObject",'enabled','runningStatus']
		var updateValue = []

		for (var x = 0; x < columnList.length; x++) {
			var data = {}
			var newValue
			var fieldType

			if(columnList[x] == 'runningStatus'){
				data.fieldName = "current_run_status"
				data.displayName = "current_run_status"
				data.newValue = "Not Started"
				data.oldValue = ""
				data.type = "p"
			}

	if(columnList[x] == 'name'){
		data.fieldName = "name"
		data.displayName = "name"
		data.newValue = $("#name").val()
		data.oldValue = ""
		data.type = "p"
		}

	if(columnList[x] == 'time'){
		data.fieldName = "time"
		data.displayName = "time"
		var times = $("#time1").val().split(',')
		var timeList = []
		for(var t=0;t<times.length;t++){
		timeList.push({"name":times[t]})
	}
		data.newValue = JSON.stringify(timeList)
		data.oldValue = ""
		data.type = "p"
	}
	if(columnList[x] == 'endDate'){
		data.fieldName = "end_date"
		data.displayName = "end_date"
		data.newValue = $("#date").val()
		data.oldValue = ""
		data.type = "p"
	}

	if(columnList[x] == 'days'){
		data.fieldName = "days"
		data.displayName = "days"
		var checkedlist = []
		var allinput = $("#dyas").find('input')
		for(var q=0;q<allinput.length;q++){
			if($(allinput[q]).is(':checked')){
				checkedlist.push({"name":$(allinput[q]).val()})
			}
		}
		data.newValue = JSON.stringify(checkedlist)
		data.oldValue = ""
		data.type = "p"
	}

	if(columnList[x] == 'jobtype'){
		data.fieldName = "type"
		data.displayName = "type"
		data.newValue = $("#jobtypes option:selected").val()
		data.oldValue = ""
		data.type = "p"
	}

	if(columnList[x] == 'conncetion'){
		data.fieldName = "connection"
		data.displayName = "connection"
		data.newValue = $("[name='connection']").find("select").val()
		data.oldValue = ""
		data.type = "p"
	}
	if(columnList[x] == 'targetObject'){
		data.fieldName = "target_obj_list"
		data.displayName = "target_obj_list"
		var datatarget = []
		for(var z =0;z< $("[name='targetObject']").find("select").val().length;z++){
			datatarget.push({"name":$("[name='targetObject']").find("select").val()[z]})
		}
		data.newValue = JSON.stringify(datatarget)
		data.oldValue = ""
		data.type = "p"
	}
	if(columnList[x] == 'enabled'){
		data.fieldName = "enable"
		data.displayName = "enable"
		data.newValue = $("#enabled").is(':checked')?1:0
		data.oldValue = ""
		data.type = "p"
	}

	updateValue.push(data)
	}

	var request ={
		source:'postgreSQL',
		database:'E2EMFPostGres',
		table:'di_cfg_job',
		primaryKey:primaryKey?[{"fieldName":'id','displayName':'id','newValue':primaryKey,'oldValue':''}]:null,
		values:updateValue,
		create:false
	};
	var request = {
			"request":JSON.stringify(request)
	};
	callAjaxService("updateToDB",function(response){
		$('#myModalDrilldown').modal('hide');
		showNotification("success", "Data Updated Successfully");
		function callbackSucessClearCache() {
			dataAnalyser.profiling.grapher.refreshTable();
		}
		callAjaxService("clearCache", callbackSucessClearCache,
		callBackFailure, null, "POST");
	},callBackFailure,request,"POST");

	})

	}
		
	this.createObjectPopUp = function(ele){
	var buttonName = "Create"
	if(ele){
		var primaryKey = Jsonparse($(ele).parents("tr").data("primarykeys"))[0].newValue;
		var columnData = Jsonparse($(ele).parents("tr").data("request"))
		buttonName = "Save"
	}
	else
		var primaryKey = null

	var callBackSuccessGetColumnData = function(response){
		
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else{
			var str='';
			localStorage["availableColumns"] = JSON.stringify(response)
			}
			}
			
			var request ={
					 source:'postgreSQL',
					 database:'E2EMFPostGres',
					 //table:'widget_config',
					 query:'SELECT distinct col_name  FROM di_cfg_source_obj_columns',
					 columns:'col_name',
					 filters:'1 and 1',
					 groupByEnabled:true,
					 cacheDisabled:true
			 	};
			 
			 callAjaxService("getData", callBackSuccessGetColumnData, callBackFailure,
					 request, "POST",null,true);
	
	$("#myModalDrilldown #poDocView").html("");

	if(primaryKey!=null)
	{
		$(".modal-title","#myModalDrilldown").html("Edit Target Object");
	}
	else{
		$(".modal-title","#myModalDrilldown").html("Create Target Object");
	}
	var str=''
		
	str+='<div class="row">'
	str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label  style="font-size:100% !imprtant,color:black">Target name</label><span style="padding: 0px;"><input id="targetName" class="form-control padding-left-10" type="text" field="id" ></span></section>'
	str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label  style="font-size:100% !imprtant,color:black">Target Folder</label><span style="padding: 0px;"><input id="targetfolderPath" class="form-control padding-left-10" type="text" field="id" ></span></section>'
	str+='</div>'
	
	str+='<div class="row">'
	str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label  style="font-size:100% !imprtant,color:black">Connection</label><span style="padding: 0px;"><div name="connection" id="connection1"></div></span></section>'
	str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label  style="font-size:100% !imprtant,color:black">Source Object</label><span style="padding: 0px;"><div name="source" id="source1"></div></span></section>'
	str+='</div>'
		
	str+='<div class="row">'
	str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label  style="font-size:100% !imprtant,color:black">Create Date Fields</label><span style="padding: 0px;"><div id="createDateFiled" class="form-control padding-left-10" type="text" field="id" style="background: #e6e3e3; height:auto;min-height: 31px;" ></div></span></section>'
	str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label  style="font-size:100% !imprtant,color:black">Last Update Fields</label><span style="padding: 0px;"><div id="lastUpdataFiled" class="form-control padding-left-10" type="text" field="id" style="background: #e6e3e3; height:auto;min-height: 31px;" ></div></span></section>'
	str+='</div>'
	
	str+='<div class="row">'
	str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label  style="font-size:100% !imprtant,color:black">Default Fileds</label><span style="padding: 0px;"><div id="defaultFileds" class="form-control padding-left-10" type="text" field="id" style="background: #e6e3e3;" ></div></span></section>'
	str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label  style="font-size:100% !imprtant,color:black">Primary Key</label><span style="padding: 0px;"><div id="primaryKey" class="form-control padding-left-10" type="text" field="id" style="background: #e6e3e3; height:auto;min-height: 31px;" ></div></span></section>'
	str+='</div>'
		
	str+='<div class="row smart-form" style="margin-left: -13px;">'
	str+='<section class="col col-xs-6 padding-bottom-10" fieldtype="text"><label class="checkbox"> <input type="checkbox" id="index" name="index"> <i></i>Index</section>'
	str+='</div>'
		
	str+='<div class="row">'
	// left column
	str+='<div class="col-md-5 col-xs-5 col-sm-5 col-lg-5">'
	str+='<h6>Available Columns</h6>'
	str+='<ul id="sortableAvaiable" class="columnConfig connectedSortable ui-sortable">'
	str+='<input class="form-control ui-sortable-handle" placeholder="Search Available Columns" name="txtsearchColumns">'
	if(primaryKey && ele){
		var coloumnList = Jsonparse(localStorage["availableColumns"]).data
		var selectedcolumnList = Jsonparse(columnData.data_columns)
		var coloumnList1 = []
		for(var e =0;e<selectedcolumnList.length;e++){
			coloumnList1.push(selectedcolumnList[e].name)
		}
		coloumnList = coloumnList.filter(function (item) {
		    return coloumnList1.indexOf(item.col_name) === -1;
		});
	}
		
	else
		var coloumnList = Jsonparse(localStorage["availableColumns"]).data
	if(coloumnList){
	for(var x =0; x<coloumnList.length;x++){
		if(coloumnList[x].col_name){
		str+='<li class="dd-item" ui-sortable-handle" data-field="id" data-displayname="ID" data-primarykey="true" data-datatype="int">'
		str+='<div class="dd-handle" style="overflow-wrap: break-word;">'+coloumnList[x].col_name+'</div>'
		str+='</li>'
			}
		}
	}
	str+='</ul>'
	str+='</div>'
	// middle column
	str+='<div class="col-md-2 col-xs-2 col-sm-2 col-lg-2 column-config-actions">'
	str+='<a id="addColumn" class="btn btn-success" style="width: 114px">Add<i class="fa fa-lg fa-fw fa-angle-right"></i></a>'
	str+='<a id="removeColumn" class="btn btn-danger" style="width: 114px"><i class="fa fa-lg fa-fw fa-angle-left"></i>Remove</a>'
	str+='<a id="addAllColumn" class="btn btn-success" style="width: 116px">Add All<i class="fa fa-lg fa-fw fa-angle-double-right"></i></a>'
	str+='<a id="removeAll" class="btn btn-danger"><i class="fa fa-lg fa-fw fa-angle-double-left"></i>Remove All</a>'
	str+='</div>'
	
	// right column
	str+='<div class="col-md-5 col-xs-5 col-sm-5 col-lg-5">'
	str+='<h6>Selected Columns</h6>'
	str+='<ul id="sortableSelected" class="columnConfig connectedSortable ui-sortable">'
	str+='<input class="form-control ui-sortable-handle" placeholder="Search Selected Columns" name="txtsearchColumns">'
		if(primaryKey && ele){
			for(var x =0; x<coloumnList1.length;x++){
				str+='<li class="dd-item" ui-sortable-handle" data-field="id" data-displayname="ID" data-primarykey="true" data-datatype="int">'
				str+='<div class="dd-handle" style="overflow-wrap: break-word;">'+coloumnList1[x]+'</div>'
				str+='</li>'
				}
		}
	str+='</ul>'		
	str+='</div>'
	
		
	str+='</div>'
	str+='<footer style="margin-bottom: -14px;width: 101%;background: aliceblue;margin-left: -20px;"><button type="button" data-dismiss="modal" area-hidden="true" class="btn btn-warning  floatRight">Cancel</button><button type="submit" id="createTargetObject" class="btn btn-primary " style="width: 65px;margin-left: 574px;">'+buttonName+'</button></footer>'
	$("#poDocView").html(str);
	$('#myModalDrilldown').modal('show');
	
	// get data for column

	
	$("#targetName").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90) && (inputValue != 95)&&(!(inputValue >= 97 && inputValue <= 122))&&(!(inputValue >= 48 && inputValue <= 57))) {
            event.preventDefault();
            showNotification("error", "Invalid Target Name");
        }
       
    });
	var defaultFieldsList = ["rundate","comb_key","batch","esslad"]
	$("#defaultFileds").html("");
	var str1 = '';
	for(var z =0; z<defaultFieldsList.length;z++){
		str1+='<span style="color:white;font-size: 12px;margin-bottom: 4px;display: inline-block;" class="label label-primary1 "> '+defaultFieldsList[z]+'</span><span> </span>'
	}
	$("#defaultFileds").html(str1);
	
	//onChange source
	var object={};
	 object.buttonWidth = '461px';
	 object.disAbledRound = true
	 
	 var onChangeSource = function(){
			var selectedVal = $("[name='source']").find("select").val()
			var roleCallBackSuccessGetData = function(response){
				if (response && response.isException) {
					showNotification("error", response.customMessage);
					return;
				} else {
					var primaryKeydata = []
					var lastUpadate = []
					var createddate = []
					
					Jsonparse(response["1"]).data.forEach(function(primary){
						primaryKeydata.push(primary.col_name)
						
					})
					Jsonparse(response["2"]).data.forEach(function(lastupdate){
						lastUpadate.push(lastupdate.col_name)
						
					})
					Jsonparse(response["3"]).data.forEach(function(createdDate){
						createddate.push(createdDate.col_name)
						
					})
					
					$("#primaryKey").html("");
					var str='';
					for(var p=0;p<primaryKeydata.length;p++){
						if(primaryKeydata[p] && primaryKeydata[p] != undefined)
							str+='<span style="color:white;font-size: 12px;margin-bottom: 4px;display: inline-block;" class="label label-primary1 "> '+primaryKeydata[p]+'</span><span> </span>'
					}
					$("#primaryKey").html(str);
					
					$("#lastUpdataFiled").html("");
					var str='';
					for(var q=0;q<lastUpadate.length;q++){
						if(lastUpadate[q] && lastUpadate[q] != undefined)
							str+='<span style="color:white;font-size: 12px;margin-bottom: 4px;display: inline-block;" class="label label-primary1 "> '+lastUpadate[q]+'</span><span> </span>'
					}
					$("#lastUpdataFiled").html(str);
					
					$("#createDateFiled").html("")
					var str='';
					for(var r=0;r<createddate.length;r++){
						if(createddate[r] && createddate[r] != undefined)
							str+='<span style="color:white;font-size: 12px;margin-bottom: 4px;display: inline-block;" class="label label-primary1 "> '+createddate[r]+'</span> </span>'
					}
					$("#createDateFiled").html(str);
					
					
				}
			}
			
			var requests = [{
				filters:"1 and 1 and data_store_key = '"+selectedVal+"' and is_pk_col <> '0' ;",
				id:"1",
				table:"di_cfg_source_obj_columns",
				columns:'col_name',
//				query:"SELECT *  FROM di_cfg_source_obj_columns where data_store_key = '"+selectedVal+"' and is_pk_col <> '0' ;",
				profilekey:"primaryKey",
				source:"postgreSQL",
				database:"E2EMFPostGres",
				groupByEnabled:false,
				cacheDisabled:true
			},{
				filters:"1 and 1 and data_store_key = '"+selectedVal+"' and is_col_last_update_date = 'Y' ;",
				id:"2",
				table:"di_cfg_source_obj_columns",
				columns:'col_name',
//				query:"SELECT *  FROM di_cfg_source_obj_columns where data_store_key = '"+selectedVal+"' and is_col_last_update_date = 'Y' ;",
				profilekey:"lastupdate",
				source:"postgreSQL",
				database:"E2EMFPostGres",
				groupByEnabled:false,
				cacheDisabled:true
			},
			{filters:"1 and 1 and data_store_key = '"+selectedVal+"' and is_creation_date = '1' ;",
			id:"3",
			table:"di_cfg_source_obj_columns",
			columns:'col_name',
//			query:"SELECT *  FROM cur_relevance.di_cfg_source_obj_columns where data_store_key = '"+selectedVal+"' and is_creation_date = '1' ;",
			profilekey:"createdDate",
			source:"postgreSQL",
			database:"E2EMFPostGres",
			groupByEnabled:false,
			cacheDisabled:true
		}];
			var request ={
					"requests":JSON.stringify(requests)
			};
			enableLoading();
			callAjaxService("getMultipleData",roleCallBackSuccessGetData,callBackFailure,request,"POST",null,true);
			
		}
	// on edit 
	if(primaryKey && ele){
		var object={};
		 object.buttonWidth = '461px';
		 object.disAbledRound = true
		
		$('#targetName').val(columnData.name)
		$('#targetName').attr("disabled","disabled");
		$("#targetfolderPath").val(columnData.target_folder_path);
		 var index = columnData.index_data;
		 if(index == "1"){
			 $("#index").prop('checked',true)
		 }else{
			 $("#index").prop('checked',false)
		 }
		 
		
		
		constructDropDown("[name='connection']","SELECT distinct connection_id,'' FROM di_cfg_source_obj_columns where connection_id like '%<Q>%'",false, columnData.connection, onChangeConnection, "Select Connection","postgreSQL","E2EMFPostGres",object);
		constructDropDown("[name='source']","SELECT distinct data_store_key,'' FROM di_cfg_source_obj_columns where data_store_key like '%<Q>%'",false, columnData.source_datastore, onChangeSource, "Select Source","postgreSQL","E2EMFPostGres",object);
//		$('#primaryKey').val(Jsonparse(columnData.primary_key)?Jsonparse(columnData.primary_key)[0].name:"")
		
		var primaryKeydata = []
		var lastUpadate = []
		var createddate = []
					
		Jsonparse(columnData.primary_key).forEach(function(primary){
			primaryKeydata.push(primary.name)
						
		})
		Jsonparse(columnData.last_update).forEach(function(lastupdate){
			lastUpadate.push(lastupdate.name)
						
		})
		Jsonparse(columnData.create_date).forEach(function(createdDate){
			createddate.push(createdDate.name)
						
		})
		
		$("#primaryKey").html("");
		var str='';
		for(var p=0;p<primaryKeydata.length;p++){
			if(primaryKeydata[p] !="" && primaryKeydata[p] !=" ")
				str+='<span style="color:white;font-size: 12px;margin-bottom: 4px;display: inline-block;" class="label label-primary1 "> '+primaryKeydata[p]+'</span> </span>'
		}
		$("#primaryKey").html(str);
		
		$("#lastUpdataFiled").html("");
		var str='';
		for(var q=0;q<lastUpadate.length;q++){
			if(lastUpadate[q] !="" && lastUpadate[q] !=" ")
				str+='<span style="color:white;font-size: 12px;margin-bottom: 4px;display: inline-block;" class="label label-primary1 "> '+lastUpadate[q]+'</span> </span>'
		}
		$("#lastUpdataFiled").html(str);
		
		$("#createDateFiled").html("")
		var str='';
		for(var r=0;r<createddate.length;r++){
			if(createddate[r]!="" && createddate[r]!=" ")
				str+='<span style="color:white;font-size: 12px;margin-bottom: 4px;display: inline-block;" class="label label-primary1 "> '+createddate[r]+'</span> </span>'
		}
		$("#createDateFiled").html(str);
		
	}
	
	
	$(".dd-item").unbind().bind('click',function(){
//		$(".dd-handle.selected").removeClass("active");
//		$(".dd-handle.selected").removeClass("selected");
		$(this).find("div").toggleClass("active selected");
	})
	
	$("#addColumn").unbind().bind('click',function(){
		$(".dd-handle.active",$("#sortableAvaiable")).each(function() {
			// $("#sortableSelected").prepend($(this).parent());
			$("#sortableSelected").find("[name='txtsearchColumns']").after($(this).parent());
			$(".dd-handle.selected").removeClass("active");
			$(".dd-handle.selected").removeClass("selected");
		});
	});
	
	$("#addAllColumn").unbind().bind('click',function(){
		$(".dd-handle",$("#sortableAvaiable")).each(function() {
			// $("#sortableSelected").append($(this).parent());
			$("#sortableSelected").find("[name='txtsearchColumns']").after($(this).parent());
			$(".dd-handle.selected").removeClass("active");
			$(".dd-handle.selected").removeClass("selected");
		});
	});
	
	$("#removeColumn").unbind().bind('click',function(){
		$(".dd-handle.active",$("#sortableSelected")).each(function() {
			// $("#sortableAvaiable").prepend($(this).parent());
			$("#sortableAvaiable").find("[name='txtsearchColumns']").after($(this).parent());
			$(".dd-handle.selected").removeClass("active");
			$(".dd-handle.selected").removeClass("selected");
		});
	});
	$("#removeAll").unbind().bind('click',function(){
		$(".dd-handle",$("#sortableSelected")).each(function( ) {
		// $("#sortableAvaiable").append($(this).parent());
			$("#sortableAvaiable").find("[name='txtsearchColumns']").after($(this).parent());
			$(".dd-handle.selected").removeClass("active");
			$(".dd-handle.selected").removeClass("selected");
		});
	});
	
	$("#sortableSelected [name='txtsearchColumns'],#sortableAvaiable [name='txtsearchColumns']").keyup(function () {
		var rex = new RegExp($(this).val(), 'i');
        $(this).parent().find("li").hide();
        $(this).parent().find("li").filter(function () {
        	 return rex.test($(this).text());
        }).show();
    });
	
	$("#createTargetObject").unbind().bind('click',function(){
		// 'createDateFiled','lastUpdataFiled',
		var columnList = [ 'targetName', 'connection', 'targetFolder','source', 'primaryKey',"defaultFileds","data_columns" ,"create_date" , "last_update", 'index']
		var updateValue = []
		
		for (var x = 0; x < columnList.length; x++) {
			var data = {}
			var newValue
			var fieldType
			if (columnList[x] == 'targetName' || columnList[x] == 'connection' || columnList[x] == 'source' || columnList[x] == 'targetFolder' ) {
				fieldType = 'text'
				if(columnList[x] == 'targetName'){
					data.fieldName = "name"
					data.displayName = "name"
					data.newValue = $("#targetName").val()
					data.oldValue = ""
					data.type = "p"
				}
					
				// $("[name='connection']").find("select").val()[0]
				if(columnList[x] == 'connection'){
					data.fieldName = "connection"
					data.displayName = "connection"
					data.newValue = $("[name='connection']").find("select").val()
					data.oldValue = ""
					data.type = "p"
				}
				
				if(columnList[x] == 'targetFolder'){
					data.fieldName = "target_folder_path"
					data.displayName = "target_folder_path"
					data.newValue = $("#targetfolderPath").val()
					data.oldValue = ""
					data.type = "p"
				}
				if(columnList[x] == 'source'){
					data.fieldName = "source_datastore"
					data.displayName = "source_datastore"
					data.newValue = $("[name='source']").find("select").val()
					data.oldValue = ""
					data.type = "p"
				}
			}
			//[{"name":$("#primaryKey").val(),"type":"number"}]
			if(columnList[x] == 'primaryKey'){
					var primaryKeys = $("#primaryKey").find('span')
					var prm = []
					for(var t=0; t<primaryKeys.length;t++){
						if($(primaryKeys[t]).text()!="" && $(primaryKeys[t]).text()!=" ")
							prm.push({"name":$(primaryKeys[t]).text()})
					}
					fieldType = 'text'
					data.fieldName = "primary_key"
					data.displayName = "primary_key"
					data.newValue = JSON.stringify(prm)
					data.oldValue = ""
					data.type = "p"
					
			}
			//[{"name":"test","type":"Date"}]
			if(columnList[x] == 'defaultFileds'){
				fieldType = 'text'
					data.fieldName = "default_fields"
					data.displayName = "default_fields"
					data.newValue = '[{"name":"runDate"},{"name":"comb_key"},{"name":"batch"},{"name":"essId"}]'
					data.oldValue = ""
					data.type = "p"
					
			}
			
			if(columnList[x] == 'create_date'){
				var craetedDates = $("#createDateFiled").find('span')
				var created = []
				for(var p=0; p<craetedDates.length;p++){
					if($(craetedDates[p]).text()!="" && $(craetedDates[p]).text()!=" ")
						created.push({"name":$(craetedDates[p]).text().trim()})
				}
					fieldType = 'text'
					data.fieldName = "create_date"
					data.displayName = "create_date"
					data.newValue = JSON.stringify(created)
					data.oldValue = ""
					data.type = "p"
					
			}
			
			if(columnList[x] == 'last_update'){
					fieldType = 'text'
					var lastupdate = $("#lastUpdataFiled").find('span')
					var lastupdates = []
					for(var q=0; q<lastupdate.length;q++){
						if($(lastupdate[q]).text() !="" && $(lastupdate[q]).text() !=" ")
							lastupdates.push({"name":$(lastupdate[q]).text().trim()})
					}
					data.fieldName = "last_update"
					data.displayName = "last_update"
					data.newValue = JSON.stringify(lastupdates)
					data.oldValue = ""
					data.type = "p"
					
			}
			//[{"name":"test","type":"Date"}]
			if(columnList[x] == 'data_columns'){
				fieldType = 'text'
					data.fieldName = "data_columns"
					data.displayName = "data_columns"
					var columns = []
					for(var y = 0; y < $(".dd-handle",$("#sortableSelected")).length; y++){
						var value = $($(".dd-handle",$("#sortableSelected"))[y]).text()
						columns.push({"name":value})
					}
					
					data.newValue = JSON.stringify(columns)
					data.oldValue = ""
					data.type = "p"
					
			}
			
			if(columnList[x] == 'index'){
				data.fieldName = "index_data"
				data.displayName = "index_data"
				data.newValue = $("#index").is(':checked')?1:0
				data.oldValue = ""
				data.type = "p"
			}

				
			updateValue.push(data)
		}
		
		var request ={
				 source:'postgreSQL',
				 database:'E2EMFPostGres',
				 table:'di_cfg_target_obj',
				 primaryKey:primaryKey?[{"fieldName":'id','displayName':'id','newValue':primaryKey,'oldValue':''}]:null,
				 values:updateValue,
				 create:false
		 	};
		 var request = {
					"request":JSON.stringify(request)
			};
		callAjaxService("updateToDB",function(response){
			$('#myModalDrilldown').modal('hide');
			showNotification("success", "Data Updated Successfully");
			function callbackSucessClearCache() {
				dataAnalyser.profiling.grapher.refreshTable();
			}
			callAjaxService("clearCache", callbackSucessClearCache,
					callBackFailure, null, "POST");
		},callBackFailure,request,"POST");
		
	})
	
	var onChangeConnection = function(){
		var selectedVal = $("[name='connection']").find("select").val()
		var filterQuery = " 1=1 ";
		if(selectedVal)
			filterQuery = " connection_id = '"+selectedVal+"'";
		constructDropDown("[name='source']","SELECT distinct data_store_key,'' FROM di_cfg_source_obj_columns where data_store_key like '%<Q>%' and "+filterQuery+"",false, '', onChangeSource, "Select Source","postgreSQL","E2EMFPostGres",object);
	}
	
	
	if(!primaryKey){
		constructDropDown("[name='connection']","SELECT distinct connection_id,'' FROM di_cfg_source_obj_columns where connection_id like '%<Q>%'",false, '', onChangeConnection, "Select Connection","postgreSQL","E2EMFPostGres",object);
		 constructDropDown("[name='source']","SELECT distinct data_store_key,'' FROM di_cfg_source_obj_columns where data_store_key like '%<Q>%'",false, '', onChangeSource, "Select Source","postgreSQL","E2EMFPostGres",object);
	}
	 

}
	
	this.evidenceAddDocumentSubType = function() {
		$("#myModalDrilldown").addClass('plain-modal-theme');
		var str = '';
		str += '<div class="row"> <section>';
		str += '<label class="label">Type</label>';
		str += '<label class="input"><input type="text" class="docSubtypeName" value=""></label>';
		str += '</section></div>';
		str += '<div class="row" style="display: none;"> <section>';
		str += '<label class="checkbox enable-checkbox"><input type="checkbox" class="isActive" value=""><i></i>Active</label>';
		str += '</section></div>';

		str += '<footer class="modal-footer">';
		str += '<button type="button" class="pull-right btn btn-warning btnPSAdd" data-dismiss="modal" aria-hidden="true">Add</button>';
		str += '</footer>';

		$("#myModalLabel", "#myModalDrilldown").html("");
		$("#myModalLabel", '#myModalDrilldown').html(
				"<div> Add Document Subtype</div>");
		$(".modal-dialog", "#myModalDrilldown").addClass("width-50p");
		$(".modal-body", "#myModalDrilldown").addClass(
				"no-padding-top no-padding-bottom");
		$('.modal-body').css("max-height", "16em");
		$("#poDocView").addClass('smart-form').html(str);
		$('#myModalDrilldown').modal('show');

		$('.btnPSAdd')
				.on(
						'click',
						function() {
							var query = "insert into document_subtype (doc_type_name, is_active, created_by, created_at) values ('"
									+ $('.docSubTypeName').val()
									+ "', 'Active', '"
									+ $('#username').text()
									+ "', now());";

							var updateRequest = {
								"source" : 'postgreSQL',
								"dataBase" : 'EVDSEARCH',
								"queries" : btoa(query)
							}
							callAjaxService("integraGenericUpdate", function(
									response) {
								if (response && response.isException) {
									showNotification('error',
											response.customMessage);
									return;
								}
								showNotification('success',
										'Document Type Added Successfully');
								$("#applyFilter").trigger("click");
							}, callBackFailure, updateRequest, "POST", "json",
									true);
						});
	}
	this.evidenceAddDocumentClassification = function() {
		$("#myModalDrilldown").addClass('plain-modal-theme');
		var str = '';
		str += '<div class="row"> <section>';
		str += '<label class="label">Name</label>';
		str += '<label class="input"><input type="text" class="docClassificationName" value=""></label>';
		str += '</section></div>';
		str += '<div class="row" style="display: none;"> <section>';
		str += '<label class="checkbox enable-checkbox"><input type="checkbox" class="isActive" value=""><i></i>Active</label>';
		str += '</section></div>';

		str += '<footer class="modal-footer">';
		str += '<button type="button" class="pull-right btn btn-warning btnPSAdd" data-dismiss="modal" aria-hidden="true">Add</button>';
		str += '</footer>';

		$("#myModalLabel", "#myModalDrilldown").html("");
		$("#myModalLabel", '#myModalDrilldown').html(
				"<div> Add Document Classification </div>");
		$(".modal-dialog", "#myModalDrilldown").addClass("width-50p");
		$(".modal-body", "#myModalDrilldown").addClass(
				"no-padding-top no-padding-bottom");
		$('.modal-body').css("max-height", "16em");
		$("#poDocView").addClass('smart-form').html(str);
		$('#myModalDrilldown').modal('show');

		$('.btnPSAdd')
				.on(
						'click',
						function() {
							var query = "insert into document_classification (doc_type_name, is_active, created_by, created_at) values ('"
									+ $('.docSubTypeName').val()
									+ "', 'Active', '"
									+ $('#username').text()
									+ "', now());";

							var updateRequest = {
								"source" : 'postgreSQL',
								"dataBase" : 'EVDSEARCH',
								"queries" : btoa(query)
							}
							callAjaxService(
									"integraGenericUpdate",
									function(response) {
										if (response && response.isException) {
											showNotification('error',
													response.customMessage);
											return;
										}
										showNotification('success',
												'Document Classification Added Successfully');
										$("#applyFilter").trigger("click");
									}, callBackFailure, updateRequest, "POST",
									"json", true);
						});
	}

	this.evidenceAddFranchise = function() {
		$("#myModalDrilldown").addClass('plain-modal-theme');
		var psDrpdwnStr = '', selectRequest = {
			"source" : 'postgreSQL',
			"dbName" : 'EVDSEARCH',
			"tableName" : "evidence_platform_specialty where status = 'Active' order by platform_and_specialty",
			"columnName" : 'platform_and_specialty'
		};
		callAjaxService(
				"getIntegraCategories",
				function(response) {
					if (response && response.isException) {
						showNotification('error', response.customMessage);
						return;
					}
					response.map(function(a) {
						psDrpdwnStr += '<option value="' + a + '">' + a
								+ '</option>';
					});
					var isChecked = 'checked';

					var str = '';

					str += '<form id="frmFranchise" class="" novalidate="novalidate"><div class="row"> <section>';
					str += '<label class="label">JNJ Franchise</label>';
					str += '<label class="input"><input type="text" class="franchise" name="franchise" value=""></label>';
					str += '</section></div>';
					str += '<div class="row"> <section>';
					str += '<label class="label">JNJ Company Name</label>';
					str += '<label class="input"><input type="text" class="businessUnit" name="businessUnit" value=""></label>';
					str += '</section></div>';
					str += '<div class="row"> <section>';
					str += '<label class="label">JNJ Company synonyms</label>';
					str += '<label class="input"><input type="text" class="synonyms" name="synonyms" value=""></label>';
					str += '</section></div>';
					str += '<div class="row"> <section>';
					str += '<label class="label">Platform And Specialty</label>';
					str += '<label class="select"><select class="width-100per platformSpecialty" placeholder="Select Platform & Specialty">'
							+ psDrpdwnStr + '</select></label>';
					str += '</section></div>';

					str += '<div class="row"> <section>';
					str += '<label class="checkbox enable-checkbox"><input type="checkbox" class="status" '
							+ isChecked + ' value=""><i></i>Active</label>';
					str += '</section></div></form>';

					str += '<footer class="modal-footer">';
					str += '<button type="button" class="pull-right btn btn-warning btnPSAdd" aria-hidden="true">Add</button>';
					str += '</footer>';

					$("#myModalLabel", "#myModalDrilldown").html("");
					$("#myModalLabel", '#myModalDrilldown').html(
							"<div> Add Franchise</div>");
					$(".modal-dialog", "#myModalDrilldown").addClass(
							"width-50p");
					$(".modal-body", "#myModalDrilldown").addClass(
							"no-padding-top no-padding-bottom");
					$('.modal-body').css("max-height", "50em");
					$("#poDocView").addClass('smart-form').html(str);

					$('.platformSpecialty').select2();

					$('#myModalDrilldown').modal('show');
					// constructSelect2("[name='franchise']","select distinct
					// franchise,'' from evidence_franchise where
					// lower(franchise) like '%<Q>%'",false, "", null,null,
					// "Select franchise","postgreSQL", "EVDSEARCH");
					$("#frmFranchise")
							.validate(
									{
										rules : {
											franchise : {
												required : true
											},
											businessUnit : {
												required : true
											},
											platformSpecialty : {
												required : true
											}
										},
										messages : {
											franchise : {
												required : 'Please enter JNJ Franchise'
											},
											businessUnit : {
												required : 'Please enter JNJ Company Name'
											},
											platformSpecialty : {
												required : 'Please enter Platform And Specialty'
											}
										},
										errorPlacement : function(error,
												element) {
											error.insertAfter(element.parent());
										}
									});

					$('.btnPSAdd')
							.on(
									'click',
									function() {
										if (!$("#frmFranchise").valid())
											return;
										var franchise = $("[name='franchise']")
												.val();
										if (!franchise) {
											showNotification('notify',
													'Please Enter franchise.');
											return;
										} else
											franchise = franchise.trim();

										var active = $('.status')
												.is(':checked') ? 'Active'
												: 'Inactive';
										var query = "insert into evidence_franchise (franchise, business_unit, synonyms, platform_and_specialty, status, created_by) values "
												+ "('"
												+ franchise
												+ "','"
												+ $('.businessUnit').val()
												+ "','"
												+ $('.synonyms').val()
												+ "','"
												+ $(".platformSpecialty")
														.select2('val')
												+ "', '"
												+ active
												+ "', '"
												+ $('#username').text() + "');";

										var updateRequest = {
											"source" : 'postgreSQL',
											"dataBase" : 'EVDSEARCH',
											"queries" : btoa(query)
										}
										callAjaxService(
												"integraGenericUpdate",
												function(response) {
													if (response
															&& response.isException) {
														if (response.message
																.indexOf("ERROR: duplicate key") > -1) {
															showNotification(
																	'error',
																	"This Franchise already Exists");
														} else {
															showNotification(
																	'error',
																	response.customMessage);
														}
														return;
													}
													$('#myModalDrilldown')
															.modal('hide');
													showNotification('success',
															'Franchise Added Successfully');
													dataAnalyser.profiling.grapher.dataTable[0].ajax
															.reload();
												}, callBackFailure,
												updateRequest, "POST", "json",
												true);
									});
				}, callBackFailure, selectRequest, "POST", "json", true);

	}
	this.evidenceRejectUser = function() {
		$("#myModalConfirm").addClass('plain-modal-theme');
		$(".modal-dialog", "#myModalConfirm").addClass("width-50p");
		$(".modal-body", "#myModalConfirm").addClass(
				"no-padding-top no-padding-bottom");
		$('.modal-body').css("max-height", "50em");
		$('#myModalConfirm').modal('show');
		$('.dynamicSection').html(
				'<select class="rejectReason form-control">'
						+ lookUpTable.get('evdRejectionFields') + '</select>');
		$('#btnConfirm')
				.unbind('click')
				.bind(
						'click',
						function(e) {
							e.preventDefault();
							if (!$('.rejectReason').val()) {
								showNotification('error',
										'Please enter remarks to Reject the user');
								return;
							}
							$('#myModalConfirm').modal('hide');
							var query = "update users set status = 'Rejected', remarks = '"
									+ $('.rejectReason').val()
									+ "' where  status ='Pending' and  ";
							var wheres = [];
							var checkboxCount = 0;
							var emails = [];
							$('[name="checkboxinline"]')
									.each(
											function() {
												if ($(this).is(':checked')) {
													try {
														var data = $(this)
																.closest('tr')
																.data();
														var email = {};
														if (data.request) {
															var request = JSON
																	.parse(data.request);
															email.to = request.emailid;
															email.from = 'MDEvidencePortal@its.jnj.com';
															email.subject = lookUpTable
																	.get("evdRejectedSubject");
															var emailContent = lookUpTable
																	.get("evdRejectedMail");
															emailContent = emailContent
																	.replaceAll(
																			"{{rejectRemarks}}",
																			$(
																					'.rejectReason')
																					.val());
															emailContent = emailContent
																	.replaceAll(
																			"{{userName}}",
																			request.common_name);
															emailContent = emailContent
																	.replaceAll(
																			"{{businessRole}}",
																			request.business_role);
															email.content = emailContent;

															emails
																	.push(btoa(JSON
																			.stringify(email)));
														}
														data = JSON
																.parse(data.primarykeys);
														data = data[0].newValue;
														wheres
																.push("username = '"
																		+ data
																		+ "' ");
														checkboxCount++;
													} catch (e) {

													}
												}
											});

							if (checkboxCount > 0) {
								query += wheres.join(' OR ');
								query = btoa(query);
							} else {
								return;
							}
							var updateRequest = {
								"source" : 'postgreSQL',
								"dataBase" : 'E2EMFPostGres',
								"queries" : query
							};
							callAjaxService("integraGenericUpdate", function(
									response) {
								if (response && response.isException) {
									showNotification('error',
											response.customMessage);
									return;
								}
								showNotification('success',
										'Users Rejected Successfully');
								$.post(directoryName
										+ "/rest/email/sendGenericEmails", {
									"requestParam" : emails.join(",")
								}, function() {
									$("#applyFilter").trigger("click");
								});
							}, callBackFailure, updateRequest, "POST", "json",
									true);
						});
	}

	this.updateIntegraDashboard = function(columnName, dbName, tableName,
			source, inNotIN, primaryKeyName, pKeyType) {
		var ids = [];
		var queries = [];
		$('[name="checkboxinline"]').each(
				function() {
					var data = $(this).closest('tr').data();
					var pKeys = JSON.parse(data.primarykeys);
					var id = "";
					pKeys.map(function(a) {
						if (pKeyType === 'int') {
							ids.push(a['newValue']);
							id = a['newValue'];
						} else {
							ids.push("'" + a["newValue"] + "'");
							id = "'" + a["newValue"] + "'";
						}

					});
					var equalize = (inNotIN === "not in") ? " <> " : " = ";
					var query = "update " + tableName + " set " + columnName
							+ " = '" + $(this).prop('checked') + "' where "
							+ primaryKeyName + " " + equalize + id + ";"
					queries.push(btoa(query));
				});

		var updateRequest = {
			"source" : source,
			"dataBase" : dbName,
			"queries" : queries.join(',')
		};
		callAjaxService("integraGenericUpdate", function(response) {
			if (response && response.isException) {
				showNotification('error', response.customMessage);
				return;
			}
			showNotification('success', 'Columns Updated Successfully');
		}, callBackFailure, updateRequest, "POST", "json", true);
	};

	this.getMaterialByMaterialCode = function(matcode, element, table) {
		var columnConfiguration;
		$("#myModalDrilldown").modal("show");
		$("#poDocView").empty();
		$("#poDocView").addClass("panel-body").append($("<table/>").attr({
			"id" : "materialDatatable"
		}).addClass("table table-striped table-hover dc-data-table"));

		var config = {
			html : '#materialDatatable',
			url : serviceCallMethodNames["getDataPagination"].url,
			source : 'postgreSQL'
		};
		if (table) {
			var brand = $(element).attr("brand"), pdfpath = $(element).attr(
					"pdfpath");
			var classifiedno = $(element).attr("classifiedno");
			$("#printView")
					.find("h4")
					.addClass("row")
					.html(
							"<div name='headerinformation' class='col-sm-10'><table><tr><th>Parent File : </th><td><a href='"
									+ pdfpath
									+ "' target='_blank'>"
									+ matcode
									+ "</a></td></tr><tr><th>Brand : </th><td>"
									+ brand
									+ "</td></tr><tr><th>Total Number of Classifications : </th><td>"
									+ classifiedno + "</td></tr></table></div>");
			columnConfiguration = [ {
				data : "file_name",
				title : "File Name"
			}, {
				data : "classification_name",
				title : "Classification Name"
			}, {
				data : "status",
				title : "Status"
			}, {
				data : "file_link",
				title : "File Link"
			} ];
			config.filter = matcode ? " parent_file_name in ('" + matcode
					+ "') and  classification_name not in ('unclassified') "
					: "1 = 1 ";
			config.database = 'KALLIK';
			config.table = table;
			config.source = 'postgreSQL';
			config.columns = columnConfiguration;
		} else {
			var description = $(element).attr("description");
			var zzbase_desc = $(element).attr("zzbase_desc");
			var zzbase_code = $(element).attr("zzbase_code");
			$("#printView")
					.find("h4")
					.addClass("row")
					.html(
							"<div name='headerinformation' class='col-sm-10'><table><tr><th>Material <strong>&nbsp;&nbsp;&nbsp;:</strong> </th><td>&nbsp;"
									+ matcode
									+ " - "
									+ description
									+ "</td></tr><tr><th>Base code : </th><td>&nbsp;"
									+ zzbase_code
									+ " - "
									+ zzbase_desc
									+ "</td></tr></table></div>");
			columnConfiguration = [
					{
						data : "customer",
						title : "Customer Code"
					},
					{
						data : "cust_name",
						title : "Customer Name"
					},
					{
						data : "cust_land",
						title : "Customer Country"
					},
					{
						data : "fin_year",
						title : "Year"
					},
					{
						data : "fin_month",
						title : "Month"
					},
					{
						data : "net_val",
						"class" : "rightalign",
						title : "Net Sales Value",
						render : Function("data",
								"return data?'$'+d3.format(',.0f')(data):''")
					}, {
						data : "ship_to_details",
						title : "Ship To Details"
					} ];

			config.filter = matcode ? " material in ('" + matcode
					+ "') and  lower(ic_customer) in ('no')" : "1 = 1 ";
			config.database = 'SolenisPostGres';
			config.table = 'SOL_SALES_TOTAL';
			config.data = {
				orderby : 5
			};
			config.columns = columnConfiguration;
			var dataRequest = {
				source : "postgreSQL",
				database : "SolenisPostGres",
				groupByEnabled : true,
				table : "sol_proc_analysis",
				filters : "header_material in('" + matcode
						+ "') and lower(cmp_label) in ('rm')",
				columns : "price_chng_on_fg"
			};
			callAjaxService(
					"getData",
					function(response) {
						if (response && response.data
								&& response.data.length > 0) {
							var data = response.data[0];
							$("#printView")
									.find("[name='headerinformation']")
									.find("table")
									.append(
											$("<tr/>")
													.append(
															$("<th/>")
																	.html(
																			"FG Margin :"))
													.append(
															$("<td/>")
																	.html(
																			"&nbsp;"
																					+ d3
																							.format(
																									",.f")
																							(
																									d3
																											.round(
																													data.price_chng_on_fg,
																													3))
																					+ '%')
																	.addClass(
																			data.price_chng_on_fg > 0 ? "text-success"
																					: "text-danger")));
						}
					}, callBackFailure, dataRequest, "POST", null, true);
			// Excel Down load;
			var downLoadExcelbtn = $("<a/>").addClass(
					"btn marginRight10 pull-right btn-success col-sm-1").attr({
				"data-header" : "",
				"data-datatable" : "materialDatatable"
			}).unbind("click").bind(
					"click",
					{
						"tableconfig" : config
					},
					function(event) {
						var config = event.data.tableconfig;
						var tableSorting = $(config.html).dataTable()
								.fnSettings().aaSorting;
						var columns = "";
						config.columns = config.columns.concat({
							data : "material",
							title : "FG Code"
						}, {
							data : "mat_desc",
							title : "FG Name"
						}, {
							data : "curr",
							title : "Currency"
						}, {
							data : "ship_to_details",
							title : "Ship To Details"
						}, {
							data : "zzbase_code",
							title : "FG Base Code"
						}, {
							data : "zzbase_desc",
							title : "FG Base Code Description"
						});
						if (config.columns && config.columns.length > 0) {
							for (var i = 0; i < config.columns.length; i++) {
								(i > 0) ? columns += "," : "";
								columns += config.columns[i].data + ' AS "'
										+ config.columns[i].title + '"';
							}
						}
						var request = {
							"columns" : columns,
							"databaseColumns" : config.dataColumns,
							"orderField" : tableSorting[0][0],
							"orderType" : tableSorting[0][1],
							"searchValue" : $(
									'#poDocView .dataTables_filter input')
									.val(),
							"source" : config.source,
							"database" : config.database,
							"table" : config.table,
							"filter" : config.filter,
							"displayName" : "Data Table"
						};
						enableLoading();
						$.fileDownload(
								'rest/dataAnalyser/datatableExcelDownload', {
									successCallback : function() {
										disableLoading();
										showNotification("success",
												"Downloaded successfully.");
									},
									failCallback : function(responseHtml) {
										disableLoading();
										showNotification("error", $(
												responseHtml).text());
									},
									httpMethod : "POST",
									data : {
										"data" : JSON.stringify(request)
									}
								});
					}).append($("<i/>").addClass("fa fa-download")).append(
					"Excel");
			$("#printView").find("h4").append(downLoadExcelbtn);
		}
		config.dataColumns = $.map(columnConfiguration, function(val) {
			return val.data;
		}).join(',');
		constructGrid(config);

	};
	THIS.updateMapping = function() {
		callAjaxService("updateRAMapping", function() {
			showNotification("success", "Mapping Updated successfully.");
		}, callBackFailure, null, "POST");
	};
	THIS.saveData = function(config) {
		var datatableId = config.charts[0].alias;
		var editablefields = [ [], [] ];
		var columndetails = config.charts[0].columns;
		var tabledata = [];
		var counter = 0;
		var primarykey = [];
		var tablerow = $((datatableId)).find('tbody > tr');

		var getfieldType = function(edittype) {
			var fieldType = "text";
			if (edittype) {
				if (edittype === 'checkbox' || edittype === 'toggle')
					fieldType = "boolean";
				else if (edittype === 'number')
					fieldType = "number";
				else if (edittype === 'date')
					fieldType = "date";
			}
			return fieldType;
		};

		for (var i = 0, row = []; i < tablerow.length; i++) {
			row = [];
			if ($(tablerow[i]).attr('edited')) {
				for (var j = 0; j < tablerow[i].cells.length; j++) {
					row[j] = $(tablerow[i].cells[j]).html();
				}
				tabledata.push(row);
			}
		}
		if (tabledata.length === 0) {
			showNotification("noftify", "There is no change in Data");
			return;
		}

		for (var i = 0; i < columndetails.length; i++) {
			if (columndetails[i].editable) {
				editablefields[0].push(i);
				editablefields[1].push(columndetails[i].data);
			}
			if (columndetails[i].primarykey)
				primarykey = columndetails[i];
		}
		if (primarykey.length === 0) {
			showNotification("error", "Primary Key not configured");
			return;
		}
		var updateRequest = [];
		for (var i = 0; i < tabledata.length; i++) {
			var values = [];
			for (var j = 0; j < tabledata[i].length; j++) {
				if (editablefields[0].contains(j)) {
					var tempval = tabledata[i][j];
					var value = {
						"fieldName" : editablefields[1][editablefields[0]
								.indexOf(j)],
						"newValue" : (getfieldType(columndetails[j].edittype) == 'boolean') ? (tempval === 't') ? "true"
								: "false"
								: tempval,
						"fieldType" : getfieldType(columndetails[j].edittype),
						"type" : "U"
					};
					values.push(value);
				}
			}
			var req = {
				source : config.charts[0].data.source,
				database : config.charts[0].data.database,
				table : config.charts[0].data.table,
				values : values,
				primaryKey : [ {
					fieldName : primarykey.data,
					fieldType : getfieldType((primarykey.edittype) ? primarykey.edittype
							: null),
					enableAudit : false,
					newValue : tabledata[i][0]
				} ]
			};
			updateRequest.push(req);
		}
		for (var i = 0, row = []; i < tablerow.length; i++) {
			$(tablerow[i]).attr('edited', '');
		}
		var request = {
			"request" : JSON.stringify({
				rowsList : updateRequest
			})
		};
		callAjaxService("updateToDBForMultipeRows", function(response) {
			if (response && response.isException) {
				showNotification("error", response.customMessage);
				return;
			} else {
				showNotification("success", "User updated Sucessfully..");
			}
		}, callBackFailure, request, "POST");
	};
	THIS.compareColumns = function(data, url, fileExtension) {
		var trData = JSON.parse($(data).parent().parent().data().request);
		var fileName = trData.file_name;
		if (fileExtension) {
			fileName = trData.file_name.replace('xlsx', fileExtension)
		}

		var tableData = JSON.parse(trData.json);
		var table = $('<table/>').attr({
			"id" : "materialDatatable"
		}).addClass("table table-striped table-hover dc-data-table");
		var th = $('<tr/>');
		for ( var key in tableData[0]) {
			th.append($("<th/>").html(key));
		}
		table.append(th);
		for ( var i in tableData) {
			var td = $('<tr/>');
			for ( var j in tableData[i]) {
				td.append($('<td/>').html(tableData[i][j]));
			}
			table.append(td);
		}

		$("#myModalUpload").modal("show");
		$(".modal-dialog").addClass("width-95per");
		$(".modal-header .modal-title").text(
				trData.section_name.replace(":", " "));
		$(".modal-content #frmModalExportImport").remove();
		$(".modal-content .modal-body").remove();
		$(".modal-content")
				.append(
						$("<div/>")
								.addClass("modal-body row")
								.append(
										$("<div/>")
												.addClass("modal-body-content")
												.append(
														$("<div/>")
																.addClass(
																		"col-md-6")
																.append(
																		$(
																				"<embed/>")
																				.attr(
																						{
																							"src" : url
																									+ fileName,
																							"width" : 600,
																							"height" : 500,
																							"alt" : fileName
																						})))
												.append(
														$("<div/>").addClass(
																"col-md-6")
																.append(table))));
	}

	THIS.duplicateUpdation = function(seqNo, data, type) {
		var rowData = JSON
				.parse($(data).parent().parent().parent().data().request), request = {
			"seqNo" : seqNo,
			"status" : type,
			"groupId" : rowData['pg_idgroup']
		};

		callAjaxService("updateDuplicate", function(response) {
			if (response && response.isException) {
				showNotification("error", response.customMessage);
				return;
			} else {
				showNotification("success",
						"Duplicate table updated Sucessfully..");
				dataAnalyser.profiling.grapher.refreshTable();
			}
		}, callBackFailure, request, "POST");
	}
	THIS.cellClickMDMDrillDownCallBack = function(event, obj, targetLoc){
    	event.preventDefault();
    	event.stopPropagation();
    	event.stopImmediatePropagation();
    	var windowHashArr = [], windowHash = window.location.hash.split('#')[1];
    	if(lookUpTable.get('redirectPivotToAnotherPageArr') !== -1){
    		windowHashArr = (lookUpTable.get('redirectPivotToAnotherPageArr')).split(',');
    	}
    	if(windowHashArr.indexOf(windowHash) !== -1){
    		localStorage.setItem('myPivotProfileName', windowHash);
    		localStorage.setItem('myPivotPrimaryKey', JSON.parse($(obj).parent().parent().parents("tr").data("primarykeys"))[0].newValue);
    		if(targetLoc == 'new window'){
    			window.open((window.location.href.replace(window.location.hash, '#'+System.currentRole.toLocaleLowerCase().split('_')[1]+'_flow_level')), '_blank');
    		} else {
    			window.location.href = window.location.href.replace(window.location.hash, '#'+System.currentRole.toLocaleLowerCase().split('_')[1]+'_flow_level');
    		}
    	}
    }

	THIS.mergeIntoGroup = function(seqNo, data, type) {
		var rowData = JSON
				.parse($(data).parent().parent().parent().data().request);
		$('#main')
				.append(
						'<div class="modal fade" id="groupModalCenter" tabindex="-1"'
								+ 'role="dialog" aria-labelledby="groupModalCenterTitle" aria-hidden="true">'
								+ '<div class="modal-dialog modal-dialog-centered modal-xs" role="document" style="max-width: 500px;">'
								+ '<div class="modal-content">'
								+ '<div class="modal-header">'
								+ '<h5 class="modal-title" id="groupModalLongTitle"></h5>'
								+ '<button type="button" style="margin-top: -22px;" class="close" data-dismiss="modal" aria-label="Close">'
								+ '<span aria-hidden="true">&times;</span>'
								+ '</button>'
								+ '</div>'
								+ '<div class="modal-body" id="groupModalBody">'
								+ '<section id="groupModalText" style="display: none;">'
								+ '<input type="text" class="form-control groupModalClass" id="groupModalTextField" name="groupModalTextField" placeholder="Enter your group name"/>'
								+ '</section>'
								+ '<section id="groupModalSelect" style="display: none;">'
								+ '<select class="form-control groupModalClass" id="groupModalSelectField" name="groupModalSelectField" placeholder="Select your group name"></select>'
								+ '</section>'
								+ '</div>'
								+ '<div class="modal-footer" style="padding: 10px;margin-top: 0;" id="groupModalFooter">'
								+ '<button type="button" id="groupModalSubmit" class="btn btn-primary">Save</button>'
								+ '</div>' + '</div>' + '</div>' + '</div>');

		$('#groupModalText').hide();
		$('#groupModalSelect').hide();
		$('#groupModalTextField').val('');
		$('#groupModalSelectField').select2('val', rowData['dd_idgroup']);
		$('#groupModalFooter').find('.alert-warning').remove();
		$('#groupModalFooter').find('.error-msg').remove();
		$('#groupModalSubmit').prop('disabled', false);
		$('#groupModalSubmit').text("Save")
		if (type === 'createGroup') {
			$('#groupModalLongTitle').html("Create New Group");
			$('#groupModalText').show();
		} else if (type === 'mergeGroup') {
			$('#groupModalLongTitle').html("Merge Group");
			$('#groupModalSelect').show();
			if (!THIS.enableGroupClassListCall) {
				makeAjaxCall(null, 'select');
				THIS.enableGroupClassListCall = true;
			}
		} else {
			$('#groupModalLongTitle').html("Merge Record");
			$('#groupModalSelect').show();
			if (!THIS.enableGroupClassListCall) {
				makeAjaxCall(null, 'select');
				THIS.enableGroupClassListCall = true;
			}
		}
		$('.groupModalClass').on('keyup keypress blur change', function(e) {
			$('#groupModalFooter').find('.error-msg').remove();
			$('#groupModalSubmit').text("Save");
		});
		$('#groupModalCenter').modal('show');
		if ((rowData['dd_idgroup'] == null || rowData['dd_idgroup'] == "")
				&& type !== 'mergeRecord') {
			$('#groupModalSubmit').prop('disabled', true);
			$('#groupModalFooter')
					.append(
							'<div class="alert alert-warning pull-left" style="display:inline-block;width:80%;text-align:center;margin:0;padding:8px;">'
									+ '<strong>Warning!</strong> No groupId available to proceed further!. </div>');
		}
		var groupClass = null, query = "";
		$('#groupModalSubmit')
				.unbind('click')
				.bind(
						'click',
						function() {
							groupClass = (type === 'createGroup') ? $(
									'#groupModalTextField').val() : $(
									'#groupModalSelectField').select2('val');
							if (groupClass) {
								if (type === "createGroup") {
									query = "UPDATE duplicate_data SET group_class='"
											+ groupClass
											+ "' WHERE dd_seqno ='"
											+ rowData['dd_seqno'] + "'";
									if ($('#groupModalSubmit').text() === 'Merge') {
										makeAjaxCall(query, null,
												"mergeExisting");
										$('#groupModalCenter').modal('hide');
									} else {
										makeAjaxCall(query, 'isExist', type);
									}
								} else if (type === "mergeGroup") {
									query = "UPDATE duplicate_data SET group_class='"
											+ groupClass
											+ "' WHERE dd_idgroup ='"
											+ rowData['dd_idgroup'] + "'";
									makeAjaxCall(query, null, type);
								} else {
									query = "UPDATE duplicate_data SET group_class='"
											+ groupClass
											+ "' WHERE dd_seqno ="
											+ rowData['dd_seqno'];
									makeAjaxCall(query, null, type);
								}
							} else {
								var msg = (type === "createGroup") ? 'Enter'
										: 'Select';
								$('#groupModalFooter').find('.error-msg')
										.remove();
								$('#groupModalFooter')
										.append(
												'<div class="error-msg pull-left" style="color:red;padding: 7px 0 0 10px;">'
														+ '<strong>Please '
														+ msg
														+ ' your group class!</strong></div>');
							}
						});
		function makeAjaxCall(query, qryType, action) {
			enableLoading();
			var requestUrl = '', updateRequest = {
				"source" : 'postgreSQL'
			};
			if (qryType == 'select') {
				updateRequest["dbName"] = 'KALLIK';
				updateRequest["tableName"] = 'duplicate_data';
				updateRequest["columnName"] = 'dd_idgroup, group_class';
				updateRequest["_union"] = true;
				updateRequest["regEx"] = 'distinct';
				requestUrl = "getIntegraCategories";
			} else if (qryType == 'isExist') {
				updateRequest["dbName"] = 'KALLIK';
				updateRequest["tableName"] = 'duplicate_data WHERE LOWER(group_class) LIKE LOWER(\''
						+ groupClass.toString() + '\')';
				updateRequest["columnName"] = 'group_class';
				updateRequest["regEx"] = 'distinct';
				requestUrl = "getIntegraCategories";
			} else {
				updateRequest["dataBase"] = 'KALLIK';
				updateRequest["queries"] = btoa(query);
				requestUrl = "integraGenericUpdate";
			}
			callAjaxService(requestUrl, callBackMergeGroupSuccess,
					callBackFailure, updateRequest, "POST", "json", true);
			function callBackMergeGroupSuccess(response) {
				disableLoading();
				if (qryType == 'select') {
					var optionsAsString = "";
					$('#groupModalSelect').find('option').remove();
					if (response.length) {
						for (var i = 0; i < response.length; i++) {
							if (response[i] != null) {
								optionsAsString += "<option value='"
										+ JSON.parse(response[i])['dd_idgroup']
										+ "'>"
										+ JSON.parse(response[i])['dd_idgroup']
										+ "</option>";
							}
						}
						if (optionsAsString !== "") {
							$('select[name="groupModalSelectField"]').append(
									optionsAsString);
						} else {
							$('#groupModalSelect')
									.html(
											'<div class="alert alert-warning"><strong>Warning!</strong> No records are available!. Please create group.</div>');
						}
						$("#groupModalSelectField").select2();
					} else {
						$('#groupModalSelect')
								.html(
										'<div class="alert alert-warning"><strong>Warning!</strong> No records are available!. Please create group.</div>');
					}
				} else if (qryType == 'isExist') {
					if (response == null) {
						makeAjaxCall(query, null, "createGroup");
						$('#groupModalCenter').modal('hide');
					} else {
						$('#groupModalFooter').find('.error-msg').remove();
						$('#groupModalFooter')
								.append(
										'<div class="error-msg pull-left" style="color:red;padding: 0px; width: 80%;">'
												+ 'This group class already exists. Please choose new name or use the merge option to merge with existing group class.</div>');
						$('#groupModalSubmit').text("Merge");
					}
				} else {
					if (action === "createGroup") {
						showNotification("success",
								"New group class created successfully");
						THIS.enableGroupClassListCall = false;
					} else if (action === "mergeGroup") {
						showNotification("success",
								"Idgroup successfully merged into group class");
					} else if (action === "mergeExisting") {
						showNotification("success",
								"Group class successfully merged into existing group class");
					} else {
						showNotification("success",
								"Record successfully merged into group class");
					}
					$('#groupModalCenter').modal('hide');
					function callbackSucessClearCache() {
						dataAnalyser.profiling.grapher.refreshTable();
					}
					callAjaxService("clearCache", callbackSucessClearCache,
							callBackFailure, null, "POST");
				}
			}
			;
		}
	}
	   
	THIS.commentsPopup = function(commentSectionName, configWidget){
    	var commentList = [];
    	var profileName = $('.breadcrumb li').last().text().split('-')[0].trim();
    	var filters = dataAnalyser.getFiltersFromProfile();
    	var chatFilter = 0;
    	var filterQuery = '';
		var filterRequired = [];
    	if(filters){
    		for(var i = 0; i<filters.length; i++){
    			if(filters[i].chat && filters[i].defaultvalue && filters[i].defaultvalue.length>0){
    				var defaultValue = "";
    				if(filters[i].defaultvalue){
    					if(filters[i].fieldType === "lookupdropdown"){
							if(filters[i].isSingle)
								defaultValue = filters[i].defaultvalue;
							else if(filters[i].defaultvalue.length>0){
								defaultValue = filters[i].defaultvalue[0];
							}
						}else if(filters[i].fieldType === "lookup"){
							if(filters[i].isSingle)
								defaultValue = filters[i].defaultvalue.id;
							else if(filters[i].defaultvalue.length>0){
								defaultValue = filters[i].defaultvalue[0].id;
							}
						}
    				}
    				filterQuery += filters[i].fieldName.trim()+'_'+defaultValue.replace(/[^a-zA-Z0-9]/g,'_');
    			}
    			if(filters[i].chat){
    				chatFilter++;
					filterRequired.push(filters[i].displayName);//dynamic
    			}
    		}
    	}
		var addComment = function(){
			 var addAnimateComment = function(commentContent){			 	
				 	var str = '';
					var options = {
							  hour : '2-digit',
							  minute:'2-digit',
							  month: 'short',
							  day  : '2-digit'
						};
					var commentTimeStamp = new Date().toLocaleString('en-us',options);		
					if($('.no_comments').length>0 && $('.no_comments').css('display')!='none')
					{
						$('.no_comments').hide();
					}					
				 	str += ' <div class="outgoing_msg macro">';
					str += ' <div class="sent_msg">';
					str += ' <p>'+commentContent+'</p>';
					str += ' <small> <span class="sent_msg_time_date"> '+commentTimeStamp+'</span></small>';
					str += ' </div>';
					str += ' </div>';				
					$(str).hide().appendTo($('.comment_threads')).fadeIn('slow');	
					$('.comment_threads').animate({ scrollTop: $('.comment_threads').get(0).scrollHeight });
			 }
			var commentContent = $('.commentContent').val();
			if(commentContent){
				$('.commentContent').val('');
				var request= {						
						'commentSectionName': configWidget,
						'commentContent': commentContent,
						'profileName': profileName,
						'filterQuery' : filterQuery
						};
				callAjaxService("addComment",function(response){
					if (!response){
		                showNotification("error", "Error while importing file");
					}
		            else if (response && response.isException) {
		                	if(response.message.search('exceeds')||response.message.search('No Data Found')){
			                    showNotification("error", response.message);
		                	} else{
		                		showNotification("error", response.customMessage);
		                	}													                
		                    return;
		            	} else {
		            			//getComments();
		            			addAnimateComment(commentContent);
		            	}		
					}, callBackFailure,request, "POST",null,true);
			}
		};
		
    	var buildCommentsPanel = function(configWidget,lastRead){
			var str = '';
			var lastUnreadCount = 0;
			var lastUnreadIndex = 0;
			$("#myModalDrilldown").addClass('plain-modal-theme');
			$("#myModalLabel", "#myModalDrilldown").html("");  
			$("#myModalDrilldown .modal-dialog").css({"max-width": "700px"});
	        $(".modal-dialog","#myModalDrilldown").css({'overflow':'hidden'});
	        $(".modal-body","#myModalDrilldown").addClass("no-padding-top no-padding-bottom");
			$('#myModalDrilldown').css({'overflow':'hidden'}).modal('show');
			$("#printView").html("");
			$("#"+configWidget+"commentsPopup").find(".unread_span").hide();

			str += '<div class="modal-header modal-header-small">';
			str += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';        
			str += '<div class="modal-title"><div class="row">';				
			str += '<div class="col-sm-3"><h1 class="default_comment_head">Comments</h1></div>';
			str += '<div style="display: inline;" class="col-sm-5"><div class="profile_heading margin-top-10" style="white-space:pre-line">'+commentSectionName+'</div></div>';
			
			if(filters){
				str += '<div class="col-sm-4" >';
				str += '<div >Applied Filters</div>';
				str += '<table style="font-size: 13px; color: black; font-weight: 400;">'
		    	for(var i = 0; i<filters.length; i++){
		    		if(filters[i].chat && filters[i].defaultvalue && filters[i].defaultvalue.length>0){
		    			var defaultValue = "";
		    			if(filters[i].fieldType == "lookupdropdown"){
		    				if(filters[i].isSingle){
		    					defaultValue = filters[i].defaultvalue;
		    				}else{
		    					defaultValue = filters[i].defaultvalue[0];
		    				}
		    			}else if(filters[i].fieldType == "lookup"){
		    				if(filters[i].isSingle){
		    					defaultValue = filters[i].defaultvalue.id;
		    				}else{
		    					defaultValue = filters[i].defaultvalue[0].id;
		    				}
		    			}
		    			str += '<tr><td>'+filters[i].displayName.trim() +'</td><td>:</td> <td>&nbsp;'+defaultValue+'</td></tr>';
		    		}
		    	}
				str+='</table>';
				str += '</div></div>';
    		}
			str += '</div>';
			str += '</div>';

			str += '<div class="modal-body no-padding-top no-padding-bottom">';
			str += '<div class="comment_threads scrollbar">';			
			if(commentList.length==0){				
				str += '<p class="no_comments">No Comments Found</p>';
			}
			 
			for(var j = commentList.length-1;j>=0; j--){
				if(System.userName != commentList[j].userName && lastRead && commentList[j].commentTimeStamp>=lastRead)
					{
						lastUnreadCount++;
						lastUnreadIndex = j;
					}
			}
			for(var i =0; i< commentList.length; i++){
				var com = commentList[i];
				var nextComment = null;
				var lastComment ; 
				if( i+1< commentList.length ){
					nextComment = commentList[i+1];
				}
				var options = {
						  hour : '2-digit',
						  minute:'2-digit',
						  month: 'short',
						  day  : '2-digit'
					};
				var commentTimeStamp = new Date(com.commentTimeStamp).toLocaleString('en-us',options);		
				
				if(lastUnreadCount && i == lastUnreadIndex){
					str += ' <div class="chat_box_single_line">';
					str += ' <abbr class="unread_heading">('+lastUnreadCount+') &nbsp;Unread &nbsp;Comments  &nbsp;&nbsp; <i class="fa fa-chevron-down fa-1x"  aria-hidden="true" ></i></abbr>';
					str += ' </div>';
					lastUnreadCount = null;
				}
				
				if(System.userName == com.userName ){
					str += ' <div class="outgoing_msg macro">';
					str += ' <div class="sent_msg">';
					str += ' <p>'+com.comment+'</p>';
					str += ' <small> <span class="sent_msg_time_date"> '+commentTimeStamp+'</span></small>';
					str += ' </div>';
					str += ' </div>';
				}				
				else{	
					str += ' <div class="row incoming_msg">';												
					str += ' <div class="col-sm-10">';
					if(i==0 ||(i>0 && lastComment.userName != com.userName)){	
						str += ' <div class="incoming_msg_img col-sm-2"> <img src="https://ptetutorials.com/images/user-profile.png"> </div>';
						str += ' <div class="incoming_msg_sender_name">'+com.userName+'</div>';							
					}
					str += ' <div class="received_withd_msg msj">';
					str += ' <p>'+com.comment+'</p>';
					if(!nextComment ||(nextComment && nextComment.userName!=com.userName)||(nextComment && (new Date(nextComment.commentTimeStamp)-new Date(com.commentTimeStamp))/1000/60/60>1)){						
						str += ' <span class="time_date"> '+commentTimeStamp+'</span>';
					}
					str += ' </div>';
					str += ' </div>';				
					str += ' </div>';
				}
				lastComment = com;
			}
			str += '</div>';
	   		str += '<footer class="top_slicer comment_footer padding-top-10 padding-bottom-10 ">';
	   		str += '<div class="row">';		
			str += '<label class="col-md-10 col-xs-10 col-sm-10 col-lg-10" ><textarea type="text" class="commentContent form-control scrollbar" id="commentContent" value="" rows="2" placeholder="Add Comment here..."></textarea></label>';
			str += '<button class="msg_send_btn msg_send_btn_disabled" type="button" id ="addComment" ><i class="fa fa-paper-plane-o fa-2x"  aria-hidden="true" ></i></button>';//fa-play-circle 
			str += '</div>';
			str += '</footer>';
			str += '</div>';
			str += '</div>';

			$("#printView").html(str);

			$(".commentContent").on('keyup keypress change blur mouseover mouseout',function(e){
				if(e.which == 13&& e.currentTarget.value.trim().length>0 ){
					$("#addComment").trigger('click');
				}
				if( e.currentTarget.value.trim().length>0)
				{					
					$(".msg_send_btn").addClass('msg_send_btn_hover');
					$(".msg_send_btn").removeClass('msg_send_btn_disabled');
				}else {
					$(".msg_send_btn").removeClass('msg_send_btn_hover');
					$(".msg_send_btn").addClass('msg_send_btn_disabled');
				}				
			});
			$("#addComment").unbind("click").bind("click",addComment);
			
			$('#myModalDrilldown').on('shown.bs.modal',function(){
				if($(".chat_box_single_line").is(":visible")){
					$('.comment_threads').scrollTo($(".chat_box_single_line"));
				}
				else if($(".comment_threads").length>0 && $(".comment_threads").get(0).childNodes.length>1){
					$('.comment_threads').animate({ scrollTop: $('.comment_threads').get(0).scrollHeight }, "slow");
				}
			});
		};
									
		var getComments = function(){
			var request= {
					'profileName' : profileName,
					'commentSectionName' : configWidget,
					'filterQuery' : filterQuery,
					'setLastRead' : true
				};
			callAjaxService("getComments", function(response){
				if (!response){
	                showNotification("error", "Error while Requesting for Existing Comments");
				}
	            else if (response && response.isException) {
	                	if(response.message.search('exceeds')||response.message.search('No Data Found')){
		                    showNotification("error", response.message);
	                	} else{
	                		showNotification("error", response.customMessage);
	                	}													                
	                    return;
	            	} else {
	            		var lastRead;
	            		if(response.commentLog){
			    			responseCommentLog =  Jsonparse(response.commentLog);
			    		}
	            		for(var x = 0; x < responseCommentLog.length; x++ ){
	            			if(configWidget == responseCommentLog[x].commentPanelId)
	            				lastRead = responseCommentLog[x].lastread
	            		}
	            		response =  JSON.parse(response.commentList);
	            		commentList = [];
	            		for ( var i = 0; i < response.length; i++) {
	            			commentList.push(response[i]);																									
	            		}
	            		buildCommentsPanel(configWidget,lastRead);
	            	}
			}, callBackFailure,request, "POST",null,true);
		}
		if((dataAnalyser.filters && chatFilter > 0 && filterQuery  && filterQuery.length>0) || !dataAnalyser.filters|| (dataAnalyser.filters && filterRequired==0)){
			getComments();
		}
		else{
			showNotification("notify", "Please Select "+filterRequired.join(', ')+ ((chatFilter>1)?' Filters':' Filter'));
		}
    }

	THIS.addCrdNotes = function(obj){
		var dataTableId = "";
		var studyType = "CR&D";
		var allNotes = ["depot_notes","study_summary","supply_summary","remarks"];
        var isEdit=false;
        var savedData = $(obj).parent().data();
        if(window.location.hash.indexOf("StudyScreen_IIT")>-1){
        	studyType = "IIT";
        	allNotes = ["remarks"];
        }
		if(obj){
			isEdit = (Object.keys(savedData).length>0?true:false);
			if(savedData && savedData.request){
				savedData = JSON.parse(savedData.request);
			}
			dataTableId = $(obj).parents(".jarviswidget").find(".dataTables_wrapper").attr("id");
			dataTableId = dataTableId.replace("_wrapper","");
		}
		var callCrdNoteServcie = function(query,message){   
			query = btoa(unescape(encodeURIComponent(query)));
			var updateRequest = {
					"source" : 'postgreSQL',//'postgreSQL',
					"dataBase" : 'E2EMFPostGres',//'E2EMFPostGres',cassa_core,CELGENE_HIVE
					"queries" : query.toString()
				};
				$("#addNote").attr("disabled","disabled");
				callAjaxService("integraGenericUpdate",function(response){
					$("#addNote").removeAttr("disabled");
					if (!response){
		                showNotification("error", "Error while importing file");
					}
		            else if (response && response.isException) {
		                	if(response.message.search('exceeds')||response.message.search('No Data Found')){
			                    showNotification("error", response.message);
		                	} else{
		                		showNotification("error", response.customMessage);
		                	}													                
		                    return;
		            	} else {
							showNotification("success", message);
							callAjaxService("clearCache", function(){
								if(dataTableId && profileAnalyser.dataTable[dataTableId])
									profileAnalyser.dataTable[dataTableId].ajax.reload();
								},callBackFailure, null, "POST");
							
							$('#myModalDrilldown').modal('hide');

		            	}		
					}, callBackFailure,updateRequest, "POST",null,true);
		}
		var callCrdNoteServcies = function(query){   
		query = btoa(unescape(encodeURIComponent(query)));
		var updateRequest = {
				"source" : 'postgreSQL',//'postgreSQL',
				"dataBase" : 'E2EMFPostGres',//'E2EMFPostGres',cassa_core,CELGENE_HIVE
				"queries" : query.toString()
			};
			$("#addNote").attr("disabled","disabled");
			callAjaxService("integraGenericUpdate",function(response){
				$("#addNote").removeAttr("disabled");
				if (!response){
	                showNotification("error", "Error while importing file");
				}
	            else if (response && response.isException) {
	                	if(response.message.search('exceeds')||response.message.search('No Data Found')){
		                    showNotification("error", response.message);
	                	} else{
	                		showNotification("error", response.customMessage);
	                	}													                
	                    return;
	            	} else {
						callAjaxService("clearCache", function(){
							if(dataTableId && profileAnalyser.dataTable[dataTableId])
								profileAnalyser.dataTable[dataTableId].ajax.reload();
							},callBackFailure, null, "POST");
						
						

	            	}		
				}, callBackFailure,updateRequest, "POST",null,true);
	}
		var addNote = function(){
			var request = {};
			var protocol_num = $("#protocol").data("protocol");
			var projectName = $("#protocol").data("project") || '';
			var userName = System.userDisplayName;
			request.version = 1;
			for(var i=0;i<allNotes.length;i++){
				request[allNotes[i]] = CKEDITOR.instances[allNotes[i]].getData();
				request[allNotes[i]] = request[allNotes[i]].replace(/'/g, '<QT>');
			}
			// c_seq,protocol_num,depot_notes,study_summary,supply_summary,remarks,version,created_by,modified_by,created_on,modified_on
			if(request && (protocol_num || projectName)){
				var query;
				if((studyType === "CR&D") && (!request.depot_notes || !request.study_summary || !request.supply_summary)){
					showNotification("notify", "Please fill all mandatory fields.");
					return;
				}
				if(isEdit){
					var version = (parseInt(savedData.version)+1);
					query= "update crd_notes set depot_notes ='"+(request.depot_notes || '')+"',study_summary='"+(request.study_summary || '')+"',supply_summary='"+(request.supply_summary || '')+"',remarks='"+request.remarks+"',version='"+version+"',modified_by='"+System.userDisplayName+"',modified_on='"+getManipulatedDate("","","","%Y-%m-%d %H:%M:%S")+"' where c_seq = "+savedData.c_seq;
				}else{
					
					 query = "INSERT INTO crd_notes(source,project_name,protocol_num,depot_notes,study_summary,supply_summary,remarks,version,created_by,modified_by,created_on,modified_on)VALUES('"
						+studyType+"','"
						+(projectName || '')+"','"
						+(protocol_num || '')+"','"
						+(request.depot_notes || '')+"','"
						+(request.study_summary || '')+"','"
						+(request.supply_summary || '')+"','"
						+request.remarks+"','"
						+request.version+"','"
						+System.userDisplayName+"','"
						+System.userDisplayName+"','"
						+ getManipulatedDate("","","","%Y-%m-%d %H:%M:%S")+"','"
						+ getManipulatedDate("","","","%Y-%m-%d %H:%M:%S")
						+"');";
					 
					 
				} 
				callCrdNoteServcie(query,"Saved successfully.");
				
			}
		};
		
		var buildNoteModal = function(protocol,project){
           // var mode= $(obj).parent().data();
			var str = '';
			var lastUnreadCount = 0;
			var lastUnreadIndex = 0;
			$("#myModalDrilldown").addClass('plain-modal-theme');
			$("#myModalLabel", "#myModalDrilldown").html("");   
			//$("#myModalDrilldown .modal-dialog").css({"max-width": "700px"});
	        //$(".modal-dialog","#myModalDrilldown").css({'overflow':'hidden'});
	        $(".modal-body","#myModalDrilldown").addClass("no-padding-top no-padding-bottom");
			$('#myModalDrilldown').modal('show');			
			$("#printView").html("");
	
			str += '<div class="modal-header modal-header-small">';
			str += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button>';        
			str += '<div class="modal-title">';	
			(isEdit==true?str += '<h1 class="default_note_head" id="protocol">Study Assessment - Update </h1>':str += '<h1 class="default_note_head" id="protocol">Study Assessment - Create</h1>');
			str += '</div>';
			str += '</div>';
			str += '<div class="modal-body no-padding-top no-padding-bottom">';
			str += '<div class="row">';
			
			
			if(studyType === "CR&D"){
				str +=	'<div class="col-md-3 padding-top-5"><b class="">Study :</b><span name="protocol" class="margin-left-5"></span></div>';
				str += '<div class="col-md-9 padding-top-5"><b class="">Modified By : </b><span name="modified_by" class="margin-left-5"></span></div>';
				str +=	'<div class="col-md-3 padding-top-5"><b class="">Version :</b><span name="version" class="margin-left-5"></span></div>';			
				str += '<div class="col-md-9 padding-top-5"><b class="">Modified On : </b><span name="modified_on" class="margin-left-5"></span></div>';
			}else{
				str += '<div class="col-md-4 padding-top-5"><b class="">Project : </b><span name="project_name" class="margin-left-5"></span></div>';
				str +=	'<div class="col-md-4 padding-top-5"><b class="">Study :</b><span name="protocol" class="margin-left-5"></span></div>';
				str +=	'<div class="col-md-4 padding-top-5"><b class="">Version :</b><span name="version" class="margin-left-5"></span></div>';	
				str += '<div class="col-md-4 padding-top-5"><b class="">Modified By : </b><span name="modified_by" class="margin-left-5"></span></div>';
				str += '<div class="col-md-4 padding-top-5"><b class="">Modified On : </b><span name="modified_on" class="margin-left-5"></span></div>';
			    //str += '<div class="col-md-4 padding-top-5"><b class="">Time:</b> <select onchange="redirect()" name="interval" id="interval"> <option value="5">5</option> <option value="10">10</option>  </select></div>';
			}
			str += '</div>';

	   		str += '<footer style="padding:5px 0 10px 0">';
			str+= '<div name="readmode" style="display:none" class="row">';//<div class="form-control margin-20" style="height:auto">
			if(studyType === "CR&D"){
				str += '<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12" ><b>TMF Status :</b><div class="well padding-left-25" name="study_summary"></div></div>';
				str += '<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12" ><b>Import License Status :</b><div class="well  padding-left-25" name="supply_summary"></div></div>';
				str += '<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12" ><b>Study Depot Summary :</b><div class="well padding-left-25" name="depot_notes"></div></div>';
			}
			str += '<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12" ><b>Study Assessment Summary :</b><div class="well padding-left-25" name="remarks"></div></div>';
			str += '</div>';
			str+= '<div name="editmode" style="display:none" class="row">';
			if(studyType === "CR&D"){
				str += '<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 well padding-5" ><div class="padding-5"><b>TMF Status :<span style="color: red"> *</span></b></div><textarea type="text" class="form-control scrollbar" id="study_summary" value="" rows="2" placeholder="Add Note here..."></textarea></div>';
				str += '<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 well padding-5" ><div class="padding-5"><b>Import License Status :<span style="color: red"> *</span></b></div><textarea type="text" class="form-control scrollbar" id="supply_summary" value="" rows="2" placeholder="Add Note here..."></textarea></div>';
				str += '<div classs="col-md-12 col-xs-12 col-sm-12 col-lg-12 well padding-5" ><div class="padding-5"><b>Study Depot Summary :<span style="color: red"> *</span></b></div><textarea type="text" class="form-control scrollbar" id="depot_notes" value="" rows="2" placeholder="Add Note here..."></textarea></div>';
			}
			str += '<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 well padding-5" ><div class="padding-5"><b>Study Assessment Summary :</b></div><textarea type="text" class="form-control scrollbar" id="remarks" value="" rows="2" placeholder="Add Note here..."></textarea></div>';
			str += '</div>';
			str += '</div>';
			var deleteBtn = '';
			if(savedData && (System.userDisplayName === savedData.created_by)){
				deleteBtn = '<button class="btn btn-danger margin-10-0 marginRight10 border-round" style="display:none" type="button" id ="deleteNote" ><i class="fa fa-trash"></i> Delete</button>';
			}
			(isEdit==true?str += '<div class="row"  style="padding:10px 13px" id="noteActions"><div class="pull-right">'+deleteBtn+'<button class="btn btn-primary margin-10-0 marginRight10 border-round" type="button" id ="editNote" ><i class="fa fa-edit"></i> Edit</button><button class="btn btn-warning margin-10-0 marginRight10 border-round" type="button" id ="cancelNote" ><i class="fa fa-close"></i> Cancel</button><button style="display:none" class="btn btn-primary margin-10-0 marginRight10 border-round" type="button" id ="addNote" ><i class="fa fa-check-square-o"></i> Update</button></div></div>':str += '<div class="row"  style="padding:10px 13px" ><button class="btn btn-primary margin-10-0 pull-right marginRight10 border-round" type="button" id ="addNote" ><i class="fa fa-plus"></i> Create</button></div>');
			str += '</footer>';
			str += '</div>';
			str += '</div>';
			
			setTimeout(function(){
				var template = {
						study_summary:'<table cellspacing="1" cellpadding="1" style="width:100%;" border="1"><tbody><tr><th>QR Due Date</th><th>Issue Resolution Date</th><th>QR Review Period</th><th>Status</th></tr><tr><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td></tr></tbody></table>',
						supply_summary:'<table cellspacing="1" cellpadding="1" style="width:100%;" border="1"><tbody><tr><th>Country</th><th>Material</th><th>Expiry Date</th><th>Initial Quantity</th><th>Remaning Quantity</th></tr><tr><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td></tr></tbody></table>',
						depot_notes:'',
						remarks:''
				};
				var toolbar = [
						   		{ name: 'document', items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
						   		{ name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
						   		{ name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
						   		{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
						   		'/',
						   		{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
						   		{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
						   		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
						   		{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
						   		'/',
						   		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
						   		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
						   		{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
						   		{ name: 'about', items: [ 'About' ] }
						   	];
				for(var i=0;i<allNotes.length;i++){
					
					CKEDITOR.replace( allNotes[i], {
						toolbarCanCollapse:true,
						toolbarStartupExpanded: false,
						extraPlugins: 'base64image',
						removePlugins: 'elementspath', 
						height:['130px'],
						toolbar:toolbar,
						removeButtons: 'Image',
						//toolbar :'Basic',
						//removePlugins:'Source',
						//removePlugins:['Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates'],
						// Upload images to a CKFinder connector (note that the response type is set to JSON).
						uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
						startupFocus : true,
						enterMode : CKEDITOR.ENTER_BR,
				        shiftEnterMode: CKEDITOR.ENTER_P
						} );
					
					$("[name='protocol']").html(protocol || "N/A");
					if(isEdit){
						$("[name='editmode']").hide();
						$("[name='readmode']").show();
						$("[name='readmode']").find("div .well").each(function(index,ele) {
							var column = $(ele).attr("name");
							$(ele).html(savedData[column]);
						});
						$("[name='version']").html(savedData.version);
						$("[name='modified_by']").html(savedData.modified_by);
						$("[name='modified_on']").html(getManipulatedDate(null,0,savedData.modified_on,"%d %b %Y %H:%M:%S"));
						CKEDITOR.instances[allNotes[i]].setData(savedData[allNotes[i]].replace(/<QT>/g, "'"));
					}else{
						CKEDITOR.instances[allNotes[i]].setData(template[allNotes[i]]);
						$("[name='editmode']").show();
						$("[name='readmode']").hide();
						$("[name='version']").html(1);
						$("[name='modified_by']").html(System.userDisplayName);
						$("[name='modified_on']").html(getManipulatedDate("","","","%d %b %Y %H:%M:%S"));
					}
				}
				$('#myModalDrilldown').on('shown.bs.modal', function () {
				    $('#myModalDrilldown').animate({ scrollTop: 0 }, 'fast');
				});
				/*for (instance in CKEDITOR.instances)
				   {
				      var editor = CKEDITOR.instances[instance];
				      if ( editor )
				      {  
				        editor.on('focus', function (event)
				        {
				        	$('#'+event.editor.name+'TBdiv').show();
				         });
				        editor.on('blur', function (event)
				        {
				        	$('#'+event.editor.name+'TBdiv').hide();
				        });
				      }
				   }*/
				
			}, 100);
			$("#printView").html(str);			
			$("#protocol").data("protocol",protocol);
			if(project){
				$("[name='project_name']").html(project);
				$("#protocol").data("project",project);
			}else{
				$("[name='project_name']").html($("#f1").find("li:eq(1)").text());
			}
			$("#addNote").unbind("click").bind("click",addNote);
			$("#deleteNote").unbind("click").bind("click",function(){
				var isDelete = confirm("Are you sure want to delete this assessment.?");
				if(isDelete){
					var query= "DELETE FROM crd_notes  where c_seq = "+savedData.c_seq;
					callCrdNoteServcie(query,"Assessment Deleted successfully.");
				}
			});
			
			$("[name='readmode']").find("ul").css("padding-left",'40px');
			$("#editNote").unbind("click").bind("click",function(){ 
			
				$(this).hide();
				//$("#cancelNote").show();
				$("#deleteNote").show();
				$("[name='editmode']").show();
				$("[name='readmode']").hide();
				$("#addNote").show();
			
				var timer= setInterval(function(){
		        var request = {};
				var protocol_num = $("#protocol").data("protocol");
				var projectName = $("#protocol").data("project") || '';
				var userName = System.userDisplayName;
				request.version = 1;
				for(var i=0;i<allNotes.length;i++){
					request[allNotes[i]] = CKEDITOR.instances[allNotes[i]].getData();
				}
				
				var version = (parseInt(savedData.version)+1);
				query= "update crd_notes set depot_notes ='"+(request.depot_notes || '')+"',study_summary='"+(request.study_summary || '')+"',supply_summary='"+(request.supply_summary || '')+"',remarks='"+(request.remarks)+"',version='"+version+"',modified_by='"+System.userDisplayName+"',modified_on='"+getManipulatedDate("","","","%Y-%m-%d %H:%M:%S")+"' where c_seq = "+savedData.c_seq;
				callCrdNoteServcies(query);
				if(!$("#myModalDrilldown").hasClass('in'))
					clearInterval(timer);
		
			
				},lookUpTable.get("AssessmentIntervalSaving"));
				});
			$("#cancelNote").unbind("click").bind("click",function(){
				$("#myModalDrilldown").modal("hide");
				/*$(this).hide();
				$("[name='editmode']").hide();
				$("#deleteNote").hide();
				$("[name='readmode']").show();
				$("#editNote").show();
				$("#addNote").hide()*/;
			});
		};
		
		if(profileAnalyser.filters){
			var protocolindex = -1;
			if(studyType === "CR&D"){
				profileAnalyser.filters.forEach(function(filter,index){
					if(filter.fieldName.toLocaleLowerCase().indexOf('protocol')>-1){
						protocolindex= index;
					}
				});
				if(protocolindex!=-1){
					if(profileAnalyser.filters[protocolindex].defaultvalue && profileAnalyser.filters[protocolindex].defaultvalue.length>0 ){
						buildNoteModal(profileAnalyser.filters[protocolindex].defaultvalue[0].id || profileAnalyser.filters[protocolindex].defaultvalue[0]);
					}
					else{
						showNotification("notify", "Please Select "+profileAnalyser.filters[protocolindex].displayName);
					}
				}else{
					showNotification("notify", "Protocol Filter Not Found");
				}
			}else{
				var filterValues = {};
				profileAnalyser.filters.forEach(function(filter,index){
					if(filter.fieldName.toLocaleLowerCase().indexOf('protocol_num')>-1 || filter.fieldName.toLocaleLowerCase().indexOf('project_name')>-1){
						if(filter.defaultvalue && filter.defaultvalue.length>0){
							filterValues[filter.fieldName] = filter.defaultvalue[0].id || filter.defaultvalue[0];
						}
					}
				});
				if(filterValues && Object.keys(filterValues).length>0){
					buildNoteModal(filterValues['protocol_num'],filterValues['project_name']);
				}else{
					showNotification("notify", "Please Select Project or Study");
				}
			}
		}

	}
};
var actionCallbackHandler = new actionCallbackHandlerObj();
