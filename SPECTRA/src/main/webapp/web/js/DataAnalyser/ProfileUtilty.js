var profileUtiltyClass = function(profileParam, rowCallBack) {
	this.rowCallBack = rowCallBack;
	this.profileType = profileParam.profileType;// sankey or profile
	this.getAll = profileParam.getAll;// true or false
	this.requiredCol = profileParam.columns;
	this.isHistory = profileParam.isHistory;// true or false
	this.isPublish = profileParam.isPublish;
	this.otherButton = profileParam.otherButton;
	// var totalCol = profileParam.columns.length;
	var This = this;
	var currentProfileSource = 'myprofile';
	var columnDefs = [];
	var profiles = {};
	if(this.otherButton !=undefined && this.otherButton.length>0){
		for(var i=0 ;i<this.otherButton.length;i++)
			columnDefs.push(profileParam.otherButton[i]);
	}

	constructSelect2(
			"#appProfile",
			"select app as id, roles.displayname as text from profile_analyser inner join roles on profile_analyser.app = roles .name where lower(roles.displayname) || lower(roles.name) like '%<Q>%' group by app,roles.displayname",
			false, null, null, function(e) {
				onChangeOfApp(e);
			});

	$('.profile-tabs li a').on('click', function() {
		currentProfileSource = $(this).attr('data-attr');
		getPreviousSelectedProfiles(currentProfileSource);
	});

	if (This.isPublish) {
		var headers = ({
			"mDataProp" : "actionid",
			"sWidth" : "5%",
			"title" : "Status",
			"sDefaultContent" : ""
		});
		This.requiredCol.push(headers);
		columnDefs = [ {
			"targets" : This.requiredCol.length - 1,
			"width" : '5%',
			"createdCell" : function(td, cellData, rowData) {
				// actionId = rowData.actionid;
				var button = $("<button/>").addClass(
						rowData.profilekey ? "btn-outline-success"
								: "btn-outline").html(
						rowData.profilekey ? "Published" : "Publish");
				button.attr({
					'data-target' : '#profilePublishModal',
					'data-toggle' : 'modal',
					'data-refid' : rowData.id,
					'data-displayname' : rowData.displayname,
					'data-actionid' : rowData.actionId,
					'data-action' : rowData.profilekey
				});
				button.unbind('click').bind('click', function(e) {
					e.preventDefault();
					e.stopPropagation();
					previewPublishProfile(this);
				});
				$(td).html(button);
			}
		} ];
	}

	if (This.isHistory) {
		var historyBtn = ({
			"mDataProp" : "type",
			"sWidth" : "5%",
			"title" : "History",
			"sDefaultContent" : ""
		});
		This.requiredCol.push(historyBtn);
		var historyTarget = {
			"targets" : This.requiredCol.length - 1,
			"width" : '5%',
			"createdCell" : function(td, cellData, rowData) {
				var button = $("<button/>").addClass("btn-outline-warning btn")
						.html("History");
				button.data({
					"refid" : rowData.id
				});
				button.unbind('click').bind('click', function(e) {
					e.preventDefault();
					e.stopPropagation();
					showHistory(this);
				});
				$(td).html(button);
			}
		};
		columnDefs.push(historyTarget);

	}

	var modifiedDate = {
		"mDataProp" : "modifieddate",
		"sWidth" : '0%',
		"bVisible" : false,
		"title" : "modifieddate",
		"sDefaultContent" : ""
	};
	This.requiredCol.push(modifiedDate);

	this.getMyProfile = function() {
		var request = {
			"type" : this.profileType,
			"getAll" : this.getAll

		};
		callAjaxService("getMyProfiles", callBackGetMyProfiles,
				callBackFailure, request, "POST");
	};

	var callBackGetMyProfiles = function(response) {
		disableLoading();
		$("#selectSource").hide(300);
		$("#myProfiles").show(300);
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		var myProfileCount = response.filter(function(obj) {
			return (obj.createdby === System.userName);
		}).length;
		$("#myProfilediv").find("[name='myprofileCount']").html(myProfileCount);
		if (response !== undefined && response !== null) {
			var data = response, responseObj = {
				"Un Published" : []
			};
			for (var i = 0; i < data.length; i++) {
				if (data[i].lastmodifiedon) {
					data[i].modifieddate = new Date(data[i].lastmodifiedon)
							.getTime();
					data[i].lastmodifiedon = getFormattedDate(new Date(
							data[i].lastmodifiedon), null, true);
				}
				if (data[i].app && data[i].app !== "null") {
					if (!responseObj[data[i].app])
						responseObj[data[i].app] = [];
					responseObj[data[i].app].push(data[i]);
				} 
					responseObj["Un Published"].push(data[i]);
				
			}
			profiles = responseObj;
			if (currentProfileSource === "myprofile") {
				$('#myProfileActive').addClass('active');
				$('#allProfileActive').removeClass('active');
				var myProfiles = [], myLocalValue = Jsonparse(localStorage
						.getItem(This.profileType), "CONFIG LOCAL STORAGE");
				if (myLocalValue) {
					if (profiles[myLocalValue.name]) {
						myProfiles = profiles[myLocalValue.name]
								.filter(function(obj) {
									return (obj.createdby === System.userName);
								});
					} else {
						myProfiles = [];
					}
				} else {
					myProfiles = profiles["Un Published"].filter(function(obj) {
						return (obj.createdby === System.userName);
					});
				}
				var selectedValue = myLocalValue;
				$("#appProfile").select2('data', selectedValue);
				// if(myProfiles.length>0){
				$('#allprofilediv').hide();
				$('#myprofilediv').show();
				$('#selectedProfileName')
						.find("h4")
						.find("b")
						.html(
								"Saved Profile -"
										+ ((myLocalValue && myLocalValue.name) ? myLocalValue.name
												: "All Profile"))
						.append(
								$("<span/>")
										.addClass(
												"badge bg-color-blue txt-color-white margin-left-5")
										.html(myProfiles.length));
				constructAllProfilesGrid('#myProfileTable', myProfiles);
				// }

				$("#allProfilediv").find("[name='allprofileCount']").html(
						data.length);
			}

			templates.getWidget(null, function() {
				templates.getPie(null, function() {
					templates.getDatatable(null, function() {
						templates.getSummaryConfig(null, function() {
							templates.getSummary(null, function() {
								templates.getCalendar(null, function() {
									templates.getImageWidget(null, function() {
										templates.getNGram(null, function() {
											templates.getIFrame(null, function() {
												templates.getHTMLTemplate(null, function() {
												});
											});
										});
									});
								});
							});
						});
					});
				});
			});
		}
		$("#historyBack").unbind("click").bind("click", function() {
			$("#myProfiles").show(300);
			$("#profileHistroyDiv").hide(300);
		});
	};

	function getPreviousSelectedProfiles(currentProfileSource) {
		if (currentProfileSource === 'myprofile') {
			var myProfiles = [];
			var myLocalProfile = Jsonparse(localStorage
					.getItem(This.profileType));
			if (myLocalProfile) {
				if (profiles[myLocalProfile.name]) {
					myProfiles = profiles[myLocalProfile.name].filter(function(
							obj) {
						return (obj.createdby === System.userName);
					});
				}
			} else {
				myProfiles = profiles["Un Published"].filter(function(obj) {
					return (obj.createdby === System.userName);
				});
			}
			$("#appProfile").select2('data', myLocalProfile);
			// if(myProfiles.length>0){
			$('#allprofilediv').hide();
			$('#myprofilediv').show();
			$("#selectedProfileName")
					.find("h4")
					.find("b")
					.html(
							"Saved Profile -"
									+ ((myLocalProfile && myLocalProfile.name) ? myLocalProfile.name
											: "Un Published"))
					.append(
							$("<span/>")
									.addClass(
											"badge bg-color-blue txt-color-white margin-left-5")
									.html(myProfiles.length));
			constructAllProfilesGrid('#myProfileTable', myProfiles);
			// }
		} else if (currentProfileSource === 'allprofile') {
			var allLocalProfile = Jsonparse(localStorage
					.getItem(This.profileType + "All"), "CONFIG LOCAL STORAGE");
			var allprofiles;
			if (allLocalProfile) {
				allProfiles = profiles[allLocalProfile.name];
				if (allProfiles === undefined || allProfiles === null
						|| allProfiles === '') {
					allProfiles = [];
				}
			} else {
				allProfiles = profiles["Un Published"];
			}
			$("#appProfile").select2('data', allLocalProfile);
			$('#myprofilediv').hide();
			$('#allprofilediv').show();
			$("#selectedProfileName")
					.find("h4")
					.find("b")
					.html(
							"Saved Profile -"
									+ ((allLocalProfile && allLocalProfile.name) ? allLocalProfile.name
											: "Un Published"))
					.append(
							$("<span/>")
									.addClass(
											"badge bg-color-blue txt-color-white margin-left-5")
									.html(allProfiles.length));
			constructAllProfilesGrid('#allProfileTable', allProfiles);
		}
	}
	;

	function onChangeOfApp(e) {
		if (currentProfileSource === 'myprofile') {
			localStorage.setItem(This.profileType, JSON.stringify($(
					"#appProfile").select2("data")));
			var myProfiles = [];
			if (e.added && e.added.name) {
				if (profiles[e.added.name]) {
					myProfiles = profiles[e.added.name].filter(function(obj) {
						return (obj.createdby === System.userName);
					});
				} else {
					myProfiles = [];
				}

			} else {
				myProfiles = profiles["Un Published"].filter(function(obj) {
					return (obj.createdby === System.userName);
				});
			}
			var mySelectedVal = $("#appProfile").select2("data");

			$("#appProfile").select2('data', mySelectedVal);
			$('#allprofilediv').hide();
			$('#myprofilediv').show();
			$('#selectedProfileName')
					.find("h4")
					.find("b")
					.html(
							"Saved Profile -"
									+ ((e.added && e.added.name) ? e.added.name
											: "Un Published"))
					.append(
							$("<span/>")
									.addClass(
											"badge bg-color-blue txt-color-white margin-left-5")
									.html(myProfiles.length));
			constructAllProfilesGrid('#myProfileTable', myProfiles);
		} else if (currentProfileSource === 'allprofile') {
			var allprofiles;
			if (e.added && e.added.name) {
				allprofiles = profiles[e.added.name];
				if (allprofiles === undefined || allprofiles === null
						|| allprofiles === '') {
					allprofiles = [];
				}
			} else {
				allprofiles = profiles["Un Published"];
			}
			// allSelectedProfile= $("#appProfile").select2("data");
			localStorage.setItem(This.profileType + "All", JSON.stringify($(
					"#appProfile").select2("data")));
			$('#myprofilediv').hide();
			$('#allprofilediv').show();
			$('#selectedProfileName')
					.find("h4")
					.find("b")
					.html(
							"Saved Profile -"
									+ ((e.added && e.added.name) ? e.added.name
											: "Un Published"))
					.append(
							$("<span/>")
									.addClass(
											"badge bg-color-blue txt-color-white margin-left-5")
									.html(allprofiles.length));
			constructAllProfilesGrid('#allProfileTable', allprofiles);
		}
	}

	var constructAllProfilesGrid = function(tableId, data) {
		$(tableId).empty();
		$(tableId)
				.DataTable(
						{
							"dom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"
									+ "t"
									// / + "<tfoot> </tfoot>"
									+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							// "oColVis":{"aiExclude": [9]},
							"aaData" : data,
							"processing" : true,
							"lengthChange" : true,
							"sort" : true,
							"info" : true,
							"jQueryUI" : false,
							"paging" : true,
							// "scrollY": "350px",
							"bDestroy" : true,
							"bScrollCollapse" : true,
							"fixedHeader" : true,
							"autoWidth" : true,
							"destroy" : true,
							"order" : [ [ This.requiredCol.length - 1, "desc" ] ],
							"fnRowCallback" : function(nRow, aData) {
								$(nRow).data({
									"refid" : aData.id,
									"actionid" : aData.actionId
								});
								$(nRow).unbind("click").bind("click",
										function() {
											/*
											 * THIS.dataTableCounter = 0;
											 * THIS.facts = []; THIS.factsConfig =
											 * []; THIS.customFacts = [];
											 * THIS.dependentFacts = [];
											 */
											This.rowCallBack(this);
										});
							},
							// "aaSorting": [[ 3, "asc" ]],
							/*
							 * "aoColumnDefs": [{ "bVisible": false, "aTargets":
							 * [3] }],
							 */
							"fixedColumns" : true,
							"columnDefs" : columnDefs,
							"aoColumns" : This.requiredCol
						});

	};

	var previewPublishProfile = function(obj) {
		$("#userEnabled").removeAttr("checked");
		$("#smart-form-register input").removeAttr("readonly");
		if ($(obj).html() !== "Publish") {
			publishedActiondetails($(obj).attr("data-action"), $(obj).attr(
					"data-actionid"), $(obj).data("refid"));
		} else {
			var publishDataObj = {};
			publishDataObj.action = $(obj).data("displayname")
					.replace(/ /g, '');
			publishDataObj.displayname = $(obj).data("displayname");
			publishDataObj.refid = $(obj).data("refid");
			publishDataObj.source = "";

			var callBackPublishProfile = function(data) {
				$(obj).html("Published").removeClass("btn-outline").addClass(
						"btn-outline-success");
				$(obj).parent().parent().find(
						".text-left.col-md-2 col-xs-2 col-sm-2 col-lg-2").text(
						data.action);
				$(obj).parent().parent().find(
						".text-right col-md-1 col-xs-1 col-sm-1 col-lg-1")
						.attr("data-action", data.action);
			};

			$("#smart-form-register input").val("");
			$("#frmSankeyFilter em").remove();
			$(".state-error").removeClass("state-error");

			templates.managePublishing(publishDataObj, "CREATE",
					callBackPublishProfile);
		}
	};

	var publishedActiondetails = function(action, actionId, refid) {
		enableLoading();

		var callbackSucessGetAction = function(response) {
			if (response && response.isException) {
				showNotification("error", response.customMessage);
				return;
			} else {
				if (response === null) {
					disableLoading();
					alert("Selected Profile is Published as Tab");
					return;
				}

				response.refid = refid;
				response.actionId = Number(actionId);
				templates.managePublishing(response, "VIEW", undefined);
			}
		};

		var request = {
			"action" : action
		};
		callAjaxService("getAction", function(response) {
			callbackSucessGetAction(response);
		}, callBackFailure, request, "POST");
	};

	var showHistory = function(obj) {
		var request = {
			"type" : "history",
			"refid" : $(obj).data("refid")
		};
		enableLoading();
		callAjaxService("getProfileHistory", callBackGetProfileHistory,
				callBackFailure, request, "POST");
	};
	var callBackGetProfileHistory = function(response) {
		disableLoading();
		$("#myProfiles").hide(300);
		$("#profileHistroyDiv").show(300);
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null && response.length > 0) {
			var data = response;
			$("#profileHistoryList").empty();
			for (var i = 0; i < data.length; i++) {
				var actionId = 0;
				if (data[i].profilekey && data[i].profilekey !== null
						&& data[i].profilekey !== "") {
					actionId = data[i].actionid;
				}
				$("#profileHistoryList")
						.append(
								$("<li/>")
										.addClass("list-group-item")
										.data({
											"refid" : data[i].id,
											"actionid" : actionId
										})
										.append(
												$("<a/>")
														.append(
																$("<span/>")
																		.addClass(
																				"text-left col-md-2 col-xs-2 col-sm-2 col-lg-2")
																		.html(
																				data[i].displayname))
														.append(
																$("<span/>")
																		.addClass(
																				"text-left col-md-2 col-xs-2 col-sm-2 col-lg-2")
																		.html(
																				data[i].profilekey))
														.append(
																$("<span/>")
																		.addClass(
																				"text-left col-md-1 col-xs-1 col-sm-1 col-lg-1")
																		.html(
																				data[i].source))
														.append(
																$("<span/>")
																		.addClass(
																				"text-left col-md-1 col-xs-1 col-sm-1 col-lg-1")
																		.html(
																				data[i].database))
														.append(
																$("<span/>")
																		.addClass(
																				"text-left col-md-2 col-xs-2 col-sm-2 col-lg-2")
																		.html(
																				data[i].configtable))
														.append(
																$("<span/>")
																		.addClass(
																				"badge text-center col-md-2 col-xs-2 col-sm-2 col-lg-2")
																		.html(
																				data[i].lastmodifiedon))
														.append(
																$("<span/>")
																		.addClass(
																				"text-center col-md-2 col-xs-2 col-sm-2 col-lg-2")
																		.html(
																				data[i].lastmodifiedby))

										/*
										 * <button class="btn-outline btn
										 * margin-right-2"
										 * href="javascript:void(0);">spine</button>
										 */
										));
			}
			$("#profileHistoryList li").unbind("click").bind("click",
					function() {
						getProfileHistoryById(this);
					});
			templates.getWidget(null, function() {
				templates.getPie(null, function() {
					templates.getDatatable(null, function() {
						templates.getSummaryConfig(null, function() {
							templates.getSummary(null, function() {
								templates.getCalendar(null, function() {
									templates.getNGram(null, function() {
										templates.getIFrame(null, function() {
											templates.getHTMLTemplate(null, function() {
											});
										});
									});
								});
							});
						});
					});
				});
			});
		} else {
			$("#profileHistoryList")
					.html(
							'<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" href="#">×</a><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No History results found for selected Profile.</p></div>');
		}
	};

	var getProfileHistoryById = function(obj) {
		var id = $(obj).data("refid");
		var request = {
			"id" : id
		};
		enableLoading();
		$("#cloneProfile").show();
		callAjaxService("getProfileHistoryConfig", function(response) {
			dataAnalyser.callBackHistroyProfile(response, "history");
		}, callBackFailure, request, "POST");
	};
};
