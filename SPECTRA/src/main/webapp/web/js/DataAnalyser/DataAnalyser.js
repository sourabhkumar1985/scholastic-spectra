pageSetUp();
var DataAnalyser = function() {
	var THIS = this;
	THIS.ProfileName = "Profile View";
	THIS.isEditProfile = "false";
	THIS.dataTableCounter = 0;
	THIS.facts = [];
	THIS.factsConfig = [];
	THIS.customFacts = [];
	THIS.dependentFacts = [];
	THIS.profiling;
	THIS.allChartsConfig = {};
	THIS.actionData = {};
	THIS.profiles = {};
	THIS.databaseTableList=[];
	var chartConfigTable;
	var mySelectedProfile;
	var allSelectedProfile;
	var currentRangeType;
	var selectedRange;
	var selectedRangeDate;
	THIS.filterContentId;
	
	$(".jarviswidget-toggle-btn").bind('click',function(){
		$(this).find("i").toggleClass("fa-minus fa-plus");
		$(this).parents("header").next().toggle();
	});
	$("[name='btnAddFact']").unbind('click').bind('click',function(e){
		$("#txtFactName").removeAttr("disabled").val("").data("customtype",$(this).attr("customtype"));
		$("#factCalculations").val("");
	});
	/*
	 * window.onerror = function (msg, url, line, column, errorObj) {
	 * console.log("Error Message: ",msg ,"\nURL: ",url,"\nLine Number:
	 * ",line,"\nColumn Number: ",column,"\nError Stack: ", errorObj); return
	 * true; };
	 */
	$("#selectTable,#drawProfile,#myProfiles,#cloneProfile,#profileHistroyDiv").hide();
	$(".col-sm-3", "#selectSource").unbind("click").bind("click", function() {
		sourceSelect(this);
	});
	$("#dataSourceBack").unbind("click").bind("click", function() {
		$("#selectSource").show(300);
		$("#divDataSource").hide(300);
	});
	$("#tableBack").unbind("click").bind("click", function() {
		$("#selectSource").show(300);
		$("#selectTable").hide(300);
	});
	$("#myProfilesBack").unbind("click").bind("click", function() {
		$("#selectSource").show(300);
		$("#myProfiles").hide(300);
	});
	$("#frmSankeyFilter,#frmModalProfileActions").submit(function(e) {
		e.preventDefault();
	});
	var filterContentId="";	
	$("#profileBack")
			.unbind("click")
			.bind("click",
					function() {
						$.confirm({
									text : "It looks like you have been editing something. \n If you leave before saving, your changes will be lost.",
									confirm : function() {
										$("#selectTable").show(300);
										$("#drawProfile").hide(300);
										$("#addFacts").empty();
										$("#droppable > article").empty();
										$("#addFacts").html("drop measure here");
									},
									cancel : function() {
										// nothing to do
									}
								});

					});
	$("#exportProfile").unbind("click").bind("click", function() {
		exportProfile();
	});
	$("#importFile").unbind("click").bind("click", function() {
		importProfile();
	});
	$("#proceed").unbind("click").bind("click", function() {
		THIS.ProfileName = "Profile View";
		$("#cloneProfile").hide();
		THIS.dataQuery = null;
		$("[name=profilegroupby]").attr("checked", "checked");
		proceedColumn();
	});
	$("#saveProfile").unbind("click").bind("click", function() {
		saveProfile();
	});
	$("#cloneProfile").unbind("click").bind("click", function() {
		saveProfile(true);
	});
	$("#profileActions").unbind("click").bind("click", function() {
		getProfileActions();
	});
	$("#catalog li,#facts a").draggable({
		appendTo : "body",
		helper : "clone"
	});

	$("#previewProfile").unbind("click").bind("click", function() {
		previewProfile();
	});
	$("#addCalendar").unbind("click").bind("click", function() {
		addCalendar();
	});
	$("#addProfileAction").unbind("click").bind("click", function() {
		addProfileAction();
	});
	$("#profileMappingSave").unbind("click").bind("click", function() {
		profileMappingSave();
	});
	$("#editProfileMappingBack").unbind("click").bind("click", function() {
		editProfileMappingBack();
	});
	
	/* Added for NGram Started */
 
	$("#addNGram").unbind("click").bind("click", function() {
		var nGramCounter = 0;
		var columnFields = [];
		var callBackNGram = function(response) {
			$("#droppable > article").append(response);
			if (!$("#droppable > article").is(':empty')) {
				$('[id^=widget-grid]').jarvisWidgets('destroy');
			}
			pageSetUp();
			$("#tableNGram").select2({
				placeholder : "Choose table",
				data : THIS.tableList || []
			}).on('change', function(e) {
				var request = {
					"source" : THIS.sourceType,
					"database" : THIS.database,
					"table" : $("#tableNGram").select2("val")
				};
				enableLoading();
				callAjaxService("getColumnList", callBackSuccess,callBackFailure, request, "POST");
				function callBackSuccess(response){ 
					var tableFields = [];
					if(response.length>0){
						for ( var i = 0; i < response.length; i++) {
							tableFields.push({
								"id" : response[i].columnName,
								"text" : response[i].columnName
							});
						}
					}
					THIS.tableField = tableFields[0].text;
					$("#field").select2({
						placeholder : "Select field",
						data : tableFields || []
					}).on('change', function(e){
						THIS.tableField = e.val;
					}).select2('val', tableFields[0].text);
				}
				function callBackFailure(error){ console.log(error);}
			}).select2('val', THIS.table);

			THIS.tableField = THIS.ColumnList[0].text;
			
			$("#field").select2({
				placeholder : "Select field",
				data : THIS.ColumnList || []
			}).select2('val', THIS.ColumnList[0].text);
				
			$("#gramType").select2({
				placeholder : "Select Gram",
				data :  [{"id":"1","text":"Uni-grams"},{
						"id":"2","text":"Bi-grams"},{
						"id":"3","text":"Tri-grams"},{
						"id":"4","text":"4-grams"},{
						"id":"5","text":"5-grams"}]
			}).select2('val', "3");
			/*
			 * $("#nGramPreview").unbind("click").bind("click", function() {
			 * getNGramAnalyser(); });
			 */
		};
		templates.getNGram(null, callBackNGram, nGramCounter);
	});
	
	/* Added for NGram ended */
	
	/* Added for IFrame Started */
	$("#addIFrame").unbind("click").bind("click", function() {
		THIS.IFrameCounter = $("[id^=tableForIFrame]").length + 1;
		var callBackIFrame = function(response) {
			if (!$("#droppable > article").is(':empty')) {
				$('[id^=widget-grid]').jarvisWidgets('destroy');
			}
			$("#droppable > article").append(response);
			pageSetUp();
			$("[name=IFrameURL]",$("#wid-id-IFrame_"+ THIS.IFrameCounter)).val('');
		}
		templates.getIFrame(null, callBackIFrame, THIS.IFrameCounter);
	});
	/* Added for IFrame ended */
	
	/* Added for HTMLTemplate Started */
	$("#addHTMLTemplate").unbind("click").bind("click", function() {
		THIS.HTMLTemplateCounter = $("[id^=tableForHTMLTemplate]").length + 1;
		var callBackHTMLTemplate = function(response) {
			if (!$("#droppable > article").is(':empty')) {
				$('[id^=widget-grid]').jarvisWidgets('destroy');
			}
			$("#droppable > article").append(response);
			pageSetUp();

			CKEDITOR.replace( 'HTMLTemplateContent_'+THIS.HTMLTemplateCounter, { 
				extraPlugins: 'base64image',
				removeButtons: 'Image',
				// Upload images to a CKFinder connector (note that the response
				// type is set to JSON).
				uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
				startupFocus : true,
				enterMode : CKEDITOR.ENTER_BR,
		        shiftEnterMode: CKEDITOR.ENTER_P
			});
		}
		templates.getHTMLTemplate(null, callBackHTMLTemplate, THIS.HTMLTemplateCounter);
	});
	/* Added for HTMLTemplate ended */
	
	$("#addImageGrid").unbind("click").bind("click",function() {
		var callBackImageWidget = function(response) {
			var widget = $(response);
			widget.find("[name='columnForImageUrl'],[name='columnForFiltered']").select2({
				placeholder : "Select Column",
				data : THIS.ColumnList || []
			});
			widget.find("[name='configEditColumn']").select2({
				placeholder : "Select Column",
				multiple:true,
				maximumSelectionSize:3,
				data : THIS.ColumnList || []
			});
			widget.find("[name='addImageConfigAction']").unbind("click").bind("click",function(){
				addConfigAction(this);
			});
			$("#droppable > article").append(widget);
			if (!$("#droppable > article").is(':empty')) {
				$('[id^=widget-grid]').jarvisWidgets('destroy');
			}
			addConfigAction(widget.find("[name='addImageConfigAction']"),{action:"<i class='fa fa-check'></i> Submit",actionCallback:"UpdateToDB()",btntype:"btn-success"});
			pageSetUp();
		};
		templates.getImageWidget(null, callBackImageWidget);
	});
	// adding code for download report
	var count =0;
	$("#addDownloadReport").change(function() {
		        if($("#addDownloadReport").is(":checked")){
		        	$('#reportDownload').show();
		        	   var str = '<form id="actionPanelDownload"></form>';
		        	$('#actionBody').find('#filtersMap').before(str);
			           if(count === 0){
			        	   $(".report_actionValue").select2().on('change',function(e){
									var actionListLength  = $("#actionPanelDownload",$("#reportDownload")).children().length;
									if(e.val<=actionListLength){
										for(var i=actionListLength; i>e.val; i--){
											$($("#actionPanelDownload",$("#reportDownload")).children()[i-1]).remove();
										}
									}else{
										for(var i=actionListLength; i<e.val; i++){
											$("#actionPanelDownload",$("#reportDownload")).append('<fieldset class="col-md-4 col-xs-4 col-sm-4 col-lg-4"><input class="form-control" type="text" name="action_'+ i+'"> <span class="note"><i class="fa fa-check text-success"></i> Action HTML</span></fieldset>');
										}
									}
								}).select2('data',{id:2,text:2});
								$(".report_displaytype",$("#reportDownload")).select2().on('change',function(){
									$(this).data($(this).attr("name"),$(this).val());
								}).select2('val',"inline");
								$(".report_actioncolor",$("#reportDownload")).select2().on('change',function(){
									$(this).data($(this).attr("name"),$(this).val());
								}).select2('val',"btn-success");  
								
						} count++;
						
						// Download Excel & CSV will be available by default
						$("#actionPanelDownload",$("#reportDownload")).append('<fieldset class="col-md-4 col-xs-4 col-sm-4 col-lg-4"><input class="form-control" type="text" name="action_0"> <span class="note"><i class="fa fa-check text-success"></i> Action HTML</span></fieldset>');
						$("#actionPanelDownload",$("#reportDownload")).find('[name=action_0]').val("<i class='fa fa-download'></i> Excel");
						
						$("#actionPanelDownload",$("#reportDownload")).append('<fieldset class="col-md-4 col-xs-4 col-sm-4 col-lg-4"><input class="form-control" type="text" name="action_1"> <span class="note"><i class="fa fa-check text-success"></i> Action HTML</span></fieldset>');
						$("#actionPanelDownload",$("#reportDownload")).find('[name=action_1]').val("<i class='fa fa-download'></i> CSV");
				}
				   
				else
				   $('#reportDownload').hide();
		     });
	
	$("#reportAddFilter")
	.unbind("click")
	.bind("click",
			function() {
				profilesDiv = $("<div/>").attr({"name":"mappedFilter"}).addClass("row");
				var filterColumn=[] ;
				for(var i=0;i<dataAnalyser.filters.length;i++){
					filterColumn.push({"id":dataAnalyser.filters[i].fieldName,"text":dataAnalyser.filters[i].fieldName});
				}
				
				// for(var i=0;i<dataAnalyser.filters.length;i++){
					var columnsSelect = $("<input/>").attr({"name":"appliedFilter"}).addClass("width-100per");				
					profilesDiv.append($("<div/>").addClass("col-md-3")
							.append($("<strong/>").addClass("note").html("Filter"))
							.append(columnsSelect));				
					$(columnsSelect).select2({
						placeholder : "Select Column",
						allowClear: true,
						data :  filterColumn
					}).select2("val", '');
						// }
				$("#btnFilters",$("#actionBody")).append(profilesDiv);
			});
	
	
	var reportMappedFilter = function(){
		
	}
	
	// -----------------------------------------------------------------
	
	$("#addTable")
			.unbind("click")
			.bind(
					"click",
					function() {
						THIS.dataTableCounter = $("[id^=tableForDatatable]").length + 1;
						var callBackDatatable = function(response) {
							if (!$("#droppable > article").is(':empty')) {
								$('[id^=widget-grid]').jarvisWidgets('destroy');
							}
							$("#droppable > article").append(response);
							pageSetUp();
							$("#tableForDatatable_" + THIS.dataTableCounter)
									.select2({
										placeholder : "Choose table",
										data : THIS.tableList || []
									}).on("change",function(e) {
												THIS.dataTableCounter = e.target.id.replace("tableForDatatable_","");
												$(e.target).attr("data-tablename",e.val);
												datatableSelect(e.val,true);// last
																			// param
																			// is
																			// for
																			// isFromDataQuery
												// onChangeHistoryTable(e.val,$(this).parents(".jarviswidget-editbox").find("[name='tableForDatatable']").select2("val"),this);
											}).select2("val", THIS.table);
							$("[name='enableHistory']",$("#wid-id-datatable_"+ THIS.dataTableCounter)).on("change",function(){
								$(this).parents(".jarviswidget-editbox").find("[name='historySection']").toggle();
							});
							$("[name='tableHistory']",$("#wid-id-datatable_"+ THIS.dataTableCounter)).select2({
								placeholder : "Select table",
								data : THIS.tableList || []
							}).on("change",function(e) {
								onChangeHistoryTable(e.val,$(this).parents(".jarviswidget-editbox").find("[name='tableForDatatable']").select2("val"),this);
							});
							$("#tableForDatatable_" + THIS.dataTableCounter).attr("data-tablename", THIS.table);
							datatableSelect(THIS.table,true,THIS.dataTableCounter,true);
							$("[name=drilldowncallback]",$("#wid-id-datatable_"+ THIS.dataTableCounter)).keyup(	function() {
								$(this).data($(this).attr("name"),$(this).val());});
							$("[name=tableorderby]",$("#wid-id-datatable_"+ THIS.dataTableCounter)).bind('keyup mouseup',function() {
								$(this).data($(this).attr("name"),$(this).val());});
							$("[name=commentSectionName]",$("#wid-id-datatable_"+ THIS.dataTableCounter)).bind('keyup mouseup',function() {
										$(this).data($(this).attr("name"),$(this).val());});														
							$("[name=defaulttablefilter]",$("#wid-id-datatable_"+ THIS.dataTableCounter)).bind('keyup mouseup',function() {
								$(this).data($(this).attr("name"),$(this).val());});
							$("[name=formTitle]",$("#wid-id-datatable_"+ THIS.dataTableCounter)).bind('keyup mouseup',function() {
								$(this).data($(this).attr("name"),$(this).val());});
							$("[name=overrideFunction]",$("#wid-id-datatable_"+ THIS.dataTableCounter)).bind('keyup mouseup',function() {
								$(this).data($(this).attr("name"),$(this).val());});
							$(".select_actions",$("#wid-id-datatable_"+ THIS.dataTableCounter)).select2().on('change',function(e){
							// #BugFix-[JIRA] (PRZM-1062) Data table action not
							// configured properly
							    var tableId= $(this).parents().closest(".jarviswidget-sortable").attr('id');
							    var actionForm = tableId.replace('wid-id-datatable_','');
								var actionListLength  = $("#actionPanelForm",$("#"+tableId)).children().length;
								if(e.val<=actionListLength){
									for(var i=actionListLength; i>e.val; i--){
										$($("#actionPanelForm",$("#"+tableId)).children()[i-1]).remove();
									}
								}else{
									for(var i=actionListLength; i<e.val; i++){
										$("#actionPanelForm",$("#"+tableId)).append('<fieldset class="col-md-4 col-xs-4 col-sm-4 col-lg-4"><input class="form-control" type="text" name="action_'+actionForm +'_'+ i+'"> <span class="note"><i class="fa fa-check text-success"></i> Action Title</span>	<input class="form-control" type="text" name="actionCallback_'+actionForm +'_'+i+'"> <span class="note"><i class="fa fa-check text-success"></i> Callback function on Action</span> </fieldset>');
									}
								}
							}).select2('data',{id:2,text:2});
							$(".select_displaytype",$("#wid-id-datatable_"+ THIS.dataTableCounter)).select2().on('change',function(){
								$(this).data($(this).attr("name"),$(this).val());
							}).select2('val',"inline");
							$(".select_actioncolor",$("#wid-id-datatable_"+ THIS.dataTableCounter)).select2().on('change',function(){
								$(this).data($(this).attr("name"),$(this).val());
							}).select2('val',"btn-success");
							// Download Excel & CSV will be available by default
								$("#actionPanelForm",$("#wid-id-datatable_"+ THIS.dataTableCounter)).append('<fieldset class="col-md-4 col-xs-4 col-sm-4 col-lg-4"><input class="form-control" type="text" name="action_'+THIS.dataTableCounter +'_0"> <span class="note"><i class="fa fa-check text-success"></i> Action Title</span>	<input class="form-control" type="text" name="actionCallback_'+THIS.dataTableCounter +'_0"> <span class="note"><i class="fa fa-check text-success"></i> Callback function on Action</span> </fieldset>');
								$("#actionPanelForm",$("#wid-id-datatable_"+ THIS.dataTableCounter)).find('[name=action_'+THIS.dataTableCounter+'_0]').val("<i class='fa fa-download'></i> Excel");
								$("#actionPanelForm",$("#wid-id-datatable_"+ THIS.dataTableCounter)).find('[name=actionCallback_'+THIS.dataTableCounter+'_0]').val("downloadExcel()");
								
								$("#actionPanelForm",$("#wid-id-datatable_"+ THIS.dataTableCounter)).append('<fieldset class="col-md-4 col-xs-4 col-sm-4 col-lg-4"><input class="form-control" type="text" name="action_'+THIS.dataTableCounter +'_1"> <span class="note"><i class="fa fa-check text-success"></i> Action Title</span>	<input class="form-control" type="text" name="actionCallback_'+THIS.dataTableCounter +'_1"> <span class="note"><i class="fa fa-check text-success"></i> Callback function on Action</span> </fieldset>');
								$("#actionPanelForm",$("#wid-id-datatable_"+ THIS.dataTableCounter)).find('[name=action_'+THIS.dataTableCounter+'_1]').val("<i class='fa fa-download'></i> CSV");
								$("#actionPanelForm",$("#wid-id-datatable_"+ THIS.dataTableCounter)).find('[name=actionCallback_'+THIS.dataTableCounter+'_1]').val("downloadCSV()");
						};
						templates.getDatatable(null, callBackDatatable,THIS.dataTableCounter);
					});

	$(".notification-holder").droppable({
	      activeClass: "ui-state-default",
	      hoverClass: "ui-state-hover",
	      accept : ":not(.btn-fact,.chart-parent-div,.dd-item.ui-sortable-handle)",
	      drop: function( event, ui ) {
	    	  var $columnName = $(ui.draggable).text();
	    	  $(this).empty().append($("<a/>").addClass("btn-outline-warning btn margin-right-2 padding-5").attr("href","javascript:void(0);").html($columnName));
	      }
	    });
	
	$(".dragRemove")
			.droppable(
					{
						activeClass : "ui-state-default",
						hoverClass : "ui-state-hover",
						accept : ":not(.btn-fact)",
						drop : function(event, ui) {
							if (ui.draggable.hasClass("sortable-header")) {
								ui.draggable.parent().remove();
								ui.draggable.remove();
							} else if (ui.draggable.hasClass("chart-div")) {
								ui.draggable.empty().append(
										$("<span/>").addClass(
												"dimension-placeholder"));
							}
						}
					});
	$("#frmSankeyFilter").validate({
		ignore: [],
		rules : {
			profilename : {
				required : true
			},
			description : {
				required : true
			},
			appname:{
				required : true
			}
		},
		messages : {
			profilename : {
				required : 'Please enter profile name'
			},
			description : {
				required : 'Please describe profile'
			},
			appname : {
				required : 'Please select app name'
			}
		},
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	
	$("#frmPublishProfile").validate({

		// Rules for form validation
		rules : {
			action : {
				required : true
			},
			displayname : {
				required : true
			},
			parent : {
				required : true
			},
			sequence : {
				required : true
			}
		},

		// Messages for form validation
		messages : {
			action : {
				key : 'Please enter Action Key'
			},
			displayname : {
				required : 'Please enter Action Display Name'
			},
			parent : {
				key : 'Please enter Parent Key'
			},
			sequence : {
				required : 'Please enter Display Order'
			}
		},
		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	$("#frmModalProfileActions").validate({
		rules : {
			actionprofile : {
				required : true
			},
			actionname : {
				required : true
			}
		},
		messages : {
			actionprofile : {
				required : 'Please select linking profile'
			},
			actionname : {
				required : 'Please enter Action name'
			}
		},
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	$("#frmPublishProfile").submit(function(e) {
		e.preventDefault();
	});
	var addConfigAction = function(obj,data){
		$(obj).parents("table").find("[name='actionPanelTable']").append($("<tr/>")
				.append($("<td/>").append($("<input/>").addClass("form-control").attr({"name":"action"}).val((data&&data.action)?data.action:"")))
				.append($("<td/>").append($("<input/>").addClass("form-control").attr({"name":"actionCallback"}).val((data&&data.actionCallback)?data.actionCallback:"")))
				.append($("<td/>").append( $("<select/>").addClass("form-control").attr({"name":"btnType"})
						.append($("<option/>").attr({"value":"btn-default"}).html("Default"))
						.append($("<option/>").attr({"value":"btn-primary"}).html("Primary"))
						.append($("<option/>").attr({"value":"btn-success"}).html("Success"))
						.append($("<option/>").attr({"value":"btn-info"}).html("Info"))
						.append($("<option/>").attr({"value":"btn-warning"}).html("Warning"))
						.append($("<option/>").attr({"value":"btn-danger"}).html("Danger"))
						.append($("<option/>").attr({"value":"btn-link"}).html("Link"))
						.append($("<option/>").attr({"value":"bgBlue"}).html("Blue"))
						.append($("<option/>").attr({"value":"bgGreen"}).html("Green"))
						.append($("<option/>").attr({"value":"bgGrey"}).html("Grey"))
						.append($("<option/>").attr({"value":"bgPink"}).html("Pink"))
						.append($("<option/>").attr({"value":"bgBrown"}).html("Brown")).val((data&&data.btntype)?data.btntype:"btn-default")
						))
				.append($("<td/>").append( $("<a/>").attr({"href":"javascript:void(0)","title":"Remove Action"}).append($("<i/>").addClass("fa fa-md fa-fw fa-remove txt-color-red")).unbind("click").bind("click",function(){
					$(this).parents("tr").remove();
				}))));
	};
	var getChartConfigList = function(){
		enableLoading();
		callAjaxService("getChartConfig", function(response){
			disableLoading();
			THIS.allChartsConfig =  response;
		},callBackFailure, {}, "POST");
	};
	if ( window.location.hash.replace(/^#/, '') === "dataAnalyser"){
		getChartConfigList();
		$('body').toggleClass("minified");
	}
	var saveProfile = function(isClone) {
		var saveDataProfile = function() {
			
			var config = getConfig();
			var filters = getFilters();
			var summary = getSummary();
			var callBackSuccessHandler = callBackSaveProfile;
			var remarks;
			var displayName;
			var fact;
			if (THIS.facts.length === 0){
				fact = "fact";
			}else{
				fact = THIS.factsConfig;
			}
			var appName;
			if(!isClone){
				var profileID = $("#saveProfile").data("id")||0;
				if((profileID ===0)){ // Update values if saving on first time
					var app = $("#appname").select2("data");
					$("#saveProfile").data("remarks",$("#myModalthredlevel #description").val());
					$("#saveProfile").data("displayname",$("#myModalthredlevel #profilename").val());
					$("#saveProfile").data("app",app.name);
				
					appName = app.id;
					remarks = $("#myModalthredlevel #description").val();
					displayName = $("#myModalthredlevel #profilename").val();
				} else {
					remarks = $("#saveProfile").data("remarks");
					displayName = $("#saveProfile").data("displayname");
					appName = $("#saveProfile").data("app");
				}
				$("#wid-id-profiling > header > h2").html(displayName);
				callBackSuccessHandler = callBackSaveProfile;
			} else {
				callBackSuccessHandler = function() {
					disableLoading();
					$('#myModalthredlevel').modal('hide');
					showNotification("success",
							"Data Profile cloned successfully..");
				};
				remarks = $("#myModalthredlevel #description").val();
				displayName = $("#myModalthredlevel #profilename").val();
				appName = $("#appname").select2("data").name;
			}
			var additionalconfig = {};
			if($("#addDownloadReport").is(":checked")){
				var actionData = [];
				var applicableFilters=[];
				$($("#actionPanelDownload",$("#reportDownload"))).children().each(function(index,item){
					if($(item).find('[name=action_'+index+']').val()){
						var obj = {};
						obj.action = $(item).find('[name=action_'+index+']').val().replace(/'/g,"@");
						obj.action = obj.action.replace(/"/g,"`");
						// obj.actionCallback = $(item).find('[name=actionCallback_'+index+']').val().replace(/'/g,"@");
						actionData.push(obj);
					}
				});
				additionalconfig.isReportDownload = actionData;
				var configObj ={}
				configObj.actiondisplaytype = $("[name=reportdisplaytype]",$("#reportDownload")).data("reportdisplaytype");
				configObj.actioncolorcode = $("[name=colorcodeValue]",$("#reportDownload")).data("actiondisplaytype");
				additionalconfig.reportConfig =  configObj;
				$("[name='mappedFilter']")
				.each(
						function(index) {var currentObj = $(this); 
						console.log(currentObj.find("[name='appliedFilter']").select2('val'));
						if (currentObj.find("[name=appliedFilter]").select2('val')
								&& currentObj.find("[name=appliedFilter]").select2(
										'val') !== "")
						applicableFilters.push(currentObj.find("[name='appliedFilter']").select2('val'));
						})
						additionalconfig.applicableFilters = applicableFilters; 	
			}
			if ($("#filterNotapplicable").is(":checked")) {
				additionalconfig.isFilterNotapplicable = true;
			}
			else{
				additionalconfig.isFilterNotapplicable = false;
				
			}
			if ($("#filterInterdependent").is(":checked")) { 
				additionalconfig.filterInterdependent = true;
		    }
			if ($("#filterModalInlineswitch").is(":checked")) { 
				additionalconfig.isFilterInline = true;
			}
			if($("#filterShowHide").is(":checked")){
				additionalconfig.isFilterHide = true;
			}
			if($("#hideMinified").is(":checked")){
				additionalconfig.isMinified = true;
			}
			if($("#hideemail").is(":checked")){
				additionalconfig.hideemail = true;
			}
			if($("#hideView").is(":checked")){
				additionalconfig.hideView = true;
			}
			if ($("[name=profilegroupby]").is(":checked")) {
				additionalconfig.profileGroupBy = true;
			}
			if ($("#pdfDownload").is(":checked")) {
				additionalconfig.pdfDownload = true;
			}
			
			if(THIS.additionalconfig && THIS.additionalconfig.dataSourceId){
				additionalconfig.dataSourceId = THIS.additionalconfig.dataSourceId;
			}
			var profilePalette = $("#profileColorPalette").attr("name");
			if (profilePalette !== "") {
				additionalconfig.profilePalette = profilePalette;
			}
			if($("#measureDisplayName").val().trim() !== ""){
				additionalconfig.measureDisplayName = $("#measureDisplayName").val();
			}
			additionalconfig = JSON.stringify(additionalconfig);
			var request = {
				"source" : THIS.sourceType,
				"database" : THIS.database,
				"table" : THIS.table,
				"fact" : JSON.stringify(fact),
				"customfacts" : JSON.stringify(THIS.customFacts).replace(/'/g,"@"),
				"config" : JSON.stringify(config),
				"filters" : JSON.stringify(filters),
				"summary" : JSON.stringify(summary),
				"remarks" : remarks,
				"displayname" : displayName,
				"additionalconfig" : additionalconfig,
				"type" : "profile",
				"app":appName,
				"id" : isClone === true ? 0 : $("#saveProfile").data("id")
			};
			if ($(".notification-holder a") && $(".notification-holder a") !== null &&$(".notification-holder a").length > 0){
				request.notificationFilter = $(".notification-holder a").text();
			}
			enableLoading();
			callAjaxService("saveProfile", callBackSuccessHandler,
					callBackFailure, request, "POST");
		};
		var profileID = $("#saveProfile").data("id")||0;
		if (isClone || profileID ===0) {
			$("#myModalthredlevel #description").val("");
			$("#myModalthredlevel #profilename").val("");
			constructSelect2("#appname","select name,displayname from roles where lower(displayname) like '%<Q>%'",false,null,"name");
			$("#myModalthredlevel").modal('show');
			$("#myModalthredlevel #btnCreate").unbind("click").bind("click",
					function() {
						if (!$("#frmSankeyFilter").valid())
							return;
						saveDataProfile();
					});
		} else {
			saveDataProfile();
		}
	};
	var callBackSaveProfile = function(response) {
		if ((response && response.isException)|| (response.message && response.message ==="failure")) {
			showNotification("error", response.customMessage||response.message);
			return;
		}
		if (response !== undefined && response !== null) {
			disableLoading();
			$('#myModalthredlevel').modal('hide');
			showNotification("success", "Profile saved successfully.");
			$("#saveProfile").data("id", response);
			$("#cloneProfile").show();
		}
	};
	var datatableSelect = function(table,isTableChange,idCounter,isFromDataQuery) {
		THIS.datatable = table;
		var request = {
			"source" : THIS.sourceType,
			"database" : THIS.database
		};
		if(THIS.dataQuery && isFromDataQuery){
			request.query = calculateDynamicFilter(THIS.dataQuery,THIS.filters);
		}else{
			request.table = THIS.datatable;
		}
		enableLoading();
		callAjaxService("getColumnList", function(response){callBackGetdatatableSelect(response,table,isTableChange,idCounter);},
				callBackFailure, request, "POST");
	};
	var callBackGetdatatableSelect = function(response,table,isTableChange,idCounter) { 
		isTableChange = isTableChange || false;
		idCounter = idCounter || THIS.dataTableCounter;
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			var datatableHeader = $(".header","#wid-id-datatable_" + THIS.dataTableCounter).empty();
			$("tbody", "#wid-id-datatable_" + THIS.dataTableCounter).remove();
			$("<div/>").append(
					$("<div/>").addClass("smart-form").append(
							$("<div/>").addClass("note").append(
									$("<strong>").html("Display name")).append(
									$("<label/>").addClass("input").append(
											$("<input/>").attr({
												"type" : "text",
												"name" : "displayname"
											}).addClass("input-xs")))));
			var columns = "";
			var columnObject = []; 
			for ( var a = 0; a < response.length; a++) {
				datatableHeader
						.append($("<th/>")
								.addClass("numbercolumn sorting")
								.attr(
										{
											"tabindex" : "0",
											"aria-controls" : "DataTables_Table_"
													+ $('[id^=DataTables_Table_]').length,
											"rowspan" : "1",
											"colspan" : "1",
											"aria-label" : response[a].columnName
										})

								.append($("<div/>").attr({
															"data-field" : response[a].data
														}).addClass("sortable-header")
												.append($("<a/>").addClass('axisHeader')
																.html(response[a].columnName)
																.attr({
																			"data-field" : response[a].columnName
																		}))));
				var column = {"data" : response[a].columnName,"defaultContent" : ""};
					column.title = response[a].columnName;
				columnObject.push(column);
				if (columns === "")
					columns += response[a].columnName;
				else
					columns += ","+ response[a].columnName;

			}
			if (isTableChange){
				$("#configure_"+THIS.dataTableCounter).unbind("click").data("table",table).data("counter",THIS.dataTableCounter).bind("click",function(){
					configureColumns(this, table,columnObject,columns);
				});
			}
			$("table", "#wid-id-datatable_" + THIS.dataTableCounter)
					.append($("<tbody/>")
						.append($("<tr/>").addClass("bg-color-white")
							.append($("<td/>").addClass("dataTables_empty").attr({"valign" : "top","colspan" : response.length + 1}).html("Data will appear on preview"))));
				}
		var historyTable = $("#wid-id-datatable_"+ idCounter).find("[name='tableHistory']").select2("val");
		if(historyTable){
			var mainTable = $("#wid-id-datatable_"+ idCounter).find("[name='tableForDatatable']").select2("val");
			onChangeHistoryTable(historyTable,mainTable,$("#wid-id-datatable_"+ idCounter).find("[name='tableHistory']"));
		}
	};
	$("#addSummary").unbind("click").bind(
			"click",
			function() {
				var callBackSummary = function(response) {
					var summary = $(response);
					var summaryVar = $("<div/>").addClass("chart-parent-div profile-summary-div");
					if($("#droppable > article").find("#configSummaryDiv").length === 0){
						$("#droppable > article").prepend($("<div/>").attr("id","configSummaryDiv"));
					}
					if($("#droppable > article").find("[name='configSummary']").length%6 === 0){
						$("#configSummaryDiv").append(summaryVar);
					}
					$("#configSummaryDiv").find(".profile-summary-div").last().append(summary);
					redrawSummary();
					axisLabelClick(summary.find("[name='configSummary']"));
				};
				templates.getSummary(null, callBackSummary);
			});
	function redrawSummary(){
		var summaryCount = $("#configSummaryDiv").find(".profile-summary-div").last().find(".chart-summary").length;
		var summaryClass = "";
		if (summaryCount === 5)
			summaryClass = "col-md-15 col-md-3 col-sm-3";
		else
			summaryClass = "col-md-" + (12 / summaryCount) + " col-sm-" + (12 / summaryCount) + " col-xs"  + (12 / summaryCount);
		$("#configSummaryDiv").find(".profile-summary-div").last().find(".chart-summary").each(function() {
			$(this).removeClass();
			$(this).find("[type]").hide();
			$(this).find("[type='"+$(this).find("[name='configSummary']").data("charttype")+"']").show();
			$(this).addClass(summaryClass + " text-center chart-summary");
			$(this).find("[name='removeSummary']").unbind('click').bind('click',function(){
				if($(this).parents(".profile-summary-div").find("section").length === 1){
					$(this).parents(".profile-summary-div").remove();
				}else{
					$(this).parent().remove();
				}					
				redrawSummary();
			});
			$(this).find("[name='configSummary']").unbind('click').bind('click',function(){
				axisLabelClick(this);
			});
		});
	}
	
	$("#addContainer")
			.unbind("click")
			.bind(
					"click",
					function() {
						var callBackWidget = function(response) {
							$("#droppable > article").append(response);
							if (!$("#droppable > article").is(':empty')) {
								$('[id^=widget-grid]').jarvisWidgets('destroy');
							}
							pageSetUp();
							$(".chart-div")
									.droppable(
											{
												activeClass : "ui-state-default",
												hoverClass : "ui-state-hover",
												accept : ":not(.btn-fact,.chart-parent-div,.dd-item.ui-sortable-handle)",
												drop : function(event, ui) {
													$(this).find(".dimension-placeholder").remove();
													if ($(this).is(":empty")) {
														if (!ui.draggable.hasClass('chart-div'))
															chartDrop(this, ui);
														else {
															$(this).html(ui.draggable.html());
															ui.draggable.find(".axisHeader")
																	.removeData("bs.popover");
															$(this).find(".axisHeader").data(ui.draggable.find(".axisHeader").data());
															// $("[rel=popover]").popover();
															$(".axisHeader",$(this)).bind("click",function() {axisLabelClick(this);});
															$(".chart-remove", $(this)).unbind("click").bind("click", function() {removeChart(this);});
															ui.draggable.empty().append($("<span/>").addClass("dimension-placeholder"));
														}
													} else {
														var tempHTML = $(this).html();
														$(this).find(".axisHeader").removeData("bs.popover");
														var tempData = $(this).find(".axisHeader").data();
														$(this).empty();
														$(this).html(ui.draggable.html());
														ui.draggable.find(".axisHeader").removeData("bs.popover");
														$(this).find(".axisHeader").data(ui.draggable.find(".axisHeader").data());
														ui.draggable.empty().html(tempHTML);
														ui.draggable.find(".axisHeader").data(tempData);
														// $("[rel=popover]").popover();
														$(".axisHeader",$(this)).bind("click",function() {axisLabelClick(this);});
														$(".axisHeader",ui.draggable).bind("click",function() {axisLabelClick(this);});
														$(".chart-remove", $(this)).unbind("click").bind("click", function() {removeChart(this);});
														$(".chart-remove", ui.draggable).unbind("click").bind("click", function() {removeChart(this);});
													}
												}
											});
							/*
							 * $(".chart-div").draggable({ start :
							 * function(event, ui) { $(".dragRemove").show(); },
							 * stop : function(event, ui) {
							 * $(".dragRemove").hide(); }, appendTo : "body",
							 * helper : "clone" });
							 */
						};
						templates.getWidget(null, callBackWidget);
					});
	$("#frmConf").validate({
		ignore: [],
		rules : {
			database : {
				required : true
			},
			table : {
				required : true
			}
		},

		// Messages for form validation
		messages : {
			database : {
				required : 'Please select database'
			},
			table : {
				required : 'Please select table'
			}
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element);
		}
	});
	$("#frmConfDataSource").validate({
		ignore: [],
		rules : {
			queryDataSource : {
				required : true
			}
		},

		// Messages for form validation
		messages : {
			queryDataSource : {
				required : 'Please select data source'
			}
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element);
		}
	});
	var previewProfile = function() {
		enableLoading();
		var config = getConfig();
		var filters = getFilters();
		var summary = getSummary();
		showFullScreen();
		drawProfile(config, $("#widget-fullscreen"));
		drawSummary(summary, "#widget-fullscreen");
		THIS.additionalconfig = THIS.additionalconfig ? THIS.additionalconfig :{};
		if ($("[name=profilegroupby").is(":checked")) {
			THIS.additionalconfig.profileGroupBy = true;
		}else{
			THIS.additionalconfig.profileGroupBy = false;
		}
		if ($("#filterNotapplicable").is(":checked")) {
		    THIS.additionalconfig.isFilterNotapplicable = true;
	    }else{
	    	THIS.additionalconfig.isFilterNotapplicable = false;
	    }
		var profilePalette = $("#profileColorPalette").attr("name");
		if (profilePalette) { 
			THIS.additionalconfig.profilePalette = profilePalette;
			var colors;
			if (THIS.additionalconfig.profilePalette && THIS.additionalconfig.profilePalette.indexOf("rgbColor") === -1 && THIS.additionalconfig.profilePalette.indexOf("linearRgbColor") === -1){
				var scheme = palette.listSchemes(THIS.additionalconfig.profilePalette)[0];
				colors = scheme.apply(scheme, [8]);
				
				
			} else{
				colors = palette.generate(new Function("x","return "+ THIS.additionalconfig.profilePalette), 8);
			}
			var decimalCode = [];
			$("#profileColorPalette").empty();
			colors.forEach(function(colorCode){
				$("#profileColorPalette").append($("<span/>").css("background","#"+colorCode));
				decimalCode.push( "#"+colorCode);
			});
			
			
			dc.config.defaultColors = function() {
				return decimalCode;//d3.scaleOrdinal().range(decimalCode);
			};
		}
		$('[id^=widget-grid]').jarvisWidgets('destroy');
		pageSetUp();
		$("#widget-fullscreen").find(".jarviswidget-fullscreen-btn").trigger(
				'click');
		$("#widget-fullscreen").find(".jarviswidget-fullscreen-btn").unbind(
				"click").bind("click", function() {
			$("#jarviswidget-fullscreen-mode").remove();
			$('.nooverflow').removeClass('nooverflow');
		});
		
		callService(config, filters, summary);
	};
	this.dynamicProfiling = function(data,filterActions) {
		templates.getWidget(null, function() {
			templates.getPie(null, function() {
				templates.getDatatable(null, function() {
					templates.getSummaryConfig(null, function() {
						templates.getSummary(null, function() {
							templates.getCalendar(null, function() {
								templates.getImageWidget(null, function() {
									templates.getNGram(null, function() {
										templates.getIFrame(null, function() {
											templates.getHTMLTemplate(null, function() {
												if(localStorage["isMultipleProfile"])
													delete localStorage["isMultipleProfile"];
												var result = dynamicprofile(data,filterActions);
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	};
	
	this.dynamicDashboardProfiles = function(data,profileActions,appliedfilters,mappedFilters,viewSettings ) { 
		templates.getWidget(null, function() {
			templates.getPie(null, function() {
				templates.getDatatable(null, function() {
					templates.getSummaryConfig(null, function() {
						templates.getSummary(null, function() {
							templates.getCalendar(null, function() {
								templates.getImageWidget(null, function() {
									templates.getNGram(null, function() {
										templates.getIFrame(null, function() {
											templates.getHTMLTemplate(null, function() {
												if(data && data.length>0){
													var requests = [],methodNames = [],profileIds = [],allConfigs ={};
													for(var i=0;i<data.length;i++){
														if(data[i].type === "profile"){
															THIS.viewSettings = viewSettings ?viewSettings:'';
															var profileType='dashboard';
															dynamicProfiling(data[i],'',profileType);
															if (data[i].additional_config) {
																THIS.additionalconfig = data[i].additional_config;
																if(THIS.additionalconfig && THIS.additionalconfig.dataQuery){
																	THIS.additionalconfig.dataQuery = THIS.additionalconfig.dataQuery.replace(/@/g, "'");
																	THIS.dataQuery = THIS.additionalconfig.dataQuery;
																}else{
																	THIS.dataQuery = null;
																}
															}
															
															var config = Jsonparse(data[i].config.replace(/@/g, "'"),"CONFIG");
															var configFilters = $.extend(true,[], appliedfilters);
															if(THIS.viewSettings && THIS.viewSettings.length>0){
																for(var vi=0;vi<THIS.viewSettings.length;vi++){
																	if(THIS.viewSettings[vi].profileid !== data[i].id){
																		configFilters.push({
																			fieldName:THIS.viewSettings[vi].fieldname,
																			defaultvalue:THIS.viewSettings[vi].filterValues,
																			fieldType:"lookupdropdown",
																			searchtype:"in"
																		});
																	}
																}
															}
															if(configFilters.length >0 && mappedFilters !== undefined ){
																for(var k=0;k<mappedFilters.length;k++){
																	for(var j=0;j<configFilters.length;j++){
													                       if(mappedFilters[k].fieldname === configFilters[j].fieldName){
													                       var mappedcol =mappedFilters[k].mappedProfiles[data[i].id]
													                       configFilters[j].fieldName = mappedFilters[k].mappedProfiles[data[i].id];
													                       if (localStorage["filtersForwaded"]) {
															                        var filtersForwadedObj =  Jsonparse(localStorage["filtersForwaded"]);
															                        if (filtersForwadedObj[mappedcol]) { 
															                         
															                         if(mappedFilters[k].fieldname === profileAnalyser.filters[j].fieldName){
															                          /*
																						 * if(profileAnalyser.filters[j].defaultvalue.length
																						 * >0){
																						 * profileAnalyser.filters[j].defaultvalue.forEach(function(defaultvalue){});
																						 * }else
																						 */
															                        if(profileAnalyser.filters[j].fieldType ==='text'){
															                           profileAnalyser.filters[j].defaultvalue=filtersForwadedObj[mappedcol];
															                           configFilters[j].defaultvalue =filtersForwadedObj[mappedcol];
															                          }else{
															                           profileAnalyser.filters[j].defaultvalue =[];
															                           configFilters[j].defaultvalue =[];
															                           if(profileAnalyser.filters[j].fieldType === "lookup"){
															                            var obj ={};
															                            obj.id = filtersForwadedObj[mappedcol];
															                            // filtersForwadedObj[mappedcol]
																						// =
																						// obj;
															                            if(profileAnalyser.filters[j].isSingle){
															                        		   profileAnalyser.filters[j].defaultvalue = obj;
															                        		   configFilters[j].defaultvalue = obj;
															                        	   }else{
															                        		   profileAnalyser.filters[j].defaultvalue.push(obj);
																                        	   configFilters[j].defaultvalue.push(obj);
															                        	   }
															                           }else if(profileAnalyser.filters[j].fieldType === "lookupdropdown"){
															                        	   if(profileAnalyser.filters[j].isSingle){
															                        		   profileAnalyser.filters[j].defaultvalue = filtersForwadedObj[mappedcol];
															                        		   configFilters[j].defaultvalue = filtersForwadedObj[mappedcol];
															                        	   }else{
															                        		   profileAnalyser.filters[j].defaultvalue.push(filtersForwadedObj[mappedcol]);
																                        	   configFilters[j].defaultvalue.push(filtersForwadedObj[mappedcol]);
															                        	   }
															                           }else{
															                        	   profileAnalyser.filters[j].defaultvalue.push(filtersForwadedObj[mappedcol]);
															                        	   configFilters[j].defaultvalue.push(filtersForwadedObj[mappedcol]);
															                           }
															                         }
															                        }
															                     } 
													                       }
													                        }
																	}
																}
															}
															for(var fi=0;fi<configFilters.length;fi++){
																if(configFilters[fi].fieldName ===""){
																	configFilters.splice(fi,1);
																	fi--;
																	
																}
															}
															var filterConfig =configFilters;
															var summary = Jsonparse(data[i].summary.replace(/@/g, "'"),"SUMMARY");
															var request = callService(config,filterConfig,summary);
															if(request && ($.trim(request.columns) || request.query)){
																methodNames.push("getData");
																profileIds.push(data[i].id);
																allConfigs[data[i].id] = {
																		config:config,
																		summary:summary,
																		query:request.query,
																		filters:filterConfig,
																		fieldAliasMapping:request.fieldAliasMapping,
																		customFacts : Jsonparse(data[i].customfacts.replace(/@/g, "'"),"custom facts"),
																		facts : THIS.facts,
																		customFields : THIS.customFields
																};
																request.source = data[i].source;
																request.database = data[i].database;
																if(data[i].configtable !== "null")
																request.table = data[i].configtable;
																request.id = data[i].id;
																var grp = Jsonparse(data[i].additional_config).profileGroupBy !== undefined ?Jsonparse(data[i].additional_config).profileGroupB :data[i].additional_config.profileGroupBy;
																request.groupByEnabled =grp !== undefined ?  grp:false;
																requests.push(request);
															}
															
														}
													}
													THIS.profileActions = profileActions;
												}
												$('.profile-settings a.views').on('click', function () {
													$(this).parent().toggleClass("open");
												});
												$('body').on('click', function (e) {
												    if (!$('.profile-settings').is(e.target) 
												        && $('.profile-settings').has(e.target).length === 0 
												        && $('.open').has(e.target).length === 0
												    ) {
												        $('.profile-settings').removeClass('open');
												    }
												});
												delete localStorage.filtersForwaded;
												topMeasureForDashboard(data,profileActions,appliedfilters,mappedFilters,viewSettings);
												callDashBoardService(requests,"getMultipleData",profileIds,allConfigs);
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	};
	this.setViewConfigInWidget = function(widget){
		try{
			if(profileAnalyser && profileAnalyser.profileView && profileAnalyser.profileView.length>0){
				var profileView = profileAnalyser.profileView.filter(function(obj){return (obj.isDefault && obj.createdBy === System.userName)});
				if(profileView && profileView.length>0){
					var viewConfig = Jsonparse(profileView[0].viewConfig);
					if(viewConfig.widget){
						for (var widgetId in viewConfig.widget) {
							if(viewConfig.widget[widgetId].hidden){
								if(widgetId.indexOf("datatable")>-1 && widgetId === widget){
									$("#"+widgetId).find(".jarviswidget-toggle-btn").find("i").addClass("fa-plus").removeClass("fa-minus");
							    	$("#"+widgetId+" > div").addClass("hide");
								}else if(widgetId.indexOf("datatable")=== -1 && !widget){
									$("#"+widgetId).find(".jarviswidget-toggle-btn").find("i").addClass("fa-plus").removeClass("fa-minus");
							    	$("#"+widgetId+" > div").addClass("hide");
								}
						    }
						}
					}
				}
			}
		}catch (e){
			
		}
	};
	var callDashBoardService = function(requests,methodNames,profileIds,allConfigs,noConstrcutFilter){
		var request ={
				"requests":JSON.stringify(requests)
		}
		enableLoading();
		callAjaxService("getMultipleData",function(response){callBackDashBoardService(response,noConstrcutFilter)},callBackFailure,request,"POST",null,true);
		function callBackDashBoardService(response,noConstrcutFilter){
			disableLoading();
			if(response ){
				if (response && response.isException) {
					showNotification("error", response.customMessage);
					return;
				} else {
					
					if (response) {
						$("#widget-grid > div >div:not(.search-div)").show();
						$(".jarviswidget> div").removeClass("hide");
						$(".jarviswidget-toggle-btn").find("i").addClass("fa-minus").removeClass("fa-plus");
						$("#results").hide();
						var keys=Object.keys(response);
						for(var i=0;i<keys.length;i++){
							if(allConfigs && allConfigs[keys[i]]){
								THIS.config = allConfigs[keys[i]].config;
								THIS.Summary = allConfigs[keys[i]].summary;
								THIS.dataQuery = allConfigs[keys[i]].query;
								THIS.fieldAliasMapping = allConfigs[keys[i]].fieldAliasMapping;
								THIS.customFacts = allConfigs[keys[i]].customFacts;
								THIS.facts = allConfigs[keys[i]].facts;
								THIS.customFields = allConfigs[keys[i]].customFields;
								THIS.filters = allConfigs[keys[i]].filters;
								THIS.profileId = keys[i];
							}
							if(Jsonparse(response[keys[i]]) <=0){
								$("#widget-grid > div >div:not(.search-div)").hide();
									$("#results").show().html('<div class="alert alert-danger fade in"><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No matching results found for selected filters.</p></div>');
									if(profileAnalyser.filters && profileAnalyser.filters.length >0){
								    	 constructFilters(profileAnalyser.filters);
								    	 break;
									}
							}else{
								callBackGetData(Jsonparse(response[keys[i]]));
								pageSetUp();
							}
						}
						setUnreadCount(THIS.filters, THIS.profileId);
					} 
					if(!noConstrcutFilter){
						$("#filterModal").css({"display": "block"});
						constructFilters(profileAnalyser.filters);
						$("#filterModal").css({"display": ""});
					}
					if(THIS.additionalconfig.isFilterHide){
	    				$('#divSankeyFilter').parents(".jarviswidget-sortable").hide();
	    				$('#srch').hide();
	    			}
					setTimeout(function(){
						dataAnalyser.setViewConfigInWidget();
					}, 1000);
				}
				
			}
		}
		
		
	};
	
	var htmlToPdf = function(html,filename,cssStyle,pdfHeight){
		var filterString = '';
		var loopCount = 0;
		var str = '<table class="table" name="filterTable" style=" border: none;">';
		var tableCol = '';
		var filterCount= 0;
		var colCount = 0;
		var finalStr = '';
		var tables = []
		
		for(var i=0; i<$('#divSankeyFilter').find('section').length;i++){
			var filterValues ='';
			var filterId =$('#divSankeyFilter').find('#f'+(i+1));
			if(filterId.find('button').attr('data-toggle') ==='dropdown'){
				if(filterId.find('[name="dropdowndiv"]').find('select').val()!=null){
					filterValues ='<b>' +filterId.find('strong').text() +'</b>' +' '+ filterId.find('[name="dropdowndiv"]').find('button').attr('title') ;
				}
			}else if(filterId.find('.select2-container').length >0){
				if(filterId.find('[name="dropdowndiv"]').select2('data').text().trim()!="" && filterId.find('[name="dropdowndiv"]').select2('data').text().trim()!="No matches found")
				filterValues = '<b>' +filterId.find('strong').text() +'</b>' +' '+ filterId.find('[name="dropdowndiv"]').select2('data').text();
			}else if(filterId.find('[name="from-defaultvalue"]').length !=0 && filterId.find('[name="to-defaultvalue"]').length !=0){
				if(filterId.find('[name="from-defaultvalue"]').val() !="" && filterId.find('[name="to-defaultvalue"]').val()!="")
				filterValues = '<b>' +filterId.find('strong').text() +'</b>' +' '+ filterId.find('[name="from-defaultvalue"]').val() +' to '+ filterId.find('[name="to-defaultvalue"]').val();
			}else{
				if( filterId.find('input').val() !="")
				filterValues = '<b>' +filterId.find('strong').text() +'</b>' +' '+ filterId.find('input').val();
			}
			if(filterValues !=""){
				filterCount++;
				 colCount ++;
					tableCol += '<td style="font-size:15px">' +filterValues+  '</td>';
			}
			if(colCount > 1){
					 colCount =0;
					 str += '<tr class="trClass">' +tableCol;	
					 str += '</tr>';
				     tableCol ='';	
			}
		}
		
		if(filterCount %2 !=0){
			 str += '<tr class="trClass">' +tableCol;	
			 str += '</tr>';
		}
		str += '</table>';
		finalStr = '<div style="margin-top:10px ;font-size:15px"><div style="padding-bottom:6px"><b><i> Filters Applied:</i></b></div> ' +str+'</div>';
				
		var request ={
				"html": $(str).find('tr').hasClass('trClass')? finalStr : '' + html,
				"css": cssStyle,
				"fileName":filename,
				"height":pdfHeight
			  };
		
		var downloadURL = 'rest/htmToPdf/getconvertedpdf';
		$.fileDownload(downloadURL,{
	        httpMethod: "POST",
	        data: request,
			successCallback: function() {
				showNotification("success","Pdf downloaded successfully.");
			
            },
            failCallback: function(responseHtml) {
            	showNotification("failure","Pdf downloaded failed.");  
            }
	});	
		
		
	};
	
	
	
	var deletePdf=function(fileName){
	
		var valu = {"fileName": fileName};
		
		$.ajax({  
		       type: "DELETE",
		       data:valu,
		       url: "rest/htmToPdf/deleteFile",   
		       success: function(resp){  
		       },  
		       error: function(e){  
		         
		       }  
		     });	
	};
	
		
	var dynamicprofile	= function(data,filterActions){
		var profilefilters = Jsonparse(data["filters"].replace(/@/g, "'"),"FILTER");
		var requests = [];
		var filterId =[];
		var count=0;
		var allreq ;
		for(var p=0;p<profilefilters.length ;p++){
			if(profilefilters[p].defaultquery){
				count ++;
				var dt ='';
				(function(index){
					allreq = $.ajax( {
					url : serviceCallMethodNames["suggestionsLookUp"].url,
					dataType : 'JSON',
					type : 'GET',
					cache: false,
					contentType: "application/json; charset=utf-8",
					data: {
						 source : data.source,// THIS.sourceType,
							database:data.database,// THIS.database,
							lookupQuery : profilefilters[p].defaultquery
		              
		            },success: function(res){
				        var defaultarr=[];
				        defaultarr.push(res[0].id);
				        profilefilters[index].defaultvalue = defaultarr;
				        console.log('inside success');
				       // filterCallBack(tempFilterObj,filters[index],data);
				    },error: function(xhr) {
					    // Do Something to handle error
						console.log('Error Occured')
					  }
				}
		       );})(p)// ajax call end
		       requests.push(allreq);
			    filterId.push(profilefilters[p].id);
			}
		}
		if(count === 0){
			dynamicProfiling(data,filterActions);	
		}else{
			$.when.apply(null, requests,profilefilters).done(function() {
			console.log('All Done'+profilefilters)
		    // Each argument is an array with the following structure: [ data,
			// statusText, jqXHR ]
			// var responseArgsArray =arguments[0];
			// //Array.prototype.slice.call(this, arguments);
			dynamicProfiling(data,filterActions,null,profilefilters);
		});
		
		}
		
	}
	
	
	
	var dynamicProfiling = function(data,filterActions,profileType,Defaultfilters) {
		filtersVal = {}
		finalFilterValue = []
		if(profileAnalyser && profileAnalyser.filters){
			profileAnalyser.filters.forEach(function(filter){
				if(filter.fieldType === "dynamicDate" || filter.fieldType === "DATE" || filter.fieldType === "dateRange"){
					filtersVal['fieldName'] = filter['fieldName']
					if(filter.fieldType === "dateRange"){
						filtersVal['defaultvalue'] = filter["defaultvalue"]
					}
					else{
						filtersVal['from-defaultvalue'] = filter["from-defaultvalue"]
						filtersVal['to-defaultValue'] = filter["to-defaultvalue"]
					}
					finalFilterValue.push(filtersVal)
					filtersVal = {};
				}
			})
			$('#clearFilter').data("defaultVal", finalFilterValue)
		}
		enableLoading();
		$('#dataAnalyserProfile').show();
		var config = Jsonparse(data["config"].replace(/@/g, "'"),"CONFIG");
		if(data.dataQuery){
			var dataQueryObj = Jsonparse(data.dataQuery,"DATA QUERY");
			THIS.dataQuery = dataQueryObj.query.replace(/@/g, "'");
			THIS.dataQuery = THIS.dataQuery.replace(/(\r\n|\n|\r)/gm," ");
		}
		THIS.config = config;
		THIS.profileId = data["id"];
		THIS.sourceType = data["source"];
		THIS.database = data["database"];
		THIS.table = data["configtable"];
		if(data.additional_config){
			if(typeof data.additional_config === "object"){
				THIS.additionalconfig = data["additional_config"];
			}
			else{
				THIS.additionalconfig = Jsonparse(data["additional_config"].replace(/@/g, "'"),"ADDITIONAL CONFIG");
			}
		}
		if(profileType !='dashboard'){			
			if (localStorage["viewSetting"] && localStorage["viewSetting"] !== null && localStorage["viewSetting"] !== "null"){
				THIS.viewSettings = Jsonparse(localStorage["viewSetting"],"VIEW SETTING");
				localStorage["viewSetting"] = null;
			}
			else if (data["view_setting"] && data["view_setting"] !== null){
				THIS.viewSettings = Jsonparse(data["view_setting"],"VIEW SETTING");
			}
		}
		THIS.profileActions = filterActions;
		if (THIS.profileActions && THIS.profileActions !== ""){
			for (var i=0; i< THIS.profileActions.length;i++ ){
				THIS.actionData[THIS.profileActions[i].name] = THIS.profileActions[i].name;
			};
		}
		THIS.facts = [];
		THIS.factsConfig = [];
		try {
			THIS.factsConfig = Jsonparse(data["fact"],"FACT CONFIG");
			for(var i=0; i<THIS.factsConfig.length; i++){
				THIS.facts.push(THIS.factsConfig[i].fact);
			}
		}catch (e){
			THIS.factsConfig = [];
			THIS.facts = data["fact"].split(",");
			for(var i=0; i<THIS.facts.length; i++){
				var factObj = {};
				factObj.fact = THIS.facts[i];
				factObj.displayname = THIS.facts[i];
				THIS.factsConfig.push(factObj);
			}
		}
		THIS.ProfileName = data["displayname"];
		if (data["summary"] && $.trim(data["summary"]) !== "")
			THIS.Summary = Jsonparse(data["summary"].replace(/@/g, "'"),"SUMMARY");
		if(data.customfacts && data.customfacts !== undefined){
			THIS.customFacts = Jsonparse(data.customfacts.replace(/@/g, "'"),"CUSTOM FACT");
			if(THIS.customFacts && THIS.customFacts.length>0){	
				THIS.customFacts.forEach(function(customfact){
					THIS.dependentFacts.push.apply(THIS.dependentFacts,customfact.dependentfacts);
				});
			}
		}
		$("#wid-id-formView").addClass("hide");
		$("#dataAnalyserProfile").removeClass("hide");
		
		$("#wid-id-profiling > header > h2").html(THIS.ProfileName);
		drawProfile(config, $("#wid-id-profiling"),'',profileType);
		drawSummary(THIS.Summary, "#wid-id-profiling",THIS.profileId);		
		if (THIS.additionalconfig && THIS.additionalconfig.profilePalette) {
			var colors;
			if (THIS.additionalconfig.profilePalette.indexOf("rgbColor") === -1 && THIS.additionalconfig.profilePalette.indexOf("linearRgbColor") === -1){
				var scheme = palette.listSchemes(THIS.additionalconfig.profilePalette)[0];
				colors = scheme.apply(scheme, [8]);
			} else{
				colors = palette.generate(new Function("x","return "+ THIS.additionalconfig.profilePalette), 8);
			}
			var decimalCode = [];
			colors.forEach(function(colorCode){
				decimalCode.push( "#"+colorCode);
			});
			d3.scaleOrdinal(decimalCode);
			/*
			 * d3.scale.category10 = function() { 
			 * return
			 * d3.scaleOrdinal().range(decimalCode); };
			 */
			dc.config.defaultColors = function() {
				return decimalCode;//d3.scaleOrdinal().range(decimalCode);
			};
		}
		
		if (THIS.additionalconfig && THIS.additionalconfig.measureDisplayName && THIS.additionalconfig.measureDisplayName !== "")
			$("#measureDisplayNameDiv").html(THIS.additionalconfig.measureDisplayName);
		if(profileType !='dashboard'){
			var cssStyle= "";
			$("link").each(function(){if ($(this).attr("href")){$.get($(this).attr("href"))
	            .done(function(response) {
	              // 
	                cssStyle += response;
	            });}});
		if (THIS.additionalconfig && THIS.additionalconfig.pdfDownload){
			
			$(".profile-settings").append($("<a/>").prepend($("<i/>").addClass("fa fa-file-pdf-o")).attr("title","Print View").unbind('click').bind
					('click',{ "config": THIS.config }, function(e) {

						$('.dataTables_wrapper').find('.dataTables_length select').find('option[value=500]').attr('selected', 'selected');
							$('.dataTables_wrapper').find('.dataTables_length select').trigger('change');

								var renderingDataTablesForPrint = function(){								
								var divContent = $('<div/>').attr({'id': 'modal-div-content'}).css({"z-index": "2147483647"});								
								$("#widget-grid").each(function(index, _val1) {
									$(_val1).find('.chart-parent-div').each(function(index, _val2) {
										var _this = _val2;
										if(!$(_this).hasClass('search-div')){
											var table = '';
											var tableName = $(_this).find('header h2').text();
											var tableHead = $(_this).find('.dataTables_wrapper .dataTables_scrollHead table thead tr[role="row"]');
											var tableBody = $(_this).find('.dataTables_wrapper .dataTables_scrollBody table tbody');
											
											if(!$(_this).find('.widget-body .row div').hasClass('dataTables_wrapper')){
												var str = $('<div/>').addClass($(_val2).attr('class'))
															.append($('<p/>').css({"font-size": "16px", "font-weight": "bolder"}).text(tableName));
												table = str.append($(_this).find('.widget-body').clone());
											} else {
												table = $('<table/>').addClass('table table-bordered').append('<caption>');
												table.find('caption').addClass('text-left').html('<p style="font-size: 16px; font-weight: bolder;">'+tableName+'</p>');
												var strHead = '<tr style="background: #585858; color: #f2f2f2;">';
												tableHead.find('th').each(function(index, _val3) {
													var _THISHEAD = _val3;
													if(index < 12)
														strHead += '<th style="max-width: 140px; word-wrap:break-word">'+$(_THISHEAD).text()+'</th>';
												});
												strHead += '</tr>';
												
												var strBody = '';
												tableBody.find('tr').each(function(index, _val4) {
													var _THISBODY = _val4;
													strBody += '<tr>';
													$(_THISBODY).find('td').each(function(index, _val5) {
														var _THISTD = _val5;
														if(index < 12)
															strBody += '<td style="max-width: 140px; word-wrap:break-word">'+$(_THISTD).text()+'</td>';
													});
													strBody += '</tr>';
												});
												table.append($('<thead/>').append(strHead)).append($('<tbody/>').append(strBody));
											}
											divContent.append(table);
										}
									});
								});
								
								$("#myModalLabel", "#myModalDrilldown").html(""); 
								$("#myModalDrilldown .modal-header").hide();
								$("#myModalDrilldown").css({"overflow": "auto"});
								$("#myModalDrilldown .modal-dialog").css({"width": "auto", "max-width": "1200px"});
								$("#printView").empty();
								$("#printView").append('<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 20px; top: 12px;"><span aria-hidden="true">x</span> </button>');
								$("#printView").css({"padding": "20px"}).append($('<h2/>').addClass('text-center').text($(".breadcrumb li").last().text().split("-")[0])
										.css({"margin": "0px", "font-weight": "bold", "text-decoration": "underline"}))
									.append($('<div/>').addClass('pull-right')
									.append($('<button/>').addClass('btn btn-sm btn-primary').text('Print').css({"margin-right": "10px"})
									.bind('click', function(e){
									    printElement(document.getElementById("modal-div-content"));
									}))
									.append($('<button/>').addClass('btn btn-sm btn-primary').text('Download')
									.bind('click', function(e){
										htmlToPdf(divContent.html(),$('.breadcrumb li').last().text().split('-')[0].trim(),cssStyle,Math.floor(document.body.scrollHeight * 0.264583 ).toString());
									})));
								$("#printView").append(divContent);
								$("#myModalDrilldown").modal("show");
							}

								
							function printTimeOut(){
								setTimeout(function(){
									var dataTablesProcessingDone = true;
									$('.dataTables_processing').each(function(e){
										if($(this).css('Display')!= 'none'){
											dataTablesProcessingDone = false;
										}
									});
									if(dataTablesProcessingDone){
										renderingDataTablesForPrint();
									}else{
										printTimeOut();
									}
								}, 4000);
							};
							printTimeOut();
							
							function printElement(elem) {
							    var domClone = elem.cloneNode(true);
							    var $printSection = document.getElementById("printSection");
							    $("#left-panel").hide();
							    if (!$printSection) {
							        var $printSection = document.createElement("div");
							        $printSection.id = "printSection";
							        document.body.appendChild($printSection);
							    }
							    $printSection.innerHTML = "";
							    $printSection.appendChild(domClone);
							    $("#widget-grid svg").hide();
							    $("#printView").hide();
							    window.print();
							    $("#widget-grid svg").show();
							    $("#printView").show();
							    $("#left-panel").show();
							}
						}));
		}
		
		$("#actionDiv").find(".profile-action-ul").remove();
		if($(".profile-settings").find('#actionHolder').length >0){
			$(".profile-settings").find('#actionHolder').after($("<a/>").addClass("dc-data-count").prepend($("<i/>").addClass("fa fa-undo")).attr("title","Reset View").unbind("click").bind("click", function() {
				$('input[name=checkboxinline],[name=checkboxselectall]').prop('checked', false);
			}));
		}else{
			$(".profile-settings").prepend($("<a/>").addClass("dc-data-count").prepend($("<i/>").addClass("fa fa-undo")).attr("title","Reset View").unbind("click").bind("click", function() {
				$('input[name=checkboxinline],[name=checkboxselectall]').prop('checked', false);
			}));	
		}
	}
		if(!localStorage["isMultipleProfile"]){
		if (data["filters"] && $.trim(data["filters"]) !== "" && data["filters"].length>0 && data["filters"]!=="[]" ) {
			var arr =[],viewfilters;
			if(data["viewfilters"] != undefined){
				viewfilters= Jsonparse(data["viewfilters"].replace(/@/g, "'"),"FILTER");
			}
			var profilefilters = Defaultfilters ? Defaultfilters: Jsonparse(data["filters"].replace(/@/g, "'"),"FILTER");
			if(viewfilters && viewfilters.length >0){
				for(var i=0;i<profilefilters.length ;i++){
				    var countViewFilters=0;
					for(var j=0;j<viewfilters.length ;j++){
						if(profilefilters[i].fieldName === viewfilters[j].fieldName){
							viewfilters[j].lookupquery = profilefilters[i].lookupquery;
							viewfilters[j].displayName = profilefilters[i].displayName;
							arr.push(viewfilters[j]);
							countViewFilters++
						}
					}
					if(countViewFilters === 0){
						arr.push(profilefilters[i]);
					}
				}
				THIS.filters = arr;
			}else{
				THIS.filters = profilefilters;
			}
			var invisibleFilterLength = THIS.filters.filter(function(a){return a.invisible;}).length;
			if(THIS.filters.length !== invisibleFilterLength && !localStorage["isMultipleProfile"]){
				filterHTML();
			}
		}
		
		if(THIS.additionalconfig){
				
				if(THIS.additionalconfig.isMinified && THIS.additionalconfig.isMinified === true){
					$("body").addClass('minified');
				}
				if(THIS.additionalconfig.hideemail && THIS.additionalconfig.hideemail === true){
					$("#actionDiv").find("[name='emailNotification']").hide();
				}
				if(THIS.additionalconfig.hideView && THIS.additionalconfig.hideView === true){
					$("#actionDiv").find(".views").hide();
				}
			}
		$('.profile-settings a.views').on('click', function () {
			$(this).parent().toggleClass("open");
		});
		$('body').on('click', function (e) {
		    if (!$('.profile-settings').is(e.target) 
		        && $('.profile-settings').has(e.target).length === 0 
		        && $('.open').has(e.target).length === 0
		    ) {
		        $('.profile-settings').removeClass('open');
		    }
		});
	}
		

		if(!localStorage["isMultipleProfile"]){
			$('[id^=widget-grid]').jarvisWidgets('destroy');
			pageSetUp();
			if($("#widget-grid").find(".jarviswidget").find("header").find(".jarviswidget-ctrls").length> 1){
				$("#widget-grid").find(".jarviswidget").find("header").find(".jarviswidget-ctrls:eq(0)").remove()
				$("#widget-grid").find(".jarviswidget").find("header").find(".widget-toolbar").remove()
			}
			callService(config, THIS.filters, THIS.Summary);
			$("#settingsOption").unbind("click").bind("click",function (e) {
		        e.stopPropagation();
		    });
		}
		
	};
	
	this.filterHTML = function(additionalconfig){
		THIS.additionalconfig = additionalconfig;
		filterHTML();
	}
	var filterHTML = function(){ 
        if (THIS.additionalconfig && THIS.additionalconfig.isFilterInline) {
        	$("#content").css({'padding-top':'0px'});
        	$("#content").prepend("<div id='srch'><strong style='margin: 15px;font-size: 14px;color: steelblue !important'>Search Criteria:</strong></div>")
			if($(".profile-settings").find('#actionHolder').length >0){
				$(".profile-settings").find('#actionHolder').after($("<a/>").addClass("refineSearch").prepend("<i class='fa fa-filter' style='font-size:15px !important'></i> Refine Search").attr("title","Show/Hide Filters").unbind('click').bind('click', function() {
					$('#widget-inline-filter').toggle();
					$('#srch').toggle();
				}));
			}else{
				$(".profile-settings").prepend($("<a/>").addClass("refineSearch").prepend("<i class='fa fa-filter' style='font-size:15px !important; color:#3E90D4'></i> Refine Search").attr({"title":"Show/Hide Filters"}).unbind('click').bind('click', function() {
					$('#widget-inline-filter').toggle();
					$('#srch').toggle();
					var isfilterloaded = $(this).attr("isfilterloaded");
					if(!isfilterloaded){
						constructFilters(THIS.filters,filterContentId);
						$(this).attr("isfilterloaded",true);
						}
				}));
			}
			$('#myModalthredlevel').modal('hide');
			$("#divInlineFilter").show();
			$("#filterProfile").hide();

			filterContentId = "divSankeyFilter";
			var tempoptions = {
					"width" : "col-md-12 col-xs-12 col-sm-12 col-lg-12 chart-parent-div search-div",
					"widget-class" : "jarviswidget  jarviswidget-sortable",
					"id" : "widget-inline-filter",
					"istemplate" : false,
					"fullscreen" : false,
					"icon" : "fa fa-filter",
					"header" : "Filter"
				};
			var tempWidget = '';
			if(templates.getWidget.template){
				tempWidget = templates.getWidget.template(tempoptions);
			}
			$($(".widget-body",$("#wid-id-profiling"))[0]).prepend(tempWidget); 
			$($("header","#widget-inline-filter")[0]).remove();
			$("#widget-inline-filter").find(".widget-body").append($("<div/>").attr("id","divSankeyFilter"));
			$("#divSankeyFilter").parent().append($("<button/>").addClass("btn btn-primary margin-10-0 pull-right")
					.attr({"name":"applyFilter","id":"applyFilter"}).append($("<i/>").addClass("fa fa-filter").html("&nbsp;")).append("Filter"))
					.append($("<button/>").addClass("btn btn-warning margin-10-0 pull-right marginRight10")
							.attr({"name":"clearFilter","id":"clearFilter"}).data("refineVal",false).append($("<i/>").addClass("fa fa-close").html("&nbsp;")).append("Clear"));
		} else{
			$(".profile-settings").prepend($("<a/>").addClass("refineSearch").prepend("<i class='fa fa-filter' style='font-size:15px !important; color:#3E90D4 '></i> Refine Search").attr("title","Filter").unbind('click').bind('click', function() {
				$('#filterModal').modal('show');
			}));
			filterContentId = "sankeyModalFilterContent";
		}
        THIS.filterContentId = filterContentId;
        if(THIS.filters && !localStorage["isMultipleProfile"]){

    		var invisibleFilterLength = THIS.filters.filter(function(a){return a.invisible;}).length;
        	if( THIS.filters.length !== invisibleFilterLength){
        		if(THIS.additionalconfig.isFilterHide){ 
        			$('#divSankeyFilter').parents(".jarviswidget-sortable").hide();
        		}	
        	}
    		
    	}
    }
	function calculateDynamicFilter(dataQuery,filters){
		var query = dataQuery;
		var dynamicFilters = query.match(/\[(.*?)\]/g)||[];
		$.each(dynamicFilters,function(){
			var compliedExpersion = this.toString();
			var validExp = true;
			var re = /(?:^|\W)#(\w+)(?!\w)/g, match;
			while (match = re.exec(this.toString())){
				 var tempVar = match[1];
				 if (tempVar.charAt(0) === "f" && filters && filters.length>0){
					 $.each(filters,function(){
						  if (this.id === tempVar){
							  firstEle = false;
							  if ((!this.defaultvalue || this.defaultvalue.length === 0) && compliedExpersion.indexOf("function()")=== -1)
							  {
								  validExp = false;
								  return;
							  }
							  if ((this.fieldType === "lookup") && this.isSingle === "true"){
								  if(this.isSingle)
									  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,(this.defaultvalue && this.defaultvalue.id)?this.defaultvalue.id:'');
								  else
									  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,this.defaultvalue?this.defaultvalue.map(function(o){return "'"+o.id+"'"}).toString():'');
							  }else if(this.fieldType === "select"){
								  if(!this.isSingle && this.defaultvalue && this.defaultvalue.length>0){
									  this.defaultvalue = this.defaultvalue.map(function(o){return (typeof o === 'object' && o.id)?o.id:o});
									  var index = this.defaultvalue.indexOf(" ");
									  if(index>-1)
										  this.defaultvalue.splice(index,1);
								  }else{
									  if(this.defaultvalue && typeof(this.defaultvalue) === "object" && this.defaultvalue.id){
										  this.defaultvalue = this.defaultvalue.id;
									  }
								  }
								  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,(this.isSingle && this.defaultvalue.trim())?this.defaultvalue:this.defaultvalue.map(function(o){return "'"+o+"'"}).toString());	  
							  }
							  else if(this.fieldType === "lookupdropdown"){
								  if(!this.isSingle){
									  var index = this.defaultvalue.indexOf(" ");
									  if(index>-1)
										  this.defaultvalue.splice(index,1);
								  }
								  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,(this.isSingle && this.defaultvalue.trim())?this.defaultvalue:this.defaultvalue.map(function(o){return "'"+o+"'"}).toString());
							  }
							  else if(this.fieldType === "TEXT")
								  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,this.defaultvalue);
							  else
								  validExp = false;
							  return;
						  };
					  });
				 }else if(tempVar.charAt(0) === "c" && dc.chartRegistry.list().length>0){
					 var chartFilters = dc.chartRegistry.list();
					 $.each(chartFilters,function(){
						  if (this.anchorName() === tempVar){
							  if (!this.filterValues || this.filterValues.length === 0)
							  {
								  validExp = false;
								  return;
							  }
							  if (firstEle)
								  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,this.filterValues.join("','"));
							  else
								  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,"'"+this.filterValues.join("','")+"'");
							  
							  firstEle = false;
							  return;
						  }
					  });
				 }else{
					 validExp = false;
				 }
			}
			if (validExp){
				if(compliedExpersion.indexOf("function()")>-1){
					try {
						query = query.replace(this,eval(compliedExpersion.replace("[","").replace("]","")));
				    } catch (e) {
				    	query = query.replace(this,' ');
				    }
					
				}else{
					query = query.replace(this,compliedExpersion.replace("[","").replaceAll("]",""));
				}
			}else{
				query = query.replace(this,' ');
			}
		});
		return query;
	}
	/**
	 * Will analysis required columns to fetch from config,summary & filter
	 * 
	 * Will Fetch required data from Database
	 */
	this.callServiceByFilter = function(data,dashboardFilters,noConstrcutFilter){
	if(data && data.length>0){
			var requests = [],methodNames = [],profileIds = [],allConfigs ={};
			for(var i=0;i<data.length;i++){
				THIS.profileId = data[i].id;
				var config = Jsonparse(data[i].config.replace(/@/g, "'"),"CONFIG");
				if (data[i].additional_config) {
					var additionalconfig = data[i].additional_config;
					if(additionalconfig && additionalconfig.dataQuery){
						additionalconfig.dataQuery = additionalconfig.dataQuery.replace(/@/g, "'");
						THIS.dataQuery = additionalconfig.dataQuery;
					}else{
						THIS.dataQuery = null;
					}
				}
				if(localStorage["isMultipleProfile"]){
					if(dashboardFilters && dashboardFilters.length >0){
						THIS.chartDefaultFilters = dashboardFilters;
					}else{
						THIS.chartDefaultFilters = [];
					}
					THIS.customFacts = Jsonparse(data[i].customfacts.replace(/@/g, "'"),"CUSTOM FACT");
					THIS.facts = [];
					THIS.factsConfig = [];
					try {
						THIS.factsConfig = Jsonparse(data[i].fact,"FACT CONFIG");
						for(var j=0; j<THIS.factsConfig.length; j++){
							THIS.facts.push(THIS.factsConfig[j].fact);
						}
					}catch (e){
						THIS.factsConfig = [];
						THIS.facts = data[i].fact.split(",");
						for(var j=0; j<THIS.facts.length; j++){
							var factObj = {};
							factObj.fact = THIS.facts[j];
							factObj.displayname = THIS.facts[j];
							THIS.factsConfig.push(factObj);
						}
					}
				}
				var filterConfig = data[i].filters !==undefined ?Jsonparse(data[i].filters.replace(/@/g, "'"),"FILTER"):"";
				var summary = Jsonparse(data[i].summary.replace(/@/g, "'"),"SUMMARY");
				var request = callService(config,filterConfig,summary);
				if(request && ($.trim(request.columns) || request.query)){
					methodNames.push("getData");
					profileIds.push(data[i].id);
					allConfigs[data[i].id] = {
							config:config,
							summary:summary,
							query:request.query,
							filters:filterConfig,
							fieldAliasMapping:request.fieldAliasMapping,
							customFacts : Jsonparse(data[i].customfacts.replace(/@/g, "'"),"custom facts"),
							facts : THIS.facts,
							customFields : THIS.customFields
					};
					request.source = data[i].source;
					request.database = data[i].database;
					request.table = data[i].configtable;
					request.id = data[i].id;
					var groupBy =Jsonparse(data[i].additional_config).profileGroupBy !==undefined ?Jsonparse(data[i].additional_config).profileGroupBy :data[i].additional_config.profileGroupBy;;
					request.groupByEnabled = groupBy ? groupBy : "false";
					requests.push(request);
				}
			}
		}

		callDashBoardService(requests,methodNames,profileIds,allConfigs,noConstrcutFilter);
	};
	var getColumnsFromConfig = function(config,profileId,facts,customFacts){
		
		var columns = "",columnObj ={},customFields = {};
		var fieldDescriptions = [];
		if (config && config.length > 0) {
			for ( var i = 0; i < config.length; i++) {
				for(k=0;k<config[i].charts.length;k++)
				{
					if(THIS.additionalconfig.isFilterNotapplicable ){
					   	config[i].charts[k]["isFilterNotapplicable"] = true;
						
					}
					else{
						config[i].charts[k]['isFilterNotapplicable'] = false;
					}
				}
				var tempCharts = config[i].charts;
				for ( var j = 0; j < tempCharts.length; j++) {
		
					if(localStorage["isMultipleProfile"] && tempCharts[j].alias !==undefined && tempCharts[j].alias.indexOf(THIS.profileId)=== -1){
						tempCharts[j].alias = tempCharts[j].alias+THIS.profileId;
					}else if(profileId && tempCharts[j].alias.indexOf(profileId)=== -1){
						tempCharts[j].alias = tempCharts[j].alias+profileId;
					}
					if (tempCharts[j].charttype !== "datatable" && tempCharts[j].charttype !== "imagegrid") {
						if(tempCharts[j].charttype !== "calendar"){
							$("#"+tempCharts[j].alias).find(".axisLabel").data("orderby",tempCharts[j].orderby);
						}
						if (tempCharts[j].fieldname
								&& tempCharts[j].fieldname !== null
								&& tempCharts[j].fieldname !== "") {
							if(THIS.dataQuery || profileId){
								if(!THIS.fieldAliasMapping)
									THIS.fieldAliasMapping = {};
								THIS.fieldAliasMapping[tempCharts[j].fieldname] = tempCharts[j].alias;
							}
							var customField;
							if(customFacts && customFacts.length>0){
								customField = customFacts.filter(function(obj){return obj.name === tempCharts[j].fieldname});
							}
							if(tempCharts[j].customtype || (customField && customField.length>0)){
								$("#"+tempCharts[j].alias).data("customtype",tempCharts[j].customtype);
								customFields[tempCharts[j].alias] = tempCharts[j].fieldname;
								var uniqueNames = [];
								var names = (tempCharts[j].formula ?tempCharts[j].formula.match(/[^[\]]+(?=])/g):customField[0].formula.match(/[^[\]]+(?=])/g));
								$.each(names, function(i, el){
								    if($.inArray(el, uniqueNames) === -1) {
								    	uniqueNames.push(addTildInColumnName(el));
								    }
								});
								if(columns === "")
									uniqueNames.join(",");
								else
									columns += ","+uniqueNames.join(",");
							}
							else{
								if (columns === "")
								columns = addTildInColumnName(tempCharts[j].fieldname) + " as "
										+ tempCharts[j].alias;
							else
								columns += "," + addTildInColumnName(tempCharts[j].fieldname)
										+ " as " + tempCharts[j].alias;
							}
							if (tempCharts[j].filter
									&& tempCharts[j].filter !== null
									&& $.trim(tempCharts[j].filter) !== "") {
								columns += "," + addTildInColumnName(tempCharts[j].filter);
							}
							if(tempCharts[j].colorfield){
								columns += "," + addTildInColumnName(tempCharts[j].colorfield);
							}
							if (tempCharts[j].description
									&& tempCharts[j].description !== null
									&& $.trim(tempCharts[j].description) !== "") {
								var tempFieldDescriptions = {
									"name" : addTildInColumnName(tempCharts[j].fieldname),
									"description" : addTildInColumnName(tempCharts[j].description)
								};
								fieldDescriptions.push(tempFieldDescriptions);
							}

							if ($.inArray(tempCharts[j].group, facts) === -1
									&& tempCharts[j].group !== ""
									&& tempCharts[j].group !== "fact"
									&& tempCharts[j].group !== "_count"
									&& tempCharts[j].group !== "drop measure here")
								facts.push(tempCharts[j].group);
							if (tempCharts[j].stack
									&& tempCharts[j].stack !== null
									&& $.trim(tempCharts[j].stack) !== ""){
									columns += "," + addTildInColumnName(tempCharts[j].stack);
								/*
								 * var stackMeasure =tempCharts[j].stack; var
								 * measureArray = stackMeasure.split(',');
								 * for(var k=0 ; k <measureArray.length ; k++){
								 * if(customFacts[0].name != measureArray[k] ){
								 * columns += "," + addTildInColumnName(
								 * measureArray[k]); } }
								 */
							}
							if (tempCharts[j].dynamicdimension
									&& tempCharts[j].dynamicdimension !== null
									&& $.trim(tempCharts[j].dynamicdimension) !== ""){
								columns += "," + addTildInColumnName(tempCharts[j].dynamicdimension);
							}
							if (tempCharts[j].calculatedfield
									&& tempCharts[j].calculatedfield !== null
									&& $.trim(tempCharts[j].calculatedfield) !== ""){
								columns += "," + addTildInColumnName(tempCharts[j].calculatedfield);
							}
							if ((tempCharts[j].charttype === "chart-geo" || tempCharts[j].charttype === "chart-map")&& tempCharts[j].bubble && tempCharts[j].bubble !== null	&& $.trim(tempCharts[j].bubble) !== "") {
								columns += "," + addTildInColumnName(tempCharts[j].bubble);
								columns += "," + addTildInColumnName(tempCharts[j].latitude);
								columns += "," + addTildInColumnName(tempCharts[j].longitude);
							}
							else if (tempCharts[j].charttype === "chart-paired-row") {
								columns += "," + addTildInColumnName(tempCharts[j].pairedfield);
							}else if (tempCharts[j].charttype === "chart-bubble" || tempCharts[j].charttype === "chart-heatmap" || 
									tempCharts[j].charttype === "chart-scatter" || tempCharts[j].charttype === "chart-series") {
								columns += "," + addTildInColumnName(tempCharts[j].xaxis);
								columns += "," + addTildInColumnName(tempCharts[j].yaxis);
								columns += "," + addTildInColumnName(tempCharts[j].radius);
							} else if (tempCharts[j].charttype === "chart-composite" || tempCharts[j].charttype === "chart-grouped-barline" || 
									tempCharts[j].charttype === "chart-composite-brush") { 
								for ( var x = 1; x < 10; x++) {
									if (tempCharts[j] && tempCharts[j]["measure-" + x]
											&& tempCharts[j]["measure-" + x] !== null) {
										columns += ","
												+ addTildInColumnName(tempCharts[j]["measure-" + x]);
									}
								}
							} else if (tempCharts[j].charttype === "chart-grouped-row" || tempCharts[j].charttype === "chart-multilevel" || tempCharts[j]=== "chart-treemap") {
								for ( var x = 1; x < 6; x++) {
									if (tempCharts[i] && tempCharts[i]["level-" + x]
											&& tempCharts[i]["level-" + x] !== null) {
										columns += ","+ addTildInColumnName(tempCharts[i]["level-" + x]);
									}
									if(tempCharts[i] && tempCharts[i]["level-"+ x+"-desc"]){
										columns += ","+ addTildInColumnName(tempCharts[i]["level-"+ x+"-desc"]);
										var tempFieldDescriptions = {
												"name" : addTildInColumnName(tempCharts[j]["level-"+ x]),
												"description" : addTildInColumnName(tempCharts[j]["level-"+ x + "-desc"])
											};
											fieldDescriptions.push(tempFieldDescriptions);
									}
								}
							}
							else if (tempCharts[j].charttype === "chart-hierarchie" || tempCharts[j].charttype === "chart-treemap") {
								for ( var x = 1; x < 5; x++) {
									if (tempCharts[j]["level-" + x]
											&& tempCharts[j]["level-" + x] !== null) {
										columns += ","
												+ addTildInColumnName(tempCharts[j]["level-" + x]);
									}
									if (tempCharts[j]["level-" + x + "-desc"]
											&& tempCharts[j]["level-" + x
													+ "-desc"] !== null) {
										columns += ","
												+ addTildInColumnName(tempCharts[j]["level-" + x]);
										var tempFieldDescriptions = {
											"name" : addTildInColumnName(tempCharts[j]["level-"+ x]),
											"description" : addTildInColumnName(tempCharts[j]["level-"+ x + "-desc"])
										};
										fieldDescriptions
												.push(tempFieldDescriptions);
									}
								}
							}
						}
					}else{
						if(THIS.dataQuery){
							if(!tempCharts[j].data.table || tempCharts[j].data.table === "null")
								tempCharts[j].data.dataQuery = THIS.dataQuery;
						}
					}
				}
			}
		}
		columnObj.columns = columns;
		columnObj.customFields = customFields;
		columnObj.facts = facts;
		columnObj.fieldDescriptions = fieldDescriptions;
		return columnObj;
		
	};
	var getFilterQueryFromFilterConfig = function(filterConfig,source){
		source = source || THIS.sourceType;
		var filterQuery = "";
		if (filterConfig && filterConfig.length > 0) {
			filterQuery = "1=1";
			var includenull = "";
			for ( var i = 0; i < filterConfig.length; i++) {
				var searchtype = filterConfig[i].searchtype || " IN ",
				fromDateOperator=">=",toDateOperator="<=";
				if($.trim(searchtype.toLowerCase()) === "not in"){
					fromDateOperator="<=";
					toDateOperator=">=";
				}
				includenull = "";
				if(filterConfig[i].includenull){
					includenull = " or "+addTildInColumnName(filterConfig[i]["fieldName"])+" = '' or "+addTildInColumnName(filterConfig[i]["fieldName"]) +" is null ";
				}
				if (filterConfig[i]["fieldType"] === 'lookup' || filterConfig[i]["fieldType"] === 'select' || filterConfig[i]["fieldType"] === 'lookupdropdown') {
					var lookupIds = "",selectedIds;
					var lookupData = filterConfig[i]["defaultvalue"];
					if (!lookupData || lookupData.length === 0)
						continue;
					if(filterConfig[i]["fieldType"] === 'lookupdropdown' && filterConfig[i].isSingle && !lookupData.trim())
						continue;
					if(lookupData && filterConfig[i].isSingle){
						if(filterConfig[i]["fieldType"] === 'select' && typeof(lookupData) === "object" && lookupData.id){
							lookupData = lookupData.id;
						 }
						var selectedValue;
						if((filterConfig[i]["fieldType"] === 'lookupdropdown' || filterConfig[i]["fieldType"] === 'select') && lookupData.trim()){
							lookupIds = lookupData;
						}else if(lookupData.id !== '""' && lookupData.id !== 'null'){
							lookupIds = lookupData.id;
						}
						if(lookupIds){
							if(lookupIds === "?"){
								if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "like")
									filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"]) + " is null OR " + addTildInColumnName(filterConfig[i]["fieldName"]) + " = '')";
								else
									filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"]) +" is not null AND " + addTildInColumnName(filterConfig[i]["fieldName"]) + " <> '')";
							}else{
								if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "not in"){
									lookupIds = ("'" + lookupIds + "'");
									filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"])
									+" "+ searchtype+ " (" + lookupIds + ")"+includenull;
								}else{
									filterQuery+= addTildInColumnName(filterConfig[i]["fieldName"])+" "+searchtype + " '%" + lookupIds + "%'"+includenull;
								}
							}
						}
					} else if (lookupData && lookupData.length > 0) {
						if( filterConfig[i]["fieldType"] === 'select')
							lookupData = lookupData.map(function(o){return (typeof o === 'object' && o.id)?o.id:o});
						if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "not in"){
							if(filterConfig[i]["fieldType"] === 'lookupdropdown' || filterConfig[i]["fieldType"] === 'select'){
								var indexVal = lookupData.indexOf(" ");
								if(indexVal>-1)
									lookupData.splice(indexVal,1);
								if(lookupData.indexOf("?")>-1){
									lookupData = lookupData.map(function(o) { return o == "?" ? "" : o; });
									lookupData.push(null);
								}
								lookupIds = lookupData.map(function(e) {e = e !== null && e.indexOf('`')>-1 ? e.replace(/`/g, '"') :e; return "'" + e+ "'"}).toString();
							}else{
								for ( var x = 0; x < lookupData.length; x++) {
									if(lookupData[x].id !== '""' && lookupData[x].id !== 'null'){
										if (x === 0 )
											lookupIds = "'" + lookupData[x].id + "'";
										else
											lookupIds += ",'" + lookupData[x].id + "'";
									}
								}
							}
							
						  
						   if (lookupData.indexOf("Others") !== -1){
								if(lookupData.length==1)
								{
									filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"])
									+ " IS NULL"+includenull;
								}
								else
								{  
									filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"])
									+ " IS NULL AND " +  addTildInColumnName(filterConfig[i]["fieldName"])
									+" "+ searchtype+ " (" + lookupIds + ")"+includenull;
								}
							}
						    else{
									filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"])
									+" "+ searchtype+ " (" + lookupIds + ")"+includenull;								
							}
						}else{
							filterQuery += " AND (";
							var operator = $.trim(searchtype.toLowerCase()) === 'like'?' OR ':' AND ';
							if(filterConfig[i]["fieldType"] === 'lookupdropdown' ||  filterConfig[i]["fieldType"] === 'select'){
								var indexVal = lookupData.indexOf(" ");
								if(indexVal>-1)
									lookupData.splice(indexVal,1);
								if(lookupData.indexOf("?")>-1){
									lookupData = lookupData.map(function(o) { return o == "?" ? null : o; });
								}
							}
							for(var x=0;x<lookupData.length;x++){
								(filterConfig[i]["fieldType"] === 'lookupdropdown' || filterConfig[i]["fieldType"] === 'select')?lookupIds=lookupData[x]:lookupData[x].id;
								if(x === 0)
									filterQuery+= addTildInColumnName(filterConfig[i]["fieldName"])+" "+searchtype + " '%" + lookupIds + "%'";
								else
									filterQuery+= operator +addTildInColumnName(filterConfig[i]["fieldName"])+" "+searchtype + " '%" + lookupIds + "%'";
							}
						}
					}
					
				if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "in")
					filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"]) + " is null OR " + addTildInColumnName(filterConfig[i]["fieldName"]) + " = '')";
				else if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "not in")
					filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"]) +" is not null AND " + addTildInColumnName(filterConfig[i]["fieldName"]) + " <> '')";
				else if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "like")
					filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"]) +" like '%null%' )";
				else if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "not like")
					filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"]) +" not like '%null% )";
				else
					filterQuery += " )";
				} else if (filterConfig[i]["fieldType"] === 'NUM') {
					if (filterConfig[i]["from-defaultvalue"]
							&& $.trim(filterConfig[i]["from-defaultvalue"]) !== "")
						filterQuery += " AND " + addTildInColumnName(filterConfig[i]["fieldName"])
								+ " >= " + filterConfig[i]["from-defaultvalue"];
					if (filterConfig[i]["to-defaultvalue"]
							&& $.trim(filterConfig[i]["to-defaultvalue"]) !== "")
						filterQuery += " AND " + addTildInColumnName(filterConfig[i]["fieldName"])
								+ " <= " + filterConfig[i]["to-defaultvalue"];
				}else if(filterConfig[i]["fieldType"] === 'dateRange'){
				var toDate,fromDate;
				var formatForDate = filterConfig[i]["dateFormat"];
				if(formatForDate === undefined){
					formatForDate = 'MM/dd/yyyy';
				}
                var returnedValue = dataAnalyser.storedDateRange(filterConfig[i]["defaultvalue"])
				if(returnedValue && returnedValue != ""){
					 fromDate = returnedValue[0].trim();
					 toDate = returnedValue[1].trim();
				}
				if (source ==="postgreSQL"){
					if (fromDate && $.trim(fromDate) !== "")
						filterQuery += " AND ("
								+ addTildInColumnName(filterConfig[i]["fieldName"])
								+  fromDateOperator+" to_date('"
								+ fromDate
								+ "', 'MM/dd/yyyy')"+includenull+")";
					if (toDate && $.trim(toDate) !== "")
						filterQuery += " AND ("
								+ addTildInColumnName(filterConfig[i]["fieldName"])
								+ toDateOperator+" to_date('"
								+ toDate
								+ "', 'MM/dd/yyyy')+1"+includenull+")";
				}else{
					if (fromDate && $.trim(fromDate) !== "")
						filterQuery += " AND (unix_timestamp("
								+ addTildInColumnName(filterConfig[i]["fieldName"])
								+ ", '"+formatForDate+"') "+fromDateOperator+" unix_timestamp('"
								+ fromDate
								+ "','MM/dd/yyyy')"+includenull+" )";
					if (toDate && $.trim(toDate) !== "")
						filterQuery += " AND (unix_timestamp("
								+ addTildInColumnName(filterConfig[i]["fieldName"])
								+ ",'"+formatForDate+"') "+toDateOperator+" unix_timestamp('"
								+ toDate
								+ "','MM/dd/yyyy')"+includenull+")";
					}
					filterQuery += includenull;
				
				} else if (filterConfig[i]["fieldType"] === 'DATE' || filterConfig[i]["fieldType"] === 'dynamicDate') {
					var fromDate = filterConfig[i]["from-defaultvalue"];
					var toDate = filterConfig[i]["to-defaultvalue"];
					var formatForDate = filterConfig[i]["dateFormat"];
					if(formatForDate === undefined){
						formatForDate = 'MM/dd/yyyy';
					}
					if(filterConfig[i]["fieldType"] === 'dynamicDate' && isNaN(Date.parse(filterConfig[i]["from-defaultvalue"]))){
						fromDate = eval(filterConfig[i]["from-defaultvalue"]);
						toDate = eval(filterConfig[i]["to-defaultvalue"]);
					}
					if (source ==="postgreSQL"){
						if (filterConfig[i]["from-defaultvalue"]
						&& $.trim(filterConfig[i]["from-defaultvalue"]) !== "")
							filterQuery += " AND ("
									+ addTildInColumnName(filterConfig[i]["fieldName"])
									+  fromDateOperator+" to_date('"
									+ fromDate
									+ "', '" +formatForDate+"')"+includenull+" )";
						if (filterConfig[i]["to-defaultvalue"]
								&& $.trim(filterConfig[i]["to-defaultvalue"]) !== "")
							filterQuery += " AND ("
									+ addTildInColumnName(filterConfig[i]["fieldName"])
									+ toDateOperator+" to_date('"
									+ toDate
									+ "','"+formatForDate+"')+1"+includenull+")";
					}else{
						if (filterConfig[i]["from-defaultvalue"]
								&& $.trim(filterConfig[i]["from-defaultvalue"]) !== "")
							filterQuery += " AND (unix_timestamp("
									+ addTildInColumnName(filterConfig[i]["fieldName"])
									+ ", '"+formatForDate+"') "+fromDateOperator+" unix_timestamp('"
									+ fromDate
									+ "','MM/dd/yyyy')"+includenull+" )";
						if (filterConfig[i]["to-defaultvalue"]
								&& $.trim(filterConfig[i]["to-defaultvalue"]) !== "")
							filterQuery += " AND (unix_timestamp("
									+ addTildInColumnName(filterConfig[i]["fieldName"])
									+ ",'"+formatForDate+"') "+toDateOperator+" unix_timestamp('"
									+ toDate
									+ "','MM/dd/yyyy')"+includenull+")";
					}
				} else if(filterConfig[i]["fieldType"] === 'raw') {
					filterQuery += " AND ";
					filterQuery+=filterConfig[i]["defaultvalue"];
				}
				 else {
						if (filterConfig[i]["defaultvalue"] && $.trim(filterConfig[i]["defaultvalue"]) !== ""){
							if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "not in"){
								filterQuery += " AND lower(" + addTildInColumnName(filterConfig[i]["fieldName"])
								+ ") "+searchtype+ " ('" + filterConfig[i]["defaultvalue"].toLowerCase()
								+ "')"+includenull;
							}else{
								filterQuery += " AND lower(" + addTildInColumnName(filterConfig[i]["fieldName"])
								+ ") "+searchtype+ " '%" + filterConfig[i]["defaultvalue"].toLowerCase()
								+ "%'" +includenull;
							}
							
						}
						
					}
						
				}
			}
		
		return filterQuery;
	};
	var getSummaryFieldFromConfig = function(summaryConfig,facts){
		var sumarryFields = [];
		if (summaryConfig && summaryConfig.length > 0) {
			for ( var i = 0; i < summaryConfig.length; i++) {
				for ( var j = 0; j < summaryConfig[i].length; j++) {
					if ( summaryConfig[i][j].fieldname
							&& $.trim(summaryConfig[i][j].fieldname) !== "" && $.inArray(summaryConfig[i][j].fieldname, sumarryFields) === -1)
						sumarryFields.push(summaryConfig[i][j].fieldname);
					if ( summaryConfig[i][j].filter
							&& $.trim(summaryConfig[i][j].filter) !== "" && $.inArray(summaryConfig[i][j].filter, sumarryFields) === -1)
						sumarryFields.push(summaryConfig[i][j].filter);
					if (summaryConfig[i][j].group
							&& summaryConfig[i][j].group !== null
							&& $.trim(summaryConfig[i][j].group) !== ""  && $.inArray(summaryConfig[i][j].group, facts) === -1 && $.trim(summaryConfig[i][j].group) !== "fact") {
						facts.push(addTildInColumnName(summaryConfig[i][j].group));
					}
					if ( summaryConfig[i][j].divider
							&& summaryConfig[i][j].divider !== null
							&& $.trim(summaryConfig[i][j].divider) !== "" && $.inArray(summaryConfig[i][j].divider, facts) === -1 && $.trim(summaryConfig[i][j].divider) !== "fact" ) {
						facts.push(addTildInColumnName(summaryConfig[i][j].divider));
					}
					if(summaryConfig[i][j].calculatedformula){
						var uniqueNames = [];
						var names = summaryConfig[i][j].calculatedformula.match(/[^[\]]+(?=])/g);
						$.each(names, function(i, el){
						    if($.inArray(el, uniqueNames) === -1) {
						    	uniqueNames.push(addTildInColumnName(el));
						    }
						    if($.inArray(el, sumarryFields) === -1) {
							    sumarryFields.push(addTildInColumnName(el));
						    }
						});
						summaryConfig[i][j].calculatedfield = uniqueNames.join(",");
					}
				}
			}
		}
		return {
			sumarryFields:sumarryFields,
			facts:facts
		};
	};
	var getColumnByFacts = function(facts,customFacts,customFields){
		var columns = [];
		if (facts && facts !== "" && facts !== "drop measure here"
			&& facts !== "fact" && facts.length > 0) {
			var countIndex = facts.indexOf("_count");
			if (countIndex > -1) {
				facts.splice(countIndex, 1);
			}
			var factIndex = facts.indexOf("fact");
			if (factIndex > -1) {
				facts.splice(factIndex, 1);
			}
			for(var k=0; k<facts.length; k++){
				var isCustomFact = false;
				if(customFacts && customFacts.length>0){
					for(var l=0; l<customFacts.length; l++){
						if(facts[k] === customFacts[l].name){
							isCustomFact = true;
							var names = customFacts[l].formula.match(/[^[\]]+(?=])/g);
							$.each(names, function(i, el){
							    if($.inArray(el, columns) === -1) {
							    	columns.push(addTildInColumnName(el));
							    }
							});
							customFields[customFacts[l].name] = customFacts[l].name;
						}
					}
				}
				if(!isCustomFact){
					columns.push(facts[k]);
				}
			}
		}
		if(customFacts && customFacts.length>0){
			for(var i=0; i<customFacts.length; i++){
				// if(columns.indexOf(customFacts[i].name)>-1){
					var names = customFacts[i].formula.match(/[^[\]]+(?=])/g);
					$.each(names, function(index, el){
					    if($.inArray(el, columns) === -1) {
					    	columns.push(el);
					    }
					});
					// columns =
					// columns.replaceAll(customFacts[i].name,uniqueNames.join(","));
					customFields[customFacts[i].name] = customFacts[i].name;
				}
			// }
		}
		if(columns.length>0){
			columns = columns.filter(function(elem, index, self) {
			    return index == self.indexOf(elem);
			});
		}
		return {
			columns:columns.toString(),
			customFields:customFields
		};
	};
	var callService = function(config,filterConfig,summaryConfig,profileId){
		var columns = "";
		var facts = [];
		var dependentFacts = [];
		facts.push.apply(facts, THIS.facts);
		THIS.filters = filterConfig;
		THIS.customFields = {};
		THIS.fieldAliasMapping = {};
// facts.push(THIS.facts);
		var fieldDescriptions = [];
		var columnsObj = getColumnsFromConfig(config,profileId,facts,THIS.customFacts);
		columns = columnsObj.columns;
		THIS.customFields = columnsObj.customFields;
		fieldDescriptions = columnsObj.fieldDescriptions;
		var filterQuery = "" , reportQuery = "" ;
		filterQuery = getFilterQueryFromFilterConfig(filterConfig, THIS.sourceType);
		if(THIS.additionalconfig){
			var profileFilters = filterConfig ? filterConfig : THIS.filters;
			if(THIS.additionalconfig.applicableFilters && THIS.additionalconfig.applicableFilters.length >0){
				var reportFilter =[];
				profileFilters.forEach( function( value ) {
				    if( THIS.additionalconfig.applicableFilters.indexOf( value['fieldName'] ) != -1 ) {
				    	reportFilter.push(value);
				       
				    }
				});
			}
		}
		reportQuery = getFilterQueryFromFilterConfig(reportFilter, THIS.sourceType);
		localStorage["reportFilter"] = reportQuery;
		var sumamryObj = getSummaryFieldFromConfig(summaryConfig,facts);
		facts = sumamryObj.facts;
		if (sumamryObj.sumarryFields && sumamryObj.sumarryFields.length > 0)
			columns += (columns && columns.trim()?" , ":"") + sumamryObj.sumarryFields.join(" , ");
		// var facts = THIS.facts;
		THIS.config = config;
		THIS.Summary = summaryConfig;
		THIS.filterQuery = filterQuery;
		if (columns !== "") {
			var factsObj = getColumnByFacts(facts,THIS.customFacts,THIS.customFields);
			if(factsObj.customFields && Object.keys(factsObj.customFields)>0){
				THIS.customFields = factsObj.customFields;				
			}
			if(factsObj.columns){
				columns += "," + factsObj.columns;
			}
			$.each(THIS.dependentFacts, function(i, el){ // removes
															// Duplicates
			    if($.inArray(el, dependentFacts) === -1) dependentFacts.push(addTildInColumnName(el));
			});
			
			if(dependentFacts && dependentFacts.length>0){
				columns += "," + dependentFacts.join(","); 
			}
			if (localStorage["from"] && localStorage["from"] !== "" && localStorage[localStorage["from"]+"_"+location.hash.replace(/^#/, '').split("?")[0]]){
				if (filterQuery && filterQuery !== "")
					filterQuery	+= " AND ";
				filterQuery +=  localStorage[localStorage["from"]+"_"+location.hash.replace(/^#/, '').split("?")[0]];
			}
			columns = columns.replaceAll(",_count","");
			var measureArray = columns.split(',');
			var colIndex;
			measureArray = measureArray.filter( function( item, index, inputArray ) {
		           return inputArray.indexOf(item) == index;
		    });
			for (var i=0; i<THIS.customFacts.length; i++) {
				colIndex = measureArray.indexOf(THIS.customFacts[i].name);
			    if (colIndex > -1) {
			    	measureArray.splice(colIndex, 1);
			    }
			}
			var request = {
				"filters" : filterQuery,
				"profilekey" : location.hash.replace(/^#/, '')
			};
			if(THIS.dataQuery){
				var query = calculateDynamicFilter(THIS.dataQuery,filterConfig);
				var filters = query.split(",");
				// changes alias to original column name
				if(filters && filters.length>0){
					for(var i=0;i<filters.length;i++){
						filters[i] = filters[i].replace(/ +(?= )/g,'');
						if(filters[i].indexOf(" as ")>0){
							var alias = filters[i].split(" as ");
							if((alias && alias.length>1)&& filterQuery.indexOf(alias[1].trim())>-1){
								var regex = new RegExp(alias[1].trim(), "g");
								var aliasColumnName = alias[0].trim() != "\"\"" ? alias[0].trim() : alias[1].trim();
								filterQuery = filterQuery.replace(regex,aliasColumnName);
							}
						}
					}
				}
				// //////////////////////////////////////
				request.filters = filterQuery;
				request.query = query;
			}else{
				request.table = THIS.table;
				request.columns = measureArray.toString();
			}
			if (fieldDescriptions && fieldDescriptions.length > 0) {
				request.masterdata = JSON.stringify({
					"descriptionList" : fieldDescriptions
				});
			}
			if(localStorage["isMultipleProfile"]){
				request.fieldAliasMapping = THIS.fieldAliasMapping;
				return request;
			}else{
				request.source = THIS.sourceType;
				request.database = THIS.database;
				if (THIS.additionalconfig && THIS.additionalconfig.profileGroupBy){
					request.groupByEnabled = true;
				}
				enableLoading();
				callAjaxService("getData", callBackSuccessGetData, callBackFailure,
						request, "POST",null,true);
			}
		} else {
			callBackGetData(null, config, THIS.Summary);
		}
	};
	var callBackSuccessGetData = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			disableLoading();
			if (response && response.data && response.data.length > 0 ) {
				$("#widget-grid > div >div:not(.search-div)").show();
				$("#results").hide();
				callBackGetData(response, THIS.config, THIS.Summary);
				
			} else if ( window.location.hash.replace(/^#/, '') !== "dataAnalyser"){
				$("#widget-grid > div >div:not(.search-div)").hide();
				$("#results").show().html('<div class="alert alert-danger fade in"><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No matching results found for selected filters.</p></div>');
			     if(THIS.filters && THIS.filters.length>0){
					var invisibleFilterLength = THIS.filters.filter(function(a){return a.invisible;}).length;
					if(THIS.filters && THIS.filters.length !== invisibleFilterLength && !localStorage["isMultipleProfile"]){
						$('#divSankeyFilter').parents(".jarviswidget-sortable").show();
						if (!constructFilters(THIS.filters,filterContentId))
							return;
						if(THIS.additionalconfig.isFilterHide){
		    				$('#divSankeyFilter').parents(".jarviswidget-sortable").hide();
		    			}
					}
				}
				}
		}
	};
	var callBackGetData = function(response, config, summary,filters,facts,profileId) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			config = config || THIS.config;
			summary = summary || THIS.Summary;
			filters = filters || THIS.filters;
			facts = facts || THIS.facts;
			disableLoading();
			var data = null;
			if (response && response !== null) {
				data = response["data"] || Jsonparse(response);
				THIS.masterData = response["masterData"];
				var timeLineFields = [];
				var bubbleFields = [];
				var heatmapFields = [];
				var dateFormatString = "%m/%d/%Y";
				var bubbleDateFormat = "%m/%d/%Y";
				var heatmapDateFormat = "%m/%d/%Y";
				if (config && config.length > 0) {
					for ( var j = 0; j < config.length; j++) {
						if (config[j].charts && config[j].charts.length > 0) {
							var mapping = config[j].charts;
							for ( var i = 0, len = mapping.length; i < len; i++) {
								if (mapping[i].charttype === "chart-line") {
									timeLineFields.push(mapping[i].alias);
									if (mapping[i].dateformat)
										dateFormatString = mapping[i].dateformat;
								}else if((mapping[i].charttype === "chart-bubble" || mapping[i].charttype === "chart-series") && mapping[i].xaxistype === "date"){
										bubbleDateFormat = d3.timeFormat(mapping[i].xaxisdateformat|| bubbleDateFormat);
										bubbleFields.push(mapping[i].xaxis);
								}
								else if(mapping[i].charttype === "chart-heatmap" && mapping[i].xaxistype === "date"){
									heatmapDateFormat = d3.timeFormat(mapping[i].xaxisdateformat|| heatmapDateFormat);
									heatmapFields.push(mapping[i].xaxis);
								}
							}
						}
					}
				}
				var dateSummaryFields = [];
				if (summary && summary.length > 0) {
					for ( var j = 0; j < summary.length; j++) {
						if (summary[j] && summary[j].length > 0) {
							var mapping = summary[j];
							for ( var i = 0, len = mapping.length; i < len; i++) {
								if(localStorage.isMultipleProfile && THIS.profileId){
									mapping[i].id = mapping[i].id+THIS.profileId;
								}
								if (mapping[i]["field-type"] === "date") {
									dateSummaryFields.push({
										"dateFomat" : mapping[i]["date-format"]|| "%m/%d/%Y",
										"fieldName" : mapping[i]["fieldname"]
									});
								}
							}
						}
					}
				}
				var dateFormat = d3.timeParse(dateFormatString);
				var isFromQuery = false;
				if(THIS.fieldAliasMapping && Object.keys(THIS.fieldAliasMapping).length>0){
					isFromQuery = true;
				}
				data.forEach(function(d) {
					if(localStorage["isMultipleProfile"]){
						if(Jsonparse(dataAnalyser.additionalconfig)["isFilterNotapplicable"]){
							$.each(d, function(key, value){
								if(d[key] == null || d[key] == ""){
									d[key] = "N/A"
										}
							});
						}
					}else{
						if(dataAnalyser.additionalconfig['isFilterNotapplicable']){
							$.each(d, function(key, value){
								if(d[key] == null || d[key] == ""){
									d[key] = "N/A"
										}
							});
						}
					}	
					if(isFromQuery){
						for(var field in THIS.fieldAliasMapping) {
							if(d[field])
								d[THIS.fieldAliasMapping[field]] = d[field];
						}
						if(!d["_count"])
							d["_count"] = 1;
					}
					if(THIS.customFacts && THIS.customFacts.length>0){
						THIS.customFacts.forEach(function(customfact){
							for(var customField in THIS.customFields) {
								if(THIS.customFields[customField] === customfact.name){
									d[customField] = eval(customfact.formula.replace(/[\[\"]+/g,"d['").replace(/[\]\"]+/g,"']")) || 0;
								} 
							}
						});
					}
					timeLineFields.forEach(function(x) { 
						if(d[x])
							d["date-" + x] = dateFormat(d[x]);
						if(d["date-" + x])
							d["month-" + x] = d3.timeMonth(d["date-" + x]);
					});
					bubbleFields.forEach(function(x) {
						if(d[x])
							d["date-"+x] = bubbleDateFormat.parse(d[x]);			
					});
					heatmapFields.forEach(function(x) {
						if(d[x])
							d["date-"+x] = heatmapDateFormat.parse($.trim(d[x]));			
					});
					dateSummaryFields.forEach(function(y) {
						var dateFormat = d3.timeFormat(y.dateFomat);
						d[y.fieldName] = dateFormat.parse(d[y.fieldName]);
					});
					if (facts.length !== 0 && facts[0] !=="fact"){
						d[facts[0]] = +d[facts[0]];
					}else{
						d["fact"] = 1;	
					}	
						
				});

			}
			if(profileId){
				THIS.modalProfile = new Profiling(data, config, filters,summary);
				THIS.modalProfile.init();
			}else{
				THIS.profiling = new Profiling(data, THIS.config, THIS.filters,summary, THIS.masterData,THIS.viewSettings,THIS.chartDefaultFilters);
				THIS.profiling.init();
				setTimeout(function(configData){ 
					for(var i=0;i<configData.length;i++){
						if(configData[i]["class"].indexOf("jarviswidget-collapsed")>-1){
							$("#widget-grid").find(".jarviswidget-collapsed").find("[role='content']").hide();
						 }
					} }, 100,THIS.config);	
			}
			if(THIS.filters && THIS.filters.length>0){
				var invisibleFilterLength = THIS.filters.filter(function(a){return a.invisible;}).length;
				if(THIS.filters && THIS.filters.length !== invisibleFilterLength && !localStorage["isMultipleProfile"]){
					$('#divSankeyFilter').parents(".jarviswidget-sortable").show();
					if(THIS.additionalconfig.isFilterHide){
	    				$('#divSankeyFilter').parents(".jarviswidget-sortable").hide();
	    			}else{
	    				if (!constructFilters(THIS.filters,filterContentId))
							return;
					}
				}
			}
			data = null;
		}
	};
	
	/**
	 * Configures Filter data and returns the value
	 * 
	 */
	var callBacksuggestionsLookUp = function(response){
		console.log('cALLbackSuggestionlookup'+response)
	}
	
	this.getFilters = function(){
		return getFilters();
	};
	var getFilters = function(){
		var filterConfig = [];
		var defaultSelVal='';
		$("#filters > .well")
				.each(
						function(index) {
							var currentObj = $(this);
							if (currentObj.find("[name=field]").select2('val')
									&& currentObj.find("[name=field]").select2(
											'val') !== "") {
								var tempConfig = {
									"fieldName" : currentObj.find("[name=field]").select2('val'),
									"displayName" : currentObj.find("[name=displayname]").val(),
									"fieldType" : currentObj.find("[name=fieldtype]").val(),
									'id':'f'+(index+1)
								};
								if(currentObj.find("[name=isMandatory]").is(":checked")){
									tempConfig.isMandatory = true;
								}
								if(currentObj.find("[name=chat]").is(":checked")){
									tempConfig.chat = true;
								}else {
									tempConfig.chat = false;
								}
								if(currentObj.find("[name=invisible]").is(":checked")){
									tempConfig.invisible = true;
								}
								if(currentObj.find("[name=includenull]").is(":checked")){
									tempConfig.includenull = true;
								}
								if(currentObj.find("[name=innotin]").is(":checked")){
									tempConfig.innotin = true;
								}
								
								
								if(currentObj.find("[name=interdependentcolumns]").select2('val')&& currentObj.find("[name=interdependentcolumns]").select2(
								'val') !== ""){
									
										tempConfig.interdependentcolumns = currentObj.find("[name=interdependentcolumns]").select2('val');
								}
								
								
							
								if(currentObj.find("[name=defaultQueryValue]").is(":checked")){
									tempConfig.defaultQueryValue = true;
									if(currentObj.find("[name=defaultSelectedVal]")){
									tempConfig["defaultquery"] = currentObj.find("[name=defaultSelectedVal]").val();
								}
									
								}
								if(currentObj.find("[name=searchtype]")){
									tempConfig.searchtype = currentObj.find("[name=searchtype]").val();
								}
								// profile analyser data push
								if(localStorage.isMultipleProfile){
									tempConfig["source"] = currentObj.find("[name=Source]").select2('val')?currentObj.find("[name=Source]").select2('val'):"";
									tempConfig["database"] = currentObj.find("[name=Database]").select2('val')?currentObj.find("[name=Database]").select2('val'):"";
								}
								
								if (tempConfig["fieldType"] === 'lookup' || tempConfig["fieldType"] === 'select' ||tempConfig["fieldType"] === 'lookupdropdown') {
									tempConfig["lookupquery"] = currentObj.find("[name=lookup]").val();
									var jsonData = Jsonparse( currentObj.find("[name=lookup]").val());
									if(jsonData && jsonData.length>0){
										tempConfig["lookupquery"] = jsonData	
									}
									tempConfig.isSingle = currentObj.find("[name=lookupdiv]:eq(1)").find('select').val();
									if(tempConfig["fieldType"] === 'lookupdropdown' || tempConfig["fieldType"] === 'select'){
										tempConfig["defaultvalue"] = currentObj.find("[name=defaultvaluelabel]").find("select").val() || "";
									}else{
										tempConfig["defaultvalue"] = currentObj.find("[name=defaultvalue]").select2('data');
									}
								} else if (tempConfig["fieldType"] === 'DATE' || tempConfig["fieldType"] === 'dynamicDate' || tempConfig["fieldType"] === 'NUM' ) {
									tempConfig["from-defaultvalue"] = currentObj.find("[name=from-defaultvalue]").val();
									tempConfig["to-defaultvalue"] = currentObj.find("[name=to-defaultvalue]").val();
									if(currentObj.find('div').find($("input[name='lookup']")).val() != ''){
										tempConfig["dateFormat"] = currentObj.find('div').find($("input[name='lookup']")).val();	
									}
									
								}else if(tempConfig["fieldType"] === 'dateRange'){
									if(currentObj.find('div').find($("input[name='lookup']")).val() != ''){
										tempConfig["dateFormat"] = currentObj.find('div').find($("input[name='lookup']")).val();	
									}
									tempConfig["defaultvalue"] = currentObj.data();
								}else if(tempConfig["fieldType"] === 'raw'){
									
									tempConfig["defaultvalue"] = currentObj.find("[name=defaultvalue]").val();
								}
								else {
									tempConfig["defaultvalue"] = currentObj.find("[name=defaultvalue]").val();
								}
								filterConfig.push(tempConfig);
							}
						});
		return filterConfig;
	};
	
	/**
	 * Constructs Config data based on charts & tables in Screen and returns the
	 * value
	 * 
	 */
	var getConfig = function(){ 
		var currentObjConfig = [];
		THIS.facts = [];
		THIS.factsConfig = [];
		$("#addFacts").children().each(function(index,item){
			THIS.facts.push($(item).attr("data-fact"));
			THIS.factsConfig.push({fact:$(item).attr("data-fact"),displayname:$(item).attr("data-displayname")});
			});
		
		if (THIS.facts.length === 0)
			THIS.facts.push("fact");
		var count = 0;
		var tableCounter = 0;
		var imageCounter = 0;
		var calendarCounter = 0;
		var NGramCounter = 0;
		var IFrameCounter = 0;
		var HTMLTemplateCounter = 0;
		var commentSectionNameHeader = '';
		$("#droppable > article")
				.children(":not(#configSummaryDiv)")
				.each(
						function() {
							var currentObj = $(this);
							var tempConfig = {
								"width" : $(this).attr("class").replaceAll(" chart-parent-div", "").concat(" chart-parent-div")
							};
							var tempWidget = $(currentObj.children());
							tempConfig["id"] = tempWidget.attr("id").replace("-temp", "")+ "-temp";
							tempConfig["class"] = tempWidget.attr("class");
							tempConfig["header"] = tempWidget.find("header").find("h2").html();
							tempConfig["icon"] = tempWidget.find("header").find(".widget-icon i").attr("class");
							tempConfig["tooltip"] = tempWidget.find("[name='tooltip']").val();
							var tempCharts = tempWidget.find(".widget-body").children();
							var chartsConfig = [];
							commentSectionNameHeader = (tempConfig["header"])?tempConfig["header"]:'';
							tempCharts
									.each(function() {
										count++;
										var currentChart = $(this);
										if (currentChart.hasClass("chart-div")) {
											var tempChartConfig = {
												"width" : $(this).attr("class")
											};
											var tempChart = currentChart
													.find("a.axisHeader");
											if (tempChart && tempChart.length > 0) {
												if(tempChart.data("customtype")){
													tempChartConfig.customtype = tempChart.data("customtype");
													tempChartConfig.formula = tempChart.data("formula");
												}
												tempChartConfig.displayname = tempChart.data("displayname");
												tempChartConfig.fieldname = tempChart.data("field");
												tempChartConfig.filter = tempChart.data("filter");
												tempChartConfig.filtervalue = tempChart.data("filtervalue");
												tempChartConfig.description = tempChart.data("description");
												tempChartConfig.chartwidth = tempChart.data("chartwidth");
												tempChartConfig.barwidth = tempChart.data("barwidth");
												tempChartConfig.chartheight = tempChart.data("chartheight");
												tempChartConfig.containerheight = tempChart.data("containerheight");
												tempChartConfig.numformat = tempChart.data("numformat");
												tempChartConfig.alias = "c"+ count;
												tempChartConfig.charttype = tempChart.data("charttype");
												tempChartConfig.aggregate = tempChart.data("aggregate");
												tempChartConfig.customsort = tempChart.data("customsort");
												tempChartConfig.orderby = tempChart.data("orderby");
												tempChartConfig.absolute = tempChart.data("absolute");
												tempChartConfig.codetype = tempChart.data("codetype");
												tempChartConfig.prefix = tempChart.data('prefix');
												tempChartConfig.suffix = tempChart.data('suffix');
												tempChartConfig.labelSuffix = tempChart.data('labelSuffix');
												tempChartConfig.showpercentage = tempChart.data('showpercentage');
												tempChartConfig.showselectedtext = tempChart.data('showselectedtext');
												tempChartConfig.zerovalues = tempChart.data('zerovalues');
												tempChartConfig.prefixScale = tempChart.data('prefixScale');
												tempChartConfig.labelAxisWidth = tempChart.data('labelAxisWidth');
												tempChartConfig.margintop = tempChart.data('margintop')?parseInt(tempChart.data('margintop')):0;
												tempChartConfig.marginbottom = tempChart.data('marginbottom')?parseInt(tempChart.data('marginbottom')):0;
												tempChartConfig.marginright = tempChart.data('marginright')?parseInt(tempChart.data('marginright')):0;
												tempChartConfig.marginleft = tempChart.data('marginleft')?parseInt(tempChart.data('marginleft')):0;
												tempChartConfig.hidexaxis = tempChart.data('hidexaxis');
												tempChartConfig.hideyaxis = tempChart.data('hideyaxis');
												tempChartConfig.brushon = tempChart.data('brushon');
												tempChartConfig.yaxisnumberformat = tempChart.data('yaxisnumberformat');
												tempChartConfig.calculatedformula = tempChart.data('calculatedformula')|| "";
												tempChartConfig.yaxisticks = tempChart.data('yaxisticks')|| 10;
												tempChartConfig.yaxisinteger = tempChart.data('yaxisinteger')|| true;
												tempChartConfig.legendhorizontal = tempChart.data("legendhorizontal");													
												tempChartConfig.legendxposition = tempChart.data("legendxposition");
												tempChartConfig.legendyposition = tempChart.data("legendyposition");
												tempChartConfig.showonlybardata = tempChart.data("showonlybardata");
												tempChartConfig.dynamicdimension = tempChart.data("dynamicdimension");
												tempChartConfig.tooltipkey = tempChart.data("tooltipkey");
												tempChartConfig.runningtotal = tempChart.data("runningtotal");
												tempChartConfig.showtable = tempChart.data("showtable");
												
												if (tempChartConfig.calculatedformula !== ""){
													var uniqueNames = [];
													var names = tempChartConfig.calculatedformula.match(/[^[\]]+(?=])/g);
													$.each(names, function(i, el){
													    if($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
													});
													tempChartConfig.calculatedfield = uniqueNames.join(",");
												}
												tempChartConfig.stack = tempChart.data('stack');
												tempChartConfig.linkaction = tempChart.data('linkaction');
												if (tempChart.data("colorcodes")) {
													if(tempChartConfig.codetype === "standard" || typeof (tempChart.data("colorcodes")) === "object"){
														tempChartConfig.colorcodes = tempChart.data("colorcodes");
													}else{
														tempChartConfig.colorcodes = Jsonparse(tempChart.data("colorcodes"),"COLOR CODES");														
													}
												}
												if (tempChart.data("colorfield")) {
													tempChartConfig.colorfield = tempChart.data("colorfield");
												}
												if (tempChart.data("measurelabel")) { 
													if(tempChartConfig.codetype === "measurelabel" || typeof (tempChart.data("measurelabel")) === "object"){
														tempChartConfig.measurelabel = tempChart.data("measurelabel");
													}else{
														tempChartConfig.measurelabel = Jsonparse(tempChart.data("measurelabel"),"MEASURE LABEL");														
													}
												}
												if (tempChart.data("linklabel")) {
													if(typeof (tempChart.data("linklabel")) === "object"){
														tempChartConfig.linklabel = tempChart.data("linklabel");
													}else{
														tempChartConfig.linklabel = Jsonparse(tempChart.data("linklabel"),"LINK LABEL");
													}													
												}
												if(tempChart.data("filtertype")){
													tempChartConfig.filtertype = tempChart.data("filtertype");
												}
												if(tempChart.data("filterfrom")){
													tempChartConfig.filterfrom = tempChart.data("filterfrom");
												}
												if(tempChart.data("filterdefaultval")){
													tempChartConfig.filterdefaultval = tempChart.data("filterdefaultval");
												}
												if(tempChart.data("filtervalues")){
													tempChartConfig.filtervalues = tempChart.data("filtervalues");
												}
												if(tempChart.data("defaultFiltervalue")){
													tempChartConfig.defaultFiltervalue = tempChart.data("defaultFiltervalue");
												}else if(tempChart.data("defaultFiltervalueFrom") || tempChart.data("defaultFiltervalueTo")){
													tempChartConfig.defaultFiltervalueFrom = tempChart.data("defaultFiltervalueFrom");
													tempChartConfig.defaultFiltervalueTo = tempChart.data("defaultFiltervalueTo");
												}
												tempChartConfig.codevalue = tempChart.data("codevalue");
												tempChartConfig.showtotal = tempChart.data("showtotal");
												if (tempChartConfig.charttype !== "chart-pie"
														&& tempChartConfig.charttype !== "chart-donut"
														&& tempChartConfig.charttype !== "chart-geo"
														&& tempChartConfig.charttype !== "chart-bubble"
															&& tempChartConfig.charttype !== "chart-map") {
													tempChartConfig.xaxisorientation = tempChart
															.data("xaxisorientation");
													tempChartConfig.yaxisorientation = tempChart
															.data("yaxisorientation");
													tempChartConfig.xaxislabel = tempChart
															.data("xaxislabel");
													tempChartConfig.yaxislabel = tempChart
															.data("yaxislabel");
												}
												if ($.trim(tempChart.data("group"))!== ""){
													  tempChartConfig.group =  tempChart.data("group");
													  tempChartConfig.isCustomGroup =  true;
												  } else{
													  tempChartConfig.group = THIS.facts[0];
													  tempChartConfig.isCustomGroup =  false;
												  }
												if (tempChartConfig.charttype === "chart-line") {
													tempChartConfig.dateformat = tempChart.data("dateformat");
													tempChartConfig.timelinetype = tempChart.data("timelinetype");
													tempChartConfig.hidechart = tempChart.data("hidechart");
													tempChartConfig.hideyaxisline = tempChart.data("hideyaxisline");
													tempChartConfig.hideyaxisbar = tempChart.data("hideyaxisbar");													
													tempChartConfig.autoplaytype = tempChart.data("autoplaytype");
													tempChartConfig.animation = tempChart.data("animation");
													tempChartConfig.playunit = tempChart.data("playunit");
													tempChartConfig.autoplay = tempChart.data("autoplay");
												}
												else if (tempChartConfig.charttype === "chart-paired-row") {
													tempChartConfig.pairedfield = tempChart.data('pairedfield');
													tempChartConfig.rightrowkey = tempChart.data('rightrowkey');
													tempChartConfig.leftrowkey = tempChart.data('leftrowkey');
												}
												else if (tempChartConfig.charttype === "chart-pie" ||tempChartConfig.charttype === "chart-donut") {
													tempChartConfig.externallabels = tempChart.data("externallabels");
											        tempChartConfig.externalradiuspadding = tempChart.data("externalradiuspadding");
											        tempChartConfig.minangleforlabel = tempChart.data("minangleforlabel");
											        tempChartConfig.drawpaths = tempChart.data("drawpaths");
											        tempChartConfig.legendxposition = tempChart.data("legendxposition");
											        tempChartConfig.legendyposition = tempChart.data("legendyposition");
											        tempChartConfig.legendhorizontal = tempChart.data("legendhorizontal");
											        tempChartConfig.showlegend = tempChart.data("showlegend");
											        tempChartConfig.innerradius = tempChart.data("innerradius");
											        
											        
												}
												else if(tempChartConfig.charttype === "chart-row"){
													tempChartConfig.excludeitems = tempChart.data("excludeitems");
													tempChartConfig.labelSuffix = tempChart.data("labelSuffix");
													
												}
												else if (tempChartConfig.charttype === "chart-geo" || tempChartConfig.charttype === "chart-map") {
													tempChartConfig.zoom = tempChart.data('zoom');
													tempChartConfig.label = tempChart.data('label');
													tempChartConfig.bubble = tempChart.data("bubble");
													tempChartConfig.latitude = tempChart.data("latitude");
													tempChartConfig.longitude = tempChart.data("longitude");
													tempChartConfig.maptype = tempChart.data("maptype");
													tempChartConfig.startColor = tempChart.data("startColor");
													tempChartConfig.endColor = tempChart.data("endColor");
													tempChartConfig.defaultFill = tempChart.data("defaultFill");
													tempChartConfig.projection = tempChart.data("projection");
													tempChartConfig.borderColor = tempChart.data("borderColor");
													tempChartConfig.highlightFillColor = tempChart.data("highlightFillColor");
													tempChartConfig.highlightBorderColor = tempChart.data("highlightBorderColor");
													tempChartConfig.showCountryLabels = tempChart.data("showCountryLabels");
												} else if (tempChartConfig.charttype === "chart-bubble" || tempChartConfig.charttype === "chart-heatmap" || 
														tempChartConfig.charttype === "chart-scatter" || tempChartConfig.charttype === "chart-series") {
													tempChartConfig.xaxis = tempChart.data("xaxis");
													tempChartConfig.yaxis = tempChart.data("yaxis");
													tempChartConfig.radius = tempChart.data("radius");
													tempChartConfig.elastic = tempChart.data("elastic");
													tempChartConfig.xaxistype = tempChart.data("xaxistype");
													tempChartConfig.yaxistype = tempChart.data("yaxistype");
													tempChartConfig.maxbubblesize = tempChart.data("maxbubblesize");
													tempChartConfig["color-1"] = tempChart.data("color-1");
													tempChartConfig["color-2"] = tempChart.data("color-2");
													tempChartConfig.xaxisformate = tempChart.data("xaxisformate");
													tempChartConfig.xaxisdateformat = tempChart.data("xaxisdateformat");
													tempChartConfig.xaxistooltipdateformat = tempChart.data("xaxistooltipdateformat");
													tempChartConfig.xaxisinterval = tempChart.data("xaxisinterval");
													tempChartConfig["xaxis-aggre"] = tempChart.data("xaxis-aggre");
													tempChartConfig["yaxis-aggre"] = tempChart.data("yaxis-aggre");
													tempChartConfig["radius-aggre"] = tempChart.data("radius-aggre");
													tempChartConfig["xaxis-label"] = tempChart.data("xaxis-label") || tempChart.data("xaxisLabel");
													tempChartConfig["yaxis-label"] = tempChart.data("yaxis-label") || tempChart.data("yaxisLabel");
													tempChartConfig["radius-label"] = tempChart.data("radius-label") || tempChart.data("radiusLabel");
													tempChartConfig["xaxis-prefix"] = tempChart.data("xaxis-prefix") || tempChart.data("xaxisPrefix");
													tempChartConfig["yaxis-prefix"] = tempChart.data("yaxis-prefix") || tempChart.data("yaxisPrefix");
													tempChartConfig["radius-prefix"] = tempChart.data("radius-prefix") || tempChart.data("radiusPrefix");
													tempChartConfig["xaxis-suffix"] = tempChart.data("xaxis-suffix") || tempChart.data("xaxisSuffix");
													tempChartConfig["yaxis-suffix"] = tempChart.data("yaxis-suffix") || tempChart.data("yaxisSuffix");
													tempChartConfig["radius-suffix"] = tempChart.data("radius-suffix") || tempChart.data("radiusSuffix");
													if (tempChartConfig.charttype === "chart-series"){
														tempChartConfig.interpolate = tempChart.data("interpolate");
														tempChartConfig.legendorder = tempChart.data("legendorder");
														tempChartConfig.runningtotal = tempChart.data("runningtotal");
													}
												} else if (tempChartConfig.charttype === "chart-multilevel" || tempChartConfig.charttype=== "chart-treemap") {													
													for ( var x = 1; x < 10; x++) {
														if (tempChart.data("level-"+ x) && tempChart.data("level-"+ x) !== null) {															
															tempChartConfig["color-"+ x] = tempChart.data("color"+ x) || tempChart.data("color-"+ x);
															tempChartConfig["aggre-"+ x] = tempChart.data("aggre-"+ x);
															tempChartConfig["name-"+ x] = tempChart.data("name-"+ x);
															tempChartConfig["level-"+ x] = tempChart.data("level-"+ x);
															tempChartConfig["level-"+ x+"-desc"] = tempChart.data("level-"+ x+"-desc");
														}
													}
													tempChartConfig.xaxis = tempChart.data("xaxis");
													tempChartConfig.yaxis = tempChart.data("yaxis");
												}
												else if (tempChartConfig.charttype === "chart-composite" || tempChartConfig.charttype === "chart-grouped-barline" || tempChartConfig.charttype === "chart-composite-brush") {
													for ( var x = 1; x < 10; x++) {
														if (tempChart.data("measure-"+ x) && tempChart.data("measure-"+ x) !== null) {
															tempChartConfig["chart-type-"+ x] = tempChart.data("chart-type-"+ x);
															tempChartConfig["color-"+ x] = tempChart.data("color-"+ x);
															tempChartConfig["measure-"+ x] = tempChart.data("measure-"+ x);
															tempChartConfig["aggre-"+ x] = tempChart.data("aggre-"+ x);
															tempChartConfig["name-"+ x] = tempChart.data("name-"+ x);
															tempChartConfig["dash-"+ x] = tempChart.data("dash-"+ x);
															tempChartConfig["symbol-"+ x] = tempChart.data("symbol-"+ x);
															tempChartConfig["symbol-size-"+ x] = tempChart.data("symbol-size-"+ x);
														}
													}
													tempChartConfig.legendhorizontal = tempChart.data("legendhorizontal");
													tempChartConfig.runningtotal = tempChart.data("runningtotal");
													tempChartConfig.showtable = tempChart.data("showtable");
													tempChartConfig.legendxposition = tempChart.data("legendxposition");
													tempChartConfig.legendyposition = tempChart.data("legendyposition");
													tempChartConfig.renderdatapoints = tempChart.data("renderdatapoints");
													tempChartConfig.renderarea = tempChart.data("renderarea");
													tempChartConfig.interpolate = tempChart.data("interpolate");
													tempChartConfig.rangebandpadding = tempChart.data("rangebandpadding");
												} else if (tempChartConfig.charttype === "chart-hierarchie" || tempChartConfig.charttype === "chart-treemap") {
													for ( var x = 1; x < 5; x++) {
														if (tempChart.data("level-"+ x)	&& tempChart.data("level-"+ x) !== null) {
															tempChartConfig["level-"+ x] = tempChart.data("level-"+ x);
														}
														if (tempChart.data("level-"+ x+ "-desc")&& tempChart.data("level-"+ x+ "-desc") !== null) {
															tempChartConfig["level-"+ x+ "-desc"] = tempChart.data("level-"+ x+ "-desc");
														}
													}
												}
											}
										} else if (currentChart.hasClass("image-div")){
											var tempWidget = currentChart.parent().prev();
											var tempChartConfig = {
													"width" : $(this).attr("class"),
													"charttype" : "imagegrid",
													"table":THIS.table,
													"alias":"#imagegrid_"+imageCounter++,
													"source" : THIS.sourceType,
													"database" : THIS.database
												};
											tempChartConfig.imageColumn = tempWidget.find("[name='columnForImageUrl']").select2("val");
											tempChartConfig.filteredColumn = tempWidget.find("[name='columnForFiltered']").select2("val");
											tempChartConfig.editableColumns = tempWidget.find("[name='configEditColumn']").select2("val");
											tempChartConfig.actionData = [];
											tempWidget.find("[name='actionPanelTable']").find("tr").each(function(){
												var action = $(this).find("[name='action']").val().trim();
												if(action)
													tempChartConfig.actionData.push({action:action,actionCallback:$(this).find("[name='actionCallback']").val(),btntype:$(this).find("[name='btnType']").val()})
											});
										} else if (currentChart.children(".fc").length > 0){
											var tempWidget = currentChart.parents(".jarviswidget:first");
											calendarCounter++;
											 tempChartConfig = {
													"charttype" : "calendar",
													"alias": "tableForCalendare_"+calendarCounter,
													"source" : THIS.sourceType,
													"database" : THIS.database
												};
											if (tempWidget.find("[name=eventid]") !== "")
												tempChartConfig.eventid = tempWidget.find("[name=eventid]").val();
											if (tempWidget.find("[name=eventname]") !== "")
												tempChartConfig.eventname = tempWidget.find("[name=eventname]").val();
											if (tempWidget.find("[name=eventstart]") !== "")
												tempChartConfig.eventstart = tempWidget.find("[name=eventstart]").val();
											if (tempWidget.find("[name=eventend]") !== "")
												tempChartConfig.eventend = tempWidget.find("[name=eventend]").val();
											if (tempWidget.find("[name=eventcolor]") !== "")
												tempChartConfig.eventcolor = tempWidget.find("[name=eventcolor]").val();
											if (tempWidget.find("[name=eventclickcallback]") !== "")
												tempChartConfig.eventclickcallback = tempWidget.find("[name=eventclickcallback]").val();
											if (tempWidget.find("[name=chartwidth]") !== "")
												tempChartConfig.chartwidth = tempWidget.find("[name=chartwidth]").val();
											if (tempWidget.find("[name=chartheight]") !== "")
												tempChartConfig.chartheight = tempWidget.find("[name=chartheight]").val();
											tempChartConfig.defaultdate =  tempWidget.find("[name=defaultdate]").val();
											tempChartConfig.table = $("#tableForCalendar_"+calendarCounter).val();
										} else if (currentChart.children(".ngram-div").length > 0){
											var tempWidget = currentChart.parents(".jarviswidget:first");
											NGramCounter++;
											tempChartConfig = {
												"charttype" : "NGram",
												"alias" : "tableForNGram",
												"source" : THIS.sourceType,
												"database" : THIS.database
											};
											if (tempWidget.find("[name=tableNGram]") !== "")
												tempChartConfig.tableNGram = tempWidget.find("[name=tableNGram]").select2("val");
											if (tempWidget.find("[name=field]") !== "")
												tempChartConfig.tableField = tempWidget.find("[name=field]").select2("val");
											if (tempWidget.find("[name=gramType]") !== "")
												tempChartConfig.ngramType = tempWidget.find("[name=gramType]").select2("val");
											if (tempWidget.find("[name=ngramSynonyms]") !== "")
												tempChartConfig.ngramSynonyms = tempWidget.find("[name=ngramSynonyms]").is(":checked");
											if (tempWidget.find("[name=ngramStopwords]") !== "")
												tempChartConfig.ngramStopwords = tempWidget.find("[name=ngramStopwords]").is(":checked");
											if (tempWidget.find("[name=ngramLemmatization]") !== "")
												tempChartConfig.ngramLemmatization = tempWidget.find("[name=ngramLemmatization]").is(":checked");
											if (tempWidget.find("[name=ngramMoreFilters]") !== "")
												tempChartConfig.ngramMoreFilters = tempWidget.find("[name=ngramMoreFilters]").is(":checked");

										} else if (currentChart.children(".IFrame-div").length > 0){
											var tempWidget = currentChart.parents(".jarviswidget:first");
											IFrameCounter++;
											tempChartConfig = {
												"charttype" : "IFrame",
												"alias" : "tableForIFrame_"+IFrameCounter
											};
											if (tempWidget.find("[name=IFrameURL]") !== "")
												tempChartConfig.IFrameURL = tempWidget.find("[name=IFrameURL]").val();
										}else if (currentChart.children(".HTMLTemplate-div").length > 0){
											var tempWidget = currentChart.parents(".jarviswidget:first");
											HTMLTemplateCounter++;
											tempChartConfig = {
												"charttype" : "HTMLTemplate",
												"alias" : "tableForHTMLTemplate_"+HTMLTemplateCounter,
												"id" : HTMLTemplateCounter
											};
											if (CKEDITOR.instances['HTMLTemplateContent_'+HTMLTemplateCounter].getData() !== "")
												tempChartConfig.HTMLTemplateContent = CKEDITOR.instances['HTMLTemplateContent_'+HTMLTemplateCounter].getData();
											if (tempWidget.find("[name=contentLayout]") !== "")
												tempChartConfig.contentLayout = $("[name=contentLayout]",$("#wid-id-HTMLTemplate_"+ HTMLTemplateCounter)).is(":checked");
										} else {
											tableCounter++;
											tempChartConfig = {
												"charttype" : "datatable"
											};
											
											var columns = "";
											var columnObject = [];
											var drilldowncallback = $("[name=drilldowncallback]",$("#wid-id-datatable_"+ tableCounter)).data("drilldowncallback");
											var orderBy = $("[name=tableorderby]",$("#wid-id-datatable_"+ tableCounter)).data("tableorderby");
											var ordertype = $("[name=tableordertype]",$("#wid-id-datatable_"+ tableCounter)).data("tableordertype");
											var serverType = $("[name=servertype]",$("#wid-id-datatable_"+ tableCounter)).val();
											
											var actionDisplayType = $("[name=actiondisplaytype]",$("#wid-id-datatable_"+ tableCounter)).data("actiondisplaytype");
											var actionColorCode = $("[name=actioncolorcode]",$("#wid-id-datatable_"+ tableCounter)).data("actioncolorcode");
											var defaulttablefilter = $("[name=defaulttablefilter]",$("#wid-id-datatable_"+ tableCounter)).data("defaulttablefilter");
											var formTitle = $("[name=formTitle]",$("#wid-id-datatable_"+ tableCounter)).data("formTitle");
											var overrideFunction = $("[name=overrideFunction]",$("#wid-id-datatable_"+ tableCounter)).data("overrideFunction");
											var widthTable = $("#wid-id-datatable_"	+ tableCounter+ " .select-width >select").val();
											if (widthTable === 0)
												widthTable = "col-md-12 col-xs-12 col-sm-12 col-lg-12";
											var groupByEnabled = false;
											var sumamryEnabled = false; 
											$("th","#wid-id-datatable_"+ tableCounter).each(function(index, ele) { 
														var column = {"data" : $(ele).find("a").data("field"),"defaultContent" : ""};
																if ($(ele).find("a").data("displayname")&& $.trim($(ele).find("a").data("displayname")) !== "")
																	column.title = $(ele).find("a").data("displayname");
																else
																	column.title = $(ele).find("a").data("field");
																
																if ($(ele).find("a").data("class")&& $.trim($(ele).find("a").data("class")) !== "")
																	column["class"] = $(ele).find("a").data("class");
																
																if ($(ele).find("a").data("groupbyaggregate")&& $.trim($(ele).find("a").data("groupbyaggregate")) !== "" && $.trim($(ele).find("a").data("groupbyaggregate")) !== "none"){
																	groupByEnabled = true;
																	column["groupbyaggregate"] = $(ele).find("a").data("groupbyaggregate");
																}
																if ($(ele).find("a").data("sourceconfig")&& $.trim($(ele).find("a").data("sourceconfig")) !== ""){
																	
																	column["sourceconfig"] = $(ele).find("a").data("sourceconfig");
																}
																if ($(ele).find("a").data("databaseconfig")&& $.trim($(ele).find("a").data("databaseconfig")) !== ""){
																	
																	column["databaseconfig"] = $(ele).find("a").data("databaseconfig");
																}
																if ($(ele).find("a").data("tabledatabase")&& $.trim($(ele).find("a").data("tabledatabase")) !== ""){
																	
																	column["tabledatabase"] = $(ele).find("a").data("tabledatabase");
																}
																
																if ($(ele).find("a").data("datatype")&& $.trim($(ele).find("a").data("datatype")) !== ""){
																	column["datatype"] = $(ele).find("a").data("datatype");
																}
																
																if ($(ele).find("a").data("summaryaggregate")&& $.trim($(ele).find("a").data("summaryaggregate")) !== "" && $.trim($(ele).find("a").data("summaryaggregate")) !== "NA" && $.trim($(ele).find("a").data("summaryaggregate")) !== "none"){
																	sumamryEnabled = true;
																	column["summaryaggregate"] = $(ele).find("a").data("summaryaggregate");
																}
																if($(ele).find("a").data("summaryaggregate") && $.trim($(ele).find("a").data("summaryaggregate")) == "NA"){
																	column['summaryName'] = "NA"
																}
																if($(ele).find("a").data("hidecoloumn") && $(ele).find("a").data("hidecoloumn") === true){
																	column.hidecoloumn = true;
																}
																if($(ele).find("a").data("editable") && $(ele).find("a").data("editable") === true){
																	column.editable = true;
																}
																if($(ele).find("a").data("mandatory") && $(ele).find("a").data("mandatory") === true){
																	column.mandatory = true;
																}
																if ($(ele).find("a").data("renderfunction")&& $.trim($(ele).find("a").data("renderfunction")) !== "") {
																	column.renderfunction = $(ele).find("a").data("renderfunction");// .replace(/\\/g,
																																	// "\\\\\\\\");
																}
																if ($(ele).find("a").data("typeoptions")&& $.trim($(ele).find("a").data("typeoptions")) !== "") {
																	column.typeoptions = $(ele).find("a").data("typeoptions");
																}
																if ($(ele).find("a").data("edittype")&& $.trim($(ele).find("a").data("edittype")) !== "") {
																	column.edittype = $(ele).find("a").data("edittype");
																}
																if ($(ele).find("a").data("editvalue")&& $.trim($(ele).find("a").data("editvalue")) !== "") {
																	column.editvalue = $(ele).find("a").data("editvalue");
																}
																if ($(ele).find("a").data("colwidth")&& $.trim($(ele).find("a").data("colwidth")) !== "") {
																	column.colwidth = $(ele).find("a").data("colwidth");
																}
																if ($(ele).find("a").data("primarykey")&& $.trim($(ele).find("a").data("primarykey")) !== "") {
																	column.primarykey = $(ele).find("a").data("primarykey");
																}
																if ($(ele).find("a").data("wraptext")&& $.trim($(ele).find("a").data("wraptext")) !== "") {
																	column.wraptext = $(ele).find("a").data("wraptext");
																}
																if ($(ele).find("a").data("enabledetail")&& $.trim($(ele).find("a").data("enabledetail")) !== "") {
																	column.enabledetail = $(ele).find("a").data("enabledetail");
																}
																if ($(ele).find("a").data("columndesc")&& $.trim($(ele).find("a").data("columndesc")) !== "") {
																	column.columndesc = $(ele).find("a").data("columndesc");
																}
																if($(ele).find("a").data("columninfo") && $(ele).find("a").data("columninfo") === true){
																	column.columninfo = true;
																}
																if($(ele).find("a").data("disable") && $(ele).find("a").data("disable") === true){
																	column.disable = true;
																}
																columnObject.push(column);
																if (columns === "")
																	columns += $(ele).find("div > a").data("field");
																else
																	columns += ","+ $(ele).find("div > a").data("field");
															});
											
											var actionData = [];
											$("#actionPanelForm",$("#wid-id-datatable_"+ tableCounter)).children().each(function(index,item){
												if($(item).find('[name=action_'+tableCounter+'_'+index+']').val() && $(item).find('[name=actionCallback_'+tableCounter+'_'+index+']').val()){
													var obj = {};
													obj.action = $(item).find('[name=action_'+tableCounter+'_'+index+']').val();
													obj.actionCallback = $(item).find('[name=actionCallback_'+tableCounter+'_'+index+']').val();
													actionData.push(obj);
												}
											});	
											// Store Pivot Settings in a
											// separate Object.
											var pivotSettings = $("#wid-id-datatable_"+ tableCounter).data('pivotSettings');
											var workflow = $("#wid-id-datatable_"+ tableCounter).data('workflow');
											var rule = $("#wid-id-datatable_"+ tableCounter).data('rule');
											var history; 
											if($("#wid-id-datatable_"+ tableCounter).find("[name='enableHistory']").is(":checked")){
												history = {};
												history.table = $("#wid-id-datatable_"+ tableCounter).find("[name='tableHistory']").select2("val");
												history.foregincolumn = $("#wid-id-datatable_"+ tableCounter).find("[name='foreginkey']").select2("val");
												history.versionColumn = $("#wid-id-datatable_"+ tableCounter).find("[name='versionColumn']").select2("val");
												history.statusColumn = $("#wid-id-datatable_"+ tableCounter).find("[name='statusColumn']").select2("val");
												history.primarykey = $("#wid-id-datatable_"+ tableCounter).find("[name='primarykey']").select2("val");
												history.tableHistoryAlias = $("#wid-id-datatable_"+ tableCounter).find("[name='tableHistoryAlias']").val();
												history.tableMasterAlias = $("#wid-id-datatable_"+ tableCounter).find("[name='tableMasterAlias']").val();
												history.msourcelink = $("#wid-id-datatable_"+ tableCounter).find("[name='msourcelink']").val();
												history.hsourcelink = $("#wid-id-datatable_"+ tableCounter).find("[name='hsourcelink']").val();
												history.mappingcolumns = {};
												var orderindex = 0;
												$("#wid-id-datatable_"+ tableCounter).find("[name='historyColumnsMapping']").find("section").each(function(){ 
													var column = $(this).data("column");
													var columnMappedValue = $(this).find("[name='mappedColumn']").select2("val");
													if(column && columnMappedValue){
														history.mappingcolumns[column] = {
															column:	$(this).find("[name='mappedColumn']").select2("val"),
															label:$(this).find("[name='mappedLabel']").val(),
															renderFunction:$(this).find("[name='renderFunction']").val(),
															orderby:orderindex++
														};
													}
												});
											}
											var data = {
												"source" : THIS.sourceType,
												"database" : THIS.database,
												"table" : $("#tableForDatatable_"+ tableCounter).attr("data-tablename"),
												"columns" : columns,
												"orderby" : orderBy,
												"ordertype" : ordertype,
												"serverType" : serverType,
												"isSelectAll": $("[name=isSelectAll]",$("#wid-id-datatable_"+ tableCounter)).is(":checked"),
												"columninfo":$("[name=columninfo]",$("#wid-id-datatable_"+ tableCounter)).is(":checked"),
												"actionData": actionData,
												"actionDisplayType":actionDisplayType,
												"actionColorCode" : actionColorCode,
												"filter" : defaulttablefilter,
												"drilldowncallback" : drilldowncallback,
												"summaryenabled": false,
												"pivotSettings" : pivotSettings,
												"workflow":workflow,
												"rule":rule,
												"formName" : formTitle,
												"overrideFunction":overrideFunction,
											};
											if(history){
												data.history = history;
											}
											if($("#tableForDatatable_"+ tableCounter).attr("data-tablename")){
												data.table = $("#tableForDatatable_"+ tableCounter).attr("data-tablename");
											}
											if (groupByEnabled === true || groupByEnabled === "true"){
												data.groupbyenabled = true;
											}
											if(sumamryEnabled){
												data.summaryenabled = true;
											}
											tempChartConfig.pageLength = $("[name=DataTables_Table_0_length]",$("#wid-id-datatable_"+ tableCounter)).val();
											tempChartConfig.isEditable = $("[name=isEditable]",$("#wid-id-datatable_"+ tableCounter)).is(":checked");
											tempChartConfig.enableAudit = $("[name=enableAudit]",$("#wid-id-datatable_"+ tableCounter)).is(":checked");
											tempChartConfig.enableAdd = $("[name=enableAdd]",$("#wid-id-datatable_"+ tableCounter)).is(":checked");
											tempChartConfig.enableHistory = $("[name=enableHistory]",$("#wid-id-datatable_"+ tableCounter)).is(":checked");
											tempChartConfig.showAudit = $("[name=showAudit]",$("#wid-id-datatable_"+ tableCounter)).is(":checked");
											tempChartConfig.editType = $("[name=editType]",$("#wid-id-datatable_"+ tableCounter)).val();
											tempChartConfig.editvalue = $("[name=editvalue]",$("#wid-id-datatable_"+ tableCounter)).val();
											tempChartConfig.colwidth = $("[name=colwidth]",$("#wid-id-datatable_"+ tableCounter)).val();
											tempChartConfig.fixedLeftColumn = $("[name=fixedLeftColumn]",$("#wid-id-datatable_"+ tableCounter)).val();
											tempChartConfig.fixedRightColumn = $("[name=fixedRightColumn]",$("#wid-id-datatable_"+ tableCounter)).val();
											tempChartConfig.enableBulkUpdate = $("[name=enableBulkUpdate]",$("#wid-id-datatable_"+ tableCounter)).is(":checked");
											tempChartConfig.enableHighlighter = $("[name=enableHighlighter]",$("#wid-id-datatable_"+ tableCounter)).is(":checked");
											tempChartConfig.enableComments = $("[name=enableComments]",$("#wid-id-datatable_"+ tableCounter)).is(":checked");
											tempChartConfig.hideNoOfRows = $("[name=hideNoOfRows]",$("#wid-id-datatable_"+ tableCounter)).is(":checked");
											tempChartConfig.hideShowHide = $("[name=hideShowHide]",$("#wid-id-datatable_"+ tableCounter)).is(":checked");
											tempChartConfig.hidePagination = $("[name=hidePagination]",$("#wid-id-datatable_"+ tableCounter)).is(":checked");
											tempChartConfig.hideSearch = $("[name=hideSearch]",$("#wid-id-datatable_"+ tableCounter)).is(":checked");
											tempChartConfig.hideFilter = $("[name=hideFilter]",$("#wid-id-datatable_"+ tableCounter)).is(":checked");
											tempChartConfig.isOverride = $("[name=isOverride]",$("#wid-id-datatable_"+ tableCounter)).is(":checked");
											tempChartConfig.commentSectionName = $("[name=commentSectionName]",$("#wid-id-datatable_"+ tableCounter)).data("commentSectionName");
											tempChartConfig.data = data;
											tempChartConfig.columns = columnObject;
											tempChartConfig.filtermapping = $("#wid-id-datatable_"+ tableCounter).data("filtermapping");
											tempChartConfig.alias = "#datatable_" + tableCounter;
											tempChartConfig.width = widthTable;
												if(tempChartConfig.enableComments && !tempChartConfig.commentSectionName ){
													tempChartConfig.commentSectionName = commentSectionNameHeader;										
												}
											}
										chartsConfig.push(tempChartConfig);
									});
						
							tempConfig.charts = chartsConfig;
							currentObjConfig.push(tempConfig);
						});
		return currentObjConfig;
	};
	var showFullScreen = function() {
		var options = {
			"width" : "col-md-12 col-xs-12 col-sm-12 col-lg-12 no-padding",
			"widget-class" : "jarviswidget jarviswidget-color-light",
			"id" : "widget-fullscreen",
			"istemplate" : false,
			"fullscreen" : true,
			"icon" : "fa fa-fw fa-bar-chart-o",
			"header" : THIS.ProfileName
		};
		var drawFullScreen = function() {
			var showFullScreenDiv = templates.getWidget.template(options);
			$("#widget-grid").append(showFullScreenDiv);
			return $(showFullScreenDiv);
		};
		if (templates.getWidget.template
				&& templates.getWidget.template !== null) {
			drawFullScreen();
		} else {
			templates.getWidget(options, drawFullScreen);
		}
	};
	var drawProfile = function(config, showFullScreenDiv,profileId,profiletype) {
		if (!config || config.length === 0) {
			return;
		}
		var count = 0,
		classObj = {
				3:"col-md-3",
				6:"col-md-6",
				9:"col-md-9",
				12:"col-md-12"
			};
		for ( var i = 0; i < config.length; i++) {
			if(localStorage["isMultipleProfile"]){
				config[i].id = config[i].id+THIS.profileId;
			}else if(profileId){
				config[i].id = config[i].id+profileId;
			}
			var tempoptions = {
				"width" : config[i].width,
				"widget-class" :config[i]["class"],
				"id" : config[i].id,
				"istemplate" : false,
				"fullscreen" : false,
				"tooltip": config[i].tooltip,
				"icon" : config[i].icon,
				"header" : config[i].header
			};

			if(System.EnabledWidget && System.EnabledWidget.data != ""){
				widgetList = []
				System.EnabledWidget.data.forEach(function(data){
					widgetList = widgetList.concat(data.widget.split(','))
				})
				var temptempoptions = {}
				widgetList.forEach(function(widgetId){
					if(widgetId == tempoptions.id){
						temptempoptions = tempoptions
					}
				})
				if(!jQuery.isEmptyObject(temptempoptions)){
					tempoptions = temptempoptions
				}else{
					continue;
				}
			}
			var drawProfileWidget = function() { 
				var tempWidget = templates.getWidget.template(tempoptions);
				  if(THIS.factsConfig.length >1 && i===0 && $("#ddFacts").length ===0){
					  $("#actionDiv").append($("<div/>").addClass("pull-left").append($("<span/>").attr("id","measureDisplayNameDiv").addClass("font-bold txt-color-blueDark hidden-mobile hidden-tablet")).append($('<input/>').addClass("measure-dropdown").attr({"type":"text","name":"ddFacts","id":"ddFacts" }))); 
				  }
				  for (var key in classObj) {
					  if($(tempWidget).hasClass(classObj[key])){
						  count += parseInt(key);
						  break;
					  }
				  }
				  $($(".widget-body",showFullScreenDiv)[0]).append(tempWidget).addClass('padding-left-5');
				  if(tempoptions.tooltip && $.trim(tempoptions.tooltip)){
					  var attribute = {
							"data-content":tempoptions.tooltip,
							"data-toggle":"popover",
							"data-html":"true"
					  };
					  var headerContent = $("#"+tempoptions.id).find("header").find("h2").html();
					  $("#"+tempoptions.id).find("header").find("span").html($("<i/>").css({"font-size":"20px","color":"#444"}).attr(attribute).addClass("fa fa-info-circle fa-lg iconhover"));
					  $("#"+tempoptions.id).find("header").find("span").find("i").popover({
				  		  placement: 'auto',
				  		  container: 'body',
				  		  width:'300px',
				  		  delay :  { "show": 100, "hide": 100 },
				  		  trigger : 'hover'
			  	  	});
				  }
				  if(count%12 !== 0){ 
					  $("#"+config[i].id).parent().addClass("no-padding-right");
				  }
				if (config[i].charts && config[i].charts.length > 0) {
					for ( var x = 0; x < config[i].charts.length; x++) {
						if(localStorage["isMultipleProfile"]){
							config[i].charts[x].alias = config[i].charts[x].alias+THIS.profileId;
						}else if(profileId){
							config[i].charts[x].alias = config[i].charts[x].alias+profileId;
						}
						var widget = showFullScreenDiv.find("#"+ config[i].id);
						if (config[i].charts[x].charttype === "datatable") {
							var actionColorCode = config[i].charts[x].data.actionColorCode || "btn-success";
							if (config[i].charts[x].data && config[i].charts[x].data.actionData && config[i].charts[x].data.actionData.length > 0){
								if(config[i].charts[x].data.actionDisplayType && config[i].charts[x].data.actionDisplayType!==undefined && config[i].charts[x].data.actionDisplayType === "dropdown"){
									$("header", $(widget)).append('<div class="btn-group floatRight" id="actionPlaceHolder"><button type="button" class="btn dropdown-toggle width115 '+actionColorCode+'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Action<span class="caret"></span> </button> <ul class="dropdown-menu" id="actionDropDown"></ul></div>');
								}else{
									$("header", $(widget)).append('<div class="btn-group floatRight" id="actionPlaceHolder"></div>');
								}
								if(config[i].charts[x].data.actionDisplayType !== "dropdown" && config[i].charts[x].enableComments === true ){
									if(config[i].charts[x].data && config[i].charts[x].data.actionData && !(config[i].charts[x].data.actionData.length > 0))
									{
										$("header", $(widget)).append('<div class="btn-group floatRight" id="actionPlaceHolder"></div>');	
									}
									$("#actionPlaceHolder", $(widget)).append('<span class="comment_widget" id="'+config[i].id+'commentsPopup" > <i class=\'fa fa-comments\'></i><span class="unread_span">1<span> </span>');
									$("#"+config[i].id+"commentsPopup").unbind("click").bind("click",{"configId": config[i].charts[0].commentSectionName, "configIDval": config[i].id},function(event){
				                		actionCallbackHandler.commentsPopup(event.data.configId, event.data.configIDval);
			                		});
								}
								for(var k=0; k<config[i].charts[x].data.actionData.length; k++){
									var actionCallback  = config[i].charts[x].data.actionData[k].actionCallback;
									actionCallback = actionCallback.indexOf('(') === -1? actionCallback+'()':actionCallback;
									actionCallback = actionCallback.replace(/@/g,"'");
									if(config[i].charts[x].data.actionDisplayType && config[i].charts[x].data.actionDisplayType!==undefined && config[i].charts[x].data.actionDisplayType === "dropdown"){
										if(actionCallback === "downloadExcel()" ){
											$("#actionDropDown", $(widget)).append('<li><a id="downloadExcel" title="Download data in XLS format" href="javascript:void(0);" data-datatable="'+ config[i].charts[x].alias +'">'+config[i].charts[x].data.actionData[k].action +'</a></li>');
										}else if(actionCallback === "downloadCSV()"){
											$("#actionDropDown", $(widget)).append('<li><a id="downloadCSV" title="Download data in CSV format" href="javascript:void(0);" data-datatable="'+ config[i].charts[x].alias +'">'+config[i].charts[x].data.actionData[k].action+'</a></li>');
										}else if(actionCallback.indexOf("actionCallbackHandler.importExcel")>-1){
											var button = $("<a/>").attr({"href":"javascript:void(0)","onClick":"javascript:"+actionCallback}).html(config[i].charts[x].data.actionData[k].action);
											if(config[i].charts[x].isEditable){
												button.data("isEditable",true);
											}
											button.data("columns",config[i].charts[x].columns);
											// $("#actionDropDown",
											// $(widget)).append('<li><a
											// href="javascript:void(0);"
											// data-columns="'+config[i].charts[x].columns+'"
											// onClick="javascript:'+actionCallback+'">'+config[i].charts[x].data.actionData[k].action+'</a></li>');
											$("#actionDropDown", $(widget)).append($('<li/>').append(button));
										}else{
											$("#actionDropDown", $(widget)).append('<li><a href="javascript:void(0);" onClick="javascript:'+actionCallback+'">'+config[i].charts[x].data.actionData[k].action+'</a></li>');
										}
										
									}else{
										if(actionCallback === "downloadExcel()" ){
											var actionHeader = (config[i].charts[x].data.actionData[k].action && config[i].charts[x].data.actionData[k].action.toLowerCase() === 'excel') ? config[i].charts[x].data.actionData[k].action : config[i].header;
											$("#actionPlaceHolder", $(widget)).append('<button type="button" id="downloadExcel" title="Download data in XLS format" class="btn marginRight10 '+actionColorCode+'" data-header="'+actionHeader+'" data-datatable="'+ config[i].charts[x].alias +'">'+ config[i].charts[x].data.actionData[k].action+'</button>'); 
										}else if(actionCallback === "downloadCSV()"){
											var actionHeader = (config[i].charts[x].data.actionData[k].action && config[i].charts[x].data.actionData[k].action.toLowerCase() === 'csv') ? config[i].charts[x].data.actionData[k].action : config[i].header;
											$("#actionPlaceHolder", $(widget)).append('<button type="button" id="downloadCSV" title="Download data in CSV format" class="btn  marginRight10 '+actionColorCode+'" data-header="'+actionHeader+'" data-datatable="'+ config[i].charts[x].alias +'">'+ config[i].charts[x].data.actionData[k].action+'</button>');
										}else if(actionCallback.indexOf("actionCallbackHandler.importExcel")>-1){
											var button = $("<a/>").attr({"href":"javascript:void(0)","onClick":"javascript:"+actionCallback}).addClass("btn  marginRight10 "+actionColorCode).html(config[i].charts[x].data.actionData[k].action);
											if(config[i].charts[x].isEditable){
												button.data("isEditable",true);
											}
											button.data("columns",config[i].charts[x].columns);
											// $("#actionPlaceHolder",
											// $(widget)).append('<button
											// type="button" class="btn
											// marginRight10
											// '+actionColorCode+'"
											// data-columns="'+config[i].charts[x].columns+'"
											// onClick="javascript:'+actionCallback+'">'+config[i].charts[x].data.actionData[k].action+'</button>');
											$("#actionPlaceHolder", $(widget)).append(button);
										}else{
											$("#actionPlaceHolder", $(widget)).append('<button type="button" class="btn  marginRight10 '+actionColorCode+'" onClick="javascript:'+actionCallback+'">'+config[i].charts[x].data.actionData[k].action+'</button>');
										}
									}
								}
							}
							if(config[i].charts[x].data.actionDisplayType !== "dropdown" && config[i].charts[x].enableBulkUpdate === true ){
								if(config[i].charts[x].data && config[i].charts[x].data.actionData && !(config[i].charts[x].data.actionData.length > 0))
								{
									$("header", $(widget)).append('<div class="btn-group floatRight" id="actionPlaceHolder"></div>');	
								}
								$("#actionPlaceHolder", $(widget)).append('<button id="submitAll" type="button" class="btn  marginRight10 btn-success" > <i class=\'fa fa-download\'></i> Submit All</button>');
							}

							$(".widget-body", $(widget)).append($("<div/>").append($("<table/>").addClass("table table-striped table-hover dc-data-table smart-form width-100per").attr({"id" : config[i].charts[x].alias.replace('#','')})));
							if(!config[i].charts[x].data.table && THIS.dataQuery){
								config[i].charts[x].data.dataQuery = THIS.dataQuery;
							}
							$("#downloadExcel", $(widget)).unbind("click").bind("click", {"tableconfig" : config[i].charts[x],"type":"excel"}, downloadExcel);
							$("#downloadCSV", $(widget)).unbind("click").bind("click", {"tableconfig" : config[i].charts[x],"type":"csv"}, downloadExcel);
							$("#submitAll", $(widget)).unbind("click").bind("click",config[i],function(event) {
								actionCallbackHandler.saveData(event.data);
							});
							if(localStorage["isMultipleProfile"]){
								$(config[i].charts[x].alias).data("profileid",THIS.profileId);
							}else if(profileId){
								$(config[i].charts[x].alias).data("profileid",profileId);
							}
						}
						else if (config[i].charts[x].charttype === "imagegrid") {
							var editColumnSection = $("<div/>").attr({"name":"editableColumsSection"}).css({"border":"1px solid #dddddd"}).addClass("smart-form row margin-8");
							$(".widget-body", $(widget))
							.append(editColumnSection)
							.append($("<div/>").addClass("row").css({"max-height":"500px","overflow":"auto"}).attr({"id" : config[i].charts[x].alias.replace('#','')}))
							.append($("<div/>").addClass("widget-footer").css({"height":"100px"}).append($("<div/>").addClass("pull-left dataTables_info").attr({"name":"showcountdiv"}))
									.append($("<div/>").addClass("pull-right").attr({"name":"pagingdiv"})));
							$(widget).find("header")
							.append( $("<div/>").addClass("circle-tile").append($("<label/>").addClass("select")
									.append($("<select/>").attr({"name":"pagedropdown"}).addClass("input-sm")
											.append( $("<option/>").attr({"value":"50"}).html("50"))
											.append( $("<option/>").attr({"value":"100"}).html("100"))
											.append( $("<option/>").attr({"value":"250"}).html("250"))
											.append( $("<option/>").attr({"value":"500"}).html("500")))));
							$(widget).find("header").find(".widget-icon").addClass("smart-form").html($("<label/>").addClass("checkbox col-sm-1 margin-2")
									.append( $("<input/>").attr({"name":"selectAllCheckbox","type":"checkbox"}))
									.append($("<i/>")));
							$(widget).find("header").find("h2").html("Select All");
							if(config[i].charts[x].editableColumns && config[i].charts[x].editableColumns.length>0){
								for(var ec=0;ec<config[i].charts[x].editableColumns.length;ec++){
									var dropDownDiv = $("<div/>").addClass("width-100per");
									var lookupquery = "select distinct("+config[i].charts[x].editableColumns[ec]+"),'' from "+config[i].charts[x].table+" where lower("+config[i].charts[x].editableColumns[ec]+") like '%<Q>%'";
									editColumnSection.append( $("<section/>").addClass("col col-4 col-sm-4")
											.append( $("<div/>").addClass("note").append($("<strong/>").html(config[i].charts[x].editableColumns[ec])))
											.append($("<label/>").addClass("input width-100per").append(dropDownDiv)));
									constructDropDown(dropDownDiv,lookupquery,false,null,null,"Select "+ config[i].charts[x].editableColumns[ec],config[i].charts[x].source,config[i].charts[x].database);
								}
							}
							if(config[i].charts[x].actionDisplayType && config[i].charts[x].actionDisplayType === "dropdown"){
								$("header", $(widget)).find(".circle-tile").append('<div class="btn-group floatRight"><button type="button" class="btn dropdown-toggle width115" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Action<span class="caret"></span> </button> <ul class="dropdown-menu" name="actionDropDown"></ul></div>');
							}else{
								$("header", $(widget)).find(".circle-tile").append('<div class="btn-group floatRight" name="actionPlaceHolder"></div>');
							}
							if (config[i].charts[x].data && config[i].charts[x].data.actionData && config[i].charts[x].data.actionData.length > 0){
								for(var k=0; k<config[i].charts[x].actionData.length; k++){
									var actionCallback  = config[i].charts[x].actionData[k].actionCallback;
									actionCallback = actionCallback.indexOf('(') === -1? actionCallback+'()':actionCallback;
									actionCallback = actionCallback.replace(/@/g,"'");
									if(config[i].charts[x].actionDisplayType && config[i].charts[x].actionDisplayType === "dropdown"){
										$("[name='actionDropDown']", $(widget)).append('<li><a href="javascript:void(0);" onClick="javascript:'+actionCallback+'">'+config[i].charts[x].actionData[k].action+'</a></li>');
										/*
										 * if(actionCallback ===
										 * "downloadExcel()" ){
										 * $("#actionDropDown",
										 * $(widget)).append('<li><a
										 * id="downloadExcel" title="Download
										 * data in XLS format"
										 * href="javascript:void(0);"
										 * data-datatable="'+
										 * config[i].charts[x].alias
										 * +'">'+config[i].charts[x].data.actionData[k].action +'</a></li>');
										 * }else if(actionCallback ===
										 * "downloadCSV()"){
										 * $("#actionDropDown",
										 * $(widget)).append('<li><a
										 * id="downloadCSV" title="Download data
										 * in CSV format"
										 * href="javascript:void(0);"
										 * data-datatable="'+
										 * config[i].charts[x].alias
										 * +'">'+config[i].charts[x].data.actionData[k].action+'</a></li>');
										 * }else
										 * if(actionCallback.indexOf("actionCallbackHandler.importExcel")>-1){
										 * var button = $("<a/>").attr({"href":"javascript:void(0)","onClick":"javascript:"+actionCallback}).html(config[i].charts[x].data.actionData[k].action);
										 * button.data("columns",config[i].charts[x].columns);
										 * //$("#actionDropDown",
										 * $(widget)).append('<li><a
										 * href="javascript:void(0);"
										 * data-columns="'+config[i].charts[x].columns+'"
										 * onClick="javascript:'+actionCallback+'">'+config[i].charts[x].data.actionData[k].action+'</a></li>');
										 * $("#actionDropDown",
										 * $(widget)).append($('<li/>').append(button));
										 * }else{ $("#actionDropDown",
										 * $(widget)).append('<li><a
										 * href="javascript:void(0);"
										 * onClick="javascript:'+actionCallback+'">'+config[i].charts[x].data.actionData[k].action+'</a></li>'); }
										 */
										
									}else{
										$("[name='actionPlaceHolder']", $(widget)).append('<button type="button" class="btn  marginRight10 '+config[i].charts[x].actionData[k].btntype+'" onClick="javascript:'+actionCallback+'">'+config[i].charts[x].actionData[k].action+'</button>');
										/*
										 * if(actionCallback ===
										 * "downloadExcel()" ){ var actionHeader =
										 * (config[i].charts[x].data.actionData[k].action &&
										 * config[i].charts[x].data.actionData[k].action.toLowerCase()
										 * === 'excel') ?
										 * config[i].charts[x].data.actionData[k].action :
										 * config[i].header;
										 * $("#actionPlaceHolder",
										 * $(widget)).append('<button
										 * type="button" id="downloadExcel"
										 * title="Download data in XLS format"
										 * class="btn marginRight10
										 * '+actionColorCode+'"
										 * data-header="'+actionHeader+'"
										 * data-datatable="'+
										 * config[i].charts[x].alias +'">'+
										 * config[i].charts[x].data.actionData[k].action+'</button>');
										 * }else if(actionCallback ===
										 * "downloadCSV()"){ var actionHeader =
										 * (config[i].charts[x].data.actionData[k].action &&
										 * config[i].charts[x].data.actionData[k].action.toLowerCase()
										 * === 'csv') ?
										 * config[i].charts[x].data.actionData[k].action :
										 * config[i].header;
										 * $("#actionPlaceHolder",
										 * $(widget)).append('<button
										 * type="button" id="downloadCSV"
										 * title="Download data in CSV format"
										 * class="btn marginRight10
										 * '+actionColorCode+'"
										 * data-header="'+actionHeader+'"
										 * data-datatable="'+
										 * config[i].charts[x].alias +'">'+
										 * config[i].charts[x].data.actionData[k].action+'</button>');
										 * }else
										 * if(actionCallback.indexOf("actionCallbackHandler.importExcel")>-1){
										 * var button = $("<a/>").attr({"href":"javascript:void(0)","onClick":"javascript:"+actionCallback}).addClass("btn
										 * marginRight10
										 * "+actionColorCode).html(config[i].charts[x].data.actionData[k].action);
										 * button.data("columns",config[i].charts[x].columns);
										 * //$("#actionPlaceHolder",
										 * $(widget)).append('<button
										 * type="button" class="btn
										 * marginRight10 '+actionColorCode+'"
										 * data-columns="'+config[i].charts[x].columns+'"
										 * onClick="javascript:'+actionCallback+'">'+config[i].charts[x].data.actionData[k].action+'</button>');
										 * $("#actionPlaceHolder",
										 * $(widget)).append(button); }else{
										 * $("#actionPlaceHolder",
										 * $(widget)).append('<button
										 * type="button" class="btn
										 * marginRight10 '+actionColorCode+'"
										 * onClick="javascript:'+actionCallback+'">'+config[i].charts[x].data.actionData[k].action+'</button>'); }
										 */
									}
								}
							}
							
						}
				else if (config[i].charts[x].charttype === "calendar") {
					$(".widget-body", $(widget))
							.append($("<div/>").addClass("row")
							.append($("<div/>").attr({"id" : config[i].charts[x].alias.replace('#','')})));
					$("#configure_"+THIS.dataTableCounter).unbind("click").bind("click",function(){
						configureColumns();
					});
				}else if (config[i].charts[x].charttype === "chart-line") {
							$(".widget-body", $(widget))
									.append($("<div/>").addClass(config[i].charts[x].xaxisorientation ? config[i].charts[x].xaxisorientation: '')
													.addClass(config[i].charts[x].yaxisorientation ? config[i].charts[x].yaxisorientation: '')
													.addClass(config[i].charts[x].width.replace("chart-div","").replace("ui-draggable","").replace("ui-droppable",""))
													.attr({
														"align" : "center"
													}).attr({
																"id" : config[i].charts[x].alias
															})
													.append($("<div/>").addClass("row")
																	.append($("<div/>").attr({
																								"id" : "timeline-"
																										+ config[i].charts[x].alias,
																								"data-fieldname" : config[i].charts[x].fieldname
																							})
																					.append($("<span/>").addClass("reset")
																									.append("Range:")
																									.append($("<span/>").addClass("filter")))
																					.append($("<a/>").addClass("reset")
																									.css("display","none")
																									.html("reset"))
																					.append($("<div/>").addClass("clearfix"))))
													.append($("<div/>").addClass("row")
																	.append($("<div/>").attr({
																								"id" : "timebar-"
																										+ config[i].charts[x].alias,
																								"data-fieldname" : config[i].charts[x].fieldname
																							})))
													.append($("<div/>").addClass("row").append($("<p/>").addClass("text-center").html("select a time range to zoom in"))));
						} else if (config[i].charts[x].fieldname
								&& config[i].charts[x].fieldname !== null
								&& config[i].charts[x].fieldname !== "") {
							var axisLabelDisplay = {};
							if ($.trim(config[i].charts[x].displayname) === "")
								axisLabelDisplay.display = "none";
							
							$(".widget-body", $(widget))
									.append($("<div/>").addClass(config[i].charts[x].yaxisorientation ? config[i].charts[x].yaxisorientation: '')
													.addClass(config[i].charts[x].xaxisorientation ? config[i].charts[x].xaxisorientation: '')
													.addClass(config[i].charts[x].width.replace("chart-div","").replace("ui-draggable","").replace("ui-droppable",""))
													.addClass(config[i].charts[x].linkaction?' link-action':'')
													.attr({"align" : "center"})
													.attr({"id" : config[i].charts[x].alias,"data-fieldname" : config[i].charts[x].fieldname,'data-charttype':config[i].charts[x].charttype,'data-xaxis':config[i].charts[x].xaxis,'data-yaxis':config[i].charts[x].yaxis,'name':'charts'})
													.append($("<div/>").addClass("axisLabel").html(config[i].charts[x].displayname).css(axisLabelDisplay))
													.data("aggregate",config[i].charts[x].aggregate)
													.append($("<a/>").addClass("reset").css("display","none").html("reset"))
													.append($("<div/>").addClass("clearfix")).append($("<div/>").attr({"name" : "maxheight","id":config[i].charts[x].alias+"container"}).css({"overflow":"auto"})));
							if(localStorage["isMultipleProfile"]){
								$("#"+config[i].charts[x].alias).data("profileid",THIS.profileId);
							}else if(profileId){
								$("#"+config[i].charts[x].alias).data("profileid",profileId);
							}
						}
						else if (config[i].charts[x].charttype === "NGram") {
							THIS.tableField = config[i].charts[x].tableField;
							THIS.gramType = config[i].charts[x].ngramType;
							var myDataTable = {}, ngramFilterTypes = []; 
							THIS.ngramFilters = {
								"synonyms": [], 
								"stopwords": [],
								"lemmatization": false,
								"hideSpecialChars": false,
								"hideNumbers":false
							};
							var accordianDiv = $("<div/>").addClass("row panel-group").css({"margin-top": "-10px", "margin-bottom": "15px"});
							for(var data in config[i].charts[x]) {
								if(data == "ngramLemmatization") {
									THIS.ngramFilters["lemmatization"] = config[i].charts[x][data] | false;
								}
								if(data == "ngramSynonyms" || data == "ngramStopwords") { 
									if(config[i].charts[x][data]) {
										ngramFilterTypes.push(data);
										accordianDiv.append($("<div/>").addClass("panel panel-default")
											.append($("<div/>").addClass("panel-heading").css({"padding": "5px 15px"})
												.append($("<div/>").addClass("panel-title")
													.append($("<a/>").css({"display": "block"}).attr({"data-toggle":"collapse", "href":"#"+data})
														.unbind("click").bind("click", function(){
															if(!$(this).attr('aria-expanded')){
																getNgramFilterTableData($(this).attr('href').split("#")[1]);
															}
														}).text(data.split("ngram")[1]))))
											.append($("<div/>").attr({"id": data}).addClass("panel-collapse collapse")
												.append($("<div/>").addClass("col-md-12").css({"z-index": 100, "padding": "15px 0 5px 15px"})
													.append($("<div/>").addClass('col-md-4').append($("<input>").addClass("form-control").attr({"name": "key"+data, "id": "key"+data})
															.unbind("keyup").bind("keyup", function(){
																var key = $(this).attr("id").split("key")[1], value = $("#value"+key).val();
																$("#firstBtn"+$(this).attr("id").split("key")[1]).attr("disabled", true);
																if((key == 'ngramStopwords' && $(this).val() && $(this).val().trim().length > 0) || 
																		($(this).val() && $(this).val().trim().length > 0 && value && value.trim().length > 0)){
																	$("#firstBtn"+$(this).attr("id").split("key")[1]).attr("disabled", false);
																} 
															}))
														.append($("<span>").addClass("note").append($("<i>").addClass("fa fa-check text-success")).text("Add your key")))
													.append($("<div/>").addClass('col-md-4').css({"display":(data=='ngramStopwords')?'none':''}).append($("<input>").addClass("form-control")
															.attr({"name": "value"+data, "id": "value"+data})
															.unbind("keyup").bind("keyup", function(){
																var value = $(this).attr("id").split("value")[1], key = $("#key"+value).val();
																$("#firstBtn"+$(this).attr("id").split("value")[1]).attr("disabled", true);
																if($(this).val() && $(this).val().trim().length > 0 && key && key.trim().length > 0){
																	$("#firstBtn"+$(this).attr("id").split("value")[1]).attr("disabled", false);
																} 
															}))
														.append($("<span>").addClass("note").append($("<i>").addClass("fa fa-check text-success")).text("Add your value")))
													.append($("<div/>").addClass('col-md-4')
														.append($("<a/>").addClass("btn btn-sm btn-success").text("Add")
															.attr({"id": "firstBtn"+data, "disabled": true}).css({"margin-right": "5px"})
															.unbind('click').bind('click', function(e){
																var divId = $(this).attr("id").split("firstBtn")[1],
																	enteredText = $(this).text();
																ngramTableDataInsertUpdate(enteredText, divId);
															}))
														.append($("<a/>").addClass("btn btn-sm btn-danger").text("Clear").attr({"id": "secondBtn"+data})
															.unbind('click').bind('click', function(e){
																var divId = $(this).attr("id").split("secondBtn")[1], 
																	enteredText = $(this).text();
																ngramTableClearCancelData(enteredText, divId);
															}))
															.append($("<a/>").addClass("btn btn-sm btn-success").text("Bulk Synonyms Upload").attr({"id": "bulk"+data}).css({"margin-left": "35px"}).css({"display":(data=='ngramStopwords')?'none':''})
																	.unbind('click').bind('click', function(e){
																		  $("#myModalUpload").modal('show');
																		     $('.fileUpload').change(function() {
																		        	if($(this).val()){
																		        		var extension = $(this).val().split('.').pop().toLowerCase();
																		        		if(["xls","xlt","xlm","xlsx"].indexOf(extension)=== -1){
																		        			showNotification("notify","Upload only excel file.");
																		        			return;
																		        		}else{
																		        			isValid = true;
																		                    $(this).parent().next().val($(this).val().replace(/^C:\\fakepath\\/i, ''));        			
																		        		}
																		        	}
																		        });
																        $("#frmModalExportImport").unbind('submit').submit(function(e) {
																        	e.preventDefault();
																        	if(!isValid)
																        		return;
																            enableLoading();
																            var formData = new FormData(document.getElementById("frmModalExportImport"));
																            formData.append("db", 'E2EMFPostGres');
															            	formData.append("table",'ngram_filter_synonyms');
															            	formData.append("columnnames",JSON.stringify(['ngram_key','ngram_value']));
																            $.ajax({
																                url: serviceCallMethodNames["uploadCSVToTable"].url,
																                type: "POST",
																                data: formData,
																                enctype: 'multipart/form-data',
																                processData: false,
																                contentType: false
																               
																            }).done(function(response) {
																                disableLoading();
																                $("#myModalUpload").modal('hide');
																                if (!response)
																                    showNotification("error", "Error while importing file");
																                else if (response && response.isException) {
																                	if(response.message.search('exceeds')||response.message.search('No Data Found')){
																	                    showNotification("error", response.message);
																                	} else{
																                	showNotification("error", response.customMessage);
																                	}													                
																                    return;
																                } else {
																                    showNotification("success", response.message);
																                    getNgramFilterTableData('ngramSynonyms');
																                  callAjaxService("clearCache", callbackSucessClearCache, callBackFailure,null, "POST");
																                }
																            });													        
																            return false;
																        }); 
																		}))	))
												.append($("<div/>").addClass("widget-body")
													.append($("<table/>").addClass("table table-responsive").attr({"id": data+"Table"})))));
									}
								}
								if(data == "ngramMoreFilters"){
									if(config[i].charts[x][data]) {								
										accordianDiv.append($("<div/>").addClass("panel panel-default")
											.append($("<div/>").addClass("panel-heading").css({"padding": "5px 15px"})
												.append($("<div/>").addClass("panel-title")
													.append($("<a/>").css({"display": "block"}).attr({"data-toggle":"collapse", "href":"#moreFilter"}).text('more Filters'))))
											.append($("<div/>").attr({"id":"moreFilter" }).addClass("panel-collapse collapse")
												.append($("<div/>").addClass("col-sm-12").css({"padding": "0 0 15px"})
													.append($("<div/>").addClass("col-md-4 col-xs-4 col-sm-4 col-lg-4 smart-form margin-top-10")
														.append($("<span/>").addClass("pull-left")
															.append($("<label/>").addClass("toggle").text("Filter Special Character :")															
																.append($("<input/>").attr({"type":"checkbox","id":"filterSpecialChars" }))
																.unbind("click").bind("click", function(){
																	if($($(this).find('input')).prop('checked')){																																					
																		THIS.ngramFilters["hideSpecialChars"] = true;
																	} else {
																		THIS.ngramFilters["hideSpecialChars"] = false;
																	}
																	generateNgram(false);	
																})
																.append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"})))))	
													.append($("<div/>").addClass("col-md-4 col-xs-4 col-sm-4 col-lg-4 smart-form margin-top-10")
														.append($("<span/>").addClass("pull-left")
																.append($("<label/>").addClass("toggle").text("Filter Numbers :")															
															.append($("<input/>").attr({"type":"checkbox","id":"filterNumbers" }))
																.unbind("click").bind("click", function(){
																	if($($(this).find('input')).prop('checked')){																	
																		THIS.ngramFilters["hideNumbers"] = true;
																	} else {
																		THIS.ngramFilters["hideNumbers"] = false;
																	}
																	generateNgram(false);	
																})
															.append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"}))))))));		
									}
								}
							}
							
							function createNgramFilterTable(divId, datas) {
								var columns = [
									{ "data" : "slno", "defaultContent" : "", "title" : "Sl No" },
									{ "data" : "ngramID", "defaultContent" : "", "title" : "Ngram Id" },
									{ "data" : "ngramKey", "defaultContent" : "", "title" : "Key" },
									{ "data" : "ngramValue", "defaultContent" : "", "title" : "Value" },
									{ "render": function (data, type, row) {
								    	   var row = jQuery.extend(true, {}, row); 
								    	   if(row.ngramType != "stopwords") {
								    		   row.ngramValue = row.ngramValue.replace(/\s/g, '-');
									    	   row.ngramValue = row.ngramValue.replace(/>/g, 'gt&');
								    	   }
								    	   row.ngramKey = row.ngramKey.replace(/\s/g, '-');
								    	   row.ngramKey = row.ngramKey.replace(/>/g, 'gt&');
								    	   return "<a class='btn btn-xs btn-success btn-edit' data-type="+divId+" data-row="+JSON.stringify(row)+">EDIT</a>"
										}
									}, { 
								       "render": function (data, type, row) {
								    	   var row = jQuery.extend(true, {}, row); 
								    	   if(row.ngramType != "stopwords") {
								    		   row.ngramValue = row.ngramValue.replace(/\s/g, '-');
									    	   row.ngramValue = row.ngramValue.replace(/>/g, 'gt&');
								    	   }
								    	   row.ngramKey = row.ngramKey.replace(/\s/g, '-');
								    	   row.ngramKey = row.ngramKey.replace(/>/g, 'gt&');
								    	   return "<a class='btn btn-xs btn-danger btn-delete' data-type="+divId+" data-row="+JSON.stringify(row)+">DELETE</a>"
								       }
								}];
								divId == "ngramStopwords"? columns.splice(3, 2): "";
								myDataTable[divId]= $("table#" + divId + "Table").DataTable({
			        				"language": { "zeroRecords" : "No records to display" },
			        				"dom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"
			        						+ "t"
			        						+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			        				"data": datas,
		        					"processing" : true,
									"sort" : true,
									"info" : true,
									"jQueryUI" : false,
									"stateSave" : true,
			        			    "destroy": true,
									"fnRowCallback" : function(nRow, aData, iDisplayIndex) {
										$("td:first", nRow).html(iDisplayIndex +1);
										return nRow;
									},
			        				"columns": columns
			        			});
								$("table#" + divId + "Table tbody tr").on('click', 'a.btn-delete', function() {
									var deletedRow = JSON.parse($(this).attr("data-row")), divId = $(this).attr('data-type');
									if(divId != "ngramStopwords") {
										deletedRow.ngramValue = deletedRow.ngramValue.replace(/-/g, ' ');
										deletedRow.ngramValue = deletedRow.ngramValue.replace(/gt&/g, '>');
									}
									deletedRow.ngramKey = deletedRow.ngramKey.replace(/-/g, ' ');
									deletedRow.ngramKey = deletedRow.ngramKey.replace(/gt&/g, '>');
									ngramTableDeleteData(deletedRow, divId);
								});
								$("table#" + divId + "Table tbody tr").on('click', 'a.btn-edit', function() {
									var deletedRow = JSON.parse($(this).attr("data-row")), divId = $(this).attr('data-type');
									if(divId != "ngramStopwords") {
										deletedRow.ngramValue = deletedRow.ngramValue.replace(/-/g, ' ');
										deletedRow.ngramValue = deletedRow.ngramValue.replace(/gt&/g, '>');
									}
									deletedRow.ngramKey = deletedRow.ngramKey.replace(/-/g, ' ');
									deletedRow.ngramKey = deletedRow.ngramKey.replace(/gt&/g, '>');
									$("#key"+divId).val(deletedRow.ngramKey);
									$("#value"+divId).val(deletedRow.ngramValue);
									$("#firstBtn" + divId).text("Update").attr("disabled", false);
									$("#secondBtn" + divId).text("Cancel");
									$("#key"+divId).attr({"data-ngramId": deletedRow.ngramID});
								});
								generateNgram(false);
							}
							
							function getNgramFilterTableData(divId){
								if(divId) {
									/* getAll ngram data request start */
									var ngramType = divId.split("ngram")[1].toLowerCase(),
										request = {
										/*
										 * "profileId":
										 * parseInt(dataAnalyser.ProfileID) ||
										 * 0,
										 */
										"userName" : System.userName,
										"ngramType": ngramType
									};
									enableLoading();
									callAjaxService("getAllNgramFilter", nGramGetAllCallBackSucess, callBackFailure, request, "POST", null, true);
									/* getAll ngram data request end */
									function nGramGetAllCallBackSucess(response){
										THIS.ngramFilters[ngramType] = response;
										createNgramFilterTable(divId, response);
									}
								} else {
									var requestObjs = [], requestCalls = [];
									for(var i=0; i<ngramFilterTypes.length; i++){
										var	request = {
											/*
											 * "profileId":
											 * parseInt(dataAnalyser.ProfileID) ||
											 * 0,
											 */
											"userName" : System.userName,
											"ngramType": ngramFilterTypes[i].split("ngram")[1].toLowerCase()
										}
										requestObjs.push(request);
										requestCalls.push("getAllNgramFilter");
									}
									enableLoading();
									callAjaxServices(requestCalls, nGramGetAllCallBackSucess, callBackFailure, requestObjs, "POST", "json", true);

									function nGramGetAllCallBackSucess(response){
										response.forEach(function(data, index){
											THIS.ngramFilters[requestObjs[index]["ngramType"]] = response[index];
										});
									}
								}
							}
							
							function ngramTableDataInsertUpdate(enteredText, divId){
								if(enteredText === "Add") {
									/* add ngram data request start */
									var request = {
										"userName" : System.userName,
										"profileId": parseInt(dataAnalyser.ProfileID) || 0,
										"ngramKey" : $("#key"+divId).val(),
										"ngramValue" : $("#value"+divId).val(),
										"ngramType": divId.split('ngram')[1].toLowerCase()
									};
									enableLoading();
									callAjaxService("addNgramFilter", nGramAddCallBackSucess, callBackFailure, request, "POST", null, true);
									/* add ngram data request end */
									function nGramAddCallBackSucess(response){
										getNgramFilterTableData(divId);
									}
								} else {
									/* update ngram data request start */
									var request = {
										"ngramId" : $("#key"+divId).attr('data-ngramId'),
										"ngramKey" : $("#key"+divId).val(),
										"ngramValue" : $("#value"+divId).val(),
										"ngramType": divId.split('ngram')[1].toLowerCase()
									};
									enableLoading();
									callAjaxService("updateNgramFilter", nGramUpdateCallBackSucess, callBackFailure, request, "POST", null, true);
									/* update ngram data request end */
									function nGramUpdateCallBackSucess(response){
										getNgramFilterTableData(divId);
									}
									$("#firstBtn" + divId).text("Add");
									$("#secondBtn" + divId).text("Clear");
								}
								
								$("#key"+divId).val('');
								$("#value"+divId).val('');
								$("#firstBtn"+divId).attr("disabled", true);
							}
							
							function ngramTableDeleteData(deletedRow, divId){
								/* delete ngram data request start */
								var request = {
									"ngramId" : deletedRow["ngramID"],
									"ngramType": deletedRow["ngramType"]
								};
								enableLoading();
								callAjaxService("deleteNgramFilter", nGramDeleteCallBackSuccess, callBackFailure, request, "POST", null, true);
								/* delete ngram data request end */
								function nGramDeleteCallBackSuccess(response){
									getNgramFilterTableData(divId);
								}
							}
							
							function ngramTableClearCancelData(enteredText, divId){
								if($(this).text() === "Clear") {

								} else {
									$("#firstBtn" + divId).text("Add");
									$("#secondBtn" + divId).text("Clear");
								}
								$("#key"+divId).val('');
								$("#value"+divId).val('');
								$("#firstBtn"+divId).attr("disabled", true);
							}
							
							$(".widget-body", $(widget))
								.append(accordianDiv)
								.append($("<div/>")
									.addClass('col-md-2 pull-left').css('padding-top','5px')
									.append($("<span/>").addClass("onoffswitch-title").append($("<i/>").addClass("fa fa-location-arrow").text(" Pan / Zoom")))
									.append($("<span/>").addClass("onoffswitch").css('margin-left', '10px')
										.append($("<input/>").addClass("onoffswitch-checkbox ngram-onoffswitch-checkbox")
											.attr({"type": "checkbox", "id":"myonoffswitch", "name": "onoffswitch"}))
										.append($("<label/>").addClass("onoffswitch-label").attr({"for": "myonoffswitch"})
											.append($("<span/>").addClass("onoffswitch-inner").attr({"data-swchon-text": "ON", "data-swchoff-text": "OFF"}))
											.append($("<span/>").addClass("onoffswitch-switch"))
										)
									)
								)
								.append($("<div/>")
									.addClass('col-md-4').append($("<input>").addClass("form-control").attr({"name": "field", "id": "field1"}))
									.append($("<span>").addClass("note").append($("<i>").addClass("fa fa-check text-success")).text("Change field name")))
								.append($("<div/>").addClass('col-md-4')
								.append($("<input>").addClass("form-control").attr({"name": "gramType", "id": "gramType1"}))
								.append($("<span>").addClass("note").append($("<i>").addClass("fa fa-check text-success")).text("Select gram type")))
								.append($("<div/>").attr({"id" : "cloud"}));
							
							var request = {
								"source" : THIS.sourceType,
								"database" : THIS.database,
								"table" : THIS.table
							};
							enableLoading();
							callAjaxService("getColumnList", callBackSuccess,callBackFailure, request, "POST");
							function callBackSuccess(response){ 
								var tableFields = [];
								if(response.length>0){
									for ( var i = 0; i < response.length; i++) {
										tableFields.push({
											"id" : response[i].columnName,
											"text" : response[i].columnName
										});
									}
								}
								$("#field1").select2({
									placeholder : "Select field",
									data : tableFields || []
								}).on('change', function(e){
									THIS.tableField = e.val;
									generateNgram(true);
								}).select2('val', THIS.tableField);
								
								getNgramFilterTableData();
							}
							
							function callBackFailure(error){ console.log(error);}
							
							$("#gramType1").select2({
								placeholder : "Select Gram",
								data: [{"id":"1","text":"Uni-grams"},
										{"id":"2","text":"Bi-grams"},
										{"id":"3","text":"Tri-grams"},
										{"id":"4","text":"4-grams"},
										{"id":"5","text":"5-grams"}]
							}).on('change', function(e){
								THIS.gramType = e.val;
								generateNgram(true);
							}).select2('val', THIS.gramType);
							
							function generateNgram(makeAjaxCall) {
								enableLoading();
								$('.ngram-onoffswitch-checkbox').prop('checked', false);
								var requestConfig = {
									"ngramType": THIS.gramType,
									"source" : THIS.sourceType,
									"database" : THIS.database,
									"table" : THIS.table,
									"tableField": THIS.tableField,
									"whereCondition": THIS.filterQuery
								}
								Graphing.prototype.drawNGram(requestConfig, makeAjaxCall);
							}
						} else if (config[i].charts[x].charttype === "IFrame") {
							THIS.IFrameURL = config[i].charts[x].IFrameURL;
							$(".widget-body", $(widget))
								.append($("<div/>").addClass('row')
									.append($("<div/>").attr({"id" : config[i].charts[x].alias}).addClass('IFrame-div')));
						} else if (config[i].charts[x].charttype === "HTMLTemplate") {
							THIS.HTMLTemplateContent = config[i].charts[x].HTMLTemplateContent;
							THIS.contentLayout = config[i].charts[x].contentLayout;
							$(".widget-body", $(widget)).css({"min-height": "0px", "height": "auto"})
								.append($("<div/>").attr({"id" : config[i].charts[x].alias}).addClass('HTMLTemplate-div'));
						}
					}
				}
			};
			if (templates.getWidget.template
					&& templates.getWidget.template !== null) {
				
				drawProfileWidget();
				
					
			} else {
				templates.getWidget(tempoptions, drawProfileWidget);
			}
			
		}
	if(THIS.factsConfig.length >1 && profiletype !='dashboard'){ 
			  var factArr = [];
			  for(var i=0; i<THIS.factsConfig.length; i++){
				  factArr.push({id:THIS.factsConfig[i].fact,text:THIS.factsConfig[i].displayname});
			  }
			  $("#ddFacts").select2({
				  allowClear: true,
				  placeholder : "Choose Facts",
				  data : factArr
			  }).on("change", function() {
				  var selectedFact = $(this).select2("data").id;
				  for(var i=0; i<config.length; i++){
					  for(var j=0; j<config[i].charts.length; j++){
						  if(!config[i].charts[j].isCustomGroup)
							  config[i].charts[j].group = selectedFact;
					  }
				  }
				  for(var i=0; i<THIS.Summary.length; i++){
					  for(var j=0; j<THIS.Summary[i].length; j++){
						  if(!THIS.Summary[i][j].isCustomGroup)
							  THIS.Summary[i][j].group = selectedFact;
					  }
				  }
				  var modifiedConfig = config;
				  callService(modifiedConfig,THIS.filters,THIS.Summary);
			  }).select2("data",factArr[0]);
		  }
	
	if(THIS.additionalconfig && THIS.additionalconfig.isReportDownload){
		loadScript("js/DataAnalyser/ActionCallbackHandler.js");
		var htmlStr = '<a class="btn btn-success btn-sm pull-right" href="javascript:void(0);" id="downloadReportExcel" title="Download data in XLS format"> <i class="fa fa-fw fa-cloud-download"></i>Excel'
		var actionColorCode = THIS.additionalconfig.reportConfig.actioncolorcode || "btn-success";
		if (THIS.additionalconfig.isReportDownload.length > 0){
			if(THIS.additionalconfig.reportConfig.actiondisplaytype && THIS.additionalconfig.reportConfig.actiondisplaytype !==undefined && THIS.additionalconfig.reportConfig.actiondisplaytype === "dropdown"){
				$(".profile-settings").prepend('<div class="btn-group " id="actionHolder"><i class="dropdown-toggle width115" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-download"></i> Select Action</i><ul class="dropdown-menu" id="reportActionDropDown"></ul></div>');
		          // <button type="button" class="btn dropdown-toggle width115
					// '+actionColorCode+'" data-toggle="dropdown"
					// aria-haspopup="true" aria-expanded="false">Select
					// Action<span class="caret"></span> </button> <ul
					// class="dropdown-menu"
					// id="reportActionDropDown"></ul></div>');
			}else{
				$(".profile-settings").prepend('<div class="btn-group" id="actionHolder"></div >');
			}
			for(var k=0; k<THIS.additionalconfig.isReportDownload.length; k++){
				var action = THIS.additionalconfig.isReportDownload[k].action.replace(/`/g,'"');
				var actionCallback  = THIS.additionalconfig.isReportDownload[k].actionCallback;
				if(actionCallback !== undefined){
					actionCallback = actionCallback.indexOf('(') === -1? actionCallback+'()':actionCallback;
					actionCallback = actionCallback.replace(/@/g,"'");
				}
				if(THIS.additionalconfig.reportConfig.actiondisplaytype && THIS.additionalconfig.reportConfig.actiondisplaytype !==undefined && THIS.additionalconfig.reportConfig.actiondisplaytype === "dropdown"){
					 if(actionCallback && actionCallback.indexOf("actionCallbackHandler.importExcel")>-1){
						var button = $("<a/>").attr({"href":"javascript:void(0)","onClick":"javascript:"+actionCallback}).html(config[i].charts[x].data.actionData[k].action);
						$("#reportActionDropDown",$("#actionDiv")).append($('<li/>').append(button));
					}else{
						$("#reportActionDropDown", $("#actionDiv")).append('<li><a>'+action+'</a></li>');
					}
					
				}else{
					 if(actionCallback && actionCallback.indexOf("actionCallbackHandler.importExcel")>-1){
						var button = $("<a/>").attr({"href":"javascript:void(0)","onClick":"javascript:"+actionCallback}).addClass("btn  marginRight10 "+actionColorCode).html(action);
						$("#actionHolder", $("#actionDiv")).append(button);
					}else{
						$("#actionHolder", $("#actionDiv")).append(action);// .append('<i
																			// onClick="javascript:'+actionCallback+'">'+THIS.additionalconfig.isReportDownload[k].action+'</button>');
					}
				}
			}
		}
		$("#actionHolder", $("#actionDiv")).unbind("click").bind("click", {"tableconfig" :"","type":"excel"}, downloadExcel);
		$("#actionHolder",  $("#actionDiv")).unbind("click").bind("click", {"tableconfig" :"","type":"csv"}, downloadExcel);		
	}
	$(".jarviswidget-toggle-btn").unbind('click').bind('click',function(){
		$(this).find("i").toggleClass("fa-minus fa-plus");
		$(this).parents("header").next().toggleClass("hide");
	});
	};
	
	var topMeasureForDashboard =function(data,profileActions,appliedfilters,mappedFilters,viewSettings){
		if(THIS.factsConfig.length >1){ 
			  var factArr = [];
			  for(var i=0; i<THIS.factsConfig.length; i++){
				  factArr.push({id:THIS.factsConfig[i].fact,text:THIS.factsConfig[i].displayname});
			  }
			  $("#ddFacts").select2({
				  allowClear: true,
				  placeholder : "Choose Facts",
				  data : factArr
			  }).on("change", function() {
				  var selectedFact = $(this).select2("data").id;
					var requests = [],methodNames = [],profileIds = [],allConfigs ={};
				  if(data && data.length>0){
						for(var i=0;i<data.length;i++){
							if(data[i].type === "profile"){
								THIS.config = config;
								THIS.profileId = data[i]["id"];
								THIS.sourceType = data[i]["source"];
								THIS.database = data[i]["database"];
								THIS.table = data[i]["configtable"];
								
							THIS.facts = [];
							THIS.factsConfig = [];
							try {
								THIS.factsConfig = Jsonparse(data[i]["fact"],"FACT CONFIG");
								for(var fact=0; fact<THIS.factsConfig.length; fact++){
									THIS.facts.push(THIS.factsConfig[fact].fact);
								}
							}catch (e){
								THIS.factsConfig = [];
								THIS.facts = data["fact"].split(",");
								for(var fact=0; fact<THIS.facts.length; fact++){
									var factObj = {};
									factObj.fact = THIS.facts[fact];
									factObj.displayname = THIS.facts[fact];
									THIS.factsConfig.push(factObj);
								}
							}
							if(data[i].customfacts && data[i].customfacts !== undefined){
								THIS.customFacts = Jsonparse(data[i].customfacts.replace(/@/g, "'"),"CUSTOM FACT");
								if(THIS.customFacts && THIS.customFacts.length>0){	
									THIS.customFacts.forEach(function(customfact){
										THIS.dependentFacts.push.apply(THIS.dependentFacts,customfact.dependentfacts);
									});
								}
							}
								THIS.viewSettings = viewSettings ?viewSettings:'';
								var config = Jsonparse(data[i].config.replace(/@/g, "'"),"CONFIG");
								var configFilters = $.extend(true,[], appliedfilters);
								if(configFilters){
									for(var k=0;k<mappedFilters.length;k++){
										  for(var j=0;j<configFilters.length;j++){
										      if(mappedFilters[k].fieldname === configFilters[j].fieldName){
											     
											     configFilters[j].fieldName = mappedFilters[k].mappedProfiles[data[i].id];
										       }
										   }
										}	
								}
								for(var fi=0;fi<configFilters.length;fi++){
									if(configFilters[fi].fieldName ===""){
										configFilters.splice(fi,1);
										fi--;
										
									}
								}
								var filterConfig =configFilters; 
								var summary = Jsonparse(data[i].summary.replace(/@/g, "'"),"SUMMARY");
								 for(var k=0; k<config.length; k++){
									  for(var j=0; j<config[k].charts.length; j++){
										  if(!config[k].charts[j].isCustomGroup)
											  config[k].charts[j].group = selectedFact;
									  }
								  }
								  for(var s=0; s<summary.length; s++){
									  for(var j=0; j<summary[s].length; j++){
										  if(!summary[s][j].isCustomGroup)
											  summary[s][j].group = selectedFact;
									  }
								  }
								  var modifiedConfig = config;
								var request = callService(modifiedConfig,filterConfig,summary);
								if(request && ($.trim(request.columns) || request.query)){
									methodNames.push("getData");
									profileIds.push(data[i].id);
									allConfigs[data[i].id] = {
											config:config,
											summary:summary,
											query:request.query,
											filters:filterConfig,
											fieldAliasMapping:request.fieldAliasMapping,
											customFacts : Jsonparse(data[i].customfacts.replace(/@/g, "'"),"custom facts"),
											facts : THIS.facts,
											customFields : THIS.customFields
									};
									request.source = data[i].source;
									request.database = data[i].database;
									if(data[i].configtable && data[i].configtable !=="null")
									request.table = data[i].configtable;
									request.id = data[i].id;
									var group =Jsonparse(data[i].additional_config).profileGroupBy !==undefined ?Jsonparse(data[i].additional_config).profileGroupBy :data[i].additional_config.profileGroupBy;
									request.groupByEnabled = group!==undefined ? group:false;
									requests.push(request);
								}
								
							}
						}
						THIS.profileActions = profileActions;
						
					}
				 callDashBoardService(requests,"getMultipleData",profileIds,allConfigs);
			  }).select2("data",factArr[0]);
		  }
		
	};
	var downloadExcel = function(event) {
	if($(event.target).attr("data-datatable") != undefined){
		  var tableSorting = $($(event.target).attr("data-datatable"))
					.dataTable().fnSettings().aaSorting;
			var tableData = event.data.tableconfig.data;
			var tableId = event.data.tableconfig.alias.replaceAll("#","");
			var totalCount = 0;
			var columnObj = event.data.tableconfig.columns;
			columnObj = columnObj.filter(function(o){ return !o.hidecoloumn || o.hidecoloumn === false});
			if(tableData.isSelectAll){
				columnObj = columnObj.filter(function(o){ return !o.name || o.name !== "selectAll"});
			}
			try{
				if(profileAnalyser && profileAnalyser.dataTable && profileAnalyser.dataTable[tableId]){
					totalCount = profileAnalyser.dataTable[tableId].page.info().recordsDisplay;
				}
				else if(dataAnalyser.profiling.grapher.dataTable && dataAnalyser.profiling.grapher.dataTable.length>0){
					for(var i=0;i<dataAnalyser.profiling.grapher.dataTable.length;i++){
						if(dataAnalyser.profiling.grapher.dataTable[i].context && dataAnalyser.profiling.grapher.dataTable[i].context[0].sTableId === tableId){
							totalCount = dataAnalyser.profiling.grapher.dataTable[i].page.info().recordsDisplay;
						}
					}
				}
			}catch(ex){
				if(dataAnalyser.profiling.grapher.dataTable && dataAnalyser.profiling.grapher.dataTable.length>0){
					for(var i=0;i<dataAnalyser.profiling.grapher.dataTable.length;i++){
						if(dataAnalyser.profiling.grapher.dataTable[i].context && dataAnalyser.profiling.grapher.dataTable[i].context[0].sTableId === tableId){
							totalCount = dataAnalyser.profiling.grapher.dataTable[i].page.info().recordsDisplay;
						}
					}
				}
			}
			var columns = "";
			var groupBy = "";
			var filter = " 1=1";
			if(localStorage["datatablefilter_"+event.data.tableconfig.alias.replace("#",'')])
				filter+=' And '+localStorage["datatablefilter_"+event.data.tableconfig.alias.replace("#",'')];
			if(localStorage["datatablecolumnfilter_"+event.data.tableconfig.alias.replace("#",'')])
				filter+=' And '+localStorage["datatablecolumnfilter_"+event.data.tableconfig.alias.replace("#",'')];
			var aggCheck = false;
			for ( var col = 0; col < columnObj.length; col++) {
				if (!columnObj[col].data)
					continue;
				columnObj[col].title = columnObj[col].title.replace(/'/g, "").replace(/"/g, "");
				if (col !== 0){
					columns += ",";
				}
				
				if(columnObj[col].groupbyaggregate &&  columnObj[col].groupbyaggregate !==null){
					columns += columnObj[col].groupbyaggregate+'('+addTildInColumnName(columnObj[col].data) + ') AS "' + columnObj[col].title+'"';
					aggCheck = true;
				} else{
					columns += addTildInColumnName(columnObj[col].data) + ' AS "' + columnObj[col].title+'"';
					if (groupBy !== "")
						groupBy += ",";
					groupBy += addTildInColumnName(columnObj[col].data);
				}
			};
			var fileName = ($(event.target).attr('data-header') && $(event.target).attr('data-header').trim() !== '')? $(event.target).attr('data-header').trim() : $(event.target).text().trim();
			fileName = (fileName.length > 250) ? 'Report' : fileName;
			var fileAttr = $(event.target).find("i").attr("filename");
			if(fileAttr)
				fileName = fileAttr;
			var request = {
				"columns" : columns,
				"databaseColumns" : tableData.columns,
				"orderField" : tableData.isSelectAll?tableSorting[0][0]-1:tableSorting[0][0],
				"orderType" : tableSorting[0][1],
				"searchValue" : $('.dataTables_filter input').val(),
				"source" : tableData.source,
				"database" : tableData.database,
				"table" : tableData.table,
				"query":tableData.dataQuery,
				"filter" : filter,
				"totalcount":totalCount,
				"displayName" : fileName
			};
			if(tableData.dataQuery){
				var filters = tableData.dataQuery.split(",");
				if(filters && filters.length>0){
					for(var i=0;i<filters.length;i++){
						if(filters[i].indexOf("as")>0){
							var alias = filters[i].split("as");
							if((alias && alias.length>1) && request.filter.indexOf(alias[1].trim())>-1){
								var regex = new RegExp(alias[1].trim(), "g");
								request.filter = request.filter.replace(regex, alias[0].trim());
							}
						}
					}
				}
			}
			if (aggCheck)
				request.groupBy = groupBy;
			
			  var $preparingFileModal = $("#preparing-file-modal");
		      $preparingFileModal.dialog({ modal: true });
		      var downloadURL;
		      if (event.data.type === "excel")
		    	  downloadURL = 'rest/dataAnalyser/datatableExcelDownload';
		      else
		    	  downloadURL = 'rest/dataAnalyser/datatableCSVDownload';
			$.fileDownload(downloadURL, {
				successCallback: function() {
	                $preparingFileModal.dialog('close');
	            },
	            failCallback: function(responseHtml) {
	                $preparingFileModal.dialog('close');
					$("#ui-id-2").html("Warning");
	                $("#error-modal").html($(responseHtml).text());
	                $("#error-modal").dialog({ modal: true });
	            },
		        httpMethod: "POST",
		        data: {"data":encodeURI(JSON.stringify(request)), "isEncoded" : "true"}
		    });
		
	}
		
	};
	var downloadCSV = function(event) {
		downloadExcel(event);
	};
	var chartDrop = function(obj, ui) {
		var thisObj = $(obj);
		var dragableEle = ui;
		var options = {
			"name" : ui.draggable.text(),
			"piestyle" : "width:" + $(thisObj).height() * 80 / 100 + "px;height:"
					+ $(thisObj).height() * 80 / 100 + "px"
		};
		var callBackPie = function(response) {
			var chart = $(response);
			if(dragableEle.draggable.data() && dragableEle.draggable.data("customtype")){
				chart.data("customtype",dragableEle.draggable.data("customtype"));
				chart.data("formula",dragableEle.draggable.data("formula"));
			}
			$(thisObj).html(chart);
			$(".axisHeader", thisObj).bind("click", function() {
				axisLabelClick(this);
			});
			$(".chart-remove", thisObj).bind("click", function() {
				removeChart(this);
			});
			axisLabelClick(chart[0]);
		};
		templates.getPie(options, callBackPie);
	};
	this.removeChart = function(obj) {
		removeChart(obj);
	};
	var removeChart = function(obj){
		$.confirm({
		    text: "Are you sure you want to delete that chart?",
		    confirm: function() {
		    	$(obj).parent().empty()
		    	.append(
						$("<span/>").addClass(
								"dimension-placeholder"));
		    },
		    cancel: function() {
		        // nothing to do
		    }
		});
	};
	this.axisLabelClick = function(obj) {
		axisLabelClick(obj);
	};
	var axisLabelClick = function(obj){
		var dropDownList = THIS.allChartsConfig.charts;
		var chartDropDown = $("#saveConfigModel").find("select[name='charttype']");
		chartDropDown.html("");
		if($(obj).attr("name") === "configSummary"){
			for(var x=0; x<dropDownList.length;x++){
				if(dropDownList[x].chartName.indexOf("summary")>-1){
					chartDropDown.append($("<option/>").attr("value",dropDownList[x].chartName).html(dropDownList[x].displayName));
				}
			}
		}else{
			for(var x=0; x<dropDownList.length;x++){
				if(dropDownList[x].chartName.indexOf("chart")>-1){
					chartDropDown.append($("<option/>").attr("value",dropDownList[x].chartName).html(dropDownList[x].displayName));
				}
			}
		}
		$("#saveConfigModel").modal("show");
		$("#saveConfigModel").find("h3").find("span").html($(obj).data('field'));
		chartDropDown.select2({width: 'resolve',width: '100%'}).select2("val",$(obj).data('charttype'))
		.unbind('change').bind('change',function(){			
			constructChartConfig(obj);
		});		
		$('#saveConfigModel').find("[name='save']").unbind('click').bind('click',obj, function (e) {
			var dupliacteProperties = {
					numformat:"format",
					filterdefaultval:"filterby",
					chartwidth:"summary-width",
					chartheight:"summary-height",
					"widget-icon" : "circle-title-icon",
					"widget-color":"circle-title-color",
					dateformat:"date-format"
			};
			var isValid = true;
			chartConfigTable.search( '' ).columns().search( '' ).draw();
			$('#saveConfigForm').find("em").remove();
			$('#saveConfigForm').find("[name]").each(function(){
				if($(this).parents("td").attr("mandatory") && !$(this).val()){
					isValid = false;
					$(this).parents("td").find("label").append($("<em/>").addClass("small invalid").html("This field is mandatory"));
					$(this).parents("td").children().toggle();
				}
				if($(this).attr('type')=== "multiselect" &&  $(this).data('select2')){
					var selectedData = $(this).select2("data");
					if(selectedData.length > 0){
						selectedData = selectedData.map(function(obj){return obj.id});
						$(e.data).data($(this).attr('name'),selectedData.join(","));
					}
				}else if($(this).attr('type')=== "select"){
					$(e.data).data($(this).attr('name'),$(this).select2("val"));
				}
				else{
					$(e.data).data($(this).attr('name'),$(this).val());
				}
				if(dupliacteProperties[$(this).attr('name')]){
					$(e.data).data(dupliacteProperties[$(this).attr('name')],$(this).val());
				}
			});
			setTimeout(function(){$('#saveConfigForm').find("em").remove()}, 3000);
			var parentDiv = $(e.data).parent();
			if($(e.data).attr("name") === "configSummary"){
				$(e.data).data('charttype',$("#saveConfigModel").find("select").val());
				$(e.data).data('widgetType',$("#saveConfigModel").find("select").val().replace('summary-',''));
			}else{
				$(e.data).html($(e.data).data('displayname')).data('charttype',$("#saveConfigModel").find("select").val());
			}			
			$("[class^=chart-]", $(e.data).parent()).hide();
			$("[type^=summary-]", $(e.data).parent()).hide();
			$("[type='"+$("#saveConfigModel").find("select").val()+"']", parentDiv).show();
			$("." + $("#saveConfigModel").find("select").val(), parentDiv).show();
			if ($("#saveConfigModel").find("select").val() === "chart-geo"){
				loadScript("js/plugin/topojson/topojson.js",function(){loadScript("js/plugin/datamaps-master/dist/datamaps.all.js",function(){ 
					new Datamap({
					 scope: 'usa',
					 element: $(".chart-geo",parentDiv)[0],
					 projection: 'mercator',
					 width:250,
					 fills: {
						 defaultFill: 'grey'
					}
				 });},null);},null);
			}
			if(isValid)
				$("#saveConfigModel").modal("hide");
		});
		constructChartConfig(obj);
	};
	var constructChartConfig = function(obj){
		var currentObj = $(obj).data(),chartType = $("#saveConfigModel").find("select").val();
		var dupliacteProperties = {
				numformat:"format",
				filterdefaultval:"filterby",
				chartwidth:"summary-width",
				chartheight:"summary-height",
				"widget-icon" : "circle-title-icon",
				"widget-color":"circle-title-color",
				dateformat:"date-format"
		};
		var data = THIS.allChartsConfig.chartProperty[chartType];
		for(var i=0;i<data.length;i++){
			var property = data[i].propertyName;
			/*
			 * if(property.indexOf("-")>-1){ property =
			 * property.replace(/[-][a-z]/gi, function(x){return
			 * x.toUpperCase();}).replace(/-/g,""); }
			 */
			var value = currentObj[property]?currentObj[property]:
				currentObj[dupliacteProperties[data[i].propertyName]]?currentObj[dupliacteProperties[data[i].propertyName]]:
				// configProperties[data[i].propertyName]?configProperties[data[i].propertyName]:
				data[i].defaultValue?data[i].defaultValue:'';
			var jsonProperties = ['colorcodes','measurelabel','linklabel'];
			if(jsonProperties.indexOf(data[i].propertyName) !== -1 && typeof(currentObj[data[i].propertyName]) === 'object'){
				value = JSON.stringify(value);
			}
			if(data[i].propertyType === 'select' || data[i].propertyType === 'multiselect'){
				if(data[i].valueList.indexOf("dimensionData") > -1 || data[i].valueList.indexOf("measureData") > -1){
					data[i].itemList = THIS[data[i].valueList];
				}else if(data[i].valueList.indexOf("allData") > -1){
					data[i].itemList = $.extend(THIS.dimensionData, THIS.measureData);
				}
				else if(data[i].valueList.indexOf("actionData") > -1){
					data[i].itemList = THIS.actionData;
				}
				else{
					data[i].itemList =  Jsonparse(data[i].valueList,data[i].propertyName+" SELECT VALUE");
				}
			}
			data[i].value = value;
			data[i].orderBy = (i+1);
		}
		$("#tablesaveConfig").empty();
		chartConfigTable = $("#tablesaveConfig")
		.DataTable(
				{
					"dom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"
						+ "t"
// / + "<tfoot> </tfoot>"
						+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"oColVis": {"aiExclude": [3]},
					"aaData" : data,
					"processing" : true,
					"lengthChange" : false,
					"sort" : true,
					"info" : true,
					"jQueryUI" : false,
					"paging": false,
					"scrollY": "350px",
			        "bScrollCollapse": true,
			        "fixedHeader": true,
					"autoWidth": false,
					"destroy" : true,
					"aaSorting": [[ 3, "asc" ]],
					/* "aoColumnDefs": [{ "bVisible": false, "aTargets": [3] }], */
					// "fixedColumns": true,
					 "columnDefs": [ {
						    "targets": 1,
						    "width":'25%',
						    "createdCell": function (td, cellData, rowData) {
						    try{
						    	var labelValue = rowData.value;
						    	if(!rowData.propertyType)
						    		rowData.propertyType = "text";
						    	var element;
						    	if(rowData.propertyType === 'textarea'){
									element = $('<label/>').addClass('textarea textarea-resizable').append($('<textarea/>').addClass('width-100per').val(rowData.value).attr({"name":rowData.propertyName}));
									if(rowData.value){
										labelValue = $("<pre/>").html(rowData.value);										
									}
						    	}
						    	else if(rowData.propertyType === 'bool'){
									element = $("<label/>").addClass("toggle").append($("<input/>").attr({"type":"checkbox","name":rowData.propertyName}))
											.append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"}));
									if(rowData.value && rowData.value.toLowerCase() === 'yes'){
										element.find('input').attr('checked','checked');
									}
								}
								else if(rowData.propertyType === 'multiselect' || rowData.propertyType === 'select'){									
									element = $('<label/>').addClass('select').append($('<input/>').attr({"name":rowData.propertyName,"type":rowData.propertyType}));
									if(rowData.itemList && Object.keys(rowData.itemList).length>0){
										var data = [],selectedValue;
										var selectedData = [];
										labelValue = [];
										if(rowData.propertyType === 'multiselect'){
											selectedValue = rowData.value?rowData.value.split(","):[];
										}else{
											labelValue.push(rowData.itemList[rowData.value]);
										}
										for(var option in rowData.itemList){
											data.push({
												id:option,
												text:rowData.itemList[option]
											});
											if(selectedValue && selectedValue.indexOf(option)>-1){
												selectedData.push({
													id:option,
													text:rowData.itemList[option]
												});
												labelValue.push(rowData.itemList[option]);
											}
										}
										labelValue = labelValue.toString();
										element.find("input").select2({
											placeholder : "Select "+rowData.propertyName,
											multiple : rowData.propertyType === 'multiselect',
											data :  data,
											width: 'resolve',
											width: '100%',
											allowClear: true
										});
										if(rowData.propertyType === 'multiselect'){
											element.find("input").select2("data", selectedData,true).on("change", function(e) {
												var selectedVal = $(this).select2("data");
												selectedVal = selectedVal.map(function(obj){ return obj.text});
												$(this).parent().prev().html(selectedVal.join(","));
												$(this).parent().css({"height":$(this).parent().find("ul").height()});
											});
										}else{
											element.find("input").select2("val", rowData.value).on("change", function(e) {
												var selectedVal = $(this).select2("data");
												$(this).parent().prev().html(selectedVal && selectedVal.text?selectedVal.text:"");
											});
										}
									}
									
								}
								else if(rowData.propertyType === 'rangetext' || rowData.propertyType === 'rangedate'){
									var items = rowData.valueList.split(",");
									var val1 = currentObj[items[0]] || '';
									var val2 = currentObj[items[1]] || '';
									element = $('<label/>').addClass('input div-2').attr({"type":"range"})
									.append($('<div>').addClass('col-6 col').append($('<input/>').addClass('input-sm col-md-10').val(val1).attr({"name":items[0]})))
									.append($('<div/>').addClass('col col-6 ').append($('<input/>').addClass('input-sm col-md-10').val(val2).attr({"name":items[1]})));
									labelValue = (val1 && val2) ?val1 +' To '+val2:'';
									if(rowData.propertyType === 'rangedate'){
										element.find('input').datepicker(
												{
													defaultDate : "+1w",
													changeMonth : true,
													dateFormat: 'mm-dd-yy',
													onClose : function(selectedDate) {
														if($(this).attr("name") === items[0]){
															$("[name="+items[1]+"]").datepicker("option", "minDate", selectedDate);
														}else{
															$("[name="+items[0]+"]").datepicker("option", "maxDate", selectedDate);
														}
														$(this).parents('label').siblings().html($("[name='"+items[0]+"']").val() +" To " + $("[name='"+items[1]+"']").val());
													}
												});
									}
								}else{
									element = $('<label/>').addClass('input').append($('<input/>').addClass('input-sm width-100per')
											.val(rowData.value).attr({"name":rowData.propertyName,"type":rowData.propertyType}));
								}
								$(td).empty();
						    	element.addClass('width-100per');
								if(rowData.propertyType === 'color'){
									$(td).append(element).unbind('click').bind('click',function(){
										$(this).parents("table").find("[mode='read']").show();
										$(this).parents("table").find("[mode='edit']").hide();
									});
								}else{
									if(rowData.propertyType.indexOf("select") === -1){
										element.find('input,textarea').on('blur',function(){
											if($(this).prop('nodeName') === 'SELECT'){
												$(this).parent().siblings().html($(this).find('option:selected').text());										
											}else if($(this).prop('nodeName') === 'TEXTAREA' && $(this).val()){
												$(this).parent().siblings().html($('<pre/>').html($(this).val()));
											}else if($(this).parents('label').attr('type') !== 'range'){
												$(this).parent().siblings().html($(this).val());
											}
											$(this).parents('label').hide();
											$(this).parents('label').prev().show();
										});
									}									
									element.attr({"mode":"edit"}).hide();// .on('click',function(e){e.preventDefault();e.stopPropagation();})
									$(td).append($("<span/>").attr({"mode":"read"}).html(labelValue || '&nbsp;'))
									.append(element)
									.unbind('click').bind('click',function(){
										$(this).parents("table").find("[mode='read']").show();
										$(this).parents("table").find("[mode='edit']").hide();
										$(this).find("span[mode='read']").hide();
										$(this).find("label").show();
									});
								}
								if(rowData.mandatory){
									$(td).prev().append($("<span/>").html("*").css("color","red"));
									$(td).attr("mandatory",true);
								}
									
						    }
						    catch (e) {
								console.log(e);
							}
					 	}
					 	
						  },{"targets":0,"width":'25%'},{"targets":2,"width":'50%'}],
					"aoColumns" : [ {
						"sWidth": '25%',
						"mDataProp" : "displayName",
						"title":"Property",
						"sDefaultContent" : ""
					}, {
						"mDataProp" : "value",
						"sWidth": '25%',
						"title":"Value",
						"sDefaultContent" : ""
					}, {
						"mDataProp" : "description",
						"sWidth": '50%',
						"title":"Description",
						"sDefaultContent" : ""
					}, {
						"mDataProp" : "orderBy",
						"sWidth": '0%',
						"bVisible": false,
						"title":"Sequence",
						"sDefaultContent" : ""
					}]
				});
		$("#tablesaveConfig_wrapper").find("table:eq(0)").css("width","921px");
	};	
	var sourceSelect = function(obj) {
		THIS.sourceType = $(obj).data("source");
		var request;
		if (THIS.sourceType === "hive" || THIS.sourceType === "postgreSQL"|| THIS.sourceType === "memsql") {
			$("#selectSource").hide(300);
			$("#selectTable").show(300);

			request = {
				"source" : THIS.sourceType
			};
			enableLoading();
			callAjaxService("getDatabaseList", callBackGetDatabaseList,
					callBackFailure, request, "POST");
		} else if (THIS.sourceType === "myProfiles") {
		
		$('#myprofilediv').show();
		$('#allprofilediv').hide();
		var requiredCol = [ {
			"sWidth": '13%',
			"mDataProp" : "displayname",
			"title":"Name",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "profilekey",
			"sWidth": '13%',
			"title":"Action",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "source",
			"sWidth": '13%',
			"title":"Source",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "database",
			"sWidth": '13%',
			"title":"Data Base",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "configtable",
			"sWidth": '13%',
			"title":"Table",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "lastmodifiedby",
			"sWidth": '12%',
			"title":"Modified By",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "lastmodifiedon",
			"sWidth": '13%',
			"title":"Modified On",
			"sDefaultContent" : ""
		}];
		var profileParam = {
				"getAll" : true,
				"profileType" : "profile",
		        "columns" : requiredCol,
		        "isHistory" : true,
		        "isPublish" : true
		};
		enableLoading();
		var profileUtitlyObj = new profileUtiltyClass(profileParam,function(e){myProfileSelect(e,this);});
		profileUtitlyObj.getMyProfile();
		}else if(THIS.sourceType === "query"){
			$("#divDataSource").show(300);
			$("#selectSource").hide(300);
			$("#proceedData").unbind('click').bind('click',function(){
				if(!$("#frmConfDataSource").valid())
					return;
				$("[name=profilegroupby]").removeAttr("checked");
				processFromDataSource();
			});
			constructSelect2("#queryDataSource","select id as id, displayname as text from profile_analyser where type in('datasource') and lower(displayname) like '%<Q>%'",false,null,null);
		}
	};
	
	/*
	 * $('.profile-tabs li a').on('click', function() { currentProfileSource =
	 * $(this).attr('data-attr'); var profileUtilty = new
	 * profileUtiltyClass(null,currentProfileSource,function(e){myProfileSelect(e,this);},"profile",requiredCol);
	 * profileUtilty.getPreviousSelectedProfiles(); });
	 */
	function processFromDataSource(){ 
		var id = $("#queryDataSource").select2("val");
		if(!THIS.additionalconfig)
			THIS.additionalconfig = {};
		THIS.additionalconfig.dataSourceId = id;
		var request = {
			"id" : id
		};
		enableLoading();
		callAjaxService("getProfileConfig", callBackGetDataSource,
				callBackFailure, request, "POST");
	};
	function callBackGetDataSource(response){
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		var data = Jsonparse(response["config"],"CONFIG")[0];
		THIS.database = data.database;
		THIS.sourceType = data.source;
		if(data.config){
			var config = Jsonparse(data.config,"CONFIG");
			config.query = config.query.replace(/@/g, "'");
			THIS.dataQuery = config.query;
		}
		tableSelect(THIS.database);
		proceedColumn();
	};

	var myProfileSelect = function(obj) {
		THIS.dataQuery = null;
		if(localStorage["isMultipleProfile"])
			delete localStorage["isMultipleProfile"];
		var id = $(obj).data("refid");
		var request = {
			"id" : id,
			"viewId":-1
		};
		enableLoading();
		$("#cloneProfile").show();
		callAjaxService("getProfileConfig", callBackGetProfileConfig,
				callBackFailure, request, "POST");
	};
	
	this.callBackHistroyProfile = function(response,type){
		
		callBackGetProfileConfig(response,type);
	}
	var callBackGetProfileConfig = function(response,type) {
		type = type||"profile";
		$("#myProfiles").hide(300);
		$("#drawProfile").show(300);
		THIS.profileActions = response["profileActions"];
		if (THIS.profileActions && THIS.profileActions !== ""){
			for (var i=0; i< THIS.profileActions.length;i++ ){
				THIS.actionData[THIS.profileActions[i].name] = THIS.profileActions[i].name;
			};
		}
		var data = response;
		if(type != "history"){
			data = Jsonparse(response["config"],"CONFIG");
		}
		try {
			THIS.factsConfig = Jsonparse(data[0]["fact"],"FACT");
			for(var i=0; i<THIS.factsConfig.length; i++){
				THIS.facts.push(THIS.factsConfig[i].fact);
			}
		}catch (e){
			THIS.factsConfig = [];
			if (data && data.length > 0 && data[0]["fact"])
				THIS.facts = data[0]["fact"].split(",");
			for(var factCount=0; factCount<THIS.facts.length; factCount++){
				var factObj ={};
				factObj.fact = THIS.facts[factCount];
				factObj.displayname = THIS.facts[factCount];
				THIS.factsConfig.push(factObj);
			}
		}
		THIS.sourceType = data[0]["source"];
		THIS.database = data[0]["database"];
		THIS.table = data[0]["configtable"];
		THIS.ProfileName = data[0]["displayname"];
		THIS.ProfileDesc = data[0]["remarks"];
		THIS.ProfileID = data[0]["id"];
		if(data[0]["customfacts"] && data[0]["customfacts"] !== undefined){
			data[0]["customfacts"] = data[0].customfacts.replace(/\\n/g,' ');
			THIS.customFacts = Jsonparse(data[0]["customfacts"].replace(/@/g, "'"),"CUSTOM FACT");
			if(THIS.customFacts && THIS.customFacts.length>0){
				THIS.customFacts.forEach(function(customfact){
					THIS.dependentFacts.push.apply(THIS.dependentFacts,customfact.dependentfacts);
				});
			}			
		}
		var $notificationFilter = data[0]["notification_filter"] || "";
		if ($notificationFilter !== "null" && $notificationFilter !== ""){
			$(".notification-holder").empty().append($("<a/>").addClass("btn-outline-warning btn margin-right-2 padding-5").attr("href","javascript:void(0);").html($notificationFilter));
		}
		if(data[0]["config"] !==""){
			data[0]["config"] = data[0]["config"].replace(/@/g, "'");
			THIS.config = Jsonparse(data[0]["config"],"CONFIG");
		}
		$("#saveProfile").data("app",data[0].app);
		if (data[0]["filters"] && $.trim(data[0]["filters"]) !== "")
			THIS.filters = Jsonparse(data[0]["filters"].replace(/@/g, "'"),"FILTER");
		if (data[0]["summary"] && $.trim(data[0]["summary"]) !== ""){
			THIS.Summary = Jsonparse(data[0]["summary"].replace(/@/g, "'"),"SUMAMRY");
			loadScript("js/plugin/liquidFillGauge/liquidFillGauge.js",function(){},"droppable");
		}
		if(response.dataQuery){
			var dataQueryObj = Jsonparse(response.dataQuery,"DATA QUERY");
			THIS.dataQuery = dataQueryObj.query.replace(/@/g, "'");
			THIS.dataQuery = THIS.dataQuery.replace(/(\r\n|\n|\r)/gm," ");
		}
		if (data[0].additional_config) {
			THIS.additionalconfig = Jsonparse(data[0].additional_config,"ADDITIONAL CONFIG");
			if (THIS.additionalconfig && THIS.additionalconfig.isFilterNotapplicable) { 
				$("#filterNotapplicable").attr("checked", "checked");
			}
			if (THIS.additionalconfig && THIS.additionalconfig.isFilterInline) { 
				$("#filterModalInlineswitch").attr("checked", "checked");
			}if(THIS.additionalconfig && THIS.additionalconfig.isFilterHide){
				$("#filterShowHide").attr("checked", "checked");
			}if (THIS.additionalconfig && THIS.additionalconfig.filterInterdependent) {
				$("#filterInterdependent").attr("checked", "checked");
			}
			if(THIS.additionalconfig){
				if(THIS.additionalconfig.isMinified){
					$("body").addClass('minified');
					$('#hideMinified').prop('checked', true);
				}
				if(THIS.additionalconfig.pdfDownload){
					$('#pdfDownload').prop('checked', true);
				}else{
					$('#pdfDownload').prop('checked', false);
				}
				if(THIS.additionalconfig.isFilterNotapplicable){
				$("#filterNotapplicable").prop("checked", true);
				}else{
				$("#filterNotapplicable").prop("checked", false);
				
				}
				if(THIS.additionalconfig.hideemail){
					$('#hideemail').prop('checked', true);
				}else{
					$('#hideemail').prop('checked', false);
				}
				if(THIS.additionalconfig.hideView){
					$('#hideView').prop('checked', true);
				}else{
					$('#hideView').prop('checked', false);
				}
			}
			
			if (THIS.additionalconfig && THIS.additionalconfig.profileGroupBy) {
				$("[name=profilegroupby]").attr("checked", "checked");
			}else{
				$("[name=profilegroupby]").removeAttr("checked");
			}
			$("#profileColorPalette").empty().attr('name', "");
			if (THIS.additionalconfig && THIS.additionalconfig.profilePalette) {
				$("#profileColorPalette").attr('name', THIS.additionalconfig.profilePalette)
				var colors;
				if (THIS.additionalconfig.profilePalette.indexOf("rgbColor") === -1 && THIS.additionalconfig.profilePalette.indexOf("linearRgbColor") === -1){
					var scheme = palette.listSchemes(THIS.additionalconfig.profilePalette)[0];
					colors = scheme.apply(scheme, [8]);
				} else{
					colors = palette.generate(new Function("x","return "+ THIS.additionalconfig.profilePalette), 8);
				}
				var decimalCode = [];
				colors.forEach(function(colorCode){
					$("#profileColorPalette").append($("<span/>").css("background","#"+colorCode));
					decimalCode.push( "#"+colorCode);
				});
				d3.scaleOrdinal(decimalCode);
				dc.config.defaultColors = function() {
					return decimalCode;// d3.scaleOrdinal().range(decimalCode);
				};
				/*
				 * d3.scale.category10 = function() { return
				 * d3.scaleOrdinal().range(decimalCode); };
				 */
			}
		}
		showFullScreen();
		drawProfile(THIS.config, $("#widget-fullscreen"));
		drawSummary(THIS.Summary, "#widget-fullscreen");
		$('[id^=widget-grid]').jarvisWidgets('destroy');
		pageSetUp();
		$("#widget-fullscreen").find(".jarviswidget-fullscreen-btn").trigger(
				'click');
		$("#widget-fullscreen").find(".jarviswidget-fullscreen-btn").unbind(
				"click").bind("click", function() {
			$("#jarviswidget-fullscreen-mode").remove();
			$('.nooverflow').removeClass('nooverflow');
			if (type==="history"){
				$("#drawProfile").hide(300);
				$("#profileHistroyDiv").show(300);
			}else{
				$("#myProfiles").show(300);
				$("#drawProfile").hide(300);
			}
			disableLoading();
		});
		$("#widget-fullscreen > header").append(
				$("<a/>").addClass("btn btn-primary pull-right").attr({
					"href" : "javascript:void(0);",
					"id" : "editProfile"
				}).html("Edit").prepend(
						$("<i/>").addClass("fa fa-fw fa fa-edit")));
		$("#editProfile").unbind("click").bind("click", function() {
			editProfile();
		});
		callService(THIS.config, THIS.filters, THIS.Summary);
	};
	var editProfile = function() { 
		$("#sourceTableDesc").html(THIS.ProfileName+" : "+THIS.sourceType+" - "+THIS.database+" - "+THIS.table);
		$("#jarviswidget-fullscreen-mode").remove();
		$('.nooverflow').removeClass('nooverflow');
		disableLoading();
		for(var i=0; i<THIS.config .length; i++){
			  for(var j=0; j<THIS.config [i].charts.length; j++){
				  if(!THIS.config [i].charts[j].isCustomGroup)
					  THIS.config [i].charts[j].group = "";
			  }
		  }
		tableSelect(THIS.database,true);

	
	};
	
	var filterCallBack = function(tempFilterObj,filter,data){
		$(tempFilterObj).find("[name=searchtype]").val(filter.searchtype?$.trim(filter.searchtype).toLowerCase():"in");
		onChangeTypeInFieldConfig(tempFilterObj.find("[name=fieldtype]"),filter);
		if (filter.fieldType === "lookup" || filter.fieldType === "select" || filter.fieldType === 'lookupdropdown') {
			if(filter.isSingle){
				$(tempFilterObj).find("[name=selecttype]").val(filter.isSingle);
				if(filter.defaultvalue && filter.fieldType === "select" && typeof(filter.defaultvalue) === "object" && filter.defaultvalue.id){
					filter.defaultvalue = filter.defaultvalue.id;
				}
			}else if(filter.fieldType === "select" && filter.defaultvalue && filter.defaultvalue.length>0){
				filter.defaultvalue = filter.defaultvalue.map(function(o){return (typeof o === 'object' && o.id)?o.id:o});
			}
			$(tempFilterObj).find("[name=lookup]").val(filter.lookupquery);
			if(typeof(filter.lookupquery) ==='object'){
				var filterValue = JSON.stringify(filter.lookupquery);
				$(tempFilterObj).find("[name=lookup]").val(filterValue);
			};
			if(filter.fieldType === 'lookupdropdown' || filter.fieldType === "select"){
				$(tempFilterObj).find("[name=defaultvaluelabel]").find("select").val(filter.defaultvalue);
			}else{
				if(filter.defaultvalue && Object.keys(filter.defaultvalue).length>0){
					$(tempFilterObj).find("[name=defaultvalue]").select2('data', filter.defaultvalue, true);
				}
			}
		} else if (filter.fieldType === 'DATE' || filter.fieldType === 'dynamicDate' || filter.fieldType === 'NUM' ) {
			$(tempFilterObj).find("[name=from-defaultvalue]").val(filter["from-defaultvalue"]);
			$(tempFilterObj).find("[name=to-defaultvalue]").val(filter["to-defaultvalue"]);
			$(tempFilterObj).find($("[name='lookupdiv']")).find($("input[name='lookup']")).val(filter["dateFormat"]);
			
		 }else if(filter.fieldType === 'dateRange'){
			$(tempFilterObj).data(filter.defaultvalue);
			$(tempFilterObj).find($("[name='lookupdiv']")).find($("input[name='lookup']")).val(filter["dateFormat"]);
             var previousSelection = dataAnalyser.storedDateRange(filter.defaultvalue);
			 if(previousSelection && previousSelection.length>0 ){
				 $(tempFilterObj).find("[name=from-defaultRangevalue]").val(previousSelection[0]?previousSelection[0].trim():"");
				 $(tempFilterObj).find("[name=to-defaultRangevalue]").val(previousSelection[1]?previousSelection[1].trim():""); 
			 }
			 
		 } else {
			$(tempFilterObj).find("[name=defaultvalue]").val(filter.defaultvalue);
		}
	}
	
	this.drawFilters = function(filters,fields){
		drawFilters(filters,fields);
	};
	
	var drawFilters = function(filters,fields) {

		$("#filters").empty();
		var currentFilter = '';
		if (filters && filters.length > 0) {
		for ( var i = 0; i < filters.length; i++) {
			currentFilter = filters[i];
				var tempFilterObj = drawFilter(fields);
				$(tempFilterObj).find("[name=field]").select2('val',filters[i].fieldName);
				$(tempFilterObj).find("[name=displayname]").val(filters[i].displayName);
				$(tempFilterObj).find("[name=fieldtype]").val(filters[i].fieldType);
				$(tempFilterObj).find("[name=interdependentcolumns]").select2('val',filters[i].interdependentcolumns);
				// adding source abd database values
				if(localStorage.isMultipleProfile){
					$(tempFilterObj).find("[name=Source]").select2('val',filters[i].source);
					onChangeTypeInFieldConfigs($(tempFilterObj),filters[i].source,filters[i].database);
				}
				
				if(filters[i].isMandatory){
					$(tempFilterObj).find("[name=isMandatory]").attr("checked","checked");	
				}
				if(filters[i].chat){
					$(tempFilterObj).find("[name=chat]").attr("checked","checked");	
				}else{
					$(tempFilterObj).find("[name=chat]").attr("checked",false);	
				}
				if(filters[i].invisible){
					$(tempFilterObj).find("[name=invisible]").attr("checked","checked");	
				}
				if(filters[i].includenull){
					$(tempFilterObj).find("[name=includenull]").attr("checked","checked");	
				}
				if(filters[i].innotin){
					$(tempFilterObj).find("[name=innotin]").attr("checked","checked");
				}
				
				if(filters[i].defaultQueryValue){
					$(tempFilterObj).find("[name=defaultSelectedVal]").val( filters[i].defaultquery);
					$(tempFilterObj).find("[name=defaultQueryValue]").attr("checked","checked");
					$(tempFilterObj).find('[name="defaultQuerydiv"]').css({'display': 'block'})
					if(filters[i].defaultquery){
						var dt ='';
						(function(index,filterObj){
							$.ajax( {
							url : serviceCallMethodNames["suggestionsLookUp"].url,
							dataType : 'JSON',
							type : 'GET',
							cache: false,
							contentType: "application/json; charset=utf-8",
							data: {
				                    source : filters[i].source?filters[i].source:THIS.sourceType,
									database:filters[i].database?filters[i].database:THIS.database,
									lookupQuery : filters[i].defaultquery
				              
				            },success: function(data){
						        dt=data;
						       filters[index].defaultvalue = data[0].id
						        console.log('inside success')
						        filterCallBack(filterObj,filters[index],data);
						    },error: function(xhr) {
							    // Do Something to handle error
								console.log('Error Occured')
							  }
						}
				       );})(i,tempFilterObj)// ajax call end
					}
				}else{
					filterCallBack(tempFilterObj,filters[i])
				}
			}
		} else
			drawFilter(fields);
	};
	
	/**
	 * Checks in Custom Facts and return's 'true' if exists
	 */
	var isCustomFact = function(fact){
		var rtnVal = undefined;
		if(THIS.customFacts && THIS.customFacts.length>0){
			for(var i=0; i<THIS.customFacts.length; i++){
				if(fact === THIS.customFacts[i].fact){
					rtnVal = THIS.customFacts[i];
					break;
				}
			}
		}
		return rtnVal;
	};
	var onChangeHistoryTable = function(historyTable,mainTable,obj){
		var callBackGetColumnsList = function(response){
			disableLoading();
			var historyColumns = [];
			var masterColumns = []
			var savedHistoryData = $(obj).parents(".jarviswidget-editbox").find("[name='historyColumnsMapping']").data() || {};
			var mappingcolumns = savedHistoryData.mappingcolumns || {};
			if(response && response.length>0){
				if(response[0] && response[0].length>0){
					for(var i=0;i<response[0].length;i++){
						historyColumns.push({id:response[0][i].columnName,text:response[0][i].columnName});
					}
				}
				var sortedHistoryData = response[1];
				if(mappingcolumns && Object.keys(mappingcolumns).length>0){
					sortedHistoryData = response[1].sort(function(a,b){
						if(mappingcolumns[a.columnName] && mappingcolumns[b.columnName] && mappingcolumns[a.columnName].orderby && mappingcolumns[b.columnName].orderby)
							return mappingcolumns[a.columnName].orderby-mappingcolumns[b.columnName].orderby;
						else
							0;
					});
				}
				if(sortedHistoryData && sortedHistoryData.length>0){
					$(obj).parents(".jarviswidget-editbox").find("[name='historyColumnsMapping']").empty();
					$(obj).parents(".jarviswidget-editbox").find("[name='searchHistoryColumns']").keyup(function () {
				        var rejexValue = new RegExp($(this).val(), 'i');
				        $(this).parent().parent().find("section").hide();
				        $(this).parent().parent().find("section").filter(function () {
				        	 return rejexValue.test($(this).text());
				        }).show();
				    });
					for(var i=0;i<sortedHistoryData.length;i++){
						masterColumns.push({id:sortedHistoryData[i].columnName,text:sortedHistoryData[i].columnName});
						var selectedData = {};
						if(mappingcolumns && Object.keys(mappingcolumns).length>0){
							if(mappingcolumns[sortedHistoryData[i].columnName] && Object.keys(mappingcolumns[sortedHistoryData[i].columnName]).length>0){
								selectedData = mappingcolumns[sortedHistoryData[i].columnName];
							}
						}else{
							selectedData.column = sortedHistoryData[i].columnName;
						}
						var selectColumn = $("<input/>").attr({"name":"mappedColumn"}).addClass("form-control");
						$(obj).parents(".jarviswidget-editbox").find("[name='historyColumnsMapping']")
						.append($("<section/>").data("column",sortedHistoryData[i].columnName).addClass("dd-handle").css({"height":"60px"})
						.append($("<div/>").addClass("col-sm-3").html(sortedHistoryData[i].columnName))
						.append($("<div/>").addClass("col-sm-3").append(selectColumn))
						.append($("<div/>").addClass("col-sm-3").append($("<input/>").attr({"name":"mappedLabel"}).addClass("form-control").val(selectedData.label)))
						.append($("<div/>").addClass("col-sm-3").append($("<textarea/>").attr({"name":"renderFunction"}).html(selectedData.renderFunction))));
						selectColumn.select2({
							placeholder : "Select Column",
							allowClear: true,
							data :  historyColumns
						}).select2("val",selectedData.column);
					}
				}
				$(obj).parents(".jarviswidget-editbox").find("[name='primarykey']").select2({
					placeholder : "Select Primary Key",
					data :  historyColumns
				}).select2("val",savedHistoryData.primarykey || '');
				$(obj).parents(".jarviswidget-editbox").find("[name='foreginkey']").select2({
					placeholder : "Select Foregin Key",
					data :  historyColumns
				}).select2("val",savedHistoryData.foregincolumn || '');
				$(obj).parents(".jarviswidget-editbox").find("[name='versionColumn']").select2({
					placeholder : "Select Version column",
					data :  historyColumns
				}).select2("val",savedHistoryData.versionColumn || '');
				$(obj).parents(".jarviswidget-editbox").find("[name='statusColumn']").select2({
					placeholder : "Select Status column",
					data :  historyColumns
				}).select2("val",savedHistoryData.statusColumn || '');
				$(obj).parents(".jarviswidget-editbox").find("[name='hsourcelink']").select2({
					placeholder : "Select Source Link",
					data :  historyColumns
				}).select2("val",savedHistoryData.hsourcelink || '');
				$(obj).parents(".jarviswidget-editbox").find("[name='msourcelink']").select2({
					placeholder : "Select Source Link",
					data :  masterColumns
				}).select2("val",savedHistoryData.msourcelink || '');
				$(obj).parents(".jarviswidget-editbox").find("[name='tableHistoryAlias']").val(savedHistoryData.tableHistoryAlias);
				$(obj).parents(".jarviswidget-editbox").find("[name='tableMasterAlias']").val(savedHistoryData.tableMasterAlias);
				$(obj).parents(".jarviswidget-editbox").find("[name='historyColumnsMapping']").sortable().disableSelection();
			}
		};
		var historyRequest = {
			"source" : THIS.sourceType,
			"database" : THIS.database,
			"table":historyTable
		},request = {
			"source" : THIS.sourceType,
			"database" : THIS.database,
			"table":mainTable
		};
		enableLoading();
		callAjaxServices(["getColumnList","getColumnList"],callBackGetColumnsList,callBackFailure,[historyRequest,request],"POST","json",true);
	};
	var drawConfig = function(config) {
		$("#wid-id-profiling > header > h2").html(THIS.ProfileName);
		$("#addFacts").html("drop measure here");
		if (THIS.factsConfig.length > 0){
			$("#addFacts").empty();
			for(var i=0; i<THIS.factsConfig.length; i++){
				var $factElement = $("<a/>");
				if(isCustomFact(THIS.factsConfig[i].fact)){
					$factElement.addClass("btn btn-info txt-color-white btn-sm margin-right-5 btn-fact customfact ui-draggable").attr({"rel":"popover","href":"javascript:void(0);","tabindex":"0" ,"data-fact":THIS.factsConfig[i].fact,"data-displayname":THIS.factsConfig[i].displayname,"data-title":"Configure Fact","data-toggle":"popover"}).html(THIS.factsConfig[i].displayname);
				}else{
					$factElement.addClass("btn btn-success btn-sm margin-right-5 btn-fact ui-draggable").attr({"rel":"popover","href":"javascript:void(0);","tabindex":"0" ,"data-fact":THIS.factsConfig[i].fact,"data-displayname":THIS.factsConfig[i].displayname,"data-title":"Configure Fact","data-toggle":"popover"}).html(THIS.factsConfig[i].displayname);
				}
				$factElement.on('inserted.bs.popover', function () {
					var $fact = $(this);
					$('.popover-content').append('<div class="col col-6"/>')
		    							 .append('<input type="text" class="form-control" name="factname" disabled="disabled">')
		    							 .append('<div class="col col-6"/>')
		    							 .append('<input type="text" class="form-control" name="factdisplayname" placeholder="Enter Fact Displayname">');
		    		
					$('input[name="factname"]').val($fact.attr("data-fact"));
					$('input[name="factdisplayname"]').val($fact.attr('data-displayname')).change(function(){
						$fact.attr("data-displayname",$(this).val());
						$fact.text($(this).val());
		    		});
		    	  });
		    	  
				$factElement.popover({
		    		  placement: 'auto',
		    		  delay :  { "show": 100, "hide": 100 },
		    		  trigger : 'click',
		    		  title : "Configure Fact"
		    	  });
				
				$("#addFacts").append($factElement);
	// $factElement.unbind("click").bind('click',
	// function(){$factElement.popover("hide");})
			}
		}
		if (THIS.additionalconfig && THIS.additionalconfig.measureDisplayName && THIS.additionalconfig.measureDisplayName !== "")
			$("#measureDisplayName").val(THIS.additionalconfig.measureDisplayName);
	// $("#addFacts").empty().append($("<a/>").addClass("btn btn-success btn-sm
	// margin-right-5 btn-fact
	// ui-draggable").attr({"href":"javascript:void(0);",
	// "title":THIS.facts}).html(THIS.facts));
		$("#saveProfile").data("displayname",THIS.ProfileName);
		$("#saveProfile").data("remarks",THIS.ProfileDesc);
		$("#saveProfile").data("id",THIS.ProfileID);
		 var showFullScreenDiv = $("#droppable > article");
		  showFullScreenDiv.empty();
		  var idCounter = 0;
		  var idCounterCalendar = 0;
		  var idCounterNGram = 0;
		  var idCounterIFrame = 0;
		  var idCounterHTMLTemplate = 0;
			  if (config && config.length > 0){
				  for (var i=0; i<config.length;i++){
					  config[i].id = config[i].id.replace("-temp","");
					  var tempoptions = {
								"width":config[i].width,
								"widget-class":config[i]["class"],
								"id":config[i].id,
								"istemplate":true,
								"fullscreen":false,
								"icon":config[i].icon,
								"header":config[i].header
						  };
					  var drawEditProfileWidget = function(){ 
						  var tempWidget = "";
						  if (config[i].id.toString().indexOf("wid-id-calendar") > -1){
							  idCounterCalendar++;
							  var calendarOptions = {
									"table-header":config[i].header,
									"idCounter": idCounterCalendar,
									"width":config[i].width
							  };
							  tempWidget = templates.getCalendar.template(calendarOptions);
						  }else if (config[i].id.toString().indexOf("wid-id-datatable") > -1){
							  idCounter++;
							  var datatableoptions = {
										"table-header":config[i].header,
										"header":config[i].header,
										"idCounter": idCounter,
										"widget-class":config[i]["class"],
										"width":config[i].width
							  };
							  tempWidget = templates.getDatatable.template(datatableoptions);
						  }else if (config[i].id.toString().indexOf("widget-id-image") > -1){
							  tempWidget = templates.getImageWidget.template(tempoptions);
						  }else if (config[i].id.toString().indexOf("wid-id-NGram") > -1){
							  idCounterNGram++;
							  var ngramOptions = {
									"table-header":config[i].header,
									"idCounter": idCounterNGram,
									"width":config[i].width
							  };
							  tempWidget = templates.getNGram.template(ngramOptions);
						  }else if (config[i].id.toString().indexOf("wid-id-IFrame") > -1){
							  idCounterIFrame++;
  							  var IFrameOptions = {
  									"table-header":config[i].header,
  									"idCounter": idCounterIFrame,
  									"width":config[i].width
  							  };
  							  tempWidget = templates.getIFrame.template(IFrameOptions);
  						  }else if (config[i].id.toString().indexOf("wid-id-HTMLTemplate") > -1){
							  idCounterHTMLTemplate++;
  							  var HTMLTemplateOptions = {
  									"table-header":config[i].header,
  									"idCounter": idCounterHTMLTemplate,
  									"width":config[i].width
  							  };
  							  tempWidget = templates.getHTMLTemplate.template(HTMLTemplateOptions);
  						  }else{
							  tempWidget = templates.getWidget.template(tempoptions);
						  }
						  showFullScreenDiv.append(tempWidget);
						  var widget = showFullScreenDiv.find("#"+config[i].id);
						  if(config[i].tooltip){
							  widget.find("[name='tooltip']").val(config[i].tooltip);
						  }
						  if (config[i].charts && config[i].charts.length > 0){
							  for (var x=0;x<config[i].charts.length;x++){
								  if (config[i].charts[x].charttype !== "" && config[i].charts[x].charttype !=="datatable" && config[i].charts[x].charttype !=="calendar"&& config[i].charts[x].charttype !=="imagegrid" && config[i].charts[x].charttype !=="NGram" && config[i].charts[x].charttype !=="IFrame" && config[i].charts[x].charttype !=="HTMLTemplate"){
									  if (x===0)
										  $(".widget-body",$(widget)).empty();
									  var tempId = "";
									  if (config[i].charts[x].alias)
										  tempId = config[i].charts[x].alias+"-temp";
									  $(".widget-body",$(widget)).append(
											  $("<div/>").addClass(config[i].charts[x].width).attr({"align":"center"}).attr({"id":tempId}).append($("<span/>").addClass("dimension-placeholder"))
									  );
									  if (config[i].charts[x].alias && config[i].charts[x].alias !== undefined && config[i].charts[x].alias !== ""){
										  var chartDiv = $("#"+config[i].charts[x].alias+"-temp");
										  chartDiv.empty();
										  var pieOptions = {
												  "name":config[i].charts[x].displayname,
												  "piestyle": "width:"+($(chartDiv).height()*80/100)+"px;height:"+($(chartDiv).height()*80/100)+"px",
												  "chart-type":config[i].charts[x].charttype,
												  "popup-content":'<div class="smart-form"><div class="note"><strong>Display name</strong></div><label class="input"> <input type="text" class="input-xs" name="displayname"></label></div>',
												  "chart-width":"auto",
												"chart-height":"auto"
										  };
										 var tempPie = templates.getPie.template(pieOptions);
										 chartDiv.append(tempPie);
										 $("[class^=chart-]",chartDiv).hide();
										 $("[class^="+config[i].charts[x].charttype+"]",chartDiv).show();
										 // $.each(config[i].charts[x],
											// function (key, data) {
											 $(".axisHeader",chartDiv).data(config[i].charts[x]).bind("click",function(){axisLabelClick(this);});
										$(".chart-remove", chartDiv).unbind("click").bind("click", function() {removeChart(this);});
										 $(".axisHeader",chartDiv).data("field",config[i].charts[x].fieldname);
										 if (config[i].charts[x].charttype === "chart-geo"){
											 $(".chart-geo",chartDiv).empty();
											 loadScript("js/plugin/topojson/topojson.js",function(){loadScript("js/plugin/datamaps-master/dist/datamaps.all.js",function(){ 
												 new Datamap({
												 scope: 'usa',
												 element: $(".chart-geo",chartDiv)[0],
												 projection: 'mercator',
												 width:250,
												 fills: {
													 defaultFill: 'grey'
												}
											 });},"#dataAnalyserProfile")},"#dataAnalyserProfile");
										 }
								  	}
								  } else if (config[i].charts[x].charttype === "datatable"){ 
									  var datatableHeader = $(".header","#wid-id-datatable_"+idCounter);
										$("tbody","#wid-id-datatable_"+idCounter).remove();
										var response = config[i].charts[x].columns;										
										  THIS.datatable = config[i].charts[x].data.table;
										  var tableName = config[i].charts[x].data.table;
										  var columnList = config[i].charts[x].data.columns;
										for (var a=0; a<response.length;a++){
											if (response[a].renderfunction && $.trim(response[a].renderfunction) !== "" && response[a].renderfunction.indexOf("@") !==-1)
												response[a].renderfunction = response[a].renderfunction.replace(/@/g, "'");
											if((!response[a].name || response[a].name !== 'selectAll') && response[a]["class"] !== 'audit-log' && response[a]["class"] !== 'actionColumn'){
												var typeOptions;
												if (typeof(response[a].typeoptions) === 'object'){
													typeOptions = JSON.stringify(response[a].typeoptions);
												}else{
													typeOptions = response[a].typeoptions;
												}
												var columnDisplayName = response[a].sTitle?response[a].sTitle:response[a].title?response[a].title:response[a].data;
												datatableHeader.append(
													$("<th/>").addClass("numbercolumn sorting").attr({"tabindex":"0","aria-controls":"DataTables_Table_"+idCounter,"rowspan":"1","colspan":"1","aria-label":columnDisplayName})
													.append($("<div/>").attr({"data-field":response[a].data}).addClass("sortable-header")
															.append($("<a/>").addClass('axisHeader').html(columnDisplayName).attr({
																"data-field": response[a].data,
																"data-displayname":columnDisplayName,
																"data-class":response[a]["class"],
																"data-groupbyaggregate":response[a].groupbyaggregate,
																"data-summaryaggregate":response[a].summaryaggregate,
																"data-hidecoloumn": response[a].hidecoloumn,
																"data-editable": response[a].editable,
																"data-mandatory": response[a].mandatory,
																"data-typeoptions": typeOptions,
																"data-edittype": response[a].edittype,
																"data-primarykey":response[a].primarykey,
																"data-wraptext":response[a].wraptext,
																"data-enabledetail":response[a].enabledetail,
																"data-renderfunction":response[a].renderfunction,
																"data-editvalue":response[a].editvalue,
																"data-columndesc":response[a].columndesc,
																"data-columninfo": response[a].columninfo,
																"data-disable": response[a].disable,
																"data-datatype": response[a].datatype,
																"data-sourceconfig": response[a].sourceconfig,
																"data-tabledatabase": response[a].tabledatabase,
																"data-databaseconfig": response[a].databaseconfig
															}))));
											}
											 
										}
										$("table","#wid-id-datatable_"+idCounter).append($("<tbody/>").append($("<tr/>").addClass("bg-color-white").append($("<td/>").addClass("dataTables_empty").attr({"valign":"top","colspan":response.length+1}).html("Data will appear on preview"))));
										$("[name=drilldowncallback]",$("#wid-id-datatable_"+idCounter)).val(config[i].charts[x].data.drilldowncallback).data("drilldowncallback",config[i].charts[x].data.drilldowncallback).keyup(function(){
											$(this).data($(this).attr("name"),$(this).val());
											});

										$("[name=tableorderby]",$("#wid-id-datatable_"+idCounter)).val(config[i].charts[x].data.orderby);
										$("[name=tableordertype]",$("#wid-id-datatable_"+idCounter)).val(config[i].charts[x].data.ordertype);
										$("[name=defaulttablefilter]",$("#wid-id-datatable_"+ idCounter)).val(config[i].charts[x].data.filter?config[i].charts[x].data.filter.replace(/@/g, "'"):'');
										$("[name=formTitle]",$("#wid-id-datatable_"+ idCounter)).val(config[i].charts[x].data.formName?config[i].charts[x].data.formName.replace(/@/g, "'"):'');
										$("[name=overrideFunction]",$("#wid-id-datatable_"+ idCounter)).val(config[i].charts[x].data.overrideFunction?config[i].charts[x].data.overrideFunction.replace(/@/g, "'"):'');
										$("[name=tableorderby],[name=tableordertype]",$("#wid-id-datatable_"+idCounter)).data({"tableorderby":config[i].charts[x].data.orderby,"tableordertype":config[i].charts[x].data.ordertype}).bind('keyup mouseup', function () {
											$(this).data($(this).attr("name"),$(this).val());
										});										
										$("[name='servertype']",$("#wid-id-datatable_"+ idCounter)).val(config[i].charts[x].data.serverType);
										$("[name='columninfo']",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].data.columninfo);
										if(config[i].charts[x].data.isSelectAll){
											$("[name='isSelectAll']",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].data.isSelectAll);	
										}
										if(config[i].charts[x].isEditable){
											$("[name=isEditable]",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].isEditable);	
										}
										
										$("[name=DataTables_Table_0_length]",$("#wid-id-datatable_"+ idCounter)).val(config[i].charts[x].pageLength||10);
										
										if(config[i].charts[x].enableAudit){
											$("[name=enableAudit]",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].enableAudit);	
										}
										if(config[i].charts[x].enableAdd){
											$("[name=enableAdd]",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].enableAdd);	
										}
										if(config[i].charts[x].enableHistory){
											$("[name=enableHistory]",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].enableHistory)
											.parents(".jarviswidget-editbox").find("[name='historySection']").show();	
										}
										if(config[i].charts[x].enableBulkUpdate){
											$("[name=enableBulkUpdate]",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].enableBulkUpdate);	
										}
										if(config[i].charts[x].enableHighlighter){
											$("[name=enableHighlighter]",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].enableHighlighter);	
										}
										$("[name=commentSectionName]",$("#wid-id-datatable_"+idCounter)).val(config[i].charts[x].commentSectionName);
										$("[name=commentSectionName]",$("#wid-id-datatable_"+idCounter)).val(config[i].charts[x].commentSectionName).data("commentSectionName",config[i].charts[x].commentSectionName).keyup(function(){
											$(this).data($(this).attr("name"),$(this).val());
										});
										if(config[i].charts[x].enableComments){
											$("[name=enableComments]",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].enableComments);
										}
										if(config[i].charts[x].hideNoOfRows){
											$("[name=hideNoOfRows]",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].hideNoOfRows);
										}	
										if(config[i].charts[x].hideShowHide){
											$("[name=hideShowHide]",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].hideShowHide);
										}
										if(config[i].charts[x].hideSearch){
											$("[name=hideSearch]",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].hideSearch);
										}
										if(config[i].charts[x].hideFilter){
											$("[name=hideFilter]",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].hideFilter);
										}
										if(config[i].charts[x].isOverride){
											$("[name=isOverride]",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].isOverride);
										}
										if(config[i].charts[x].hidePagination){
											$("[name=hidePagination]",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].hidePagination);
										}										
										$("[name=enableComments]",$("#wid-id-datatable_"+ idCounter)).on('change',{"idCounter":idCounter},function(e){
											if($("[name=enableComments]",$("#wid-id-datatable_"+ e.data.idCounter)).is(':checked')){
												$("[name=commentSectionName]",$("#wid-id-datatable_"+e.data.idCounter)).parent().show();
												}else{
													$("[name=commentSectionName]",$("#wid-id-datatable_"+e.data.idCounter)).parent().hide();
												}
										});
										
	
										$("[name='enableHistory']",$("#wid-id-datatable_"+ idCounter)).on("change",function(){
											$(this).parents(".jarviswidget-editbox").find("[name='historySection']").toggle();
										});
										$("[name='tableHistory']",$("#wid-id-datatable_"+ idCounter)).select2({
											placeholder : "Select table",
											data : THIS.tableList || []
										}).on("change",function(e) {
											onChangeHistoryTable(e.val,$(this).parents(".jarviswidget-editbox").find("[name='tableForDatatable']").select2("val"),this);
										}).select2("val", (config[i].charts[x].data.history && config[i].charts[x].data.history.table)?config[i].charts[x].data.history.table:'');
										if(config[i].charts[x].data && config[i].charts[x].data.history){
											$("[name=historyColumnsMapping]",$("#wid-id-datatable_"+ idCounter)).data({"mappingcolumns":config[i].charts[x].data.history.mappingcolumns});
											$("[name=historyColumnsMapping]",$("#wid-id-datatable_"+ idCounter)).data({"foregincolumn":config[i].charts[x].data.history.foregincolumn});
											$("[name=historyColumnsMapping]",$("#wid-id-datatable_"+ idCounter)).data({"versionColumn":config[i].charts[x].data.history.versionColumn});
											$("[name=historyColumnsMapping]",$("#wid-id-datatable_"+ idCounter)).data({"statusColumn":config[i].charts[x].data.history.statusColumn});
											$("[name=historyColumnsMapping]",$("#wid-id-datatable_"+ idCounter)).data({"primarykey":config[i].charts[x].data.history.primarykey});
											
											$("[name=historyColumnsMapping]",$("#wid-id-datatable_"+ idCounter)).data({"tableHistoryAlias":config[i].charts[x].data.history.tableHistoryAlias});
											$("[name=historyColumnsMapping]",$("#wid-id-datatable_"+ idCounter)).data({"tableMasterAlias":config[i].charts[x].data.history.tableMasterAlias});
											$("[name=historyColumnsMapping]",$("#wid-id-datatable_"+ idCounter)).data({"hsourcelink":config[i].charts[x].data.history.hsourcelink});
											$("[name=historyColumnsMapping]",$("#wid-id-datatable_"+ idCounter)).data({"msourcelink":config[i].charts[x].data.history.msourcelink});
										}
										if(config[i].charts[x].showAudit){
											$("[name=showAudit]",$("#wid-id-datatable_"+ idCounter)).prop('checked',config[i].charts[x].showAudit);	
										}
										if(config[i].charts[x].editType){
											$("[name=editType]",$("#wid-id-datatable_"+ idCounter)).val(config[i].charts[x].editType);	
										}
										if(config[i].charts[x].editvalue){
											$("[name=editvalue]",$("#wid-id-datatable_"+ idCounter)).val(config[i].charts[x].editvalue);	
										}
										if(config[i].charts[x].fixedLeftColumn){
											$("[name=fixedLeftColumn]",$("#wid-id-datatable_"+ idCounter)).val(config[i].charts[x].fixedLeftColumn);	
										}
										if(config[i].charts[x].fixedRightColumn){
											$("[name=fixedRightColumn]",$("#wid-id-datatable_"+ idCounter)).val(config[i].charts[x].fixedRightColumn);	
										}
										$("[name=defaulttablefilter]",$("#wid-id-datatable_"+ idCounter)).data({"defaulttablefilter":config[i].charts[x].data.filter?config[i].charts[x].data.filter.replace(/@/g, "'"):''}).bind('keyup mouseup',function() {
											$(this).data($(this).attr("name"),$(this).val());});
										$("[name=formTitle]",$("#wid-id-datatable_"+ idCounter)).data({"formTitle":config[i].charts[x].data.formName?config[i].charts[x].data.formName.replace(/@/g, "'"):''}).bind('keyup mouseup',function() {
											$(this).data($(this).attr("name"),$(this).val());});
										$("[name=overrideFunction]",$("#wid-id-datatable_"+ idCounter)).data({"overrideFunction":config[i].charts[x].data.overrideFunction?config[i].charts[x].data.overrideFunction.replace(/@/g, "'"):''}).bind('keyup mouseup',function() {
											$(this).data($(this).attr("name"),$(this).val());});
										
											if (config[i].charts[x].data && config[i].charts[x].data.table){
												$("#tableForDatatable_" + idCounter).select2({
													placeholder : "Choose table",
													data : THIS.tableList || []
												}).on("change",function(e) {
																THIS.dataTableCounter = e.target.id
																		.replace("tableForDatatable_", "");
																$(e.target).attr("data-tablename", e.val);
																datatableSelect(e.val,true);
														}).select2("val", config[i].charts[x].data.table);
												$("#tableForDatatable_" + idCounter).attr("data-tablename", config[i].charts[x].data.table);
												datatableSelect(config[i].charts[x].data.table,false,idCounter);
											}
											var actionDataLength = 0;
											var actionDisplayType = "inline";
											var actionColorCode = "btn-success";
											if(config[i].charts[x].data.actionData && config[i].charts[x].data.actionData.length > 0){
												actionDataLength = config[i].charts[x].data.actionData.length;
											}
											if(config[i].charts[x].data.actionDisplayType && config[i].charts[x].data.actionDisplayType !==undefined){
												actionDisplayType = config[i].charts[x].data.actionDisplayType;
											}
											if(config[i].charts[x].data.actionColorCode && config[i].charts[x].data.actionColorCode !==undefined){
												actionColorCode = config[i].charts[x].data.actionColorCode;
											}
											$(".select_actions",$("#wid-id-datatable_"+ idCounter)).select2().on('change',idCounter,function(e){
												var actionListLength  = $("#actionPanelForm",$("#wid-id-datatable_"+ e.data)).children().length;
												if(e.val<=actionListLength){
													for(var i=actionListLength; i>e.val; i--){
														$($("#actionPanelForm",$("#wid-id-datatable_"+ e.data)).children()[i-1]).remove();
													}
												}else{
													for(var i=actionListLength; i<e.val; i++){
														$("#actionPanelForm",$("#wid-id-datatable_"+ e.data)).append('<fieldset class="col-md-4 col-xs-4 col-sm-4 col-lg-4"><input class="form-control" type="text" name="action_'+e.data +'_'+ i+'"> <span class="note"><i class="fa fa-check text-success"></i> Action Title</span>	<input class="form-control" type="text" name="actionCallback_'+e.data +'_'+i+'"> <span class="note"><i class="fa fa-check text-success"></i> Callback function on Action</span> </fieldset>');
													}
												}
											}).select2('data',{id:actionDataLength,text:actionDataLength});
											
											$(".select_displaytype",$("#wid-id-datatable_"+ idCounter)).select2().on('change',function(){
												$(this).data($(this).attr("name"),$(this).val());
											}).select2('val',actionDisplayType);
											$(".select_displaytype",$("#wid-id-datatable_"+ idCounter)).data("actiondisplaytype",actionDisplayType);
											if (config[i].charts[x].data && config[i].charts[x].data.actionData && config[i].charts[x].data.actionData.length > 0){
												for(var j=0; j<config[i].charts[x].data.actionData.length; j++){
													$("#actionPanelForm",$("#wid-id-datatable_"+ idCounter)).append('<fieldset class="col-md-4 col-xs-4 col-sm-4 col-lg-4"><input class="form-control" type="text" name="action_'+idCounter +'_'+ j+'"> <span class="note"><i class="fa fa-check text-success"></i> Action Title</span>	<input class="form-control" type="text" name="actionCallback_'+idCounter +'_'+j+'"> <span class="note"><i class="fa fa-check text-success"></i> Callback function on Action</span> </fieldset>');
													$("#actionPanelForm",$("#wid-id-datatable_"+ idCounter)).find('[name=action_'+idCounter+'_'+j+']').val(config[i].charts[x].data.actionData[j].action);
													var actionCallback = config[i].charts[x].data.actionData[j].actionCallback.replace(/@/g,"'");
													$("#actionPanelForm",$("#wid-id-datatable_"+ idCounter)).find('[name=actionCallback_'+idCounter+'_'+j+']').val(actionCallback);
												}
											}
											if(config[i].charts[x].data && config[i].charts[x].data.pivotSettings) {
												$("#wid-id-datatable_"+ idCounter).data("pivotSettings", config[i].charts[x].data.pivotSettings);
											}
											if(config[i].charts[x].data && config[i].charts[x].data.rule) {
												$("#wid-id-datatable_"+ idCounter).data("rule", config[i].charts[x].data.rule);
											}
											if(config[i].charts[x].data && config[i].charts[x].data.workflow) {
												$("#wid-id-datatable_"+ idCounter).data("workflow", config[i].charts[x].data.workflow);
											}
											$(".select_actioncolor",$("#wid-id-datatable_"+ idCounter)).select2().on('change',function(){
												$(this).data($(this).attr("name"),$(this).val());
											}).select2('val',actionColorCode).data("actioncolorcode",actionColorCode);
											var filtermapping = config[i].charts[x].filtermapping;
											$("#wid-id-datatable_"+ idCounter).data("filtermapping",filtermapping);
											$("#configure_"+idCounter).data("counter",idCounter).unbind("click").bind("click",function(){
												configureColumns(this, tableName,response,columnList,filtermapping);
											});
// if (config[i].charts[x].data.groupbyenabled === true ||
// config[i].charts[x].data.groupbyenabled ==="true"){
// $("#configure_"+idCounter).data("groupby-aggregate",config[i].charts[x].data.groupbyaggregate);
// $("#configure_"+idCounter).data("groupby-columns",config[i].charts[x].data.groupbycolumns);
// $("#configure_"+idCounter).data("groupby-enabled",config[i].charts[x].data.groupbyenabled);
// }
//											
// if (config[i].charts[x].data.summaryenabled === true ||
// config[i].charts[x].data.summaryenabled ==="true"){
// $("#configure_"+idCounter).data("summary-enabled",config[i].charts[x].data.summaryenabled);
// $("#configure_"+idCounter).data("summary-aggregate",config[i].charts[x].data.summaryaggregate);
// }
								  }else if (config[i].charts[x].charttype === "imagegrid"){
									  /*
										 * if (x===0)
										 * $(".widget-body",$(widget)).empty();
										 * var tempId = ""; if
										 * (config[i].charts[x].alias) tempId =
										 * config[i].charts[x].alias+"-temp";
										 * $(".widget-body",$(widget)).append(
										 * $("<div/>").addClass(
										 * config[i].charts[x].width).attr({"align":"center"}).attr({"id":tempId}).append(
										 * $("<span/>").css({"top":"90px","position":"relative"})) );
										 */
									  $(widget).find("[name='columnForImageUrl']").select2({
											placeholder : "Select Column",
											data : THIS.ColumnList || []
									  }).select2("val", config[i].charts[x].imageColumn);
									  $(widget).find("[name='columnForFiltered']").select2({
											placeholder : "Select Column",
											data : THIS.ColumnList || []
									  }).select2("val", config[i].charts[x].filteredColumn);
									  $(widget).find("[name='configEditColumn']").select2({
											placeholder : "Select Columns",
											multiple:true,
											maximumSelectionSize:3,
											data : THIS.ColumnList || []
									  }).select2("val", config[i].charts[x].editableColumns);
									  if(config[i].charts[x].actionData && config[i].charts[x].actionData.length>0){
										  for(var ad=0;ad<config[i].charts[x].actionData.length;ad++){
											  addConfigAction($(widget).find("[name='addImageConfigAction']"),config[i].charts[x].actionData[ad]);
										  }
									  }
									  $(widget).find("[name='addImageConfigAction']").unbind("click").bind("click",function(e){
											addConfigAction(this);
									  });
								  } else if (config[i].charts[x].charttype === "calendar"){
										$('#'+"Calendars_"+idCounterCalendar).fullCalendar({
											header: {
												left: 'prev,next today',
												center: 'title',
												right: 'month,basicWeek,basicDay'
											},
											defaultDate: System.getFormattedDate(new Date()),
											editable: true,
											eventLimit: true, // allow "more"
																// link when too
																// many events
											events: []
										});
										calendarTableSelect(config[i].charts[x].table,config[i].charts[x],idCounterCalendar);
										pageSetUp();
										$("#tableForCalendar_" + idCounterCalendar)
												.select2({
													placeholder : "Choose table",
													data : THIS.tableList || []
												})
												.on("change",function() {
															THIS.calendarCounter = e.target.id
																	.replace("tableForCalendar_","");
															$(e.target).attr("data-tablename",e.val);
															calendarTableSelect(e.val);
														}).select2("val", config[i].charts[x].table);
										
								  } else if (config[i].charts[x].charttype === "NGram"){
										if(!THIS.tableField) {
											THIS.tableField = THIS.ColumnList[0].text;
										}
									  $("#tableNGram").select2({
											placeholder : "Choose table",
											data : THIS.tableList || []
										}).on('change', function(e) {
											var request = {
												"source" : THIS.sourceType,
												"database" : THIS.database,
												"table" : $("#tableNGram").select2("val")
											};
											enableLoading();
											callAjaxService("getColumnList", callBackSuccess,callBackFailure, request, "POST");
											function callBackSuccess(response){ 
												var tableFields = [];
												if(response.length>0){
													for ( var i = 0; i < response.length; i++) {
														tableFields.push({
															"id" : response[i].columnName,
															"text" : response[i].columnName
														});
													}
												}
												$("#field").select2({
													placeholder : "Select field",
													data : tableFields || []
												}).on('change', function(e){
													THIS.tableField = e.val;
												}).select2('val', THIS.tableField);
											}
											function callBackFailure(error){ console.log(error);}
										}).select2('val', THIS.table);
										
										$("#field").select2({
											placeholder : "Select field",
											data : THIS.ColumnList || []
										}).select2('val', THIS.tableField);
											
										$("#gramType").select2({
											placeholder : "Select Gram",
											data :  [{"id":"1","text":"Uni-grams"},{
													"id":"2","text":"Bi-grams"},{
													"id":"3","text":"Tri-grams"},{
													"id":"4","text":"4-grams"},{
													"id":"5","text":"5-grams"}]
										}).select2('val', "3");
										
										if(config[i].charts[x].ngramSynonyms) {
											$("[name=ngramSynonyms]",$("#wid-id-NGram")).prop('checked',config[i].charts[x].ngramSynonyms);	
										}
										if(config[i].charts[x].ngramStopwords) {
											$("[name=ngramStopwords]",$("#wid-id-NGram")).prop('checked',config[i].charts[x].ngramStopwords);	
										}
										if(config[i].charts[x].ngramLemmatization) {
											$("[name=ngramLemmatization]",$("#wid-id-NGram")).prop('checked',config[i].charts[x].ngramLemmatization);	
										}
										if(config[i].charts[x].ngramMoreFilters) {
											$("[name=ngramMoreFilters]",$("#wid-id-NGram")).prop('checked',config[i].charts[x].ngramMoreFilters);	
										}
								  } else if (config[i].charts[x].charttype === "IFrame"){
									  if(config[i].charts[x].IFrameURL) {
										  $("[name=IFrameURL]",$("#wid-id-IFrame_"+idCounterIFrame)).val(config[i].charts[x].IFrameURL);	
									  }
   								  } else if (config[i].charts[x].charttype === "HTMLTemplate"){
   									  if(config[i].charts[x].HTMLTemplateContent) {
										  CKEDITOR.replace( 'HTMLTemplateContent_'+idCounterHTMLTemplate, { 
											  extraPlugins: 'base64image',
											  removeButtons: 'Image',
											  // Upload images to a CKFinder
												// connector (note that the
												// response type is set to
												// JSON).
											  uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
											  startupFocus : true,
											  enterMode : CKEDITOR.ENTER_BR,
											  shiftEnterMode: CKEDITOR.ENTER_P
										  });
										  CKEDITOR.instances['HTMLTemplateContent_'+idCounterHTMLTemplate].setData(config[i].charts[x].HTMLTemplateContent);
									  }
   									  if(config[i].charts[x].contentLayout) {
										  $("[name=contentLayout]",$("#wid-id-HTMLTemplate_"+idCounterHTMLTemplate)).prop('checked', config[i].charts[x].contentLayout);	
									  }
   								  }
							  }
						  }
						  if(config[i]["class"].indexOf("jarviswidget-collapsed") > -1){
							  widget.addClass("jarviswidget-collapsed");
							  widget.find("header").next().hide();
						  }
					  };
					  if (templates.getWidget.template && templates.getWidget.template !== null){
						  drawEditProfileWidget();
						}
						else{
							templates.getWidget(tempoptions,drawEditProfileWidget);
						}
				  }
			  }
			  if (!$("#droppable > article").is(':empty')){
				  $('[id^=widget-grid]')
				.jarvisWidgets('destroy');
			  }
			  pageSetUp();
			  $(".chart-div")
				.droppable(
						{
							activeClass : "ui-state-default",
							hoverClass : "ui-state-hover",
							accept : ":not(.btn-fact,.chart-parent-div,.dd-item.ui-sortable-handle)",
							drop : function(event, ui) {
								$(this).find(".dimension-placeholder").remove();
								if ($(this).is(":empty")) {
									if (!ui.draggable.hasClass('chart-div'))
										chartDrop(this, ui);
									else {
										$("[rel=popover]").popover('destroy');
										$(this).html(ui.draggable.html());
										ui.draggable.find(".axisHeader").removeData("bs.popover");
										$(this).find(".axisHeader")
												.data(ui.draggable.find(".axisHeader").data());
										$(".axisHeader",$(this)).bind("click",
														function() {
															axisLabelClick(this);
														});
										$(".chart-remove", $(this)).unbind("click").bind("click", function() {removeChart(this);});
										ui.draggable.empty().append($("<span/>").addClass("dimension-placeholder"));
									}
								} else {
									var tempHTML = $(this).html();
									$(this).find(".axisHeader")
											.removeData("bs.popover");
									var tempData = $(this).find(".axisHeader").data();
									$(this).empty();
									$(this).html(ui.draggable.html());
									ui.draggable.find(".axisHeader")
											.removeData("bs.popover");
									$(this).find(".axisHeader")
											.data(ui.draggable.find(".axisHeader").data());
									ui.draggable.empty().html(tempHTML);
									ui.draggable.find(".axisHeader").data(tempData);
									$(".axisHeader",$(this)).bind("click",
													function() {
														axisLabelClick(this);
													});
									$(".axisHeader",ui.draggable).bind("click",
													function() {
														axisLabelClick(this);
													});
									$(".chart-remove", $(this)).unbind("click").bind("click", function() {removeChart(this);});
									$(".chart-remove", ui.draggable).unbind("click").bind("click", function() {removeChart(this);});
								}
							}
						});
			  $(".chart-remove").unbind("click").bind("click", function() {removeChart(this);});
	};
	var callBackGetDatabaseList = function(response) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			var databaseList = [];
			for ( var i = 0; i < response.length; i++) {
				databaseList.push({
					"id" : response[i],
					"text" : response[i]
				});
			}
			$("#database").select2({
				placeholder : "Choose Database",
				data : databaseList
			}).on("change", function(e) {
				tableSelect(e.val);
			});
			$("#table").select2({
				placeholder : "Choose Table",
				data : []
			});
		}
	};
	var tableSelect = function(database,isEditProfile) {
		THIS.database = database;
		var request = {
			"source" : THIS.sourceType,
			"database" : database
		};
		enableLoading();
		callAjaxService("getTableList", function(response){callBackGetTableList(response,isEditProfile);}, callBackFailure,
				request, "POST");
	};
	var callBackGetTableList = function(response,isEditProfile) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			THIS.tableList = [];
			for ( var i = 0; i < response.length; i++) {
				THIS.tableList.push({
					"id" : response[i],
					"text" : response[i]
				});
			}
			$("#table").select2({
				placeholder : "Choose table",
				data : THIS.tableList
			});

			if (typeof THIS.config === "undefined" && $("#tableForDatatable")
					&& $("#tableForDatatable").length > 0) {
				THIS.datatable = THIS.datatable || THIS.table;
				$("#tableForDatatable").select2({
					placeholder : "Choose table",
					data : THIS.tableList
				}).on("change", function(e) {
					datatableSelect(e.val,true);
				}).select2("val", THIS.datatable);
			}
		}
		if (isEditProfile)
			proceedColumn(true);
		
	
	};
	var proceedColumn = function(isEditProfile) {

		THIS.isEditProfile = isEditProfile;
		if (!isEditProfile && !THIS.dataQuery) {
			if (!$("#frmConf").valid())
				return;
			THIS.table = $("#table").val();
		}
		var request = {
			"source" : THIS.sourceType,
			"database" : THIS.database
		};
		if(THIS.dataQuery){
			request.query = calculateDynamicFilter(THIS.dataQuery,THIS.filters);
		}else{
			request.table = THIS.table;
		}
	//	$("#sourceTableDesc").html(THfIS.ProfileName+" : "+THIS.sourceType+" - "+THIS.database+" - "+THIS.table);
		enableLoading();
		callAjaxService("getColumnList", callBackGetColumnList,
				callBackFailure, request, "POST");
		
	};
	var callBackGetColumnList = function(response) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		THIS.ColumnList = [],THIS.dimensionData = {},THIS.measureData= {},THIS.customMeasureList = [];
		if (response !== undefined && response !== null) {
			THIS.measureData["_count"]= "_count";
			$("#selectTable").hide(300);
			$("#divDataSource").hide(300);
			$("#drawProfile").show(300);
			if (THIS.sourceType === "hive") {
				$("#tableInfo").append($("<h1>").addClass("page-title txt-color-blueDark").html("HIVE")
										.prepend($("<i/>").addClass("fa-fw fa fa-cloud"))
										.append($("<span/>").html(" &gt;   "+ THIS.database.toUpperCase()+ "."+ (THIS.table?THIS.table.toUpperCase():''))));
			}
			$(".widget-body", $("#wid-id-measures")).empty();
			$("#columnList,#facts").find("[name='txtsearchColumns']").remove();
			$("#columnList,#facts").prepend($("<input/>").addClass("form-control").attr({"placeholder":"Search Columns","name":"txtsearchColumns"}));
			$("#columnList [name='txtsearchColumns'],#facts [name='txtsearchColumns']").keyup(function () {
	            var rex = new RegExp($(this).val(), 'i');
	            $(this).parent().find("li").hide();
	            $(this).parent().find("li").filter(function () {
	            	 return rex.test($(this).text());
	            }).show();
	        });
			$("#columnList ol").empty();
			
			$("#facts").append($("<ul/>").addClass("list-unstyled"));
			var stringDatatypes = [ "string", "character varying", "varchar","character", "char", "text" ];
			for ( var i = 0; i < response.length; i++) {
				THIS.ColumnList.push({
					"id" : response[i].columnName,
					"text" : response[i].columnName
				});
				if (response[i].dataType && stringDatatypes.contains(response[i].dataType.toLowerCase())) {
					$("#columnList ol").append(
							$("<li/>").addClass("dd-item").data("id",
									response[i].columnName).append(
									$("<div/>").addClass("dd-handle").attr({
										"title" : response[i].columnName
									}).html(response[i].columnName)));
					THIS.dimensionData[response[i].columnName] = response[i].columnName;
				} else {
					$(".widget-body", $("#wid-id-measures")).find("ul").append($("<li/>").addClass("dd-item")
							.append($("<a/>").addClass("btn-fact dd-handle").attr({"href" : "javascript:void(0);","title" : response[i].columnName}).html(response[i].columnName)));
					THIS.measureData[response[i].columnName] = response[i].columnName;
				}
			}
			if(THIS.customFacts && THIS.customFacts.length>0){	
				THIS.customFacts.forEach(function(customfact){
					if(customfact.customtype === "measure"){
						$("#facts").find("ul").append($("<li/>").attr({"name":customfact.name}).addClass("dd-item ui-draggable ui-draggable-handle active")
								.data("formula",customfact.formula).data("customtype",customfact.customtype)
								.append($("<a/>").addClass("btn-fact dd-handle")
										.attr({"href" : "javascript:void(0);","title" : customfact.name}).html(customfact.name).append($("<i/>").addClass("fa fa-md fa-fw fa-remove txt-color-red cursor-pointer pull-right").attr({"name":"removecustomfact"}))));
						THIS.measureData[customfact.name] = customfact.name;
					}else{
						$("#columnList").find("ol").append($("<li/>").attr({"name":customfact.name}).addClass("dd-item ui-draggable ui-draggable-handle active")
								.data("formula",customfact.formula).data("customtype",customfact.customtype)
								.append($("<div/>").addClass("dd-handle").html(customfact.name).append($("<i/>").addClass("fa fa-md fa-fw fa-remove txt-color-red cursor-pointer pull-right").attr({"name":"removecustomfact"}))));
						THIS.dimensionData[customfact.name] = customfact.name;
					}
					// btn btn-success btn-sm margin-right-5
					/*
					 * $(".widget-body", $("#wid-id-measures")) .append($("<a/>").addClass("btn
					 * btn-info txt-color-white btn-sm margin-right-5 customfact
					 * btn-fact").attr({"href" :
					 * "javascript:void(0);","data-fact":customfact.fact,"data-displayname":customfact.displayname,"title" :
					 * customfact.fact}).html(customfact.displayname));
					 * THIS.customMeasureList.push(customfact.fact);
					 */
				});
			}
			/*
			 * var lists = $("#columnList").find("li").sort(function(a, b) {
			 * return
			 * $(a).text().toUpperCase().localeCompare($(b).text().toUpperCase());
			 * }); $("#columnList ol").empty().append(lists);
			 */
			$("[name='removecustomfact']").unbind('click').bind('click',function(e){
				var factName = $(this).parents("li").attr("name");
				$(this).parents("li").remove();
				e.preventDefault();e.stopPropagation();
				THIS.customFacts.forEach(function(customfact){
					if(customfact.name === factName){
						THIS.customFacts.splice(THIS.customFacts.indexOf(customfact), 1);
					}
				});
			});
			$("#columnList li,#facts a").draggable({
				appendTo : "body",
				helper : "clone"
			});
			
			$("#columnList li,#facts li").bind("click",addEditCustomFactHandler);
			$("#addFacts").droppable({
			      activeClass: "ui-state-default",
			      hoverClass: "ui-state-hover",
			      accept: "#facts .btn-fact",
			      drop: function( event, ui ) {
			    	  var $factElement = $(ui.draggable.clone());
			    	  $factElement.removeClass("dd-handle").addClass("btn btn-success btn-sm margin-right-5");
			    	  $factElement.find("[name='removecustomfact']").remove();
			    	  $factElement.attr("data-toggle","popover");
			    	  $factElement.on('inserted.bs.popover', function () {
			    		$('.popover-content').append('<div class="col col-6"/>')
			    							 .append('<input type="text" class="form-control" name="factname" disabled="disabled">')
			    							 .append('<div class="col col-6"/>')
			    							 .append('<input type="text" class="form-control" name="factdisplayname" placeholder="Enter Fact Displayname">');
			    		
			    		$('input[name="factname"]').val($factElement.attr("data-fact"));
			    		$('input[name="factdisplayname"]').val($factElement.attr('data-displayname')).change(function(){
			    			$factElement.attr("data-displayname",$(this).val());
			    			$factElement.text($(this).val());
			    		});
			    	  });
			    	  $factElement.popover({
			    		  placement: 'auto',
			    		  delay :  { "show": 100, "hide": 100 },
			    		  trigger : 'click',
			    		  title : "Configure Fact"
			    	  });
			    	  $factElement.attr("data-fact",$factElement.attr("data-fact")||$factElement.attr("data-original-title"));
			    	  $factElement.attr("data-displayname",$factElement.attr("data-displayname")|| $factElement.attr("data-original-title"));
			    	  $factElement.attr({"data-original-title":"Configure Measure","rel":"popover"});
	    	  		if($(this).html()==="drop measure here"){
	    	  			$(this).empty().append($factElement);
	    	  		}else{
	    	  			$(this).append($factElement);
	    	  		}
			      }
			    });
			
			$( "#addFacts" ).sortable();
			if (THIS.isEditProfile){
				
				drawConfig(THIS.config);
				drawFilters(THIS.filters);
				if(THIS.additionalconfig && THIS.additionalconfig.isReportDownload){
					$("#addDownloadReport").prop('checked', true);
					drawReportDownload();
				}
				
				constructSummary(THIS.Summary);
			} else {
				$.removeData($("#saveProfile")[0], "id");
				$("#filters").empty();
				drawFilter();
				$("#addContainer").trigger("click");
			}
		}
	};
	
	var drawReportDownload =function(){
		count++;
		$('#reportDownload').show();
		 var str = '<form id="actionPanelDownload"></form>';
     	$('#actionBody').find('#filtersMap').before(str);
			var actionDataLength = 0;
			var actionDisplayType = "inline";
			var actionColorCode = "btn-success";
			
			if(THIS.additionalconfig.reportConfig && THIS.additionalconfig.reportConfig.actioncolorcode){
				actionColorCode = THIS.additionalconfig.reportConfig.actioncolorcode;
			}
			
			if(THIS.additionalconfig.reportConfig && THIS.additionalconfig.reportConfig.actiondisplaytype){
				actionDisplayType = THIS.additionalconfig.reportConfig.actiondisplaytype;
			}
			
			if(THIS.additionalconfig.isReportDownload.length> 0){
				actionDataLength = THIS.additionalconfig.isReportDownload.length;
			}
			$(".report_actionValue").select2().on('change',function(e){
				var actionListLength  = $("#actionPanelDownload",$("#reportDownload")).children().length;
				if(e.val<=actionListLength){
					for(var i=actionListLength; i>e.val; i--){
						$($("#actionPanelDownload",$("#reportDownload")).children()[i-1]).remove();
					}
				}else{
					for(var i=actionListLength; i<e.val; i++){
						$("#actionPanelDownload",$("#reportDownload")).append('<fieldset class="col-md-4 col-xs-4 col-sm-4 col-lg-4"><input class="form-control" type="text" name="action_'+ i+'"> <span class="note"><i class="fa fa-check text-success"></i> Action Title HTML</span></fieldset>');
					}
				}
			}).select2('data',{id:actionDataLength,text:actionDataLength});
			
			$(".report_displaytype",$("#reportDownload")).select2().on('change',function(){
				$(this).data($(this).attr("name"),$(this).val());
			}).select2('val',actionDisplayType);
			$(".report_actioncolor",$("#reportDownload")).data("actionColorCode",actionColorCode);
			
			$(".report_actioncolor",$("#reportDownload")).select2().on('change',function(){
				$(this).data($(this).attr("name"),$(this).val());
			}).select2('val',actionColorCode).data("actioncolorcode",actionColorCode);
			if (THIS.additionalconfig.isReportDownload.length > 0){
				for(var j=0; j<THIS.additionalconfig.isReportDownload.length; j++){
					$("#actionPanelDownload",$("#reportDownload")).append('<fieldset class="col-md-4 col-xs-4 col-sm-4 col-lg-4"><input class="form-control" type="text" name="action_'+ j+'"> <span class="note"><i class="fa fa-check text-success"></i> Action Title HTML</span> </fieldset>');
					$("#actionPanelDownload",$("#reportDownload")).find('[name=action_'+j+']').val(THIS.additionalconfig.isReportDownload[j].action.replace(/@/g,"'"));
					// var actionCallback =
					// THIS.additionalconfig.isReportDownload[j].actionCallback.replace(/@/g,"'");
					// $("#actionPanelDownload",$("#reportDownload")).find('[name=actionCallback_'+j+']').val(actionCallback);
				}
			}
			
			if(THIS.additionalconfig.applicableFilters && THIS.additionalconfig.applicableFilters.length > 0){
				
				var filterColumn=[] ;
				for(var k=0;k<dataAnalyser.filters.length;k++){
					filterColumn.push({"id":dataAnalyser.filters[k].fieldName,"text":dataAnalyser.filters[k].fieldName});
				}
				for(var i=0;i<THIS.additionalconfig.applicableFilters.length;i++){
				var	profilesDiv = $("<div/>").attr({"name":"mappedFilter"}).addClass("row");
				var columnsSelect = $("<input/>").attr({"name":"appliedFilter"}).addClass("width-100per");				
				profilesDiv.append($("<div/>").addClass("col-md-3")
						.append($("<strong/>").addClass("note").html("Filter"))
						.append(columnsSelect));				
				$(columnsSelect).select2({
					placeholder : "Select Column",
					allowClear: true,
					data :  filterColumn
				}).select2("val", THIS.additionalconfig.applicableFilters[i]);
				$("#btnFilters",$("#actionBody")).append(profilesDiv);
						}
				
			}
	}
	
	var selectedValQuery = function(obj) {
	var currentObj = $(obj).parent().parent();
	if($(obj).find("[name='defaultQueryValue']").is(':checked')){
		currentObj.find('[name="defaultQuerydiv"]').css({'display': 'block'})
	}else{
		currentObj.find('[name="defaultQuerydiv"]').css({'display': 'none'})	
	}
	};
	
	var drawFilter = function(fields,obj) {
		$( "#dialog" ).hide();	
		$(".lastRange,.nextRange").attr("disabled", true);
		var tempObj = $("<div/>").addClass("well col-sm-12 col-md-12 margin-bottom-10 padding-bottom-10").attr({'id':'f'+($("#filters").find(".well").length+1)});
		tempObj.append($("<div/>").addClass("col col-2").append($("<div/>").addClass("note padding-2")
				.append($("<strong/>").html("Field"))).append($("<label/>").addClass("input")
						.append($("<input/>").attr({type : "text",name : "field"}).addClass("width-100per").val(""))))
				.append($("<div/>").addClass("col col-2").append($("<div/>").addClass("note padding-2")
						.append($("<strong/>").html("Display Name")))
								.append($("<label/>").addClass("input")
										.append($("<input/>").attr({type : "text",name : "displayname"}).val(""))))
				.append($("<div/>").addClass("col col-1").append($("<label/>").addClass("note padding-2")
						.append($("<strong/>").html("Type"))).append($("<Select/>").change(function() {
									onChangeTypeInFieldConfig(this);
								}).addClass("form-control input-sm").attr({
									name : "fieldtype" 
								}).append($("<option/>").attr("value", "TEXT").html("Text"))
								.append($("<option/>").attr("value", "DATE").html("Date"))
								.append($("<option/>").attr("value", "dynamicDate").html("Dynamic Date"))
								.append($("<option/>").attr({"value":"dateRange"}).html("Date Range"))
								.append($("<option/>").attr("value", "NUM").html("Number"))
								.append($("<option/>").attr("value", "lookup").html("Look Up"))
								.append($("<option/>").attr("value", "lookupdropdown").html("Advancd Look Up"))
								.append($("<option/>").attr("value", "select").html("Drop Down"))
								.append($("<option/>").attr("value","raw").html("Raw"))))
				.append($("<div/>").addClass("col col-4")						
						.append($("<div/>").addClass("note padding-2")
						.append( $("<strong/>").html("Default Value"))
						.append($("<div/>").css({"margin-top":"-10px"}).addClass("pull-right").append($("<label/>").addClass("toggle")
								.append( $("<strong/>").addClass("note").html("Include null/empty"))
								.append( $('<input type="checkbox" name="includenull"/>'))
								.append($("<i/>").attr({"data-swchon-text":"On","data-swchoff-text":"Off"})))))
								.append($("<label/>").attr({"name":"defaultvaluelabel"}).addClass("input").append($("<input/>").attr({type : "text",name : "defaultvalue"}))))
				.append($("<div/>").addClass("col col-1").append($("<div/>").addClass("note").append($("<strong/>").html("Invisible")))
						.append($("<label/>").addClass("toggle").append($("<input/>").attr({"type":"checkbox","name":"invisible"}))
								.append($("<i/>").attr({"data-swchon-text":"On","data-swchoff-text":"Off"}))))				
				.append($("<div/>").addClass("col col-1").append($("<div/>").addClass("note").append($("<strong/>").html("Mandatory")))
						.append($("<label/>").addClass("toggle").append($("<input/>").attr({"type":"checkbox","name":"isMandatory"}))
								.append($("<i/>").attr({"data-swchon-text":"On","data-swchoff-text":"Off"}))))				
				.append($("<div/>").addClass("col col-1")
								.append($("<div/>").addClass("note").append($("<strong/>").html("Add")))
								.append($("<i/>").addClass("fa fa-md fa-fw fa-plus-square color-blue cursor-pointer").attr("title", "add link")
												.unbind("click").bind("click",function(){drawFilter(fields,this);}))
								.append($("<i/>").addClass("fa fa-md fa-fw fa fa-remove txt-color-red cursor-pointer").attr("title", "remove link")
												.unbind("click").bind("click",function() {removeFilter(this);})))
						.append($("<div/>").addClass("col col-8").attr({"name":"lookupdiv"}).append($("<div/>").addClass("note").attr({"id":"lookupId"})
								.append($("<strong/>").html("Look Up Query")))
								.append($("<label/>").addClass("input").append($("<input/>").attr({type : "text",name : "lookup",id:"lookUpId"}))))
						.append($("<div/>").addClass("col col-1").attr({"name":"lookupdiv"}).append($("<div/>").addClass("note")
								.append($("<strong/>").html("Select Type")))
								.append($("<label/>").addClass("input").append($("<select/>").addClass("input-sm")
										.append($("<option/>").attr("value", "").html("Multiple"))
										.append($("<option/>").attr("value", "true").html("Single")).attr({name : "selecttype"}))))
						.append($("<div/>").addClass("col col-1").append($("<div/>").addClass("note")
								.append($("<strong/>").html("Search Type")))
								.append($("<label/>").addClass("input").append($("<select/>").addClass("input-sm").attr({name : "searchtype"})
										.append($("<option/>").html("In").attr("value","in"))
										.append($("<option/>").html("Not In").attr("value","not in"))
										.append($("<option/>").html("Like").attr("value","like"))
										.append($("<option/>").html("Not Like").attr("value","not like")))))
						.append($("<div/>").addClass("col col-1").append($("<div/>").addClass("note")
								.append($("<strong/>").html("Show Type")))
								.append($("<label/>").addClass("toggle").append($("<input/>").attr({"type":"checkbox","name":"innotin"}))
										.append($("<i/>").attr({"data-swchon-text":"On","data-swchoff-text":"Off"}))))
						.append($("<div/>").addClass("col col-1").attr({"name":"defaultValSwitch"}).append($("<div/>").addClass("note")
								.append($("<strong/>").html("Default Query")))
								.append($("<label/>").addClass("toggle").append($("<input/>").attr({"type":"checkbox","name":"defaultQueryValue"})).unbind("click").bind("click",function() {selectedValQuery(this);})
										.append($("<i/>").attr({"data-swchon-text":"On","data-swchoff-text":"Off"}))))
						.append($("<div/>").addClass("col col-1").css({"padding": "20px"}).append($("<div/>").addClass("note")
								.append($("<strong/>").html("Chat")))
								.append($("<label/>").addClass("toggle").append($("<input/>").attr({"type":"checkbox","name":"chat"})).unbind("click").bind("click",function() {selectedValQuery(this);})
										.append($("<i/>").attr({"data-swchon-text":"On","data-swchoff-text":"Off"}))))									
						
						
						.append($("<div/>").addClass("col col-8").attr({"name":"defaultQuerydiv"}).css({'display':'none'}).append($("<div/>").addClass("note").attr({"id":"defaultQueryId"})
								.append($("<strong/>").html("Default Value Query")))
								.append($("<label/>").addClass("input").append($("<input/>").attr({type : "text",name : "defaultSelectedVal",id:"defaultSelectedId"}))))
						.append(localStorage.isMultipleProfile?$("<div/>").addClass("col col-2").append($("<div/>").addClass("note padding-2")
								.append($("<strong/>").html("Source"))).append($("<label/>").addClass("input")
								.append($("<input/>").attr({type : "text",name : "Source"}).change(function() {
									onChangeTypeInFieldConfigs(this);
								}).addClass("width-100per").val(""))):'')
						.append(localStorage.isMultipleProfile?$("<div/>").addClass("col col-2").append($("<div/>").addClass("note padding-2")
								.append($("<strong/>").html("Database"))).append($("<label/>").addClass("input")
								.append($("<input/>").attr({type : "text",name : "Database"}).addClass("width-100per").val(""))):'')
						
							.append($("<div/>").addClass("col col-2").append($("<div/>").addClass("note padding-2")
				.append($("<strong/>").html("Inter Dependent Columns"))).append($("<label/>").addClass("input")
						.append($("<input/>").attr({type : "text",name : "interdependentcolumns"}).addClass("width-100per").val(""))))
						
		if (obj != null){
			$("#"+$(obj).parent().parent().attr("id")).after(tempObj);
		}
		else{
			$("#filters").append(tempObj);
		}
		
		// adding source types
		var sourceList = [];
		var databaseList = ["Hive", "PostgreSQL"];
		for ( var i = 0; i < databaseList.length; i++) {
			sourceList.push({
				"id" : databaseList[i],
				"text" : databaseList[i]
			});
		}
		$(tempObj).find("[name=Source]").select2({
			allowClear: true,
			placeholder : "Choose field",
			data : sourceList
		});
		$(tempObj).find("[name=field]").select2({
			allowClear: true,
			placeholder : "Choose field",
			data : fields || THIS.ColumnList
		});
		$(tempObj).find("[name=interdependentcolumns]").select2({
			allowClear: true,
			placeholder : "Choose field",
			// "class":"multiselect-item multiselect-all",
			multiple:true,
		    data : fields || THIS.ColumnList
            
			
		});
	
		return tempObj;
	};	
	var removeFilter = function(obj) {
		$(obj).parents(".well:first").remove();
	};
	var formatforDate = $('#filters').find("[name='lookupdiv']").find("[name='lookup']").val();
	
	// onchange of source
	var onChangeTypeInFieldConfigs = function(obj, selectedValue, databaseValue){
		var source = selectedValue?selectedValue:$(obj).parent().parent().parent().find("[name='Source']").select2('val');
		request = {
				"source" : source
			};
		callAjaxService("getDatabaseList",function(response){
			if (response !== undefined && response !== null) {
				var databaseList = [];
				for ( var i = 0; i < response.length; i++) {
					databaseList.push({
						"id" : response[i],
						"text" : response[i]
					});
				}
				
				 if(selectedValue==null) 
					 databaseList =obj.value?databaseList:[];
				// on load
				if(selectedValue){
				$(obj).find("[name='Database']").select2({
					placeholder : "Databases",
					data : databaseList
				});
				}
				else{
					$(obj).parent().parent().parent().find("[name='Database']").select2({
						placeholder : "Databases",
						data : databaseList
					});
				}
				if(selectedValue){
					$(obj).find("[name='Database']").select2('val',databaseValue);
				}
			}
		},
				callBackFailure, request, "POST", null, true);
	
	}
	

	var onChangeTypeInFieldConfig = function(obj, data) {
		var inputLabel = $(obj).parent().parent().find("[name='defaultvaluelabel']");
		// $(obj).parent().next().find("label:eq(1)");
		
		$(inputLabel).removeClass().empty();
		if ($(obj).val() === 'lookup' || $(obj).val() === 'select' || $(obj).val() === 'lookupdropdown') {
			
			// $(inputLabel).addClass("input").append($("<input/>").attr({"type":"text","name":"defaultvalue"}));
			var mainDiv = $(obj).parent().parent();
			mainDiv.find("[name='lookupdiv']").show().find("select").unbind("change").bind("change",function(){
		constructDefaultValueSelect();
		});
		mainDiv.find("[name='lookupdiv']:eq(0)").find("input").unbind("blur").bind("blur",function(){
			constructDefaultValueSelect();
		});
			
			function constructDefaultValueSelect(){
				var selectType = mainDiv.find("[name='lookupdiv']:eq(1)").find("select").val();
				if(data && data.isSingle){
					selectType = data.isSingle;
				}
				
				// $(obj).parent().parent().find("defaultvaluelabel]").select2('destroy');
				if($(obj).val() === 'select'){
					var selectdata =  mainDiv.find("[name='lookupdiv']:eq(0)").find('input').val() || data.lookupquery,selectedValue = "";
					if(selectdata && typeof(selectdata) !='object'){
						selectdata = Jsonparse(selectdata);
						if(!selectdata || Object.keys(selectdata).length === 0){
							selectdata = [];
							var values =  mainDiv.find("[name='lookupdiv']:eq(0)").find('input').val() || data.lookupquery;
							values = values.split(',');
							if(values && values.length>0){
								for(var i=0;i<values.length;i++){
									selectdata.push({value:values[i],label:values[i]});	
								}								
							}
						}else if(selectdata.filter(function(o){return o.id}).length>0){
							var tempData = [];
							for(var i=0;i<selectdata.length;i++){
								tempData.push({value:selectdata[i].id,label:selectdata[i].text});	
							}
							selectdata = tempData;
						}
					}
					$(obj).parent().parent().find("[name='lookupdiv']:eq(0)").find("strong").html("Enter Select value");
					if (data && data.defaultvalue && Object.keys(data.defaultvalue).length>0) {
						if(data.isSingle){
							if(typeof(data.defaultvalue) === "object" && data.defaultvalue.id){
								selectedValue = data.defaultvalue.id;
							}else{
								selectedValue = data.defaultvalue;
							}
						}else if(data.defaultvalue.length>0){
							selectedValue = data.defaultvalue.map(function(o){return (typeof o === 'object' && o.id)?o.id:o});
						}
					}
					constructDropDown(inputLabel,selectdata,!selectType,selectedValue,"","Select "+ $(obj).parent().prev().find("input").val(),THIS.sourceType,THIS.database);
					
				}else if($(obj).val() === 'lookupdropdown'){
					var selectedData,lookupquery;
					if(data && data.defaultvalue){
						selectedData = data.defaultvalue;
					}
					lookupquery = mainDiv.find("[name='lookupdiv']:eq(0)").find("input").val();
					lookupquery = lookupquery?lookupquery:(data && data.lookupquery)? data.lookupquery:"";
					constructDropDown(inputLabel,lookupquery,!selectType,selectedData,"","Select " + $(obj).parent().prev().find("input").val(),(data && data.source)?data.source:THIS.sourceType,(data && data.database)?data.database:THIS.database);
				}
				else{
					mainDiv.find("[name='lookupdiv']:eq(0)").find("strong").html("Look Up Query");
					var selectedData,lookupquery;
					if(data && data.defaultvalue){
						selectedData = data.defaultvalue;
					}
					lookupquery = mainDiv.find("[name='lookupdiv']:eq(0)").find("input").val();
					lookupquery = lookupquery?lookupquery:(data && data.lookupquery)? data.lookupquery:"";
					constructSelect2($(inputLabel).find("[name='defaultvalue']"),lookupquery,!selectType,selectedData,"",null,"Enter " + $(obj).parent().prev().find("input").val(),THIS.sourceType,THIS.database);	
				}
				
			}
			$(inputLabel).addClass("input").append($("<input/>").attr({
				"type" : "text",
				"name" : "defaultvalue"
			}).addClass("width-100per"));
			constructDefaultValueSelect();			
			
		}else if($(obj).val() === 'raw'){
			$(inputLabel).addClass("textarea textarea-resizable").append($("<textarea/>").addClass("custom-scroll").attr({
				"type" : "text",
				"name" : "defaultvalue"
			}));
		}else if($(obj).val() === 'dateRange'){
			
			$(obj).parent().parent().find($("[name='lookupdiv'] #lookupId")).find('strong').text('Date Format');
			$(inputLabel).addClass("row").append(
					$('<span/>').addClass('col-sm-2 text-right').html('From:'))
					.append($('<input/>').addClass('col-sm-3 input-xs').attr({"name" :"from-defaultRangevalue" , "readonly":true}))
					.append($('<span/>').addClass('col-sm-1 text-right').html('To:')).append(
							$('<input/>').addClass('col-sm-3 input-xs').attr({"name" :"to-defaultRangevalue" , "readonly":true}));
			$("input[name='from-defaultRangevalue'],input[name='to-defaultRangevalue']").click(function(){
		    currentFieldset = $(this).parents('.well');
		    var currentindex=$(this).parent();
            $( "#dialog" ).dialog({
            	autoOpen:false,
    	        width: 500,
    	        resizable: false,
    	        height: 250 ,
    	        open: function(event){
    	        	$('#savedRange').find('span').html('');
    	        	 $('input[type="radio"]').prop('checked', false);
    	        	 $(".nextRange").attr("disabled", true);
				     $(".lastRange").attr("disabled", true);
				     $("[name='betweenRange']").attr("disabled", true).val("");
				     $(".nextRange").val('');
				     $(".lastRange").val('');
    	        }
    	    });
         $( "#dialog" ).data(currentindex);
         $( "#dialog" ).dialog("open");
        	 if((data  && currentFieldset.attr('id') === data.id) && jQuery.isEmptyObject(currentFieldset.data())){
        		 dataAnalyser.storedDateRange(data.defaultvalue);
	          }
        	 else if(!jQuery.isEmptyObject(currentFieldset.data())) {
        		 dataAnalyser.storedDateRange(currentFieldset.data());
	          } 
	});
  } else if ( $(obj).val() === 'DATE' || $(obj).val() === 'dynamicDate' || $(obj).val() === 'NUM') {
	  $(obj).parent().parent().find($("[name='lookupdiv'] #lookupId")).find('strong').text('Look Up Query');
         if($(obj).val() != 'NUM'){
        	 $(obj).parent().parent().find($("[name='lookupdiv'] #lookupId")).find('strong').text('Date Format');
         }
			$(inputLabel).addClass("row").append(
					$('<span/>').addClass('col-sm-2 text-right').html('From:'))
					.append($('<input/>').addClass('col-sm-3 input-xs').attr("name", "from-defaultvalue"))
					.append($('<span/>').addClass('col-sm-1 text-right').html('To:')).append(
							$('<input/>').addClass('col-sm-3 input-xs').attr("name", "to-defaultvalue"));
			if ($(obj).val() === 'DATE') {
				 $(inputLabel).find('input:eq(0)').datepicker(
							{
								defaultDate : "+1w",
								changeMonth : true,
								onClose : function(selectedDate) {
									$(inputLabel).find('input:eq(1)').datepicker(
											"option", "minDate", selectedDate);
								}
							});
					$(inputLabel).find('input:eq(1)').datepicker(
							{
								defaultDate : "+1w",
								changeMonth : true,
								onClose : function(selectedDate) {
									$(inputLabel).find('input:eq(0)').datepicker(
											"option", "maxDate", selectedDate);
								}
							});
			}
			// $(obj).parent().parent().find("[name='lookupdiv']").hide();
		}else {
			$(obj).parent().parent().find($("[name='lookupdiv'] #lookupId")).find('strong').text('Look Up Query');
			$(inputLabel).addClass("input").append($("<input/>").attr({
				"type" : "text",
				"name" : "defaultvalue"
			}));
			// $(obj).parent().parent().find("[name='lookupdiv']").hide();

		}
		if($(obj).val() != 'DATE' || $(obj).val() != 'dynamicDate' || $(obj).val() != 'dateRange' ){
			$(obj).parent().parent().find('div').find($("[name='lookup']#lookUpId")).val('');
		}
	};
    
	
	this.storedDateRange=function(dataObject){
		 var radioOpt = dataObject.currentRangeType ;
		 $('#dialog').find('.tab-content').find('.tab-pane.fade').removeClass('active');
			$('#myTab a[href="#' + radioOpt + '"]').tab('show');
		    $('#'+radioOpt).addClass('active');
		    var radioOption = $('#'+radioOpt).find($("input[type='radio']"));// list
																				// of
																				// radio
																				// buttons
		    var val = dataObject.radioValue; // local storage value
		    var selected = dataObject.selectedRangeDate;
		    
		    for(var i=0;i<radioOption.length;i++){
		      if(radioOption[i].value == val){
		    	  radioOption[i].checked = true; // marking the required
													// radio as checked
		           if(val === 'lastRangeValue' && $("input[name='"+radioOpt+"']:checked").val() === 'lastRangeValue' ){
				    	$('#'+radioOpt ).find('#'+radioOpt+'LastRange').val(selected);
				    	$('#'+radioOpt+'NextRange').attr("disabled", true);
			            $('#'+radioOpt+'LastRange').attr("disabled", false);
			       } else if(val === 'nextRangeValue' && $("input[name='"+radioOpt+"']:checked").val() === 'nextRangeValue' ){
				    	$('#'+radioOpt ).find('#'+radioOpt+'NextRange').val(selected);
						$('#'+radioOpt+'NextRange').attr("disabled", false);
				        $('#'+radioOpt+'LastRange').attr("disabled", true);
			       } else if(val === 'betweenValue'){
			    	   if(selected){
			    		   if(selected.from){
			    			   $('#betweenFrom'+radioOpt ).val(selected.from);
			    		   }
			    		   if(selected.to){
			    			   $('#betweenTo'+radioOpt ).val(selected.to);
			    		   }
			    	   }
						$('#'+radioOpt+'NextRange').attr("disabled", true);
				        $('#'+radioOpt+'LastRange').attr("disabled", true);
			        }
		       }
		    }
		  return  dateRangeFun(val,radioOpt,selected);
	};

	$("#dialog input[type='number']").bind('keyup mouseup',function(){
	selectedRangeDate = $(this).val();
	if(selectedRangeDate !=""){
		var dataVal = $(this).data("value");
		var selectTab=$('#dialog').find('.active').find('a').text().toLowerCase();
		dynamicRange(dataVal,selectTab,selectedRangeDate);	
	}
	
	
	});
	
    var dynamicRange = function(dataVal,selectTab,selectedRangeDate){
    	var dataValue = dataVal;
    	var selectedTab = selectTab;
    	 selectedRangeDate = selectedRangeDate;
    	var result;
    	if(dataValue === 'nextRangeValue' && selectedTab === "day" ){
    		result = getManipulatedDate(selectedTab) + ' To ' + getManipulatedDate(selectedTab,selectedRangeDate);
    	}else if(dataValue === 'lastRangeValue' && selectedTab === "day"){
    		selectedRangeDate = '-'+selectedRangeDate;
    		result = getManipulatedDate(selectedTab,selectedRangeDate) + ' To ' + getManipulatedDate(selectedTab);
    	}else if(dataValue === 'nextRangeValue'){
    		result = getTruncateDate(selectedTab,getManipulatedDate(selectedTab) )+ ' To ' + getTruncateDate(selectedTab,getManipulatedDate(selectedTab,selectedRangeDate),true);
    	}else if(dataValue === 'lastRangeValue'){
    		selectedRangeDate = '-'+selectedRangeDate;
    		result = getTruncateDate(selectedTab,getManipulatedDate(selectedTab,selectedRangeDate) )+ ' To ' + getTruncateDate(selectedTab,getManipulatedDate(selectedTab),true);
    	}
    	else if(dataValue === 'betweenValue'){
    		var betWeenFrom = $("#betweenFrom"+selectTab).val();
    		var betWeenTo = $("#betweenTo"+selectTab).val();
    		if(betWeenFrom && betWeenTo){
    			betWeenFrom = betWeenFrom*-1;
    			result = getManipulatedDate(selectTab,betWeenFrom) +' To '+getManipulatedDate(selectTab,betWeenTo)
    		}
    	}
    	
    	$('#savedRange').find('span').html(result);
    	return result;
    };
    
    $('#myTab li a').on('click', function() { 
	var currentSelectedTab = $(this).attr('data-attr');
// $('#dialog').find('.tab-content').find('.tab-pane.fade').removeClass('active');
// $('#dialog').find('.tab-content').find('#'+currentSelectedTab).addClass('active');
	 var fieldSet = currentFieldset.data();
	 if($('#sankeyModalFilterContent').length != 0 && fieldSet){
		 fieldSet = currentFieldset.data('defaultvalue');
	 }
	 if(!jQuery.isEmptyObject(fieldSet) && fieldSet.currentRangeType !=currentSelectedTab){
		// $( ".selectedRange" ).html('');
		 $('#savedRange').find('span').html('');
    	 $('input[type="radio"]').prop('checked', false);
    	 $(".nextRange").attr("disabled", true);
	     $(".lastRange").attr("disabled", true);
	     $("[name='betweenRange']").attr("disabled", true).val("");
	     $(".lastRange").val('');
		 $(".nextRange").val('');
	 }else if(!jQuery.isEmptyObject(fieldSet) && fieldSet.currentRangeType ===currentSelectedTab){
		 dataAnalyser.storedDateRange(fieldSet);
	 }
});
	$('.saveYear').click(function () {
	selectedRange = $('#savedRange').find('span').text();
     var selectedDateRange = selectedRange.split("To");
     var selectedRange = ""
     if(selectedDateRange !="" ){
    	 if(radioValue === "betweenValue"){
    		 selectedRange = {
    			from:$("#betweenFrom"+currentRangeType).val(),
    			to:$("#betweenTo"+currentRangeType).val()
    		 };
    	 }else{
    		 selectedRange = selectedRangeDate;
    	 }
    	 if($('#sankeyModalFilterContent').length != 0){
    		currentFieldset.data("defaultvalue",{
			      "currentRangeType" : currentRangeType,
			      "selectedRangeDate" : selectedRange,
			      "radioValue"       : radioValue,
			      
			});
    		 $( "#dialog" ).data().find($("input[name='from-defaultvalue']")).val(selectedDateRange[0].trim());
    	     $( "#dialog" ).data().find($("input[name='to-defaultvalue']")).val(selectedDateRange[1].trim());
    	 }else{
    		 currentFieldset.data({
			      "currentRangeType" : currentRangeType,
			      "selectedRangeDate" : selectedRange,
			      "radioValue"       : radioValue,
			      
			});
    		 $( "#dialog" ).data().find($("input[name='from-defaultRangevalue']")).val(selectedDateRange[0].trim());
    	     $( "#dialog" ).data().find($("input[name='to-defaultRangevalue']") ).val(selectedDateRange[1].trim()); 
    	 }
    	 
     }
	$('#dialog').dialog('close');
	 return false;
   });
	
	$("#dialog input[type='radio']").click(function(){
		var currentlySelectedRangeType = this.name;
		var currentselection = $("input[name='"+this.name+"']:checked").val();
		dateRangeFun(currentselection,currentlySelectedRangeType);
   });
 
	var dateRangeFun = function(currentselection,currentlySelectedRangeType,Range){
	var result,calculatedRange,calculatedDateRange;
	     currentRangeType = currentlySelectedRangeType;
	    radioValue = currentselection;
	    if(radioValue === 'previous' || radioValue === 'next' ){
        	selectedRangeDate = -1;
        	if(radioValue === 'next'){
                selectedRangeDate = 1;
        	}
        	calculatedRange = getTruncateDate(currentRangeType,getManipulatedDate(currentRangeType,selectedRangeDate) )+ ' To ' + getTruncateDate(currentRangeType,getManipulatedDate(currentRangeType,selectedRangeDate),true);
        }else if(radioValue === 'current'){
        	selectedRangeDate = 0;
        	calculatedRange = getTruncateDate(currentRangeType) + ' To ' + getTruncateDate(currentRangeType,'',true);
        }else if(radioValue === 'currentDay' || radioValue === 'previousDay'){
        	selectedRangeDate = 0;
        	if(radioValue === 'previousDay'){
                selectedRangeDate = -1;
        	}
        	calculatedRange = getManipulatedDate(currentRangeType,selectedRangeDate) + ' To ' + getManipulatedDate(currentRangeType);
        }else if(radioValue === 'nextDay'){
        	 selectedRangeDate = 1;
        	 calculatedRange = getManipulatedDate(currentRangeType) + ' To ' + getManipulatedDate(currentRangeType,selectedRangeDate);
        }
		$("#"+currentRangeType+"LastRange").val('');
		$("#"+currentRangeType+"NextRange").val('');
		$("#"+currentRangeType+"LastRange").attr("disabled", true);
	    $("#"+currentRangeType+"NextRange").attr("disabled", true);
	    $("#betweenFrom"+currentRangeType+",#betweenTo"+currentRangeType).attr("disabled", true);
    	if(radioValue === 'nextRangeValue' && $("input[name='"+currentRangeType+"']").is(':checked')){
    		selectedRangeDate = Range;
    		$("#"+currentRangeType+"NextRange").val(selectedRangeDate);
    		$("#betweenFrom"+currentRangeType+",#betweenTo"+currentRangeType).val("");
    		$("#"+currentRangeType+"LastRange").val('');
             $("#"+currentRangeType+"NextRange").attr("disabled", false);
     	     $("#"+currentRangeType+"LastRange").attr("disabled", true);
     	     if(selectedRangeDate !=undefined){
     	    	result = dynamicRange(radioValue,currentRangeType,selectedRangeDate);	 
     	     }
     	     
        }else if(radioValue === 'lastRangeValue' && $("input[name='"+currentRangeType+"']").is(':checked')){
        	 selectedRangeDate = Range;
        	 $("#"+currentRangeType+"NextRange").val('');
        	 $("#betweenFrom"+currentRangeType+",#betweenTo"+currentRangeType).val("");
     		$("#"+currentRangeType+"LastRange").val(selectedRangeDate);
              $("#"+currentRangeType+"NextRange").attr("disabled", true);
      	     $("#"+currentRangeType+"LastRange").attr("disabled", false);
         	if(selectedRangeDate !=undefined){
     	    	result = dynamicRange(radioValue,currentRangeType,selectedRangeDate);	 
     	     }
         }else if(radioValue === 'betweenValue' && $("input[name='"+currentRangeType+"']").is(':checked')){
        	 $("#betweenFrom"+currentRangeType+",#betweenTo"+currentRangeType).attr("disabled", false);
        	 result = dynamicRange(radioValue,currentRangeType);
         }
    		$('#savedRange').find('span').html(calculatedRange);
    		if(result !=undefined){
    			calculatedRange = result;
    		}
    		if(calculatedRange !=undefined){
    			calculatedDateRange = calculatedRange.split("To");
    		}
    		
            return calculatedDateRange;
	};
	this.constructFilters = function(config,filterContentId,source,database){
		constructFilters(config,filterContentId,source,database);
	};
	
	var constructFilters = function(config,filterContentId,source,database,alertNotification) { 
		var profileSource = source ? source : THIS.sourceType;
		var profileDB =	database ? database :THIS.database;
		filterContentId = filterContentId || "divSankeyFilter";	
		var rowDiv, mainSection, rowIndex = 0,sectionClassName = "col col-4 col-sm-4",isInline;
		var onSelectFun;
		$("[name='clearFilter']").unbind("click").bind("click",function() {
			var filters = [];
			dataAnalyser.filters.forEach(function(filter) {
				if((filter.fieldType === "lookupdropdown" || filter.fieldType === "lookup" ) && !filter.invisible || filter.fieldType === "dynamicDate"){
					filter.defaultvalue = "";
				}
				filters.push(filter);

			});
			dataAnalyser.constructFilters(filters,"divSankeyFilter",dataAnalyser.sourceType, dataAnalyser.database);
			$("#applyFilter").trigger('click');
			$("#filterModal").modal('hide');
		});
		try{
			if(profileAnalyser && profileAnalyser !== null){
				var addConfigs = Jsonparse(profileAnalyser.data.additional_config) ? Jsonparse(profileAnalyser.data.additional_config) :'';
				THIS.additionalconfig = addConfigs;
			}
				if ((THIS.additionalconfig && THIS.additionalconfig.isFilterInline)) { 
					sectionClassName = "col col-3 col-sm-3";
					isInline = true;
					$("#divSankeyFilter").removeClass("modal-body");
					$("#divSankeyFilter").parent().removeClass("widget-body");
				}
				if(THIS.additionalconfig && THIS.additionalconfig.filterInterdependent){
					localStorage[filterContentId]=null;
					var filObj={};
					onSelectFun=function(filObj) {
						getInterDependentFil(filObj,filterContentId !==undefined && filterContentId !=="" && filterContentId !==null ? '#'+filterContentId:'#divSankeyFilter')
					}
				}
			
		}catch(e){
			if ((THIS.additionalconfig && THIS.additionalconfig.isFilterInline)) {
				sectionClassName = "col col-3 col-sm-3";
				isInline = true;
				$("#divSankeyFilter").removeClass("modal-body");
				$("#divSankeyFilter").parent().removeClass("widget-body");
			}	
			if(THIS.additionalconfig && THIS.additionalconfig.filterInterdependent){
				localStorage[filterContentId]=null;
				var filObj={};
				onSelectFun=function(filObj) {
					getInterDependentFil(filObj,filterContentId !==undefined && filterContentId !=="" && filterContentId !==null ? '#'+filterContentId:'#divSankeyFilter')
				}
			}
		}
		
		if (config && config.length > 0) {
			var previousFilters = getFilterQueryFromFilterConfig(THIS.filters, THIS.sourceType);
			var mandatoryPass = true;
			$('#'+filterContentId).html('');
			rowDiv = $('<div/>').addClass('row smart-form'+(isInline?' col-sm-10':''));
			$('#'+filterContentId).append(rowDiv);
			var counter=0;
			
			$.each(config,function(index, obj) {
				
		// if (rowIndex % 2 === 0) {
		// rowDiv = $('<div/>').addClass('row');
		// $('#'+filterContentId).append(rowDiv);
		// }
			if(config[index].source){
				profileSource = config[index].source
			}
			if(config[index].database){
				profileDB = config[index].database
			}
				var filterIndex = index;
				if(alertNotification === 'alertNotification'){
							filterIndex = filterIndex+$("#divSankeyFilter").find('section').length;
						}
								mainSection = $('<section/>').addClass(obj.fieldType === 'raw'?"col col-12 col-sm-12":sectionClassName).attr({
									'key' : filterIndex,
									'id':'f'+(filterIndex+1)
								}).data({
									"lookup-query" : obj.lookupquery,
									"field-name" : obj.fieldName,
									"display-name" : obj.displayName,
									"field-type" : obj.fieldType,
									"isSingle":obj.isSingle,
									"isMandatory":obj.isMandatory,
									"invisible":obj.invisible,
									"includenull":obj.includenull,
									"searchtype":obj.searchtype,
									"innotin":obj.innotin,
									"defaultvalue" : obj.defaultvalue,
									"dateformat": obj.dateFormat,
									"chat":obj.chat,
									"interdependentcolumns":obj.interdependentcolumns,
									"source":obj.source,
									"database":obj.database,
									"fromdefaultvalue":obj["from-defaultvalue"]?obj["from-defaultvalue"]:'',
									"todefaultvalue":obj["to-defaultvalue"]?obj["to-defaultvalue"]:'',
									"alertFilters":obj.alertFilters
								});
								if(obj.profileid)
									mainSection.data("profileid",obj.profileid);
								if (obj.isMandatory && (!obj.defaultvalue || obj.defaultvalue.length === 0))
									mandatoryPass = false;
								if(obj.invisible && alertNotification !== 'alertNotification'){
									counter=1;
									mainSection.hide();
								}
								rowDiv.append(mainSection.append($('<div/>').addClass('note').append($('<strong/>').html(obj.displayName || obj.fieldName+ ' :')))
												.append($('<label/>').addClass(obj.fieldType === 'raw'?'textarea textarea-resizable':'input width-100per')
													.append(obj.innotin?$("<select/>").attr({"name":"searchtype"}).css({"padding-left":"0px"}).addClass("form-control col col-3 ")
																		.append($("<option/>").html("In").attr({"value":"in"}))
																		.append($("<option/>").attr({"value":"not in"}).html("Not In"))
																		.append($("<option/>").html("Like").attr("value","like"))
																		.append($("<option/>").html("Not Like").attr("value","not like")):'')
																.append(obj.fieldType === 'raw'? $('<textarea/>').addClass("custom-scroll").attr({"name":"Rawfilter"}).val(obj.defaultvalue)
																		.attr({'placeholder' : 'Enter '}).addClass('form-control'+(obj.innotin?' width-75per':''))
																:(obj.fieldType === 'TEXT' ? $('<input/>').val(obj.defaultvalue.replace(/'/g,""))
																		.attr({'placeholder' : 'Enter '+ obj.displayName}).addClass('form-control'+(obj.innotin?' width-75per':''))
																: (obj.fieldType === 'lookup' ||obj.fieldType === 'select' ||obj.fieldType === 'lookupdropdown')? $('<div/>').attr({"name":"dropdowndiv"}).addClass((obj.innotin?'col-md-9':'col-md-12')).append($('<input/>')
																		.addClass('input'+isInline?' chart-bar ':' width-100per')): $('<div/>')
																				.addClass(obj.innotin?'row':'').append($('<div/>').addClass(obj.innotin?'col-sm-4':'col-sm-6')
// .append($('<div/>').addClass('note
// col-sm-3').append($('<strong/>').html('From')))
																				.append($('<label/>').addClass('input').append($('<input/>').attr({"name" : "from-defaultvalue","placeholder":"From"}).css({"width":"95%"})
																					.addClass('form-control').val(obj["from-defaultvalue"])))).append($('<div/>').addClass(obj.innotin?'col-sm-4':'col-sm-6')
																					  .append($('<label/>').addClass('input').append($('<input/>').attr({"name" : "to-defaultvalue","placeholder":"To"}).css({"width":"95%"})
																						.addClass('form-control').val(obj["to-defaultvalue"]))))))));
								if(obj.innotin){
									mainSection.find("[name='innotin']").val("notin");									
								}
								if(obj.searchtype){
									mainSection.find("[name='searchtype']").val(obj.searchtype);									
								}
								if (obj.fieldType === 'DATE'  || obj.fieldType === 'dynamicDate' ) {
									if(obj.fieldType === 'dynamicDate'){
										if(isNaN(Date.parse(obj["from-defaultvalue"]))){
											mainSection.find('input:eq(0)').val(eval(obj["from-defaultvalue"]));
											mainSection.find('input:eq(1)').val(eval(obj["to-defaultvalue"]));
										}else{
											mainSection.find('input:eq(0)').val(obj["from-defaultvalue"]);
											mainSection.find('input:eq(1)').val(obj["to-defaultvalue"]);
										}
									}
									mainSection.find('input:eq(0)')
											.datepicker({
														defaultDate : "+1w",
														changeMonth : true,
														onClose : function(selectedDate) 
															{
																$('div[key="'+ filterIndex+ '"]').find('input:eq(1)').datepicker("option","minDate",selectedDate);
															}
													});
									mainSection.find('input:eq(1)')
											.datepicker({
														defaultDate : "+1w",
														changeMonth : true,
														onClose : function(selectedDate) 
															{
															$('div[key="'+ filterIndex+ '"]').find('input:eq(0)')
																	.datepicker("option","maxDate",selectedDate);
															}
													});
								}else if (obj.fieldType === 'raw' ) {
// obj.fieldName.replaceAll(mainSection.find("[name='Rawfilter']").val());
								} 
									else if(obj.fieldType === 'dateRange') {
									mainSection.find('input:eq(0)').addClass('cursor-pointer').attr({'readonly':true});
									mainSection.find('input:eq(1)').addClass('cursor-pointer').attr({'readonly':true});
								   var defaultValue = obj.defaultvalue;
								   if(Object.keys(defaultValue).length>0){
									   var previousDateRange =dataAnalyser.storedDateRange(defaultValue);
										  if(previousDateRange && previousDateRange !=""){
											  mainSection.find('input:eq(0)').val(previousDateRange[0].trim());
												mainSection.find('input:eq(1)').val(previousDateRange[1].trim());  
										  }
										     
								   }
								  
									mainSection.find('input').click(function(){
								    currentFieldset = $(this).parents("section");// find("section#"+currentField.attr('id'));
								    var Fieldset = currentFieldset.data('defaultvalue');
								    $('#dialog').find('.tab-content').find('.tab-pane.fade').removeClass('active');
						            $( "#dialog" ).dialog({
						    	        width: 500,
						    	        resizable: false,
						    	        height: 250 ,
						    	        open: function(event){
						    	        	 $( ".selectedRange" ).html('');
						    	        	 $('input[type="radio"]').prop('checked', false);
						    	        	 $(".nextRange").attr("disabled", true);
										     $(".lastRange").attr("disabled", true);
										}
						    	    });
						            
						            $( "#dialog" ).dialog("open");
						            $( "#dialog" ).data(currentFieldset);
				                 
				                 if(config && jQuery.isEmptyObject(Fieldset)){
				                	 for(var i=0; i<config.length;i++){
					                	 if(currentFieldset.attr('id') === config[i].id ){
					                		dataAnalyser.storedDateRange(config[i].defaultvalue);
					                	 } 
					                 } 
				                 }
				                 else if(!jQuery.isEmptyObject(Fieldset)) {
						        		 dataAnalyser.storedDateRange(Fieldset);
						        	} 
				            });
								}else if (obj.fieldType === 'lookup' || obj.fieldType === 'select' || obj.fieldType === 'lookupdropdown') {
									var object={};
								     if($('#'+filterContentId).length>0 && THIS.additionalconfig && THIS.additionalconfig.filterInterdependent){
								    	 var configFil=[];
								    	 configFil = jQuery.extend(true, [], config);
										 configFil.splice(index,1);
										 if(obj.interdependentcolumns && obj.interdependentcolumns.length > 0){
											 var configFils = [];
											 for(var z = 0; z < obj.interdependentcolumns.length; z++){
												 for(var x = 0; x < configFil.length; x++){
													 if(configFil[x].fieldName == obj.interdependentcolumns[z]){
														 configFils.push(configFil[x])
													 }
												 }
											 }
											 configFils = jQuery.extend(true, [], configFils);
											 var defaultFilters = getFilterQueryFromFilterConfig(configFils, profileSource);
											 object.defaultFilters = defaultFilters ;
											 object.interdependentFil = true;
											 object.id= filterIndex;
											 object.source=profileSource;
											 object.database=profileDB;
											 object.divId = filterContentId;
											 object.alertNotification = alertNotification;
											 object.previousFilters=defaultFilters;
											 if((filterIndex+1-counter) % 4 ===0)
												 object.popright=true;
											 if(alertNotification === 'alertNotification')
												 object.buttonWidth = '200px';
											 object.allFil =  jQuery.extend(true, [], config);
									 	}
								     }
									if(obj.fieldType === 'select'){
										var selectdata =  obj.lookupquery,selectedValue = "";
										if(selectdata && typeof(obj.lookupquery) !=="object"){
											selectdata = Jsonparse(selectdata);
											if(!selectdata || Object.keys(selectdata).length === 0){
												selectdata = [];
												var values =  obj.lookupquery.split(',');
												if(values && values.length>0){
													for(var i=0;i<values.length;i++){
														selectdata.push({value:values[i],label:values[i]});	
													}								
												}
											}else if(selectdata.filter(function(o){return o.id}).length>0){
												var tempData = [];
												for(var i=0;i<selectdata.length;i++){
													tempData.push({value:selectdata[i].id,label:selectdata[i].text});	
												}
												selectdata = tempData;
											}
										}
										if (obj.defaultvalue && ((typeof o === 'object' && Object.keys(obj.defaultvalue).length>0) || obj.defaultvalue.length > 0)) {
											if(obj.isSingle){
												if(typeof(obj.defaultvalue) === "object" && obj.defaultvalue.id){
													selectedValue = obj.defaultvalue.id;
												}else{
													selectedValue = obj.defaultvalue;
												}
											}else if(obj.defaultvalue.length>0){
												selectedValue = obj.defaultvalue.map(function(o){return (typeof o === 'object' && o.id)?o.id:o});
											}
										}
										constructDropDown(mainSection.find("[name='dropdowndiv']"),selectdata,!obj.isSingle,selectedValue,onSelectFun,"Select "+ obj.displayName,profileSource,profileDB,object);
									}else if(obj.fieldType === 'lookupdropdown'){
										constructDropDown(mainSection.find("[name='dropdowndiv']"),obj.lookupquery,!obj.isSingle,obj.defaultvalue,onSelectFun,"Select "+ obj.displayName,profileSource,profileDB,object);
									}else{
										constructSelect2(mainSection.find('input'),obj.lookupquery,!obj.isSingle,obj.defaultvalue,"",onSelectFun,"Enter "+ obj.displayName,profileSource,profileDB);
									}
								}
								rowIndex++;
							});
		}
		if(!localStorage["isMultipleProfile"]){
			$('[name="applyFilter"]').unbind('click').bind('click', function(e) {
				var filterConfig = getFiltersFromProfile(filterContentId);
				console.log("ga testing for cleck event");
				ga('send', 'event', 'button', 'click', 'Filter', 'Applied Filters to Screen');
				if (!filterConfig){
					e.preventDefault();
					return;
				}	
				enableLoading();
				callService(THIS.config, filterConfig, THIS.Summary);
				
				$("#filterModal").modal('hide');
			});
			if (!mandatoryPass && localStorage["from"] === ""){
				disableLoading();
				$("#widget-grid > div >div:not(.search-div)").hide();
				if (THIS.additionalconfig && THIS.additionalconfig.isFilterInline) {
					$('#widget-inline-filter [name="applyFilter"]').trigger("click");
				}else{
					$("#filterModal").modal("show");
					$('#filterModal [name="applyFilter"]').trigger("click");
				}
				return false;
			}else{
				return true;
			}
		}
		
	};
		
	this.getFiltersValue =function(filterContentId){
		return getFiltersFromProfile(filterContentId);
	};
	var getFiltersFromProfile = function(filterContentId) {
		filterContentId = filterContentId || "";
		if (filterContentId === "")
			return;
		var filterConfig = [];
		$('#'+filterContentId +' section').each(
				function() {
					var currentObj = $(this),isDefaultValue;
					var tempConfig = {
						"fieldName" : currentObj.data("field-name"),
						"displayName" : currentObj.data("display-name"),
						"fieldType" : currentObj.data("field-type"),
						"searchtype":currentObj.data("searchtype"),
						"innotin":currentObj.data("innotin"),
						"id":$(this).attr("id"),
						"invisible":currentObj.data("invisible"),
						"includenull":currentObj.data("includenull"),
						"source":currentObj.data("source"),
						"database":currentObj.data("database"),
						"interdependentcolumns":currentObj.data("interdependentcolumns"),
						"chat":currentObj.data("chat")
					};
					if(currentObj.data("profileid"))
						tempConfig.profileid = currentObj.data("profileid");
					if(currentObj.find("[name='searchtype']").length > 0){
						tempConfig.searchtype = currentObj.find("[name='searchtype']").val();
					}
					if (tempConfig["fieldType"] === 'lookup' || tempConfig["fieldType"] === 'select' || tempConfig["fieldType"] === 'lookupdropdown') {
						tempConfig["lookupquery"] = currentObj.data("lookup-query");
						if(tempConfig["fieldType"] === 'lookupdropdown' || tempConfig["fieldType"] === 'select'){
							tempConfig["defaultvalue"] = currentObj.find("[name='dropdowndiv']").find("select").val();
						}else{
							tempConfig["defaultvalue"] = currentObj.find("input").select2('data');
						}
						tempConfig.isSingle = currentObj.data("isSingle");
						if(tempConfig.defaultvalue && ((typeof o === 'object' && Object.keys(tempConfig.defaultvalue).length>0) || tempConfig.defaultvalue.length > 0)){
							isDefaultValue = true;
						}
					}else if(tempConfig["fieldType"] === 'dateRange'){
						tempConfig["defaultvalue"] = currentObj.data('defaultvalue');
						tempConfig["dateFormat"] = currentObj.data('dateformat');;
					} else if (tempConfig["fieldType"] === 'DATE' || tempConfig["fieldType"] === 'dynamicDate' || tempConfig["fieldType"] === 'NUM') {
						tempConfig["from-defaultvalue"] = currentObj.find("[name^=from-defaultvalue]").val();
						tempConfig["to-defaultvalue"] = currentObj.find("[name^=to-defaultvalue]").val();
						tempConfig["dateFormat"] = currentObj.data('dateformat');;
						if(tempConfig["from-defaultvalue"] && tempConfig["to-defaultvalue"]){
							isDefaultValue = true;
						}
					} else if (tempConfig["fieldType"] === 'raw' ) {
						tempConfig["defaultvalue"] = currentObj.find("textarea").val();
						if(tempConfig["from-ddefaultvalue"] && tempConfig["to-defaultvalue"]){
							isDefaultValue = true;
						}
					} else {
						tempConfig["defaultvalue"] = currentObj.find("input").val();
						if(tempConfig.defaultvalue){
							isDefaultValue = true;
						}
					}
					$(currentObj).find("em").remove();
					if(currentObj.data("isMandatory") && !isDefaultValue){
						$(currentObj).find("label:eq(0)").append($("<em/>").addClass("invalid").html(currentObj.data("display-name")+" can not be empty."));						
						filterConfig = false;
						return false;
					}
					filterConfig.push(tempConfig);
				});
		// setTimeout(function () {$("#"+filterContentId).find("em").remove()},
		// 2000);
		return filterConfig;
	};
	/*
	 * var addSummary = function(obj) { var template; var addSummaryConfig =
	 * function(){ template = templates.getSummaryConfig.template(null); var
	 * options = { "popup-content" : template, "placement" : "left" };
	 * $(".profile-summary-div").append( $("<div/>").addClass("col-md-12
	 * col-xs-12 col-sm-12 col-lg-12 text-center").append(
	 * templates.getSummary.template(options)));
	 * loadScript("js/plugin/liquidFillGauge/liquidFillGauge.js",function(){reDraw($(obj).parents(".profile-summary-div:first"));},"droppable"); }
	 * if(!templates.getSummaryConfig.template){ template =
	 * templates.getSummaryConfig(null,function(){ addSummaryConfig(); }); }
	 * else{ addSummaryConfig(); } }; var removeSummary = function(obj) { var
	 * summaryParent = $(obj).parents(".profile-summary-div:first");
	 * $(obj).parents(".chart-summary:first").parent().remove();
	 * reDraw(summaryParent); }; var reDraw = function(obj) { var summaryCount =
	 * $(".chart-summary", $(obj)).length; var summaryClass = ""; if
	 * (summaryCount === 5) summaryClass = "col-md-15 col-md-3 col-sm-3"; else
	 * summaryClass = "col-md-" + (12 / summaryCount) + " col-sm-" + (12 /
	 * summaryCount) + " col-xs" + (12 / summaryCount); $(".chart-summary",
	 * $(obj)).each(function() { $(this).parent().removeClass();
	 * $(this).parent().addClass(summaryClass + " text-center"); });
	 * $("[name=addSummary]").unbind("click").bind("click", function() {
	 * addSummary(this); });
	 * $("[name=removeSummary]").unbind("click").bind("click", function() {
	 * removeSummary(this); }); if (!$("#droppable > article").is(':empty')) {
	 * $('[id^=widget-grid]').jarvisWidgets('destroy'); }
	 * $("[name=settingSummary]").unbind("click"); $("[rel=popover]",
	 * $(obj)).popover('destroy');
	 * $("[name=settingSummary]").removeData("bs.popover"); pageSetUp();
	 * $("[name=settingSummary]").bind("click", function() {
	 * onClickSettingSummary(this); }); };
	 */
	/*
	 * var onClickSettingSummary = function(obj) { var popover =
	 * $(obj).siblings('div.popover'); $(popover).find("h3").append($("<div/>").addClass("smart-form
	 * pull-right") .append($("<label/>").addClass("toggle").append($("<input/>").attr({"type":"checkbox","name":"isfilter"}))
	 * .append($("<i/>").attr({"data-swchon-text":"On","data-swchoff-text":"Off"}))
	 * .append($("<strong/>").html("Filter").addClass("note")))); var result =
	 * $(obj).data();
	 * $(popover).find("[name='isfilter']").attr("checked",result.isfilter); for (
	 * var k in result) { var exceptData = [ "content", "originalTitle",
	 * "placement", "html", "bs.popover", "width" ]; if (exceptData.indexOf(k)
	 * === -1 && $("[name=" + k + "]", popover).length > 0) $("[name=" + k +
	 * "]", popover).val(result[k]); }
	 * 
	 * loadScript("js/plugin/liquidFillGauge/liquidFillGauge.js",function(){loadLiquidFillGauge("fillgauge",
	 * 55,null,100);},"droppable");
	 * $("[name=ibox],[name=widget],[name=circle-tile],[name=fillgauge]").hide();
	 * $(".widget-type").click(function(){
	 * $("[name=ibox],[name=widget],[name=circle-tile],[name=fillgauge]").hide();
	 * $(".widget-type").removeClass("active"); $(this).addClass("active"); if
	 * ($(this).hasClass("ibox")){ $("[name=ibox]").show();
	 * $(obj).data("widget-type","ibox"); }else if ($(this).hasClass("widget")){
	 * $("[name=widget]").show(); $(obj).data("widget-type","widget"); }else if
	 * ($(this).hasClass("circle-tile")){ $("[name=circle-tile]").show();
	 * $(obj).data("widget-type","circle-tile"); }else if
	 * ($(this).hasClass("fillgauge")){ $("[name=fillgauge]").show();
	 * $(obj).data("widget-type","fillgauge"); } }); var widgetType =
	 * $(obj).data("widget-type");
	 * popover.find("."+widgetType).addClass("active");
	 * $("[name="+widgetType+"]").show(); $("input.input-xs", popover).on('keyup
	 * change click', function() { $(obj).data($(this).attr("name"),
	 * $(this).val()); }); $(".toggle", popover).on('click', function() {
	 * $(obj).data($(this).find("input").attr("name"),
	 * $(this).find("input").is(":checked")); }); $("select",
	 * popover).change(function() { $(obj).data($(this).attr("name"),
	 * $(this).val()); }); };
	 */
	
	/**
	 * Constructs the Summary Config and returns value
	 * 
	 */
	var getSummary = function(){
		var summaryCount = 0;
		var summaryConfig = [];
		$("#configSummaryDiv").find(".profile-summary-div").each(
				function() {
					var summaryArray = [];
					var summaryCharts = $(this).children();
					summaryCharts
							.each(function() {
								summaryCount++;
								var data = $(this).find("[name='configSummary']").data();
								var tempConfig = {
									"width" : data.width,
									"id" : "summary" + summaryCount,
									"widget-type": data.widgetType|| data.widget-type || "fillgauge"
								};
								for(var k in data){
									tempConfig[k] = data[k];
								}
								/*
								 * var result = $(this).find(
								 * "[name=settingSummary]").data(); for ( var k
								 * in result) { var exceptData = [ "content",
								 * "originalTitle", "placement", "html",
								 * "bs.popover", "width" ]; if
								 * (exceptData.indexOf(k) === -1) tempConfig[k] =
								 * result[k]; }
								 */

								/*
								 * $(this).find("input").each( function() {
								 * tempConfig[$(this).attr("name")] =
								 * $(this).val(); });
								 * $(this).find("select").each( function()
								 * {tempConfig[$(this).attr("name")] =
								 * $(this).val(); });
								 */
								if ($.trim(tempConfig["group"])!== ""){
									tempConfig.isCustomGroup =  true;
								  } else{
									  tempConfig.group = THIS.facts[0];
									  tempConfig.isCustomGroup =  false;
								  }
								summaryArray.push(tempConfig);
							});
					summaryConfig.push(summaryArray);
				});
		return summaryConfig;
	};
	var constructSummary = function(summary) {
		$(".profile-summary-div").remove();
		if($("#droppable > article").find("#configSummaryDiv").length === 0){
			$("#droppable > article").prepend($("<div/>").attr("id","configSummaryDiv"));
		}
		if (summary && summary.length > 0) {
			for ( var i = 0; i < summary.length; i++) {
				var summaryVar = $("<div/>").addClass("chart-parent-div profile-summary-div text-center").appendTo("#configSummaryDiv");
				// $("#droppable > article").prepend(summaryVar);
				for ( var j = 0; j < summary[i].length; j++) {
					/*
					 * var options = { "popup-content" :
					 * templates.getSummaryConfig.template(null) }; if (j === 0) {
					 * options.placement = "right"; } else { options.placement =
					 * "left"; } var tempDiv = $("<div/>").addClass(summary[i][j].width)
					 * .append(templates.getSummary.template(options));
					 */
					var tempDiv = $(templates.getSummary.template());
					$("#configSummaryDiv").find(".profile-summary-div").last().append(tempDiv);
					// var coloum_config = Object.keys(summary[i][j]);
					for(key in summary[i][j]){
						if(key === "liquidFillGaugeSettings"){
							for(k in summary[i][j][key]){
								tempDiv.find("a[name='configSummary']").data(k,summary[i][j][key][k]);	
							}							
						}else{
							tempDiv.find("a[name='configSummary']").data(key,summary[i][j][key]);
						}						
					}
					tempDiv.find("a[name='configSummary']").data("charttype","summary-"+ summary[i][j].widgetType);
					tempDiv.find("a[name='configSummary']").data("widgetType",summary[i][j].widgetType);
					/*
					 * for ( var idx = 0; idx < coloum_config.length; idx++) {
					 * tempDiv.find("[name=" + coloum_config[idx] + "]").val(
					 * summary[i][j][coloum_config[idx]]);
					 * tempDiv.find("[name=settingSummary]").data(
					 * coloum_config[idx], summary[i][j][coloum_config[idx]]); }
					 */
					
					if (summary[i][j].isCustomGroup !==  true){
						tempDiv.find("[name=group]").val("").data("group","");
					}
				}
				;
				// reDraw(summaryVar);
			}
			redrawSummary();
		}
	};
	var drawSummary = function(summary, ele,profileId) {
		var summaryCount = 0;
		var diameter = 200;
		var summaryHeight ="auto";
		if (summary && summary.length > 0) {
			loadScript("js/plugin/liquidFillGauge/liquidFillGauge.js",function(){},"droppable");
			var summaryWidget = null;
			if($('[name="summaryWidget"]').length>0)
				summaryWidget = $('[name="summaryWidget"]');
			else
				summaryWidget = $("<div/>").attr("name","summaryWidget");

			$(ele + " .widget-body:first").find('.search-div ').length !=0 ? $(ele + " .widget-body").find('.search-div ').after(summaryWidget):$(ele + " .widget-body:first").prepend(summaryWidget);
			for ( var i = 0; i < summary.length; i++) {
				var summaryVar = null;
				if($('[name="summaryWidget"]').find('.profile-summary-div').length>0)
					summaryVar = $($('[name="summaryWidget"]').find('.profile-summary-div'));
				else
					summaryVar = $("<div/>").addClass("chart-parent-div profile-summary-div row  text-center");
				
				summaryWidget.append(summaryVar);
				for ( var j = 0; j < summary[i].length; j++) {
					if(profileId)
						summary[i][j].id = summary[i][j].id+profileId;
					summaryCount++;
					var summaryClass = "";
					if (summary[i].length === 5)
						summaryClass = "col-md-15 col-md-3 col-sm-3";
					else
						summaryClass = "col-md-" + (12 / summary[i].length) + " col-sm-" + (12 / summary[i].length) + " col-xs"  + (12 / summary[i].length);
					if(summary[i][j].overrideclass){
						if(parseInt(summary[i][j].overrideclass))
							summaryClass = "col-md-" + summary[i][j].overrideclass + " col-sm-" + summary[i][j].overrideclass + " col-xs"  + summary[i][j].overrideclass;
						else
							summaryClass=summary[i][j].overrideclass;
					}
					diameter = summary[i][j]["summaryWidth"]||summary[i][j]["summary-width"]|| diameter;
					summaryHeight  = summary[i][j]["summaryHeight"]||summary[i][j]["summary-height"];
					var tempDiv = $("<div/>").addClass(summaryClass)
							.attr({
								"id" : summary[i][j].id// +summary[i][j].fieldname
							});
					
					summaryVar.append(tempDiv);
					diameter = diameter > $("#" + (summary[i][j].id)).width() ? diameter: $("#summary[i][j].id").width();
					var widgetType =summary[i][j]["widgetType"] || summary[i][j]["widget-type"];
					if (widgetType==="ibox"){
						var processBar = summary[i][j]["process-bar-color"]||summary[i][j].processBarColor;
						tempDiv.append($("<div/>").addClass("ibox")
								.append($("<div/>").addClass("ibox-content").css({"width":summary[i][j]["summary-width"],"height":summaryHeight})
								.append($("<h5/>").html(summary[i][j].detail))
								.append($("<h2/>"))
								.append($("<div/>").addClass("progress progress-mini").append($("<div/>").addClass("progress-bar "+ processBar).css("width","100%")))
								.append($("<div/>").addClass("margin-top-10 font-sm").html(summary[i][j]["description"]))
								));
					} else if (widgetType==="widget"){
						var widgetColor = summary[i][j].widgetColor || summary[i][j]["widget-color"];
						var widgetIcon = summary[i][j].widgetIcon || summary[i][j]["widget-icon"];
						var showTotal = summary[i][j].showtotal;
						var tooltip = summary[i][j].tooltip;
						tempDiv.append($("<div/>").addClass("widget ").css({"background-color":widgetColor,"width":summary[i][j]["summary-width"],"height":summaryHeight})
								.append($("<div/>").addClass("row no-margin")
								.append($("<div/>").addClass("col-xs-3")
										/*
										 * .append( tooltip?$("<i/>").css({"font-size":"20px"}).attr({"data-content":tooltip,"data-toggle":"popover","data-html":true,"rel":"popover"})
										 * .addClass("fa fa-info-circle fa-lg
										 * pull-left iconhover"):"")
										 */
										.append($("<i/>").addClass(widgetIcon+" margin-top-10")))
								.append($("<div/>").addClass(widgetIcon?"col-xs-9":"col-xs-12")
										.append($("<span/>").css({"margin-top":"-5px"}).addClass("font-md").append(summary[i][j].detail+" ")
										)
										.append((showTotal && showTotal === 'no')?'':"<h2/>").addClass('font-bold'))
								)
							.append($("<div/>").addClass("font-sm").html(summary[i][j]["description"]))
							);
					} else if (widgetType==="circle-tile"){
						var widgetColor = summary[i][j].circleTitleColor ||  summary[i][j]["circle-title-color"]||
						summary[i][j].widgetColor || summary[i][j]["widget-color"];
						var widgetIcon = summary[i][j].circleTitleIcon || summary[i][j]["circle-title-icon"]||
							summary[i][j].widgetIcon || summary[i][j]["widget-icon"];
						var tooltip = summary[i][j].tooltip;
						tempDiv.append($("<div/>").addClass("circle-tile")
								.append($("<a/>").append($("<div/>").css({"background-color":widgetColor}).addClass("circle-tile-heading ").append($("<i/>").addClass(widgetIcon))))
								
								.append($("<div/>").addClass("circle-tile-content ").css({"background-color":widgetColor,"width":summary[i][j]["summary-width"],"height":summaryHeight})
										.append($("<div/>").addClass("text-faded").html(summary[i][j].detail))
										.append($("<div/>").addClass("circle-tile-number text-faded").append($("<span/>").addClass(summary[i][j].summaryfontsize +(tooltip?"margin-left-35":"")))
												.append( tooltip?$("<i/>").css({"font-size":"20px","color":"#fff"}).attr({"data-content":tooltip,"data-toggle":"popover","data-html":true,"rel":"popover"})
														.addClass("fa fa-info-circle fa-lg pull-right iconhover margin-10"):""))
										
										.append($("<a/>").addClass("circle-tile-footer").html(summary[i][j]["description"])))
								);
					}else{
						d3.select("#" + (summary[i][j].id)).append("svg").attr(
								"width", diameter).attr("height", diameter).attr(
								"class", "bubble").attr('id','svg-' + summary[i][j].id+summary[i][j].fieldname);
					}
				}
				;
			}
			$("[name='summaryWidget']").find("i[rel='popover']").popover({
		  		  placement: 'auto',
		  		  container: 'body',
		  		  width:'300px',
		  		  delay :  { "show": 100, "hide": 100 },
		  		  trigger : 'hover'
	  	  	});
		}
	};
	var addCalendar = function(){
		THIS.calendarCounter = $("[id^=tableForCalendar]").length + 1;
		var callBackCalendar = function(response) {
			if (!$("#droppable > article").is(':empty')) {
				$('[id^=widget-grid]').jarvisWidgets('destroy');
			}
			$("#droppable > article").append(response);
			pageSetUp();
			$("#tableForCalendar_" + THIS.calendarCounter)
					.select2({
						placeholder : "Choose table",
						data : THIS.tableList || []
					})
					.on(
							"change",
							function(e) {
								THIS.calendarCounter = e.target.id
										.replace(
												"tableForCalendar_",
												"");
								$(e.target)
										.attr("data-tablename",
												e.val);
								// calendarSelect(e.val);
							}).select2("val", THIS.table);
			$("#tableForCalendar_" + THIS.calendarCounter)
					.attr("data-tablename", THIS.table);
			// calendarSelect(THIS.table);
			$(
					"[name=drilldowncallback]",
					$("#wid-id-calendar_"
							+ THIS.calendarCounter)).keyup(
					function() {
						$(this).data($(this).attr("name"),
								$(this).val());
					});
			$(
					"[name=tableorderby]",
					$("#wid-id-calendar_"
							+ THIS.calendarCounter)).bind(
					'keyup mouseup',
					function() {
						$(this).data($(this).attr("name"),
								$(this).val());
					});
			$('#'+"Calendars_"+THIS.calendarCounter).fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,basicWeek,basicDay'
				},
				defaultDate: System.getFormattedDate(new Date()),
				editable: true,
				eventLimit: true, // allow "more" link when too many events
				events: []
			});
/*
 * var tempOption = $("<div/>"); tempOption.append($("<option/>").attr("value",
 * "").html("Choose")); if (THIS.ColumnList && THIS.ColumnList.length > 0) { for (
 * var i = 0; i < THIS.ColumnList.length; i++) { tempOption.append($("<option/>").attr("value",
 * THIS.ColumnList[i].id).html(THIS.ColumnList[i].text)); } }
 * $("#wid-id-calendar_1 .settings select").append(tempOption.html());
 */
			pageSetUp();
			$("#tableForCalendar_" + THIS.calendarCounter)
					.select2({
						placeholder : "Choose table",
						data : THIS.tableList || []
					}).on("change",function(e) {
								THIS.calendarCounter = e.target.id.replace("tableForCalendar_","");
								$(e.target).attr("data-tablename",e.val);
								calendarTableSelect(e.val);
							}).select2("val", THIS.table);
			calendarTableSelect(THIS.table);
		};
		var options = {
				"table-header":"Calendar",
				"idCounter":THIS.calendarCounter,
				"width":"col-md-12 col-xs-12 col-sm-12 col-lg-12"
		};
		templates.getCalendar(options, callBackCalendar,
				THIS.calendarCounter);
	};
	var calendarTableSelect = function(table,config,idCounterCalendar){
		    THIS.datatable = table;
			var request = {
				"source" : THIS.sourceType,
				"database" : THIS.database
			};
			if(THIS.dataQuery){
				request.query = calculateDynamicFilter(THIS.dataQuery,THIS.filters);
			}else{
				request.table = THIS.table;
			}
			enableLoading();
			callAjaxService("getColumnList", function(response){callBackGetCalendarTableSelect(response,config,idCounterCalendar);},
					callBackFailure, request, "POST");
	};
	var callBackGetCalendarTableSelect = function(response,config,idCounterCalendar){
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			disableLoading();
			$("#wid-id-calendar_1 .settings select").empty();
			var tempOption = $("<div/>");
			tempOption.append($("<option/>").attr("value", "").html("Choose"));
			if (response && response.length > 0) {
				for ( var i = 0; i < response.length; i++) {
					tempOption.append($("<option/>").attr("value",
							response[i].columnName).html(response[i].columnName));
				}
			}
			$("#wid-id-calendar_1 .settings select").append(tempOption.html());
			$("[name=eventclickcallback]",$("#wid-id-calendar_"+idCounterCalendar)).val(config.eventclickcallback);
			$("[name=chartwidth]",$("#wid-id-calendar_"+idCounterCalendar)).val(config.chartwidth);
			$("[name=chartheight]",$("#wid-id-calendar_"+idCounterCalendar)).val(config.chartheight);
			$("[name=eventid]",$("#wid-id-calendar_"+idCounterCalendar)).val(config.eventid);
			$("[name=eventname]",$("#wid-id-calendar_"+idCounterCalendar)).val(config.eventname);
			$("[name=eventstart]",$("#wid-id-calendar_"+idCounterCalendar)).val(config.eventstart);
			$("[name=eventend]",$("#wid-id-calendar_"+idCounterCalendar)).val(config.eventend);
			$("[name=eventcolor]",$("#wid-id-calendar_"+idCounterCalendar)).val(config.eventcolor);
			$("[name=defaultdate]",$("#wid-id-calendar_"+idCounterCalendar)).val(config.defaultdate);
		}
	};
	
	/**
	 * Invoked to Configure Column details for a Table
	 */
	var tableFilterConfiguration = function(fields,fieldMapping, pivotSettings, table,workflow, rule){ 
		var profileFilters = getFilters(),chartsMappingBody;
		var chartsConfig = getConfig();
		var summaryConfig = getSummary();
		if (profileFilters && profileFilters.length > 0){
			var filterMappingBody = $("#filtersMapping tbody").empty();
			for (var f=0; f < profileFilters.length; f++){
				filterMappingBody.append($("<tr/>").append($("<td/>").attr("field",profileFilters[f].fieldName).html(profileFilters[f].displayName + " (" + profileFilters[f].fieldName + ")")).append($("<td/>").append($("<input/>").addClass("width-100per"))));
			};
		};
		if (chartsConfig && chartsConfig.length > 0){
			chartsMappingBody =  $("#chartsMapping tbody").empty();
			for (var panel=0; panel < chartsConfig.length; panel++){
				if (!chartsConfig[panel].charts && chartsConfig[panel].charts.length === 0)
					continue;
				for (var chart=0; chart < chartsConfig[panel].charts.length; chart++){
					if (chartsConfig[panel].charts[chart].fieldname && chartsConfig[panel].charts[chart].charttype !== "datatable")
						chartsMappingBody.append($("<tr/>").append($("<td/>").attr("field",chartsConfig[panel].charts[chart].fieldname).html(chartsConfig[panel].charts[chart].displayname + " (" +chartsConfig[panel].charts[chart].fieldname + ")")).append($("<td/>").append($("<input/>").addClass("width-100per"))));
				}
			};
		};
		if (summaryConfig && summaryConfig.length > 0){
			for (var panel=0; panel < summaryConfig.length; panel++){
				if (!summaryConfig[panel] && summaryConfig[panel].length === 0)
					continue;
				for (var chart=0; chart < summaryConfig[panel].length; chart++){
					if (summaryConfig[panel][chart].fieldname)
						chartsMappingBody.append($("<tr/>").append($("<td/>").attr("field",summaryConfig[panel][chart].fieldname)
								.html(summaryConfig[panel][chart].fieldname)).append($("<td/>").append($("<input/>").addClass("width-100per"))));
				}
			};
		};
		$("#filtersMapping input,#chartsMapping input, #pivotTableColumn,#pivotHeaderCols,#pivotTableRow,#workflowColumn,#ruleColumn").select2({
			allowClear: true,
			placeholder : "Choose mapping",
			data : fields
		});
		$('#pivotTables').empty();
		if(workflow){
			$('#workflowColumn').select2('val', workflow.column);
			if(workflow.isWorkflow)
				$("#tableWorlflowSettings").find("[name='isWorkflow']").attr("checked",true);
			if(workflow.disableOnComplete) {
				$("#tableWorlflowSettings").find("[name='wf-disableOnComplete']").attr("checked",true);
			}
		}
		$('#isRule').prop('checked', false);
		$('#ruleColumn').val('');
		$('#ruleLink').val('');
		$('#ruleProject').val('');
		if(rule) {
			$('#ruleColumn').select2('val', rule.column);
			if(rule.isRule) {
				$('#isRule').prop('checked', true);
			}
			if(rule.ruleLink) {
				$('#ruleLink').val(rule.ruleLink);
			}
			if(rule.ruleProject) {
				$('#ruleProject').val(rule.ruleProject);
			}
		}
		
		$('#pivotHeaderCols').val('');
		$('#pivotTableAs').val('');
		$('#pivotEditCols').val('');
		$('#pivotTableOrder').val('');
		$('#pivotTables').empty();
		$('#pivotTableElements').empty();
		$('.pivotTableOptions').hide();
		if(pivotSettings && pivotSettings.enabled) {
			if(pivotSettings.column) {
				$('#pivotTableColumn').select2('val', pivotSettings.column);
			}
			if(pivotSettings.tableAs) {
				$('#pivotTableAs').val(pivotSettings.tableAs);
			}
			if(pivotSettings.tableAs) {
				$('#pivotHeaderCols').select2('val', pivotSettings.headerCols);
			}
			if(pivotSettings.editCols && pivotSettings.editCols.length > 0) {
				$('#pivotEditCols').val(pivotSettings.editCols.join(','))
			}
			if(pivotSettings.colOrder && pivotSettings.colOrder.length > 0) {
				$('#pivotTableOrder').val(pivotSettings.colOrder.join(','))
			}
			if(pivotSettings.row) {
				$('#pivotTableRow').select2('val', pivotSettings.row);
			}
			
			if(pivotSettings.tables && pivotSettings.multiTable) {
				$('.pivot-settings-multi-table').find(':checkbox').prop('checked', true);
				var tables = pivotSettings.tables;				
				$('.pivot-settings-multi-table').find(':checkbox').prop('checked', true);
				addPivotTable(THIS.tableList, THIS.sourceType, THIS.database, THIS.ColumnList, System.CloneObject(tables));
			} else {
				$('.pivot-settings-multi-table').find(':checkbox').prop('checked', false);
			}
		} else {
			$('#pivotTableColumn').select2('val', '');
		}
		 $('#pivotEditCols').select2({
				placeholder : "Enter Pivot Element",
				minimumInputLength : 1,
				allowClear: true,
				width: 'resolve',
				multiple : true,
				ajax : {
					url : serviceCallMethodNames["suggestionsLookUp"].url,
					dataType : 'json',
					type : 'GET',
					data : function(term) {
						if(!$('#pivotTableColumn').select2('val')) {
							return;
						}
						return {
							q : term,
							source : THIS.sourceType,
							database : THIS.database,
							lookupQuery : "select distinct " + $('#pivotTableColumn').select2('val') + ", '' from "  + table + " where lower(" + $('#pivotTableColumn').select2('val') + ") like  '%<Q>%' "
						};
					},
					results : function(data) {
						return {
							results : data
						};
					}
				},
				initSelection : function(element, callback) {
					callback($.map(element.val().split(','),
							function(id) {
								id = id.split(":");
								return {
									id : id[0],
									name : id[1]
								};
							}));
				},
				dropdownCsclass : "bigdrop",
				formatResult : function (resp) {
					return resp.id;
				 },
				formatSelection : function (resp) {
					return resp.id;
				 },
				escapeMarkup : function(m) {
					return m;
				}
			});
		 $('#pivotTableOrder').select2({
				placeholder : "Enter Pivot Element",
				minimumInputLength : 1,
				allowClear: true,
				width: 'resolve',
				multiple : true,
				ajax : {
					url : serviceCallMethodNames["suggestionsLookUp"].url,
					dataType : 'json',
					type : 'GET',
					data : function(term) {
						if(!$('#pivotTableColumn').select2('val')) {
							return;
						}
						return {
							q : term,
							source : THIS.sourceType,
							database : THIS.database,
							lookupQuery : "select distinct " + $('#pivotTableColumn').select2('val') + ", '' from "  + table + " where lower(" + $('#pivotTableColumn').select2('val') + ") like  '%<Q>%' "
						};
					},
					results : function(data) {
						return {
							results : data
						};
					}
				},
				initSelection : function(element, callback) {
					callback($.map(element.val().split(','),
							function(id) {
								id = id.split(":");
								return {
									id : id[0],
									name : id[1]
								};
							}));
				},
				dropdownCsclass : "bigdrop",
				formatResult : function (resp) {
					return resp.id;
				 },
				formatSelection : function (resp) {
					return resp.id;
				 },
				escapeMarkup : function(m) {
					return m;
				}
			});
		$('.pivot-settings-multi-table').click(function(){
			if($(this).find(':checkbox').is(':checked')) {
				addPivotTable(THIS.tableList, THIS.sourceType, THIS.database, THIS.ColumnList);
			} else {
				$('#pivotTables').empty();
			}
		});
		$("#disablefilter").prop( "checked",false);
		$("#disableshow").prop( "checked",false);
		if (!fieldMapping){
			$("#filtersMapping tbody tr").each(function(){
				 $(this).find("input").select2("val",$(this).find("td:first").attr("field"));
			});
			$("#chartsMapping tbody tr").each(function(){
				 $(this).find("input").select2("val",$(this).find("td:first").attr("field"));
			});
		}else{
			if(fieldMapping.disablefilter)
			{
				$("#disablefilter").prop( "checked", fieldMapping.disablefilter);
			}
			if(fieldMapping.disableshow)
			{
				$("#disableshow").prop( "checked", fieldMapping.disableshow);
			}
			$.each(fieldMapping,function(key,value){
				var parentTr = $("#chartsMapping tbody td[field="+key+"]").parent();
				parentTr.find("input").select2("val",value);
				parentTr = $("#filtersMapping tbody td[field="+key+"]").parent();
				parentTr.find("input").select2("val",value);
			});
		}
	};
	
	var addPivotTable = function(defaultTablesList, source, database, columnsList, existingTables, tableObjMap) {
		$('#pivotTables').find('.addTableRow').hide();
		var curTableObj = {};
		if(existingTables && existingTables.length > 0) {
			curTableObj = existingTables.splice(0, 1)[0];
		}
		var tableName = (curTableObj && curTableObj.tableAlias) ? curTableObj.tableAlias : (curTableObj && curTableObj.table) ? curTableObj.table :  'New Table' ;
		var tabName = 'table-';
		tabName+= (curTableObj && curTableObj.table) ? curTableObj.table :  $('#pivotTables').find('li').length + 1;
		var str = '<li data-tabName="'+tabName+'">';
		str += '<a href="#'+tabName+'" data-toggle="tab">';
		str += '<i class="fa fa-md fa-fw fa-plus-square color-blue cursor-pointer addTableRow" title="add table"></i>&nbsp;';
		str += tableName;
		str += '&nbsp; <i class="fa fa-md fa-fw fa-remove txt-color-red cursor-pointer deleteTable" title="delete link"></i>';
		str += '</a>';
		str += '</li>';
		$('#pivotTables').append(str);
		tableObjMap = addPivotTableContents(curTableObj, defaultTablesList, source, database, columnsList, tableObjMap, tabName);
		$('.addTableRow').one('click', function(){
			addPivotTable(defaultTablesList, source, database, columnsList);
		});
		$('.deleteTable').on('click', deletePivotTable);
		
		$('.pivotColumnTA').on('keyup', function(){
			var obj = atob($('.hdn-pivotTable-' + $('#hdn-pivotTableOptions-tabName').val() + '-pivotObject').val());
			try{
				obj = JSON.parse(obj);
			} catch(e) {
				obj = {};
			}
			
			if(!obj[$('#hdn-pivotTableOptions-column').val()]) {
				obj[$('#hdn-pivotTableOptions-column').val()] = {};
			}
			
			obj[$('#hdn-pivotTableOptions-column').val()][$(this).attr('data-type')] = $(this).val();
			$('.hdn-pivotTable-' + $('#hdn-pivotTableOptions-tabName').val() + '-pivotObject').val(btoa(JSON.stringify(obj)));
		});
		
		$('.pivotColumnSel').on('change', function(){
			var obj = atob($('.hdn-pivotTable-' + $('#hdn-pivotTableOptions-tabName').val() + '-pivotObject').val());
			try{
				obj = JSON.parse(obj);
			} catch(e) {
				obj = {};
			}
			
			if(!obj[$('#hdn-pivotTableOptions-column').val()]) {
				obj[$('#hdn-pivotTableOptions-column').val()] = {};
			}
			
			obj[$('#hdn-pivotTableOptions-column').val()][$(this).attr('data-type')] = $(this).val();
			$('.hdn-pivotTable-' + $('#hdn-pivotTableOptions-tabName').val() + '-pivotObject').val(btoa(JSON.stringify(obj)));
		});
		
		$('.pivotColumnChk').on('click', function(){
			var obj = atob($('.hdn-pivotTable-' + $('#hdn-pivotTableOptions-tabName').val() + '-pivotObject').val());
			try{
				obj = JSON.parse(obj);
			} catch(e) {
				obj = {};
			}
			
			if(!obj[$('#hdn-pivotTableOptions-column').val()]) {
				obj[$('#hdn-pivotTableOptions-column').val()] = {};
			}
			obj[$('#hdn-pivotTableOptions-column').val()][$(this).attr('data-type')] = $(this).prop('checked');
			$('.hdn-pivotTable-' + $('#hdn-pivotTableOptions-tabName').val() + '-pivotObject').val(btoa(JSON.stringify(obj)));
		});
		
		if(existingTables && existingTables.length === 0 && tableObjMap && Object.keys(tableObjMap).length > 0) {
			var request = {
					"source" : source,
					"database" : database,
					"tables": Object.keys(tableObjMap).join(',')
			};
			$('#pivotTableElements').find('.tab-pane').first().addClass('active');
			$('#pivotTables').find('.addTableRow').last().show();
			callAjaxService("getMultiColumnList", function(response){
					if(response && !response.isException) {
						for(var i in response) {
							for(var j = 0; j < tableObjMap[i].length; j+=1) {
								callBackGetPivotTableColumns(response[i], tableObjMap[i][j].obj, tableObjMap[i][j].cols, null);
							}
						}
						$('#pivotTables').find('li').first().addClass('active');
					}
				}, callBackFailure, request, "POST");
		} else if(existingTables && existingTables.length > 0) {
			addPivotTable(defaultTablesList, source, database, columnsList, existingTables, tableObjMap);
		}
	}
	
	var addPivotTableContents = function(curTableObj, defaultTablesList, source, database, columnsList, tableObjMap, tabName) {
		var str = '<div class="tab-pane pivotTable-'+tabName+'" id="'+tabName+'"><div class="row additionalRow" style="margin-top: 5px;">';
			if(!curTableObj.columnConfigs) {
				curTableObj.columnConfigs = {};
			}
			str += '<input type="hidden" class="col-pivotTable" value="'+tabName+'" />';
			str += '<input type="hidden" class="col-pivotObject hdn-pivotTable-'+tabName+'-pivotObject" value="'+btoa(JSON.stringify(curTableObj.columnConfigs))+'" />';
			str += '<div class="col-sm-1">';
			 str += '<label>Table</label>';
			str += '</div>';
			str += '<div class="col-sm-3">';
			  str += '<input type="text" data-type="table" class="pivotTable width-100per"/>';			  
			str += '</div>';
			str += '<div class="col-sm-1">';
			 str += '<label>Table Alias: </label>';
			str += '</div>';
			str += '<div class="col-sm-2">';
			str += '<span class="smart-form"><label class="input"><input type="text" class="pivotTableAs" /></label></span>';
			str += '</div>';
			str += '<div class="col-sm-1">';
			 str += '<label>PrimaryKeys</label>';
			str += '</div>';
			str += '<div class="col-sm-3">';
			  str += '<input type="text" data-type="table-column" class="pivotPKColumn width-100per"/>';
			str += '</div>';
			str += '<div class="row">';
			  str += '<div class="col-sm-10">';
			   	str += '<select data-tabName="'+tabName+'" data-type="table-column" class="pivotColumn width-100per" multiple></select>';
			  str += '</div>';
			str += '</div>';
			str += '</div></div>';
			$('#pivotTableElements').append(str);
			var tableEle = $('.additionalRow').last().find('.pivotTable');
			var pkEle = $('.additionalRow').last().find('.pivotPKColumn');
			
			$(tableEle).select2({
				data: defaultTablesList,
				placeholder: 'Select table'
			}).on("change", function(e) {
				var table = $(e.target).val();
				var columnsObj = $(e.target).closest('.additionalRow');
				var request = {
						"source" : source,
						"database" : database,
						"table": table
				};
				callAjaxService("getColumnList", function(response){callBackGetPivotTableColumns(response,columnsObj);},
						callBackFailure, request, "POST");
			});
			if(Object.keys(curTableObj).length > 0) {
				if(curTableObj.tableAlias) {
					$('.additionalRow').last().find('.pivotTableAs').val(curTableObj.tableAlias);
				}
				$(tableEle).select2('val', curTableObj.table);
				if(!tableObjMap) {
					tableObjMap = {};
				}
				if(tableObjMap[curTableObj.table]) {
					tableObjMap[curTableObj.table].push({cols: curTableObj.columns, obj: $('.additionalRow').last()});
				} else {
					tableObjMap[curTableObj.table] = [{ cols: curTableObj.columns, obj:$('.additionalRow').last()}];
				}
			}
			if(curTableObj && curTableObj.pKey) {
				$(pkEle).val(curTableObj.pKey)
			}
			$(pkEle).select2({
				data: columnsList,
				multiple : true
			});
			return tableObjMap;
	}
	
	var deletePivotTable = function() {
		if($('#pivotTables li').length === 1) {
			showNotification('error', 'Atleast 2 tables are required');
			return;
		}
		$('.pivotTable-' + $(this).closest('li').attr('data-tabName')).remove();
		$(this).closest('li').remove();		
		$('#pivotTables').find('.addTableRow').last().show();
	}
	
	var callBackGetPivotTableColumns = function(response, obj, defaultColumns) {
		if(response && response.isException) {
			showNotification('error', response.customMessage);
			return;
		}
		if(response.length > 0) {
			var str = '';
			response.map(function(o){
				var selected = (defaultColumns && defaultColumns.indexOf(o.columnName) > -1)? 'selected' : '';
				str += '<option class="dualLstBxSect" value="'+o.columnName+'" '+selected+'> '+o.columnName+' </option>';
			});
			var column = $(obj).find('.pivotColumn');
			var table  = $(obj).find('.pivotTable');
			column.html(str);
			column.bootstrapDualListbox({
				 moveOnSelect: false				 
			});
			$('.dualLstBxSect').on('click', function(e){
				var selected = $(e.target).val();				
				var tabName = $(this).closest('.tab-pane').find('.col-pivotTable').val();
				if(tabName) {
					var obj;
					try{ 
						obj = JSON.parse(atob($('.hdn-pivotTable-'+tabName+'-pivotObject').val()));
						obj = obj[selected] || {};
					} catch(e) {
						obj = {};
					}
					
					$('.pivotTableOptions').show();
					$('#hdn-pivotTableOptions-column').val(selected);
					$('#hdn-pivotTableOptions-table').val($(table).select2('val'));
					$('#hdn-pivotTableOptions-tabName').val(tabName);
					
					$('#pivotDisplayName').val(obj.columnAlias);
					$('#pivotRenderfunction').val(obj.render);
					$('#pivotTypeoptions').val(obj.typeOpt);
					$('#pivotHeadderSource').val(obj.headdingSource);
					$('#pivotEdittype').val((obj.edtType) ? obj.edtType : 'text');
					
					$('#pivotHeader').prop('checked', !!obj.pivotHeader);
					$('#pivotHideColumn').prop('checked', !!obj.hidden);
					$('#pivotWrapText').prop('checked', !!obj.wraptext);
					$('#pivotEditable').prop('checked', !!obj.editable);
				}
			});
		}
	}
	
	var configureColumns = function(obj,table,selectedColumns,selectedColumnList,fieldMapping){ 
		var idCounter = $(obj).data("counter");
		var groupByColumnList = $(obj).data("groupby-columns");
		var pivotSettings = $("#wid-id-datatable_" + idCounter).data("pivotSettings");
		var workflow = $("#wid-id-datatable_" + idCounter).data("workflow");
		var rule = $("#wid-id-datatable_" + idCounter).data("rule");
		$("#myModalDrilldown").modal("show");
		$("#myModalDrilldown .modal-header").hide();
		$("#poDocView").empty();
		var avaiableColumnsDiv = $("<div/>").addClass("col-md-5 col-xs-5 col-sm-5 col-lg-5").append($("<h6/>").html("Available Columns"));
		var selectedColumnsDiv = $("<div/>").addClass("col-md-5 col-xs-5 col-sm-5 col-lg-5").append($("<h6/>").html("Selected Columns"));
		var columnActionsDiv = $("<div/>").addClass("col-md-2 col-xs-2 col-sm-2 col-lg-2 column-config-actions");
		$("#myModalDrilldown .modal-footer").remove();
		$("#poDocView").append($("<div/>").addClass("row").append(avaiableColumnsDiv).append(columnActionsDiv).append(selectedColumnsDiv));
		columnActionsDiv
			.append($("<a/>").addClass("btn btn-success btn-lg width-150").html("Add").append($("<i/>").addClass("fa fa-lg fa-fw fa-angle-right")).click(function(){addColumn();}))
			.append($("<a/>").addClass("btn btn-danger btn-lg width-150").html("Remove").prepend($("<i/>").addClass("fa fa-lg fa-fw fa-angle-left")).click(function(){removeColumn();}))
			.append($("<a/>").addClass("btn btn-success btn-lg width-150").html("Add All").append($("<i/>").addClass("fa fa-lg fa-fw fa-angle-double-right")).click(function(){addAllColumn();}))
			.append($("<a/>").addClass("btn btn-danger btn-lg width-150").html("Remove All").prepend($("<i/>").addClass("fa fa-lg fa-fw fa-angle-double-left")).click(function(){removeAllColumn();}));
		var selectedColumnsUL = $("<ul/>").attr("id","sortableSelected").addClass("columnConfig connectedSortable");
		if (selectedColumns && selectedColumns.length > 0) {
			selectedColumnsUL.prepend($("<input/>").addClass("form-control").attr({"placeholder":"Search Selected Columns","name":"txtsearchColumns"}));
			for ( var i = 0; i < selectedColumns.length; i++) {
				if (selectedColumns[i]["class"] === "audit-log")
					continue;
				if (selectedColumns[i]["class"] === "actionColumn")
					continue;

				var typeOptions;
				if (typeof(selectedColumns[i].typeoptions) === 'object'){
					typeOptions = JSON.stringify(selectedColumns[i].typeoptions);
				}else{
					typeOptions = selectedColumns[i].typeoptions;
				}
				selectedColumnsUL.append($("<li/>").addClass("dd-item").append($("<div/>").addClass("dd-handle").html(selectedColumns[i].title?selectedColumns[i].title:selectedColumns[i].data)).attr({
					"data-field": selectedColumns[i].data,
					"data-displayname":selectedColumns[i].title?selectedColumns[i].title:selectedColumns[i].data,
					"data-class":selectedColumns[i]["class"],
					"data-renderfunction":selectedColumns[i].renderfunction,
					"data-groupbyaggregate":selectedColumns[i].groupbyaggregate,
					"data-hidecoloumn":selectedColumns[i].hidecoloumn,
					"data-summaryaggregate":selectedColumns[i].summaryaggregate,
					"data-editable":selectedColumns[i].editable,
					"data-mandatory":selectedColumns[i].mandatory,
					"data-typeoptions":typeOptions,
					"data-edittype":selectedColumns[i].edittype,
					"data-primarykey":selectedColumns[i].primarykey,
					"data-wraptext":selectedColumns[i].wraptext,
					"data-enabledetail":selectedColumns[i].enabledetail,
					"data-colwidth":selectedColumns[i].colwidth,
					"data-editvalue":selectedColumns[i].editvalue,
					"data-columndesc":selectedColumns[i].columndesc,
					"data-columninfo": selectedColumns[i].columninfo,
					"data-disable":selectedColumns[i].disable,
					"data-datatype":selectedColumns[i].datatype,
					"data-sourceconfig":selectedColumns[i].sourceconfig,
					"data-tabledatabase":selectedColumns[i].tabledatabase,
					"data-databaseconfig":selectedColumns[i].databaseconfig
				}));
			}
		}
		selectedColumnsDiv.append(selectedColumnsUL);
		var request = {
				"source" : THIS.sourceType,
				"database" : THIS.database
			};
		if(THIS.dataQuery){
			request.query = calculateDynamicFilter(THIS.dataQuery,THIS.filters);
		}else{
			request.table = table;
		}
			enableLoading();
			callAjaxService("getColumnList", function(response){callBackGetDatatableColumnList(table,response,avaiableColumnsDiv,selectedColumnList,groupByColumnList,fieldMapping, pivotSettings,workflow,rule);},
					callBackFailure, request, "POST");
			$("#poDocView").append($("<hr/>")).append($("<div/>").addClass("row")
					.append($("<div/>").addClass("col-md-12 col-xs-12 col-sm-12 col-lg-12 smart-form border-right-dashed").attr("id","showColumnDetails")
					)
				);
			$("#tableConfigUpdate").unbind("click").bind("click",function(){updateColumns(idCounter,table);}); 		
	};
	var callBackGetDatatableColumnList = function(table,response,avaiableColumns,selectedColumnList,groupByColumnList,fieldMapping, pivotSettings,workflow, rule){
		disableLoading();
		var fields = [];
		selectedColumnList = selectedColumnList || "";
		selectedColumnList = selectedColumnList.split(",");
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}else if (response === undefined || response === null) {
			return;
		}
		var numbericFields = [];
		var stringDatatypes = [ "string", "character varying", "varchar",
				"character", "char", "text" ];
		var availableColumnsUL = $("<ul/>").attr("id","sortableAvaiable").addClass("columnConfig connectedSortable");
		if (response && response.length > 0) {
			availableColumnsUL.prepend($("<input/>").addClass("form-control").attr({"placeholder":"Search Available Columns","name":"txtsearchColumns"}));
			for ( var i = 0; i < response.length; i++) {
				if (selectedColumnList.indexOf(response[i].columnName) === -1){
					availableColumnsUL.append($("<li/>").addClass("dd-item").append($("<div/>").addClass("dd-handle").html(response[i].columnName)).attr({
						"data-field": response[i].columnName,
						"data-displayname":response[i].columnName
					}));
				}
				if (!stringDatatypes.contains(response[i].dataType)) {
					numbericFields.push({"id":response[i].columnName,"text":response[i].columnName});
				} 
				fields.push({"id":response[i].columnName,"text":response[i].columnName}); 
				$.each(avaiableColumns.parent().find('#sortableSelected').find('li'), function(index, val){
					if(val.dataset.field == response[i].columnName)
						val.dataset["datatype"] = response[i].dataType
				});
			}
			tableFilterConfiguration(fields,fieldMapping, pivotSettings, table,workflow, rule );
		}
		avaiableColumns.append(availableColumnsUL);
		$("#sortableSelected [name='txtsearchColumns'],#sortableAvaiable [name='txtsearchColumns']").keyup(function () {
			var rex = new RegExp($(this).val(), 'i');
	        $(this).parent().find("li").hide();
	        $(this).parent().find("li").filter(function () {
	        	 return rex.test($(this).text());
	        }).show();
	    });
		$( "#sortableAvaiable, #sortableSelected" ).sortable({
		      connectWith: ".connectedSortable"
		    }).disableSelection();
		$("#groupByFields").select2({
			placeholder : "Choose table",
			data : numbericFields,
			multiple:true
		});
		if (groupByColumnList && groupByColumnList !== "")
			$("#groupByFields").select2("val",groupByColumnList.split(","));
		$(".dd-item").click(function(){columnClick(this);});
	};
	
	/**
	 * Will update Column Configuration details to Table DOM
	 * 
	 */
	var updateColumns = function(idCounter,table){ 
		if ($("#sortableSelected li").length === 0){
			alert("Columns can't be empty.");
			return;
		}
		$("#myModalDrilldown").modal("hide");
		var datatableHeader = $(".header","#wid-id-datatable_"+idCounter);
		$(".header","#wid-id-datatable_"+idCounter).empty();
		var fieldMapping = {};
		$("#chartsMapping tbody tr:not(.nomapping),#filtersMapping tbody tr:not(.nomapping)").each(function(){
			var sourceField = $(this).find("td:first").attr("field");
			var targetField = $(this).find("input").select2("val");
			fieldMapping[sourceField] = targetField;
		});
		var disablefilter=$("#disablefilter").is(":checked");
		fieldMapping["disablefilter"]=(disablefilter);
		var disableshow=$("#disableshow").is(":checked");
		fieldMapping["disableshow"]=(disableshow);
		$("#wid-id-datatable_"+idCounter).data("filtermapping",fieldMapping);
		var columns = "";
		var columnObject = [];
		var pivotSettings = {
				"enabled" : $('#pivotTableColumn').select2('val') ? true : false,
				"column" : $('#pivotTableColumn').select2('val') || '',
				"multiTable" : $('.pivot-settings-multi-table').find(':checkbox').prop('checked'),
				"tables": [],
				"colOrder": $('#pivotTableOrder').select2('val'),
				"editCols" : $('#pivotEditCols').select2('val'),
				"tableAs" : $('#pivotTableAs').val(),
				"headerCols" : $('#pivotHeaderCols').select2('val')
		};
		var workFlow = {};
		if($("#tableWorlflowSettings").find("[name='isWorkflow']").is(":checked")){
			workFlow.column = $("#workflowColumn").select2('val');
			workFlow.isWorkflow = true;
			workFlow.disableOnComplete = $("#tableWorlflowSettings").find("[name='wf-disableOnComplete']").is(":checked");
			$("#wid-id-datatable_"+idCounter).data('workflow', workFlow);
		}
		var rule = {};
		if($('#isRule').is(':checked')) {
			rule.isRule = true;
			rule.ruleLink = $('#ruleLink').val();
			rule.column = $('#ruleColumn').select2('val');
			rule.ruleProject = $('#ruleProject').val();
			$("#wid-id-datatable_"+idCounter).data('rule', rule);
		}
		if($('.pivot-settings-multi-table').find(':checkbox').prop('checked')) {
			var tables = [];
			$('#pivotTableElements .additionalRow').each(function(ele){
				var obj = {
						"editable" : $(this).find('.pivotEditChkBox').prop('checked'),
						"table": $(this).find('.pivotTable').select2('val'),
						"columns" : $(this).find('.pivotColumn').val(),
						"pKey" : $(this).find('.pivotPKColumn').select2('val'),
						"tableAlias" : $(this).find('.pivotTableAs').val(),
						"columnConfigs": {}
					}
				try{
					obj.columnConfigs = JSON.parse(atob($(this).find('.col-pivotObject').val()));
				}catch (e){
					obj.columnConfigs = {};
				}
				tables.push(obj);
			});
			pivotSettings.tables = tables;
		}
		$("#wid-id-datatable_"+idCounter).data('pivotSettings', pivotSettings);		
		$("#sortableSelected li").each(function(){ 
			var typeOptions;
			if (typeof($(this).data("typeoptions")) === 'object'){
				typeOptions = JSON.stringify($(this).data("typeoptions"));
			}else{
				typeOptions = $(this).data("typeoptions");
			}
			datatableHeader.append(
					$("<th/>").addClass("numbercolumn sorting").attr({"tabindex":"0","rowspan":"1","colspan":"1","aria-label":$(this).data("displayname")})
					
					.append($("<div/>").attr({"data-field":$(this).data("field")}).addClass("sortable-header")
							.append($("<a/>").addClass('axisHeader').html($(this).data("displayname")).attr({
								"data-field": $(this).data("field"),
								"data-displayname":$(this).data("displayname"),
								"data-class":$(this).data("class"),
								"data-renderfunction":$(this).data("renderfunction"),
								"data-groupbyaggregate":$(this).data("groupbyaggregate"),
								"data-hidecoloumn":$(this).data("hidecoloumn"),
								"data-editable":$(this).data("editable"),
								"data-mandatory":$(this).data("mandatory"),
								"data-typeoptions":typeOptions,
								"data-edittype":$(this).data("edittype"),
								"data-editvalue":$(this).data("editvalue"),
								"data-colwidth":$(this).data("colwidth"),
								"data-primarykey":$(this).data("primarykey"),
								"data-wraptext":$(this).data("wraptext"),
								"data-datatype":$(this).data('datatype'),
								"data-enabledetail":$(this).data("enabledetail"),
								"data-summaryaggregate":$(this).data("summaryaggregate"),
								"data-columndesc":$(this).data("columndesc"),
								"data-columninfo": $(this).data("columninfo"),
								"data-disable":$(this).data("disable"),
								"data-datatype":$(this).data("datatype"),
								"data-sourceconfig":$(this).data("sourceconfig"),
								"data-tabledatabase":$(this).data("tabledatabase"),
								"data-databaseconfig":$(this).data("databaseconfig"),
								
							}))));
		
				var column = {"data" :$(this).data("field"),"defaultContent" : ""};
						if ($(this).data("displayname")&& $.trim($(this).data("displayname")) !== "")
							column.title =$(this).data("displayname");
						else
							column.title =$(this).data("field");
						if ($(this).data("class")&& $.trim($(this).data("class")) !== "")
							column["class"] =$(this).data("class");
						
						if ($(this).data("groupbyaggregate")&& $.trim($(this).data("groupbyaggregate")) !== "")
							column["groupbyaggregate"] =$(this).data("groupbyaggregate");
						
						if ($(this).data("sourceconfig")&& $.trim($(this).data("sourceconfig")) !== "")
							column["sourceconfig"] =$(this).data("sourceconfig");
						
						if ($(this).data("databaseconfig")&& $.trim($(this).data("databaseconfig")) !== "")
							column["databaseconfig"] =$(this).data("databaseconfig");
						
						if ($(this).data("tabledatabase")&& $.trim($(this).data("tabledatabase")) !== "")
							column["tabledatabase"] =$(this).data("tabledatabase");
						
						if ($(this).data("datatype")&& $.trim($(this).data("datatype")) !== "") 
							column["datatype"] =$(this).data('datatype');

						if ($(this).data("summaryaggregate")&& $.trim($(this).data("summaryaggregate")) !== "")
							column["summaryaggregate"] =$(this).data("summaryaggregate");
						
						if ($(this).data("edittype")&& $.trim($(this).data("edittype")) !== "")
							column["edittype"] =$(this).data("edittype");
						
						if ($(this).data("editvalue")&& $.trim($(this).data("editvalue")) !== "")
							column["editvalue"] =$(this).data("editvalue");
						
						if ($(this).data("colwidth")&& $.trim($(this).data("colwidth")) !== "")
							column["colwidth"] =$(this).data("colwidth");
						
						if ($(this).data("wraptext")&& $.trim($(this).data("wraptext")) !== "")
							column["wraptext"] =$(this).data("wraptext");
						
						if ($(this).data("enabledetail")&& $.trim($(this).data("enabledetail")) !== "")
							column["enabledetail"] =$(this).data("enabledetail");
						
						if ($(this).data("primarykey")&& $.trim($(this).data("primarykey")) !== "")
							column["primarykey"] =$(this).data("primarykey");
						
						if ($(this).data("renderfunction")&& $.trim($(this).data("renderfunction")) !== "") {
							column.renderfunction =$(this).data("renderfunction").toString();// .replace(/"/g,//
																								// "'");
						}
						if ($(this).data("typeoptions")&& $.trim($(this).data("typeoptions")) !== "") {
							column.typeoptions =$(this).data("typeoptions");// .replace(/"/g,
																			// "'");
						}
						if ($(this).data("hidecoloumn") && $(this).data("hidecoloumn") === true) {
							column.hidecoloumn = true;
						}
						if ($(this).data("editable") && $(this).data("editable") === true) {
							column.editable = true;
						}
						if ($(this).data("mandatory") && $(this).data("mandatory") === true) {
							column.mandatory = true;
						}
						if ($(this).data("columninfo") && $(this).data("columninfo") === true) {
							column.columninfo = true;
						}
						if ($(this).data("disable") && $(this).data("disable") === true) {
							column.disable = true;
						}
						if ($(this).data("columndesc")&& $.trim($(this).data("columndesc")) !== "")
							column["columndesc"] = $(this).data("columndesc");
						columnObject.push(column);
						if (columns === "")
							columns += $(this).data("field");
						else
							columns += ","+ $(this).data("field");
		});	
		$("#configure_"+idCounter).unbind("click").bind("click",function(){
			configureColumns(this, table,columnObject,columns,fieldMapping);
		});
	};
	
	/**
	 * Invoked to configure Column details when Column is clicked in
	 * Configurations
	 * 
	 */
	var callGetDatabaseList = function(response){
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			$("#showColumnDetails").find("#databaseconfig").html("");
			for ( var i = 0; i < response.length; i++) {
				$("#showColumnDetails").find("#databaseconfig").append('<option>'+response[i]+'</option>')	
			}
		
	}
	}
	var columnClick = function(obj){
		var objData = $(obj).data();
		$(".dd-handle.selected").removeClass("selected");
		$(obj).find("div").addClass("selected").toggleClass("active");
		$("#btnUpdateHolder").addClass("margin-top-55");
		var hideColumn = {
				"type":"checkbox",
				"name":"hidecoloumn"
		};
		if (objData.hidecoloumn &&  objData.hidecoloumn === true){
			hideColumn.checked = "checked";
		};
		var editable = {
				"type":"checkbox",
				"name":"editable"
		};
		if (objData.editable && objData.editable === true){
			editable.checked = "checked";
		};
		var mandatory = {
				"type":"checkbox",
				"name":"mandatory"
		};
		if (objData.mandatory && objData.mandatory === true){
			mandatory.checked = "checked";
		};
		var primaryKey = {
				"type":"checkbox",
				"name":"primarykey"
		};
		if (objData.primarykey && objData.primarykey === true){
			primaryKey.checked = "checked";
		}
		var wrapText = {
				"type":"checkbox",
				"name":"wraptext"
		};
		if (objData.wraptext && objData.wraptext === true){
			wrapText.checked = "checked";
		}
		
		var enableDetail = {
				"type":"checkbox",
				"name":"enabledetail"
		};
		if (objData.enabledetail && objData.enabledetail === true){
			enableDetail.checked = "checked";
		}
		var columninfo = {
				"type":"checkbox",
				"name":"columninfo"
		};
		if (objData.columninfo && objData.columninfo === true){
			columninfo.checked = "checked";
		}
		var disable = {
				"type":"checkbox",
				"name":"disable"
		};
		if (objData.disable &&  objData.disable === true){
			disable.checked = "checked";
		};
		var typeOptions;
		if (typeof(objData.typeoptions) === 'object'){
			typeOptions = JSON.stringify(objData.typeoptions);
		}else{
			typeOptions = objData.typeoptions;
		}
		$("#showColumnDetails").empty()
		.append($("<div/>").addClass("col col-2").append($("<label/>").html("Field").addClass("label")).append($("<label/>").addClass("input").append($("<input/>").attr({"type":"text","name":"field","disabled":"disabled"}).val(objData.field))))
		.append($("<div/>").addClass("col col-2").append($("<label/>").html("Display Name").addClass("label")).append($("<label/>").addClass("input").append($("<input/>").attr({"type":"text","name":"displayname"}).val(objData.displayname))))
		.append($("<div/>").addClass("col col-2").append($("<label/>").html("Width").addClass("label")).append($("<label/>").addClass("input").append($("<input/>").attr({"type":"text","name":"colwidth"}).val(objData["colwidth"]))))
		.append($("<div/>").addClass("col col-2").append($("<label/>").html("Class").addClass("label")).append($("<label/>").addClass("input").append($("<input/>").attr({"type":"text","name":"class"}).val(objData["class"]))))
		.append($("<div/>").addClass("col col-2 groupbyfields").append($("<label/>").addClass("label").html("Aggregate")).append($("<label>").addClass("select").append($("<select/>").attr({"name":"aggregateGroupBy"}).unbind("change").bind("change",function(){$(obj).data('groupbyaggregate',this.value);}).append($("<option>").attr("value","none").html("None")).append($("<option>").attr("value","sum").html("SUM")).append($("<option>").attr("value","count").html("COUNT")).append($("<option>").attr("value","distinct").html("Distinct")).append($("<option>").attr("value","avg").html("AVG")).val( objData.groupbyaggregate||'none')).append($("<i/>"))))
		.append($("<div/>").addClass("col col-2 enableSummaryfield").append($("<label/>").addClass("label").html("Summary Type")).append($("<label>").addClass("select").append($("<select/>").attr({"name":"aggregateSummary"}).unbind("change").bind("change",function(){$(obj).data('summaryaggregate',this.value);}).append($("<option>").attr("value","none").html("None")).append($("<option>").attr("value","NA").html("N/A")).append($("<option>").attr("value","sum").html("SUM")).append($("<option>").attr("value","count").html("COUNT")).append($("<option>").attr("value","distinct").html("Distinct")).append($("<option>").attr("value","avg").html("AVG")).val(objData.summaryaggregate||'none')).append($("<i/>"))))
		/*
		 * .append($("<div/>").addClass("col col-2 smart-form") .append($("<label/>").addClass("toggle").html("Invisible").append($("<input>").attr(hideColumn)).append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"}))) )
		 */
		.append($("<div/>").addClass("col col-4").append($("<label/>").html("Render Function").addClass("label margin-top-10")).append($("<label/>").addClass("textarea").append($("<textarea/>").attr({"name":"renderfunction"}).val(objData.renderfunction))))
		.append($("<div/>").addClass("col col-3 ").append($("<label/>").addClass("label").html("Source")).append($("<label>").addClass("select").append($("<select/>").attr({"name":"sourceconfig"}).unbind("change").bind("change",function(){
			$(obj).data('sourceconfig',this.value);
			request = {
					"source" : this.value
				};
				if(this.value){
				callAjaxService("getDatabaseList", callGetDatabaseList,
						callBackFailure, request, "POST");
				}else{
					$("#showColumnDetails").find("#databaseconfig").html("");
				}													
			}).append($("<option>").attr("value","hive").html("Hive")).append($("<option>").attr("value","postgress").html("Postgress")).val(objData.sourceconfig||'')).append($("<i/>"))))
			.append($("<div/>").addClass("col col-3 ").append($("<label/>").addClass("label").html("Database")).append($("<label>").addClass("select").append($("<select/>").attr({"name":"databaseconfig","id":"databaseconfig"}).unbind("change").bind("change",function(){
			   $(obj).data('databaseconfig',this.value);
			}).append($("<option>").attr("value",objData.databaseconfig).html(objData.databaseconfig)).val(objData.databaseconfig||'')).append($("<i/>"))))
			//.append($("<div/>").addClass("col col-3 ").append($("<label/>").addClass("label").html("Database")).append($("<label>").addClass("select").append($("<select/>").attr({"id":"tabledatabase","name":"tabledatabase"}).append($("<option>").html("none")).val(objData.database||'')).append($("<i/>"))))
		.append($("<div/>").addClass("col col-3 margin-top-10 smart-form")
			.append( $("<label/>").addClass("toggle").html("Invisible").append($("<input>").attr(hideColumn)).append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"})))
			.append($("<label/>").addClass("toggle").html("Mandatory").append($("<input>").attr(mandatory)).append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"})))
			.append($("<label/>").addClass("toggle").html("Primary Key").append($("<input>").attr(primaryKey)).append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"})))
			.append($("<label/>").addClass("toggle").html("Visible Detail").append($("<input>").attr(enableDetail)).append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"})))
			.append($("<label/>").addClass("toggle").html("Disable").append($("<input>").attr(disable)).append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"})))
			)
		.append($("<div/>").addClass("col col-2 margin-top-10 smart-form")
			.append($("<label/>").addClass("toggle").html("Editable").append($("<input>").attr(editable)).append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"})))
			.append($("<label/>").addClass("toggle").html("Wrap Text").append($("<input>").attr(wrapText)).append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"})))
			.append( $("<label/>").addClass("label col-sm-5").html("Edit Type"))
			.append( $("<label>").addClass("select col-sm-7").append($("<select/>").attr({"name":"edittype"}).unbind("change")
					.bind("change",function(){$(obj).data('edittype',this.value);})
					.append($("<option/>").attr("value","text").html("Text"))
					.append($("<option/>").attr("value","datestring").html("Date As Text"))
					.append($("<option/>").attr("value","txtarea").html("Text Area"))
					.append($("<option/>").attr("value","select").html("Select"))
					.append($("<option/>").attr("value","checkbox").html("Checkbox"))
					.append($("<option/>").attr("value","toggle").html("Toggle"))
					.append($("<option/>").attr("value","date").html("Date"))
					.append($("<option/>").attr("value","lookup").html("Lookup"))
					.append($("<option/>").attr("value","number").html("Number")).val(objData.edittype||'text')).append($("<i/>")))
			.append($("<label/>").html("Default Value").addClass("label col-sm-7")).append($("<label/>").addClass("input col-sm-7").append($("<input/>").attr({"type":"text","name":"editvalue"}).val(objData.editvalue)))		
			)
		.append($("<div/>").addClass("col col-3 margin-top-10 smart-form")
				.append($("<label/>").html("Select Option / Lookup Query").addClass("label")).append($("<label/>").addClass("textarea").append($("<textarea/>").attr({"name":"typeoptions"}).val(typeOptions)))
			)
		.append($("<div/>").addClass("col col-3 margin-top-10 smart-form")
				.append($("<label/>").append($("<label/>").addClass("toggle").html("Column Info").append($("<input>").attr(columninfo)).append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"})))
						.addClass("label")).append($("<label/>").addClass("textarea").append($("<textarea/>").attr({"name":"columndesc","title":"Add column description here."}).val(objData.columndesc)))
			)	;
	
		$("input,textarea", $("#showColumnDetails")).unbind("click").unbind("mouseup").unbind("change").bind(
				'keyup mouseup change',
				function() {
					if ($(this).attr("type") === "checkbox")
						$(obj).data($(this).attr("name"), $(this).is(":checked"));
					else{
						$(obj).data($(this).attr("name"), $(this).val());
						if ($(this).attr("name") === "displayname")
							$(obj).find("div").html($(this).val());
					}
				});
	};
	var addColumn = function(){
		$(".dd-handle.active",$("#sortableAvaiable")).each(function() {
			// $("#sortableSelected").prepend($(this).parent());
			$("#sortableSelected").find("[name='txtsearchColumns']").after($(this).parent());
		});
	};
	var addAllColumn = function(){
		$(".dd-handle",$("#sortableAvaiable")).each(function() {
			// $("#sortableSelected").append($(this).parent());
			$("#sortableSelected").find("[name='txtsearchColumns']").after($(this).parent());
		});
	};
	var removeColumn = function(){
		$(".dd-handle.active",$("#sortableSelected")).each(function() {
			// $("#sortableAvaiable").prepend($(this).parent());
			$("#sortableAvaiable").find("[name='txtsearchColumns']").after($(this).parent());
		});
	};
	var removeAllColumn = function(){
		$(".dd-handle",$("#sortableSelected")).each(function( ) {
		// $("#sortableAvaiable").append($(this).parent());
			$("#sortableAvaiable").find("[name='txtsearchColumns']").after($(this).parent());
		});
	};
		
	/**
	 * Will Handle Custom adding/editing of FACT
	 */
	var addCustomFactHandler = function(){
		var factName = $("#txtFactName").val();
		var factCalculations =  $("#factCalculations").val();
		var customtype = $("#txtFactName").data("customtype");
		if(!factName){
			showNotification("notify","Please Enter field name.");
			return;
		}
		var duplicatefacts = THIS.customFacts.filter(function(o){return o.name === factName;});
		if(!$("#txtFactName").attr("disabled") &&(THIS.dimensionData[factName] || THIS.measureData[factName])){
			showNotification("notify",factName+" is already exist");
			return;
		}
		if(duplicatefacts && duplicatefacts.length>0){
			THIS.customFacts.forEach(function(customfact){
				if(customfact.name === factName){
					customfact.formula = factCalculations;
				}
			});
			
		}else{
			THIS.customFacts.push({
				name:factName,
				customtype:customtype,
				formula:factCalculations
			});
			if(customtype === "measure"){
				$("#facts").find("ul").append($("<li/>").addClass("dd-item").attr({"name":factName})
						.append($("<a/>").addClass("dd-handle margin-right-5 btn-fact").data("formula",factCalculations).data("customtype",customtype)
								.attr({"href" : "javascript:void(0);","title" : factName}).html(factName).append($("<i/>").addClass("fa fa-md fa-fw fa-remove txt-color-red cursor-pointer pull-right").attr({"name":"removecustomfact"}))));
				THIS.measureData[factName] = factName;
			}else{
				$("#columnList").find("ol").append($("<li/>").attr({"name":factName}).addClass("dd-item ui-draggable ui-draggable-handle active")
						.data("formula",factCalculations).data("customtype",customtype)
						.append($("<div/>").addClass("dd-handle").html(factName).append($("<i/>").addClass("fa fa-md fa-fw fa-remove txt-color-red cursor-pointer pull-right").attr({"name":"removecustomfact"}))));			
			}
		}
		if(customtype === "measure"){
            $("#facts").find("[name='"+factName+"']").data("formula",factCalculations);
		}else{
			$("#columnList").find("[name='"+factName+"']").data("formula",factCalculations);
			}
		$("#addCustomFactModel").modal("hide");
		$("#columnList li,#facts a").draggable({
			appendTo : "body",
			helper : "clone"
		});
		$("[name='removecustomfact']").unbind('click').bind('click',function(e){
			$(this).parents("li").remove();
			var factName = $(this).parents("li").attr("name");
			e.preventDefault();e.stopPropagation();
			THIS.customFacts.forEach(function(customfact){
				if(customfact.name === factName){
					THIS.customFacts.splice(THIS.customFacts.indexOf(customfact), 1);
				}
			});
		});
		$("#columnList li,#facts li").bind("click",addEditCustomFactHandler);
		/*
		 * $("#customFactForm","#addCustomFactModel").submit();
		 * if(!$("#customFactForm","#addCustomFactModel").valid()){ return; }
		 * $("#addCustomFactModel").modal("hide"); var factName =
		 * $("#customFactForm #txtFactName" ,"#addCustomFactModel").val(); var
		 * factDisplayName = $("#customFactForm #txtFactDisplayname"
		 * ,"#addCustomFactModel").val(); var factCalculations =
		 * $("#customFactForm #factCalculations" ,"#addCustomFactModel").val();
		 * var dependentFacts = $("#customFactForm #drpDependentFacts"
		 * ,"#addCustomFactModel").select2('val'); var isCustomfactEdit = false;
		 * THIS.customFacts.forEach(function(customfact){ if(customfact.fact ===
		 * factName){ isCustomfactEdit = true; customfact.displayname =
		 * factDisplayName; customfact.dependentfacts = dependentFacts;
		 * customfact.calculation = factCalculations; $(".widget-body
		 * a[data-fact='"+factName+"']",
		 * $("#wid-id-measures")).attr({"data-displayname":factDisplayName,"title" :
		 * factName}).html(factDisplayName); $("#addFacts
		 * a[data-fact='"+factName+"']").attr({"data-displayname":factDisplayName}).html(factDisplayName); }
		 * });
		 * THIS.dependentFacts.push.apply(THIS.dependentFacts,dependentFacts);
		 * if(!isCustomfactEdit){ $(".widget-body", $("#wid-id-measures"))
		 * .append($("<a/>").addClass("btn btn-info txt-color-white btn-sm
		 * margin-right-5 btn-fact customfact") .attr({"href" :
		 * "javascript:void(0);","data-fact":factName,"data-displayname":factDisplayName,"title" :
		 * factName}).html(factDisplayName));
		 * THIS.customMeasureList.push(factName); var factObj = {}; factObj.fact =
		 * factName; factObj.displayname = factDisplayName;
		 * factObj.dependentfacts = dependentFacts; factObj.calculation =
		 * factCalculations;
		 * 
		 * THIS.customFacts.push(factObj); $("#columnList li,#facts
		 * a").draggable({ appendTo : "body", helper : "clone" });
		 * $("#columnList li,#facts
		 * a.customfact").bind("click",addEditCustomFactHandler); }
		 */
	};
	$("#btnCustomFactSubmit").unbind("click").bind("click", addCustomFactHandler);
	var addEditCustomFactHandler = function(e){
		$("#addCustomFactModel").modal("show");
		$("#txtFactName").val($(e.currentTarget).text()).attr({disabled:"disabled"});
		$("#factCalculations").val($(e.currentTarget).data("formula"));
		$("#txtFactName").data($(e.currentTarget).data());
		/*
		 * $("#customFactForm","#addCustomFactModel").validate().resetForm();
		 * $("#customFactForm .customFactDetails"
		 * ,"#addCustomFactModel").val(""); $("#customFactForm #txtFactName"
		 * ,"#addCustomFactModel").removeAttr("disabled"); var customMeasureList =
		 * []; THIS.customMeasureList.forEach(function(item){
		 * customMeasureList.push({id:item,text:item}); });
		 * $("#drpDependentFacts").select2({ data: customMeasureList, width:260,
		 * multiple:true, placeholder:"Select Dependent Facts" });
		 * $("#btnCustomFactSubmit").unbind("click").bind("click",
		 * addCustomFactHandler);
		 * 
		 * if($(this).attr("id") !== "btnAddFact"){
		 * $("#addCustomFactModel").modal("show"); $("#customFactForm
		 * #txtFactName" ,"#addCustomFactModel").attr({disabled:"disabled"});
		 * var customFact = isCustomFact($(this).attr("data-fact"));
		 * $("#customFactForm #txtFactName"
		 * ,"#addCustomFactModel").val(customFact.fact); $("#customFactForm
		 * #txtFactDisplayname"
		 * ,"#addCustomFactModel").val(customFact.displayname);
		 * $("#customFactForm #factCalculations"
		 * ,"#addCustomFactModel").val(customFact.calculation);
		 * $("#customFactForm #drpDependentFacts"
		 * ,"#addCustomFactModel").select2('val',customFact.dependentfacts); }
		 */
	};
	
	$("#btnAddFact").unbind().bind("click",addEditCustomFactHandler);	
	$("#customFactForm","#addCustomFactModel").submit(function(e) {
		e.preventDefault();
	});
	
	$("#customFactForm","#addCustomFactModel").validate({
		rules : {
			txtFactName : {
				required : true
			},
			txtFactDisplayname : {
				required : true
			},
			factCalculations : {
				required : true
			}
		}
	});
	
	var editDeleteFactHandler = function(){
		$("#editDeletFactModel").modal("hide");
		$("#addFacts").children().each(function(index,item){
			var factElement =  $("#editFactForm input[value='"+$(item).attr("data-fact")+"']","#editDeletFactModel");
			if(factElement === undefined || factElement.length===0 ){
				$(item).remove();
			}
		});
		$("#editFactForm .editFactGroup","#editDeletFactModel").each(function(index1,item1){
			var factName = $(item1).find("input[id^='txtFactName_']").val();
			var factDisplayName = $(item1).find("input[id^='txtFactDisplayname_']").val();
			$("#addFacts a[data-fact='"+factName+"']").attr({"data-displayname":factDisplayName}).html(factDisplayName);
			THIS.customFacts.forEach(function(customfact){
				if(customfact.fact === factName){
					customfact.displayname = factDisplayName;
					$(".widget-body a[data-fact='"+factName+"']", $("#wid-id-measures")).attr({"data-displayname":factDisplayName,"title" : factName}).html(factDisplayName);
				}
			});
		});
	};
	
	var deleteFactHandler = function(e){
		$(e.currentTarget).parent().remove();
	};
	
	var editDeleteFact = function(){
		if($("#addFacts").children().length===0){
			return false;
		}
		$("#editFactForm .editFactGroup").remove();
		$("#addFacts").children().each(function(index,item){
			var fact = $(item).attr("data-fact");
			var factDisplayName = $(item).attr("data-displayname");
			$("#editFactForm").append('<div class="form-group form-group-sm paddingBottom5 editFactGroup"><div class="col-sm-5"> <input class="form-control editFactDetails" type="text" id="txtFactName_'+index+'" name="txtFactName_'+index+'" title="Fact Name" value="'+fact+'" disabled ><span class="note"><i class="fa fa-check text-success"></i> Fact Name</span></div>'
					+ '<div class="col-sm-6"><input class="form-control editFactDetails" type="text" id="txtFactDisplayname_'+index+'" name="txtFactDisplayname_'+index+'" title="Display Name" value="'+factDisplayName+'" ><span class="note"><i class="fa fa-check text-success"></i> Fact Display Name</span></div>'
					+' <i class="fa fa-minus-circle fa-2x col-md-1 btnRemoveFact" style="cursor: pointer;" title="Remove Fact"></i> </div>');
		});
		
		$("#btnEditFactSubmit","#editDeletFactModel").unbind("click").bind("click", editDeleteFactHandler);
		$(".btnRemoveFact","#editDeletFactModel").unbind("click").bind("click", deleteFactHandler);
	};
	
	$("#btnEditFact").unbind().bind("click",editDeleteFact);	
	$("#editFactForm","#editDeletFactModel").submit(function(e) {
		e.preventDefault();
	});
	
	this.getFiltersFromProfile = function(){
		return getFiltersFromProfile(filterContentId);
	};

	this.updateSolrCategories = function() {
		enableLoading();
		callAjaxService("updateSolrCategories", callBackUpdateSolrCategories, callBackFailure, {"rules":"All"}, "POST")
	};
	var callBackUpdateSolrCategories = function(response) {
		disableLoading();
		if (response && (response.isException || response.isException === 'true')){
			showNotification("error",response.customMessage);
			return;
		} else {
			showNotification("success","Categories updated successfully.");
			return;
		}
	};
	this.updateSolrCSpeciality = function() {
		enableLoading();
		callAjaxService("updateSolrCategories", callBackUpdateSolrCSpeciality, callBackFailure, {}, "POST")
	};
	var callBackUpdateSolrCSpeciality = function(response) {
		disableLoading();
		if (response && (response.isException || response.isException === 'true')){
			showNotification("error",response.customMessage);
			return;
		} else {
			showNotification("success","Categories updated successfully.");
			return;
		}
	};
	var callBackSuccess = function(){
		showNotification("success"," downloaded successfully");
	}
	var callBackGetProfileActions = function(response){
		disableLoading();
		if (response && (response.isException || response.isException === 'true')){
			showNotification("error",response.customMessage);
			return;
		} else {
			$("#actionEdit").hide();
			$("#actionsGridDiv").show();
			$("#profileActionsModal").modal("show");
			$("#modalHeaderProfileActions").html("Actions ["+THIS.ProfileDesc + "]");
			$("#actionsGrid tbody").empty();
			if (response.length === 0){
				$("#actionsGrid").append($("<tr/>")
						.append($("<td/>").attr("colspan",5).addClass("text-center").append($("<div/>").html("No actions configured.")))
						);
				return;
			}
			THIS.profileActions = response;
			for (var i=0; i< response.length;i++ ){
				var actionEnabled = response[i].enabled ? "Yes": "No"; 
				$("#actionsGrid").append($("<tr/>").data("request",response[i])
						.append($("<td/>").html(response[i].actionType))
						.append($("<td/>").html(response[i].name))
						.append($("<td/>").html(response[i].linkedProfileName))
						.append($("<td/>").html(response[i].type))
						.append($("<td/>").html(actionEnabled))
						);
				THIS.actionData[response[i].name] = response[i].name;
			};
			$("#actionsGrid tr").unbind("click").bind("click",function(){
				addProfileAction(this);
			});
		}
	};
	// export/download profile
	var exportProfile = function(){
		var profileName = (THIS.ProfileName).replaceAll(" ","");
		var request = {
				"ProfileName":profileName,
				"ProfileId" : THIS.ProfileID
			};
		
		var downloadURL = 'rest/dataAnalyser/exportProfile';
		$.fileDownload(downloadURL, {
			successCallback: function() {
				showNotification("success","Profile Json exported successfully");
            },
            failCallback: function(responseHtml) {
                $preparingFileModal.dialog('close');
                $("#error-modal").html($(responseHtml).text());
                $("#error-modal").dialog({ modal: true });
            },
	        httpMethod: "POST",
	        data: request
	    });
	};
		var importProfile = function(){
		var profileId = THIS.ProfileID;
		        if (!profileId) {
		            showNotification("error", "Primary Key not configured.");
		        }
		        $("#myModalUpload").modal('show');
		        $('.fileUpload').change(function() {
		            $(this).parent().next().val($(this).val().replace(/^C:\\fakepath\\/i, ''));
		        });
		        $("#frmModalExportImport").unbind('submit').submit(function() {
		           enableLoading();
		            var formData = new FormData(document.getElementById("frmModalExportImport"));
		            formData.append("ProfileId", THIS.ProfileID);
		            $.ajax({
		                url: serviceCallMethodNames["importProfile"].url,
		                type: "POST",
		                data: formData,
		                enctype: 'multipart/form-data',
		                processData: false,
		                contentType: false
		            }).done(function(response) {
		                disableLoading();
		                $("#myModalUpload").modal('hide');
		                if (!response)
		                    showNotification("error", "Error while importing file");
		                else if (response && response.isException) {
		                    showNotification("error", response.customMessage);
		                    return;
		                } else {
		                    showNotification("success", response.message);
		                }
		            });
		            return false;
		        });
	};
	var getProfileActions = function(){
		var request = {
			"profileID":THIS.ProfileID
		};
		enableLoading();
		callAjaxService("getProfileActions", callBackGetProfileActions, callBackFailure, request, "POST");
	};
	var addProfileAction = function(obj){
		$("#actionEdit").show(300);
		$("#actionsGridDiv").hide(300);

		function formatResult(resp) {
			return resp.name;
		}

		function formatSelection(resp) {
			return  resp.name;
		}
		$("#profileMappingConfig").hide();
		$("#actionProfile").select2({
			placeholder : "Select profile to link",
			minimumInputLength : 1,
			
			maximumSelectionSize: 1,
			formatSelectionTooBig: function () {
				return 'You have already selected action';
			},
			multiple : false,
			initSelection: function (element, callback) {
                var data = { "id" :"name" };
                callback(data);
            },
			ajax : {
				url : serviceCallMethodNames["suggestionsLookUp"].url,
				dataType : 'json',
				type : 'GET',
				data : function(
						term) {
					return {
						q : term,
						source : "postgreSQL",
						database : "E2EMFPostGres",
						lookupQuery : "select action,display_name from actions where lower(display_name) like '%<Q>%' or cast(action_id as CHAR) like '%<Q>%' or lower(action) like '%<Q>%'"
					};
				},
				results : function(data) { 
					return {
						results : data
					};
				}
			},
			formatResult : formatResult,
			formatSelection : formatResult,
			dropdownCsclass : "bigdrop",
			escapeMarkup : function(m) {
				return m;
			} 
		}).on("change",function(e) {
			if (e.val !== null && e.val !== ""){
				$("#mappingSection label").removeClass("state-disabled");
				$("#mappingSection input").removeAttr("disabled");
				
			} else{
				$("#mappingSection label").addClass("state-disabled");
				$("#mappingSection input").attr("disabled","disabled");
			}
		});
		$("[name=checkbox-mapping]").change(function() { 
		    if ($(this).is(":checked")){
		    	var profileID = $("#actionProfile").select2("val");
		    	if (!profileID || profileID === null || profileID === ""){
		    		showNotification("error","Select linking profile for mapping.");
		    		return;
		    	}
				var request = {
					"profileKey" : profileID,
					"viewId":-1
				};
				enableLoading();
				callAjaxService("getProfileConfig", callBackGetProfileConfigForMapping,
						callBackFailure, request, "POST");
				$("#profileMappingConfig").show();
		    }else{
		    	$("#profileMappingConfig").hide();
		    }
		});
		if (obj){
			var actionRequestData = $(obj).data();
			if (!actionRequestData || !actionRequestData.request)
				return

			var actionRequest = actionRequestData.request;
			$("#actionEdit [name=actionname]").val(actionRequest.name);
			$("[name=urlType]").val(actionRequest.type);
			$("[name=actionType]").val(actionRequest.actionType);
			$("[name=actionEnabled]").removeAttr("checked");
			if (actionRequest.enabled)
				$("[name=actionEnabled]").trigger("click");
				
			var linkedProfile = {
				"id":actionRequest.linkedProfileKey,
				"name":actionRequest.linkedProfileName
			};
			$("#actionProfile").select2('data', linkedProfile, true);
			$("#actionEdit").data("id",actionRequest.id);
			$("#mappingSection label").removeClass("state-disabled");
			$("#mappingSection input").removeAttr("disabled");
			$("[name=checkbox-mapping]").removeAttr("checked");
			if (actionRequest.fieldMapping && actionRequest.fieldMapping !== "{}"){
				$("[name=checkbox-mapping]").trigger("click");
				THIS.fieldMapping = Jsonparse(actionRequest.fieldMapping,"FIELD MAPPING");
			}else{
				THIS.fieldMapping = null;
				$("[name=checkbox-mapping]").attr("checked",false);
			}
		}else{
			$("#actionEdit").data("id",0);
			$("[name=checkbox-mapping]").attr("checked",false);
		}
		
	};
	var callBackGetDatatableColumnListForMapping = function(response){
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			var fieldMapping = THIS.fieldMapping || null;
			disableLoading();
			$("#mappingSection").show();
			var profileFilters = getFilters();
			var chartsConfig = getConfig();
			if (profileFilters && profileFilters.length > 0){
				var filterMappingBody = $("#profileFiltersMapping tbody").empty();
				for (var f=0; f < profileFilters.length; f++){
					filterMappingBody.append($("<tr/>").append($("<td/>").attr("field",profileFilters[f].fieldName).html(profileFilters[f].displayName + " (" + profileFilters[f].fieldName + ")")).append($("<td/>").append($("<input/>").addClass("width-100per"))));
				};
			};
			if (chartsConfig && chartsConfig.length > 0){
				var chartsMappingBody =  $("#profileChartsMapping tbody").empty();
				for (var panel=0; panel < chartsConfig.length; panel++){
					if (!chartsConfig[panel].charts && chartsConfig[panel].charts.length === 0)
						continue;
					for (var chart=0; chart < chartsConfig[panel].charts.length; chart++){
						if (chartsConfig[panel].charts[chart].charttype !== "datatable")
							chartsMappingBody.append($("<tr/>").append($("<td/>").attr("field",chartsConfig[panel].charts[chart].fieldname).html(chartsConfig[panel].charts[chart].displayname + " (" +chartsConfig[panel].charts[chart].fieldname + ")")).append($("<td/>").append($("<input/>").addClass("width-100per"))));
						if (chartsConfig[panel].charts[chart].charttype === "chart-map"){
							chartsMappingBody.append($("<tr/>").append($("<td/>").attr("field",chartsConfig[panel].charts[chart].latitude).html(chartsConfig[panel].charts[chart].latitude)).append($("<td/>").append($("<input/>").addClass("width-100per"))));
							chartsMappingBody.append($("<tr/>").append($("<td/>").attr("field",chartsConfig[panel].charts[chart].longitude).html(chartsConfig[panel].charts[chart].longitude)).append($("<td/>").append($("<input/>").addClass("width-100per"))));
						}
						if (chartsConfig[panel].charts[chart].charttype === "chart-scatter"){
							chartsMappingBody.append($("<tr/>").append($("<td/>").attr("field",chartsConfig[panel].charts[chart].xaxis).html(chartsConfig[panel].charts[chart].latitude)).append($("<td/>").append($("<input/>").addClass("width-100per"))));
							chartsMappingBody.append($("<tr/>").append($("<td/>").attr("field",chartsConfig[panel].charts[chart].yaxis).html(chartsConfig[panel].charts[chart].longitude)).append($("<td/>").append($("<input/>").addClass("width-100per"))));
						}
					}
				};
			};
			var fields = [];
			if (response && response.length > 0) {
				for ( var i = 0; i < response.length; i++) {
					fields.push({"id":response[i].columnName,"text":response[i].columnName});
				}
			}
			$("#profileFiltersMapping input,#profileChartsMapping input").select2({
				allowClear: true,
				placeholder : "Choose mapping",
				tags : fields,
				maximumSelectionSize: 1
			});
			if (!fieldMapping){
				$("#profileFiltersMapping tbody tr").each(function(){
					 $(this).find("input").select2("val",$(this).find("td:first").attr("field"));
				});
				$("#profileChartsMapping tbody tr").each(function(){
					 $(this).find("input").select2("val",$(this).find("td:first").attr("field"));
				});
			}else{
				$.each(fieldMapping,function(key,value){
					var parentTr = $("#profileChartsMapping tbody td[field="+key+"]").parent();
					parentTr.find("input").select2("val",value);
					parentTr = $("#profileFiltersMapping tbody td[field="+key+"]").parent();
					parentTr.find("input").select2("val",value);
				});
			}
		}
	};
	var callBackGetProfileConfigForMapping = function(response){
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			var data = Jsonparse(response["config"],"CONFIG");
			var database = data[0]["database"];
			var source = data[0]["source"];
			var table = data[0]["configtable"];
			var request = {
					"source" : source,
					"database" : database
				};
			if(THIS.dataQuery){
				request.query = calculateDynamicFilter(THIS.dataQuery,THIS.filters);
			}else{
				request.table = table;
			}
				enableLoading();
				callAjaxService("getColumnList",callBackGetDatatableColumnListForMapping,
						callBackFailure, request, "POST");	
		}
	};
	var callBackProfileMappingSave = function(response){
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			editProfileMappingBack();
			getProfileActions();
		}
	};
	var profileMappingSave = function(){
		if (!$("#frmModalProfileActions").valid())
			return;
		var linkedProfile = $("#actionProfile").select2("data");
		var fieldMapping = {};
		if ($("[name=checkbox-mapping]").is(":checked")){
			$("#profileChartsMapping tbody tr:not(.nomapping),#profileFiltersMapping tbody tr:not(.nomapping)").each(function(){
				var sourceField = $(this).find("td:first").attr("field");
				var targetField = $(this).find("input").select2("val");
				fieldMapping[sourceField] = targetField;
			});
		}
		var action ={
			id: $("#actionEdit").data("id")||0,
			profileID: THIS.ProfileID,
			name:$("#actionEdit [name=actionname]").val(),
			fieldMapping:JSON.stringify(fieldMapping),
			linkedProfileKey:linkedProfile.id,
			linkedProfileName:linkedProfile.name,
			type:$("[name=urlType]").find("option:selected").val(),
			actionType:$("[name=actionType]").find("option:selected").val(),
			enabled:$("[name=actionEnabled]").is(":checked")
		};
		
		var request = {};
		request.action = JSON.stringify(action);
		enableLoading();
		callAjaxService("saveProfileAction",callBackProfileMappingSave,
				callBackFailure, request, "POST");
	};
	var editProfileMappingBack = function(){
		$("#actionEdit").hide(300);
		$("#actionsGridDiv").show(300);
	};
	this.tooltipActionClick = function(obj){
		var selectedProfile = $(obj).text();
		var selectedProfileAction = null;
		var filtersForwaded = $(obj).attr('data-filtersForwaded');
		if(filtersForwaded){
			localStorage['filtersForwaded'] = filtersForwaded;
		}
		$.each(THIS.profileActions,function(){
			if (this.name === selectedProfile){
				selectedProfileAction =  this;
				return
			}
		});
		if (selectedProfileAction){
			var profileKey = location.hash.replace(/^#/, '').split("?")[0];
			localStorage["from"] = "";
			var filterQuery,profiling,profileId;
			if(THIS.modalProfile){
				profileId = THIS.profileId;
				profiling = THIS.modalProfile;
				profileKey = THIS.profileKey;
				// localStorage.currentProfile = profileKey;
			}else{
				profiling = THIS.profiling;
				if(location.hash && location.hash.indexOf("from$=$")>-1){
					localStorage.currentProfile = location.hash.substring(location.hash.indexOf("from$=$")+7,location.hash.length);;
				}
			}
			if (selectedProfileAction.fieldMapping)
				filterQuery = profiling.grapher.constructFilterQuery(Jsonparse(selectedProfileAction.fieldMapping,"field mapping"),"",profileId);
			else
				filterQuery = profiling.grapher.constructFilterQuery();
			
			localStorage[profileKey+"_"+selectedProfileAction.linkedProfileKey] = filterQuery;
						
			var url = location.href.replace(location.hash,'')+"#"+ selectedProfileAction.linkedProfileKey+"?from$=$"+profileKey;
			if (selectedProfileAction.type ==="newtab"){
				var win = window.open(url, '_blank');
				win.focus();
			} else if(selectedProfileAction.type ==="modal"){
				getProfileForModal(selectedProfileAction,filterQuery);
			}else{
				 window.location=url;
			}
		}
	};
	this.getProfileForModal = function(selectedProfileAction,filterQuery,filterDefaultValue){
		templates.getWidget(null, function() {
			templates.getPie(null, function() {
				templates.getDatatable(null, function() {
					templates.getSummaryConfig(null, function() {
						templates.getSummary(null, function() {
							templates.getCalendar(null, function() {
								templates.getImageWidget(null, function() {
									templates.getNGram(null, function() {
										templates.getIFrame(null, function() {
											templates.getHTMLTemplate(null, function() {
												/*
												 * if(localStorage["isMultipleProfile"])
												 * delete
												 * localStorage["isMultipleProfile"];
												 */
												getProfileForModal(selectedProfileAction,filterQuery,filterDefaultValue);
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	};
	var getProfileForModal = function(selectedProfileAction,filterQuery,filterDefaultValue){
		var request = {
			"profileKey" : selectedProfileAction.linkedProfileKey
		};
		enableLoading();
		callAjaxService("getProfileConfig", function(response){callBackGetProfileConfigForModal(response,filterQuery,selectedProfileAction,filterDefaultValue)},callBackFailure, request, "POST");
	};
	var callBackGetProfileConfigForModal= function(response,filterQueryFromProfile,selectedProfileAction,filterDefaultValue){
		var profileKey;
		if(selectedProfileAction.linkedProfileKey){
			profileKey = selectedProfileAction.linkedProfileKey;
		}
		if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        } else if (response === undefined || response === null) {
            showNotification("error", "No Data avaiable..");
            return;
        }
        var data = Jsonparse(response.config,"PROFILE ON MODAL");
        if(response.profileActions && response.profileActions.length>0){
        	THIS.profileActions = response.profileActions;
        }
        if(profileKey)
        	THIS.profileKey = profileKey;
        data = data[0];
        THIS.profileId = data.id;
        var profileConfig = Jsonparse(data["config"].replace(/@/g, "'"),"CONFIG");
        $("#profileOnModal").modal("show");
        $("#profileOnModal > div").css({"width":selectedProfileAction.width || "500px"});
        $("#divContentProfileonModal").find(".widget-body").empty();
        drawProfile(profileConfig, $("#divContentProfileonModal"),data.id);
        var summary = Jsonparse(data.summary);
        drawSummary(summary, "#divContentProfileonModal",data.id);
        var factsColumn = Jsonparse(data.fact);
        factsColumn = Object.keys(factsColumn).length>0?factsColumn:[];
        var facts = factsColumn.map(function(o){return o.fact});
        var customFacts = Jsonparse(data.customfacts);
        customFacts = Object.keys(customFacts).length>0?customFacts:[];
        // var customfactData = customFacts.replace(/@/g, "'");
        var customFields,fieldDescriptions;
        var columnsObj = getColumnsFromConfig(profileConfig,data.id,[],[]);
        facts = facts.concat(columnsObj.facts);
        columns = columnsObj.columns;
        customFields = columnsObj.customFields;
		fieldDescriptions = columnsObj.fieldDescriptions;		
        var filters = Jsonparse(data.filters);
        var filterQuery,dataQuery;
        if(filters && Object.keys(filters).length>0){
        	if(filterDefaultValue){
        		for(field in filterDefaultValue){
        			if(filterDefaultValue.hasOwnProperty(field)){
        				for(var i=0;i<filters.length;i++){
                    		if(filters[i].fieldName === field){
                    			filters[i].defaultvalue = filterDefaultValue[field];
                    			break;
                    		}
                    	}
        			}
        		}
            	
        	}
        	filterQuery = getFilterQueryFromFilterConfig(filters,data.source);
        }else{
        	filterQuery = " 1 = 1 ";
        }
        if(response.dataQuery){
        	dataQuery = Jsonparse(response.dataQuery);
        }
        if(filterQueryFromProfile){
        	filterQuery+=" And "+filterQueryFromProfile;
        	var datatableConfig = profileConfig.filter(function(o){return o.id.indexOf("datatable")>-1});
        	if(datatableConfig && datatableConfig.length>0){
        		for(var j=0;j<datatableConfig.length;j++){
        			datatableConfig[j].charts[0].data.filter = filterQuery;
        			if(dataQuery && dataQuery.query){
        				datatableConfig[j].charts[0].data.dataQuery = dataQuery.query;
        			}
        		}
        	}
        }	
        var summaryObj = getSummaryFieldFromConfig(summary,facts);
        facts = summaryObj.facts;
		if (summaryObj.sumarryFields && summaryObj.sumarryFields.length > 0)
			columns += (columns && columns.trim()?" , ":"") + summaryObj.sumarryFields.join(" , ");
        var factsObj = getColumnByFacts(facts,customFacts,customFields);
        if(factsObj.customFields && Object.keys(factsObj.customFields)>0){
			customFields = factsObj.customFields;				
		}
        var columns = columnsObj.columns.replaceAll(",_count","");
		if(factsObj.columns){
			columns += "," + factsObj.columns;
		}
        var request ={
        	source:data.source,
        	database:data.database,
        	columns:columns,
        	filters:filterQuery
        };
        if(dataQuery && dataQuery.query){
        	request.query = dataQuery.query;
        }else{
        	request.table = data.configtable;
        }
        if (fieldDescriptions && fieldDescriptions.length > 0) {
			request.masterdata = JSON.stringify({
				"descriptionList" : fieldDescriptions
			});
		}
        if(THIS.sourceType)
        	request.source = THIS.sourceType;
        else
        	THIS.sourceType = request.source;
        if(THIS.database)
        	request.database = THIS.database;
		if (THIS.additionalconfig && THIS.additionalconfig.profileGroupBy){
			request.groupByEnabled = true;
		}
		if(columns){
			enableLoading();
			callAjaxService("getData", function(response){
				callBackGetData(response,profileConfig,summary,filters,facts,data.id)
				}, callBackFailure,request, "POST",null,true);
			
		}else{
			callBackGetData(null, profileConfig,summary,filters,facts,data.id);
		}
		// var callBackGetData = function(response, config,
		// summary,filters,facts,profileId)
        // callService(profileConfig, null, null,data.id);
	};

	var setUnreadCount = function (filters,profileId){
		var filterQuery = '',fieldNames = {};
		try{
			if(profileAnalyser.filterMapping && profileAnalyser.filterMapping.length>0){
				for(var i=0;i<profileAnalyser.filterMapping.length;i++){
					if(profileAnalyser.filterMapping[i].mappedProfiles && profileAnalyser.filterMapping[i].mappedProfiles[profileId]){
						fieldNames[profileAnalyser.filterMapping[i].fieldname] = profileAnalyser.filterMapping[i].mappedProfiles[profileId];
					}
				}
			}
		}catch(ex){
			console.log(ex);
		}
		if(filters){
    		for(var i = 0; i<filters.length; i++){
    			if(filters[i].chat && filters[i].defaultvalue && filters[i].defaultvalue.length>0){
    				var field = (fieldNames[filters[i].fieldName.trim()] || filters[i].fieldName.trim());
    				if (filters[i].defaultvalue[0].id)
    					filterQuery += field+'_'+filters[i].defaultvalue[0].id.replace(/[^a-zA-Z0-9]/g,'_');
    				else
    					filterQuery += field+'_'+filters[i].defaultvalue[0].replace(/[^a-zA-Z0-9]/g,'_');
    			}
    		}	
		}
		var updateUnreadCount = function(unreadCount,configId){
			if(unreadCount){
				$("#"+configId+"commentsPopup").find(".unread_span").html(unreadCount);
				$("#"+configId+"commentsPopup").find(".unread_span").css({"display":"inline"});
			}else{
				$("#"+configId+"commentsPopup").find(".unread_span").hide();
			}
		};
		function getCommentsSuccessCallBackHandler(response){
			if (!response){
		        showNotification("error", "Error while Requesting for Existing Comments");
			}
		    else if (response && response.isException) {
		        	if(response.message.search('exceeds')||response.message.search('No Data Found')){
		                showNotification("error", response.message);
		        	} else{
		        		showNotification("error", response.customMessage);
		        	}													                
		            return;
		    }else {
		    		var responseCommentLog = [];
		    		if(response.commentLog){
		    			responseCommentLog =  Jsonparse(response.commentLog);
		    		}
		    		response =  JSON.parse(response.commentList);
	        		var allCommentPannel = [];
	        		for(var index= 0; index < response.length; index++ ){
	        			allCommentPannel.push(response[index].commentPanelId)
	        		}
	        		var uniqueCommentPannel = []
	        		uniqueCommentPannel = allCommentPannel.filter(function(item, pos) {
		    		    return allCommentPannel.indexOf(item) == pos;
		    		})
		    		var leftOutWidget = [];
		    			for(var t= 0; t < responseCommentLog.length; t++){
		    					leftOutWidget.push(responseCommentLog[t].commentPanelId)
		    		}
		    		var leftOutWidgets = uniqueCommentPannel.filter(function(item) {
		    				  return leftOutWidget.indexOf(item) === -1
		    				})
		    		if(responseCommentLog){
		    			unreadCalculator(responseCommentLog, response, false);
		    			if(leftOutWidgets){
		    				unreadCalculator(leftOutWidgets, response, true);
		    			}
		    		}
		    	}
		}
		// calculate Number of unread messages
		function unreadCalculator(responseCommentLog, response, isLeftWidget){

			for(var k = 0; k< responseCommentLog.length; k++){
				var unreadCount = 0;
				for(var x = 0; x < response.length; x++){
					if(isLeftWidget){
						if(response[x].commentPanelId==responseCommentLog[k] && System.userName!=response[x].userName){
							unreadCount++;
						}
					}else{
						if( response[x].commentTimeStamp>=responseCommentLog[k].lastread && response[x].commentPanelId==responseCommentLog[k].commentPanelId && System.userName!=response[x].userName){
							unreadCount++;
						}
					}
				}
				if(unreadCount == 0){
					var panelID = isLeftWidget?responseCommentLog[k]:responseCommentLog[k].commentPanelId;
					updateUnreadCount(undefined,panelID);
				}
				else{
					var panelID = isLeftWidget?responseCommentLog[k]:responseCommentLog[k].commentPanelId;
					updateUnreadCount(unreadCount,panelID);
				}
			}
			
		}
// var profileName = THIS.ProfileName;
		var profileName = $('.breadcrumb li').last().text().split('-')[0].trim();
		var request= {
				'profileName' : profileName,
				/* 'commentSectionName' : commentSectionName, */
				'filterQuery' : (filterQuery)?filterQuery:"",
				'setLastRead' : false
			};
		callAjaxService("getComments", function(response){getCommentsSuccessCallBackHandler(response);}, callBackFailure,request, "POST",null,true);
		};
};
var dataAnalyser = new DataAnalyser();