<%@page import="java.security.Principal"%>
<%@page import="org.springframework.security.authentication.UsernamePasswordAuthenticationToken"%>
<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="com.relevance.prism.util.PrismHandler"%>
<%@page import="com.relevance.prism.util.Log"%>
<%@page import="java.sql.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
   <title>CASSA</title>
   <link rel="shortcut icon" href="img/favicon/favicon_qrcode.ico" type="image/x-icon">
   <link rel="icon" href="img/favicon/favicon_qrcode.ico" type="image/x-icon">
   <link rel="icon" href="css/bootstrap.min.css" type="image/x-icon">
	<style>
	.jumbotron-header {
		font-family: 'open-sans';
		font-weight: 600;
		font-style: normal;
		font-size: 36px;
		color: rgb(213, 25, 0);
		text-align: center;
	}
	.jumbotron-paragraph {
		font-family: 'open-sans';
		font-weight: 400;
		font-style: normal;
		font-size: 18px;
		color: #828282;
		text-align: center;
	}
	</style>
</head>
<body>	

	<%
	    PrismHandler.logMethodEntry("SSO Entry", "SSO Logic started");
		Connection connection = null;
	//	request.getUserPrincipal().getName();
	try {
		System.setProperty("javax.security.auth.useSubjectCredsOnly", "false");
		System.setProperty("java.security.auth.login.config", "/home/svc_dsynrelevance/tomcat/apache-tomcat-7.0.91/bin/login.conf");
		System.setProperty("java.security.krb5.conf", "/home/svc_dsynrelevance/tomcat/apache-tomcat-7.0.91/bin/krb5.conf");
		//HttpSession session = request.getSession(true);
		session.setAttribute("user", "anonymoususer");
		Log.Info("Logged in user (from kerborose):"+request.getRemoteUser());
		String userName = (request.getRemoteUser() == null) ? "" : request.getRemoteUser().toLowerCase();
		out.println("logged in user : " + request.getRemoteUser());
		System.out.println("logged in user : " + request.getRemoteUser());
		//session.setAttribute("app", "ROLE_Evidence");
		//session.setAttribute("sso", "true");
		//Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	//authentication.setAuthenticated(true);
    	UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(request.getRemoteUser(), null);
    	SecurityContextHolder.getContext().setAuthentication(auth);
    	if(SecurityContextHolder.getContext().getAuthentication() != null){
    		
    		// query the db to fetch user registaton status and his business role
    		// if registered user take him to search index page
    		// if non-registered user take him into self-registration page
    		// If user is inactive he has to be in general error page with error message
    		// If user is registed but pending he has to be in OOPS message page
    		// If user is registered but rejected take him into self-registration page
    		
    		// This SSO.jsp has to be mapped to proper name in web.xml
    		
    	     //       Connection connection = null;
	          try {

	               Class.forName("com.mysql.jdbc.Driver"); 
	               connection = DriverManager.getConnection("jdbc:mysql://cgsyn-dev-mysql.ct5w2xljgsni.us-east-1.rds.amazonaws.com:3306/cur_relevance","svc_dsynrelevanc","Admin123");               
	               Log.Info("Using Postgress connection...");

	        } catch (Exception ee) {
	        	  Log.Info("Exception in getting data from batch table "  + ee.getMessage());
	        	  Log.Info("Exception" + ee.getMessage());
	        	  Log.Info("Exception in detail" + ee);
	               ee.printStackTrace();
	               Log.Info(ee.getMessage());
	               throw ee;
	        } 		

	        if (connection != null) {
	               //System.out.println("You made it, take control your database now!");
	               Log.Info("You made it, take control your database now!");
	        } else {
	               //System.out.println("Failed to make connection!");
	               Log.Info("Failed to make connection!");
	        }
	        Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            //ResultSet rs = stmt.executeQuery("select a.enabled,a.primaryrole,a.status,b.act_status, b.user_remark from users a, user_additional_details b where a.username = b.username and a.username ='"+userName+"';");
			
			ResultSet rs = stmt.executeQuery("select a.enabled,a.primaryrole,a.status from users a where a.username ='"+userName+"';");
            Log.Info("Result set object:" +rs);
            String primaryRole = null;
            String status = null;
            String enabled = null;
            String userStatus = null;
			String userRemarks = null;
			
              if(rs != null)
            {	
            	  rs.beforeFirst();  
            	  rs.last();  
            	  int size = rs.getRow();
            	  
                if(size != 0) 
               {	  
                  enabled = rs.getString("enabled");
            	  primaryRole = rs.getString("primaryrole");
            	  status = rs.getString("status");
            	  //userStatus = rs.getString("act_status");
            	  //userRemarks = rs.getString("user_remark");
                if (status.equalsIgnoreCase("Approved")) {
                	   try{
                		   Log.Info("Updating Last Login in User Additionals");
                     	   /* stmt.executeUpdate("update user_additional_details set last_login = now() where username = '"+userName+"';"); */
                     	  Log.Info("Last Login in User Additionals Updated");
                	   } catch(Exception exc) {
                		   Log.Info("ERROR during update last login !");
                		   System.out.println(exc.getMessage());
                	   }
                	   
                	   /* if(request.getParameter("rl") != null && !request.getParameter("rl").toString().isEmpty()) {
                		   	Log.Info("Getting Reading Reading List Info" + request.getParameter("rl").toString());
                			session.setAttribute("rl", request.getParameter("rl").toString());
                	   } */
					   
                	  session.setAttribute("user", request.getRemoteUser());
            	     Log.Info("user status is Approved");
            	 if(request.getParameter("app") != null && !request.getParameter("app").toString().isEmpty() && request.getParameter("app").toString().equalsIgnoreCase("admin")) {
            		 session.setAttribute("app", "ROLE_ADMIN");
            	 } else {
            		 session.setAttribute("app", "ROLE_CELGENE");
            	 }
            	
            	Log.Info("Logged in user is successfully authenticated..");				
           		response.sendRedirect(request.getContextPath()+ "/index");
              } 
            } else
           {
        	   Log.Info("user is non-registered user");
        	   response.sendRedirect("web/usernotfound.jsp");
           } 
         }
    		
    	} 
	}catch(Exception ee){
		response.sendRedirect(request.getContextPath()+ "/login?app=ROLE_CELGENE");
		PrismHandler.logMethodEntry("SSO Exception", ee.getMessage());
	}
	  finally{
				try{
					connection.close();
				}catch(Exception sqe){
					 Log.Info("Connection closed");
					 Log.Info("Exception" + sqe.getMessage());
				}
        }		
    	 PrismHandler.logMethodEntry("SSO Exit", "SSO Logic ended");
	%>	
</body>
</html>
