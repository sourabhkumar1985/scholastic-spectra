package com.relevance.gbt.service;

import com.relevance.prism.util.AppException;

public interface BatchGeneologyService {
	
	public String searchSolar(Object... obj) throws AppException;
	
	public Object fetchBatchGeneology(Object... obj);
	
}
