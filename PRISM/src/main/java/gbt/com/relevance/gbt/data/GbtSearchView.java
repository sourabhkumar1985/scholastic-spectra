package com.relevance.gbt.data;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *  GBT SearchView Object
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"maraSearchResults", "maktSearchResults", "afkoSearchResults", "afpoSearchResults",  "aufmSearchResults", "resbSearchResults", "batchWhereUsedList","batchStocks","batchMappingList","materialMappingList"})
public class GbtSearchView {
	
	/**
     * 
     */
	@JsonProperty("maraSearchResults")
	private List<Mara> maraSearchResults = new ArrayList<Mara>();

	/**
     * 
     */
	@JsonProperty("maktSearchResults")
	private List<Makt> maktSearchResults = new ArrayList<Makt>();
	/**
     * 
     */
	@JsonProperty("afkoSearchResults")
	private List<Afko> afkoSearchResults = new ArrayList<Afko>();

	/**
     * 
     */
	@JsonProperty("afpoSearchResults")
	private List<Afpo> afpoSearchResults = new ArrayList<Afpo>();

	/**
     * 
     */
	@JsonProperty("aufmSearchResults")
	private List<Aufm> aufmSearchResults = new ArrayList<Aufm>();

	/**
     * 
     */
	@JsonProperty("resbSearchResults")
	private List<Resb> resbSearchResults = new ArrayList<Resb>();
	
	@JsonProperty("batchWhereUsedList")
	private List<BatchWhereUsed> batchWhereUsedList = new ArrayList<BatchWhereUsed>();
	
	@JsonProperty("batchStocksList")
	private List<BatchStocks> batchStocksList = new ArrayList<BatchStocks>();

	@JsonProperty("materialMappingList")
	private List<MaterialMapping> materialMappingList = new ArrayList<MaterialMapping>();

	@JsonProperty("batchMappingList")
	private List<BatchMapping> batchMappingList = new ArrayList<BatchMapping>();
	
	public List<MaterialMapping> getMaterialMappingList() {
		return materialMappingList;
	}

	public void setMaterialMappingList(List<MaterialMapping> materialMappingList) {
		this.materialMappingList = materialMappingList;
	}

	public List<BatchMapping> getBatchMappingList() {
		return batchMappingList;
	}

	public void setBatchMappingList(List<BatchMapping> batchMappingList) {
		this.batchMappingList = batchMappingList;
	}

	public List<Mara> getMaraSearchResults() {
		return maraSearchResults;
	}

	public List<Makt> getMaktSearchResults() {
		return maktSearchResults;
	}

	public List<Afko> getAfkoSearchResults() {
		return afkoSearchResults;
	}

	public List<Afpo> getAfpoSearchResults() {
		return afpoSearchResults;
	}

	public List<Aufm> getAufmSearchResults() {
		return aufmSearchResults;
	}

	public List<Resb> getResbSearchResults() {
		return resbSearchResults;
	}
	
	public List<BatchWhereUsed> getBatchWhereUsedList() {
		return batchWhereUsedList;
	}

	public List<BatchStocks> getBatchStocksList() {
		return batchStocksList;
	}
	
	public void setMaraSearchResults(List<Mara> maraSearchResults) {
		this.maraSearchResults = maraSearchResults;
	}

	public void setMaktSearchResults(List<Makt> maktSearchResults) {
		this.maktSearchResults = maktSearchResults;
	}

	public void setAfkoSearchResults(List<Afko> afkoSearchResults) {
		this.afkoSearchResults = afkoSearchResults;
	}

	public void setAfpoSearchResults(List<Afpo> afpoSearchResults) {
		this.afpoSearchResults = afpoSearchResults;
	}

	public void setAufmSearchResults(List<Aufm> aufmSearchResults) {
		this.aufmSearchResults = aufmSearchResults;
	}

	public void setBatchWhereUsedList(List<BatchWhereUsed> batchWhereUsedList) {
		this.batchWhereUsedList = batchWhereUsedList;
	}
	
	public void setResbSearchResults(List<Resb> resbSearchResults) {
		this.resbSearchResults = resbSearchResults;
	}

	public void setBatchStocksList(List<BatchStocks> batchStocksList) {
		this.batchStocksList = batchStocksList;
	}
	
}
