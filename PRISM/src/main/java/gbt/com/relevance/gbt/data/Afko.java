package com.relevance.gbt.data;
/**
 * 
 * E2emf GBT Pojo  
 *
 */
public class Afko {

	private String id;
	
	private String workOrder;
	
	private String referenceNum;

	public String getId() {
		return id;
	}

	public String getWorkOrder() {
		return workOrder;
	}

	public String getReferenceNum() {
		return referenceNum;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setWorkOrder(String workOrder) {
		this.workOrder = workOrder;
	}

	public void setReferenceNum(String referenceNum) {
		this.referenceNum = referenceNum;
	}

}
