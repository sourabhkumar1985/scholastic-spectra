package com.relevance.gbt.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.XML;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.relevance.gbt.data.GbtSearchView;
import com.relevance.gbt.service.GbtPostGresService;
import com.relevance.gbt.service.GbtService;
import com.relevance.gbt.util.GbtConstants;
import com.relevance.gbt.util.GbtLog;
import com.relevance.gbt.util.GbtSolrUtil;
import com.relevance.prism.data.SearchParam;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.DataObfuscator;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.Log;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.representation.Form;

/**
 *  E2emf GlobalBatchResource
 * 
 */
@Path("/geneology")
public class GlobalBatchResource {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getSearchParams")
	public String getSearchParams(@FormParam("key") String key,
			@FormParam("facet") String facet, @FormParam("start") String start,
			@FormParam("rows") String rows, @FormParam("source") String source,@FormParam("searchInDisplaycolumns") String searchInDisplaycolumns)
			throws AppException {
		String searchResponse = null;
		GbtService gbtService = new GbtService();
		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		try {
			GbtLog.Info("Recieved search Params : " + key);
			SearchParam searchParam = new SearchParam();
			searchParam.setKey(key);
			//searchParam.setKey(DataObfuscator.deObfuscateToken(key));
			searchParam.setFacet(facet);
			searchParam.setStart(start);
			searchParam.setRows(rows);
			searchParam.setSource(source);
			searchResponse = (String) gbtService.searchSolar(searchParam,
					"searchService",searchInDisplaycolumns);
			//searchResponse = DataObfuscator.obfuscate(searchResponse);
			endTime = System.currentTimeMillis();

		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			GbtLog.Error("Exception in getSearchParams for GBT Search " + e);
			GbtLog.Error(e.getMessage());
		}

		GbtLog.Info("Returning Solr Search Response to presentation,  total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");

		return searchResponse;

	}

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getBatchJSON")
	public String getBatchJSON(@FormParam("batch") String batch) {

		// Connection conn = getPostGresConnection();
		// System.out.println("conn:" +conn);

		String response = null;
		GbtPostGresService gbtService = new GbtPostGresService();
		/*long startTime = System.currentTimeMillis();
		long endTime = 0L;*/

		try {
			GbtLog.Info("Recieved batch number : " + batch);
			response = gbtService.getBatchJSON(batch);
			//endTime = System.currentTimeMillis();

		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			GbtLog.Error("Exception in getting data from batch table " + e);
			System.out.println("Exception" + e.getMessage());
			GbtLog.Error(e.getMessage());
		}

		// GbtLog.Info("Returning Solr Search Response to presentation,  total Time taken is "
		// + E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
		// + " seconds");
		//System.out.println("response returned" + response);

		return response;

	}

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getReletedBatch")
	public String getReletedBatch(@FormParam("key") String key,
			@FormParam("type") String type) {

		// Connection conn = getPostGresConnection();
		// System.out.println("conn:" +conn);

		String response = null;
		GbtPostGresService gbtService = new GbtPostGresService();
		/*long startTime = System.currentTimeMillis();
		long endTime = 0L;*/

		try {
			GbtLog.Info("Recieved batch number : " + key);
			response = gbtService.getRelatedBatchs(key, type);
			//endTime = System.currentTimeMillis();

		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			GbtLog.Error("Exception in getting data from batch table " + e);
			System.out.println("Exception" + e.getMessage());
			GbtLog.Error(e.getMessage());
		}

		// GbtLog.Info("Returning Solr Search Response to presentation,  total Time taken is "
		// + E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
		// + " seconds");
		//System.out.println("response returned" + response);

		return response;

	}

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getBatchWhereUsedList")
	public String getBatchWhereUsedList(
			@FormParam("batchsString") String batchsString, @FormParam("system") String system) {

		// Connection conn = getPostGresConnection();
		// System.out.println("conn:" +conn);

		String gbtSearchViewJson = null;
		GbtSearchView gbtSearchView = null;
		GbtPostGresService gbtService = new GbtPostGresService();
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		/*long startTime = System.currentTimeMillis();
		long endTime = 0L;*/

		try {
			GbtLog.Info("Recieved batch numbers : " + batchsString);
			gbtSearchView = gbtService.getBatchWhereUsedList(batchsString, system);
			//endTime = System.currentTimeMillis();
			gbtSearchViewJson = gson.toJson(gbtSearchView);
			gbtSearchViewJson = DataObfuscator.obfuscate(gbtSearchViewJson);
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			GbtLog.Error("Exception in getting data from batch table " + e);
			System.out.println("Exception" + e.getMessage());
			GbtLog.Error(e.getMessage());
		}

		// GbtLog.Info("Returning Solr Search Response to presentation,  total Time taken is "
		// + E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
		// + " seconds");
		//System.out.println("response returned" + gbtSearchViewJson);

		return gbtSearchViewJson;

	}

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getBatchStocksList")
	public String getBatchStocksList(
			@FormParam("batchsString") String batchsString, @FormParam("system") String system) {

		// Connection conn = getPostGresConnection();
		// System.out.println("conn:" +conn);

		String gbtSearchViewJson = null;
		GbtSearchView gbtSearchView = null;
		GbtPostGresService gbtService = new GbtPostGresService();
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		/*long startTime = System.currentTimeMillis();
		long endTime = 0L;*/

		try {
			GbtLog.Info("Recieved batch numbers : " + batchsString);
			gbtSearchView = gbtService.getBatchStocksList(batchsString, system);
			//endTime = System.currentTimeMillis();
			gbtSearchViewJson = gson.toJson(gbtSearchView);
			gbtSearchViewJson = DataObfuscator.obfuscate(gbtSearchViewJson);	
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			GbtLog.Error("Exception in getting data from batch table " + e);
			System.out.println("Exception" + e.getMessage());
			GbtLog.Error(e.getMessage());
		}

		// GbtLog.Info("Returning Solr Search Response to presentation,  total Time taken is "
		// + E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
		// + " seconds");
		//System.out.println("response returned" + gbtSearchViewJson);

		return gbtSearchViewJson;

	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/batchgenealogy")
	public String batchGenealogy(@FormParam("key") String materialbatch,
			@FormParam("type") String genealogy, @FormParam("source") String source) {
		StringBuilder output = new StringBuilder();
		//String issueKey = null;
		String resultOutput = null;
		try {

			Client client = Client.create();
			GbtSolrUtil solrUtil = new GbtSolrUtil();
			String serviceURL = solrUtil
					.getAppProperty(GbtConstants.rocksDBServiceURL);
			
			
			////////
			if (genealogy == null || "".equalsIgnoreCase(genealogy))
			{
				WebResource webResource = client.resource(serviceURL);
				Form f = new Form();
				f.add("materialbatch", materialbatch);
				f.add("genealogy", "forward");
				f.add("source", source);
				// String
				// input="{\"fields\":{\"project\":{\"key\":\"JS\"},\"summary\":\"Test jira issue with attachment\",\"description\":\"This is a test CR attachment\", \"reporter\": {\"name\": \"Nagaraj\"},\"issuetype\":{\"name\":\"Bug\"},\"fixVersions\":[{\"id\":\"11000\"}],\"components\":[{\"id\":\"10600\"}],\"customfield_10001\":[\"SAP\"]}}";
				ClientResponse response = webResource.type(
						"application/x-www-form-urlencoded").post(
						ClientResponse.class, f);
				
				output.append("{\"forward\":");
				output.append(response.getEntity(String.class));
				output.append(",");
				
				webResource = client.resource(serviceURL);
				f = new Form();
				f.add("materialbatch", materialbatch);
				f.add("genealogy", "backward");
				f.add("source", source);
				// String
				// input="{\"fields\":{\"project\":{\"key\":\"JS\"},\"summary\":\"Test jira issue with attachment\",\"description\":\"This is a test CR attachment\", \"reporter\": {\"name\": \"Nagaraj\"},\"issuetype\":{\"name\":\"Bug\"},\"fixVersions\":[{\"id\":\"11000\"}],\"components\":[{\"id\":\"10600\"}],\"customfield_10001\":[\"SAP\"]}}";
				response = webResource.type(
						"application/x-www-form-urlencoded").post(
						ClientResponse.class, f);
				
				output.append("\"backward\":");
				output.append(response.getEntity(String.class));
				output.append("}");
				resultOutput = DataObfuscator.obfuscate(output.toString());				
			}
			else
			{
				WebResource webResource = client.resource(serviceURL);
				Form f = new Form();
				f.add("materialbatch", materialbatch);
				f.add("genealogy", genealogy);
				f.add("source", source);
				// String
				// input="{\"fields\":{\"project\":{\"key\":\"JS\"},\"summary\":\"Test jira issue with attachment\",\"description\":\"This is a test CR attachment\", \"reporter\": {\"name\": \"Nagaraj\"},\"issuetype\":{\"name\":\"Bug\"},\"fixVersions\":[{\"id\":\"11000\"}],\"components\":[{\"id\":\"10600\"}],\"customfield_10001\":[\"SAP\"]}}";
				ClientResponse response = webResource.type(
						"application/x-www-form-urlencoded").post(
						ClientResponse.class, f);
	
				output.append("{\"");
				output.append(genealogy);
				output.append("\":");
				output.append(response.getEntity(String.class));
				output.append("}");
				resultOutput = DataObfuscator.obfuscate(output.toString());
			}
			////////////
			

		} catch (Exception e) {
			Log.Error(e);
		}
		//return output.toString();
		return resultOutput;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("/batchgenealogy")
	public String getBatchGenealogy(@QueryParam("key") String materialbatch,
			@QueryParam("type") String genealogy,
			@QueryParam("returnformat") String format,@QueryParam("source") String source) {
		String output = null;
		//String issueKey = null;
		try {

			Client client = Client.create();
			GbtSolrUtil solrUtil = new GbtSolrUtil();
			String serviceURL = solrUtil
					.getAppProperty(GbtConstants.rocksDBServiceURL);
			WebResource webResource = client.resource(serviceURL);
			Form f = new Form();
			f.add("materialbatch", materialbatch);
			f.add("genealogy", genealogy);
			f.add("source", source);
			// String
			// input="{\"fields\":{\"project\":{\"key\":\"JS\"},\"summary\":\"Test jira issue with attachment\",\"description\":\"This is a test CR attachment\", \"reporter\": {\"name\": \"Nagaraj\"},\"issuetype\":{\"name\":\"Bug\"},\"fixVersions\":[{\"id\":\"11000\"}],\"components\":[{\"id\":\"10600\"}],\"customfield_10001\":[\"SAP\"]}}";
			ClientResponse response = webResource.type(
					"application/x-www-form-urlencoded").post(
					ClientResponse.class, f);

			output = response.getEntity(String.class);
			if (format != null && format.equalsIgnoreCase("xml")) {
				JSONArray jsonstr = new JSONArray("[" + output + "]");
				String xml = XML.toString(jsonstr);

				StringBuilder builder = new StringBuilder();
				builder.append("<?xml version=\"1.0\"?>").append("<Batch>")
						.append(xml).append("</Batch>");
				output = builder.toString();
			}
		} catch (Exception e) {
			Log.Error(e);
		}
		return output;
	}
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getBatchMapping")
	public String getBatchMapping(@FormParam("batchsString") String batchsString) {

		// Connection conn = getPostGresConnection();
		// System.out.println("conn:" +conn);
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		String response = null;
		GbtSearchView gbtSearchView = null;
		GbtPostGresService gbtService = new GbtPostGresService();
		/*long startTime = System.currentTimeMillis();
		long endTime = 0L;*/

		try {
			gbtSearchView = gbtService.getBatchMapping(batchsString);
			response = gson.toJson(gbtSearchView);
			response = DataObfuscator.obfuscate(response);
			//endTime = System.currentTimeMillis();

		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			GbtLog.Error("Exception in getting data from batch table " + e);
			System.out.println("Exception" + e.getMessage());
			GbtLog.Error(e.getMessage());
		}

		// GbtLog.Info("Returning Solr Search Response to presentation,  total Time taken is "
		// + E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
		// + " seconds");
		//System.out.println("response returned" + response);

		return response;

	}
	
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getMaterialMapping")
	public String getMaterialMapping(@FormParam("key") String key,
			@FormParam("type") String type) {
		GbtPostGresService gbtService = new GbtPostGresService();
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		String response = null;
		/*long startTime = System.currentTimeMillis();
		long endTime = 0L;*/
		GbtSearchView gbtSearchView = null;
		
		try {
			GbtLog.Info("Recieved batch number : " + key);
			gbtSearchView = gbtService.getMaterialMapping();
			response = gson.toJson(gbtSearchView);
			response = DataObfuscator.obfuscate(response);
			//endTime = System.currentTimeMillis();

		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			GbtLog.Error("Exception in getting data from batch table " + e);
			System.out.println("Exception" + e.getMessage());
			GbtLog.Error(e.getMessage());
		}

		// GbtLog.Info("Returning Solr Search Response to presentation,  total Time taken is "
		// + E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
		// + " seconds");
		//System.out.println("response returned" + response);

		return response;

	}
}
