package com.rl.cms.refresh;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.sql.Date;

public class Attributes {

	private boolean active;
	private boolean can_email;
	private String name;
	private String display_name;
	private String filename;
	private double size;
	private String type;
	private String description;
	private String notes;
	private String created_at;
	private String updated_at;
	private String deleted_at;
	private String created_at_as_date;
	private String updated_at_as_date;
	private String deleted_at_as_date;
	private String[] folders;
	private String download_link;
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isCan_email() {
		return can_email;
	}
	public void setCan_email(boolean can_email) {
		this.can_email = can_email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
	public String getDisplay_name() {
		return display_name;
	}
	public void setDisplay_name(String display_name) {
		this.display_name = display_name;
	}	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}	
	public double getSize() {
		return size;
	}
	public void setSize(double size) {
		this.size = size;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}	
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
		if(created_at != null){
			this.created_at_as_date = convertTime(Long.parseLong(created_at));
		}
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
		if(updated_at != null){
			this.updated_at_as_date = convertTime(Long.parseLong(updated_at));
		}
	}
	public String getDeleted_at() {
		return deleted_at;
	}
	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
		if(deleted_at != null){
			this.deleted_at_as_date = convertTime(Long.parseLong(deleted_at));
		}
	}	
	public String getCreated_at_as_date() {
		return created_at_as_date;
	}
	public void setCreated_at_as_date(String created_at_as_date) {
		this.created_at_as_date = created_at_as_date;
	}
	public String getUpdated_at_as_date() {
		return updated_at_as_date;
	}
	public void setUpdated_at_as_date(String updated_at_as_date) {
		this.updated_at_as_date = updated_at_as_date;
	}
	public String getDeleted_at_as_date() {
		return deleted_at_as_date;
	}
	public void setDeleted_at_as_date(String deleted_at_as_date) {
		this.deleted_at_as_date = deleted_at_as_date;
	}
	public String[] getFolders() {
		return folders;
	}
	public void setFolders(String[] folders) {
		this.folders = folders;
	}
	public String getDownload_link() {
		return download_link;
	}
	public void setDownload_link(String download_link) {
		this.download_link = download_link;
	}	
	
	public String convertTime(long time){
	    Date date = new Date(time);
	    Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
	    return format.format(date);
	}
}
