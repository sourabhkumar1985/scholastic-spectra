package com.relevance.prism.data;

public class SecondaryRole {
	private int sectionId;
	private String sectionName;
	private String facetName;
	private String groupIds;
	private String groupNames;
	private String userName;
	private String email;
	private String sectionDelimeter;
	public int getSectionId() {
		return sectionId;
	}
	public void setSectionId(int sectionId) {
		this.sectionId = sectionId;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public String getFacetName() {
		return facetName;
	}
	public void setFacetName(String facetName) {
		this.facetName = facetName;
	}
	public String getGroupIds() {
		return groupIds;
	}
	public void setGroupIds(String groupIds) {
		this.groupIds = groupIds;
	}
	public String getGroupNames() {
		return groupNames;
	}
	public void setGroupNames(String groupNames) {
		this.groupNames = groupNames;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSectionDelimeter() {
		return sectionDelimeter;
	}
	public void setSectionDelimeter(String sectionDelimeter) {
		this.sectionDelimeter = sectionDelimeter;
	}
}
