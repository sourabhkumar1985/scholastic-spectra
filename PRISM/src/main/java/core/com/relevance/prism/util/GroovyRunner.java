package com.relevance.prism.util;

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;

public class GroovyRunner {
	private final static Logger logger = Logger.getLogger(GroovyRunner.class);
	public static void main(final String[] args) throws IllegalAccessException, InstantiationException, IOException {
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		try{
		// Create GroovyClassLoader.
		final GroovyClassLoader classLoader = new GroovyClassLoader();
		// Create a String with Groovy code.
		//final StringBuilder groovyScript = new StringBuilder();
		//groovyScript.append("class Sample {");
		//groovyScript.append("	String sayIt(name) { if(name == 'Manager1'){\"Groovy says: Cool $name!\"}else{\"Groovy says: Hot $name!\"} }");
		//groovyScript.append("}");
		connection = getPostGresDBConnection();
		stmt = connection.createStatement();
		rs = stmt.executeQuery("select id, rule_name, rule_text, rule_order from rules where enabled = 'true' order by rule_order");
		// Load string as Groovy script class.
		while(rs.next()){
			int id = rs.getInt("id");
			String rule_name = rs.getString("rule_name");
			String rule_text = rs.getString("rule_text");
			int rule_order = rs.getInt("rule_order");
			//rule to say hello
			if(rule_name.equalsIgnoreCase("sayHello")){
				Class groovy = classLoader.parseClass(rule_text);
				GroovyObject groovyObj = (GroovyObject) groovy.newInstance();
				Object output = groovyObj.invokeMethod("sayIt", new Object[] { "Manager1" });
				//System.out.println("Groovy says: Cool mrhaki!".equals(output));
				System.out.println(output);
				output = groovyObj.invokeMethod("sayIt", new Object[] { "Manager2" });
				System.out.println(output);
			}
			//rule to check Age
			if(rule_name.equalsIgnoreCase("checkAge")){
				Class groovy = classLoader.parseClass(rule_text);
				GroovyObject groovyObj = (GroovyObject) groovy.newInstance();
				Object output = groovyObj.invokeMethod("checkAge", new Object[] { new Integer(17) });
				System.out.println(output);
				output = groovyObj.invokeMethod("checkAge", new Object[] { new Integer(20) });
				System.out.println(output);
			}
			//rule to get master data
			if(rule_name.equalsIgnoreCase("getMasterData")){
				Class groovy = classLoader.parseClass(rule_text);
				GroovyObject groovyObj = (GroovyObject) groovy.newInstance();
				Object output = groovyObj.invokeMethod("getMasterdata", new Object[] { "oracle-item-master-data" ,  "catalog-data", "spreadsheet-data", "label-data"});
				System.out.println(output);
				output = groovyObj.invokeMethod("getMasterdata", new Object[] { "oracle-item-master-data" ,  "catalog-data", "spreadsheet-data", null});
				System.out.println(output);
				output = groovyObj.invokeMethod("getMasterdata", new Object[] { "oracle-item-master-data" , "catalog-data", null, null});
				System.out.println(output);
				output = groovyObj.invokeMethod("getMasterdata", new Object[] { "oracle-item-master-data", null, null, null });
				System.out.println(output);
				output = groovyObj.invokeMethod("getMasterdata", new Object[] { null, null, null, null });
				System.out.println(output);
			}
		}		
		//Load Groovy script file.
		//groovy = classLoader.parseClass(new File("SampleScript.groovy"));
		//groovyObj = (GroovyObject) groovy.newInstance();
		//output = groovyObj.invokeMethod("scriptSays", new Object[] { "mrhaki", new Integer(2) });
		//assert "Hello mrhaki, from Groovy. Hello mrhaki, from Groovy".equals(output);
		}catch (Exception je) {
            logger.error("Exception Occurred : " + je);
        }
		finally{
			try{
				connection.close();
			}catch (SQLException je) {
	            logger.error("Exception Occurred : " + je);
	        }			
		}
	}
	public static Object getResult(String ruleText, String methodName, Object [] parameters){
		Object output = null;
		try{
		// Create GroovyClassLoader.
		final GroovyClassLoader classLoader = new GroovyClassLoader();
		Class groovy = classLoader.parseClass(ruleText);
		GroovyObject groovyObj = (GroovyObject) groovy.newInstance();
		output = groovyObj.invokeMethod(methodName, parameters);
		//logger.info("output : " + output);
		return output;
		}catch (Exception je) {
            logger.error("Exception Occurred : " + je);
        }
		finally{
			try{
				//
			}catch (Exception je) {
	            logger.error("Exception Occurred : " + je);
	        }			
		}
		return output;
	}
	public static Connection getPostGresDBConnection() throws Exception {
		Connection connection = null;
		try {
			Class.forName("org.postgresql.Driver");
			String propertiesFile = "spectra.properties";
			Properties prop = new Properties();
			prop.load(GroovyRunner.class.getClassLoader().getResourceAsStream(propertiesFile));
			System.out.println(prop.getProperty("dburl").trim() + " : " + prop.getProperty("userid").trim() + " : "+ prop.getProperty("password").trim());
			connection = DriverManager.getConnection(prop.getProperty("dburl").trim(), prop.getProperty("userid").trim(), prop.getProperty("password").trim());
		} catch (Exception ex) {
			logger.error("Exception Occurred : " + ex);
		}
		return connection;
	}
}
