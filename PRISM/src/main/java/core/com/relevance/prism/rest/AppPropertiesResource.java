package com.relevance.prism.rest;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.data.AppProperties;
import com.relevance.prism.data.Category;
import com.relevance.prism.service.AppPropertiesService;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.FormData;
import com.relevance.prism.util.Log;

@Path("/app_properties")
public class AppPropertiesResource {
	
	@Context
	ServletContext context;

	public AppPropertiesResource(@Context ServletContext value) {
		this.context = value;
		//System.out.println("Conext:" + this.context);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/refreshproperties")
	public String getDataObfuscationValue(){
		String json = null;
		try{
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			AppProperties appProperties = appPropertiesService.getProperties();
			E2emfAppUtil.reinitialize(appProperties);
			Map<String, FormData> formDataMap = appPropertiesService.getFormDataMap();
			E2emfAppUtil.setFormDataMap(formDataMap);
			E2emfAppUtil.setWordMap(appPropertiesService.getObfuscationWordMap(false));
			E2emfAppUtil.setRevWordMap(appPropertiesService.getObfuscationWordMap(true));
			E2emfAppUtil.setCharMap(appPropertiesService.getObfuscationCharMap(false));
			E2emfAppUtil.setRevCharMap(appPropertiesService.getObfuscationCharMap(true));
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}catch(Exception ex){
			Log.Error("Exception while retrieving the Data Obfuscation Config Value " + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		return json;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateproperties")
	public String updateProperties(@FormParam("enabled") String enabled)
	{
		return null;
		/*	 
		String responseMessage = null;
		try{
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			responseMessage = appPropertiesService.updateDataObfuscation(enabled);			
			String dataObfuscationValue = enabled;
			return responseMessage;
			
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}catch(Exception ex){
			Log.Error("Exception while updating the property values " + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
		*/
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getformdatalist")
	public String getFormDataList(){
		String json = null;
		try{
			Gson gson = new Gson();
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			json = gson.toJson(appPropertiesService.getFormDataMap().values());
			//json = gson.toJson(E2emfAppUtil.getFormDataMap().values());
		}
		catch(AppException appe){
			Log.Error("Exception while retrieving the Form Data Map " + appe.getMessage());
			Log.Error(appe);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		catch(Exception ex){
			Log.Error("Exception while retrieving the Form Data Map " + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		return json;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getpropertylist")
	public String getAppPropertiesList(){
		String json = null;
		try{
			Gson gson = new Gson();
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			json = gson.toJson(appPropertiesService.getProperties().getProp().values());
			//json = gson.toJson(E2emfAppUtil.getAppProperties().getProp().values());
		}
		catch(AppException appe){
			Log.Error("Exception while retrieving the App Properties list " + appe.getMessage());
			Log.Error(appe);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		catch(Exception ex){
			Log.Error("Exception while retrieving the App Properties list " + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		return json;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getobfwordlist")
	public String getWordMapList(){
		String json = null;
		try{
			Gson gson = new Gson();
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			json = gson.toJson(appPropertiesService.getObfuscationWordMap(false).values());
			//json = gson.toJson(E2emfAppUtil.getWordMap().values());
		}
		catch(AppException appe){
			Log.Error("Exception while retrieving the Word Map " + appe.getMessage());
			Log.Error(appe);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		catch(Exception ex){
			Log.Error("Exception while retrieving the Word Map " + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		return json;
	}
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getobfcharlist")
	public String getCharMapList(){
		String json = null;
		try{
			Gson gson = new Gson();
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			json = gson.toJson(appPropertiesService.getObfuscationCharMap(false).values());
			//json = gson.toJson(E2emfAppUtil.getCharMap().values());
		}
		catch(AppException appe){
			Log.Error("Exception while retrieving the Character Map " + appe.getMessage());
			Log.Error(appe);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		catch(Exception ex){
			Log.Error("Exception while retrieving the Character Map " + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		return json;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addformdata")
	public String addFormData(@FormParam("propertyname") String propertyname, 
						@FormParam("obfstatus") int obfstatus, 
						@FormParam("deobfstatus") int deobfstatus,
						@FormParam("remarks") String remarks,
						@FormParam("obftype") int obftype 
						){
		String responseMessage = null;
		try{
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			responseMessage = appPropertiesService.insertFormData(propertyname,obfstatus,deobfstatus,remarks,obftype);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while adding the action to the DB." + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addobfwords")
	public String addObfWords(@FormParam("fromword") String fromword, 
						    @FormParam("toword") String toword						
						){
		String responseMessage = null;
		try{
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			responseMessage = appPropertiesService.insertObfWords(fromword,toword);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while adding the action to the DB." + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addappproperties")
	public String addAppProperties(@FormParam("name") String name, 
						           @FormParam("value") String value,
						           @FormParam("remarks") String remarks,
						           @FormParam("type") String type
						){
		String responseMessage = null;
		try{
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			responseMessage = appPropertiesService.insertAppProperties(name,value,remarks,type);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while adding the action to the DB." + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateformdata")
	public String updateFormData(@FormParam("propertyname") String propertyname, 
						@FormParam("obfstatus") int obfstatus, 
						@FormParam("deobfstatus") int deobfstatus,
						@FormParam("remarks") String remarks,
						@FormParam("obftype") int obftype 
						){
		String responseMessage = null;
		try{
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			responseMessage = appPropertiesService.updateFormData(propertyname,obfstatus,deobfstatus,remarks,obftype);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while Updating the Form Data in the DB." + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateobfwords")
	public String updateObfWords(@FormParam("fromword") String fromword, 
						    @FormParam("toword") String toword						
						){
		String responseMessage = null;
		try{
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			responseMessage = appPropertiesService.updateObfWords(fromword,toword);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while Updating the Obfuscation Words in the DB." + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateappproperties")
	public String updateAppProperties(@FormParam("name") String name, 
						           @FormParam("value") String value,
						           @FormParam("remarks") String remarks,
						           @FormParam("type") String type
						){
		String responseMessage = null;
		try{
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			responseMessage = appPropertiesService.updateAppProperties(name,value,remarks,type);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while updating the App Properties in the DB." + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteformdata")
	public String deleteFormData(@FormParam("propertyname") String propertyname
						){
		String responseMessage = null;
		try{
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			responseMessage = appPropertiesService.deleteFormData(propertyname);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while deleting the Form Data in the DB." + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteobfwords")
	public String deleteObfWords(@FormParam("fromword") String fromword				
						){
		String responseMessage = null;
		try{
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			responseMessage = appPropertiesService.deleteObfWords(fromword);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while adding the Obf Word from the DB." + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteappproperties")
	public String deleteAppProperties(@FormParam("name") String name
						){
		String responseMessage = null;
		try{
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			responseMessage = appPropertiesService.deleteAppProperties(name);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while deleting the App Properties from the DB." + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getcategorieslist")
	public String getCategoriesList(){
		String json = null;
		try{
			Gson gson = new Gson();
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			List<Category> categoriesList = appPropertiesService.getCategoriesList();
			json = gson.toJson(categoriesList);			
		}
		catch(AppException appe){
			Log.Error("Exception while retrieving the Categories List From DB " + appe.getMessage());
			Log.Error(appe);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		catch(Exception ex){
			Log.Error("Exception while retrieving the Form Data Map " + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		return json;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addcategory")
	public String addCategory(@FormParam("parent") String parent, 
			@FormParam("parentId") String parentId, 
			@FormParam("category") String category, 
			@FormParam("keywords") String keywords
						){
		String responseMessage = null;
		try{
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			responseMessage = appPropertiesService.insertCategory(parent, parentId, category, keywords);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while adding the Category to the DB." + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updatecategory")
	public String updateCategory(@FormParam("parent") String parent,
						@FormParam("parentId") String parentId,
						@FormParam("categoryId") String categoryId, 
						@FormParam("category") String category, 
						@FormParam("keywords") String keywords
						){
		String responseMessage = null;
		try{
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			responseMessage = appPropertiesService.updateCategory(parent, parentId, categoryId, category, keywords);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while Updating the Category in the DB." + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deletecategory")
	public String deleteCategory(@FormParam("categoryId") String categoryId
						){
		String responseMessage = null;
		try{
			AppPropertiesService appPropertiesService = new AppPropertiesService();
			responseMessage = appPropertiesService.deleteCategory(categoryId);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while deleting the Category from the DB." + ex.getMessage());
			Log.Error(ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
}
