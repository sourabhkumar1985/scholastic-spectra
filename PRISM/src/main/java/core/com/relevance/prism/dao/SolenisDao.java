package com.relevance.prism.dao;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

import com.google.gson.Gson;
import com.relevance.prism.data.ChartProperty;
import com.relevance.prism.data.SolenisSimulation;
import com.relevance.prism.util.PrismHandler;

public class SolenisDao extends BaseDao {
	public Logger _logger = Logger.getLogger(this.getClassName());
	public String processcpusimulation(String fileName, String type) {
		String response = null;
		List<SolenisSimulation> simulationInput = new ArrayList<>();
		List<String> values = new ArrayList<>();
		List<String> resell_values = new ArrayList<>();
		Map<String, Double> currencyList = null;		
		// Map<String, Map<String, Map<String,Integer>>> mapObj = new
		// HashMap<>();
		Gson gson = new Gson();
		StringBuilder query = new StringBuilder();
		StringBuilder resell_query = new StringBuilder();
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;

		resell_query.append("update sol_resell as m set inflation = case when c.current_cpu = 0 then 0 else ((c.current_cpu - cpu_base)) end from (values ");
		//query.append("update sol_simulation_output as m set total_inflation = ((c.current_cpu - cpu_past)*c.volume) from (values "); comp_plant = c.header_plant,
		//query.append("update sol_simulation_sales_cpu as m set component_plant = c.header_plant, cpu_future = c.current_cpu,inflation = case when header_sales_qty = 0 then 1 else ((c.current_cpu - cpu_past)*c.volume*distributed_proportion)/header_sales_qty end from (values ");
		query.append("update sol_simulation_sales_cpu as m set inflation = ((c.current_cpu - cpu_base)*total_comp_qty*distributed_proportion) from (values ");
		try {
			FileInputStream fis = new FileInputStream(fileName);
			// Create Workbook instance for xlsx/xls file input stream
			currencyList = getCurrency();
			Workbook workbook = null;
			if (fileName.toLowerCase().endsWith("xlsx")) {
				workbook = new XSSFWorkbook(fis);
			} else if (fileName.toLowerCase().endsWith("xls")) {
				workbook = new HSSFWorkbook(fis);
			}
			int numberOfSheets = workbook.getNumberOfSheets();
			for (int i = 0; i < numberOfSheets; i++) {
				// Get the nth sheet from the workbook
				Sheet sheet = workbook.getSheetAt(i);
				// every sheet has rows, iterate over them
				Iterator<Row> rowIterator = sheet.iterator();
				int rowcount = 0;
				String[] columns = null;
				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();
					// LinkedHashMap<String, String> jsonOrderedMap = new
					// LinkedHashMap<String, String>();
					String material = "";
					String plant = "";
					String currency = "";
					double currentCpu = 0;
					double volume = 0;
					if (rowcount == 0) {
						columns = new String[row.getLastCellNum()];
						for (int j = 0; j < row.getLastCellNum(); j++) {
							Cell cell = row
									.getCell(j, Row.CREATE_NULL_AS_BLANK);
							columns[j] = cell.getStringCellValue().trim();
						}
					} else {
						// jsonOrderedMap = new LinkedHashMap<String, String>();
						for (int cn = 0; cn < row.getLastCellNum(); cn++) {
							Cell cell = row.getCell(cn,
									Row.CREATE_NULL_AS_BLANK);
							if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
								if (columns[cn].equalsIgnoreCase("Material")) {
									material = String.valueOf((int) cell.getNumericCellValue());
								} else if (columns[cn].equalsIgnoreCase("plant")) {
									plant = String.valueOf((int) cell.getNumericCellValue());
								} else if (columns[cn].equalsIgnoreCase("CPU")) {
									currentCpu = cell.getNumericCellValue();
								} else if (columns[cn].equalsIgnoreCase("Volume")) {
									volume = cell.getNumericCellValue();
								} else if (columns[cn].equalsIgnoreCase("Currency")) {
									currency = String.valueOf((int) cell.getNumericCellValue());
								}
							} else if(cell.getCellType() == Cell.CELL_TYPE_STRING){
								if (columns[cn].equalsIgnoreCase("Material")) {
									material = cell.getStringCellValue();
								} else if (columns[cn]
										.equalsIgnoreCase("plant")) {
									plant = cell.getStringCellValue();
								} else if (columns[cn].equalsIgnoreCase("CPU")) {
									currentCpu = Integer.parseInt(cell.getStringCellValue());
								} else if (columns[cn]
										.equalsIgnoreCase("Volume")) {
									volume = Integer.parseInt(cell
											.getStringCellValue());
								} else if (columns[cn]
										.equalsIgnoreCase("Currency")) {
									currency = cell.getStringCellValue();
								}
							}
						}
						if (currencyList.containsKey(currency)) {
							currentCpu = currentCpu* currencyList.get(currency);
						}
						SolenisSimulation simulation = new SolenisSimulation();
						simulation.setMaterial(material);
						simulation.setPlant(plant);
						simulationInput.add(simulation);
						if((material == null || "".equalsIgnoreCase(material.trim())) || (plant == null || "".equalsIgnoreCase(plant.trim()))
								|| (currentCpu == 0)){
							_logger.info("Invalid input Material:" + material+ " Plant:" + plant + " CPU:" + currentCpu);
						}else{
							values.add("('" + material + "','" + plant + "',"+ currentCpu + "," + volume + ")");
						}
						if((material != null && !"".equalsIgnoreCase(material.trim())) && (plant != null && !"".equalsIgnoreCase(plant.trim()))){
							resell_values.add("('" + material + "','" + plant + "',"+ currentCpu + ")");
						}
					}
					rowcount++;
				}
			}
			con = dbUtil.getPostGresConnection("SolenisPostGres");
			query.append(StringUtils.join(values, ","));
			resell_query.append(StringUtils.join(resell_values, ","));
			query.append(") as c(component,comp_plant,current_cpu,volume) where c.component = m.component and c.comp_plant = m.comp_plant");
			resell_query.append(") as c(matl_num,plant,current_cpu) where c.matl_num = m.matl_num and c.plant = m.plant");
			if (con != null) {
				int noOfRows = 0;;
				stmt = con.createStatement();
				stmt.executeUpdate("update sol_simulation_sales_cpu set inflation = null;update sol_resell set inflation = null;");
				if(values.size() > 0){
					noOfRows = stmt.executeUpdate(query.toString());
				}
				if(resell_values.size()>0){
					stmt.executeUpdate(resell_query.toString());
				}
				_logger.info("Total "+noOfRows+" rows affected.");
			}
			response = gson.toJson(simulationInput);
			fis.close();
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	public Map<String, Double> getCurrency() {
		Connection con = null;
		Statement stmt = null;
		Map<String, Double> currencyList = new HashMap<>();
		ResultSet rs = null;
		try {
			con = dbUtil.getPostGresConnection("SolenisPostGres");
			stmt = con.createStatement();
			rs = stmt.executeQuery("select currency_from,exch_rate from sol_exch_rates_solenis");
			while (rs.next()) {
				currencyList.put(rs.getString("currency_from"),rs.getDouble("exch_rate"));
			}
			rs.close();
		} catch (Exception ex) {
			// currencyList = PrismHandler.handleException(ex);
		} finally {
			// PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return currencyList;
	}
}
