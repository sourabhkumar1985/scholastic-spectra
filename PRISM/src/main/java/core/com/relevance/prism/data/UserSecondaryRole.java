package com.relevance.prism.data;

public class UserSecondaryRole {

	private int userSecondaryRoleId;
	private String userName;
	private String secondaryRole;
	private String dataSource;
	
	public int getUserSecondaryRoleId() {
		return userSecondaryRoleId;
	}
	public void setUserSecondaryRoleId(int userSecondaryRoleId) {
		this.userSecondaryRoleId = userSecondaryRoleId;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getSecondaryRole() {
		return secondaryRole;
	}
	public void setSecondaryRole(String secondaryRole) {
		this.secondaryRole = secondaryRole;
	}
	public String getDataSource() {
		return dataSource;
	}
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}	
}
