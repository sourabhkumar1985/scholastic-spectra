package com.relevance.prism.util;

import java.net.UnknownHostException;

import jcifs.UniAddress;
import jcifs.smb.SmbAuthException;
import jcifs.smb.SmbException;

public class UserAuthenticator {

	public boolean login(String userName, String password) {
		boolean result = false;
		   try{
			   //System properties
//			   System.setProperty("jcifs.resolveOrder", "LMHOSTS,WINS,DNS");
			   System.setProperty("jcifs.resolveOrder", "WINS,DNS");
			   System.setProperty("jcifs.lmCompatibility", "2");
			   System.setProperty("jcifs.netbios.hostname", "ADG_SAMPLECODE");


		
			   //Login Information and validations
			   String jnjmsusername = userName;//"LKODAVAT";
			   String jnjmsdomain = "NA";//Values coming like {ap,eu,la,na and jnj} na.jnj.com
			   String jnjmspassword = password;//"Venkata@12";//enter password
			   if (jnjmsdomain != null  && jnjmsusername != null && jnjmspassword != null) {
				   jnjmsdomain = jnjmsdomain.toString().trim();
				   jnjmsusername = jnjmsusername.toString().trim();
				   jnjmspassword = password.toString().trim();
				   jnjmsdomain = jnjmsdomain.toLowerCase();
				   //	if the domain is provided with the “.jnj.com” suffix,
				   //	remove .jnj.com
				   if (jnjmsdomain.equalsIgnoreCase("jnj.com")){//for removing .com from jnj.com domain
					   jnjmsdomain = jnjmsdomain.replaceAll(".com","").trim();
				   }else if (jnjmsdomain.endsWith(".jnj.com")) {//for removing .jnj.com from na.jnj.com
					   jnjmsdomain = jnjmsdomain.substring(0, jnjmsdomain.length() - 8);
				   }
				   //	get the UniAddress
				   UniAddress domainController = null;
				   if (jnjmsdomain.equalsIgnoreCase("jnj")){
					   jnjmsdomain = jnjmsdomain.toUpperCase();
//					   domainController = UniAddress.getByName("10.36.108.28");
//					   manager.reportSuccess("domainController if"+domainController);
				   }else{
//					   manager.reportSuccess("jnjmsdomain   :"+jnjmsdomain);
					   if(jnjmsdomain.equalsIgnoreCase("ap")){
						   domainController = UniAddress.getByName("10.36.108.36");
//						   jnjmsdomain = jnjmsdomain.toUpperCase();
					   }else if(jnjmsdomain.equalsIgnoreCase("eu")){
						   domainController = UniAddress.getByName("10.36.108.34");
//						   jnjmsdomain = jnjmsdomain.toUpperCase();
					   }else if(jnjmsdomain.equalsIgnoreCase("la")){
						   domainController = UniAddress.getByName("10.36.108.32");
//						   jnjmsdomain = jnjmsdomain.toUpperCase();
					   }else if(jnjmsdomain.equalsIgnoreCase("na")){
						   domainController = UniAddress.getByName("10.36.108.29");
//						   jnjmsdomain = jnjmsdomain.toUpperCase();
					   }

				   }		

				   //	Create the NtlmPasswordAuthentication credentials object
				   jcifs.smb.NtlmPasswordAuthentication credentials = new jcifs.smb.NtlmPasswordAuthentication(jnjmsdomain, jnjmsusername, jnjmspassword);
//				   manager.reportSuccess(" domain   :"+credentials.getDomain());
				   //	submit the authentication attempt
				   jcifs.smb.SmbSession.logon(domainController, credentials);
				   //String userName = UUID.
				   //wdContext.currentContextElement().setMessage("Login Success");
				   result = true;
			   }else{
				   System.out.println("Please Enter login details");
			   }
			   //	   convert the passed-in domain to lower case
		   }catch (UnknownHostException ue) {
			   result = false;
			   System.out.println("Domain Controller IP address not being resolved"+ ue);
		   } catch (SmbAuthException authE) {
			   //	handle the case of a bad username / password
			   result = false;
			   System.out.println("Incorrect Username/Password or Domain" + authE);
			  
		   } catch (SmbException e) {
			   //  Auto-generated catch block
			   result = false;
			   System.out.println("SmbException" + e);
			   
		   }catch (Exception e) {
			   result = false;
			   System.out.println("Exception" + e);
			  
		   }
		System.out.println("Login authentication result   :"+ result);
		
		return result;
	}
}


