package com.relevance.prism.data;

import java.util.Comparator;

/**
 * 
 * E2emf FlowData View Object 
 *
 */
public class FlowData {
	
	private String fromCode;
	private String fromName;
	private int from;
	private String fromType;
	private String fromText;
	private String fromColor;
	
	private String toCode;
	private String toName;
	private int to;
	private String toText;
	private String toType;
	private String toColor;
	
	private String linkType;
	private String linkSubType;
	private double linkValue;
	private String linkColor;
	private String linkValueSuffix;
	private String linkValuePrefix;
	private String linkToolTip;	
	
	public String getFromText() {
		return fromText;
	}

	public void setFromText(String fromText) {
		this.fromText = fromText;
	}

	public String getToText() {
		return toText;
	}

	public void setToText(String toText) {
		this.toText = toText;
	}

	public String getFromColor() {
		return fromColor;
	}

	public void setFromColor(String fromColor) {
		this.fromColor = fromColor;
	}

	public String getToColor() {
		return toColor;
	}

	public void setToColor(String toColor) {
		this.toColor = toColor;
	}

	public String getLinkColor() {
		return linkColor;
	}

	public void setLinkColor(String linkColor) {
		this.linkColor = linkColor;
	}

	public String getLinkSubType() {
		return linkSubType;
	}

	public void setLinkSubType(String linkSubType) {
		this.linkSubType = linkSubType;
	}

	public String getToType() {
		return toType;
	}

	public void setToType(String toType) {
		this.toType = toType;
	}

	public String getFromType() {
		return fromType;
	}

	public void setFromType(String fromType) {
		this.fromType = fromType;
	}

	public String getFromCode() {
		return fromCode;
	}

	public void setFromCode(String fromCode) {
		this.fromCode = fromCode;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public String getToCode() {
		return toCode;
	}

	public void setToCode(String toCode) {
		this.toCode = toCode;
	}

	public String getToName() {
		return toName;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public String getLinkType() {
		return linkType;
	}

	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}

	public double getLinkValue() {
		return linkValue;
	}

	public void setLinkValue(double linkValue) {
		this.linkValue = linkValue;
	}

	public String getLinkToolTip() {
		return linkToolTip;
	}

	public void setLinkToolTip(String linkToolTip) {
		this.linkToolTip = linkToolTip;
	}
	public String getLinkValueSuffix() {
		return linkValueSuffix;
	}

	public void setLinkValueSuffix(String linkValueSuffix) {
		this.linkValueSuffix = linkValueSuffix;
	}

	public String getLinkValuePrefix() {
		return linkValuePrefix;
	}

	public void setLinkValuePrefix(String linkValuePrefix) {
		this.linkValuePrefix = linkValuePrefix;
	}
	/*Comparator for sorting the list by roll no*/
    public static final Comparator<FlowData> netValueComparator = new Comparator<FlowData>() {

	public int compare(FlowData s1, FlowData s2) {

	   Double value1 = new Double(s1.getLinkValue());
	   Double value2 = new Double(s2.getLinkValue());
	   
	   /*For ascending order*/
	   return value2.compareTo(value1);

	   /*For descending order*/
	   //rollno2-rollno1;
   }};  
}
