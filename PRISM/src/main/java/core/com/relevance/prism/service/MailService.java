package com.relevance.prism.service;

import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import com.relevance.prism.dao.EmailNotificationDao;
import com.relevance.prism.dao.MailDao;
import com.relevance.prism.data.EmailNotification;
import com.relevance.prism.data.Mail;
import com.relevance.prism.data.TableDetails;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.EmailClient;
import com.relevance.prism.util.EmailConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.CDL;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.opencsv.CSVReader;
import com.relevance.prism.data.EmailNotification;
import com.relevance.prism.data.Mail;

/**
 * 
 * @author Shawkath Khan
 * @Created_Date Mar 15, 2016
 * @Purpose To send email using pop3 by reading the email settings and template
 *          from JANDJ database
 * @Modified_By
 * @Modified_Date
 * @see B#methodB()
 */

public class MailService extends BaseService {

	/**
	 * @purpose To send email using pop3
	 * @param jsonMailContent
	 * @throws Exception
	 * @throws SQLException
	 */

	public Boolean sendEmail(String mailContent, String subject, String app, File file) throws Exception {
		MailDao mailerDao = new MailDao();
		return mailerDao.sendEmail(mailContent, subject, app, file);
	}

	public String saveEmailNotification(EmailNotification emailNotification, String path)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			Log.Info("insertEmailNotification method called on MailService");
			EmailNotificationDao emailNotificationDao = new EmailNotificationDao();
			responseMessage = emailNotificationDao
					.saveEmailNotification(emailNotification,path);
			Log.Info("Inserted Email Notification in Table");
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}

	public Boolean sendEmailNotifications(List<TableDetails> tableDetailsList,
			String content, String subject, String emailRecepients,
			String fileformat) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		MailDao mailerDao = new MailDao();
		List<File> fileList = new ArrayList<>();
		String fileName;
		try {

			if (tableDetailsList.size() == 1
					&& tableDetailsList.get(0).getData().length() > 0) {
				fileName = parseFileName(tableDetailsList.get(0).getFileName());
				File fileAttachment = getEmailAttachment(tableDetailsList
						.get(0).getData(), fileName, fileformat);
				if (fileAttachment != null)
					fileList.add(fileAttachment);
			} else {
				for (TableDetails tableDetails : tableDetailsList) {
					if (tableDetails.getData().length() > 0) {
						fileName = parseFileName(tableDetails.getFileName());
						// File fileAttachment =
						// mailerDao.convertJsonToCsv(tableDetails.getData(),
						// fileName);
						File fileAttachment = getEmailAttachment(
								tableDetails.getData(), fileName, fileformat);
						if (fileAttachment != null)
							fileList.add(fileAttachment);
					}
				}
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		if (fileList.size() > 0)
			return mailerDao.sendMailWithAttachments(subject, content,
					emailRecepients, fileList);
		else
			return false;
	}

	private File getEmailAttachment(org.json.JSONArray dataJson,
			String fileName, String fileformat) {

		File file = null;
		MailDao mailerDao = new MailDao();
		if ("csv".equalsIgnoreCase(fileformat))
			file = mailerDao.convertJsonToCsv(dataJson, fileName);
		if ("xlsx".equalsIgnoreCase(fileformat))
			file = mailerDao.convertJsonToXlsx(dataJson, fileName);
		return file;
	}

	private String parseFileName(String fileName) {
		String fileName1 = fileName.replaceAll("&", "-");
		String fileName2 = fileName1.replaceAll("\\\\", "-");
		String fileName3 = fileName2.replaceAll("/", "-");
		String fileName4 = fileName3.replaceAll(":", "-");
		String fileName5 = fileName4.replaceAll(":", "-");
		String fileName6 = fileName5.replaceAll("\\*", "-");
		String fileName7 = fileName6.replaceAll("\\?", "-");
		String fileName8 = fileName7.replaceAll("\\\"", "-");
		String fileName9 = fileName8.replaceAll("\\<", "-");
		String fileName10 = fileName9.replaceAll("\\>", "-");
		return fileName10.replaceAll("\\|", "-");
	}

	public Boolean sendEmailWithTemplate(String mailContent, String templateName, String subject)
			throws Exception {
		MailDao mailerDao = new MailDao();
		return mailerDao.sendEmailWithTemplate(mailContent, templateName, subject);
	}

	public String deleteEmailNotification(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String successMessage = null;
		try {
			Log.Info("deleteEmailNotification method called on MailService");
			EmailNotificationDao emailNotificationDao = new EmailNotificationDao();
			EmailNotification emailNotification = new EmailNotification();
			emailNotification.setId(Integer.parseInt(obj[0].toString()));
			successMessage = emailNotificationDao
					.deleteEmailNotification(emailNotification);
			return successMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return successMessage;
	}

	public List<EmailNotification> getEmailNotificationsList() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<EmailNotification> emailNotificationList = null;
		try {
			PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
			EmailNotificationDao emailNotificationDao = new EmailNotificationDao();
			emailNotificationList = emailNotificationDao
					.getAllEmailNotifications();
			PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return emailNotificationList;
	}

	public String sendEmailNotifications() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			EmailNotificationDao emailNotificationDao = new EmailNotificationDao();
			List<EmailNotification> emailNotificationsList = emailNotificationDao
					.getDistinctEmailNotificationsAndFilters();
			for (EmailNotification emailNotification : emailNotificationsList) {
				boolean enabled = emailNotification.isEnabled();
				if (enabled) {
					try {
						sendNotification(emailNotification);
					} catch (Exception ex) {
						PrismHandler.handleException(ex);
					}
				}
				responseMessage = "{\"message\":\"Successfully Email Notifications are sent\"}";
			}
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}

	public String sendEmailNotifications(String appName) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			EmailNotificationDao emailNotificationDao = new EmailNotificationDao();
			List<EmailNotification> emailNotificationsList = emailNotificationDao
					.getDistinctEmailNotificationsAndFilters(appName);
			for (EmailNotification emailNotification : emailNotificationsList) {
				boolean enabled = emailNotification.isEnabled();
				if (enabled) {
					try {
						sendNotification(emailNotification);
					} catch (Exception ex) {
						PrismHandler.handleException(ex);
					}
				}
				responseMessage = "{\"message\":\"Successfully Email Notifications are sent\"}";
			}
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}

	private void sendNotification(EmailNotification emailNotification)
			throws Exception {
		List<TableDetails> tableDetailsList = null;
		DataAnalyserService dataAnalyserService = null;
		try {
			dataAnalyserService = new DataAnalyserService();
			String frequency = emailNotification.getFrequency();
			int frequencyDay = emailNotification.getFrequencyDay();
			if (EmailConstants.DAILY.equalsIgnoreCase(frequency)) {
				tableDetailsList = dataAnalyserService
						.getProfileTableDetails(emailNotification);
				sendEmailNotifications(tableDetailsList,
						emailNotification.getContent(),
						emailNotification.getSubject(),
						emailNotification.getDistEmail(),
						emailNotification.getFileformat());
			} else if (EmailConstants.WEEKLY.equalsIgnoreCase(frequency)) {
				Calendar date = Calendar.getInstance();
				int dayOfTheWeek = date.get(Calendar.DAY_OF_WEEK);
				if (frequencyDay == dayOfTheWeek) {
					tableDetailsList = dataAnalyserService
							.getProfileTableDetails(emailNotification);
					sendEmailNotifications(tableDetailsList,
							emailNotification.getContent(),
							emailNotification.getSubject(),
							emailNotification.getDistEmail(),
							emailNotification.getFileformat());
				}
			} else if (EmailConstants.MONTHLY.equalsIgnoreCase(frequency)) {
				Calendar cal = Calendar.getInstance();
				int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
				if (frequencyDay == dayOfMonth) {
					tableDetailsList = dataAnalyserService
							.getProfileTableDetails(emailNotification);
					sendEmailNotifications(tableDetailsList,
							emailNotification.getContent(),
							emailNotification.getSubject(),
							emailNotification.getDistEmail(),
							emailNotification.getFileformat());
				} else if (frequencyDay == -1) {
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"MM/dd/yyyy");
					Date date = new Date();
					Date convertedDate = dateFormat.parse((date.getMonth() + 1)
							+ "/" + date.getDate() + "/"
							+ (date.getYear() + 1900));
					Calendar c = Calendar.getInstance();
					c.setTime(convertedDate);
					c.set(Calendar.DAY_OF_MONTH,
							c.getActualMaximum(Calendar.DAY_OF_MONTH));
					Date lastDayOfMonth = c.getTime();
					if (convertedDate.compareTo(lastDayOfMonth) == 0) {
						tableDetailsList = dataAnalyserService
								.getProfileTableDetails(emailNotification);
						sendEmailNotifications(tableDetailsList,
								emailNotification.getContent(),
								emailNotification.getSubject(),
								emailNotification.getDistEmail(),
								emailNotification.getFileformat());
					}
				}
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
	}

	public Boolean sendGenericEmailWithAttachment(String subject,
			String content, String emailRecepients, List<File> files)
			throws Exception {
		MailDao mailerDao = new MailDao();
		return mailerDao.sendMailWithAttachments(subject, content,
				emailRecepients, files);
	}
	/*mailX method*/
	public String sendGenericMailX(String subject, String content, String recepients, List<File> files) {
		String response = "true";
		String from = E2emfAppUtil.getAppProperty(E2emfConstants.emailFrom);
		try {

			Log.Info("Sendding Mail,");

			System.out.println("Sendding Mail");

			String host = "127.0.0.1";

			Properties properties = System.getProperties();

			properties.setProperty("mail.smtp.host", host);
			Session session = Session.getDefaultInstance(properties);

			// Create a default MimeMessage object.

			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			if (recepients.contains(",")) {
				final String[] lstToEmails = recepients.split(",");
				for (String toEmail : lstToEmails) {
					message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
				}
			} else if (recepients.contains(";")) {
				final String[] lstToEmails = recepients.split(";");
				for (String toEmail : lstToEmails) {
					message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
				}
			} else {
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(recepients));
			}
			message.setSubject(subject);

			// Now set the actual message

			message.setContent(content, "text/html; charset=ISO-8859-1");
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(content, "text/html");
			javax.mail.Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			if (files != null) {
				for (File file : files) {
					MimeBodyPart attachFilePart = new MimeBodyPart();
					String filecontent = FileUtils.readFileToString(file);
					String fileName = file.getName();
					String fileExt = FilenameUtils.getExtension(fileName);
					System.out.println("Extension processed is : " + fileExt);

					DataSource source = null;
					source = new FileDataSource(file.getAbsolutePath());
					attachFilePart.setDataHandler(new DataHandler(source));

					switch (fileExt) {
					case "csv":
						attachFilePart.setDataHandler(
								new DataHandler(new ByteArrayDataSource(filecontent.getBytes(), "text/csv")));
						// attachFilePart.setHeader("Content-Type", "text/csv");
						break;

					case "xls":
					case "xlsx":
						System.out.println("Inside the xlsx file extension case");
						attachFilePart.setHeader("Content-Type",
								" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
						break;

					case "pdf":
						attachFilePart.setHeader("Content-Type", "application/pdf");
						break;

					case "jpg":
					case "jpeg":
						attachFilePart.setHeader("Content-Type", "image/jpg");
						break;

					case "png":
						attachFilePart.setHeader("Content-Type", "image/png");
						break;

					case "gif":
						attachFilePart.setHeader("Content-Type", "image/gif");
						break;

					default:
						break;
					}
					attachFilePart.setFileName(fileName);
					multipart.addBodyPart(attachFilePart);
				}
			}
			message.setContent(multipart);
			Transport.send(message);
			Log.Info("Mail Sent");

			System.out.println("Mail Sent");
		} catch (Exception ex) {
			System.out.println("Exception Occurred While Sending the Email : - Message " + ex.getMessage()
					+ " Stacktrace " + ex.getStackTrace());
			Log.Error("Exception Occurred While Sending the Email : - Message " + ex.getMessage() + " Stacktrace "
					+ ex.getStackTrace());

			response = "Exception Occurred While Sending the Email : - Message " + ex.getMessage() + " Stacktrace "
					+ ex.getStackTrace();

		}

		return response;

	}
	/*  ending of method*/
	
	public Boolean sendConfigurableEmail(String name, String replaces)  throws Exception {
		Boolean response = false;
		try {
			if(name == null) {
				throw new Exception("No Email Template Name specified");
			}
			MailDao mailerDao = new MailDao();
			Mail mail =  mailerDao.getEmailByCondition(name);
			if(mail != null) {
				
				response = true;
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		return response;
	}
}
