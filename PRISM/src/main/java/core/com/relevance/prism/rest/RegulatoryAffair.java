package com.relevance.prism.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.service.RegulatoryAffairService;
import com.relevance.prism.util.PrismHandler;

@Path("/regulatoryAffair")
public class RegulatoryAffair extends BaseResource {
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/generateMapping")
	public String generateMapping(@FormParam("ids") String id) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String databaseList = null;
		String response = null;
		try {
			RegulatoryAffairService regulatoryAffairService = new RegulatoryAffairService();
			databaseList = regulatoryAffairService.generateMapping();
			Gson gson = new Gson();
			response = gson.toJson(databaseList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/generateOracleItemMapping")
	public String generateOracleItemMapping(@FormParam("ids") String id) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String databaseList = null;
		String response = null;
		try {
			RegulatoryAffairService regulatoryAffairService = new RegulatoryAffairService();
			databaseList = regulatoryAffairService.generateOracleItemMapping();
			Gson gson = new Gson();
			response = gson.toJson(databaseList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
