package com.relevance.prism.controller;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ldap.AuthenticationException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.ThrowableAnalyzer;
import org.springframework.security.web.util.ThrowableCauseExtractor;
import org.springframework.web.filter.GenericFilterBean;

import com.relevance.prism.util.Log;

public class AjaxTimeoutRedirectFilter extends GenericFilterBean {
	private ThrowableAnalyzer throwableAnalyzer = new DefaultThrowableAnalyzer();
	private AuthenticationTrustResolver authenticationTrustResolver = new AuthenticationTrustResolverImpl();
	private int customSessionExpiredErrorCode = 901;
	private String ajaxCall = "Ajax call detected, send {} error code";
	private String redirectToLoginPage = "Redirect to login page...............................";
	//private String e2emfLoginPage = "/SPECTRA/login";
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		try {
			Log.Info("Chain processed normally");
			chain.doFilter(request, response);
		} catch (IOException ex) {
			throw ex;
		} catch (Exception ex) {
			Throwable[] causeChain = throwableAnalyzer.determineCauseChain(ex);
			RuntimeException ase = (AuthenticationException) throwableAnalyzer.getFirstThrowableOfType(AuthenticationException.class, causeChain);
			if (ase == null) {
				ase = (AccessDeniedException) throwableAnalyzer.getFirstThrowableOfType(AccessDeniedException.class,causeChain);
			}
			if (ase != null) {
				if (ase instanceof AuthenticationException) {
					Log.Info(ajaxCall + this.customSessionExpiredErrorCode);
					HttpServletResponse resp = (HttpServletResponse) response;
					Log.Info(redirectToLoginPage);
					resp.sendRedirect(((HttpServletRequest)request).getContextPath() + "/login");
					return;
				} else if (ase instanceof AccessDeniedException) {
					if (authenticationTrustResolver.isAnonymous(SecurityContextHolder.getContext().getAuthentication())) {
						Log.Info("User session expired or not logged in yet");
						String ajaxHeader = ((HttpServletRequest) request).getHeader("X-Requested-With");
						if ("XMLHttpRequest".equals(ajaxHeader)) {
							Log.Info(ajaxCall + this.customSessionExpiredErrorCode);
							HttpServletResponse resp = (HttpServletResponse) response;
							Log.Info(redirectToLoginPage);
							resp.sendRedirect(((HttpServletRequest)request).getContextPath() + "/login");
						} else {
							Log.Info(ajaxCall + this.customSessionExpiredErrorCode);
							HttpServletResponse resp = (HttpServletResponse) response;
							Log.Info(redirectToLoginPage);
							resp.sendRedirect(((HttpServletRequest)request).getContextPath() + "/login");
						}
					} else {
						Log.Info(ajaxCall + this.customSessionExpiredErrorCode);
						HttpServletResponse resp = (HttpServletResponse) response;
						resp.sendError(this.customSessionExpiredErrorCode);
					}
				}
			}
		}
	}

	private static final class DefaultThrowableAnalyzer extends	ThrowableAnalyzer {
		/**
		 * * @see org.springframework.security.web.util.ThrowableAnalyzer#
		 * initExtractorMap()
		 */
		@Override
		protected void initExtractorMap() {
			super.initExtractorMap();
			registerExtractor(ServletException.class, new ThrowableCauseExtractor() {
				public Throwable extractCause(Throwable throwable) {
					ThrowableAnalyzer.verifyThrowableHierarchy(throwable, ServletException.class);
					return ((ServletException) throwable).getRootCause();
				}
			});
		}
	}

	public void setCustomSessionExpiredErrorCode(
			int customSessionExpiredErrorCode) {
		this.customSessionExpiredErrorCode = customSessionExpiredErrorCode;
	}
}
