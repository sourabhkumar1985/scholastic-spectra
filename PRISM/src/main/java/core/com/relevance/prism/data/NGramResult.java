package com.relevance.prism.data;

public class NGramResult {
	private String source;
	private String category;
	private String subCategory;
	private int nGram;
	private String company;
	private String clinicalSpeciality;
	private String result;
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	public int getnGram() {
		return nGram;
	}
	public void setnGram(int nGram) {
		this.nGram = nGram;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getClinicalSpeciality() {
		return clinicalSpeciality;
	}
	public void setClinicalSpeciality(String clinicalSpeciality) {
		this.clinicalSpeciality = clinicalSpeciality;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
}
