package com.relevance.prism.data;

public class Role {
	
	private int roleActionId;
	private String role;
	private long actionId;
	private String actionName;
	private int[] actionIds;
	private String displayName;
	private String name;
	private String favicon;
	private String logo;
	private String themeName;
	private int menuOnTop;
	private String loginContent;
	private String icon;
	private int displayOrder;
	private String widgetClass;
	private String widgetColor;
	private int fixedHeader;
	private int fixedNavigation;
	private int fixedRibbon;
	private int sso;
	private int signUp;
	private String parentRole;
	private String topRightIcon;
	
	
	public int getSso() {
		return sso;
	}
	public void setSso(int sso) {
		this.sso = sso;
	}
	public int getSignUp() {
		return signUp;
	}
	public void setSignUp(int signUp) {
		this.signUp = signUp;
	}
	public int getFixedHeader() {
		return fixedHeader;
	}
	public void setFixedHeader(int fixedHeader) {
		this.fixedHeader = fixedHeader;
	}
	public int getFixedNavigation() {
		return fixedNavigation;
	}
	public void setFixedNavigation(int fixedNavigation) {
		this.fixedNavigation = fixedNavigation;
	}
	public int getFixedRibbon() {
		return fixedRibbon;
	}
	public void setFixedRibbon(int fixedRibbon) {
		this.fixedRibbon = fixedRibbon;
	}
	public String getWidgetColor() {
		return widgetColor;
	}
	public void setWidgetColor(String widgetColor) {
		this.widgetColor = widgetColor;
	}
	public int getRoleActionId() {
		return roleActionId;
	}
	public void setRoleActionId(int roleActionId) {
		this.roleActionId = roleActionId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public long getActionId() {
		return actionId;
	}
	public void setActionId(long actionId) {
		this.actionId = actionId;
	}
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	public int[] getActionIds() {
		return actionIds;
	}
	public void setActionIds(int[] actionIds) {
		this.actionIds = actionIds;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFavicon() {
		return favicon;
	}
	public void setFavicon(String favicon) {
		this.favicon = favicon;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getThemeName() {
		return themeName;
	}
	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}
	public int getMenuOnTop() {
		return menuOnTop;
	}
	public void setMenuOnTop(int menuOnTop) {
		this.menuOnTop = menuOnTop;
	}
	public String getLoginContent() {
		return loginContent;
	}
	public void setLoginContent(String loginContent) {
		this.loginContent = loginContent;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public int getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}
	public String getWidgetClass() {
		return widgetClass;
	}
	public void setWidgetClass(String widgetClass) {
		this.widgetClass = widgetClass;
	}
	public String getPaRentrole() {
		return parentRole;
	}
	public void setParentRole(String parentrole) {
		this.parentRole = parentrole;
	}
	public String getTopRightIcon() {
		return topRightIcon;
	}
	public void setTopRightIcon(String topRightIcon) {
		this.topRightIcon = topRightIcon;
	}
	
	
}
