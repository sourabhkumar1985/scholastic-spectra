package com.relevance.prism.rest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.MDC;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;

/**
 * 
 * E2emf RequestProcessor
 * 
 */
public class RequestProcessor extends BaseResource implements ResourceFilter,
		ContainerRequestFilter {

	@Context
	ServletContext context;

	@Context
	UriInfo uriInfo;

	@Context
	private HttpServletRequest httpRequest;

	public RequestProcessor(@Context ServletContext value) {
		this.context = value;
		// System.out.println("Conext:" + this.context);
	}

	/*
	 * @Override public ContainerRequest filter(ContainerRequest request) {
	 * String requestUrl = request.getAbsolutePath().toString();
	 * 
	 * System.out.println("Request Recieved for processing is  \n" +
	 * requestUrl); Log.Info("Request Recieved for processing is  \n" +
	 * requestUrl);
	 * 
	 * String name = httpRequest.getSession().getAttribute("name").toString();
	 * MDC.put("MyMDC", name);
	 */
	/*
	 * UriBuilder ub = uriInfo.getAbsolutePathBuilder(); URI requestUri = null;
	 * 
	 * if (requestUrl.contains("onload")) { requestUri =
	 * ub.path("processInitialLoad").build(); } else if
	 * (requestUrl.contains("search")) {
	 * System.out.println("search service Called"); //requestUri =
	 * ub.path("search").build(); } else if (requestUrl.contains("profile")) {
	 * System.out.println("profile service Called"); //requestUri =
	 * ub.path("profile").build(); } else if (requestUrl.contains("master")) {
	 * System.out.println("master service Called"); //requestUri =
	 * ub.path("master").build(); } else{ System.out.println(
	 * "No Specific request recieved, hence fowarding to startupService");
	 * requestUri = ub.path("onload/processInitialLoad").build(); }
	 * 
	 * requestUri = ub.path("onload/processInitialLoad").build();
	 * request.setUris(request.getBaseUri(), requestUri);
	 * 
	 * Log.Debug("Forwarding the request to container with URL: " +
	 * request.getAbsolutePath());
	 */
	/*
	 * return request; }
	 */

	public ContainerRequest filter(ContainerRequest request) {
		try {
			String requestUrl = request.getAbsolutePath().toString();
			System.out.println("Request Recieved for processing is  \n"
					+ requestUrl);
			Log.Info("Request Recieved for processing is  \n" + requestUrl);
			Authentication auth = SecurityContextHolder.getContext()
					.getAuthentication();

			String name = null;
			if (auth != null) {
				name = auth.getName(); // get logged in username
				name = name.toLowerCase();
				if(name == null || name.equalsIgnoreCase("''")){
					MDC.put("MyMDC", "SYSTEM");
				}else{
					MDC.put("MyMDC", name);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return request;
	}

	@Override
	public ContainerRequestFilter getRequestFilter() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ContainerResponseFilter getResponseFilter() {
		// TODO Auto-generated method stub
		return null;
	}

}
