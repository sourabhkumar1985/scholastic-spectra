package com.relevance.prism.service;

import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.relevance.prism.dao.CommentsDao;
import com.relevance.prism.data.Comment;
import com.relevance.prism.data.CommentLog;
import com.relevance.prism.util.PrismHandler;

public class CommentsService extends BaseService {
	
	public String insertComment(Comment comment) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			CommentsDao commentsDao = new CommentsDao();
			responseMessage = commentsDao.insertComment(comment);
		} catch(Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public HashMap<String,String> getComments(Comment comment, boolean setLastRead) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Comment> commentList = null;
		List<CommentLog> commentLog = null;
		HashMap<String,String> hp = null;
		try {
			CommentsDao commentsDao = new CommentsDao();
			commentList = commentsDao.getComments(comment);
			hp = new HashMap<>();
			hp.put("commentList",new Gson().toJson(commentList));
			
			commentLog  = commentsDao.getLastRead(comment,setLastRead);
			hp.put("commentLog",new Gson().toJson(commentLog));

			return hp;
		} catch(Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return hp;
	}
	
	
	public List<CommentLog> getLastRead(Comment comment,boolean setLastRead) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<CommentLog> commentLog = null;
		try {
			CommentsDao commentsDao = new CommentsDao();
			commentLog = commentsDao.getLastRead(comment,setLastRead);
			return commentLog;
		} catch(Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return commentLog;
	}
}
