package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



//import com.relevance.eemf.data.E2emfMaterialFlowData;
import com.relevance.prism.data.FlowData;
import com.relevance.prism.data.GoodsHistoryList;
import com.relevance.prism.data.MaterialReportList;
import com.relevance.prism.data.E2emfBusinessObject;
import com.relevance.prism.data.Master;
import com.relevance.prism.data.MasterItem;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.PrismDbUtil;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
import com.relevance.prism.data.E2emfView;

public class SecurityMasterDao extends BaseDao implements E2emfDao{
	
	 Connection con = null;
	 Statement stmt = null;
	    
	    @Override
		public GoodsHistoryList getGoodsHistory(Object... obj){
			return null;
		}
	@Override
	public E2emfBusinessObject getBusinessObject() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E2emfBusinessObject getBusinessObject(Object... obj) throws SQLException, NullPointerException, Exception{		
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String param = null;
		String searchParam = null;
		String masterSearchQuery = null;
		
		ResultSet rs = null;
		List<MasterItem>  masterPojoList = new ArrayList<MasterItem>();
		Master master = new Master();
		
		
		if(obj.length >1){			
			param = (String) obj[0];
			searchParam = (String) obj[1];
			Log.Info("Getting Filter details for param " + param + " and search Str : " +searchParam);
		}else{
			Log.Info("No params specified for Security Master Search ");
		}
		
		if( param != null && searchParam != null){
				masterSearchQuery = dbUtil.buildSecurityMasterSearchQuery(param, searchParam);
				
				try{
					//if (con == null)
					con = dbUtil.getHiveConnection(E2emfConstants.securityDB);
					
					if(con != null){
					
						//if(stmt == null)
							stmt = con.createStatement();
						
						rs = stmt.executeQuery(masterSearchQuery); 
				        Log.Info("Fetched resultset with fetchsize " + rs.getFetchSize() + " for the Security searchQuery : \n " + masterSearchQuery );
					   	if(rs != null) {        	
				        	while(rs.next()){
				        		MasterItem masterPojo = new MasterItem();
				        		
				        		masterPojo.setId(rs.getString("id"));
				        		
				        		masterPojoList.add(masterPojo);
				        	}
				        	
				        	master.setMasterDataList(masterPojoList);
				        } 
				        /*else{
				        	Log.Info("ResultSet Empty for executed for query : \n " + masterSearchQuery);
				        } */
						
					} else {
						Log.Info("Connection object is null");
					}
					
				}catch(Exception ex){
					PrismHandler.handleException(ex, true);
				}
				finally {
					PrismHandler.handleFinally(con, rs, stmt);
				}	
					
		}else{
			Log.Info("Criteria not set to fetch Master Details..");
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return master;
		
		}

	@Override
	public List<FlowData> getFlowDataList(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeQuery(String masterSearchQuery) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeQuery(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E2emfView getLevel2ViewDetails(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E2emfView getLevel3ViewDetails(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E2emfView getDefaultViewDetails(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public MaterialReportList getMaterialReport(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}
	/*@Override
	public E2emfMaterialFlowData getE2emfMaterialFlowData(String source)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}*/
	@Override
	public E2emfBusinessObject getActions(Object[] obj) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public E2emfBusinessObject getRoles(Object[] obj) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public E2emfBusinessObject getPrimaryRoles(Object[] obj)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

}
