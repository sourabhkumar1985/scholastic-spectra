package com.relevance.prism.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.relevance.prism.service.CrystalRegExpService;
import com.relevance.prism.util.PrismHandler;

@Path("/crystal")
public class CrystalRegExp extends BaseResource {
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/saveExtractedText")
	public String saveExtractedText(
			@FormParam("source") String source, 
			@FormParam("database") String database, 
			@FormParam("table") String table,
			@FormParam("type") String type,
			@FormParam("regexp") String regexp) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		String serviceMessage = null;
		CrystalRegExpService regexpService = new CrystalRegExpService();
		try {
			serviceMessage = regexpService.insertRegExpersion(source, database, table, type, regexp);
			response = "{\"message\" : \""+serviceMessage+"\"}";
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
