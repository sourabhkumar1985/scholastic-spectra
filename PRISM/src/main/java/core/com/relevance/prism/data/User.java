package com.relevance.prism.data;

import java.util.List;
public class User{

	private String username;
	private String password;
	private long enabled;
	private List<String> roles;
	private String[] rolesAsArray;
	private String themeName;
	private long menuOnTop;
	private String primaryRole;
	private String displayname;
	private String name;
	private String parentRole;
	private String favicon;
	private String logo;
	private String loginContent;
	private long displayOrder;
	private String widgetClass;
	private String icon;
	private String widgetColor;
	private long fixedHeader;
	private long fixedNavigation;
	private long fixedRibbon;
	private String dateformat;
	private String email;
	private String businessrole;
	private String firstName;
	private String lastName;
	private String timezone;
	private String status;
	private String region;
	private String remarks;
	private long sso;
	private long signUp;
	private String topRightIcon;
	
	private int id;
	private boolean response;
	private int userId;
	private UserAdditionalDetails additionalDetails;
	
	
	public UserAdditionalDetails getAdditionalDetails() {
		return additionalDetails;
	}

	public void setAdditionalDetails(UserAdditionalDetails additionalDetails) {
		this.additionalDetails = additionalDetails;
	}

	public long getSso() {
		return sso;
	}

	public void setSso(long sso) {
		this.sso = sso;
	}

	public long getSignUp() {
		return signUp;
	}

	public void setSignUp(long signUp) {
		this.signUp = signUp;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getDateformat() {
		return dateformat;
	}

	public void setDateformat(String dateformat) {
		this.dateformat = dateformat;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public long getFixedHeader() {
		return fixedHeader;
	}

	public void setFixedHeader(long fixedHeader) {
		this.fixedHeader = fixedHeader;
	}

	public long getFixedNavigation() {
		return fixedNavigation;
	}

	public void setFixedNavigation(long fixedNavigation) {
		this.fixedNavigation = fixedNavigation;
	}

	public long getFixedRibbon() {
		return fixedRibbon;
	}

	public void setFixedRibbon(long fixedRibbon) {
		this.fixedRibbon = fixedRibbon;
	}

	public String getWidgetColor() {
		return widgetColor;
	}

	public void setWidgetColor(String widgetColor) {
		this.widgetColor = widgetColor;
	}

	public String getWidgetClass() {
		return widgetClass;
	}

	public void setWidgetClass(String widgetClass) {
		this.widgetClass = widgetClass;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	
	public String getLoginContent() {
		return loginContent;
	}

	public void setLoginContent(String loginContent) {
		this.loginContent = loginContent;
	}
	
	public String getTopRightIcon() {
		return topRightIcon;
	}

	public void setTopRightIcon(String topRightIcon) {
		this.topRightIcon = topRightIcon;
	}

	public String getPrimaryRole() {
		return primaryRole;
	}

	public void setPrimaryRole(String primaryRole) {
		this.primaryRole = primaryRole;
	}

	public long getMenuOnTop() {
		return menuOnTop;
	}

	public void setMenuOnTop(long menuOnTop) {
		this.menuOnTop = menuOnTop;
	}

	public String getThemeName() {
		return themeName;
	}

	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getEnabled() {
		return enabled;
	}

	public void setEnabled(long enabled) {
		this.enabled = enabled;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	
	public String[] getRolesAsArray() {
		return rolesAsArray;
	}

	public void setRolesAsArray(String[] rolesAsArray) {
		this.rolesAsArray = rolesAsArray;
	}
	public long getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(long displayOrder) {
		this.displayOrder = displayOrder;
	}
	@Override
	public String toString(){
		return "User Name : "+ username + " Password : " + password + " Enabled : " + enabled;
	}
	
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFavicon() {
		return favicon;
	}
	public void setFavicon(String favicon) {
		this.favicon = favicon;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isResponse() {
		return response;
	}

	public void setResponse(boolean response) {
		this.response = response;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getBusinessRole() {
		return businessrole;
	}

	public void setBusinessRole(String businessRole) {
		this.businessrole = businessRole;
	}
	public String getParentRole() {
		return parentRole;
	}

	public void setParentRole(String parentrole) {
		this.parentRole = parentrole;
	}

	
	
	
}
