package com.relevance.prism.data;

import org.json.JSONArray;

public class TableDetails {
	private JSONArray data;
	private String fileName;
	public JSONArray getData() {
		return data;
	}
	public void setData(JSONArray data) {
		this.data = data;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
