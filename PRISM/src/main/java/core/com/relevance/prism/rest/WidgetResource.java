package com.relevance.prism.rest;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.data.Widget;
import com.relevance.prism.service.WidgetService;
import com.relevance.prism.util.PrismHandler;

@Path("/widget")
public class WidgetResource  extends BaseResource  {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getWidget")
	public String getEntity(@FormParam("widgetName") String widgetName){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		Widget widgetResponse;
		try{
			if(widgetName == null || "".equalsIgnoreCase(widgetName)) {
				throw new Exception("No Widgetname specified");
			}
			WidgetService service = new WidgetService();
			widgetResponse = service.getWidgetByName(widgetName);
			Gson gson = new Gson();
			response = gson.toJson(widgetResponse);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
