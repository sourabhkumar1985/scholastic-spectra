package com.relevance.prism.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import com.relevance.prism.util.PrismHandler;

public class NGramAlgorithm extends BaseService implements Callable<Map<String, Integer>> {
	private String input;
	private int gramNumber;
	
	String thetext;
	String cutoff;
	String punc;
	String gram;
	String[] previous = new String[] { "", "", "", "", "" };
	int total = 0;
	String current = "";
	List<String> unImportantWordsList;
	Map<String, Integer> nGramHashMap;
	Iterator<Map.Entry<String, Integer>> it ;
	String[] unImportantWords = new String[] { "please", "i", "me", "my",
			"myself", "we", "us", "our", "ours", "ourselves", "you", "your",
			"yours", "yourself", "yourselves", "he", "him", "his", "himself",
			"she", "her", "hers", "herself", "it", "its", "itself", "they",
			"them", "their", "theirs", "themselves", "what", "which", "who",
			"whom", "whose", "this", "that", "these", "those", "am", "is",
			"are", "was", "were", "be", "been", "being", "have", "has", "had",
			"having", "do", "does", "did", "doing", "will", "would", "should",
			"can", "could", "ought", "i'm", "you're", "he's", "she's", "it's",
			"we're", "they're", "i've", "you've", "we've", "they've", "i'd",
			"you'd", "he'd", "she'd", "we'd", "they'd", "i'll", "you'll",
			"he'll", "she'll", "we'll", "they'll", "isn't", "aren't", "wasn't",
			"weren't", "hasn't", "haven't", "hadn't", "doesn't", "didn't",
			"won't", "wouldn't", "shan't", "shouldn't", "can't", "cancouldn't",
			"mustn't", "let's", "that's", "who's", "what's", "here's",
			"there's", "when's", "where's", "why's", "how's", "a", "an", "the",
			"and", "but", "if", "or", "because", "as", "until", "while", "of",
			"at", "by", "for", "with", "about", "against", "between", "into",
			"through", "during", "before", "after", "above", "below", "to",
			"from", "up", "upon", "down", "in", "out", "on", "off", "over",
			"under", "again", "further", "then", "once", "here", "there",
			"when", "where", "why", "how", "all", "any", "both", "each", "few",
			"more", "most", "other", "some", "such", "no", "nor", "only",
			"own", "same", "so", "than", "too", "very", "say", "says", "said",
			"shall", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b",
			"c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o",
			"p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "\n", "\\n" };
	public NGramAlgorithm(String input, int gramNumber){
		this.input = input;
		this.gramNumber = gramNumber;
		this.unImportantWordsList  = Arrays.asList(unImportantWords);
	}
	@Override
	 public Map<String, Integer> call()  throws Exception{
		Map<String, Integer> nGramHashMapTemp = null;
		try {
			nGramHashMapTemp = processLine(gramNumber, input);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		return nGramHashMapTemp;
	}
	public Map<String, Integer> processLine(int n, String line) throws Exception {
		HashMap<String, Integer> ngrams;
		HashMap<String, Integer> unigrams;
		 Map<String, Integer> response = null;
		try {
			ngrams = new HashMap<>();
			unigrams = new HashMap<>();
			boolean found;
			String[] words = line.split("[\\s+]");
			for (String word : words) {
				String processedWord = word.trim();
				if (processedWord.isEmpty() || unImportantWordsList.contains(processedWord.toLowerCase()))
					continue;
				
				Integer gramCount = unigrams.get(processedWord);
				if (gramCount != null)
					unigrams.put(processedWord, gramCount + 1);
				else
					unigrams.put(processedWord, 1);

				gram = "";
				total++;
				found = false;
				for (int i = n - 2; i >= 0; i--) {
					if (previous[i].trim() != "") {
						found = true;
						if (gram.trim() == "")
							gram = previous[i].trim();
						else
							gram += " " + previous[i].trim();
					}
				}
				if (processedWord != "" && found) {
					if (gram.trim() == "")
						gram = processedWord;
					else
						gram += " " + processedWord;

					if (gram.trim().split(" ").length == n) {
						gram = gram.toLowerCase();
						if (ngrams.containsKey(gram))
							ngrams.put(gram, ngrams.get(gram) + 1);
						else
							ngrams.put(gram, 1);
					}
				}
				previous[4] = previous[3];
				previous[3] = previous[2];
				previous[2] = previous[1];
				previous[1] = previous[0];

				previous[0] = processedWord;
			}
			if (n == 1) {
				response = unigrams;
			} else {
				response = ngrams;
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		return response;
	}
}
