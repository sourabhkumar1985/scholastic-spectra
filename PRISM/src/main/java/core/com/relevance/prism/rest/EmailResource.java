package com.relevance.prism.rest;

import java.io.File;
import java.net.URL;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.relevance.prism.util.EmailClient;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
import com.relevance.prism.util.ZipUtil;

@Path("/email")
public class EmailResource extends BaseResource{

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/sendemail")
	public String sendEmail(@FormParam("sourceemail") String sourceemail, 
						@FormParam("password") String password, 
						@FormParam("toemail") String toemail,
						@FormParam("subject") String subject,
						@FormParam("body") String body, 
						@FormParam("urllist") String urllist){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		String filepath = null;
		String tDir = System.getProperty("java.io.tmpdir");
		try{
			System.out.println("URL List : " + urllist);
			String [] urls = urllist.split(",");
			if(urls.length > 1){
				filepath = ZipUtil.createZipFile(urls);
				File file = new File(filepath);
				//responseMessage = EmailClient.sendEmail(toemail, subject, body, filepath);
				response = EmailClient.sendEmailInLinuxServers(toemail, subject, body, file.getAbsolutePath());
			}else if(urls.length == 1) {
				String urlPath = urls[0];
				URL url = new URL(urlPath);	            
	            String fileName = urlPath.substring(urlPath.lastIndexOf("/")+1);
	            String path = tDir + fileName;
	            File outPutfile = new File(path);
	            outPutfile.delete();
	            FileUtils.copyURLToFile(url, outPutfile);
	            //responseMessage = EmailClient.sendEmail(toemail, subject, body, outPutfile.getAbsolutePath());
	            response = EmailClient.sendEmailInLinuxServers(toemail, subject, body, outPutfile.getAbsolutePath());
			}else{
				Log.Error("Exception while adding the Sending Email No Files are Selected.");
				response = "{\"message\":\"failure\"}";
			}
        	//File filepath = new File(filePath);			
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/sendGenericEmail")
	public String sendGenericEmail(@FormParam("from") String from, 
			@FormParam("to") String to, 
			@FormParam("subject") String subject,
			@FormParam("content") String content) {
		String response = null;
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
			Log.Info("Inside Email Resource");
			System.out.println("Inside Email Resource");
			EmailClient.sendGenericMailX(from, to, subject, content);
			response = "{\"message\" : \"Mail Sent successfully\"}";
			Log.Info("Email Sent Successfully");
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/sendGenericEmails")
	public String sendGenericEmails(@FormParam("requestParam") String requestParam) {
		String response = null;
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
			Log.Info("Inside Email Multiple Resource");
			System.out.println("Inside Email Multiple Resource");
			if(requestParam == null || requestParam.equalsIgnoreCase("")) {
				throw new Exception("No Request Param Specified");
			}
			String[] emails = requestParam.split(",");
			for(String email : emails) {
				email = new String(DatatypeConverter.parseBase64Binary(email), "UTF-8");
				JSONParser parser = new JSONParser();
				JSONObject json = (JSONObject) parser.parse(email);
				EmailClient.sendGenericMailX((String)json.get("from"), (String)json.get("to"), (String)json.get("subject"), (String)json.get("content"));
			}
			
			response = "{\"message\" : \"Mails Sent successfully\"}";
			Log.Info("Emails Sent Successfully");
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
