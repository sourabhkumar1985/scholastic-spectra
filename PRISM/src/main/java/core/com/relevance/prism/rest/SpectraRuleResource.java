package com.relevance.prism.rest;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.relevance.prism.service.SpectraRulesService;
import com.relevance.prism.util.PrismHandler;

@Path("/rule-engine")
public class SpectraRuleResource extends BaseResource{
		@Context
		ServletContext context;

		public SpectraRuleResource(@Context ServletContext value) {
			this.context = value;
		}
		
		@POST
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
		@Path("/runrules")
		public String runRules(@FormParam("ids") String ids, @FormParam("project") String project)
				throws Exception {
			PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
			String response = null;
			try {
				SpectraRulesService spectraRulesService = new SpectraRulesService();
				String [] idsList = ids.split(",");
				response = spectraRulesService.runRules(idsList, project);
			} catch (Exception ex) {
				response = PrismHandler.handleException(ex);
			}
			PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
			return response;
		}
}
