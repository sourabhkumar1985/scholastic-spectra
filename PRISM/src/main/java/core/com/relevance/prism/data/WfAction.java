package com.relevance.prism.data;

/**
 *
 * @author: Shawkath Khan
 * @Created_Date: Feb 17, 2016
 * @Purpose: Entity for Workflow Action
 * @Modified_By: 
 * @Modified_Date:  
 */

public class WfAction {

	private String code;
	private String description;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
