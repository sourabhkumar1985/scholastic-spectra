package com.relevance.prism.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.MDC;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.userdetails.LdapUserDetails;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.relevance.prism.data.Role;
import com.relevance.prism.data.User;
import com.relevance.prism.data.UserAdditionalDetails;
import com.relevance.prism.data.UserSecondaryRole;
import com.relevance.prism.service.AdminService;
import com.relevance.prism.service.LicenseKeyService;
import com.relevance.prism.service.UserService;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;

@Controller
public class LoginController {

	private String menuStyle = "menustyle";

	@RequestMapping({ "login", "ic", "IC", "PPA", "ppa", "docspera", "DOCSPERA" })
	public ModelAndView getLoginForm(
			@RequestParam(required = false) String authfailed, String logout,
			String denied, HttpSession sessionObj, HttpServletRequest request,
			HttpServletResponse response) {
		String viewName = "/web/login";
		String message = "";
		String licenseKeyStatus = "";
		
		try{
			LicenseKeyService keyservice = new LicenseKeyService();
			licenseKeyStatus = keyservice.licenseCheck();
		} catch (Exception ee)
		{
			Log.Error(ee);
		}

		try {
			if (sessionObj != null) {
				Authentication auth = SecurityContextHolder.getContext()
						.getAuthentication();
				String name = null;
				if (auth != null) {
					name = auth.getName();
					name = ("".equalsIgnoreCase(name)) ? "anonymoususer" : name;
				}
				if (sessionObj.getAttribute("app") != null && name != null
						&& !"anonymoususer".equalsIgnoreCase(name)) {
					if (request.getParameter("app") != null
							&& !"".equalsIgnoreCase(request.getParameter("app")))
						sessionObj.setAttribute("app",
								request.getParameter("app"));
					response.sendRedirect(request.getContextPath() + "/index");
					return null;
				}
				sessionObj.setAttribute("app", request.getParameter("app"));
				sessionObj.setAttribute("version", E2emfAppUtil
						.getAppVersionProperty(E2emfConstants.revisionNo));
				sessionObj.setAttribute("serverIp", E2emfAppUtil.getAppProperty("serverIpConfig"));
			}
			
		} catch (AppException appe) {
			Log.Error(appe);
		} catch (Exception e) {
			Log.Error(e);
		}
		
		
		
		if (authfailed != null) {
			message = "Invalid username or password, try again !";
		} else if (logout != null) {
			message = "Logged out successfully, login again to continue !";
		} else if (denied != null) {
			message = sessionObj.getAttribute("errorMessage") != null && !"".equalsIgnoreCase(sessionObj.getAttribute("errorMessage").toString())? sessionObj.getAttribute("errorMessage").toString() : "User Access Denied. Please contact System administrator."; 
		}else if(licenseKeyStatus == "Expired")
		{
			message = "The License key has Expired, please renew now!";
			sessionObj.setAttribute("redirectPage", "web/licenseexpired.jsp");
			//viewName = "web/LicenseExpired";
		}
		 else if(licenseKeyStatus == "Invalid")
		{
			message = "The License key is Invalid, please renew/update!";
			sessionObj.setAttribute("redirectPage", "web/licenseexpired.jsp");
			//viewName = "web/LicenseExpired";
		}
		return new ModelAndView(viewName, "message", message);
	}

	@RequestMapping("dsplogin")
	public void getDSPLoginForm(HttpServletRequest request, HttpServletResponse response) {
		HttpSession sessionObj = null;
		try {
			if(request.getParameter("client").equalsIgnoreCase("MERCK")){
				if(request.getParameter("username") != null && request.getParameter("app") != null){
					sessionObj = request.getSession(true);
					//String userName = request.getParameter("username");
					//String password = request.getParameter("password");
					sessionObj.setAttribute("app", request.getParameter("app"));
					sessionObj.setAttribute("user", request.getParameter("username"));
					//sessionObj.setAttribute("password", request.getParameter("password"));
					sessionObj.setAttribute("client", request.getParameter("client"));
					sessionObj.setAttribute("app", request.getParameter("app"));
					sessionObj.setAttribute("version", E2emfAppUtil.getAppVersionProperty(E2emfConstants.revisionNo));
					sessionObj.setAttribute("serverIp", E2emfAppUtil.getAppProperty("serverIpConfig"));
										
					//UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userName, password);

				    // Authenticate the user
				    //Authentication authentication = AuthenticationManager.authenticate(authRequest);
				    //SecurityContext securityContext = SecurityContextHolder.getContext();
				    //securityContext.setAuthentication(authentication);

				    // Create a new session and add the security context.
				    //HttpSession session = request.getSession(true);
				    //sessionObj.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
				    
				    response.sendRedirect(request.getContextPath() + "/indexpageforcustomusers");
				}
			}
		} catch (AppException appe) {
			Log.Error(appe);
		} catch (Exception e) {
			Log.Error(e);
		}		
	}
	
	@RequestMapping("index")
	public String geIndexPage(HttpSession sessionObj) {
		String app;
		String firstName;
		String lastName;
		String email;
		String displayName;
		try {
			if (sessionObj != null && sessionObj.getAttribute("app") != null)
				app = sessionObj.getAttribute("app").toString();
			else
				return "redirect:login";
			Authentication auth = SecurityContextHolder.getContext()
					.getAuthentication();
			SAMLCredential credential=(SAMLCredential)SecurityContextHolder.getContext().getAuthentication().getCredentials();
			String name=credential.getAttributeAsString("userID");
			Log.Info(name+"   *****************");
//			String name;
			String primaryRoleName;
			Role primaryRole;
			String userRoleActions;
			LdapUserDetails userDetails;
			boolean isSSO = false;
			if (auth != null && !auth.getPrincipal().toString().equalsIgnoreCase("anonymousUser")) {
				Gson gson = new Gson();
//				name = auth.getName();
//				name = name.toLowerCase();
				if (auth.getPrincipal() != null
						&& auth.getPrincipal() instanceof LdapUserDetails) {
					userDetails = (LdapUserDetails) auth.getPrincipal();
					String dn = userDetails.getDn();
					String[] array = dn.split("(?<!\\\\),");
					String dispName = array[0].replace("cn=", "");
					dispName = dispName.replace("\\", "").replace("'", "`");
					sessionObj.setAttribute("userDisplayName", dispName);
				} else {
					sessionObj.setAttribute("userDisplayName", name);
				}
				
				Log.Info("Logged In User ID is :" + name);
				sessionObj.setAttribute("userName", name);
				sessionObj.setAttribute("UserEmail", name + "@its.jnj.com");
				if (name == null || "''".equalsIgnoreCase(name)) {
					MDC.put("MyMDC", "SYSTEM");
				} else {
					MDC.put("MyMDC", name);
				}
				Role role;
				AdminService adminService = new AdminService();
				UserService userService = new UserService();
				
				User user = new User();
				user.setName(name);
				user.setUsername(name);
				user = userService.getUser(user);
				sessionObj.setAttribute("userBusinessRole", (user!= null && user.getBusinessRole() == null) ? "" : user.getBusinessRole());
				if(user.getAdditionalDetails() != null && user.getAdditionalDetails().getSpeciality() != null) {
					sessionObj.setAttribute("pltfrmSplty", user.getAdditionalDetails().getSpeciality());
				}
				if(user.getAdditionalDetails() != null && user.getAdditionalDetails().getSpeciality() != null) {
					sessionObj.setAttribute("dept", user.getAdditionalDetails().getDepartment());
				}
				if(user.getRegion() != null) {
					sessionObj.setAttribute("region", user.getRegion());
				}
				
				firstName = user.getFirstName();
				lastName = user.getLastName();
				email = user.getEmail();
				primaryRoleName = user.getPrimaryRole();
				//Check if User is Rejected? if yes, redirect to login.
				if(user.getUsername() == null || "".equalsIgnoreCase(user.getName())) {
					sessionObj.setAttribute("app", null);
					return "redirect:login";
				}
				if(user.getStatus() != null && user.getStatus().equalsIgnoreCase("rejected")) {
					//sessionObj.setAttribute("app", sessionObj.getAttribute("app"));
					sessionObj.setAttribute("app", null);
					return "redirect:login";
				} else if(user.getStatus() != null && user.getStatus().equalsIgnoreCase("pending")) {
					sessionObj.setAttribute("app", null);
					sessionObj.setAttribute("errorMessage",
							"Your registration request has not been approved, you will get access once it is approved by business admin.");
					return "redirect:login?denied&app=" + app;
				}
				user.setStatus((user.getStatus() == null || "".equalsIgnoreCase(user.getStatus()))? "Approved" : user.getStatus());
				if(user.getEnabled() == 0) {
					sessionObj.setAttribute("app", null);
					sessionObj.setAttribute("errorMessage",
							"Your account is disabled. Please contact the system administrator.");
					return "redirect:login?denied&app=" + app;
				}
				if ((firstName == null || "null".equals(firstName))
						&& (lastName == null || "null".equals(lastName))) {
					displayName = name;
				} else if (firstName != null
						&& (lastName == null || "null".equals(lastName))) {
					displayName = firstName;
				} else if (lastName != null
						&& (firstName == null || "null".equals(firstName))) {
					displayName = name + " " + lastName;
				} else {
					displayName = firstName + " " + lastName;
				}

				sessionObj.setAttribute("userDisplayName", displayName);

				if (email != null && !email.trim().isEmpty()
						&& !"null".equalsIgnoreCase(email)) {
					sessionObj.setAttribute("UserEmail", email);
				}
				/*List<UserSecondaryRole> userSecondaryRolesList = userService
						.getDataSourcesForUser(E2emfConstants.qfindRole, name);
				sessionObj.setAttribute("userSecondaryRolesList",
						gson.toJson(userSecondaryRolesList));*/
				List<Role> userRoles = userService.getUserRolesForDisplay(name);//assgined apps				
				//Check For duplicates in Roles against primaryRoleName and appending to roles.
				if(primaryRoleName != null && !primaryRoleName.isEmpty()) {
					boolean isPrimaryRoleAdded = false;
					for(Role userRole : userRoles) {
						if(primaryRoleName.equalsIgnoreCase(userRole.getRole())) {
							isPrimaryRoleAdded = true;							
							break;
						}
					}
					if(!isPrimaryRoleAdded) {
						primaryRole = userService.getRole(primaryRoleName);
						if(primaryRole != null) {
							userRoles.add(primaryRole);
						}
					}
				}
				
				if(sessionObj.getAttribute("sso") != null && !primaryRoleName.equalsIgnoreCase(app)   && "true".equalsIgnoreCase(sessionObj.getAttribute("sso").toString())) {
					sessionObj.setAttribute("app", primaryRoleName);
					sessionObj.setAttribute("sso", "false");
					isSSO = true;
				}
				
				boolean appCheck = false;
				for (Role roleForApp : userRoles) {
					if (roleForApp.getName().equalsIgnoreCase(app)) {
						appCheck = true;
						break;
					}
				}
				if (appCheck && !isSSO) {
					userService.updateUserPrimaryRole(name, app);
					role = userService.getTheme(name);
					if (role != null) {
						sessionObj.setAttribute("themename",
								role.getThemeName());
						sessionObj.setAttribute("topMenuSettings", role.getTopRightIcon());
						if (role.getMenuOnTop() == 1)
							sessionObj.setAttribute(menuStyle, " menu-on-top");
						else
							sessionObj.setAttribute(menuStyle, " ");
						sessionObj.setAttribute("primaryRole", role.getName());
						sessionObj.setAttribute("appname",
								role.getDisplayName());
						sessionObj.setAttribute("lastUpdatedOn", null);
						if(user.getAdditionalDetails() != null && user.getAdditionalDetails().getLastUpdatedDate() != null) {
							sessionObj.setAttribute("lastUpdatedOn", user.getAdditionalDetails().getLastUpdatedDate());
						}
						sessionObj.setAttribute("NotificationClosedOn", null);
						if(user.getAdditionalDetails() != null && user.getAdditionalDetails().getNotificationCloedDate() != null) {
							sessionObj.setAttribute("NotificationClosedOn", user.getAdditionalDetails().getNotificationCloedDate());
						}
						sessionObj.setAttribute("logo", role.getLogo());
						sessionObj.setAttribute("favicon", role.getFavicon());
						sessionObj.setAttribute("icon", role.getIcon());
						sessionObj.setAttribute("widgetclass",
								role.getWidgetClass());
						sessionObj.setAttribute("widgetcolor",
								role.getWidgetColor());

					}

					userRoleActions = userService.getUserRoleActionList(name);
					String lookUpValues = adminService.getLookUpValueList();
					sessionObj.setAttribute("userroleactions", userRoleActions);
					sessionObj.setAttribute("lookupvalues", lookUpValues);
					List<Role> userRolesForDisplay = userService
							.getUserRolesForDisplay(name);//storing assigned apps
					sessionObj.setAttribute("userrolesfordisplay",
							gson.toJson(userRolesForDisplay));
					
					UserAdditionalDetails useradditionalDetails = new UserAdditionalDetails();
					useradditionalDetails.setUserName(user.getUsername());
					userService.updateUserLastLoginDate(useradditionalDetails);
					
					return "/web/index";
				} else if (name != null && "anonymoususer".equals(name)) {
					return "redirect:login?app=" + app;
				} else {
					sessionObj.setAttribute("app", null);
					sessionObj.setAttribute("errorMessage",
							"Access denied for this user!");
					return "redirect:login?denied&app=" + app;
				}
			}
		} catch (AppException appe) {
			Log.Error(appe);
		} catch (Exception e) {
			Log.Error(e);
		}

		return null;
	}
	
	@RequestMapping("indexpageforcustomusers")
	public String getIndexPageForCustomUsers(HttpServletRequest request, HttpServletResponse response) {
		String app;
		String firstName;
		String lastName;
		String email;
		String displayName;
		HttpSession sessionObj = null;
		try {
			if(request.getParameter("client").equalsIgnoreCase("MERCK")){
				if(request.getParameter("username") != null && request.getParameter("app") != null){
					sessionObj = request.getSession(true);
					//String userName = request.getParameter("username");
					//String password = request.getParameter("password");
					sessionObj.setAttribute("app", request.getParameter("app"));
					sessionObj.setAttribute("user", request.getParameter("username"));
					//sessionObj.setAttribute("password", request.getParameter("password"));
					sessionObj.setAttribute("client", request.getParameter("client"));
					sessionObj.setAttribute("app", request.getParameter("app"));
					sessionObj.setAttribute("version", E2emfAppUtil.getAppVersionProperty(E2emfConstants.revisionNo));
					sessionObj.setAttribute("serverIp", E2emfAppUtil.getAppProperty("serverIpConfig"));
										
					//UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userName, password);

				    // Authenticate the user
				    //Authentication authentication = AuthenticationManager.authenticate(authRequest);
				    //SecurityContext securityContext = SecurityContextHolder.getContext();
				    //securityContext.setAuthentication(authentication);

				    // Create a new session and add the security context.
				    //HttpSession session = request.getSession(true);
				    //sessionObj.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
				    
				    //response.sendRedirect(request.getContextPath() + "/indexpageforcustomusers");
				}
			}
		} catch (AppException appe) {
			Log.Error(appe);
		} catch (Exception e) {
			Log.Error(e);
		}		
		try {
			if (sessionObj != null && sessionObj.getAttribute("app") != null)
				app = sessionObj.getAttribute("app").toString();
			else
				return "redirect:login";
			//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String name;
			String primaryRoleName;
			Role primaryRole;
			String userRoleActions;
			//LdapUserDetails userDetails;
			boolean isSSO = false;
			//if (auth != null && !auth.getPrincipal().toString().equalsIgnoreCase("anonymousUser")) {
				Gson gson = new Gson();
				//name = auth.getName();
				//name = name.toLowerCase();
				/*if (auth.getPrincipal() != null	&& auth.getPrincipal() instanceof LdapUserDetails) {
					userDetails = (LdapUserDetails) auth.getPrincipal();
					String dn = userDetails.getDn();
					String[] array = dn.split("(?<!\\\\),");
					String dispName = array[0].replace("cn=", "");
					dispName = dispName.replace("\\", "").replace("'", "`");
					sessionObj.setAttribute("userDisplayName", dispName);
				} else {
					sessionObj.setAttribute("userDisplayName", name);
				}*/
				name = sessionObj.getAttribute("user").toString();
				sessionObj.setAttribute("userDisplayName", name);				
				Log.Info("Logged In User ID is :" + name);
				sessionObj.setAttribute("userName", name);
				//sessionObj.setAttribute("UserEmail", name + "@its.jnj.com");
				if (name == null || "''".equalsIgnoreCase(name)) {
					MDC.put("MyMDC", "SYSTEM");
				} else {
					MDC.put("MyMDC", name);
				}
				Role role;
				AdminService adminService = new AdminService();
				UserService userService = new UserService();
				
				User user = new User();
				user.setName(name);
				user.setUsername(name);
				user = userService.getUser(user);
				sessionObj.setAttribute("userBusinessRole", (user!= null && user.getBusinessRole() == null) ? "" : user.getBusinessRole());
				
				if(user.getAdditionalDetails() != null && user.getAdditionalDetails().getSpeciality() != null) {
					sessionObj.setAttribute("pltfrmSplty", user.getAdditionalDetails().getSpeciality());
				}
				if(user.getAdditionalDetails() != null && user.getAdditionalDetails().getSpeciality() != null) {
					sessionObj.setAttribute("dept", user.getAdditionalDetails().getDepartment());
				}
				if(user.getRegion() != null) {
					sessionObj.setAttribute("region", user.getRegion());
				}
				
				firstName = user.getFirstName();
				lastName = user.getLastName();
				email = user.getEmail();
				primaryRoleName = user.getPrimaryRole();
				//Check if User is Rejected? if yes, redirect to login.
				if(user.getUsername() == null || "".equalsIgnoreCase(user.getName())) {
					sessionObj.setAttribute("app", null);
					return "redirect:login";
				}
				if(user.getStatus() != null && user.getStatus().equalsIgnoreCase("rejected")) {
					//sessionObj.setAttribute("app", sessionObj.getAttribute("app"));
					sessionObj.setAttribute("app", null);
					return "redirect:login";
				} else if(user.getStatus() != null && user.getStatus().equalsIgnoreCase("pending")) {
					sessionObj.setAttribute("app", null);
					sessionObj.setAttribute("errorMessage",
							"Your registration request has not been approved, you will get access once it is approved by business admin.");
					return "redirect:login?denied&app=" + app;
				}
				user.setStatus((user.getStatus() == null || "".equalsIgnoreCase(user.getStatus()))? "Approved" : user.getStatus());
				if(user.getEnabled() == 0) {
					sessionObj.setAttribute("app", null);
					sessionObj.setAttribute("errorMessage",
							"Your account is disabled. Please contact the system administrator.");
					return "redirect:login?denied&app=" + app;
				}
				if ((firstName == null || "null".equals(firstName))
						&& (lastName == null || "null".equals(lastName))) {
					displayName = name;
				} else if (firstName != null
						&& (lastName == null || "null".equals(lastName))) {
					displayName = firstName;
				} else if (lastName != null
						&& (firstName == null || "null".equals(firstName))) {
					displayName = name + " " + lastName;
				} else {
					displayName = firstName + " " + lastName;
				}

				sessionObj.setAttribute("userDisplayName", displayName);

				if (email != null && !email.trim().isEmpty()
						&& !"null".equalsIgnoreCase(email)) {
					sessionObj.setAttribute("UserEmail", email);
				}
				/*List<UserSecondaryRole> userSecondaryRolesList = userService
						.getDataSourcesForUser(E2emfConstants.qfindRole, name);
				sessionObj.setAttribute("userSecondaryRolesList",
						gson.toJson(userSecondaryRolesList));*/
				List<Role> userRoles = userService.getUserRolesForDisplay(name);//assgined apps				
				//Check For duplicates in Roles against primaryRoleName and appending to roles.
				if(primaryRoleName != null && !primaryRoleName.isEmpty()) {
					boolean isPrimaryRoleAdded = false;
					for(Role userRole : userRoles) {
						if(primaryRoleName.equalsIgnoreCase(userRole.getRole())) {
							isPrimaryRoleAdded = true;							
							break;
						}
					}
					if(!isPrimaryRoleAdded) {
						primaryRole = userService.getRole(primaryRoleName);
						if(primaryRole != null) {
							userRoles.add(primaryRole);
						}
					}
				}
				
				if(sessionObj.getAttribute("sso") != null && !primaryRoleName.equalsIgnoreCase(app)   && "true".equalsIgnoreCase(sessionObj.getAttribute("sso").toString())) {
					sessionObj.setAttribute("app", primaryRoleName);
					sessionObj.setAttribute("sso", "false");
					isSSO = true;
				}
				
				boolean appCheck = false;
				for (Role roleForApp : userRoles) {
					if (roleForApp.getName().equalsIgnoreCase(app)) {
						appCheck = true;
						break;
					}
				}
				if (appCheck && !isSSO) {
					userService.updateUserPrimaryRole(name, app);
					role = userService.getTheme(name);
					
					if (role != null) {
						sessionObj.setAttribute("themename",
								role.getThemeName());
						sessionObj.setAttribute("topMenuSettings", role.getTopRightIcon());
						if (role.getMenuOnTop() == 1)
							sessionObj.setAttribute(menuStyle, " menu-on-top");
						else
							sessionObj.setAttribute(menuStyle, " ");
						sessionObj.setAttribute("primaryRole", role.getName());
						sessionObj.setAttribute("appname",
								role.getDisplayName());
						sessionObj.setAttribute("lastUpdatedOn", null);
						if(user.getAdditionalDetails() != null && user.getAdditionalDetails().getLastUpdatedDate() != null) {
							sessionObj.setAttribute("lastUpdatedOn", user.getAdditionalDetails().getLastUpdatedDate());
						}
						sessionObj.setAttribute("NotificationClosedOn", null);
						if(user.getAdditionalDetails() != null && user.getAdditionalDetails().getNotificationCloedDate() != null) {
							sessionObj.setAttribute("NotificationClosedOn", user.getAdditionalDetails().getNotificationCloedDate());
						}
						sessionObj.setAttribute("logo", role.getLogo());
						sessionObj.setAttribute("favicon", role.getFavicon());
						sessionObj.setAttribute("icon", role.getIcon());
						sessionObj.setAttribute("widgetclass",
								role.getWidgetClass());
						sessionObj.setAttribute("widgetcolor",
								role.getWidgetColor());

					}

					userRoleActions = userService.getUserRoleActionList(name);
					String lookUpValues = adminService.getLookUpValueList();
					sessionObj.setAttribute("userroleactions", userRoleActions);
					sessionObj.setAttribute("lookupvalues", lookUpValues);
					List<Role> userRolesForDisplay = userService
							.getUserRolesForDisplay(name);//storing assigned apps
					sessionObj.setAttribute("userrolesfordisplay",
							gson.toJson(userRolesForDisplay));
					
					
					UserAdditionalDetails useradditionalDetails = new UserAdditionalDetails();
					useradditionalDetails.setUserName(user.getUsername());
					userService.updateUserLastLoginDate(useradditionalDetails);
					
					return "/web/index";
				} else if (name != null && "anonymoususer".equals(name)) {
					return "redirect:login?app=" + app;
				} else {
					sessionObj.setAttribute("app", null);
					sessionObj.setAttribute("errorMessage",
							"Access denied for this user!");
					return "redirect:login?denied&app=" + app;
				}
			//}
		} catch (AppException appe) {
			Log.Error(appe);
		} catch (Exception e) {
			Log.Error(e);
		}

		return null;
	}
	
	@RequestMapping("POProfiling")
	public String gePOProfilePage() {
		return "ajax/Profiling/e2e_profiling";
	}

	@RequestMapping("user")
	public String geUserPage() {
		return "user";
	}

	@RequestMapping("admin")
	public String geAdminPage() {
		return "/web/admin";
	}

	@RequestMapping("apps")
	public String geApps() {
		return "/web/apps";
	}
	
	@RequestMapping("SSO")
	public String geSSO() {
		return "/SSO";
	}

	@RequestMapping("403page")
	public String ge403denied() {
		return "redirect:login";
	}
}
