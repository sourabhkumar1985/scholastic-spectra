package com.relevance.prism.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.util.PrismHandler;
import com.relevance.prism.security.ReadLDAPUserAttributes;
import com.relevance.prism.security.UserAttributes;


@Path("/LDAPDetails")
public class LDAPUserDetailsResource extends BaseResource {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getLDAPDetails")
	public String getLDAPDetails(@FormParam("userName") String userName)
			{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		UserAttributes attrs = null;
		Gson gson;
		try {
			attrs = ReadLDAPUserAttributes.getLDAPUserDetails(userName);
			if(attrs != null) {
				gson = new Gson();
				response = gson.toJson(attrs);
			} else {
				response = "{\"isException\" : \"true\", \"customMessage\" : \"No User Found\"}";
			}
			
		} catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}

	