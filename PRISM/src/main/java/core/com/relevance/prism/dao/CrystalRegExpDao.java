package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class CrystalRegExpDao extends BaseDao{
	
	public String insertRegExp(String source, String database, String table, String type, String regex) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Log.Info("Inserting  Values for Crystal Object : " + regex);
			
			connection = dbUtil.getPostGresConnection(database);
			
			stmt = connection.createStatement();
			String insertQuery = "INSERT INTO "+table.split("_")[0]+"_ext_regx"+" (file_name, raw_text, regexp_matches, type_reg) SELECT file_name, raw_text, regexp_matches(raw_text, "+regex+"), '"+type+"' as type_reg from "+table+""; 
			stmt.executeUpdate(insertQuery);
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "Successfully inserted Extracted Text in table";
		
	}

}
