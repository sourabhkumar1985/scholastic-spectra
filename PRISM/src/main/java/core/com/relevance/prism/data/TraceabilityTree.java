package com.relevance.prism.data;

import java.util.List;
import java.util.Map;

public class TraceabilityTree {
	private String key;
	private int level;
	private String parent;
	private Map<String,String> supportingColumns;
	private List<TraceabilityTree> children;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public Map<String, String> getSupportingColumns() {
		return supportingColumns;
	}
	public void setSupportingColumns(Map<String, String> supportingColumns) {
		this.supportingColumns = supportingColumns;
	}
	public List<TraceabilityTree> getChildren() {
		return children;
	}
	public void setChildren(List<TraceabilityTree> children) {
		this.children = children;
	}
}
