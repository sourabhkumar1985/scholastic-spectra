package com.relevance.prism.data;

public class CommentLog {
	
	private String lastread;
	private String commentPanelId;
	public String getLastread() {
		return lastread;
	}
	public void setLastread(String lastread) {
		this.lastread = lastread;
	}
	public String getCommentPanelId() {
		return commentPanelId;
	}
	public void setCommentPanelId(String commentPanelId) {
		this.commentPanelId = commentPanelId;
	}	
}
