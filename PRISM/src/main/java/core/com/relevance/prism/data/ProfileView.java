package com.relevance.prism.data;

public class ProfileView {
	private int id;
	private int profileId;
	private String name;
	private String viewSetting;
	private String viewFilters;
	private String createdBy;
	private String createdOn;
	private boolean isPublic;
	private boolean isDefault;
	private boolean viewType;
	private String filterQuery;
	private String viewConfig;
	
		
	public String getViewConfig() {
		return viewConfig;
	}
	public void setViewConfig(String viewConfig) {
		this.viewConfig = viewConfig;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getProfileId() {
		return profileId;
	}
	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getViewSetting() {
		return viewSetting;
	}
	public void setViewSetting(String viewSetting) {
		this.viewSetting = viewSetting;
	}
	public String getViewFilters() {
		return viewFilters;
	}
	public void setViewFilters(String viewFilters) {
		this.viewFilters = viewFilters;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public boolean isPublic() {
		return isPublic;
	}
	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}
	public boolean isDefault() {
		return isDefault;
	}
	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}
	public boolean isViewType() {
		return viewType;
	}
	public void setViewType(boolean viewType) {
		this.viewType = viewType;
	}
	public String getFilterQuery() {
		return filterQuery;
	}
	public void setFilterQuery(String filterQuery) {
		this.filterQuery = filterQuery;
	}
}
