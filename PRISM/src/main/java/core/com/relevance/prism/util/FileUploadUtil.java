package com.relevance.prism.util;

import java.io.FileInputStream;
import java.io.FileReader;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.hive.ql.parse.HiveParser_FromClauseParser.tableName_return;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.simple.parser.JSONParser;

import com.opencsv.CSVReader;
import com.relevance.prism.data.Audit;

public class FileUploadUtil extends BaseUtil {

	private int updateCount = 0;
	private int insertCount = 0;
	private List<String> primaryKeyPositionList = new ArrayList<>();
	private static final int BATCH_SIZE = 1000;
	private PrismDbUtil dbUtil = null;

	public FileUploadUtil() {
		dbUtil = new PrismDbUtil();
	}



	public String uploadExcelData(String fileName, String db, String table, String columnnames, String username)	throws Exception {
		PreparedStatement ps = null;
		Connection con = null;
		org.json.simple.JSONArray configcolumn = null;
		String message = "{\"message\":\" ";
		try{
			configcolumn = (org.json.simple.JSONArray) new JSONParser().parse(columnnames);
			StringBuilder query = new StringBuilder();
			StringBuilder valuepart =new StringBuilder();
			valuepart.append(" values (");
			query.append("insert into " ).append(table).append("(");
			for(int i=0; i<configcolumn.size(); i++){
				String column = (String) configcolumn.get(i);
				query.append(column).append(",");
				valuepart.append("?,");
			}
			if(table.equals("ngram_filter_synonyms")){
				query.append("user_name,ngram_type,");
				valuepart.append("'").append(username).append("','synonyms',");				
			}
			query.setCharAt(query.length()-1, ')');
			valuepart.setCharAt(valuepart.length()-1, ')');
			query.append(valuepart).append(" ;");					
			
			con = getDBConnection("",db);
			ps = con.prepareStatement(query.toString());
									
			FileInputStream fis = new FileInputStream(fileName);
			// Create Workbook instance for xlsx/xls file input stream
			Workbook workbook = null;
	
			if (fileName.toLowerCase().endsWith("xlsx")) {
				workbook = new XSSFWorkbook(fis);
			} else if (fileName.toLowerCase().endsWith("xls")) {
				workbook = new HSSFWorkbook(fis);
			}
			// Get the number of sheets in the xlsx file
			int numberOfSheets = workbook.getNumberOfSheets();
			// loop through each of the sheets
			boolean noDataFound = true;
			for (int i = 0; i < numberOfSheets; i++) {
				// Get the nth sheet from the workbook
				Sheet sheet = workbook.getSheetAt(i);
				for (Row row : sheet) {
					noDataFound = false;
					int index = 1;
					for(Cell cell : row){
						if(cell!=null){
							String cellValue = "";
							switch (cell.getCellType()) {
							case Cell.CELL_TYPE_STRING:
							    cellValue = cell.getStringCellValue();
							    break;

							case Cell.CELL_TYPE_FORMULA:
							    cellValue = cell.getCellFormula();
							    break;

							case Cell.CELL_TYPE_NUMERIC:							    
							        cellValue = Double.toString(cell.getNumericCellValue());							    
							    break;

							case Cell.CELL_TYPE_BLANK:
							    cellValue = "";
							    break;

							case Cell.CELL_TYPE_BOOLEAN:
							    cellValue = Boolean.toString(cell.getBooleanCellValue());
							    break;
							}
							if(index > configcolumn.size())
				        	{
				        		insertCount=0;
				        		throw new SQLException("column number exceeds in excel sheet");				        		
				        	}
							ps.setString(index++, cellValue);				        					        	
				        }
					}
				        ps.addBatch();
				        insertCount++;
				}
			}
			ps.executeBatch();
			if(noDataFound)
        	{
        		insertCount=0;
        		throw new SQLException("No Data Found");				        		
        	}
		}catch(Exception ex){
			PrismHandler.handleException(ex, true);	
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		Log.Info("File Upload : " + insertCount + "  of Records are inserted in " + table + " table.");
		if (insertCount > 0) {
			message += +insertCount + " Records are Inserted";
		}
		message += ".\"}";
		return message ;
	}
	
	
	
	/**
	 * 
	 * @param fileName
	 * @param env
	 * @param db
	 * @param table
	 * @param primaryKeyName
	 * @return
	 * @throws Exception
	 */
	public String readAndUploadExcelData(String fileName, String env, String db, String table,
			ArrayList<String> primaryKeyNamesList, String configcolumns, String userName, String userDisplayName,
			String defaultValueConfig)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		updateCount = 0;
		insertCount = 0;
		ArrayList<String> columnNamesList = new ArrayList<>();
		HashMap<String, Integer> columnNamesMap = new HashMap<>();
		HashMap<String, Integer> defaultNamesMap = new HashMap<>();
		ArrayList<String> primaryKeysList = new ArrayList<>();
		Connection connection = null;
		Connection queryConnection = null;
		Statement stmt = null;
		String updateQuery = null;
		String insertQuery = null;
		PreparedStatement updateStatement = null;
		PreparedStatement insertStatement = null;
		ResultSet rsPost = null;
		Statement stmtPost = null;
		ResultSet primaryKeysResultSet = null;
		List<String> auditLogsQuery = new ArrayList<>();
		List<Audit> audits = new ArrayList<>(); 
		try {
			String paramCon = null;
			connection = getPostGresDBConnection();
			String dbquery = "select name from app_properties where name ='" + db
					+ "' or (value like '%jdbc%' and value like '%" + db + "%')";
			stmtPost = connection.createStatement();
			rsPost = stmtPost.executeQuery(dbquery);
			if (rsPost != null) {
				while (rsPost.next()) {
					paramCon = rsPost.getString("name");
				}
			}
			connection.close();
			queryConnection = getDBConnection(env, paramCon);
			stmt = queryConnection.createStatement();
			String primaryKeysListQuery = preparePrimaryKeyQuery(primaryKeyNamesList, table);
			primaryKeysResultSet = stmt.executeQuery(primaryKeysListQuery);
			if (primaryKeysResultSet != null) {
				while (primaryKeysResultSet.next()) {
					StringBuilder finalValue = new StringBuilder("");
					for (String primaryKeyName : primaryKeyNamesList) {
						String primaryKeyValue = "" + primaryKeysResultSet.getString(primaryKeyName);
						finalValue.append(primaryKeyValue).append("|");
					}
					primaryKeysList.add(finalValue.toString());
				}
			}
			// Create the input stream from the xlsx/xls file
			FileInputStream fis = new FileInputStream(fileName);
			// Create Workbook instance for xlsx/xls file input stream
			Workbook workbook = null;
			if (fileName.toLowerCase().endsWith("xlsx")) {
				workbook = new XSSFWorkbook(fis);
			} else if (fileName.toLowerCase().endsWith("xls")) {
				workbook = new HSSFWorkbook(fis);
			}
			// Get the number of sheets in the xlsx file
			int numberOfSheets = workbook.getNumberOfSheets();
			// loop through each of the sheets

			for (int i = 0; i < numberOfSheets; i++) {
				// Get the nth sheet from the workbook
				Sheet sheet = workbook.getSheetAt(i);
				// every sheet has rows, iterate over them
				Iterator<Row> rowIterator = sheet.iterator();
				int rowcount = 0;
				while (rowIterator.hasNext()) {
					// String name = "";
					// String shortCode = "";
					// Get the row object
					Row row = rowIterator.next();
					// Every row has columns, get the column iterator and
					// iterate over them
					rowcount = rowcount + 1;
					if (rowcount == 1) {
						connection = getDBConnection(env, paramCon);
						// connection = dbUtil.getPostGresConnection(paramCon);
						createTableForExcel(row, connection, table);// create
																	// the table
																	// if it
																	// does not
																	// exist

						createColumnNamesMapAndListForExcel(connection, table, row, primaryKeyNamesList, columnNamesMap,columnNamesList, configcolumns,defaultValueConfig,defaultNamesMap,env);
						continue;
					}
					if (rowcount == 2) {
						if (env.equalsIgnoreCase("HIVE")) {
							updateQuery = prepareInsertQuery(table, columnNamesList,columnNamesMap, env,defaultNamesMap);
						} else {
							updateQuery = prepareUpdateQuery(table, columnNamesList, primaryKeyNamesList,columnNamesMap);
						}
						insertQuery = prepareInsertQuery(table, columnNamesList,columnNamesMap,env,defaultNamesMap);
						updateStatement = queryConnection.prepareStatement(updateQuery);
						insertStatement = queryConnection.prepareStatement(insertQuery);
					}
					if (row != null) {
						addExcelDataToTable(queryConnection, row, rowcount, table, columnNamesList, primaryKeyNamesList,
								primaryKeyPositionList, primaryKeysList, updateQuery, insertQuery, columnNamesMap,
								updateStatement, insertStatement, env,defaultNamesMap,defaultValueConfig);
						Cell primarykeyCell = row.getCell(Integer.parseInt(primaryKeyPositionList.get(0)),
								Row.CREATE_NULL_AS_BLANK);
						DataFormatter formatter = new DataFormatter(); // creating
																		// formatter
																		// using
																		// the
																		// default
																		// locale
						String primaryKey = formatter.formatCellValue(primarykeyCell);
						String auditLogQuery = null;
						if (!env.equalsIgnoreCase("HIVE")) {
							auditLogQuery = "INSERT INTO audit values ('U', '" + table + "', '" + primaryKey
									+ "', '', '', '', NOW(), '" + userDisplayName + "', '" + userName + "', '');";
							auditLogsQuery.add(auditLogQuery);
						}/*else{
							Audit audit = new Audit();
							audit.setSource(env);
							audit.setType('U');
							audit.setDatabase(db);
							audit.setPrimaryKey(primaryKey);
							audit.setTableName(table);
							audit.setUserId(userName);
							audit.setUserName(userDisplayName);
							audits.add(audit);
						}*/
					}
				} // end of rows iterator
				if (!env.equalsIgnoreCase("HIVE")) {
					updateStatement.executeBatch();
					insertStatement.executeBatch();
					auditLogQueries(auditLogsQuery, env, paramCon);
				}/*else{
					auditLogHiveQueries(audits);
				}*/
			} // end of sheets for loop
				// close file input stream
			fis.close();
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rsPost, stmtPost);
			PrismHandler.handleFinally(queryConnection, primaryKeysResultSet, stmt);
			updateStatement.close();
			insertStatement.close();
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		Log.Info("File Upload : " + insertCount + "  of Records are inserted in " + table + " table.");
		Log.Info("File Upload : " + updateCount + " Number of Records are updated in " + table + " table.");
		String message = "{\"message\":\" ";
		if (insertCount > 0) {
			if(updateCount > 0){
				   message += insertCount + " Records are inserted";
			}
			else{
				   message += insertCount + " Records are inserted.\"";
			}
		}
		if (updateCount > 0) {
			if (insertCount > 0)
				message += " and ";
			message += updateCount + " Records are updated in database.\"";
		}
		message += "}";
		return message;
	}
	/*private void auditLogHiveQueries(List<Audit> audits) throws Exception{
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		StringBuilder query = null;
		for (Audit audit : audits) {
			try {
				con = getDBConnection(audit.getSource(), audit.getDatabase());
				stmt = con.createStatement();
				query = new StringBuilder(
						"INSERT INTO audit(type, tablename, primarykey, fieldname,displayname, oldvalue, newvalue,username,userid,id,updatedate) VALUES ('")
						.append(String.valueOf(audit.getType())).append("','")
						.append(audit.getTableName()).append("','")
						.append(audit.getPrimaryKey()).append("','")
						.append(audit.getFieldName()).append("','")
						.append(audit.getDisplayName()).append("','")
						.append(audit.getOldValue()).append("','")
						.append(audit.getNewValue()).append("','")
						.append(audit.getUserName()).append("','")
						.append(audit.getUserId());
				int maxId = 0;
				rs = stmt.executeQuery("SELECT Max(id) from audit");
				if (rs.next()) {
					maxId = rs.getInt(1);
				}
				rs.close();
				query.append("',").append(maxId + 1);
				DateFormat dateFormat = new SimpleDateFormat(
						"yyyy/MM/dd HH:mm:ss");
				Date date = new Date();
				query.append(",'").append(dateFormat.format(date)).append("')");
				stmt.executeUpdate(query.toString());
			} catch (Exception ex) {
				System.out.println(ex);
			}finally {
				PrismHandler.handleFinally(con, rs, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}*/
	private void auditLogQueries(List<String> queries, String env, String dbName) {
		Connection connection = null;
		Statement insertStatement = null;
		try {
			if (queries == null || queries.isEmpty()) {
				return;
			}
			connection = getDBConnection(env, dbName);
			insertStatement = connection.createStatement();
			for (String query : queries) {
				insertStatement.addBatch(query);
			}
			insertStatement.executeBatch();
			insertStatement.close();
			connection.close();
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}

	/**
	 * 
	 * @param fileName
	 * @param env
	 * @param db
	 * @param table
	 * @param separator
	 * @param primaryKeyName
	 * @return
	 * @throws Exception
	 */
	public String readAndUploadCSVData(String fileName, String env, String db, String table, char separator,
			ArrayList<String> primaryKeyNamesList, String configColumns) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		updateCount = 0;
		insertCount = 0;
		ArrayList<String> columnNamesList = new ArrayList<>();
		HashMap<String, Integer> columnNamesMap = new HashMap<>();
		HashMap<String, Integer> defaultNamesMap = new HashMap<>();
		ArrayList<String> primaryKeysList = new ArrayList<>();
		Connection connection = null;
		Connection queryConnection = null;
		Statement stmt = null;
		String updateQuery = null;
		String insertQuery = null;
		PreparedStatement updateStatement = null;
		PreparedStatement insertStatement = null;
		ResultSet rsPost = null;
		Statement stmtPost = null;
		ResultSet primaryKeysResultSet = null;
		try {
			String paramCon = null;
			connection = getPostGresDBConnection();
			String dbquery = "select name from app_properties where name ='" + db
					+ "' or (value like '%jdbc%' and value like '%" + db + "%')";
			stmtPost = connection.createStatement();
			rsPost = stmtPost.executeQuery(dbquery);
			if (rsPost != null) {
				while (rsPost.next()) {
					paramCon = rsPost.getString("name");
				}
			}
			queryConnection = getDBConnection(env, paramCon);
			stmt = queryConnection.createStatement();
			String primaryKeysListQuery = preparePrimaryKeyQuery(primaryKeyNamesList, table);
			primaryKeysResultSet = stmt.executeQuery(primaryKeysListQuery);
			if (primaryKeysResultSet != null) {
				while (primaryKeysResultSet.next()) {
					StringBuilder finalValue = new StringBuilder("");
					for (String primaryKeyName : primaryKeyNamesList) {
						String primaryKeyValue = "" + primaryKeysResultSet.getString(primaryKeyName);
						finalValue.append(primaryKeyValue).append("|");
					}
					primaryKeysList.add(finalValue.toString());
				}
			}
			// Build reader instance
			// Read data.csv
			// Default seperator is comma
			// Default quote character is double quote
			// Start reading from line number 2 (line numbers start from zero)
			CSVReader reader = new CSVReader(new FileReader(fileName), separator, '"', 0);
			// Read CSV line by line and use the string array as you want
			String[] row;
			int rowcount = 0;
			while ((row = reader.readNext()) != null) {
				rowcount = rowcount + 1;
				if (rowcount == 1) {
					createTableForCSV(row, connection, table);// create table if
																// it does not
																// exist
					connection = dbUtil.getPostGresConnection(paramCon);
					createColumnNamesMapAndListForCSV(connection, table, row, primaryKeyNamesList, columnNamesMap,
							columnNamesList, configColumns);
					continue;
				}
				if (rowcount == 2) {
					updateQuery = prepareUpdateQuery(table, columnNamesList, primaryKeyNamesList,columnNamesMap);
					if (env.equalsIgnoreCase("HIVE")) {
						updateQuery = prepareInsertQuery(table, columnNamesList,columnNamesMap, env,defaultNamesMap);
					}
					insertQuery = prepareInsertQuery(table, columnNamesList,columnNamesMap, env,defaultNamesMap);
					updateStatement = queryConnection.prepareStatement(updateQuery);
					insertStatement = queryConnection.prepareStatement(insertQuery);
				}
				if (row != null) {
					// Verifying the read data here
					addCSVDataToTable(row, columnNamesList, columnNamesMap, queryConnection, rowcount,
							primaryKeyNamesList, primaryKeyPositionList, primaryKeysList, updateQuery, insertQuery,
							updateStatement, insertStatement, env);
				}
			}
			if (!env.equalsIgnoreCase("HIVE")) {
				updateStatement.executeBatch();
				insertStatement.executeBatch();
			}
			if (reader != null) {
				try {
					reader.close();
				} catch (Exception ex) {
					PrismHandler.handleException(ex, true);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rsPost, stmtPost);
			PrismHandler.handleFinally(queryConnection, primaryKeysResultSet, stmt);
			updateStatement.close();
			insertStatement.close();
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		Log.Info("File Upload : " + insertCount + " Number of Records are inserted in " + table + " table.");
		Log.Info("File Upload : " + updateCount + " Number of Records are updated in " + table + " table.");
		return "{\"message\":\" " + insertCount + " Number of Records are inserted and" + updateCount
				+ " Number of Records are updated in " + table + " table.";
	}

	/**
	 * 
	 * @param conn8
	 * @param row
	 * @param rowCount
	 * @param table
	 * @param columnNamesList
	 * @param primaryKeyName
	 * @param primaryKeyPosition
	 * @param primaryKeysList
	 * @param updateQuery
	 * @param insertQuery
	 * @param columnNamesMap
	 * @param updateStatement
	 * @param insertStatement
	 * @throws AppException
	 * @throws SQLException
	 */
	private void addExcelDataToTable(Connection conn, Row row, int rowCount, String table,
			ArrayList<String> columnNamesList, List<String> primaryKeyNamesList, List<String> primaryKeyPositionList,
			ArrayList<String> primaryKeysList, String updateQuery, String insertQuery,
			HashMap<String, Integer> columnNamesMap, PreparedStatement updateStatement,
			PreparedStatement insertStatement, String env,HashMap<String, Integer> defaultNamesMap,String defaultValueConfig) throws Exception {
		try {
			StringBuilder finalValue = new StringBuilder("");
			for (String primaryKeyPosition : primaryKeyPositionList) {
				Cell primarykeyCell = row.getCell(Integer.parseInt(primaryKeyPosition), Row.CREATE_NULL_AS_BLANK);
				String primaryKeyStringValue = null;
				switch (primarykeyCell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					primaryKeyStringValue = primarykeyCell.getStringCellValue().trim();
					finalValue.append(primaryKeyStringValue).append("|");
					break;
				case Cell.CELL_TYPE_NUMERIC:
					primaryKeyStringValue = new DataFormatter().formatCellValue(primarykeyCell);
					finalValue.append(primaryKeyStringValue).append("|");
					break;
				}
			}
			int primaryKeyType = -1;
			boolean primaryKeyIsSet = false;
			if (primaryKeysList.contains(finalValue.toString())) {
				updateCount++;
				int positionOfPrimaryKeyInList = 0;
				StringBuilder updateHiveQuery = null;
				if (env.equalsIgnoreCase("HIVE")) {
					updateHiveQuery = new StringBuilder();
					updateHiveQuery.append(updateQuery);
				}
				for (int j = 0,k=0; j < columnNamesList.size(); j++) {
					if(columnNamesMap.get(columnNamesList.get(j).toLowerCase())== null){
						continue;
					}
					if (primaryKeyPositionList.contains(Integer.toString(j))) {
						positionOfPrimaryKeyInList = primaryKeyPositionList.indexOf(Integer.toString(j));
						primaryKeyType = columnNamesMap.get(columnNamesList.get(j)).intValue();
					}
					Cell cell = row.getCell(j, Row.CREATE_NULL_AS_BLANK);
					String columnValue = null;
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						columnValue = cell.getStringCellValue().trim();
						break;
					case Cell.CELL_TYPE_NUMERIC:
						columnValue = new DataFormatter().formatCellValue(cell);
						break;
					}
					Integer column = columnNamesMap.get(columnNamesList.get(j).toLowerCase());
					if (column == null) {
						continue;
					}
					int columnType = column.intValue();
					if (defaultValueConfig != null && !"".equalsIgnoreCase(defaultValueConfig)) {
						if (!env.equalsIgnoreCase("HIVE")){
							JSONArray arrJSON = new JSONArray(defaultValueConfig);
							for(int dfcount=0;dfcount<arrJSON.length();dfcount++){
								org.json.JSONObject item = arrJSON.getJSONObject(dfcount);
								if(item.getString("key").equalsIgnoreCase(columnNamesList.get(j))){
									columnValue = item.getString("defaultvalue");
								}
					        } 
						}
					}
					switch (columnType) {
					case 12:
						if (env.equalsIgnoreCase("HIVE")) {
							updateHiveQuery.append("'" + columnValue + "'");
						} else {
							updateStatement.setString(k + 1, columnValue);
							if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 12) {
								updateStatement.setString((columnNamesMap.size() + 1 + positionOfPrimaryKeyInList),
										columnValue);
								if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
									primaryKeyIsSet = true;
								}
							}
						}
						break;
					case 2:
						if (env.equalsIgnoreCase("HIVE")) {
							updateHiveQuery.append(E2emfAppUtil.parseInt(columnValue));
						} else {
							updateStatement.setInt(k + 1, E2emfAppUtil.parseInt(columnValue));
							if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 2) {
								updateStatement.setInt((columnNamesMap.size() + 1 + positionOfPrimaryKeyInList),
										E2emfAppUtil.parseInt(columnValue));
								if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
									primaryKeyIsSet = true;
								}
							}
						}
						break;
					case 4:
						if (env.equalsIgnoreCase("HIVE")) {
							updateHiveQuery.append(E2emfAppUtil.parseInt(columnValue));
						} else {
							updateStatement.setInt(k + 1, E2emfAppUtil.parseInt(columnValue));
							if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 4) {
								updateStatement.setInt((columnNamesMap.size() + 1 + positionOfPrimaryKeyInList),
										E2emfAppUtil.parseInt(columnValue));
								if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
									primaryKeyIsSet = true;
								}
							}
						}
						break;
					case 6:
						if (env.equalsIgnoreCase("HIVE")) {
							updateHiveQuery.append(E2emfAppUtil.parseFloat(columnValue));
						} else {
							updateStatement.setFloat(k + 1, E2emfAppUtil.parseFloat(columnValue));
							if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 6) {
								updateStatement.setFloat((columnNamesMap.size() + 1 + positionOfPrimaryKeyInList),
										E2emfAppUtil.parseFloat(columnValue));
								if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
									primaryKeyIsSet = true;
								}
							}
						}
						break;
					case 8:
						if (env.equalsIgnoreCase("HIVE")) {
							updateHiveQuery.append(E2emfAppUtil.parseDouble(columnValue));
						} else {
							updateStatement.setDouble(k + 1, E2emfAppUtil.parseDouble(columnValue));
							if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 8) {
								updateStatement.setDouble((columnNamesMap.size() + 1 + positionOfPrimaryKeyInList),
										E2emfAppUtil.parseDouble(columnValue));
								if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
									primaryKeyIsSet = true;
								}
							}
						}
						break;
					case 91:
						if (env.equalsIgnoreCase("HIVE")) {
							updateHiveQuery.append(E2emfAppUtil.parseDate(columnValue));
						} else {
							updateStatement.setDate(k + 1, E2emfAppUtil.parseDate(columnValue));
							if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 12) {
								updateStatement.setDate((columnNamesMap.size() + 1 + positionOfPrimaryKeyInList),
										E2emfAppUtil.parseDate(columnValue));
								if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
									primaryKeyIsSet = true;
								}
							}
						}
						break;	
					case -7:
						if (env.equalsIgnoreCase("HIVE")) {
							updateHiveQuery.append(columnValue);
							if ("t".equalsIgnoreCase(columnValue) || "true".equalsIgnoreCase(columnValue)) {
								updateHiveQuery.append(Boolean.TRUE);
							} else {
								updateHiveQuery.append(Boolean.FALSE);
							}
						} else {
							if ("t".equalsIgnoreCase(columnValue) || "true".equalsIgnoreCase(columnValue)) {
								updateStatement.setBoolean(k + 1, Boolean.TRUE);
							} else {
								updateStatement.setBoolean(k + 1, Boolean.FALSE);
							}
							if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 12) {
								updateStatement.setBoolean((columnNamesMap.size() + 1 + positionOfPrimaryKeyInList),
										Boolean.TRUE);
								if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
									primaryKeyIsSet = true;
								}
							}
						}
						break;
					case 93:
						Timestamp timeStamp = E2emfAppUtil.parseTimestamp(columnValue);
						if (env.equalsIgnoreCase("HIVE")) {
							updateHiveQuery.append(timeStamp);
						} else {
							updateStatement.setTimestamp(k + 1, timeStamp);
							if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 12) {
								updateStatement.setTimestamp((columnNamesMap.size() + 1 + positionOfPrimaryKeyInList),
										timeStamp);
								if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
									primaryKeyIsSet = true;
								}
							}
						}
						break;
					}
					if (env.equalsIgnoreCase("HIVE") && k < (columnNamesMap.keySet().size() - 1)) {
						updateHiveQuery.append(",");
					}
					k++;
					/*
					 * if(columnType == -7){ updateStatement.setBit(j+1,
					 * columnValue); }else if(columnType == -6){
					 * updateStatement.setTinyInt(j+1, columnValue); } else
					 * if(columnType == -5){ updateStatement.setBigInt(j+1,
					 * columnValue); } else if(columnType == -4){
					 * updateStatement.setLong(j+1, columnValue); } else
					 * if(columnType == -3){ updateStatement.setVarBinary(j+1,
					 * columnValue); } else if(columnType == -2){
					 * updateStatement.setBinary(j+1, columnValue); } else
					 * if(columnType == -1){ updateStatement.setLongVarchar(j+1,
					 * columnValue); } else if(columnType == 0){
					 * updateStatement.setNull(j+1, columnValue); } else
					 * if(columnType == 1){ updateStatement.setChar(j+1,
					 * columnValue); } else if(columnType == 3){
					 * updateStatement.setdecimal(j+1, columnValue); } else
					 * if(columnType == 5){ updateStatement.setSmallInt(j+1,
					 * Integer.parseInt(columnValue)); } else if(columnType ==
					 * 7){ updateStatement.setReal(j+1, columnValue); } else
					 * if(columnType == 92){ updateStatement.setTime(j+1,
					 * columnValue); } else if(columnType == 93){
					 * updateStatement.setTimeStamp(j+1, columnValue);
					 */
				}
				// Log.Info("Update Query : " + updateQuery);
				// System.out.println("Update Query : " +
				// updateQuery.toString());
				if (defaultValueConfig != null && !"".equalsIgnoreCase(defaultValueConfig)) {
					if (env.equalsIgnoreCase("HIVE")){
						JSONArray arrJSON = new JSONArray(defaultValueConfig);
						for(int k=0;k<arrJSON.length();k++){
							org.json.JSONObject item = arrJSON.getJSONObject(k);
							updateHiveQuery.append(",");
							updateHiveQuery.append("'" + item.getString("defaultvalue") +"'");
				        } 
					}
				
				}
				if (env.equalsIgnoreCase("HIVE")) {
					updateHiveQuery.append(")");
					Statement stmt = conn.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
							java.sql.ResultSet.CONCUR_READ_ONLY);
					stmt.executeUpdate(updateHiveQuery.toString());
				} else {
					updateStatement.addBatch();
					if (rowCount % BATCH_SIZE == 0) {
						updateStatement.executeBatch();
					}
				}
			} else {
				// primaryKeysList.add(primaryKeyStringValue);
				StringBuilder insertHiveQuery = null;
				if (env.equalsIgnoreCase("HIVE")) {
					insertHiveQuery = new StringBuilder();
					insertHiveQuery.append(insertQuery);
				}
				insertCount++;
				for (int j = 0,k=0; j < columnNamesList.size(); j++) {
					Cell cell = row.getCell(j, Row.CREATE_NULL_AS_BLANK);
					String columnValue = null;
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						columnValue = cell.getStringCellValue().trim();
						break;
					case Cell.CELL_TYPE_NUMERIC:
						columnValue = new DataFormatter().formatCellValue(cell);
						break;
					}
					Integer column =columnNamesMap.get(columnNamesList.get(j).toLowerCase());
					if (column == null) {
						continue;
					}
					int columnType = column.intValue();
					switch (columnType) {
					case 12:
						if (env.equalsIgnoreCase("HIVE")) {
							insertHiveQuery.append("'" + columnValue + "'");
						} else {
							insertStatement.setString(k + 1, columnValue);
						}
						break;
					case 2:
						if (env.equalsIgnoreCase("HIVE")) {
							insertHiveQuery.append(E2emfAppUtil.parseInt(columnValue));
						} else {
							insertStatement.setInt(k + 1, E2emfAppUtil.parseInt(columnValue));
						}
						break;
					case 4:
						if (env.equalsIgnoreCase("HIVE")) {
							insertHiveQuery.append(E2emfAppUtil.parseInt(columnValue));
						} else {
							insertStatement.setInt(k + 1, E2emfAppUtil.parseInt(columnValue));
						}
						break;
					case 6:
						if (env.equalsIgnoreCase("HIVE")) {
							insertHiveQuery.append(E2emfAppUtil.parseInt(columnValue));
						} else {
							insertStatement.setFloat(k + 1, E2emfAppUtil.parseFloat(columnValue));
						}
						break;
					case 8:
						if (env.equalsIgnoreCase("HIVE")) {
							insertHiveQuery.append(E2emfAppUtil.parseDouble(columnValue));
						} else {
							insertStatement.setDouble(k + 1, E2emfAppUtil.parseDouble(columnValue));
						}
						break;
					case 91:
						if (env.equalsIgnoreCase("HIVE")) {
							insertHiveQuery.append(E2emfAppUtil.parseDate(columnValue));
						} else {
							insertStatement.setDate(k + 1, E2emfAppUtil.parseDate(columnValue));
						}
						break;
					case -5:
						if (env.equalsIgnoreCase("HIVE")) {
							BigInteger bigInt = new BigInteger(columnValue);
							insertHiveQuery.append(bigInt);
						} else {
						//	insertStatement.setBigDecimal(j+1, columnValue);
						}
						break;	
					case -7:
						if (env.equalsIgnoreCase("HIVE")) {
							if ("f".equalsIgnoreCase(columnValue) || "false".equalsIgnoreCase(columnValue)) {
								insertHiveQuery.append(Boolean.FALSE);
							}
							if ("t".equalsIgnoreCase(columnValue) || "true".equalsIgnoreCase(columnValue)) {
								insertHiveQuery.append(Boolean.TRUE);
							}
						} else {
							if ("f".equalsIgnoreCase(columnValue) || "false".equalsIgnoreCase(columnValue)) {
								insertStatement.setBoolean(k + 1, Boolean.FALSE);
							}
							if ("t".equalsIgnoreCase(columnValue) || "true".equalsIgnoreCase(columnValue)) {
								insertStatement.setBoolean(k + 1, Boolean.TRUE);
							}
						}
						break;
					case 93:
						Timestamp timeStamp = E2emfAppUtil.parseTimestamp(columnValue);
						if (env.equalsIgnoreCase("HIVE")) {
							insertHiveQuery.append(timeStamp);
						} else {
							insertStatement.setTimestamp(k + 1, timeStamp);
						}
						break;
					}
					if (env.equalsIgnoreCase("HIVE") && k < (columnNamesMap.keySet().size() - 1)) {
						insertHiveQuery.append(",");
					}
					k++;
					/*
					 * else if(columnType == -7){ insertStatement.setBit(j+1,
					 * columnValue); } else if(columnType == -6){
					 * insertStatement.setTinyInt(j+1, columnValue); } else
					 * if(columnType == -5){ insertStatement.setBigInt(j+1,
					 * columnValue); } else if(columnType == -4){
					 * insertStatement.setLong(j+1, columnValue); } else
					 * if(columnType == -3){ insertStatement.setVarBinary(j+1,
					 * columnValue); } else if(columnType == -2){
					 * insertStatement.setBinary(j+1, columnValue); } else
					 * if(columnType == -1){ insertStatement.setLongVarchar(j+1,
					 * columnValue); } else if(columnType == 0){
					 * insertStatement.setNull(j+1, columnValue); } else
					 * if(columnType == 1){ insertStatement.setChar(j+1,
					 * columnValue); } else if(columnType == 3){
					 * insertStatement.setdecimal(j+1, columnValue); } else
					 * if(columnType == 5){ updateStatement.setSmallInt(j+1,
					 * Integer.parseInt(columnValue)); } else if(columnType ==
					 * 7){ updateStatement.setReal(j+1, columnValue); } else
					 * if(columnType == 92){ insertStatement.setTime(j+1,
					 * columnValue); } else if(columnType == 93){
					 * insertStatement.setTimeStamp(j+1, columnValue);
					 */
				}
				if (defaultValueConfig != null && !"".equalsIgnoreCase(defaultValueConfig)) {
					JSONArray arrJSON = new JSONArray(defaultValueConfig);
				
				for(int k=0;k<arrJSON.length();k++){
					org.json.JSONObject item = arrJSON.getJSONObject(k);
					insertHiveQuery.append(",");
					insertHiveQuery.append("'" + item.getString("defaultvalue") +"'");
		        } 
				}
				/*for(){
					
				}*/
				// Log.Info("Insert query : " + insertQuery.toString());
				// System.out.println("Insert query : " +
				// insertQuery.toString());
				if (env.equalsIgnoreCase("HIVE")) {
					insertHiveQuery.append(")");
					Statement stmt = conn.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
							java.sql.ResultSet.CONCUR_READ_ONLY);
					stmt.executeUpdate(insertHiveQuery.toString());
				} else {
					insertStatement.addBatch();
					if (rowCount % BATCH_SIZE == 0) {
						insertStatement.executeBatch();
					}
				}

			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
	}

	/**
	 * 
	 * @param row
	 * @param columnNamesList
	 * @param columnNamesMap
	 * @param conn
	 * @param rowCount
	 * @param primaryKeyName
	 * @param primaryKeyPosition
	 * @param primaryKeysList
	 * @param updateQuery
	 * @param insertQuery
	 * @param updateStatement
	 * @param insertStatement
	 * @throws AppException
	 * @throws SQLException
	 */
	private void addCSVDataToTable(String[] row, ArrayList<String> columnNamesList,
			HashMap<String, Integer> columnNamesMap, Connection conn, int rowCount, List<String> primaryKeyNamesList,
			List<String> primaryKeyPositionList, ArrayList<String> primaryKeysList, String updateQuery,
			String insertQuery, PreparedStatement updateStatement, PreparedStatement insertStatement, String env)
			throws Exception {
		StringBuilder finalValue = new StringBuilder("");
		for (String primaryKeyPosition : primaryKeyPositionList) {
			String cell = row[Integer.parseInt(primaryKeyPosition)];
			String primaryKeyStringValue = cell.trim();
			finalValue.append(primaryKeyStringValue).append("|");
		}
		int primaryKeyType = -1;
		boolean primaryKeyIsSet = false;
		if (primaryKeysList.contains(finalValue.toString())) {
			updateCount++;
			int positionOfPrimaryKeyInList = 0;
			StringBuilder updateHiveQuery = null;
			if (env.equalsIgnoreCase("HIVE")) {
				updateHiveQuery = new StringBuilder();
				updateHiveQuery.append(updateQuery);
			}
			for (int j = 0; j < columnNamesList.size(); j++) {
				if (primaryKeyPositionList.contains(Integer.toString(j))) {
					positionOfPrimaryKeyInList = primaryKeyPositionList.indexOf(Integer.toString(j));
					primaryKeyType = columnNamesMap.get(columnNamesList.get(j)).intValue();
				}
				String columnValue = row[j];
				int columnType = columnNamesMap.get(columnNamesList.get(j)).intValue();
				if (columnValue == null || columnValue.trim().length() == 0) {
					updateStatement.setNull(j + 1, columnType);
				}
				switch (columnType) {
				case 12:
					if (env.equalsIgnoreCase("HIVE")) {
						updateHiveQuery.append("'" + columnValue + "'");
					} else {
						updateStatement.setString(j + 1, columnValue);
						if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 12) {
							updateStatement.setString((columnNamesList.size() + 1 + positionOfPrimaryKeyInList),
									columnValue);
							if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
								primaryKeyIsSet = true;
							}
						}
					}
					break;
				case 2:
					if (env.equalsIgnoreCase("HIVE")) {
						updateHiveQuery.append(E2emfAppUtil.parseInt(columnValue));
					} else {
						updateStatement.setInt(j + 1, E2emfAppUtil.parseInt(columnValue));
						if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 2) {
							updateStatement.setInt((columnNamesList.size() + 1 + positionOfPrimaryKeyInList),
									E2emfAppUtil.parseInt(columnValue));
							if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
								primaryKeyIsSet = true;
							}
						}
					}
					break;
				case 4:
					if (env.equalsIgnoreCase("HIVE")) {
						updateHiveQuery.append(E2emfAppUtil.parseInt(columnValue));
					} else {
						updateStatement.setInt(j + 1, E2emfAppUtil.parseInt(columnValue));
						if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 4) {
							updateStatement.setInt((columnNamesList.size() + 1 + positionOfPrimaryKeyInList),
									E2emfAppUtil.parseInt(columnValue));
							if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
								primaryKeyIsSet = true;
							}
						}
					}
					break;
				case 6:
					if (env.equalsIgnoreCase("HIVE")) {
						updateHiveQuery.append(E2emfAppUtil.parseFloat(columnValue));
					} else {
						updateStatement.setFloat(j + 1, E2emfAppUtil.parseFloat(columnValue));
						if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 6) {
							updateStatement.setFloat((columnNamesList.size() + 1 + positionOfPrimaryKeyInList),
									E2emfAppUtil.parseFloat(columnValue));
							if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
								primaryKeyIsSet = true;
							}
						}
					}
					break;
				case 8:
					if (env.equalsIgnoreCase("HIVE")) {
						updateHiveQuery.append(E2emfAppUtil.parseDouble(columnValue));
					} else {
						updateStatement.setDouble(j + 1, E2emfAppUtil.parseDouble(columnValue));
						if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 8) {
							updateStatement.setDouble((columnNamesList.size() + 1 + positionOfPrimaryKeyInList),
									E2emfAppUtil.parseDouble(columnValue));
							if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
								primaryKeyIsSet = true;
							}
						}
					}
					break;
				case 91:
					if (env.equalsIgnoreCase("HIVE")) {
						updateHiveQuery.append(E2emfAppUtil.parseDate(columnValue));
					} else {
						updateStatement.setDate(j + 1, E2emfAppUtil.parseDate(columnValue));
						if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 12) {
							updateStatement.setDate((columnNamesList.size() + 1 + positionOfPrimaryKeyInList),
									E2emfAppUtil.parseDate(columnValue));
							if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
								primaryKeyIsSet = true;
							}
						}
					}
				case -7:
					if (env.equalsIgnoreCase("HIVE")) {
						if ("f".equalsIgnoreCase(columnValue) || "false".equalsIgnoreCase(columnValue)) {
							updateHiveQuery.append(Boolean.FALSE);
						}
						if ("t".equalsIgnoreCase(columnValue) || "true".equalsIgnoreCase(columnValue)) {
							updateHiveQuery.append(Boolean.TRUE);
						}
					} else {
						if ("f".equalsIgnoreCase(columnValue) || "false".equalsIgnoreCase(columnValue)) {
							updateStatement.setBoolean(j + 1, Boolean.FALSE);
						}
						if ("t".equalsIgnoreCase(columnValue) || "true".equalsIgnoreCase(columnValue)) {
							updateStatement.setBoolean(j + 1, Boolean.TRUE);
						}
						if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 12) {
							updateStatement.setBoolean((columnNamesList.size() + 1 + positionOfPrimaryKeyInList),
									Boolean.TRUE);
							if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
								primaryKeyIsSet = true;
							}
						}
					}
					break;
				case 93:
					Timestamp timeStamp = E2emfAppUtil.parseTimestamp(columnValue);
					if (env.equalsIgnoreCase("HIVE")) {
						updateHiveQuery.append(timeStamp);
					} else {
						updateStatement.setTimestamp(j + 1, timeStamp);
						if (!primaryKeyIsSet && (primaryKeyType > -1) && primaryKeyType == 12) {
							updateStatement.setTimestamp((columnNamesList.size() + 1 + positionOfPrimaryKeyInList),
									timeStamp);
							if (positionOfPrimaryKeyInList == (primaryKeyPositionList.size() - 1)) {
								primaryKeyIsSet = true;
							}
						}
					}
					break;
				}
				if (env.equalsIgnoreCase("HIVE") && j < (columnNamesList.size() - 1)) {
					updateHiveQuery.append(",");
				}
			}
			/*
			 * if(columnType == -7){ updateStatement.setBit(j+1, columnValue);
			 * }if(columnType == -6){ updateStatement.setTinyInt(j+1,
			 * columnValue); }if(columnType == -5){
			 * updateStatement.setBigInt(j+1, columnValue); }if(columnType ==
			 * -4){ updateStatement.setLong(j+1, columnValue); }if(columnType ==
			 * -3){ updateStatement.setVarBinary(j+1, columnValue);
			 * }if(columnType == -2){ updateStatement.setBinary(j+1,
			 * columnValue); }if(columnType == -1){
			 * updateStatement.setLongVarchar(j+1, columnValue); }if(columnType
			 * == 0){ updateStatement.setNull(j+1, columnValue); }if(columnType
			 * == 1){ updateStatement.setChar(j+1, columnValue);
			 * }/*if(columnType == 3){ updateStatement.setdecimal(j+1,
			 * columnValue); }if(columnType == 5){
			 * updateStatement.setSmallInt(j+1,
			 * E2emfAppUtil.parseInt(columnValue)); }if(columnType == 7){
			 * updateStatement.setReal(j+1, columnValue); }if(columnType == 92){
			 * updateStatement.setTime(j+1, columnValue); }if(columnType == 93){
			 * updateStatement.setTimeStamp(j+1, columnValue); }
			 */
			// Log.Info("Update Query : " + updateQuery);
			// System.out.println("Update Query : " + updateQuery.toString());
			if (env.equalsIgnoreCase("HIVE")) {
				updateHiveQuery.append(")");
				Statement stmt = conn.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				stmt.executeUpdate(updateHiveQuery.toString());
			} else {
				updateStatement.addBatch();
				if (rowCount % BATCH_SIZE == 0) {
					updateStatement.executeBatch();
				}
			}
		} else {
			insertCount++;
			StringBuilder insertHiveQuery = null;
			if (env.equalsIgnoreCase("HIVE")) {
				insertHiveQuery = new StringBuilder();
				insertHiveQuery.append(insertQuery);
			}
			for (int j = 0; j < columnNamesList.size(); j++) {
				String columnValue = row[j];
				int columnType = columnNamesMap.get(columnNamesList.get(j)).intValue();
				switch (columnType) {
				case 12:
					if (env.equalsIgnoreCase("HIVE")) {
						insertHiveQuery.append("'" + columnValue + "'");
					} else {
						insertStatement.setString(j + 1, columnValue);
					}
					break;
				case 2:
					if (env.equalsIgnoreCase("HIVE")) {
						insertHiveQuery.append(E2emfAppUtil.parseInt(columnValue));
					} else {
						insertStatement.setInt(j + 1, E2emfAppUtil.parseInt(columnValue));
					}
					break;
				case 4:
					if (env.equalsIgnoreCase("HIVE")) {
						insertHiveQuery.append(E2emfAppUtil.parseInt(columnValue));
					} else {
						insertStatement.setInt(j + 1, E2emfAppUtil.parseInt(columnValue));
					}
					break;
				case 6:
					if (env.equalsIgnoreCase("HIVE")) {
						insertHiveQuery.append(E2emfAppUtil.parseFloat(columnValue));
					} else {
						insertStatement.setFloat(j + 1, E2emfAppUtil.parseFloat(columnValue));
					}
					break;
				case 8:
					if (env.equalsIgnoreCase("HIVE")) {
						insertHiveQuery.append(E2emfAppUtil.parseDouble(columnValue));
					} else {
						insertStatement.setDouble(j + 1, E2emfAppUtil.parseDouble(columnValue));
					}
					break;
				case 91:
					if (env.equalsIgnoreCase("HIVE")) {
						insertHiveQuery.append(E2emfAppUtil.parseDate(columnValue));
					} else {
						insertStatement.setDate(j + 1, E2emfAppUtil.parseDate(columnValue));
					}
					break;
				case -7:
					if (env.equalsIgnoreCase("HIVE")) {
						if ("f".equalsIgnoreCase(columnValue) || "false".equalsIgnoreCase(columnValue)) {
							insertHiveQuery.append(Boolean.FALSE);
						}
						if ("t".equalsIgnoreCase(columnValue) || "true".equalsIgnoreCase(columnValue)) {
							insertHiveQuery.append(Boolean.TRUE);
						}
					} else {
						if ("f".equalsIgnoreCase(columnValue) || "false".equalsIgnoreCase(columnValue)) {
							insertStatement.setBoolean(j + 1, Boolean.FALSE);
						}
						if ("t".equalsIgnoreCase(columnValue) || "true".equalsIgnoreCase(columnValue)) {
							insertStatement.setBoolean(j + 1, Boolean.TRUE);
						}
					}
					break;
				case 93:
					Timestamp timeStamp = E2emfAppUtil.parseTimestamp(columnValue);
					if (env.equalsIgnoreCase("HIVE")) {
						insertHiveQuery.append(timeStamp);
					} else {
						insertStatement.setTimestamp(j + 1, timeStamp);
					}
					break;
				}
				if (env.equalsIgnoreCase("HIVE") && j < (columnNamesList.size() - 1)) {
					insertHiveQuery.append(",");
				}
				/*
				 * if(columnType == -7){ insertStatement.setBit(j+1,
				 * columnValue); }if(columnType == -6){
				 * insertStatement.setTinyInt(j+1, columnValue); }if(columnType
				 * == -5){ insertStatement.setBigInt(j+1, columnValue);
				 * }if(columnType == -4){ insertStatement.setLong(j+1,
				 * columnValue); }if(columnType == -3){
				 * insertStatement.setVarBinary(j+1, columnValue);
				 * }if(columnType == -2){ insertStatement.setBinary(j+1,
				 * columnValue); }if(columnType == -1){
				 * insertStatement.setLongVarchar(j+1, columnValue);
				 * }if(columnType == 0){ insertStatement.setNull(j+1,
				 * columnValue); }if(columnType == 1){
				 * insertStatement.setChar(j+1, columnValue); }if(columnType ==
				 * 3){ insertStatement.setdecimal(j+1, columnValue);
				 * }if(columnType == 5){ insertStatement.setSmallInt(j+1,
				 * E2emfAppUtil.parseInt(columnValue)); }if(columnType == 7){
				 * insertStatement.setReal(j+1, columnValue); }if(columnType ==
				 * 92){ insertStatement.setTime(j+1, columnValue);
				 * }if(columnType == 93){ insertStatement.setTimeStamp(j+1,
				 * columnValue);
				 */
			}
			// Log.Info("Insert query : " + insertQuery.toString());
			// System.out.println("Insert query : " + insertQuery.toString());
			if (env.equalsIgnoreCase("HIVE")) {
				insertHiveQuery.append(")");
				Statement stmt = conn.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				stmt.executeUpdate(insertHiveQuery.toString());
			} else {
				insertStatement.addBatch();
				if (rowCount % BATCH_SIZE == 0) {
					insertStatement.executeBatch();
				}
			}
		}
	}

	/**
	 * 
	 * @param row
	 * @param connection
	 * @param table
	 * @throws Exception
	 */
	private void createTableForExcel(Row row, Connection connection, String table) throws Exception {
		Statement stmt = connection.createStatement();
		StringBuilder createTableQuery = new StringBuilder("create table " + table + "(");
		DatabaseMetaData dbm = connection.getMetaData();
		ResultSet tables = dbm.getTables(null, null, table, null);
		if (tables.next()) {
			// Table exists
			Log.Info("Table Already Exists");
		} else {
			if (row != null) {
				// Verifying the read data here
				for (int cn = 0; cn < row.getLastCellNum(); cn++) {
					if (cn < (row.getLastCellNum() - 1)) {
						Cell cell = row.getCell(cn, Row.CREATE_NULL_AS_BLANK);
						String cellValue = cell.getStringCellValue().trim();
						String[] nameAndType = cellValue.split(":");
						String columnType;
						if (nameAndType.length == 1)
							columnType = "text";
						else
							columnType = nameAndType[1];
						createTableQuery.append(nameAndType[0] + " " + columnType + ", ");
					} else {
						Cell cell = row.getCell(cn, Row.CREATE_NULL_AS_BLANK);
						String cellValue = cell.getStringCellValue().trim();
						String[] nameAndType = cellValue.split(":");
						String columnType;
						if (nameAndType.length == 1)
							columnType = "text";
						else
							columnType = nameAndType[1];
						createTableQuery.append(nameAndType[0] + " " + columnType + ")");
					}
				}
			}
			Log.Info("Create Table Query " + createTableQuery.toString());
			// System.out.println("Create Table Query " +
			// createTableQuery.toString());
			stmt.executeUpdate(createTableQuery.toString());
		}
		tables.close();
		stmt.close();
		// connection.close();
	}

	/**
	 * 
	 * @param row
	 * @param connection
	 * @param table
	 * @throws Exception
	 */
	private void createTableForCSV(String[] row, Connection connection, String table) throws Exception {
		Statement stmt = connection.createStatement();
		StringBuilder createTableQuery = new StringBuilder("create table " + table + "(");
		DatabaseMetaData dbm = connection.getMetaData();
		ResultSet tables = dbm.getTables(null, null, table, null);
		if (tables.next()) {
			// Table exists
			Log.Info("Table Already Exists");
			// System.out.println("Table Already Exists");
		} else {
			if (row != null) {
				// Verifying the read data here
				for (int cn = 0; cn < row.length; cn++) {
					if (cn < (row.length - 1)) {
						String cellValue = row[cn].trim();
						String[] nameAndType = cellValue.split(":");
						String columnType;
						if (nameAndType.length == 1)
							columnType = "text";
						else
							columnType = nameAndType[1];
						createTableQuery.append(nameAndType[0] + " " + columnType + ", ");
					} else {
						String cellValue = row[cn].trim();
						String[] nameAndType = cellValue.split(":");
						String columnType;
						if (nameAndType.length == 1)
							columnType = "text";
						else
							columnType = nameAndType[1];
						createTableQuery.append(nameAndType[0] + " " + columnType + ")");
					}
				}
			}
			// Table does not exist
			Log.Info("Create Table Query " + createTableQuery.toString());
			// System.out.println("Create Table Query " +
			// createTableQuery.toString());
			stmt.executeUpdate(createTableQuery.toString());
		}
		tables.close();
		stmt.close();
		connection.close();
	}

	/**
	 * 
	 * @param connection
	 * @param table
	 * @param row
	 * @param primaryKeyName
	 * @param columnNamesMap
	 * @param columnNamesList
	 * @throws Exception
	 */
	private void createColumnNamesMapAndListForExcel(Connection connection, String table, Row row,
			ArrayList<String> primaryKeyNamesList, HashMap<String, Integer> columnNamesMap,
			ArrayList<String> columnNamesList, String configcolumns,String defaultValueConfig,
			HashMap<String, Integer> defaultNamesMap,String env) throws Exception {
		DatabaseMetaData dbmd = connection.getMetaData();
		ResultSet columns = dbmd.getColumns(null, null, table, "%");
		org.json.simple.JSONArray configcolumn = null;
		org.json.simple.JSONArray defaultValueCol = null;
		HashMap<String, Integer> columnNames = new HashMap<>();
		HashMap<String, Integer> defaultColumnNames = new HashMap<>();
		if (configcolumns != null && !"".equalsIgnoreCase(configcolumns)) {
			configcolumn = (org.json.simple.JSONArray) new JSONParser().parse(configcolumns);
		}
		if (defaultValueConfig != null && !"".equalsIgnoreCase(defaultValueConfig)) {
			defaultValueCol = (org.json.simple.JSONArray) new JSONParser().parse(defaultValueConfig);
		}
		if (columns != null) {
			while (columns.next()) {
				// get the info from the resultset (eg the java.sql.Types
				// value):
				int dataType = columns.getInt("DATA_TYPE");
				String columnName = columns.getString("COLUMN_NAME");
				if (configcolumn != null && configcolumn.size() > 0) {
					for (int count = 0; count < configcolumn.size(); count++) {
						org.json.simple.JSONObject obj = (org.json.simple.JSONObject) configcolumn.get(count);
						if (columnName.equalsIgnoreCase((String) obj.get("key"))) {
							columnNames.put(columnName,Integer.valueOf(dataType));
							Log.Info("Column Name : " + columnName+ " Column Type : " + dataType);
							System.out.println("Column Name : " + columnName+ " Column Type : " + dataType);
							break;
						}
					}
				}
				if (defaultValueCol != null && defaultValueCol.size() > 0) {
				  for (int count = 0; count < defaultValueCol.size(); count++) {
					org.json.simple.JSONObject obj = (org.json.simple.JSONObject) defaultValueCol.get(count);
					if (columnName.equalsIgnoreCase((String) obj.get("key"))) {
						defaultColumnNames.put(columnName,Integer.valueOf(dataType));
						Log.Info("Column Name : " + columnName+ " Column Type : " + dataType);
						System.out.println("Column Name : " + columnName+ " Column Type : " + dataType);
						break;
					}
				}
			  }
			}
		}
		columns.close();
		for (int cn = 0; cn < row.getLastCellNum(); cn++) {
			Cell cell = row.getCell(cn, Row.CREATE_NULL_AS_BLANK);
			String cellValue = cell.getStringCellValue().trim();
			String[] columnAndType = new String[2];
			if (cellValue.contains(":")) {
				columnAndType = cellValue.split(":");
			} else {
				columnAndType[0] = cellValue;
				columnAndType[1] ="temp";
			}
			columnAndType[0] = getColumnNameFromConfigColumn(configcolumn, columnAndType[0]);
			if (primaryKeyNamesList.contains(columnAndType[0])) {
				primaryKeyPositionList.add("" + cn);
			}
			
			if(columnNames.get(columnAndType[0]) !=null){
				columnNamesMap.put(columnAndType[0], columnNames.get(columnAndType[0]));
			}
			columnNamesList.add(columnAndType[0]);
		}
		if (defaultValueConfig != null && !"".equalsIgnoreCase(defaultValueConfig)) {
			defaultValueCol = (org.json.simple.JSONArray) new JSONParser().parse(defaultValueConfig);
			if (!env.equalsIgnoreCase("HIVE")){
				JSONArray arrJSON = new JSONArray(defaultValueConfig);
				for(int dfcount=0;dfcount<defaultValueCol.size();dfcount++){
					org.json.JSONObject item = arrJSON.getJSONObject(dfcount);
					if(!columnNamesList.contains(item.getString("key"))){
						columnNamesList.add(item.getString("key"));
					}
					if(columnNamesMap.get(item.getString("key")) == null){
						columnNamesMap.put(item.getString("key"),defaultColumnNames.get(item.getString("key")));
						System.out.println("Key: "+item.getString("key") + " & Value: " + defaultColumnNames.get(item.getString("key")));
					}
		        } 
			}
		}
		for(Map.Entry<String,Integer> keyValue : defaultColumnNames.entrySet()){
			defaultNamesMap.put(keyValue.getKey(),  keyValue.getValue());
          System.out.println("Key: "+keyValue.getKey() + " & Value: " + keyValue.getValue());
        } 
	}

	private String getColumnNameFromConfigColumn(org.json.simple.JSONArray configcolumn, String column) {
		String columnName = null;
		if (configcolumn != null && configcolumn.size() > 0) {
			for (int count = 0; count < configcolumn.size(); count++) {
				org.json.simple.JSONObject obj = (org.json.simple.JSONObject) configcolumn.get(count);
				columnName = (String) obj.get("value");
				if (column.equalsIgnoreCase(columnName)) {
					column = (String) obj.get("key");
					break;
				}
			}
		}
		return column;
	}

	private void createColumnNamesMapAndListForCSV(Connection connection, String table, String[] row,
			ArrayList<String> primaryKeyNamesList, HashMap<String, Integer> columnNamesMap,
			ArrayList<String> columnNamesList, String configcolumns) throws Exception {
		DatabaseMetaData dbmd = connection.getMetaData();
		ResultSet columns = dbmd.getColumns(null, null, table, "%");
		if (columns != null) {
			while (columns.next()) {
				// get the info from the resultset (eg the java.sql.Types
				// value):
				int dataType = columns.getInt("DATA_TYPE");
				String columnName = columns.getString("COLUMN_NAME");
				columnNamesMap.put(columnName, Integer.valueOf(dataType));
				Log.Info("Column Name : " + columnName + " Column Type : " + dataType);
				System.out.println("Column Name : " + columnName + " Column Type : " + dataType);
			}
		}
		columns.close();
		org.json.simple.JSONArray configcolumn = null;
		if (configcolumns != null && !"".equalsIgnoreCase(configcolumns)) {
			configcolumn = (org.json.simple.JSONArray) new JSONParser().parse(configcolumns);
		}
		for (int cn = 0; cn < row.length; cn++) {
			String cellValue = row[cn].trim();
			String[] columnAndType = new String[2];
			if (cellValue.contains(":")) {
				columnAndType = cellValue.split(":");
			} else {
				columnAndType[0] = cellValue;
				columnAndType[1] = "temp";
			}
			columnAndType[0] = getColumnNameFromConfigColumn(configcolumn, columnAndType[0]);
			if (primaryKeyNamesList.contains(columnAndType[0])) {
				primaryKeyPositionList.add("" + cn);
			}
			columnNamesList.add(columnAndType[0]);
		}
	}

	/**
	 * 
	 * @param table
	 * @param columnNamesList
	 * @return
	 */
	private String prepareInsertQuery(String table, ArrayList<String> columnNamesList,HashMap<String, Integer> columnNamesMap, String env,HashMap<String, Integer> defaultNamesMap) {
		StringBuilder insertQuery = new StringBuilder("INSERT INTO " + table + "(");
		for (int i = 0,k=0; i < columnNamesList.size(); i++) {
			if(columnNamesMap.get(columnNamesList.get(i))!= null){
				if (k < (columnNamesMap.keySet().size()-1)) {
					String cellValue = columnNamesList.get(i);
					String[] columnAndType = new String[2];
					if (cellValue.contains(":")) {
						columnAndType = cellValue.split(":");
					} else {
						columnAndType[0] = cellValue;
						columnAndType[1] = "temp";
					}
					insertQuery.append(columnAndType[0]);
					insertQuery.append(", ");
				} else {
					String cellValue = columnNamesList.get(i);
					String[] columnAndType = new String[2];
					if (cellValue.contains(":")) {
						columnAndType = cellValue.split(":");
					} else {
						columnAndType[0] = cellValue;
						columnAndType[1] = "temp";
					}
					insertQuery.append(columnAndType[0]);
				}
				k++;
			}
		}
		
		for(Map.Entry<String,Integer> keyValue : defaultNamesMap.entrySet()){
			insertQuery.append(", ");
			insertQuery.append(keyValue.getKey());
        }
		
		insertQuery.append(") VALUES (");
		if (!env.equalsIgnoreCase("HIVE")) {
			for (int j = 0,k=0; j < columnNamesList.size(); j++) {
				if(columnNamesMap.get(columnNamesList.get(j))!= null){
					if (k < (columnNamesMap.keySet().size()-1)) {
						insertQuery.append("?,");
					} else {
						insertQuery.append("?");
					}
					k++;
				}
			}
			insertQuery.append(")");
		}
		return insertQuery.toString();
	}

	/**
	 * 
	 * @param table
	 * @param columnNamesList
	 * @return
	 */
	private String prepareUpdateQuery(String table, ArrayList<String> columnNamesList,
			ArrayList<String> primaryKeyNamesList,HashMap<String, Integer> columnNamesMap) {
		StringBuilder updateQuery = new StringBuilder("UPDATE " + table + " SET ");
		for (int cn = 0,k=0; cn < columnNamesList.size(); cn++) {
			if(columnNamesMap.get(columnNamesList.get(cn))!= null){
				if (k < (columnNamesMap.keySet().size()-1)) {
					String cellValue = columnNamesList.get(cn);
					String[] columnAndType = new String[2];
					if (cellValue.contains(":")) {
						columnAndType = cellValue.split(":");
					} else {
						columnAndType[0] = cellValue;
						columnAndType[1] = "temp";
					}
					updateQuery.append(columnAndType[0] + " = ?, ");
				} else {
					String cellValue = columnNamesList.get(cn);
					String[] columnAndType = new String[2];
					if (cellValue.contains(":")) {
						columnAndType = cellValue.split(":");
					} else {
						columnAndType[0] = cellValue;
						columnAndType[1] = "temp";
					}
					updateQuery.append(columnAndType[0] + " = ? ");
				}
				k++;
			}
		}
		updateQuery.append(" WHERE " + preparePrimaryKeyListQuery(primaryKeyNamesList));
		return updateQuery.toString();
	}

	public String preparePrimaryKeyQuery(List<String> primaryKeyNamesList, String table) {
		StringBuilder selectQuery = new StringBuilder("SELECT ");
		for (int cn = 0; cn < primaryKeyNamesList.size(); cn++) {
			if (cn < (primaryKeyNamesList.size() - 1)) {
				selectQuery.append(primaryKeyNamesList.get(cn) + ", ");
			} else {
				selectQuery.append(primaryKeyNamesList.get(cn));
			}
		}
		selectQuery.append(" FROM " + table);
		return selectQuery.toString();
	}

	public String preparePrimaryKeyListQuery(List<String> primaryKeyNamesList) {
		StringBuilder query = new StringBuilder("");
		for (int cn = 0; cn < primaryKeyNamesList.size(); cn++) {
			if (cn < (primaryKeyNamesList.size() - 1)) {
				query.append(primaryKeyNamesList.get(cn) + " = ? and ");
			} else {
				query.append(primaryKeyNamesList.get(cn) + " = ?");
			}
		}
		return query.toString();
	}

	/**
	 * 
	 * @return
	 * @throws AppException
	 * @throws SQLException
	 */
	private Connection getPostGresDBConnection() throws AppException, SQLException {
		PrismDbUtil dbUtil = new PrismDbUtil();
		Connection connection = dbUtil.getPostGresConnection();
		return connection;
	}

	/**
	 * 
	 * @param env
	 * @param paramCon
	 * @return
	 * @throws AppException
	 * @throws SQLException
	 */
	private Connection getDBConnection(String env, String paramCon) throws AppException, SQLException {
		Connection connection = null;
		PrismDbUtil dbUtil = new PrismDbUtil();
		if (env.equalsIgnoreCase("HIVE")) {
			connection = dbUtil.getHiveConnection(paramCon);

		} else {
			connection = dbUtil.getPostGresConnection(paramCon);

		}
		return connection;
	}
}