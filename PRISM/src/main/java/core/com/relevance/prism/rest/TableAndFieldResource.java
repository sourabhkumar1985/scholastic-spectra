package com.relevance.prism.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.relevance.prism.data.E2emfField;
import com.relevance.prism.data.E2emfTable;
import com.relevance.prism.service.TableAndFieldService;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.DataObfuscator;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

@Path("/tableandfield/")
public class TableAndFieldResource extends BaseResource {
	
	@Context
	ServletContext context;

	public TableAndFieldResource(@Context ServletContext value) {
		this.context = value;
		//System.out.println("Conext:" + this.context);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/fieldreport/")
	public String getFieldReport(
			@FormParam("databasename") String databaseName,
			@FormParam("tablename") String tableName,
			@FormParam("field") String fieldName, 
			@FormParam("groupby") String groupBy, 
			@FormParam("calculation") String calculation
			) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Log.Info("Recieved request for Report for Field : " + fieldName);
		String response = null;
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		
		try{
			TableAndFieldService tableAndFieldService = new TableAndFieldService();
			List<E2emfField> e2emfFileldList = tableAndFieldService.getFieldReport(databaseName, tableName, fieldName, groupBy, calculation);
			response = gson.toJson(e2emfFileldList);
			response = DataObfuscator.obfuscate(response);		
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addfield")
	public String addField(@FormParam("databasename") String databasename,
						@FormParam("tablename") String tablename, 
						@FormParam("fieldname") String fieldname,
						@FormParam("numeric") String numeric){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			TableAndFieldService tableAndFieldService = new TableAndFieldService();
			response = tableAndFieldService.insertField(databasename, tablename, fieldname, numeric);		
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updatefield")
	public String updateField(@FormParam("databasename") String databasename,
							@FormParam("tablename") String tablename, 
							@FormParam("fieldname") String fieldname,
							@FormParam("numeric") String numeric){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			TableAndFieldService tableAndFieldService = new TableAndFieldService();
			response = tableAndFieldService.updateField(databasename, tablename, fieldname, numeric);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deletefield")
	public String deleteField(@FormParam("databasename") String databaseName, @FormParam("tablename") String tableName, @FormParam("fieldname") String fieldName){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			TableAndFieldService tableAndFieldService = new TableAndFieldService();
			response = tableAndFieldService.deleteField(databaseName, tableName, fieldName);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/gettablesmap/")
	public String getTablesMap() {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Log.Info("Recieved request for Tables Map");
		String response = null;
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		
		try{
			TableAndFieldService tableAndFieldService = new TableAndFieldService();
			ArrayList<E2emfTable> e2EmfTableMap = tableAndFieldService.getTablesMap();
			response = gson.toJson(e2EmfTableMap);		
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
