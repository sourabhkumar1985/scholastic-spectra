package com.relevance.prism.service;

//import com.relevance.eemf.data.E2emfMaterialFlowData;
import com.relevance.prism.data.GoodsHistoryList;
import com.relevance.prism.data.MaterialReportList;
import com.relevance.prism.data.E2emfBusinessObject;
import com.relevance.prism.util.AppException;
import com.relevance.prism.data.E2emfView;
/**
 * 
 * E2emf Service Interface 
 *
 */
public interface Service {
	
	public String getJsonObject();
	
	public Object getJsonObject(Object... obj) throws Exception;
	
	public E2emfBusinessObject getBusinessObject() throws Exception;

	public E2emfBusinessObject getBusinessObject(Object... obj) throws Exception;

	public E2emfView getViewObject(Object... obj) throws Exception;
	
	public GoodsHistoryList getGoodsHistoryViewObject(Object... obj) throws Exception;
	
	public E2emfView getViewObject(E2emfBusinessObject businessObj);			
	
	public String generateJsonString(Object... obj)  throws Exception;
	
	public MaterialReportList getMaterialReportViewObject(Object... obj) throws Exception;

	//E2emfMaterialFlowData getE2emfMaterialFlowData(String source) throws AppException;

	public String getActions(Object... obj) throws Exception;

	public String getRoles(Object... obj) throws Exception;

	public String getPrimaryRoles(Object... obj) throws Exception;
	
	public String getSearchSuggestion(Object... obj) throws AppException;
	
}
