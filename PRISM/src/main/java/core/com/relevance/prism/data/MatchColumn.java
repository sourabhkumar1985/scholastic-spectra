package com.relevance.prism.data;


public class MatchColumn {

	public static final int EXACT_MATCH = 1;
	public static final int LEVIN_MATCH = 2;
	
	public static final String COMMON_WORDS = "if,on,the,of";
	private String name;
	private int matchType;
	private int matchPriority;
	private boolean cleanBeforeMatch;
	private boolean exclusiveMatch;//Matched items on the right will not be considered for next cycle match
	private boolean ignoreCommonWords;
	private boolean ignoreGivenWords;
	private String ignoreWords;
	
	
	public MatchColumn()
	{
		cleanBeforeMatch = false;
		exclusiveMatch = false;
	}
	
	public boolean isIgnoreCommonWords() {
		return ignoreCommonWords;
	}

	public void setIgnoreCommonWords(boolean ignoreCommonWords) {
		this.ignoreCommonWords = ignoreCommonWords;
	}

	public boolean isIgnoreGivenWords() {
		return ignoreGivenWords;
	}

	public void setIgnoreGivenWords(boolean ignoreGivenWords) {
		this.ignoreGivenWords = ignoreGivenWords;
	}

	public String getIgnoreWords() {
		return ignoreWords;
	}

	public void setIgnoreWords(String ignoreWords) {
		this.ignoreWords = ignoreWords;
	}

	public boolean isExclusiveMatch() {
		return exclusiveMatch;
	}
	public void setExclusiveMatch(boolean exclusiveMatch) {
		this.exclusiveMatch = exclusiveMatch;
	}
	private DistanceObject defaultDistance;
	
	
	public DistanceObject getDefaultDistance() {
		return defaultDistance;
	}
	public void setDefaultDistance(DistanceObject defaultDistance) {
		this.defaultDistance = defaultDistance;
	}
	public boolean isCleanBeforeMatch() {
		return cleanBeforeMatch;
	}
	public void setCleanBeforeMatch(boolean cleanBeforeMatch) {
		this.cleanBeforeMatch = cleanBeforeMatch;
	}
	public int getMatchPriority() {
		return matchPriority;
	}
	public void setMatchPriority(int matchPriority) {
		this.matchPriority = matchPriority;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMatchType() {
		return matchType;
	}
	public void setMatchType(int matchType) {
		this.matchType = matchType;
	}
	
	
}
