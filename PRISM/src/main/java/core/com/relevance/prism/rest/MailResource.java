package com.relevance.prism.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import com.itextpdf.text.pdf.codec.Base64.InputStream;
import com.relevance.prism.data.EmailNotification;
import com.relevance.prism.service.MailService;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

/**
 * 
 * @author Shawkath Khan
 * @Created_Date Mar 17, 2016
 * @Purpose API resources to send emails from prism with different templates
 * @Modified_By
 * @Modified_Date
 */

@Path("/mail")
public class MailResource extends BaseResource {

	@Context
	ServletContext context;

	/**
	 * @purpose Sends email based on the template name
	 * @return NA
	 * @throws AppException
	 */
   
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/sendmail")
	public String sendMail(@FormParam("datajson") String content, @FormParam("subject") String subject, @FormParam("app") String app) throws AppException {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = "";
		Log.Info("Received sendMail request");
		try {
			MailService mailService = new MailService();
			mailService.sendEmail(content, subject, app, null);
		} catch (Exception e) {
			Log.Error(e);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	/*@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/sendmailwithattachment")
	public String sendMailWithAttachments(@FormParam("datajson") String datajson, @FormParam("recepients") String recepients ) throws AppException {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = "";
		Log.Info("Received sendMail request with attachment");
		try { 
			MailService mailService = new MailService();
			mailService.sendEmailWithAttachment(datajson, "Test Body Content" ,recepients);
		} catch (Exception e) {
			Log.Error(e);
		} 
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}*/

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/sendemailnotifications")
	public String sendMailNotifications(@FormParam("notificationid") String notificationid
			) throws AppException {
		long startTime = System.currentTimeMillis();
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = "";
		Log.Info("Received sendEmail Notifications Request");
		try {
			MailService mailService = new MailService();
			responseMessage = mailService.sendEmailNotifications();
			return responseMessage;
		} catch (Exception e) {
			Log.Error(e);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		long endTime = System.currentTimeMillis();
		Log.Info("Email Notifications are sent successfully, total Time taken is  " +  E2emfAppUtil.getTotalElapsedTime(startTime, endTime) + " seconds");
		return responseMessage;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/saveemailnotification")
	public String saveEmailNotification(@FormParam("notification") String notification,@FormParam("profileName") String profileName,@Context HttpServletRequest req/*@FormParam("notificationname") String notificationname, 
						@FormParam("notificationdist") String notificationdist, 
						@FormParam("profileid") String profileid ,
						
						@FormParam("notificationtext") String notificationtext,
						@FormParam("viewid") String viewid*/
						) throws Exception{
		
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		EmailNotification emailNotification = null;
		String createdBy;
		try{
			HttpSession session = req.getSession(true);
			createdBy = session.getAttribute("userName").toString();
			String appName=session.getAttribute("app").toString();
			Gson gson = new Gson();
			emailNotification = gson.fromJson(notification, EmailNotification.class);
			emailNotification.setCreatedBy(createdBy);
			emailNotification.setAppName(appName);
			String scheme = req.getScheme();             // http
		    String serverName = req.getServerName();     // hostname.com
		    int serverPort = req.getServerPort();        // 80
		    String contextPath = req.getContextPath();   // /mywebapp
		    String servletPath = req.getServletPath();   // /servlet/MyServlet
		    String path = scheme + "://" + serverName + ":" +  serverPort + contextPath  + "/index#" + profileName ;   
		
		MailService mailService = new MailService();
		response = mailService.saveEmailNotification(emailNotification,path);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteemailnotification")
	public String deleteEmailNotification(@FormParam("id") String id) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			MailService mailService = new MailService();
			response = mailService.deleteEmailNotification(id);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getemailnotificationslist")
	public String getEmailNotificationsList(){

		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			MailService mailService = new MailService();
			List<EmailNotification> emailNotificationsList = mailService.getEmailNotificationsList();
			Gson gson = new Gson();
			response = gson.toJson(emailNotificationsList);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON) 
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/sendmailwithattachments")
	public String sendMailWithAttachments(		
			@FormParam("subject") String subject,
			@FormParam("content")  String content ,
			@FormParam("recepients") String recepients,
			@FormParam("files") String files,
			@FormParam("attachments") String genericAttachments
			) throws AppException {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = "";
		Log.Info("Received sendEmail Notifications Request");
		try { 
			List<File> attachedFiles = new ArrayList<File>();
			if(files != null) {
				String[] attachmentFiles=files.split(",");
				for (int idx = 0; idx < attachmentFiles.length; idx++) {
					String fileName = attachmentFiles[0].substring(attachmentFiles[0].lastIndexOf("/")+1);
					URL url=new URL(attachmentFiles[0]) ;
					String filePath=System.getProperty("java.io.tmpdir") +   File.separator + fileName;
					File file = new File(filePath);
					FileUtils.copyURLToFile(url, file);
					attachedFiles.add(file);
				}
			} else if(genericAttachments != null && !"".equalsIgnoreCase(genericAttachments)) {
				String[] attachmentFiles = genericAttachments.split(",");
				for (int idx = 0; idx < attachmentFiles.length; idx++) {
					String fileName = attachmentFiles[idx].substring(attachmentFiles[idx].lastIndexOf("/")+1);
					URL url=new URL(attachmentFiles[idx]) ;
					String filePath=System.getProperty("java.io.tmpdir") +   File.separator + fileName;
					File file = new File(filePath);
					FileUtils.copyURLToFile(url, file);
					attachedFiles.add(file);
				}
			}			
			MailService mailService = new MailService();
			Boolean status = mailService.sendGenericEmailWithAttachment(subject,content, recepients, attachedFiles);
			responseMessage = status.toString();
			return responseMessage;
		} catch (Exception e) {
			Log.Error(e);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	} 
	
	
	//save uploaded file to new location
		private void writeToFile(InputStream uploadedInputStream,
			String uploadedFileLocation) {
			try { 
				OutputStream out = new FileOutputStream(new File(
						uploadedFileLocation));
				int read = 0;
				byte[] bytes = new byte[1024];
				out = new FileOutputStream(new File(uploadedFileLocation));
				while ((read = uploadedInputStream.read(bytes)) != -1) {
					out.write(bytes, 0, read);
				}
				out.flush();
				out.close();
			} catch (IOException e) {
			}
		}
	
	
}
