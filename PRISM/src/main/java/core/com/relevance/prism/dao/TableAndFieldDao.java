package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.relevance.prism.data.E2emfField;
import com.relevance.prism.data.E2emfTable;
import com.relevance.prism.util.PrismHandler;

public class TableAndFieldDao extends BaseDao{
	Connection con = null;
	Statement stmt = null;
	
	public ArrayList<E2emfTable> getTablesMap() throws SQLException, NullPointerException, Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		//Map<String, ArrayList<E2emfField>> tablesMap = new HashMap<String, ArrayList<E2emfField>>();
		//Map<String, ArrayList<E2emfTable>> databaseMap = new HashMap<String, ArrayList<E2emfTable>>();
		List<String> databaseList = new ArrayList<String>();
		ArrayList<E2emfTable> detaildTableList = new ArrayList<E2emfTable>();
		ResultSet rs = null;
		Statement stmt = null;
		try {
			String databaseQuery = "select distinct databasename from hive_tables_fields";
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(databaseQuery);
			if (rs != null) {
				while (rs.next()) {
					databaseList.add(rs.getString("databasename"));
				}
			}
			rs.close();
			if (databaseList != null) {
				Iterator<String> databaseIterator = databaseList.iterator();
				while (databaseIterator.hasNext()) {
					List<String> tableList = new ArrayList<String>();
					String databaseName = databaseIterator.next();
					String tableQuery = "select distinct tablename from hive_tables_fields where databasename = '" + databaseName + "'";
					rs = stmt.executeQuery(tableQuery);
					if (rs != null) {
						while (rs.next()) {
							tableList.add(rs.getString("tablename"));
						}
					}
					Iterator<String> tableIterator = tableList.iterator();
					while(tableIterator.hasNext()){
						E2emfTable e2emfTable = new E2emfTable();
						ArrayList<E2emfField> fields = new ArrayList<E2emfField>();
						rs = null;
						String tableName = tableIterator.next();
						e2emfTable.setDatabaseName(databaseName);
						e2emfTable.setTableName(tableName);
						String fieldQuery = "select fieldname, numeric from hive_tables_fields where tablename = '"	+ tableName + "' and databasename = '" + databaseName + "'";
						rs = stmt.executeQuery(fieldQuery);
						if (rs != null) {
							while (rs.next()) {
								E2emfField e2emfField = new E2emfField();
								e2emfField.setFieldName(rs.getString("fieldname"));
								e2emfField.setNumeric(rs.getString("numeric"));
								fields.add(e2emfField);
							}
							e2emfTable.setFields(fields);	
							detaildTableList.add(e2emfTable);
							//System.out.println("Database Name : " + databaseName + "Table Name : " + tableName);
							//tablesMap.put(tableName, fields);
						}						
					}					
				}
			}
		}catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return detaildTableList;
	}
	
	public String insertField(Object... obj) throws SQLException, NullPointerException, Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String databaseName = null;
		String tableName = null;
		String fieldName = null;
		String numeric = null;
		long max_table_field_id = 0;
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		try {
			databaseName = (String) obj[0];
			tableName = (String) obj[1];
			fieldName = (String) obj[2];
			numeric = (String) obj[3];
			String selectMaxQuery = "select max(table_field_id) from hive_tables_fields";
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(selectMaxQuery);
			if (rs != null) {
				while (rs.next()) {
					max_table_field_id = rs.getLong(1);
				}
			}
			max_table_field_id = max_table_field_id + 1;
			String insertQuery = "insert into hive_tables_fields (table_field_id, databasename, tablename, fieldname, numeric) values("
					+ max_table_field_id
					+ ",'"
					+ databaseName
					+ "','"
					+ tableName
					+ "','"
					+ fieldName 
					+ "',"
					+ numeric 
					+")";
			stmt.executeUpdate(insertQuery);
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\" : \"successfully inserted Field in DB.\"}";
	}
	public String updateField(Object... obj) throws SQLException, NullPointerException, Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String databaseName = null;
		String tableName = null;
		String fieldName = null;
		String numeric = null;
		Connection connection = null;
		Statement stmt = null;
		//}
			try {
				databaseName = (String) obj[0];
				tableName = (String) obj[1];
				fieldName = (String) obj[2];
				numeric = (String) obj[3];
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				String updateQueryForField = "update hive_tables_fields set numeric=" + numeric + " where databasename = '" + databaseName +"' and tablename = '" + tableName + "' and fieldname = '" + fieldName + "'";			
				stmt.executeUpdate(updateQueryForField);			
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
			PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
			return "{\"message\" : \"successfully updated Field in DB.\"}";
	}
	
	public String deleteField(Object... obj) throws SQLException, NullPointerException, Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String databaseName = null;
		String tableName = null;
		String fieldName = null;
		Connection connection = null;
		Statement stmt = null;
			try {
				databaseName = (String) obj[0];
				tableName = (String) obj[1];
				fieldName = (String) obj[2];
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				String tableAndFieldsDeleteQuery = "delete from hive_tables_fields where databasename = '" + databaseName +"' and tablename = '"	+ tableName + "' and fieldname='" + fieldName + "'" ;
				stmt.executeUpdate(tableAndFieldsDeleteQuery);
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
			PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
			return "{\"message\" : \"successfully deleted the Field from DB\"}";
	}
	
	public List<E2emfField> getFieldReport(Object... obj) throws SQLException, NullPointerException, Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String databaseName = null;
		String tableName = null;
		String fieldName = null;
		String groupBy = null;
		String calculation = null;
		String query = null;
		ArrayList<E2emfField> fieldList = new ArrayList<E2emfField>();
		try {
			databaseName = obj[0].toString();
			tableName =  obj[1].toString();
			fieldName = obj[2].toString();
			groupBy = obj[3].toString();
			calculation = obj[4].toString();
			//if(calculation.equalsIgnoreCase("SUM")){
			if(calculation != null && calculation != "" && calculation.length() >0){
				query = "select "+ groupBy + ", " + calculation +"(" + fieldName + ") as aggregatevalue"+ " from "+ tableName +" group by "+ groupBy + " order by " + calculation +"(" + fieldName + ") desc";
				con = dbUtil.getHiveConnection(databaseName);
				stmt = con.createStatement();
				rs = stmt.executeQuery(query);

				if (rs != null) {
					while (rs.next()) {
						E2emfField e2emfField = new E2emfField();
						e2emfField.setFieldName(fieldName);
						String aggregateValue = rs.getString("aggregatevalue");
						e2emfField.setAggregateValue(aggregateValue);			
						String [] groupByArray = groupBy.split(",");
						HashMap<String,String> groupByMap = new HashMap<String,String>();
						for(String groupByString : groupByArray){
							groupByMap.put(groupByString, rs.getString(groupByString));
						}
						e2emfField.setGroupByMap(groupByMap);								
						fieldList.add(e2emfField);
					}					
			  }
			}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());	
		return fieldList;
	}
}
