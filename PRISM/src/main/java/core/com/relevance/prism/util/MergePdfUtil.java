package com.relevance.prism.util;

import java.io.IOException;
import java.sql.SQLException;
import java.io.File;
import java.io.FileOutputStream;
 
import java.net.URL;

import org.apache.commons.io.FileUtils;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
public class MergePdfUtil {
	/** The resulting PDF file. */
   public static final String tDir = System.getProperty("java.io.tmpdir");
   public static final String RESULT = tDir + File.separator + "Smartfind__Multidrawings.pdf";
   public static String merge(String[] urllist)
        throws IOException, DocumentException, SQLException {
        String [] filesList = new String [urllist.length];
        int count = 0 ;
        for(String urlPath : urllist){
			URL url = new URL(urlPath);	            
            //String fileName = urlPath.substring(urlPath.lastIndexOf(File.separator)+1);
			String fileName = urlPath.substring(urlPath.lastIndexOf("/")+1);
            String path = tDir + fileName;
            //File inputFile = new File(filePath);
            File outPutfile = new File(path);
            FileUtils.copyURLToFile(url, outPutfile);
            filesList[count] = outPutfile.getAbsolutePath();
            count++;
		}
        // step 1
        Document document = new Document();
        // step 2
        PdfCopy copy = new PdfCopy(document, new FileOutputStream(RESULT));
        // step 3
        document.open();
        // step 4
        PdfReader reader;
        int n;
        // loop over the documents you want to concatenate
        for (int i = 0; i < filesList.length; i++) {
            reader = new PdfReader(filesList[i]);
            // loop over the pages in that document
            n = reader.getNumberOfPages();
            for (int page = 0; page < n; ) {
                copy.addPage(copy.getImportedPage(reader, ++page));
            }
            copy.freeReader(reader);
            reader.close();
        }
        // step 5
        document.close();
        return RESULT;
    }
}
