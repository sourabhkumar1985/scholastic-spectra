package com.relevance.prism.rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.data.Report;
import com.relevance.prism.service.ReportsService;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

@Path("/report")
public class ReportsResource extends BaseResource {

	@Context
	ServletContext context;

	public ReportsResource(@Context ServletContext value) {
		this.context = value;
		//System.out.println("Conext:" + this.context);
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getreportslist")
	public String getReportsList() {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			ReportsService reportsService = new ReportsService();
			List<Report> reportsList = reportsService.getReportsList();
			Gson gson = new Gson();
			response = gson.toJson(reportsList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addreport")
	public String addReport(@FormParam("name") String name, 
						@FormParam("description") String description, 
						@FormParam("query") String query,
						@FormParam("param") String param,
						@FormParam("source") String source,
						@FormParam("roleid") String roleid,
						@FormParam("dbname") String dbname) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		//String json = null;
		String response = null;
		try{
			ReportsService reportsService = new ReportsService();
			response = reportsService.insertReport(name, description, query, param, source, roleid, dbname);
			//Gson gson = new Gson();
			//json = gson.toJson(user);
			return response;
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deletereport")
	public String deleteReport(@FormParam("id") String id) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			ReportsService reportsService = new ReportsService();
			response = reportsService.deleteReport(id);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updatereport")
	public String updateReport(@FormParam("id") String id,
						@FormParam("name") String name, 
						@FormParam("description") String description, 
						@FormParam("query") String query,
						@FormParam("param") String param,
						@FormParam("source") String source,
						@FormParam("roleid") String roleid,
						@FormParam("dbname") String dbname) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		//String json = null;
		String response = null;
		try{
			ReportsService reportsService = new ReportsService();
			response = reportsService.updateReport(id, name, description, query, param, source, roleid, dbname);
			//Gson gson = new Gson();
			//json = gson.toJson(user);
			return response;
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
