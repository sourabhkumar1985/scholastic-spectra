package com.relevance.prism.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;

import com.relevance.prism.service.HtmlTOPdfService;
import com.relevance.prism.util.PrismHandler;

@Path("/htmToPdf")
public class HtmlToPdfResource extends BaseResource {

	@POST
	@Produces("application/pdf")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getconvertedpdf")

	public Response getHtmlToPdf(@FormParam("html") final String html, @FormParam("css") final String css,
			@FormParam("fileName") final String fileName,@FormParam("height") final String height) throws Exception {

		String errorMsg = "file  not found";
		// FileOutputStream fileOutPutStream = null;
		final File file;
		// StreamingOutput stream=null;
		// String absPath = null;
		ResponseBuilder response = null;
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());

		try {
			System.out.println("inside service");
			HtmlTOPdfService htmltopdfservice = new HtmlTOPdfService();
			System.out.println("calling service");
			file = htmltopdfservice.htmlToPdfService(html, css, fileName,height);
			
			response = Response.ok((Object) file, "application/pdf");
			response.header("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
			response.header("Set-Cookie", "fileDownload=true; path=/");
			
			

		} catch (Exception ex) {
			PrismHandler.handleException(ex);
		} 

		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		if (response != null) {
			return response.build();

		} else {
			return Response.serverError().entity(errorMsg).build();
		}

	}

	@DELETE
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteFile")
	public Response deleteFile(@FormParam("fileName")String fileName) throws Exception {
      System.out.println("inside deleting");
      System.out.println("fileNameis" + fileName);
		//File file = new File(fileName);
		//file.delete(); 
	 //   file = new File(fileName);
	   // file.delete();
Thread.sleep(9000);
      
      File folder = new File(".");
      for (File file : folder.listFiles()) {
       if (file.getName().startsWith(fileName)) {
        file.delete();
       }
      }
      
      
		return Response.ok().build();
	}

}