/**
 * 
 */
package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

/**
 * @author skhan71
 *
 */
public class AppDao extends BaseDao {

	

	public String  getAllowedAppListForUser(String username) throws Exception{		
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		String role = "";
		String response="";
		
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Log.Info("Fetching the list of allowed app list for the user: " + username);
		
		try {
			con = dbUtil.getPostGresConnection();
			if (con != null) {
				stmt = con.createStatement();
				// FETCHING LIST OF ALLOWED ROLES FOR USER
				String queryGetRoles = "SELECT role FROM user_roles WHERE username='"+username+"'";	
				System.out.println(queryGetRoles);
				rs = stmt.executeQuery(queryGetRoles);
				if (rs != null) {
					while (rs.next()) {					
					role +=   rs.getString("role") + ",";
					}
					if (!role.equals(""))
						role=role.substring(0,role.length()-2);
					rs.close();
				}
				// FETCHING LAST USED APP BY THE USER
				String queryGetUsedApp = "select CASE WHEN lastusedapp IS NULL THEN primaryrole ELSE lastusedapp END from users where lower(username) =lower('"+username+"') ";	
				System.out.println(queryGetUsedApp);
				rs = stmt.executeQuery(queryGetUsedApp);
				 String lastUsedApp = "";
				if (rs != null) {
					while (rs.next()) {
						lastUsedApp = rs.getString("role");
					}
				}			
				response="{\"roles\":\"" + role + "\",\"lastusedapp\":\"" + lastUsedApp + "\"}";
			}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		return response;	
	}
	
	public boolean updateLastUsedAppByUser(String userName, String appName) throws Exception{		
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Log.Info("Inserting updateLastUsedAppByUser with params: " + userName +" & " + appName);		
		try {
			con = dbUtil.getPostGresConnection();
			String query = "UPDATE users SET lastusedapp='"+appName+"' WHERE username='"+ userName +"'";		
			if (con != null) {
				stmt = con.createStatement();
				stmt.executeUpdate(query);				
			}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
			return false;
		}
		finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		return true;	
	}
	
	public String  getLastUsedAppOfUser(String username) throws Exception{		
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		String response="";
		
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Log.Info("Fetching the list of allowed app list for the user: " + username);
		
		try {
			con = dbUtil.getPostGresConnection();
			if (con != null) {
				stmt = con.createStatement();

				// FETCHING LAST USED APP BY THE USER
				String queryGetUsedApp = "select CASE WHEN lastusedapp IS NULL THEN primaryrole ELSE lastusedapp END from users where lower(username) =lower('"+username+"') ";	
				System.out.println(queryGetUsedApp);
				rs = stmt.executeQuery(queryGetUsedApp);
				 String lastUsedApp = "";
				if (rs != null) {
					while (rs.next()) {
						lastUsedApp = rs.getString("lastusedapp");
					}
				}			
				response=lastUsedApp;
			}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		return response;	
	}
	
	
}
