package com.relevance.prism.dao;

import info.debatty.java.stringsimilarity.Levenshtein;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.relevance.prism.data.DistanceObject;
import com.relevance.prism.data.MatchColumn;
import com.relevance.prism.data.MatchLink;
import com.relevance.prism.data.Query;
import com.relevance.prism.data.Row;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class ItemMatchingDao extends BaseDao {

	Connection con = null;
	Statement stmt = null;

	String emea_db = null;
	String emea_wrk = null;
	String emea_stg = null;

	final String cleanExpression = "^0+(?!$)";

	public ItemMatchingDao() {
		emea_db = E2emfAppUtil.getAppProperty(E2emfConstants.emeaDB);
	}

	private List<Row> getRecords(Query query, String dbName) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ResultSet rs = null;
		List<Row> rows = new ArrayList<>();
		try {
			con = dbUtil.getHiveConnection(dbName);

			Log.Debug("Connecting to DB..." + dbName);
			stmt = con.createStatement();

			Log.Debug("Executing query..." + query.getQuery());
			rs = stmt.executeQuery(query.getQuery());
			ResultSetMetaData rsmd = rs.getMetaData();

			if (rs != null) {
				while (rs.next()) {
					Row row = new Row();

					if (query.getDefaultValues() != null) {
						row.getRow().putAll(query.getDefaultValues().getRow());
					}

					for (int i = 1; i <= rsmd.getColumnCount(); i++) {
						row.getRow().put(rsmd.getColumnName(i),
								rs.getString(rsmd.getColumnName(i)));
					}

					rows.add(row);
				}
			}

			rs.close();

		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}

		Log.Debug("Query execution completed");
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return rows;

	}

	public List<MatchLink> findMatches(Query srcQuery, String srcDB,
			Query tgtQuery, String tgtDB, List<MatchColumn> matchCols)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Row> srcRows = null;
		List<Row> tgtRows = null;

		Log.Debug("findMatches started...");

		srcRows = getRecords(srcQuery, srcDB);
		tgtRows = getRecords(tgtQuery, tgtDB);

		List<MatchLink> linkList = new ArrayList<>();
		for (Row row : srcRows) {
			MatchLink link = new MatchLink();
			link.setSrcRow(row);
			link.setLinkPriority(0);
			linkList.add(link);
		}

		for (MatchColumn mc : matchCols) {
			Map<String, ArrayList<Row>> tgtKeyMap = null;
			if (mc.getMatchType() == MatchColumn.EXACT_MATCH) {
				tgtKeyMap = getTargetRowMap(tgtRows, mc);
			}

			for (MatchLink link : linkList) {
				if (mc.getMatchPriority() > link.getLinkPriority()) {
					String lookUpValue = link.getSrcRow().getRow()
							.get(mc.getName());
					//  Changes 114 to 119 made by Shawkath against PRZM-480 
					if (lookUpValue != null && lookUpValue.trim().length() > 0) {
						if (mc.isCleanBeforeMatch())
							lookUpValue = lookUpValue.toLowerCase().trim()
									.replaceFirst(cleanExpression, "");
					}
					if (mc.getMatchType() == MatchColumn.EXACT_MATCH
							&& tgtKeyMap.containsKey(lookUpValue)) {
						link.setLinkPriority(mc.getMatchPriority());
						link.setTgtRows(getExactMatchRows(lookUpValue,
								tgtKeyMap, mc));
						link.setDistance(mc.getDefaultDistance());
 
						// Used to identify to mark the matched items in next
						// loop
						if (link.getTgtRows() != null
								&& link.getTgtRows().size() > 0) {
							link.setName(mc.getName());
						}
					} else if (mc.getMatchType() == MatchColumn.LEVIN_MATCH) {
						link.setLinkPriority(mc.getMatchPriority());
						link.setTgtRows(getLevinMatchRows(lookUpValue, tgtRows,
								mc, 7, true));

						if (link.getTgtRows() != null
								&& link.getTgtRows().size() > 0) {
							link.setName(mc.getName());
							link.setDistance(mc.getDefaultDistance());
						} else
							link.setDistance(getDistanceObject(10, 0,
									"No Match", "No Match"));
					} else {
						link.setDistance(getDistanceObject(10, 0, "No Match",
								"No Match"));
					}

				}

			}

			if (mc.isExclusiveMatch())// Mark matched items to not include in
										// next cycle of match
			{
				for (MatchLink link : linkList) {
					if (link.getTgtRows() != null && link.getName() != null
							&& mc.getName() != null
							&& mc.getName().equals(link.getName())) {
						for (Row row : link.getTgtRows()) {
							row.setMatched(true);
						}
					}
				}
			}

		}

		Log.Debug("findMatches completed");
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return linkList;

	}

	private ArrayList<Row> getLevinMatchRows(String lookUpValue,
			List<Row> rows, MatchColumn mc, int threshold, boolean findOneOnly) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Levenshtein levenshtein = new Levenshtein();
		double minDistance = 1000;
		double distance = 0;

		ArrayList<Row> matchList = new ArrayList<>();

		if (mc.isIgnoreCommonWords()) {
			// Remove MatchColumn.COMMON_WORDS from lookup value
		}

		if (mc.isIgnoreGivenWords()) {
			// Remove MatchColumn.COMMON_WORDS from lookup value
		}

		for (Row row : rows) {
			if (row.isMatched()) {
				continue;
			}

			String compareValue = row.getRow().get(mc.getName());
			if (lookUpValue != null && compareValue != null
					&& lookUpValue.trim().length() > 0
					&& compareValue.trim().length() > 0) {
				if (mc.isIgnoreCommonWords()) {
					// Remove MatchColumn.COMMON_WORDS from compareValue
				}

				if (mc.isIgnoreGivenWords()) {
					// Remove MatchColumn.COMMON_WORDS from compareValue
				}
				distance = levenshtein.distance(lookUpValue, compareValue);
			} else {
				continue;
			}

			if (distance <= threshold && distance < lookUpValue.length()
					&& distance < compareValue.length()) {
				if (distance == minDistance && !findOneOnly)// Don't add to the
															// list when only
															// one is to be
															// found
				{
					matchList.add(row);
				} else if (distance < minDistance) {
					minDistance = distance;
					matchList = new ArrayList<>();
					matchList.add(row);
				}
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return matchList;
	}

	private ArrayList<Row> getExactMatchRows(String lookUpValue,
			Map<String, ArrayList<Row>> tgtKeyMap, MatchColumn mc) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		if (mc.isCleanBeforeMatch())
			lookUpValue = lookUpValue.toLowerCase().trim()
					.replaceFirst(cleanExpression, "");

		if (mc.isIgnoreCommonWords()) {
			// Remove MatchColumn.COMMON_WORDS from lookup value
		}

		if (mc.isIgnoreGivenWords()) {
			// Remove MatchColumn.COMMON_WORDS from lookup value
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return tgtKeyMap.get(lookUpValue);
	}

	private HashMap<String, ArrayList<Row>> getTargetRowMap(List<Row> tgtRows,
			MatchColumn mc) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<String, ArrayList<Row>> tgtKeyMap = new HashMap<>();

		for (Row row : tgtRows) {

			if (row.isMatched())// Take only non matched rows for comparison
			{
				continue;
			}

			String lookUpValue = row.getRow().get(mc.getName());

			if (lookUpValue != null && lookUpValue.trim().length() > 0) {
				if (mc.isCleanBeforeMatch())
					lookUpValue = lookUpValue.toLowerCase().trim()
							.replaceFirst(cleanExpression, "");

				if (mc.isIgnoreCommonWords()) {
					// Remove MatchColumn.COMMON_WORDS from lookup value
				}

				if (mc.isIgnoreGivenWords()) {
					// Remove MatchColumn.COMMON_WORDS from lookup value
				}

				if (tgtKeyMap.containsKey(lookUpValue)) {
					ArrayList<Row> subRows = tgtKeyMap.get(lookUpValue);
					subRows.add(row);
				} else {
					ArrayList<Row> subRows = new ArrayList<>();
					subRows.add(row);
					tgtKeyMap.put(lookUpValue, subRows);
				}
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return tgtKeyMap;
	}

	public static DistanceObject getDistanceObject(int type, double distance,
			String typeDesc, String categoryDesc) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DistanceObject distanceObject = new DistanceObject();
		distanceObject.setType(type);
		distanceObject.setTypeDesc(typeDesc);
		distanceObject.setTypeCategory(categoryDesc);

		if (type == 1) {
			distanceObject.setWeightage(100);
		} else if (type == 2) {
			distanceObject.setWeightage(70);
		} else if (type == 3) {
			distanceObject.setWeightage(90);
		} else if (type == 4) {
			distanceObject.setDistance(distance);
			distanceObject.setWeightage(40);
		} else if (type == 10) {
			distanceObject.setWeightage(0);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return distanceObject;
	};

}
