package com.relevance.prism.data;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Manikantan
 * E2emf DataAnalyserView 
 *
 */
public class DataAnalyserView implements E2emfView {
	
	JSONArray jsonArray;
	JSONObject jsonDescriptionArray;

	public JSONObject getJsonDescriptionArray() {
		return jsonDescriptionArray;
	}

	public void setJsonDescriptionArray(JSONObject jsonDescriptionArray) {
		this.jsonDescriptionArray = jsonDescriptionArray;
	}

	public JSONArray getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}

	
}
