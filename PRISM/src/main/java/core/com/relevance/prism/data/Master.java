package com.relevance.prism.data;

import java.util.ArrayList;
import java.util.List;
/**
 * 
 * E2emf Master Business Object  
 *
 */
public class Master extends E2emfBusinessObject {

	List<MasterItem> masterPojoList = new ArrayList<MasterItem>();

	public List<MasterItem> getMasterDataList() {
		return this.masterPojoList;
	}

	public void setMasterDataList(List<MasterItem> masterPojoList) {
		this.masterPojoList = masterPojoList;
	}

}
