package com.relevance.prism.data;

import java.util.List;


/**
 * @author Saurabh
 * Column Information Properties Pojo Object  
 */
public class ColumnInformation {
	
	private int noOfRecords;
	private int distinct;
	private int min;
	private int max;
	private double avg;
	private int median;
	private double stdDeviation;
	private boolean isNumaric;
	
	List<ColumnDetail> columnDetails;
	
	public List<ColumnDetail> getColumnDetails() {
		return columnDetails;
	}
	public void setColumnDetail(List<ColumnDetail> columnDetails) {
		this.columnDetails = columnDetails;
	}
	public double getStdDeviation() {
		return stdDeviation;
	}
	public void setStdDeviation(double stdDeviation) {
		this.stdDeviation = stdDeviation;
	}
	public int getnoOfRecords() {
		return noOfRecords;
	}
	public void setnoOfRecords(int noOfRecords) {
		this.noOfRecords = noOfRecords;
	}
	public int getDistinctValue() {
		return distinct;
	}
	public void setDistinctValue(int distinct) {
		this.distinct = distinct;
	}
	public int getMinValue() {
		return min;
	}
	public void setMinValue(int min) {
		this.min = min;
	}
	public int getMaxValue() {
		return max;
	}
	public void setMaxValue(int max) {
		this.max = max;
	}
	
	public double getAvgValue() {
		return avg;
	}
	public void setAvgValue(double avg) {
		this.avg = avg;
	}
	public int getMedianValue() {
		return median;
	}
	public void setMedianValue(int median) {
		this.median = median;
	}
	public boolean isNumaric() {
		return isNumaric;
	}
	public void setNumaric(boolean isNumaric) {
		this.isNumaric = isNumaric;
	}
}
