package com.relevance.prism.dao;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.CDL;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.opencsv.CSVReader;
import com.relevance.prism.data.EmailNotification;
import com.relevance.prism.data.Mail;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismDbUtil;
import com.relevance.prism.util.PrismHandler;

/**
 * @author Shawkath Khan
 * @CreatedOn Mar 15, 2016
 * @Purpose Dao for Mail. Converts Json into html using Mustache library & sends
 *          email. Source URL: https://github.com/spullara/mustache.java
 * @ModifiedBy
 * @ModifiedDate
 * 
 */

public class MailDao extends BaseDao {

	public Boolean sendEmailWithTemplate(String jsonMailContent, String templateName, String subject)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		PrismDbUtil dbUtil = new PrismDbUtil();
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			Log.Info("sendEmail() method called on MailerDao");
			String query = "select * from mail where template_name='"
					+ templateName + "'";
			con = dbUtil.getPostGresConnection();
			Mail mailObj = new Mail();
			if (con != null) {
				stmt = con.createStatement();
				rs = stmt.executeQuery(query);
				if (rs != null) {
					while (rs.next()) {
						mailObj.setUsername(rs.getString("username"));
						mailObj.setPassword(rs.getString("password"));
						mailObj.setSmptHost(rs.getString("smtp_host"));
						mailObj.setSmtpPort(rs.getString("smtp_port"));
						mailObj.setFromEmail(rs.getString("fromemail"));
						String toEmails = rs.getString("toemails");
						String[] strArrToEmails = toEmails.split(",");
						List<String> lstToEmails = new ArrayList<>(
								Arrays.asList(strArrToEmails));
						mailObj.setToEmails(lstToEmails);
						String ccEmails = rs.getString("cclist");
						String[] strArrCcEmails = ccEmails.split(",");
						List<String> lstCcEmails = new ArrayList<>(
								Arrays.asList(strArrCcEmails));
						mailObj.setCcEmails(lstCcEmails);
						String bccEmails = rs.getString("bcclist");
						String[] strArrBccEmails = bccEmails.split(",");
						List<String> lstBccEmails = new ArrayList<>(
								Arrays.asList(strArrBccEmails));
						mailObj.setBccEmails(lstBccEmails);
						mailObj.setTemplateContent(rs
								.getString("template_content"));
					}
				}
			}
			// Reading template, email properties from database
			String output = convertJsonTemplateToHtml(jsonMailContent,
					mailObj.getTemplateContent());
			triggerMail(mailObj, output, subject);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return true;
	}

	public Boolean sendEmail(String content, String subject, String app, File file)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		PrismDbUtil dbUtil = new PrismDbUtil();
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			Log.Info("sendEmail() method called on MailerDao");
			con = dbUtil.getPostGresConnection();
			Mail mailObj = new Mail();
			if (con != null) {
				stmt = con.createStatement();
				String query = "SELECT * FROM mail where template_name = '"+ app +"'"; 
				rs = stmt.executeQuery(query);
				if (rs != null) {
					while (rs.next()) {
						mailObj.setUsername(rs.getString("username"));
						mailObj.setPassword(rs.getString("password"));
						mailObj.setSmptHost(rs.getString("smtp_host"));
						mailObj.setSmtpAuth(rs.getString("smtp_auth"));
						mailObj.setSmtpPort(rs.getString("smtp_port"));
						mailObj.setFromEmail(rs.getString("fromemail"));
						String toEmails = rs.getString("toemails");
						String[] strArrToEmails = toEmails.split(",");
						List<String> lstToEmails = new ArrayList<>(
								Arrays.asList(strArrToEmails));
						mailObj.setToEmails(lstToEmails);
						String ccEmails = rs.getString("cclist");
						String[] strArrCcEmails = ccEmails.split(",");
						List<String> lstCcEmails = new ArrayList<>(
								Arrays.asList(strArrCcEmails));
						mailObj.setCcEmails(lstCcEmails);
						String bccEmails = rs.getString("bcclist");
						String[] strArrBccEmails = bccEmails.split(",");
						List<String> lstBccEmails = new ArrayList<>(
								Arrays.asList(strArrBccEmails));
						mailObj.setBccEmails(lstBccEmails);
						mailObj.setTemplateContent(rs
								.getString("template_content"));
					}
				}
			}
			triggerMail(mailObj, content, subject, file);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return true;
	}
	
	public Boolean sendEmailWithAttachment(File file, String content, String subject, String emailRecepients)
			throws Exception { 

		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		PrismDbUtil dbUtil = new PrismDbUtil();
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			Log.Info("sendEmail() method called on MailerDao");
			con = dbUtil.getPostGresConnection();
			Mail mailObj = new Mail();
			if (con != null) {
				stmt = con.createStatement();
				String query = "SELECT * FROM mail where template_name = 'DEFAULT'"; 
				rs = stmt.executeQuery(query);
				while (rs.next()) {
					mailObj.setUsername(rs.getString("username"));
					mailObj.setPassword(rs.getString("password"));
					mailObj.setSmptHost(rs.getString("smtp_host"));
					mailObj.setSmtpPort(rs.getString("smtp_port"));
					mailObj.setSmtpAuth(rs.getString("smtp_auth"));						
					mailObj.setFromEmail(rs.getString("fromemail"));
					String toEmails = rs.getString("toemails");
					String[] strArrToEmails = toEmails.split(",");
					List<String> lstToEmails = new ArrayList<>(
							Arrays.asList(strArrToEmails));
					mailObj.setToEmails(lstToEmails);
					String ccEmails = rs.getString("cclist");
					String[] strArrCcEmails = ccEmails.split(",");
					List<String> lstCcEmails = new ArrayList<>(
							Arrays.asList(strArrCcEmails));
					mailObj.setCcEmails(lstCcEmails);
					String bccEmails = emailRecepients;
					String[] strArrBccEmails = bccEmails.split(",");
					List<String> lstBccEmails = new ArrayList<>(
							Arrays.asList(strArrBccEmails));
					mailObj.setBccEmails(lstBccEmails);						
				}
			}
			triggerMail(mailObj, content, subject, file);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return true;
	}
	
	public String convertJsonTemplateToHtml(String json, String template)
			throws Exception {
		String output = "";
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
			MustacheFactory mf = new DefaultMustacheFactory();
			Mustache mustache = mf.compile(new StringReader(template),
					"template");
			StringWriter outputWriter = new StringWriter();
			JSONParser parser = new JSONParser();
			JSONObject jsonObj = (JSONObject) parser.parse(json);
			mustache.execute(outputWriter, toMap(jsonObj));
			output = outputWriter.toString();
		} catch (Exception ex) {
			Log.Error("Error converting Json to html using mustache");
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return output;
	}

	@SuppressWarnings("rawtypes")
	public Map<String, Object> toMap(JSONObject object) throws Exception {
		@SuppressWarnings("unchecked")
		Map<String, Object> map = new HashMap();
		Iterator keys = object.keySet().iterator();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			map.put(key, fromJson(object.get(key)));
		}
		return map;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List toList(JSONArray array) throws Exception {
		
		List list = new ArrayList();
		for (int i = 0; i < array.size(); i++) {
			list.add(fromJson(array.get(i)));
		}
		return list;
	}

	private Object fromJson(Object json) throws Exception {
		if (json instanceof JSONObject) {
			return toMap((JSONObject) json);
		} else if (json instanceof JSONArray) {
			return toList((JSONArray) json);
		} else {
			return json;
		}
	}

	public void triggerMail(Mail mailObj, String mailContent, String subject) {

		Session sessionSMTP = null;
		try {
			final String username = mailObj.getUsername();
			final String password = mailObj.getPassword();
			final String smtpHost = mailObj.getSmptHost();
			final String smtpPort = mailObj.getSmtpPort();
			final String smtpAuth = mailObj.getSmtpAuth();
			final String fromEmail = mailObj.getFromEmail();
			final List<String> lstToEmails = mailObj.getToEmails();
			final List<String> lstCcEmails = mailObj.getCcEmails();
			final List<String> lstBccEmails = mailObj.getBccEmails();
			Properties smtpProp = System.getProperties();
				smtpProp.put("mail.smtp.host", smtpHost);
				smtpProp.put("mail.smtp.port", smtpPort);
				if ("true".equalsIgnoreCase(smtpAuth)) {
					smtpProp.put("mail.smtp.auth", "true");
					smtpProp.put("mail.smtp.user", username);
					smtpProp.put("mail.smtp.password", password);
					smtpProp.put("mail.smtp.starttls.enable", "true");
					sessionSMTP = Session.getInstance(smtpProp,
							new javax.mail.Authenticator() {
								protected PasswordAuthentication getPasswordAuthentication() {
									return new PasswordAuthentication(username,
											password);
								}
							});
				} else {
					sessionSMTP = Session.getInstance(smtpProp);
				}
				
			MimeMessage message = new MimeMessage(sessionSMTP);
			message.setFrom(new InternetAddress(fromEmail));
			for (String toEmail : lstToEmails) {
				message.addRecipient(Message.RecipientType.TO,
						new InternetAddress(toEmail));
			}
			for (String ccEmail : lstCcEmails) {
				message.addRecipient(Message.RecipientType.CC,
						new InternetAddress(ccEmail));
			}
			for (String bccEmail : lstBccEmails) {
				message.addRecipient(Message.RecipientType.BCC,
						new InternetAddress(bccEmail));
			}
			message.setSubject(subject);
			message.setContent(mailContent, "text/html; charset=ISO-8859-1");
			Transport transport = sessionSMTP.getTransport("smtp");
			transport.connect();
			transport.send(message);
		} catch (Exception e) {			
			Log.Error(e.getMessage());
			PrismHandler.handleException(e);
		} 
	}
	
	public void triggerMail(Mail mailObj, String content, String subject, File file) {

		Session sessionSMTP = null;
		final String username = mailObj.getUsername();
		final String password = mailObj.getPassword();
		final String smtpHost = mailObj.getSmptHost();
		final String smtpPort = mailObj.getSmtpPort();
		final String smtpAuth = mailObj.getSmtpAuth();
		final String fromEmail = mailObj.getFromEmail();
		final List<String> lstToEmails = mailObj.getToEmails();
		final List<String> lstCcEmails = mailObj.getCcEmails();
		final List<String> lstBccEmails = mailObj.getBccEmails();
		Properties smtpProp = System.getProperties();
		try {			
			smtpProp.put("mail.smtp.host", smtpHost);
			smtpProp.put("mail.smtp.port", smtpPort);
			if ("true".equalsIgnoreCase(smtpAuth)) {
				smtpProp.put("mail.smtp.auth", "true");
				smtpProp.put("mail.smtp.user", username);
				smtpProp.put("mail.smtp.password", password);
				smtpProp.put("mail.smtp.starttls.enable", "true");
				sessionSMTP = Session.getInstance(smtpProp,
						new javax.mail.Authenticator() {
							protected PasswordAuthentication getPasswordAuthentication() {
								return new PasswordAuthentication(username,
										password);
							}
						});
				sessionSMTP.setDebug(true);
			} else {				
				smtpProp.put("mail.smtp.auth", "false");
				smtpProp.put("mail.smtp.ssl.trust", smtpHost);
				sessionSMTP = Session.getInstance(smtpProp);
				sessionSMTP.setDebug(true); 
			}  
			
			Message message = new MimeMessage(sessionSMTP);
			message.setFrom(new InternetAddress(fromEmail));
			for (String toEmail : lstToEmails) {
				message.addRecipient(Message.RecipientType.TO,
						new InternetAddress(toEmail));
			}
			for (String ccEmail : lstCcEmails) {
				message.addRecipient(Message.RecipientType.CC,
						new InternetAddress(ccEmail));
			}
		for (String bccEmail : lstBccEmails) {
				message.addRecipient(Message.RecipientType.BCC,
						new InternetAddress(bccEmail));
			}
			message.setSubject(subject);
			message.setContent(content, "text/html; charset=ISO-8859-1");
			if(file != null){
				javax.mail.Multipart multipart = new MimeMultipart();
				MimeBodyPart attachFilePart = new MimeBodyPart();
				String filecontent = FileUtils.readFileToString(file);
				if(file.getName().endsWith("csv")){
					attachFilePart.setDataHandler(new DataHandler(new ByteArrayDataSource(filecontent.getBytes(),"text/csv")));
				}else if(file.getName().endsWith("xls") || file.getName().endsWith("xlsx")){
					attachFilePart.setHeader("Content-Type", " application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				}
				attachFilePart.setFileName(file.getName());
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(content, "text/html"); 
				multipart.addBodyPart(messageBodyPart);
				multipart.addBodyPart(attachFilePart);
				message.setContent(multipart);	
			}
			Transport transport = sessionSMTP.getTransport("smtp");
			transport.connect();
			transport.send(message);
		} catch (Exception e) {
			Log.Error(e.getMessage());
			PrismHandler.handleException(e);
		}

	}
	
	/*public void triggerGenericMail(
			Mail mailObj, 
			String subject,
			String mailContent, 
			String fromEmail, 
			String toEmailList,
			String ccEmailList, 
			String bccEmailList,
			String content, List<File> files) {
		Session sessionSMTP = null;
		try {
			final String username = mailObj.getUsername();
			final String password = mailObj.getPassword();
			final String smtpHost = mailObj.getSmptHost();
			final String smtpPort = mailObj.getSmtpPort();
			final String smtpAuth = mailObj.getSmtpAuth();
			MimeMessage message = new MimeMessage(sessionSMTP);
			message.setFrom(new InternetAddress(fromEmail));

			if(toEmailList.contains(",")){
				final String [] lstToEmails = toEmailList.split(",");
				for (String toEmail : lstToEmails) {
					message.addRecipient(Message.RecipientType.TO,
							new InternetAddress(toEmail));
				}
			}
			else if(toEmailList.contains(";")){
				final String [] lstToEmails = toEmailList.split(";");
				for (String toEmail : lstToEmails) {
					message.addRecipient(Message.RecipientType.TO,
							new InternetAddress(toEmail));
				}
			}
			if(ccEmailList.contains(",")){
				final String [] lstCcEmails = ccEmailList.split(",");
				for (String ccEmail : lstCcEmails) {
					message.addRecipient(Message.RecipientType.CC,
							new InternetAddress(ccEmail));
				}
			}
			else if(ccEmailList.contains(";")){
				final String [] lstCcEmails = ccEmailList.split(";");
				for (String ccEmail : lstCcEmails) {
					message.addRecipient(Message.RecipientType.CC,
							new InternetAddress(ccEmail));
				}
			}
			if(bccEmailList.contains(",")){
				final String [] lstBccEmails = bccEmailList.split(",");
				for (String bccEmail : lstBccEmails) {
					message.addRecipient(Message.RecipientType.BCC,
							new InternetAddress(bccEmail));
				}
			}
			else if(bccEmailList.contains(";")){
				final String [] lstBccEmails = bccEmailList.split(";");
				for (String bccEmail : lstBccEmails) {
					message.addRecipient(Message.RecipientType.BCC,
							new InternetAddress(bccEmail));
				}
			}			
			Properties smtpProp = System.getProperties();
				smtpProp.put("mail.smtp.host", smtpHost);
				smtpProp.put("mail.smtp.port", smtpPort);
				if ("true".equalsIgnoreCase(smtpAuth)) {
					smtpProp.put("mail.smtp.auth", "true");
					smtpProp.put("mail.smtp.user", username);
					smtpProp.put("mail.smtp.password", password);
					smtpProp.put("mail.smtp.starttls.enable", "true");
					sessionSMTP = Session.getInstance(smtpProp,
							new javax.mail.Authenticator() {
								protected PasswordAuthentication getPasswordAuthentication() {
									return new PasswordAuthentication(username,
											password);
								}
							});
				} else {
					sessionSMTP = Session.getInstance(smtpProp);
				}				
			message.setSubject(subject);
			message.setContent(mailContent, "text/html; charset=ISO-8859-1");
			Transport transport = sessionSMTP.getTransport("smtp");
			transport.connect();
			transport.send(message);
		} catch (Exception e) {
			Log.Error(e.getMessage());
			PrismHandler.handleException(e);
		} 
	}*/

	
	
	public File convertJsonToCsv(org.json.JSONArray dataJson, String fileName) {
		
		try {
			String tempDir = System.getProperty("java.io.tmpdir");
			File file = new File(tempDir + File.separator + fileName + ".csv");
			String csv = CDL.toString(dataJson);
			FileUtils.writeStringToFile(file, csv, "UTF-8");
			return file;
		} catch (Exception ex) {
			PrismHandler.handleException(ex);
			return null;
		}		
	}
	
	
	
	public File convertJsonToXlsx(org.json.JSONArray dataJson, String fileName) {		
		try {
			String tempDir = System.getProperty("java.io.tmpdir");
			File xlsFile = new File(tempDir + File.separator + fileName + ".xlsx");
			String jsonStr = CDL.toString(dataJson);			
			FileOutputStream fileOut = new FileOutputStream(xlsFile);
			InputStream is = new ByteArrayInputStream(jsonStr.getBytes());
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			CSVReader reader = null;
	        try {
	        	final XSSFWorkbook hwb=new XSSFWorkbook();
				XSSFSheet  sheet =  hwb.createSheet(fileName); 
	            reader = new CSVReader(br);
	            String[] line;
	            int rowIdx=0;
	        	CellStyle style = hwb.createCellStyle();
				style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
				style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	            while ((line = reader.readNext()) != null) {
	            	XSSFRow xlsrow = sheet.createRow(0 + rowIdx);
	            	for(int colIdx=0;colIdx<line.length;colIdx++){
	            		XSSFCell cell = xlsrow.createCell(colIdx);
	            		String data = line[colIdx];                
	            		cell.setCellValue(data);	            		
	            		if(rowIdx == 0){	        			
	        				cell.setCellStyle(style);
	            		}
	            	}
	            	rowIdx++;
	            }
	            hwb.write(fileOut);	
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {				
				fileOut.close();
				if (reader != null) reader.close();
			}					
			//FileUtils.writeStringToFile(file, csv, "UTF-8");
			return xlsFile;
		} catch (Exception ex) {
			PrismHandler.handleException(ex);
			return null;
		}		
	}
	
	public Boolean sendMailWithAttachments( String subject, String content, String emailRecepients, List<File> files)
			throws Exception { 
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		PrismDbUtil dbUtil = new PrismDbUtil();
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			Log.Info("sendEmail() method called on MailerDao");
			con = dbUtil.getPostGresConnection();
			Mail mailObj = new Mail();
			if (con != null) {
				stmt = con.createStatement();
				String query = "SELECT * FROM mail where template_name = 'DEFAULT'"; 
				rs = stmt.executeQuery(query);
				while (rs.next()) {
					mailObj.setUsername(rs.getString("username"));
					mailObj.setPassword(rs.getString("password"));
					mailObj.setSmptHost(rs.getString("smtp_host"));
					mailObj.setSmtpPort(rs.getString("smtp_port"));
					mailObj.setSmtpAuth(rs.getString("smtp_auth"));						
					mailObj.setFromEmail(rs.getString("fromemail"));
				}
			}
			triggerGenericMail(mailObj, subject, content, emailRecepients, files);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return true;
	}

	public void triggerGenericMail(
			Mail mailObj, 
			String subject,
			String content, 
			String recepients,
			List<File> files) {		
		Session sessionSMTP = null;			
		Properties smtpProp = System.getProperties();
		try{
/*			System.setProperty("javax.net.ssl.keyStore", "C:/Program Files/Java/jdk1.7.0_79/jre/ib/security/cacerts");
	        System.setProperty("javax.net.ssl.trustStrore", "C:/Program Files/Java/jdk1.7.0_79/jre/ib/security/cacerts");
	        System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
	        System.setProperty("javax.net.ssl.trustStorePassword", "changeit");*/
			final String username = mailObj.getUsername();
			final String password = mailObj.getPassword();
			final String smtpHost = mailObj.getSmptHost();
			final String smtpPort = mailObj.getSmtpPort();
			final String smtpAuth = mailObj.getSmtpAuth();
			final String fromEmail = mailObj.getFromEmail();
		
		smtpProp.put("mail.smtp.host", smtpHost);
		smtpProp.put("mail.smtp.port", smtpPort);
		if ("true".equalsIgnoreCase(smtpAuth)) {
			smtpProp.put("mail.smtp.auth", "true");
			smtpProp.put("mail.smtp.user", username);
			smtpProp.put("mail.smtp.password", password);
			
			if(!smtpHost.contains("jnj")){
				smtpProp.put("mail.smtp.starttls.enable", "true");
				smtpProp.put("mail.smtp.ssl.trust", smtpHost);
			}
			if(smtpHost.contains("outlook")){
				smtpProp.put("mail.smtp.ssl.enable", false);
			}
			    
			sessionSMTP = Session.getInstance(smtpProp,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username, password);
						}
					}); 
		} else {
			smtpProp.put("mail.smtp.auth", "false");
			sessionSMTP = Session.getInstance(smtpProp);
			//sessionSMTP.setDebug(true);
		}	
//		sessionSMTP.setDebug(true);
			MimeMessage message = new MimeMessage(sessionSMTP);
			message.setFrom(new InternetAddress(fromEmail));
			if(recepients.contains(",")){
				final String [] lstToEmails = recepients.split(",");
				for (String toEmail : lstToEmails) {
					message.addRecipient(Message.RecipientType.TO,
							new InternetAddress(toEmail));
				}
			}
			else if(recepients.contains(";")){
				final String [] lstToEmails = recepients.split(";");
				for (String toEmail : lstToEmails) {
					message.addRecipient(Message.RecipientType.TO,
							new InternetAddress(toEmail));
				}
			}else{
				message.addRecipient(Message.RecipientType.TO,
						new InternetAddress(recepients));
			}
			message.setSubject(subject);
			message.setContent(content, "text/html; charset=ISO-8859-1");			
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(content, "text/html"); 
			javax.mail.Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			if(files != null){
				for (File file : files) {
					MimeBodyPart attachFilePart = new MimeBodyPart();
					String filecontent = FileUtils.readFileToString(file);
					String fileName = file.getName();
					String fileExt  = FilenameUtils.getExtension(fileName);
					System.out.println("Extension processed is : " + fileExt);
					/*if(fileName.contains("csv")){
						attachFilePart.setDataHandler(new DataHandler(new ByteArrayDataSource(filecontent.getBytes(),"text/csv")));
					}
					if(fileName.contains(".xls") || fileName.contains(".xlsx")){
						 DataSource source = new FileDataSource(files.get(i).getAbsolutePath());
						 attachFilePart.setDataHandler(new DataHandler(source));
						//attachFilePart.setDataHandler(new DataHandler(new ByteArrayDataSource(filecontent.getBytes(),"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")));
						 attachFilePart.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					} 
					if(fileName.contains(".pdf")){
						 DataSource source = new FileDataSource(files.get(i).getAbsolutePath());
						 attachFilePart.setDataHandler(new DataHandler(source));
						 attachFilePart.setHeader("Content-Type", "application/pdf");
					}
					if(fileName.contains(".jpg") || fileName.contains(".jpeg")){
						 DataSource source = new FileDataSource(files.get(i).getAbsolutePath());
						 attachFilePart.setDataHandler(new DataHandler(source));
						 attachFilePart.setHeader("Content-Type", "image/jpg");
					}*/
					
					DataSource source = null;
					 source = new FileDataSource(file.getAbsolutePath());
					 attachFilePart.setDataHandler(new DataHandler(source));
					
					switch (fileExt) {
					case "csv":
						attachFilePart.setDataHandler(new DataHandler(new ByteArrayDataSource(filecontent.getBytes(),"text/csv")));
						//attachFilePart.setHeader("Content-Type", "text/csv");
						break;
					
					case "xls":
					case "xlsx":					 
						 attachFilePart.setHeader("Content-Type", " application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");					
						break;
						
					case "pdf":
						 attachFilePart.setHeader("Content-Type", "application/pdf");
						break;
					
					case "jpg":
					case "jpeg":
						 attachFilePart.setHeader("Content-Type", "image/jpg");
						break;
						
					case "png":
						 attachFilePart.setHeader("Content-Type", "image/png");
						 break;
						 
					case "gif":
						 attachFilePart.setHeader("Content-Type", "image/gif");
						 break;
	
					default:
						break;
					}
					attachFilePart.setFileName(fileName);				
					multipart.addBodyPart(attachFilePart);
				}				
			}
			message.setContent(multipart);			
			Transport transport = sessionSMTP.getTransport("smtp");
			transport.connect();
			transport.send(message);
		} catch (Exception e) {
			Log.Error(e.getMessage());
			PrismHandler.handleException(e);
		}
	}
	
	//Done By Madan Srivats.
	//Conditions needs to be discussed.
	public Mail getEmailByCondition(String name) {
		PrismDbUtil dbUtil = new PrismDbUtil();
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		Mail mailObj = new Mail();
		try {

			StringBuilder query = new StringBuilder("select * from mail where");
			query.append(" lower(template_name) = '"+name.toLowerCase()+"' ");
			con = dbUtil.getPostGresConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(query.toString());
			if (rs != null) {
				mailObj.setUsername(rs.getString("username"));
				mailObj.setPassword(rs.getString("password"));
				mailObj.setSmptHost(rs.getString("smtp_host"));
				mailObj.setSmtpPort(rs.getString("smtp_port"));
				mailObj.setFromEmail(rs.getString("fromemail"));
				String toEmails = rs.getString("toemails");
				String[] strArrToEmails = toEmails.split(",");
				List<String> lstToEmails = new ArrayList<>(
						Arrays.asList(strArrToEmails));
				mailObj.setToEmails(lstToEmails);
				String ccEmails = rs.getString("cclist");
				String[] strArrCcEmails = ccEmails.split(",");
				List<String> lstCcEmails = new ArrayList<>(
						Arrays.asList(strArrCcEmails));
				mailObj.setCcEmails(lstCcEmails);
				String bccEmails = rs.getString("bcclist");
				String[] strArrBccEmails = bccEmails.split(",");
				List<String> lstBccEmails = new ArrayList<>(
						Arrays.asList(strArrBccEmails));
				mailObj.setBccEmails(lstBccEmails);
				mailObj.setTemplateContent(rs
						.getString("template_content"));
			}
			
		} catch (Exception e) {
			Log.Error(e.getMessage());
			PrismHandler.handleException(e);
		}
		return mailObj;
	}
	
	public boolean insertMailLog(String from, String to, String subject, String content, String status) {
		boolean response = false;
		PrismDbUtil dbUtil = new PrismDbUtil();
		Statement stmt = null;
		Connection con = null;
		try {
			Log.Info("Inserting Email Logs");
			String query = "insert into email_log(mail_to, mail_from, subject, mail_content,email_status) values ('"+to+"', '"+from+"', '"+subject+"', '"+content+"', '"+status+"');";
			con = dbUtil.getPostGresConnection();
			stmt = con.createStatement();
			int res = stmt.executeUpdate(query);
			response = (res > 0) ? true : false;
			Log.Info("Email Logs Inserted Successfully");
		} catch (Exception e) {
			Log.Error(e.getMessage());
			PrismHandler.handleException(e);
		}
		
		return response;
	}
}
