package com.relevance.prism.data;

import java.util.ArrayList;
import java.util.List;

public class MaterialReportList implements E2emfView{
	

	private List<Material> aggregatedMaterialReportList = new ArrayList<Material>();

	public List<Material> getAggregatedMaterialReportList() {
		return aggregatedMaterialReportList;
	}

	public void setAggregatedMaterialReportList(
			List<Material> aggregatedMaterialReportList) {
		this.aggregatedMaterialReportList = aggregatedMaterialReportList;
	}
}
