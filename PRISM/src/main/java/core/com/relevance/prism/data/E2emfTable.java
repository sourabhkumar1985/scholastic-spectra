package com.relevance.prism.data;

import java.util.ArrayList;

public class E2emfTable {

	private String databaseName;
	private String tableName;
	private ArrayList<E2emfField> fields;
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public ArrayList<E2emfField> getFields() {
		return fields;
	}
	public void setFields(ArrayList<E2emfField> fields) {
		this.fields = fields;
	}
	
	
}
