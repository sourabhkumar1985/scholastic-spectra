/**
 * 
 */
package com.relevance.prism.util;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * @author Shawkath Khan Added On: Sep 11, 2015 This util class is created to
 *         change the currency type based on the locale
 */
public class ReportsUtil {

	// English language, United States
	// static Locale locale = null;
	// English language, United Kingdom

	public static Locale getLocale(String countryCode) {
		Locale locale = null;
		if (countryCode.equals("US"))
			locale = new Locale("en", "US");
		else if (countryCode.equals("UK"))
			locale = new Locale("en", "UK");
		else
			locale = new Locale("en", "US");
		return locale;
	}

	public static String getCurrencyFormat(String countryCode, String data) {
		Locale locale = getLocale(countryCode);
		String stdCost = "";
		NumberFormat currencyFormatter = NumberFormat
				.getCurrencyInstance(locale);
		if (data != null) {
			Double doubleStdCost = Double.parseDouble(data);
			stdCost = currencyFormatter.format(doubleStdCost);
		}
		// if (stdCost.contains("¤"))
		// System.out.println("before : " + stdCost);
		// stdCost = stdCost.replace("¤", "\u20AC");
		// System.out.println("before : " + stdCost);
		return stdCost;
	}

}
