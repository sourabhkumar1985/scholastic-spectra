package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.PrismHandler;
import com.relevance.prism.data.Attribute;
import com.relevance.prism.data.Audit;
import com.relevance.prism.data.MasterData;
import com.relevance.prism.dao.HashMapMasterData;

public class RuleAttributeMasterDataDao extends BaseDao {

	public Connection getPostGresDBConnection() throws Exception {
		Connection connection = null;
		try {
			connection = dbUtil.getPostGresConnection(E2emfConstants.qfind_db);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		return connection;
	}

	public List<MasterData> getItemNumbers() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		QfindMDMDao qfindMDMDao = new QfindMDMDao();
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<MasterData> att_list = new ArrayList<MasterData>();
		MasterData masterdata = null;
		String query = "select distinct item_number from(select item_number from solrmaster_label "
				+ "UNION ALL select item_number from solrmaster_catalog "
				+ "UNION ALL select catalog as item_number from spreadsheet_datasource "
				+ "UNION ALL select item_number from solrmaster_item) a where item_number is not null";
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()){
					masterdata = new MasterData();
					String skuNumber = rs.getString("item_number");
					masterdata.setSku_number(skuNumber);
					HashMap<String, String> allAttributesMap = qfindMDMDao.getAllattributeMap();
					List<HashMap<String, String>> allAttributes = getListOfHashMaps();
					HashMap<String, String> dataMap = new HashMap<String, String>();
					for (HashMap<String, String> attributeMap : allAttributes) {						
						Iterator<String> iter = attributeMap.keySet().iterator();
						while (iter.hasNext()) {
							String name = iter.next();
							String attributeName = attributeMap.get(name);
							String ruleName = allAttributesMap.get(name).toString();
							if (ruleName.equalsIgnoreCase("rule_1")) {
								String attribute_value = getListAttribute(name, skuNumber);
								dataMap.put(name, attribute_value);
							}
							if (ruleName.equalsIgnoreCase("rule_2")) {
								String attribute_value = getCountryOfOriginAttribute(name, skuNumber);
								dataMap.put(name, attribute_value);
							}
						}
					}
					insertDataIntoPostgres(connection, dataMap);
				}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		return att_list;
	}

	public String getListAttribute(String attribute_name, String item_number) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String attribute_value = null;
		try {
			HashMapMasterData.initiatlize();
			connection = getPostGresDBConnection();
			HashMap<String, String> labelTableHashMap = HashMapMasterData.getDataFromLabelTable();
			String column_name1 = null;
			Object column = labelTableHashMap.get(attribute_name);
			if (column != null) {
				column_name1 = column.toString();
			} else {
				column_name1 = null;
			}
			if (column_name1 != null) {
				String query1 = "select " + column_name1 + " from solrmaster_label where item_number='" + item_number
						+ "'";
				stmt = connection.createStatement();
				rs = stmt.executeQuery(query1);
				if (!rs.next()) {
					HashMapMasterData.initiatlize();
					HashMap<String, String> spreadsheetTableHashMap = HashMapMasterData.getDataFromSpreadsheetTable();
					String column_name2 = null;
					Object column2 = spreadsheetTableHashMap.get(attribute_name);
					if (column2 != null) {
						column_name2 = column2.toString();
					} else {
						column_name2 = null;
					}
					if (column_name2 != null) {
						String query2 = "select " + column_name2 + " from spreadsheet_datasource where catalog='"
								+ item_number + "'";
						stmt = connection.createStatement();
						rs = stmt.executeQuery(query2);
						if (!rs.next()) {
							HashMapMasterData.initiatlize();
							HashMap<String, String> catalogTableHashMap = HashMapMasterData.getDataFromCatalogTable();
							String column_name3 = null;
							Object column3 = catalogTableHashMap.get(attribute_name);
							if (column3 != null) {
								column_name3 = column3.toString();
							} else {
								column_name3 = null;
							}
							if (column_name3 != null) {
								String query3 = "select " + column_name3
										+ " from solrmaster_catalog where item_number='" + item_number + "'";
								stmt = connection.createStatement();
								rs = stmt.executeQuery(query3);
								if (!rs.next()) {
									attribute_value = "";
								} else {
									do {
										if (attribute_name.equalsIgnoreCase("type")) {
											attribute_value = "CATALOG";
										} else {
											attribute_value = rs.getString(1);
										}
									} while (rs.next());
								}
							} else {
								attribute_value = "";
							}
						} else {
							do {
								attribute_value = rs.getString(1);
							} while (rs.next());
						}
					} else {
						HashMap<String, String> catalogTableHashMap = HashMapMasterData.getDataFromCatalogTable();
						String column_name3 = null;
						Object column3 = catalogTableHashMap.get(attribute_name);
						if (column3 != null) {
							column_name3 = column3.toString();
						} else {
							column_name3 = null;
						}
						if (column_name3 != null) {
							String query3 = "select " + column_name3 + " from solrmaster_catalog where item_number='"
									+ item_number + "'";
							stmt = connection.createStatement();
							rs = stmt.executeQuery(query3);
							if (!rs.next()) {
								attribute_value = "";
							} else {
								do {
									if (attribute_name.equalsIgnoreCase("type")) {
										attribute_value = "CATALOG";
									} else {
										attribute_value = rs.getString(1);
									}
								} while (rs.next());
							}
						} else {
							attribute_value = "";
						}
					}
				} else {
					do {
						attribute_value = rs.getString(1);
					} while (rs.next());
				}
			} else {
				HashMap<String, String> spreadsheetTableHashMap = HashMapMasterData.getDataFromSpreadsheetTable();
				String column_name2 = null;
				Object column2 = spreadsheetTableHashMap.get(attribute_name);
				if (column2 != null) {
					column_name2 = column2.toString();
				} else {
					column_name2 = null;
				}
				if (column_name2 != null) {
					String query2 = "select " + column_name2 + " from spreadsheet_datasource where catalog='"
							+ item_number + "'";
					stmt = connection.createStatement();
					rs = stmt.executeQuery(query2);
					if (!rs.next()) {
						attribute_value = "";
					} else {
						do {
							attribute_value = rs.getString(1);
						} while (rs.next());
					}
				} else {
					HashMap<String, String> catalogTableHashMap = HashMapMasterData.getDataFromCatalogTable();
					String column_name3 = null;
					Object column3 = catalogTableHashMap.get(attribute_name);
					if (column3 != null) {
						column_name3 = column3.toString();
					} else {
						column_name3 = null;
					}
					if (column_name3 != null) {
						String query3 = "select " + column_name3 + " from solrmaster_catalog where item_number='"
								+ item_number + "'";
						stmt = connection.createStatement();
						rs = stmt.executeQuery(query3);
						if (!rs.next()) {
							attribute_value = "";
						} else {
							do {
								if (attribute_name.equalsIgnoreCase("type")) {
									attribute_value = "CATALOG";
								} else {
									attribute_value = rs.getString(1);
								}
							} while (rs.next());
						}
					} else {
						attribute_value = "";
					}
				}
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		return attribute_value;
	}	

	public static void insertDataIntoPostgres(Connection connection, HashMap<String, String> dataMap)
			throws Exception {
		try {
			StringBuilder sql = new StringBuilder("INSERT INTO QFIND_MASTERDATA_LATEST").append(" (");
			StringBuilder placeholders = new StringBuilder();
			for (Iterator<String> iter = dataMap.keySet().iterator(); iter.hasNext();) {
				sql.append(iter.next());
				placeholders.append("?");
				if (iter.hasNext()) {
					sql.append(",");
					placeholders.append(",");
				}
			}
			sql.append(") VALUES (").append(placeholders).append(")");
			PreparedStatement preparedStatement = connection.prepareStatement(sql.toString());
			System.out.println(preparedStatement);
			int i = 1;
			for (Object value : dataMap.values()) {
				if((value == null)){
					preparedStatement.setString(i, "");
				}else{
					preparedStatement.setString(i, value.toString());
				}
				i++;
			}
			int affectedRows = preparedStatement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<HashMap<String, String>> getListOfHashMaps() {
		List<HashMap<String, String>> setList = new ArrayList<HashMap<String, String>>();
		HashMapMasterData.initiatlize();
		HashMap<String, String> labelTableHashMap = HashMapMasterData.getDataFromLabelTable();
		HashMap<String, String> spreadsheetTableHashMap = HashMapMasterData.getDataFromSpreadsheetTable();
		HashMap<String, String> catalogTableHashMap = HashMapMasterData.getDataFromCatalogTable();
		setList.add(labelTableHashMap);
		setList.add(spreadsheetTableHashMap);
		setList.add(catalogTableHashMap);
		return setList;
	}

	public String getCountryOfOriginAttribute(String attribute_name, String item_number) throws Exception {
		String country_of_origin = null;
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			HashMapMasterData.initiatlize();
			HashMap<String, String> labelTableHashMap = HashMapMasterData.getDataFromLabelTable();
			String column_name1 = null;
			Object column = labelTableHashMap.get(attribute_name);
			if (column != null) {
				column_name1 = column.toString();
			} else {
				column_name1 = null;
			}
			if (column_name1 != null) {
				String query1 = "select " + attribute_name + " from solrmaster_label where item_number='" + item_number
						+ "'";
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				rs = stmt.executeQuery(query1);
				if (!rs.next()) {
					country_of_origin = "";
				} else {
					do {
						country_of_origin = rs.getString(1);
					} while (rs.next());
				}
			} else {
				country_of_origin = "";
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		return country_of_origin;
	}

	
	public List<Audit> getAttributeLog(String item_number,String attributname) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Audit auditdata = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<Audit> auditdatalist=new ArrayList<Audit>();
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			
			rs = stmt.executeQuery("select * from audit where primarykey = '" + item_number + "' and displayname= '" + attributname + "' and max(updatedate)");
			while (rs.next()) {
				auditdata = new Audit();
				String type_char=rs.getString("type");
				auditdata.setType(type_char.toString().charAt(0));
				String table_name=rs.getString("tablename");
				auditdata.setTableName(table_name);
				String item_no=rs.getString("primarykey");
				auditdata.setPrimaryKey(item_no);
				String field_name=rs.getString("fieldname");
				auditdata.setFieldName(field_name);
				String old_value=rs.getString("oldvalue");
				auditdata.setOldValue(old_value);
				String new_value=rs.getString("newvalue");
				auditdata.setNewValue(new_value);
				String user_name=rs.getString("userName");
				auditdata.setUserName(user_name);
				String user_id=rs.getString("userid");
				auditdata.setUserId(user_id);
				String display_name=rs.getString("displayname");
				auditdata.setDisplayName(display_name);
				String time_stamp=rs.getString("timestamp");
				auditdata.setTimestamp(time_stamp);
				
				
				auditdatalist.add(auditdata);
			}

		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}

		return auditdatalist;
	}

	
	public boolean addDataIntoLogTable(String item_number, String attributename,String new_value,String userName)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		PreparedStatement stmt = null;
		PreparedStatement stmt1 = null;
		Calendar calendar = Calendar.getInstance();
		java.util.Date currentDate = calendar.getTime();
		java.sql.Date date = new java.sql.Date(currentDate.getTime());
		ResultSet rs = null;
		boolean success = false;
		String query1 = "select " + attributename + " from masterdata where catalog_no='" + item_number + "' LIMIT 1";
		try {
			connection = getPostGresDBConnection();			
			stmt = connection.prepareStatement(query1);			
			rs = stmt.executeQuery();
			while (rs.next()) {
				String old_value = rs.getString(1);
				stmt1 = connection.prepareStatement("insert into attribute_log(item_number,attribute_name,old_value,new_value,who_modified,when_modified) values(?,?,?,?,?,?)");
				stmt1.setString(1, item_number);
				stmt1.setString(2, attributename);
				stmt1.setString(3, old_value);
				stmt1.setString(4, new_value);
				stmt1.setString(5, userName);
				stmt1.setDate(6, date);
				int res = stmt1.executeUpdate();
			}
			success = true;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
			success = false;
		} finally {
			PrismHandler.handleFinally(connection, null, stmt);
		}
		return success;
	}

	public List<MasterData> getDataFromMasterData(String itemnumberlist) throws Exception
	{
		
		boolean result = false;
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<MasterData> mdata = new ArrayList<MasterData>();
		String outer_query = "select * from masterdata where catalog_no IN ("+itemnumberlist+")";
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(outer_query);
			ResultSetMetaData rsmd = rs.getMetaData();
			int columns = rsmd.getColumnCount();
			MasterData masterdata = null;
			Attribute attribute = null;
			while(rs.next()) {
				masterdata = new MasterData();
				List<Attribute> att_list = new ArrayList<Attribute>();
			for (int i = 1; i <= columns; i++) {				
				String column_name = rsmd.getColumnName(i);
					String item_no = rs.getString(1);
					masterdata.setSku_number(item_no);
					String inner_query = "select * from audit where primarykey='" + item_no + "'and fieldname='"
							+ column_name + "' order by updatedate desc";
					Statement stmt1 = null;
					ResultSet rs1 = null;
					stmt1 = connection.createStatement();
					attribute = new Attribute();
					rs1 = stmt1.executeQuery(inner_query);
					if (rs1.next()) {
						do {
							String att_name = rs1.getString("fieldname");
							attribute.setAttribute_name(att_name);
							attribute.setEdited(true);
							att_list.add(attribute);
						} while (rs1.next());
					} else {						
						attribute.setAttribute_name(column_name);
						attribute.setEdited(false);
						att_list.add(attribute);
					}					
				}
				masterdata.setMasterdata(att_list);
				mdata.add(masterdata);	
			}					
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		return mdata;
	}
}
