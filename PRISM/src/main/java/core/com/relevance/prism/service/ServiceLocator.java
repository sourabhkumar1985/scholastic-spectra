package com.relevance.prism.service;

//import com.relevance.eemf.dao.Level3DetailedDao;
//import com.relevance.eemf.dao.MaterialDao;
//import com.relevance.eemf.dao.ProfileDao;
//import com.relevance.eemf.dao.PurchaseOrderDao;
//import com.relevance.eemf.dao.SystemMapDao;
//import com.relevance.eemf.service.L3DetailedService;
//import com.relevance.eemf.service.MaterialService;
//import com.relevance.eemf.service.ProfileService;
//import com.relevance.eemf.service.PurchaseOrderService;
//import com.relevance.eemf.service.SystemMapService;
//import com.relevance.ppa.dao.EMEAMasterDao;
//import com.relevance.ppa.service.EMEAMasterService;
import com.relevance.prism.dao.E2emfDao;
import com.relevance.prism.dao.MasterDao;
import com.relevance.prism.dao.SecurityMasterDao;
//import com.relevance.search.dao.SearchDao;
//import com.relevance.search.service.SearchService;
//import com.relevance.slob.dao.SlobBuyProfileMasterDao;
//import com.relevance.slob.dao.SlobMasterDao;
//import com.relevance.slob.service.SlobBuyProfileMasterService;
//import com.relevance.slob.service.SlobMasterService;

/**
 * 
 * E2emf Service Locator 
 *
 */
public class ServiceLocator {
	
	public static Service getServiceInstance(String serviceName) {
		Service service = null;
		if(serviceName.equalsIgnoreCase("master")){
			service = new MasterService();
		}
		/*else if(serviceName.equalsIgnoreCase("search")){
			service = new SearchService();
			
		}*/else if(serviceName.equalsIgnoreCase("tokenAnalysis")){
			service = new TokenAnalysisService();
			
		}/*else if(serviceName.equalsIgnoreCase("material")){
		
		service = new MaterialService();
		}else if(serviceName.equalsIgnoreCase("purchase")){
		
		service = new PurchaseOrderService();
		}
		else if(serviceName.equalsIgnoreCase("profile")){
			service = new ProfileService();
		}
		else if(serviceName.equalsIgnoreCase("systemmap")){
			service = new SystemMapService();
		}
		else if(serviceName.equalsIgnoreCase("l3detail")){
			service = new L3DetailedService();
		}
		else if(serviceName.equalsIgnoreCase("slobmaster")){
			service = new SlobMasterService();
		}else if(serviceName.equalsIgnoreCase("emeamaster")){
			service = new EMEAMasterService();
		}else if(serviceName.equalsIgnoreCase("slobbuyprofilemaster")){
			service = new SlobBuyProfileMasterService();
		}*/
		else if(serviceName.equalsIgnoreCase("securitymaster")){
			service = new SecurityMasterService();
		}
		return service;
	}

	public static E2emfDao getServiceDaoInstance(String serviceName) {
		E2emfDao daoService = null;
		if(serviceName.equalsIgnoreCase("master")){
			daoService = new MasterDao();
		}
		/*else if(serviceName.equalsIgnoreCase("search")){
			daoService = new SearchDao();
			
		}else if(serviceName.equalsIgnoreCase("material")){		
			daoService = new MaterialDao();
			
		}else if(serviceName.equalsIgnoreCase("purchase")){		
			daoService = new PurchaseOrderDao();
		}
		else if(serviceName.equalsIgnoreCase("profile")){
			daoService = new ProfileDao();
		}
		else if(serviceName.equalsIgnoreCase("systemmap")){
			daoService = new SystemMapDao();
			
		}else if(serviceName.equalsIgnoreCase("l3detail")){
			daoService = new Level3DetailedDao();
		}else if(serviceName.equalsIgnoreCase("slobmaster")){
			daoService = new SlobMasterDao();
		}else if(serviceName.equalsIgnoreCase("emeamaster")){
			daoService = new EMEAMasterDao();
		}else if(serviceName.equalsIgnoreCase("slobbuyprofilemaster")){
			daoService = new SlobBuyProfileMasterDao();
		}*/else if(serviceName.equalsIgnoreCase("securitymaster")){
			daoService = new SecurityMasterDao();
		}
		
		
		return daoService;
	}

}
