package com.relevance.prism.data;

public class SearchLog {

	private String userID;
	private String userName;
	private String displayQuery;
	private String jsonQuery;
	private String type;
	private String Module;
	private String logDate;
	private String bookmarkName;
	private String expirationDate;
	
	private int id;
	
	
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBookmarkName() {
		return bookmarkName;
	}
	public void setBookmarkName(String bookmarkName) {
		this.bookmarkName = bookmarkName;
	}
	public String getLogDate() {
		return logDate;
	}
	public void setLogDate(String logDate) {
		this.logDate = logDate;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getDisplayQuery() {
		return displayQuery;
	}
	public void setDisplayQuery(String displayQuery) {
		this.displayQuery = displayQuery;
	}
	public String getJsonQuery() {
		return jsonQuery;
	}
	public void setJsonQuery(String jsonQuery) {
		this.jsonQuery = jsonQuery;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getModule() {
		return Module;
	}
	public void setModule(String module) {
		Module = module;
	}
}
