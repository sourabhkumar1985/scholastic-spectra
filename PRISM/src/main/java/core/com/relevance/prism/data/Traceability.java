package com.relevance.prism.data;

public class Traceability {
	private String child;
	private String parent;
	private String direction;
	private String suportingColumns;
	private String key;
	public String getChild() {
		return child;
	}
	public void setChild(String child) {
		this.child = child;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getSuportingColumns() {
		return suportingColumns;
	}
	public void setSuportingColumns(String suportingColumns) {
		this.suportingColumns = suportingColumns;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
}
