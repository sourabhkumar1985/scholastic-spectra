package com.relevance.prism.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * E2emf Database Utiity Class
 * 
 */
public class PrismDbUtil {

	PrismDbUtil dbUtil = null;
	/*
	 * public static String hiveURL = null; public static String gbtHiveURL = null;
	 * public static String hivedriverURL = null; public static String hiveUser =
	 * null; public static String hivePwd = null;
	 * 
	 * public static String postgresURL = null; public static String
	 * postgresdriverURL = null; public static String postgresUser = null; public
	 * static String postgresPwd = null;
	 */

	// private static E2emfAppUtil appUtil = null;

	public static boolean isDbUtilInitialized = false;
	// private boolean usePostGres = false;// change this to false to use HIVE as
	// source DB

	public PrismDbUtil() {
		Log.Info("initializing AppUtils and DbUtils...");
		// appUtil = new E2emfAppUtil();

		if (!isDbUtilInitialized)
			initialize();

	}

	// SearchQueries
	String firstLevelNodeSelectQuery = "select ebeln, ekorg, ekgrp, lifnr,purorggrp as bsart,purdoctype as zterm, werks, bukrs, netwr,  menge, meins from ";
	// String secondLevelNodeSelectQuery =
	// "select ebeln, ekorg, ekgrp, lifnr, bsart, zterm, werks, bukrs, netwr, wkurs,
	// menge, meins from poproc26 ";

	String ekkoPurchaseDocSelectQuery = "select * from ekkotabHB ";
	String ekpoPurchaseDocSelectQuery = "select * from ekpotabHB ";

	private static void initialize() {
		isDbUtilInitialized = true;
	}

	public Connection getHiveConnection(String databaseName) throws AppException {
		Connection hiveConn = null;// Should not static variable since it will be taken from connection pool
		try {

			if (databaseName == null)
				throw new AppException("Database name is null");
			String[] connectionStringProps = E2emfAppUtil.getAppProperty(databaseName).split(":");
			if (connectionStringProps.length < 2)
				throw new AppException("Database properties requires JDBC Context name and DB name");
			String contextName = connectionStringProps[0];
			String dbName = connectionStringProps[1];
			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext.lookup(contextName);
			hiveConn = ds.getConnection();
			Statement st = null;
			try {
				st = hiveConn.createStatement();
				st.execute("use " + dbName);
			} catch (SQLException sql) {
				Log.Error("SQL Exception while setting default DB " + sql);
				throw new AppException(sql.getMessage(), "Error while processing the request.", 1, sql.getCause(),
						true);
			} finally {
				if (st != null) {
					st.close();
				}
			}
			return hiveConn;
		} catch (SQLException sql) {
			Log.Error("SQL Exception trying to get connection " + sql);
			// TODO Auto-generated catch block
			throw new AppException(sql.getMessage(), sql.getStackTrace(), "Error while processing the request.", 1,
					sql.getCause(), true);
		} catch (NullPointerException ne) {
			Log.Error("NullPointer Exception trying to get connection " + ne);
		} /*
			 * catch (ClassNotFoundException e) {
			 * Log.Error("ClassNotFoundException trying to get connection " +
			 * e.getMessage()); e.printStackTrace(); }
			 */catch (Exception sql) {
			Log.Error("Exception trying to get connection " + sql);
			throw new AppException(sql.getMessage(), sql.getStackTrace(), "Error while processing the request.", 2,
					sql.getCause(), true);

		}
		return hiveConn;
	}

	public Connection getPhoenixConnection() throws AppException {
		Connection gbtPhoenixConn = null;// Should not static variable since it will be taken from connection pool
		try {
			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext.lookup("jdbc/Phoenix");
			gbtPhoenixConn = ds.getConnection();

			Statement st = null;
			try {
				st = gbtPhoenixConn.createStatement();
				// st.execute("use " + appUtil.getAppProperty(E2emfConstants.gbtDB));

			} catch (SQLException sql) {
				Log.Error("SQL Exception while setting default GBT DB " + sql);
				throw new AppException(sql.getMessage(), "Error while processing the request.", 1, sql.getCause(),
						true);
			} finally {
				if (st != null) {
					st.close();
				}
			}

		} catch (SQLException sql) {
			Log.Error("SQL Exception trying to get connection " + sql);
			// TODO Auto-generated catch block
			throw new AppException(sql.getMessage(), "Error while processing the request.", 1, sql.getCause(), true);
		} catch (Exception e) {
			Log.Error("Exception trying to get connection " + e);
			throw new AppException(e.getMessage(), "Error while processing the request.", 2, e.getCause(), true);
		}

		return gbtPhoenixConn;
	}

	public Connection getPostGresConnection() throws AppException {
		Connection connection = null;

		Log.Info("-------PostGres JDBC Driver Registered! ------");

		try {

//			Context initContext = new InitialContext();
//			Context envContext = (Context) initContext.lookup("java:/comp/env");
//			DataSource ds = (DataSource) envContext.lookup("jdbc/" + E2emfConstants.postGresDBContext);
//			connection = ds.getConnection();

			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = null;
			ds = (DataSource) envContext.lookup("jdbc/" + E2emfConstants.postGresDBContext);
			Log.Info("Using MySQL connection...");
			System.out.println("Using MySQL connection...");
			/*if (!isMysqlDatabase()) {
				ds = (DataSource) envContext.lookup("jdbc/" + E2emfConstants.postGresDBContext);
				Log.Info("Using Postgress connection...");
				System.out.println("Using Postgress connection...");
			}else {
				ds = (DataSource) envContext.lookup("jdbc/" + E2emfConstants.MYSQL_DB_CONTEXT);
				Log.Info("Using MySQL connection...");
				System.out.println("Using MySQL connection...");

			}*/
			connection = ds.getConnection();
			
			Log.Info("Using MySQL connection...");

		} catch (Exception ee) {
			Log.Error("Exception in getting data from batch table " + ee);
			System.out.println("Exception" + ee.getMessage());
			System.out.println("Exception in detail" + ee);
			Log.Error(ee.getMessage());
			throw new AppException(ee.getMessage(), "Error while processing the request.", 1, ee.getCause(), true);
		}
		/*
		 * catch (SQLException e) { GbtLog.Error(e.getMessage());
		 * GbtLog.Info("Connection Failed! Check output console");
		 * System.out.println("Connection Failed! Check output console");
		 * e.printStackTrace(); return null; }
		 */

		if (connection != null) {
			// System.out.println("You made it, take control your database now!");
			Log.Info("You made it, take control your database now!");
		} else {
			// System.out.println("Failed to make connection!");
			Log.Info("Failed to make connection!");
		}

		return connection;
	}

	public Connection getPostGresConnection(String dbName) throws AppException {
		Connection connection = null;

		Log.Info("-------PostGres JDBC Driver Registered! ------");

		try {

			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext.lookup("jdbc/" + dbName);
			connection = ds.getConnection();

			Log.Info("Using Postgress connection...");

		} catch (Exception ee) {
			Log.Error("Exception in getting data from batch table " + ee);
			System.out.println("Exception" + ee.getMessage());
			System.out.println("Exception in detail" + ee);
			Log.Error(ee.getMessage());
			throw new AppException(ee.getMessage(), "Error while processing the request.", 1, ee.getCause(), true);
		}
		/*
		 * catch (SQLException e) { GbtLog.Error(e.getMessage());
		 * GbtLog.Info("Connection Failed! Check output console");
		 * System.out.println("Connection Failed! Check output console");
		 * e.printStackTrace(); return null; }
		 */

		if (connection != null) {
			// System.out.println("You made it, take control your database now!");
			Log.Info("You made it, take control your database now!");
		} else {
			// System.out.println("Failed to make connection!");
			Log.Info("Failed to make connection!");
		}

		return connection;
	}

	public Connection getPostGresConnectionForLogs() {
		Connection connection = null;

		Log.Info("-------PostGres JDBC Driver Registered! ------");

		try {

			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext.lookup("jdbc/E2EMFPostGres");
			connection = ds.getConnection();

			Log.Info("Using Postgress connection...");

		} catch (Exception ee) {
			Log.Error("Exception while logging in logs table " + ee);
			System.out.println("Exception" + ee.getMessage());
			System.out.println("Exception in detail" + ee);
			Log.Error(ee.getMessage());
		}
		/*
		 * catch (SQLException e) { GbtLog.Error(e.getMessage());
		 * GbtLog.Info("Connection Failed! Check output console");
		 * System.out.println("Connection Failed! Check output console");
		 * e.printStackTrace(); return null; }
		 */

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
			Log.Info("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
			Log.Info("Failed to make connection!");
		}

		return connection;
	}

	public String getCommaSeperatedInClauseValues(List<String> valueList) {
		StringBuilder inValues = new StringBuilder();

		if (valueList.size() > 0) {
			for (int i = 0; i < valueList.size(); i++) {
				inValues.append("'").append(valueList.get(i)).append("'");
				if (i < valueList.size() - 1) {
					inValues.append(", ");
				}
			}

		}
		return inValues.toString();
	}

	public String buildFirstLevelNodeQuery(String nodeName, String finalNodeVal, String source) {
		StringBuilder where = new StringBuilder(" where ");
		String tableName = "E2E_" + source + "_L2_PO";
		where.append(nodeName).append(" = '").append(finalNodeVal).append("' limit 100");

		StringBuilder firstLeveNodeQuery = new StringBuilder(firstLevelNodeSelectQuery).append(tableName).append(where);

		return firstLeveNodeQuery.toString();

	}

	public String buildSecondLevelNodeQuery(String firstNodeName, String finalNodeVal, String secondNodeName,
			String secondNodeValue, String source) {
		String tableName = "E2E_" + source + "_L2_PO";
		StringBuilder where = new StringBuilder(" where ");

		where.append(firstNodeName).append(" = '").append(finalNodeVal).append("' and ");
		where.append(secondNodeName).append(" = '").append(secondNodeValue).append("' limit 100");

		StringBuilder secondLevelNodeQuery = new StringBuilder(firstLevelNodeSelectQuery).append(tableName)
				.append(where);
		return secondLevelNodeQuery.toString();

	}

	// to be implemented for E2emf secondLevelData Populatation fro Canada flow
	public Map<String, String> buildSearchSummaryE2emfQuery(String purchaseDoc) {
		Map<String, String> searchSummaryQueryMap = new HashMap<String, String>();

		return searchSummaryQueryMap;

	}

	public Map<String, String> buildSearchSummaryQuery(String purchaseDoc) {

		Map<String, String> searchSummaryQueryMap = new HashMap<String, String>();

		StringBuilder where = new StringBuilder(" where ");

		where.append("purchdoc = '").append(purchaseDoc).append("'");

		StringBuilder ekkoSearchQuery = new StringBuilder(ekkoPurchaseDocSelectQuery).append(where);
		StringBuilder ekpoSearchQuery = new StringBuilder(ekpoPurchaseDocSelectQuery).append(where);

		searchSummaryQueryMap.put(E2emfConstants.SEARCH_SUMMARY_EKKO_QUERY, ekkoSearchQuery.toString());
		searchSummaryQueryMap.put(E2emfConstants.SEARCH_SUMMARY_EKPO_QUERY, ekpoSearchQuery.toString());

		return searchSummaryQueryMap;

	}

	public String buildMasterSearchQuery(String param, String searchStr, String source) {

		String masterSearchQuery = null;
		String tableName = null;
		if (param.equals("V")) {

			tableName = "E2E_" + source + "_VEND";
			masterSearchQuery = "select lifnr as id, name1 as name from " + tableName + " where source='" + source
					+ "' and name1 like '%" + searchStr + "%' or name1 like lcase('%" + searchStr
					+ "%') or name1 like ucase('%" + searchStr + "%') or lifnr like '%" + searchStr + "%' limit 30"; // limit
																														// 100
		} else if (param.equals("C")) {

			tableName = "E2E_" + source + "_CUST";
			masterSearchQuery = "select kunnr as id, name1 as name from " + tableName + " where source='" + source
					+ "' and name1 like '%" + searchStr + "%' or name1 like lcase('%" + searchStr
					+ "%') or name1 like ucase('%" + searchStr + "%') or kunnr like '%" + searchStr + "%'  limit 30";// limit
																														// 100
		} else if (param.equals("M")) {

			tableName = "E2E_" + source + "_MATR";
			masterSearchQuery = "select matnr as id, maktx as name from " + tableName + " where source='" + source
					+ "' and maktx like '%" + searchStr + "%' or maktx like lcase('%" + searchStr
					+ "%') or maktx like ucase('%" + searchStr + "%') or matnr like '%" + searchStr + "%' limit 30";
		} else if (param.equals("P")) {

			tableName = "E2E_" + source + "_PLANT";

			searchStr = searchStr.toUpperCase();
			masterSearchQuery = "select werks as id, plantdesc as name from " + tableName + " where source='" + source
					+ "' and plantdesc like '%" + searchStr + "%' or plantdesc like lcase('%" + searchStr
					+ "%') or plantdesc like ucase('%" + searchStr + "%') or werks like '%" + searchStr + "%' limit 30";
		}

		Log.Info("MasterSearch Query built is \n" + masterSearchQuery);

		return masterSearchQuery;
	}

	public String buildSlobMasterSearchQuery(String param, String searchStr, String source) {
		String masterSearchQuery = null;
		String tableName = null;
		if (param.equals("M")) {
			tableName = "SLOB_" + source + "_FINAL";
			masterSearchQuery = "select distinct material as id , material_desc as name from " + tableName
					+ " where category = 'MAKE' and ((material like '%" + searchStr + "%' or material like lcase('%"
					+ searchStr + "%') or material like ucase('%" + searchStr + "%') or material like '%" + searchStr
					+ "%')"

					+ " or (material_desc like '%" + searchStr + "%' or material_desc like lcase('%" + searchStr
					+ "%') or material_desc like ucase('%" + searchStr + "%') or material_desc like '%" + searchStr
					+ "%'))" + "limit 30";
		} else if (param.equals("B")) {
			tableName = "SLOB_" + source + "_FINAL";
			masterSearchQuery = "select distinct batch as id from " + tableName
					+ " where category = 'MAKE' and (batch like '%" + searchStr + "%' or batch like lcase('%"
					+ searchStr + "%') or batch like ucase('%" + searchStr + "%') or batch like '%" + searchStr
					+ "%') limit 30";
		}
		Log.Info("MasterSearch Query built is \n" + masterSearchQuery);

		return masterSearchQuery;
	}

	public String buildSlobBuyProfileMasterSearchQuery(String param, String searchStr, String source) {
		String masterSearchQuery = null;
		String tableName = null;
		if (param.equals("M")) {
			tableName = "SLOB_" + source + "_FINAL";
			masterSearchQuery = "select distinct material as id , material_desc as name from " + tableName
					+ " where category = 'BUY' and ((material like '%" + searchStr + "%' or material like lcase('%"
					+ searchStr + "%') or material like ucase('%" + searchStr + "%') or material like '%" + searchStr
					+ "%')"

					+ " or (material_desc like '%" + searchStr + "%' or material_desc like lcase('%" + searchStr
					+ "%') or material_desc like ucase('%" + searchStr + "%') or material_desc like '%" + searchStr
					+ "%'))" + "limit 30";
		} else if (param.equals("B")) {
			tableName = "SLOB_" + source + "_FINAL";
			masterSearchQuery = "select distinct batch as id from " + tableName
					+ " where category = 'BUY' and (batch like '%" + searchStr + "%' or batch like lcase('%" + searchStr
					+ "%') or batch like ucase('%" + searchStr + "%') or batch like '%" + searchStr + "%') limit 30";
		}
		Log.Info("MasterSearch Query built is \n" + masterSearchQuery);

		return masterSearchQuery;
	}

	public String buildEMEAMasterSearchQuery(String param, String searchStr, String source) {
		String masterSearchQuery = null;
		String tableName = null;
		if (param.equals("M")) {
			tableName = "PPV_ITEMS";
			masterSearchQuery = "select distinct matnr as id , txtlg as name from " + tableName
					+ " where ((matnr like '%" + searchStr + "%' or matnr like lcase('%" + searchStr
					+ "%') or matnr like ucase('%" + searchStr + "%') or matnr like '%" + searchStr + "%')"

					+ " or (txtlg like '%" + searchStr + "%' or txtlg like lcase('%" + searchStr
					+ "%') or txtlg like ucase('%" + searchStr + "%') or txtlg like '%" + searchStr + "%'))"
					+ "limit 30";
		} else if (param.equals("V")) {
			tableName = "PPV_VENDORS";
			masterSearchQuery = "select distinct lifnr as id , name1 as name from " + tableName
					+ " where ((lifnr like '%" + searchStr + "%' or lifnr like lcase('%" + searchStr
					+ "%') or lifnr like ucase('%" + searchStr + "%') or lifnr like '%" + searchStr + "%')"

					+ " or (name1 like '%" + searchStr + "%' or name1 like lcase('%" + searchStr
					+ "%') or name1 like ucase('%" + searchStr + "%') or name1 like '%" + searchStr + "%'))"
					+ "limit 30";
		} else if (param.equals("S")) {
			tableName = "PPV_SPEC_CODE";
			masterSearchQuery = "select spec_code as id, spec_code as name from " + tableName
					+ " where (spec_code like '%" + searchStr + "%' or spec_code like lcase('%" + searchStr
					+ "%') or spec_code like ucase('%" + searchStr + "%') or spec_code like '%" + searchStr + "%')"
					+ "limit 30";
		}

		Log.Info("MasterSearch Query built is \n" + masterSearchQuery);

		return masterSearchQuery;
	}

	public String buildSecurityMasterSearchQuery(String param, String searchStr) {
		String masterSearchQuery = null;
		String tableName = null;
		if (param.equals("S")) {
			tableName = "SECURITYANALYSIS";
			masterSearchQuery = "select distinct source as id from " + tableName + " where source like '%" + searchStr
					+ "%' or source like lcase('%" + searchStr + "%') or source like ucase('%" + searchStr
					+ "%') or source like '%" + searchStr + "%' limit 30";
		} else if (param.equals("T")) {
			tableName = "SECURITYANALYSIS";
			masterSearchQuery = "select distinct target as id from " + tableName + " where target like '%" + searchStr
					+ "%' or target like lcase('%" + searchStr + "%') or target like ucase('%" + searchStr
					+ "%') or target like '%" + searchStr + "%' limit 30";
		} else if (param.equals("R")) {
			tableName = "SECURITYANALYSIS";
			masterSearchQuery = "select distinct source_role as id from " + tableName + " where source_role like '%"
					+ searchStr + "%' or source_role like lcase('%" + searchStr + "%') or source_role like ucase('%"
					+ searchStr + "%') or source_role like '%" + searchStr + "%' limit 30";
		}
		Log.Info("MasterSearch Query built is \n" + masterSearchQuery);

		return masterSearchQuery;
	}

	public String buildSystemMapQuery(String param, String value) {
		String tableName = "E2E_" + value + "_AG_MF_SM ";
		String systemMapQuery = null;
		// if (param.equals("INBOUND")) {

		systemMapQuery = "select SENDER,sndprt as SENDER_TYPE, RECEIVER,rcvprt as RECEIVER_TYPE,direct as DIRECTION, docnum_sum as DOC_COUNT FROM "
				+ tableName;

		/*
		 * if (param != null) { systemMapQuery = systemMapQuery + " where " + param +
		 * " = '" + value + "'"; }
		 */

		// }

		Log.Info("System Map Query built is \n" + systemMapQuery);

		return systemMapQuery;
	}

	public JSONArray convert(ResultSet rs) throws SQLException {
		LinkedHashMap<String, String> jsonOrderedMap = new LinkedHashMap<String, String>();
		JSONArray json = new JSONArray();
		ResultSetMetaData rsmd = rs.getMetaData();

		try {
			int numColumns = rsmd.getColumnCount();
			while (rs.next()) {
				jsonOrderedMap = new LinkedHashMap<String, String>();
				for (int i = 1; i < numColumns + 1; i++) {
					String column_name = rsmd.getColumnLabel(i); //getColumnName(i);
					//System.out.println("Type:" +rsmd.getColumnType(i));
					//System.out.println("Type:" +rsmd.getColumnTypeName(i));
					if(rsmd.getColumnTypeName(i).equalsIgnoreCase("DATETIME")){
						jsonOrderedMap.put(" "," ");	
					} else{
					    jsonOrderedMap.put(column_name, rs.getString(column_name));
					}
				}
				JSONObject orderedJson = new JSONObject(jsonOrderedMap);
				json.put(orderedJson);
			}
		} catch (Exception e) {
			Log.Error("Exception in constructing JSON Array " + e);
		}
		return json;
	}
	
	private String getFormatedDate(java.sql.Date myDateTime){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(myDateTime.getTime());
	}
	

	/*
	 * public synchronized String getJSONFromResultSet(ResultSet rs, String keyName)
	 * {
	 * 
	 * Map json = new Hashtable<String, List>(); List list = new
	 * ArrayList<String>(); int i = 0;
	 * 
	 * if (rs != null) { try { ResultSetMetaData metaData = rs.getMetaData(); while
	 * (rs.next()) {
	 * 
	 * String nodeString = ""; int columnIndex; for (columnIndex = 1; columnIndex <=
	 * metaData .getColumnCount(); columnIndex++) { // Based on conv with Dinil -
	 * Impala will send None or // "" only for non values/null values String
	 * colValue = ""; if (rs.getString(metaData.getColumnName(columnIndex)) != null)
	 * { colValue = rs.getString(metaData .getColumnName(columnIndex)); } if
	 * (colValue != null || colValue.equalsIgnoreCase("None") ||
	 * colValue.equals(" ")) {
	 * 
	 * if (keyName.equalsIgnoreCase("\"0\"")) { nodeString = "\"" + "C-" +
	 * rs.getString(metaData .getColumnName(columnIndex)) + "\""; } else {
	 * nodeString = "\"" + rs.getString(metaData .getColumnName(columnIndex)) +
	 * "\""; }
	 * 
	 * list.add(nodeString); } } } } catch (SQLException e) {
	 * Log.Error("SQLException in creating Json from ResultSet " +
	 * e.getStackTrace()); e.printStackTrace(); } json.put(keyName, list); } //
	 * return JSONValue.toJSONString(json); return json.toString().replace("=",
	 * ":"); // System.out.println("The value is "+ json.toString().replace("=", //
	 * ":")); // return json.toString(); }
	 */

	public void releaseConnection(Connection con) {
		// Add to the Pool of Connection

	}

	/**
	 * @param customerIdList
	 * @return
	 */
	public String getCSV(List<String> inputList) {
		StringBuilder retValue = new StringBuilder("");
		boolean isFirstRec = true;
		for (String Id : inputList) {
			if (isFirstRec)
				isFirstRec = false;
			else
				retValue.append(",");
			retValue.append("'" + Id + "'");
		}

		return retValue.toString();
	}

	// preparing data into CSV format
	public String getCSVFromResultSet(ResultSet rs) {
		String dropShipTitle = "po_count,companycode,purchaseorg,purchasegroup,plantgroup,orderquantity,netprice,grossordervalue,source,lastchangedon,material_type,vendor_type,po_type";
		// String dropShipHead1 = "";
		String dropShipData = "";
		// int count = 0;

		try {
			Log.Info("Fetching csv from the Resultset of size " + rs.getFetchSize());

			if (rs != null) {

				ResultSetMetaData metaData = rs.getMetaData();
				Log.Info("Retrieved meta Data from the resultset ....");

				while (rs.next()) {
					int columnIndex = 0;
					String colValue = " ";

					for (columnIndex = 1; columnIndex <= metaData.getColumnCount(); columnIndex++) {
						// Preparing Header with "," seperated values.
						/*
						 * if(count <= columnIndex) { if(count == metaData.getColumnCount()-1) {
						 * count=count+1; dropShipHead= dropShipHead +
						 * metaData.getColumnName(columnIndex); } else if(count < columnIndex){
						 * count=count+1; dropShipHead= dropShipHead +
						 * metaData.getColumnName(columnIndex)+","; } }
						 */
						// Binding data with "," seperated values.
						if (colValue != null) {
							colValue = colValue + rs.getString(metaData.getColumnName(columnIndex)) + ",";
						}
					}
					colValue = colValue.substring(0, colValue.length() - 1);
					dropShipData = colValue + "\n" + dropShipData;
				}
				Log.Info("created csv data from the resultset recieved.....");

			}

		} catch (SQLException sql) {
			Log.Error("SQLException in creating csv from resultset....." + sql);
		} catch (Exception e) {
			Log.Error("Exception in creating csv from resultset....." + e);

		}

		return dropShipTitle + "\n" + dropShipData;

	}

	public String getCSVFromResultSet(ResultSet rs, String profileType) {
		Log.Info("Fetching the csv from the resultset for type " + profileType);
		String dropShipTitle = "";
		String dropShipData = "";

		try {

			if (profileType.equalsIgnoreCase("poprofile")) {
				dropShipTitle = "po_count,companycode,companycodename,purchaseorg,purchaseorgname,purchasegroup,purchasegroupname,plantgroup,plantname,orderquantity,netprice,grossordervalue,source,lastchangedon,material_type,vendor_type,po_type";
			} else if (profileType.equalsIgnoreCase("soprofile")) {
				// dropShipTitle="so_count,companycode,salesorg,salesgroup,division,netvalue,salesdoctype,plantgroup,lastchangedon,material_type,so_type";
				dropShipTitle = "so_count,companycode,companycodename,salesorg,salesorgname,salesgroup,salesgroupname,division,divisionname,salesdoctype,plantgroup,plantname,netvalue,source,region,lastchangedon,materialtype,so_type";

			} else if (profileType.equalsIgnoreCase("system")) {
				dropShipTitle = "createdon,messagetype,messagetypename,basictype,idocstatus,idocstatusname,direction,receiverport,receiverpartntype,receiverpartnerno,receiver,senderport,senderpartnertype,senderpartnerno,sender,docnum_count";
			}

			if (rs != null) {
				ResultSetMetaData metaData = rs.getMetaData();
				Log.Info("Retrieved MetaData from resultset for profile " + profileType + " with resultset size "
						+ rs.getFetchSize());

				while (rs.next()) {
					int columnIndex = 0;
					String colValue = "";

					for (columnIndex = 1; columnIndex <= metaData.getColumnCount(); columnIndex++) {
						// Preparing Header with "," seperated values.
						/*
						 * if(count <= columnIndex) { if(count == metaData.getColumnCount()-1) {
						 * count=count+1; dropShipHead= dropShipHead +
						 * metaData.getColumnName(columnIndex); } else if(count < columnIndex){
						 * count=count+1; dropShipHead= dropShipHead +
						 * metaData.getColumnName(columnIndex)+","; } }
						 */
						// Binding data with "," seperated values.

						if (colValue != null) {
							colValue = colValue + rs.getString(metaData.getColumnName(columnIndex)) + ",";
						}

					}
					colValue = colValue.substring(0, colValue.length() - 1);
					dropShipData = colValue + "\n" + dropShipData;

					// Log.Info("dropShipData : " + dropShipData);
				}

			}

		} catch (SQLException sql) {
			Log.Error("SQLException creating csv from resultset for profile " + profileType + " " + sql);
		} catch (Exception e) {
			Log.Error("Exception creating csv from resultset for profile " + profileType + " " + e);
		}

		return dropShipTitle + "\n" + dropShipData;
	}

	public String[] getTierKeyParam(String queryParam) {
		String[] tierKeyParamArray = queryParam.split(" ");

		return tierKeyParamArray;
	}

	public double roundDouble(double number) {
		double number1 = number;

		number1 = Math.round(number1 * 100);
		number1 = number1 / 100;

		// System.out.println("number1:" +number1);
		return number1;
	}

	public String buildActionSearchQuery(String param, String searchStr) {

		String actionSearchQuery = null;
		String tableName = null;
		if (param.equals("A")) {

			tableName = "actions";
			actionSearchQuery = "select action_id as id, display_name as name from " + tableName
					+ " where lower(display_name) like lower('%" + searchStr
					+ "%') or lower(action) like lower('%" + searchStr + "%')" + " limit 30";
		} else if (param.equals("R")) {
			tableName = "roles";
			actionSearchQuery = "select name as id, displayname as name from " + tableName
					+ " where lower(name) like lower('%" + searchStr + "%') or lower(name) like lower('%" + searchStr
					+ "%') or lower(displayname) like lower('%" + searchStr + "%') or lower(displayname) like lower('%"
					+ searchStr + "%') limit 30";
		}

		Log.Info("ActionSearch Query built is \n" + actionSearchQuery);

		return actionSearchQuery;
	}

	/*
	 * public String buildPrimaryRoleSearchQuery(String param, String searchStr) {
	 * 
	 * String actionSearchQuery = null; String tableName = null; if
	 * (param.equals("PR")) {
	 * 
	 * tableName = "user_roles"; actionSearchQuery =
	 * "select role as id,  role as name from "+tableName+" where role like '%" +
	 * searchStr + "%' or lower(role) like lower('%" + searchStr +
	 * "%') or role like upper('%" + searchStr + "%')"+ " limit 30"; }
	 * Log.Info("ActionSearch Query built is \n" + actionSearchQuery);
	 * 
	 * return actionSearchQuery; }
	 */
	public static boolean isMysqlDatabase() throws IOException {
		String value = null;
		try {
			// ClassLoader classLoader =
			// Thread.currentThread().getContextClassLoader().getResourceAsStream("/database.properties");
			InputStream input = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("/database.properties");
			Properties properties = new Properties();
			properties.load(input);
			value = properties.getProperty("dbused");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return "mysql".equalsIgnoreCase(value);
	}
}
