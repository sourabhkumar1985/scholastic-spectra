package com.relevance.prism.rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.data.Action;
import com.relevance.prism.data.Role;
import com.relevance.prism.service.AdminService;
import com.relevance.prism.util.PrismHandler;

@Path("/admin")
public class AdminResource extends BaseResource{ 

	@Context
	ServletContext context;

	public AdminResource(@Context ServletContext value) {
		this.context = value;
		//System.out.println("Conext:" + this.context);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addaction")
	public String addAction(@FormParam("action") String action, 
						@FormParam("displayname") String displayname, 
						@FormParam("parent") String parent,
						@FormParam("parentName") String parentName,
						@FormParam("sequence") String sequence,
						@FormParam("icon") String icon, 
						@FormParam("enabled") String enabled,@FormParam("sources") String sources) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			AdminService adminService = new AdminService();
			response = adminService.insertAction(action,displayname,parent,parentName,sequence,icon,enabled,sources);
			return response;
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateaction")
	public String updateAction(@FormParam("actionid") String actionid, 
							@FormParam("action") String action, 
							@FormParam("displayname") String displayname,
							@FormParam("parent") String parent,
							@FormParam("parentName") String parentName,
							@FormParam("sequence") String sequence, 
							@FormParam("icon") String icon, 
							@FormParam("enabled") String enabled,@FormParam("sources") String sources) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			AdminService adminService = new AdminService();
			Action resultAction = adminService.updateAction(actionid,action,displayname,parent,parentName,sequence,icon,enabled,sources);
			Gson gson = new Gson();
			response = gson.toJson(resultAction);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteaction")
	public String deleteAction(@FormParam("actionid") String actionid) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			AdminService adminService = new AdminService();
			String successMessage = adminService.deleteAction(actionid);
			return successMessage;
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
		
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getaction")
	public String getAction(@FormParam("actionid") String actionid) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			AdminService adminService = new AdminService();
			Action resultAction = adminService.getAction(actionid);
			Gson gson = new Gson();
			response = gson.toJson(resultAction);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getactionlist")
	public String getActionList() {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			AdminService adminService = new AdminService();
			List<Action> actionList = adminService.getActionList();
			Gson gson = new Gson();
			response = gson.toJson(actionList);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addroleaction")
	public String addRoleAction(@FormParam("role") String role, 
						@FormParam("actionid") String actionid) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			AdminService adminService = new AdminService();
			response = adminService.insertRoleAction(role, actionid);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteroleaction")
	public String deleteRoleAction(@FormParam("key") String roleactionid) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			AdminService adminService = new AdminService();
			String successMessage = adminService.deleteRoleAction(roleactionid);
			return successMessage;
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getroleactionlist")
	public String getRoleActionList() {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			AdminService adminService = new AdminService();
			List<Role> roleList = adminService.getRoleActionList();
			Gson gson = new Gson();
			response = gson.toJson(roleList);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addlookupvalue")
	public String addLookupValue(@FormParam("key") String key, 
						@FormParam("displayname") String displayname) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			AdminService adminService = new AdminService();
			response= adminService.insertLookupValue(key, displayname);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updatelookupvalue")
	public String updateLookupValue(@FormParam("key") String key, 
							@FormParam("displayname") String displayname) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			AdminService adminService = new AdminService();
			response = adminService.updateLookupValue(key, displayname);
			return response;
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deletelookupvalue")
	public String deleteLookupValue(@FormParam("key") String key) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			AdminService adminService = new AdminService();
			response = adminService.deleteLookupValue(key);
			return response;
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getlookuplist")
	public String getLookupList() {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			AdminService adminService = new AdminService();
			response = adminService.getLookUpValueList();
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
