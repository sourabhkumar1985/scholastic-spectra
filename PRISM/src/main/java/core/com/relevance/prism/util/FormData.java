package com.relevance.prism.util;


public class FormData 
{
	private String name;
	private boolean isObfuscate;
	private boolean isDeObfuscate;
	private int obfType;
	private String remarks;
	
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public int getObfType() {
		return obfType;
	}
	public void setObfType(int obfType) {
		this.obfType = obfType;
	}
	public boolean isDeObfuscate() {
		return isDeObfuscate;
	}
	public void setDeObfuscate(boolean isDeObfuscate) {
		this.isDeObfuscate = isDeObfuscate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isObfuscate() {
		return isObfuscate;
	}
	public void setObfuscate(boolean isObfuscate) {
		this.isObfuscate = isObfuscate;
	}
	
	/*
	public void decrypt() {
		Set<String> keySet = formDataMap.keySet();
		Iterator<String> keySetIterator = keySet.iterator();
		while(keySetIterator.hasNext()){
			String keyValue = keySetIterator.next();
			String formDataValue = formDataMap.get(keyValue);
			if(formDecryptList.containsKey(keyValue)){
				formDataMap.put(keyValue, E2emfAppUtil.decrypt(formDataValue));
			}
		}
	}
	*/
	
		   
}
