package com.relevance.prism.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;
import com.relevance.prism.data.Action;
import com.relevance.prism.data.AdHocReportQuery;
import com.relevance.prism.data.Audit;
import com.relevance.prism.data.ChartConfiguration;
import com.relevance.prism.data.ChartProperty;
import com.relevance.prism.data.Charts;
import com.relevance.prism.data.Column;
import com.relevance.prism.data.ColumnConfigure;
import com.relevance.prism.data.ColumnDetail;
import com.relevance.prism.data.ColumnInformation;
import com.relevance.prism.data.DataAnalyserView;
import com.relevance.prism.data.DataTable;
import com.relevance.prism.data.DataTableView;
import com.relevance.prism.data.E2emfView;
import com.relevance.prism.data.Field;
import com.relevance.prism.data.FieldList;
import com.relevance.prism.data.ProfileConfig;
import com.relevance.prism.data.ProfileView;
import com.relevance.prism.data.Report;
import com.relevance.prism.data.UpdateColumns;
import com.relevance.prism.service.MailService;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class DataAnalyserDao extends BaseDao {
	public Logger _logger = Logger.getLogger(this.getClassName());
	private static final String HIVE = "HIVE";

	public List<String> getDatabaselist(String source) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<String> databaseList = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbUtil.getPostGresConnection();
			query.append("SELECT name FROM app_properties");
			if (source.equalsIgnoreCase(HIVE)) {
				query.append(" WHERE type='hive'");
			} else if ("solr".equalsIgnoreCase(source)) {
				query.append(" WHERE type='solr'");
			} else if ("memsql".equalsIgnoreCase(source)) {
				query.append(" WHERE type='memsql'");
			} else if ("mongo".equalsIgnoreCase(source)) {
				query.append(" WHERE type='mongo'");
			}else {
				query.append(" WHERE type='postgres'");
			}
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					databaseList.add(rs.getString("name"));
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return databaseList;
	}
	public ChartConfiguration getChartConfigurationList()
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ChartConfiguration chartConfig = new ChartConfiguration();
		Map<String, ArrayList<ChartProperty>> chartMap = new HashMap<>();
		List<Charts> chartslist=new ArrayList<>();
		String query = null;
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbUtil.getPostGresConnection();
			query = "select prop.*, cp.chart_name,cp.is_mandatory ,charts.display_name from properties as prop inner join chart_properties as cp on prop.property_name = cp.property_name INNER JOIN charts on charts.chart_name= cp.chart_name;";
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(query);
				rs = stmt.executeQuery(query);
				while (rs.next()) {
					String chartName = rs.getString("chart_name");
					if(chartName.equals("summary-fillgauge"))
						System.out.println(chartName+" "+rs.getString("property_name"));
					ChartProperty property = new ChartProperty();
					property.setPropertyName(rs.getString("property_name"));
					property.setDisplayName(rs.getString("display_name"));
					property.setDescription(rs.getString("description"));
					property.setDefaultValue(rs.getString("default_value"));
					property.setSequence(rs.getInt("seq"));
					property.setSection(rs.getInt("section"));
					property.setValueList(rs.getString("value_list"));
					property.setPropertyType(rs.getString("property_type"));
					property.setChartName(rs.getString("chart_name"));
					property.setMandatory(rs.getInt("is_mandatory"));
					ArrayList<ChartProperty> chartPropertyList = chartMap
							.get(chartName);
					if (chartPropertyList == null) {
						ArrayList<ChartProperty> chartPropertyListToStore = new ArrayList<>();
						chartMap.put(chartName, chartPropertyListToStore);
						chartPropertyList = chartMap.get(chartName);
					}
					chartPropertyList.add(property);
				}
			
				chartslist = getChartDisplay();
				chartConfig.setChartProperty(chartMap);
				chartConfig.setCharts(chartslist); 
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return chartConfig;
	}
	
	public List<Charts> getChartDisplay()
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Charts> chartslist=new ArrayList<>();
		String query = null;
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try{
			con = dbUtil.getPostGresConnection();
			query = "select chart_name, display_name, enabled FROM charts where enabled=true ORDER BY seq ";
			if (con != null) {
				stmt = con.createStatement(
				java.sql.ResultSet.TYPE_FORWARD_ONLY,
				java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(query);
				rs = stmt.executeQuery(query);
					while (rs.next()) {
							Charts chart = new Charts();
							chart.setChartName(rs.getString("chart_name"));
							chart.setDisplayName(rs.getString("display_name"));
							chartslist.add(chart);
							   }
							}
		}
		catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		
		}
		return chartslist;
		
	}
	
	
	public ColumnInformation getcolumnInfo(String source, String database,
			String table, String column, String filters,boolean isDetails) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ColumnInformation columnInfo = new ColumnInformation();
		String query = null;
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		List<Column> columnDetailsList = getColumnlist(source, database, table,null);
		String dataType = "";
		try {
			con = getDatabaseConnection(source, database);
			for (int i = 0; i < columnDetailsList.size(); i++) {
				if (columnDetailsList.get(i).getColumnName()
						.equalsIgnoreCase(column)) {
					dataType = columnDetailsList.get(i).getDataType();
					break;
				}
			}
			if (filters == null || "".equalsIgnoreCase(filters.trim()))
				filters = " 1=1 ";
			query = "select count(*) as count,count(distinct(" + column
					+ ")) as dist from " + table+" where "+filters;
			if (con != null) {
				stmt = con.createStatement();
				System.out.println(query);
				rs = stmt.executeQuery(query);
				rs.next();
				columnInfo.setnoOfRecords(rs.getInt(1));
				columnInfo.setDistinctValue(rs.getInt(2));
				rs.close();
				// for integer column
				if (dataType.equals("bigint") || dataType.equals("double")|| dataType.indexOf("decimal") >-1) {
					query = "select MIN(" + column + ") as min,MAX(" + column
							+ ") as max, AVG(" + column + "),appx_median("
							+ column + "),stddev(" + column + ") from " + table+" where "+filters;
					rs = null;
					System.out.println(query);
					rs = stmt.executeQuery(query);
					rs.next();
					columnInfo.setMinValue(rs.getInt(1));
					columnInfo.setMaxValue(rs.getInt(2));
					columnInfo.setAvgValue(rs.getDouble(3));
					columnInfo.setMedianValue(rs.getInt(4));
					columnInfo.setStdDeviation(rs.getDouble(5));
					columnInfo.setNumaric(true);
					rs.close();
				}
				if(isDetails){
					query = "select (" + column
							+ ") as \"key\",count(*) as count from " + table+" where "+filters
							+ " group by " + column;
					rs = null;
					System.out.println(query);
					rs = stmt.executeQuery(query);
					List<ColumnDetail> list = new ArrayList<>();
					while (rs.next()) {
						ColumnDetail detail = new ColumnDetail();
						detail.setCount(rs.getInt("count"));
						detail.setKey(rs.getString("key"));
						list.add(detail);
					}
					columnInfo.setColumnDetail(list);
					rs.close();
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return columnInfo;
	}

	private String getDatabaseName(String database) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		String paramCon = null;
		try {
			query.append("select name from app_properties where name ='")
					.append(database)
					.append("' or (value like '%jdbc%' and value like '%")
					.append(database).append("%')");
			con = dbUtil.getPostGresConnection();
			stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
					java.sql.ResultSet.CONCUR_READ_ONLY);
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				paramCon = rs.getString("name");
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return paramCon;
	}

	public Connection getDatabaseConnection(String type, String database)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		String paramCon;
		try {
			paramCon = getDatabaseName(database);
			// if (type.equalsIgnoreCase(HIVE)) {
			if ((HIVE).equalsIgnoreCase(type)) {
				con = dbUtil.getHiveConnection(paramCon);   // commented by Nagaraj
				//con = dbUtil.getHiveConnection("CELGENE_HIVE");   // added by Nagaraj
			} else {
				con = dbUtil.getPostGresConnection(paramCon); // commented by Nagaraj
				//con = dbUtil.getPostGresConnection("E2EMFPostGres");  // added by Nagaraj
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return con;
	}

	public List<Column> getColumnlist(String source, String database,
			String table,String dataSourceQuery) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder query = new StringBuilder();

		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		List<Column> columnDetailsList = new ArrayList<>();
		try {
			con = getDatabaseConnection(source, database);
			if(dataSourceQuery != null && !"".equalsIgnoreCase(dataSourceQuery)){
				dataSourceQuery = dataSourceQuery.replace("<FILTER>", " 1=1");
				if(dataSourceQuery.endsWith("where")){
					dataSourceQuery = dataSourceQuery +" 1=1";
				}
				dataSourceQuery = dataSourceQuery.replace("<ORDERBY>","");
				dataSourceQuery = dataSourceQuery.replace("<SEARCH>","");
				query.append(dataSourceQuery);
			}else{
				if (source.equalsIgnoreCase(HIVE)) {
					query.append("describe " + table);
				} else {
					query.append("select column_name as name, data_type as type from INFORMATION_SCHEMA.COLUMNS where table_name = '"
							+ table + "'");
				}
			}
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				if(dataSourceQuery != null && !"".equalsIgnoreCase(dataSourceQuery)){
					ResultSetMetaData  metaData = rs.getMetaData();
					for (int i = 1; i <= metaData.getColumnCount(); i++)
					{
						Column columnDetails = new Column();
						columnDetails.setColumnName(metaData.getColumnName(i));
						columnDetails.setDataType(metaData.getColumnTypeName(i));
						columnDetailsList.add(columnDetails);
					}
				}else{
					while (rs.next()) {
						Column columnDetails = new Column();
						columnDetails.setColumnName(rs.getString("name"));
						columnDetails.setDataType(rs.getString("type"));
						columnDetailsList.add(columnDetails);
					}
				}
				
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return columnDetailsList;
	}

	public List<String> getTablelist(String source, String database)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<String> databaseList = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = getDatabaseConnection(source, database);
			if (source.equalsIgnoreCase(HIVE) || source.equalsIgnoreCase("memsql")) {
				query.append("show tables");
			} else {
				String appPropertyName = null;
				String[] appPropertyNames = null;
				String dbName = "spectra";
				appPropertyName = E2emfAppUtil.getAppProperty(database);
				if (appPropertyName != null && !"".equalsIgnoreCase(appPropertyName.trim())){
					appPropertyNames = appPropertyName.split(":");
				}
				if(appPropertyNames != null && appPropertyNames.length>0 && appPropertyNames[1] != null){
					dbName = appPropertyNames[1];
				}
				//query.append("SELECT table_name as name FROM information_schema.views WHERE table_schema='public' union SELECT table_name as name FROM information_schema.tables WHERE table_schema='public' ORDER BY 1"); // commented by Nagaraj
				query.append("SELECT table_name as name FROM information_schema.tables where table_schema='"+dbName+"' union SELECT table_name as name FROM information_schema.tables WHERE table_schema='"+dbName+"' ORDER BY 1"); // added by Nagaraj
			}
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					if(source.equalsIgnoreCase("memsql")){
						databaseList.add(rs.getString("Tables_in_apotex"));
					}else{
						databaseList.add(rs.getString("name"));
					}
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);

		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return databaseList;
	}

	private JSONObject getDescription(ProfileConfig config) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder query = null;

		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		String source;
		String database;
		String table;
		FieldList description;
		JSONObject jsonDescription = null;
		try {

			source = config.getSource();
			database = config.getDatabase();
			table = config.getTable();
			description = config.getDescription();
			if (description == null || description.getDescriptionList() == null
					|| description.getDescriptionList().isEmpty())
				return null;

			con = getDatabaseConnection(source, database);
			stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
					java.sql.ResultSet.CONCUR_READ_ONLY);
			query = new StringBuilder("SELECT ");
			StringBuilder tempField = new StringBuilder();
			HashMap<String, HashMap<String, String>> descriptionList = new HashMap<>();
			for (Field fieldAndDescription : description.getDescriptionList()) {
				if (fieldAndDescription.getDescription() == null
						|| "".equalsIgnoreCase(fieldAndDescription
								.getDescription().trim()))
					continue;
				if (!"".equalsIgnoreCase(tempField.toString()))
					tempField.append(",");

				tempField.append(fieldAndDescription.getName()).append(",")
						.append(fieldAndDescription.getDescription());
				descriptionList.put(fieldAndDescription.getName().replace("`",""),
						new HashMap<String, String>());
			}
			query.append(tempField).append(" FROM ").append(table)
					.append(" GROUP BY ").append(tempField);
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				for (Field fieldAndDescription : description
						.getDescriptionList()) {
					descriptionList.get(fieldAndDescription.getName().replace("`","")).put(
							rs.getString(fieldAndDescription.getName().replace("`","")),
							rs.getString(fieldAndDescription.getDescription().replace("`","")));
				}
			}
			jsonDescription = new JSONObject(descriptionList);

		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return jsonDescription;
	}

	private StringBuilder constructQuery(ProfileConfig config) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder query = new StringBuilder();
		String columns;
		String filters;
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		String source;
		String database;
		try {
			columns = config.getColumns();
			filters = config.getFilter();
			if (config.isGroupByEnabled()) {
				HashSet<String> groupBy = new HashSet<>();
				source = config.getSource();
				database = config.getDatabase();
				con = getDatabaseConnection(source, database);
				query.append("SELECT ").append(columns).append(" FROM ")
						.append(config.getTable()).append(" LIMIT 1");
				stmt = con.createStatement();
				rs = stmt.executeQuery(query.toString());
				ResultSetMetaData columnsMetadata = rs.getMetaData();
				int i = 0;
				List<String> columnList = Arrays.asList(columns
						.split("\\s*,\\s*"));
				List<String> numbericType = new ArrayList<>();
				numbericType.add("double");
				numbericType.add("bigint");
				numbericType.add("int8");
				numbericType.add("int2");
				numbericType.add("int");
				numbericType.add("int4");
				numbericType.add("decimal");
				numbericType.add("float8");
				numbericType.add("float4");
				numbericType.add("tinyint");
				query = new StringBuilder("SELECT ");
				StringBuilder cols = new StringBuilder();
				while (i < columnsMetadata.getColumnCount()) {
					String column = columnList.get(i).toLowerCase();
					if (i != 0)
						cols.append(" , ");

					if (numbericType.contains(columnsMetadata
							.getColumnTypeName(i + 1).toLowerCase())) {
						cols.append("SUM(").append(column).append(") AS ")
								.append(column);
					} else {
						List<String> columnAliasList = Arrays.asList(column
								.split(" as "));
						String columnName = columnAliasList.get(0);
						String columnLabel;
						if (columnAliasList.size() > 1) {
							columnLabel = columnAliasList.get(1);
						} else {
							columnLabel = columnAliasList.get(0);
						}
						cols.append(columnName).append(" AS ")
								.append(columnLabel);
						groupBy.add(columnName);
					}
					i++;
				}
				cols.append(" , COUNT(*) as _count ");
				query.append(cols).append(" FROM ").append(config.getTable());
				if (filters != null && !"".equalsIgnoreCase(filters.trim()))
					query.append(" WHERE " + filters);
				if(!groupBy.isEmpty()){
					query.append(" GROUP BY ").append(StringUtils.join(groupBy, ","));
				}
			} else {
				query.append("SELECT ").append(columns)
						.append(" , 1 as _count ")
				 .append(" FROM ")
						.append(config.getTable());
				if (filters != null && !"".equalsIgnoreCase(filters.trim()))
					query.append(" WHERE ").append(filters);

			}
		
			  if (E2emfAppUtil.getAppProperty(E2emfConstants.QUERY_LIMIT) !=
			  null && E2emfAppUtil.getAppProperty(
			  E2emfConstants.QUERY_LIMIT).length() > 0) { query.append(" ")
			  .append(E2emfAppUtil
			  .getAppProperty(E2emfConstants.QUERY_LIMIT)); }
			 
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return query;
	}

	private StringBuilder constructQueryForEmail(ProfileConfig config) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder query = new StringBuilder();
		String columns;
		String filters;
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		String source;
		String database;
		try {
			columns = config.getColumns();
			filters = config.getFilter();
			if (config.isGroupByEnabled()) {
				HashSet<String> groupBy = new HashSet<>();
				source = config.getSource();
				database = config.getDatabase();
				con = getDatabaseConnection(source, database);
				query.append("SELECT ").append(columns).append(" FROM ")
						.append(config.getTable()).append(" LIMIT 1");
				stmt = con.createStatement();
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				ResultSetMetaData columnsMetadata = rs.getMetaData();
				int i = 0;
				List<String> columnList = Arrays.asList(columns
						.split("\\s*,\\s*"));
				List<String> numbericType = new ArrayList<>();
				numbericType.add("double");
				numbericType.add("bigint");
				numbericType.add("int8");
				numbericType.add("int2");
				numbericType.add("int4");
				numbericType.add("decimal");
				numbericType.add("float8");
				numbericType.add("float4");
				numbericType.add("tinyint");
				query = new StringBuilder("SELECT ");
				StringBuilder cols = new StringBuilder();
				while (i < columnsMetadata.getColumnCount()) {
					String column = columnList.get(i).toLowerCase();
					if (i != 0)
						cols.append(" , ");

					if (numbericType.contains(columnsMetadata
							.getColumnTypeName(i + 1).toLowerCase())) {
						cols.append("SUM(").append(column).append(") AS ")
								.append(column);
					} else {
						List<String> columnAliasList = Arrays.asList(column
								.split(" as "));
						String columnName = columnAliasList.get(0);
						String columnLabel;
						if (columnAliasList.size() > 1) {
							columnLabel = columnAliasList.get(1);
						} else {
							columnLabel = columnAliasList.get(0);
						}
						cols.append(columnName).append(" AS ")
								.append(columnLabel);
						groupBy.add(columnName);
					}
					i++;
				}
				//cols.append(" , COUNT(*) as _count ");
				query.append(cols).append(" FROM ").append(config.getTable());
				if (filters != null && !"".equalsIgnoreCase(filters.trim()))
					query.append(" WHERE " + filters);
				query.append(" GROUP BY ").append(
						StringUtils.join(groupBy, ","));
			} else {
				if (config.getSource().equalsIgnoreCase(HIVE)) {
					query.append("SELECT ").append(columns)
							//.append(" , 1 as _count ").append(" FROM ")
						.append(" FROM ")
							.append(config.getTable());
				} else {
					query.append("SELECT ").append(columns)
							//.append(" , 1 as _count ").append(" FROM ")
					 .append(" FROM ")
							.append(config.getTable());
				}
				if (filters != null && !"".equalsIgnoreCase(filters.trim()))
					query.append(" WHERE ").append(filters);

			}
		
			  if (E2emfAppUtil.getAppProperty(E2emfConstants.QUERY_LIMIT) !=
			  null && E2emfAppUtil.getAppProperty(
			  E2emfConstants.QUERY_LIMIT).length() > 0) { query.append(" ")
			  .append(E2emfAppUtil
			  .getAppProperty(E2emfConstants.QUERY_LIMIT)); }
			 //query.append(" limit 100");
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return query;
	}
	public E2emfView getData(ProfileConfig config) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataAnalyserView view = new DataAnalyserView();
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		String source;
		String database;
		try {
			source = config.getSource();
			database = config.getDatabase();
			
			// Added by Nagaraj to check DB null being passed from the UI - Start
			/*if(database == null){
				database = "E2EMFPostGres";
			}*/			
			// Added by Nagaraj to check DB null being passed from the UI - END
			
			String dataQuery = config.getQuery();
			con = getDatabaseConnection(source, database);
			if (dataQuery != null && !"".equalsIgnoreCase(dataQuery.trim())){
				String filters = config.getFilter();
				if (filters == null || "".equalsIgnoreCase(filters.trim()))
					filters = " 1=1 ";
				dataQuery = dataQuery.replace("<FILTER>", filters);
				dataQuery = dataQuery.replace("<ORDERBY>","");
				dataQuery = dataQuery.replace("<SEARCH>","");
				query.append(dataQuery);
				/*String filters;
				String where =" WHERE ";
				filters = config.getFilter();
				if(dataQuery.toLowerCase().indexOf("where".toLowerCase()) >-1){
					where = " ";
				}
				if (filters != null && !"".equalsIgnoreCase(filters.trim()))
					query.append(where + filters);*/
			}else{
				query = constructQuery(config);
			}
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				view.setJsonDescriptionArray(getDescription(config));
System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				JSONArray convertedJson = dbUtil.convert(rs);
				view.setJsonArray(convertedJson);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return view;
	}
	public E2emfView getData(ProfileConfig config,boolean isEmailNotification) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataAnalyserView view = new DataAnalyserView();
		StringBuilder query = new StringBuilder();

		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		String source;
		String database;
		try {
			source = config.getSource();
			database = config.getDatabase();
			con = getDatabaseConnection(source, database);
			query = constructQueryForEmail(config);
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				view.setJsonDescriptionArray(getDescription(config));
System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				JSONArray convertedJson = dbUtil.convert(rs);
				view.setJsonArray(convertedJson);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return view;
	}

	private JSONArray getPaginationData(ProfileConfig config,
			String filterQuery, String columnConcatate, String groupBy,
			String columns) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ResultSet countResultset = null;
		String filter;
		String defaultFilter;
		StringBuilder query;
		StringBuilder countQuery;
		String searchValue = "";
		String dataQuery;
		Integer orderby;
		Integer secondOrderBy;
		String secondOrderType;
		String pagenumber;
		String pagesize;
		int rowcount = 0;
		JSONArray convertedJson = null;
		StringBuilder columnConcat;
		try {
			columnConcat = new StringBuilder();
			query = new StringBuilder();
			countQuery = new StringBuilder();
			filter = config.getFilter();
			defaultFilter = config.getDefaultFilter();
			orderby = config.getOrderby();
			secondOrderBy = config.getSecondOrderBy();
			secondOrderType = config.getSecondOrderType();
			dataQuery = config.getQuery();
			pagenumber = config.getPagenumber();
			pagesize = config.getPagesize();
			searchValue = config.getSearchValue();
            if("postgreSQL".equalsIgnoreCase(config.getSource()) ){
				filter = filter.replace("\\'", "''");
				if(searchValue != null && !"".equalsIgnoreCase(searchValue))
            		searchValue = searchValue.replace("\\'", "''");
            }
			con = getDatabaseConnection(config.getSource(),
					config.getDatabase());
			if(dataQuery != null && !"".equalsIgnoreCase(dataQuery)){
				if (filter == null || "".equalsIgnoreCase(filter.trim()))
					filter = " 1=1 ";
				if (searchValue != null
						&& !"".equalsIgnoreCase(searchValue.trim())
						&& !"null".equalsIgnoreCase(searchValue)) {
					StringBuilder searchQuery = new StringBuilder();
					String[] columnList = columns.split(",");
					if (groupBy != null && !"".equalsIgnoreCase(groupBy)) {
						columnList = groupBy.split(",");
					}
					for (int i = 0; i < columnList.length; i++) {
						if (i != 0)
							columnConcat.append(" , ' ', ");
						if (config.getSource().equalsIgnoreCase(HIVE)) {
							columnConcat.append("COALESCE(NULLIF(CAST(")
							.append(columnList[i])
							.append(" AS String ),''),'')");
						}else{
							columnConcat.append("CASE  WHEN ")
							.append(columnList[i])
							.append(" IS NOT NULL THEN cast(")
							.append(columnList[i])
							.append(" AS char) ELSE '' END ");
						}
					}
					searchQuery.append(" AND ( LOWER(CONCAT(").append(columnConcat)
							.append(")) like '%")
							.append(searchValue.toLowerCase()).append("%')");
					filter = filter+searchQuery.toString();
				}
				if (defaultFilter != null && !"".equalsIgnoreCase(defaultFilter.trim())
						&& !"null".equalsIgnoreCase(defaultFilter)) {
					filter = filter+" AND (" + defaultFilter + ")";
				}
				dataQuery = dataQuery.replace("<FILTER>", filter);
				dataQuery = dataQuery.replace("<SEARCH>","");
				if (orderby != null) {
					if(secondOrderBy != null) {
						orderby = orderby + 1;
						secondOrderBy += 1;
						String strOrder = " ORDER BY "+Integer.toString(orderby)+" " + config.getOrderType()+ ", " + Integer.toString(secondOrderBy)+" " + config.getSecondOrderType();// + " NULLS LAST ";
						dataQuery = dataQuery.replace("<ORDERBY>", strOrder);
					} else {
						orderby = orderby + 1;
						dataQuery = dataQuery.replace("<ORDERBY>", " ORDER BY "+Integer.toString(orderby)+" " + config.getOrderType());
					}
				}else{
					dataQuery = dataQuery.replace("<ORDERBY>","");
				}
				query.append(dataQuery);
				/*String where =" WHERE 1=1 ";
				if(dataQuery.toLowerCase().indexOf("where".toLowerCase()) >-1){
					where = " 1=1 ";
				}
				query.append(where);
				query.append(filterQuery);*/
				if (pagesize != null) {
					query.append(" LIMIT  " + pagesize);
				}
				if (pagesize != null) {
					query.append(" OFFSET  " + pagenumber);
				}
			}else{
				countQuery.append("select count(*) as rowcount from ").append(config.getTable() + " WHERE 1=1 ").append(filterQuery);
				if (config.getSource().equalsIgnoreCase(HIVE)) {
					query.append("SELECT ").append(columns).append(" FROM ")
							.append(config.getTable() + " WHERE 1=1 ");
					query.append(filterQuery);
					if (searchValue != null
							&& !"".equalsIgnoreCase(searchValue.trim())
							&& !"null".equalsIgnoreCase(searchValue)) {
						String[] columnList = columns.split(",");
						if (groupBy != null && !"".equalsIgnoreCase(groupBy)) {
							columnList = groupBy.split(",");
						}
						for (int i = 0; i < columnList.length; i++) {
							if (i != 0)
								columnConcat.append(" , ' ', ");

							columnConcat.append("COALESCE(NULLIF(CAST(")
									.append(columnList[i])
									.append(" AS String ),''),'')");
						}
						query.append(" AND ( LOWER(CONCAT(").append(columnConcat)
								.append(")) like '%")
								.append(searchValue.toLowerCase()).append("%')");
					}
					if (groupBy != null && !"".equalsIgnoreCase(groupBy)) {
						query.append(" group by ").append(groupBy);
					}
					if (orderby != null) {
						if(secondOrderBy != null) {
							orderby = orderby + 1;
							secondOrderBy += 1;
							String strOrder = " ORDER BY "+Integer.toString(orderby)+" " + config.getOrderType()+ ", " + Integer.toString(secondOrderBy)+" " + config.getSecondOrderType();// + " NULLS LAST ";
							query.append(strOrder);
						} else {
							orderby = orderby + 1;
							query.append(" ORDER BY ")
									.append(Integer.toString(orderby))
									.append(" " + config.getOrderType());
						}
					}
					if (pagesize != null) {
						query.append(" LIMIT  ").append(pagesize);
					}
					if (pagenumber != null) {
						query.append(" OFFSET  ").append(pagenumber);
					}
				} else {
					query.append("SELECT ").append(columns).append(" FROM ")
							.append(config.getTable() + " WHERE 1=1 ");
					if (filter != null && !"".equalsIgnoreCase(filter.trim())
							&& !"null".equalsIgnoreCase(filter)) {
						filterQuery = " AND (" + filter + ")";
					}
					if (defaultFilter != null && !"".equalsIgnoreCase(defaultFilter.trim())
							&& !"null".equalsIgnoreCase(defaultFilter)) {
						filterQuery = filterQuery+" AND (" + defaultFilter + ")";
					}
					query.append(filterQuery);
					if (searchValue != null
							&& !"".equalsIgnoreCase(searchValue.trim())
							&& !"null".equalsIgnoreCase(searchValue)) {
						String[] columnList = columns.split(",");
						if (groupBy != null && !"".equalsIgnoreCase(groupBy)) {
							columnList = groupBy.split(",");
						}
						for (int i = 0; i < columnList.length; i++) {
							if (i != 0)
								columnConcat.append(" OR ");

							columnConcat.append("(lower(CASE  WHEN ")
									.append(columnList[i])
									.append(" IS NOT NULL THEN cast(")
									.append(columnList[i])
									.append(" AS char) ELSE '' END) like '%"+searchValue.toLowerCase()+"%')");
						}
						query.append(" AND (").append(columnConcat).append(")");
						/*query.append(" AND ( LOWER(").append(columnConcat)
								.append(") like '%")
								.append(searchValue.toLowerCase()).append("%')");*/
					}
					if (groupBy != null && !"".equalsIgnoreCase(groupBy)) {
						query.append(" group by " + groupBy);
					}
					if (orderby != null) {
						
						if(secondOrderBy != null) {
							orderby = orderby + 1;
							secondOrderBy += 1;
							String strOrder = " ORDER BY "+Integer.toString(orderby)+" " + config.getOrderType()+ ", " + Integer.toString(secondOrderBy)+" " + config.getSecondOrderType(); //+ " NULLS LAST ";
							query.append(strOrder);
						} else {
							orderby = orderby + 1;
							query.append(" ORDER BY ")
									.append(Integer.toString(orderby))
									.append(" " + config.getOrderType());
						}
					}
					if (pagesize != null) {
						query.append(" LIMIT  " + pagesize);
					}
					if (pagesize != null) {
						query.append(" OFFSET  " + pagenumber);
					}
				}
			}
			
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				if(pagesize == null && dataQuery == null && "".equalsIgnoreCase(dataQuery)){
					System.out.println(countQuery.toString());
					countResultset = stmt.executeQuery(countQuery.toString());
					while(countResultset.next()){
						rowcount = countResultset.getInt("rowcount");
					}
					if(rowcount>1000){
						Exception newEx = new Exception("There are more than 1000 rows in the database,please configure as server type.");;
						throw newEx;
					}else{
						System.out.println(query.toString());
						rs = stmt.executeQuery(query.toString());
					}
				}else{
					System.out.println(query.toString());
					rs = stmt.executeQuery(query.toString());
				}
				if(rs != null){
					convertedJson = dbUtil.convert(rs);	
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return convertedJson;
	}

	private String getTotalRowCount(ProfileConfig config, String columnConcat,
			String groupBy) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		Statement stmt = null;
		String dataQuery = null;
		ResultSet rs = null;
		StringBuilder query = new StringBuilder();
		String totalCount = null;
		try {
			con = getDatabaseConnection(config.getSource(),
					config.getDatabase());
			stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
					java.sql.ResultSet.CONCUR_READ_ONLY);
			dataQuery = config.getQuery();
			if(dataQuery != null && !"".equalsIgnoreCase(dataQuery)){
				dataQuery = dataQuery.replace("<SEARCH>","");
				dataQuery = dataQuery.replace("<FILTER>", " 1=1");
				if(dataQuery.endsWith("where")){
					dataQuery = dataQuery +" 1=1";
				}
				dataQuery = dataQuery.replace("<ORDERBY>","");
				rs = stmt.executeQuery("select count(*) as count from ( "+dataQuery.toString()+") tab");
				while(rs.next()){
					totalCount = rs.getString("count");
				}							
			}else{
				if (groupBy != null && !"".equalsIgnoreCase(groupBy)) {
					query.append(
							"SELECT COUNT(*) as x,COUNT(*) OVER () AS count  FROM ")
							.append(config.getTable()).append(" group by ")
							.append(groupBy).append(" limit 1 ");
				} else {
					query.append("SELECT COUNT(*) as count FROM ").append(
							config.getTable());
				}
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				if (rs.next()) {
					totalCount = rs.getString("count");
				}
			}
			
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return totalCount;
	}

	private String getFilteredRowCount(ProfileConfig config,
			String columnConcat, String groupBy, String filterQuery)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		String dataQuery = null;
		StringBuilder query = new StringBuilder();
		String filteredCount = null;
		String table;
		try {
			table = config.getTable();
			con = getDatabaseConnection(config.getSource(),
					config.getDatabase());
			stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
					java.sql.ResultSet.CONCUR_READ_ONLY);
			dataQuery = config.getQuery();
			 if("postgreSQL".equalsIgnoreCase(config.getSource()) ){
				 filterQuery = filterQuery.replace("\\'", "''");
	            	//searchValue = searchValue.replace("\\'", "''");
	            }
			if (!"".equalsIgnoreCase(filterQuery.trim())
					|| !"".equalsIgnoreCase(columnConcat.trim())) {
				if(dataQuery != null && !"".equalsIgnoreCase(dataQuery)){
					dataQuery = dataQuery.replace("<SEARCH>","");
					String filterValues = config.getFilter();
					String defaultFilter = config.getDefaultFilter();
					if (defaultFilter != null && !"".equalsIgnoreCase(defaultFilter.trim())
							&& !"null".equalsIgnoreCase(defaultFilter)) {
						filterValues = filterValues+" AND (" + defaultFilter + ")";
					}
					if("postgreSQL".equalsIgnoreCase(config.getSource())){
						dataQuery = dataQuery.replace("<FILTER>", filterValues.replace("\\'", "''"));
					}else{
						dataQuery = dataQuery.replace("<FILTER>", filterValues);
					}
					if(dataQuery.endsWith("where")){
						dataQuery = dataQuery +" 1=1";
					}
					dataQuery = dataQuery.replace("<ORDERBY>","");
				
					rs = stmt.executeQuery("select count(*) as count from ( "+dataQuery.toString()+") tab");
					while(rs.next()){
						filteredCount = rs.getString("count");
					}							
				}else{
					if (groupBy != null && !"".equalsIgnoreCase(groupBy)) {
						query.append(
								"SELECT COUNT(*) as x, COUNT(*) OVER () AS count  FROM ")
								.append(table).append(" WHERE 1=1 ")
								.append(filterQuery).append(columnConcat)
								.append(" group by ").append(groupBy)
								.append(" limit 1 ");
					} else {
						query.append("SELECT COUNT(*) as count FROM ")
								.append(table).append(" WHERE 1=1 ")
								.append(filterQuery).append(columnConcat);
					}
					System.out.println(query.toString());
					rs = stmt.executeQuery(query.toString());
					if (rs.next()) {
						filteredCount = rs.getString("count");
					}
				}
				
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return filteredCount;
	}

	private JSONArray getRowSummary(ProfileConfig config, String columnConcat,
			String filterQuery, ColumnConfigure[] columnsList) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		StringBuilder query = null;
		String table;
		String dataQuery = null;
		JSONArray convertedJson = null;
		try {
			table = config.getTable();
			dataQuery = config.getQuery();
			con = getDatabaseConnection(config.getSource(),
					config.getDatabase());
			stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
					java.sql.ResultSet.CONCUR_READ_ONLY);
			if("postgreSQL".equalsIgnoreCase(config.getSource()) ){
				 filterQuery = filterQuery.replace("\\'", "''");
	            	//searchValue = searchValue.replace("\\'", "''");
	            }
			query = new StringBuilder("SELECT ");
			for (ColumnConfigure column : columnsList) {
				if (column.getSummaryaggregate() != null
						&& !"".equalsIgnoreCase(column.getSummaryaggregate())) {
					if (!"SELECT".equalsIgnoreCase(query.toString().trim()))
						query.append(" , ");

					query.append(column.getSummaryaggregate()).append("(")
							.append(column.getColumnName()).append(")  as ")
							.append(column.getColumnName());
				}
			}
			if(dataQuery != null && !dataQuery.equalsIgnoreCase("")){
				if(filterQuery.trim().startsWith("AND ")){
					filterQuery = "1=1 " + filterQuery ;
				}
				if(columnConcat != null && !"".equalsIgnoreCase(columnConcat)){
					filterQuery = filterQuery + " " + columnConcat;
				}
				if(dataQuery.contains("<FILTER>")){
					dataQuery = dataQuery.replaceAll("<FILTER>", filterQuery.trim());
				}
				if(dataQuery.contains("<ORDERBY>")) {
					dataQuery = dataQuery.replaceAll("<ORDERBY>", "");
				}
				if(dataQuery.contains("<GROUPBY>")) {
					dataQuery = dataQuery.replaceAll("<GROUPBY>", "");
				}
				if(dataQuery.contains("<SEARCH>")) {
					dataQuery = dataQuery.replaceAll("<SEARCH>", "");
				}
				query.append(" FROM ").append("("+dataQuery+") tab");
			}else{
				query.append(" FROM ").append(table).append(" WHERE 1=1 ");
				if (!query.toString().trim().equalsIgnoreCase(filterQuery.trim())
						|| !"".equalsIgnoreCase(columnConcat.trim())) {
					query.append(filterQuery).append(columnConcat);
				}
			}
			
			System.out.println(query.toString());

			rs = stmt.executeQuery(query.toString());
			JSONArray convertedSummaryJson = dbUtil.convert(rs);
			convertedJson = convertedSummaryJson;
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return convertedJson;
	}
	
	public Map<String, DataTableView> getMultipleDataPagination(List<ProfileConfig> configs) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTableView datatableView = new DataTableView();
		HashMap<String, DataTableView> datatableViews = new HashMap<>();
		String filterQuery = "";
		String columnConcat = "";
		String groupBy = "";
		ColumnConfigure[] columnsList = null;
		String source;
		String columns;
		String columnConfigurations;
		String filter;
		String searchValue;
		boolean groupByEnabled;
		String summaryEnabled;
		String dataQuery;
		try {
			if(!configs.isEmpty()){
				for(ProfileConfig config : configs) {
					datatableView = new DataTableView();
					columnConfigurations = config.getColumnConfigurations();
					columns = config.getColumns();
					filter = config.getFilter();
					dataQuery = config.getQuery();
					groupByEnabled = config.isGroupByEnabled();
					searchValue = config.getSearchValue();
					source = config.getSource();
					summaryEnabled = config.getSummaryEnabled();
					Gson gson = new Gson();
					columnsList = gson.fromJson(columnConfigurations,
							ColumnConfigure[].class);
					if (groupByEnabled) {
						columns = "";
						for (ColumnConfigure column : columnsList) {
							if (column.getGroupbyaggregate() != null
									&& !column.getGroupbyaggregate().equalsIgnoreCase(
											"")) {
								if ("" == columns) {
									columns = column.getGroupbyaggregate() + "("
											+ column.getColumnName() + ")  as "
											+ column.getColumnName();
								} else {
									columns += ", " + column.getGroupbyaggregate()
											+ "(" + column.getColumnName() + ")  as "
											+ column.getColumnName();
								}
							} else {
								if ("".equalsIgnoreCase(columns)) {
									columns = column.getColumnName();
								} else {
									columns += ", " + column.getColumnName();
								}

								if ("".equalsIgnoreCase(groupBy)) {
									groupBy = column.getColumnName();
								} else {
									groupBy += ", " + column.getColumnName();
								}
							}
						}
					}
					if (source.equalsIgnoreCase(HIVE)) {
						if (filter != null && !"".equalsIgnoreCase(filter.trim())
								&& !"null".equalsIgnoreCase(filter)) {
							filterQuery = " AND (" + filter + ")";
						}
						if (searchValue != null
								&& !"".equalsIgnoreCase(searchValue.trim())
								&& !"null".equalsIgnoreCase(searchValue)) {
							String[] columnList = columns.split(",");
							if (groupBy != null && !"".equalsIgnoreCase(groupBy)) {
								columnList = groupBy.split(",");
							}
							for (int i = 0; i < columnList.length; i++) {
								if (i == 0)
									columnConcat = "cast(" + columnList[i]
											+ " AS String)";
								else
									columnConcat += ",' ',cast(" + columnList[i]
											+ " AS String)";
							}
							columnConcat = " AND ( LOWER(CONCAT(" + columnConcat
									+ ")) like '%" + searchValue.toLowerCase() + "%')";
						}
					} else {
						if (filter != null && !"".equalsIgnoreCase(filter.trim())
								&& !"null".equalsIgnoreCase(filter)) {
							filterQuery = " AND (" + filter + ")";
						}
						if (searchValue != null
								&& !"".equalsIgnoreCase(searchValue.trim())
								&& !"null".equalsIgnoreCase(searchValue)) {
							String[] columnList = columns.split(",");
							if (groupBy != null && !"".equalsIgnoreCase(groupBy)) {
								columnList = groupBy.split(",");
							}
							for (int i = 0; i < columnList.length; i++) {
								if (i == 0)
									columnConcat = "CASE  WHEN " + columnList[i]
											+ " IS NOT NULL THEN cast(" + columnList[i]
											+ " AS text) ELSE '' END";
								else
									columnConcat += " || ' ' || CASE WHEN "
											+ columnList[i] + " IS NOT NULL THEN cast("
											+ columnList[i] + " AS text) ELSE '' END";
							}
							columnConcat = " AND ( LOWER(" + columnConcat + ") like '%"
									+ searchValue.toLowerCase() + "%')";
						}
					}
					JSONArray paginationData = getPaginationData(config, filterQuery,
							columnConcat, groupBy, columns);
					datatableView.setData(paginationData);
					//if(dataQuery == null || "".equalsIgnoreCase(dataQuery)){
						String totalCount = getTotalRowCount(config, columnConcat, groupBy);
						datatableView.setRecordsTotal(totalCount);
						datatableView.setRecordsFiltered(totalCount);
						if (columnConfigurations != null) {
							String filteredCount = getFilteredRowCount(config,
									columnConcat, groupBy, filterQuery);
							datatableView.setRecordsFiltered(filteredCount);
						}
					//}		

					if (summaryEnabled != null
							&& "true".equalsIgnoreCase(summaryEnabled.trim())) {
						JSONArray summaryData = getRowSummary(config, columnConcat,
								filterQuery, columnsList);
						datatableView.setSummaryData(summaryData);
					} else {
						datatableView.setSummaryData(new JSONArray());
					}
					datatableViews.put(config.getTable(), datatableView);
				}
			}
		} catch(Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return datatableViews;
	}
	
	public E2emfView getDataPagination(ProfileConfig config) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTableView datatableView = new DataTableView();
		String filterQuery = "";
		String columnConcat = "";
		String groupBy = "";
		ColumnConfigure[] columnsList = null;
		String source;
		String columns;
		String columnConfigurations;
		String filter;
		String defaultFilter;
		String searchValue;
		boolean groupByEnabled;
		String summaryEnabled;
		try {
			columnConfigurations = config.getColumnConfigurations();
			columns = config.getColumns();
			filter = config.getFilter();
			defaultFilter = config.getDefaultFilter();
			groupByEnabled = config.isGroupByEnabled();
			searchValue = config.getSearchValue();
			source = config.getSource();
			summaryEnabled = config.getSummaryEnabled();
			Gson gson = new Gson();
			columnsList = gson.fromJson(columnConfigurations,
					ColumnConfigure[].class);
			if (groupByEnabled) {
				columns = "";
				for (ColumnConfigure column : columnsList) {
					if (column.getGroupbyaggregate() != null
							&& !column.getGroupbyaggregate().equalsIgnoreCase(
									"")) {
						if ("" == columns) {
							columns = column.getGroupbyaggregate() + "("
									+ column.getColumnName() + ")  as "
									+ column.getColumnName();
						} else {
							columns += ", " + column.getGroupbyaggregate()
									+ "(" + column.getColumnName() + ")  as "
									+ column.getColumnName();
						}
					} else {
						if ("".equalsIgnoreCase(columns)) {
							columns = column.getColumnName();
						} else {
							columns += ", " + column.getColumnName();
						}

						if ("".equalsIgnoreCase(groupBy)) {
							groupBy = column.getColumnName();
						} else {
							groupBy += ", " + column.getColumnName();
						}
					}
				}
			}
			if (source.equalsIgnoreCase(HIVE)) {
				if (filter != null && !"".equalsIgnoreCase(filter.trim())
						&& !"null".equalsIgnoreCase(filter)) {
					filterQuery = " AND (" + filter + ")";
				}
				if (defaultFilter != null && !"".equalsIgnoreCase(defaultFilter.trim())
						&& !"null".equalsIgnoreCase(defaultFilter)) {
					filterQuery = " AND (" + defaultFilter + ")";
				}
				if (searchValue != null
						&& !"".equalsIgnoreCase(searchValue.trim())
						&& !"null".equalsIgnoreCase(searchValue)) {
					String[] columnList = columns.split(",");
					if (groupBy != null && !"".equalsIgnoreCase(groupBy)) {
						columnList = groupBy.split(",");
					}
					for (int i = 0; i < columnList.length; i++) {
						if (i == 0)
							columnConcat = "cast(" + columnList[i]
									+ " AS String)";
						else
							columnConcat += ",' ',cast(" + columnList[i]
									+ " AS String)";
					}
					columnConcat = " AND ( LOWER(CONCAT(" + columnConcat
							+ ")) like '%" + searchValue.toLowerCase() + "%')";
				}
			} else {
				if (filter != null && !"".equalsIgnoreCase(filter.trim())
						&& !"null".equalsIgnoreCase(filter)) {
					filterQuery = " AND (" + filter + ")";
				}
				if (defaultFilter != null && !"".equalsIgnoreCase(defaultFilter.trim())
						&& !"null".equalsIgnoreCase(defaultFilter)) {
					filterQuery = " AND (" + defaultFilter + ")";
				}
				if (searchValue != null
						&& !"".equalsIgnoreCase(searchValue.trim())
						&& !"null".equalsIgnoreCase(searchValue)) {
					String[] columnList = columns.split(",");
					if (groupBy != null && !"".equalsIgnoreCase(groupBy)) {
						columnList = groupBy.split(",");
					}
					for (int i = 0; i < columnList.length; i++) {
						if (i == 0)
							columnConcat = "LOWER((CASE  WHEN " + columnList[i]
									+ " IS NOT NULL THEN cast(" + columnList[i]
									+ " AS char) ELSE '' END) LIKE '%"+searchValue.toLowerCase()+"%')";
						else
							columnConcat += " OR lower((CASE WHEN "
									+ columnList[i] + " IS NOT NULL THEN cast("
									+ columnList[i] + " AS char) ELSE '' END) LIKE '%"+searchValue.toLowerCase()+"%')";
					}
					columnConcat = " AND ("+columnConcat+")";
					/*columnConcat = " AND ( LOWER(" + columnConcat + ") like '%"
							+ searchValue.toLowerCase() + "%')";*/
				}
			}
			JSONArray paginationData = getPaginationData(config, filterQuery,
					columnConcat, groupBy, columns);
			datatableView.setData(paginationData);
				String totalCount = getTotalRowCount(config, columnConcat, groupBy);
				datatableView.setRecordsTotal(totalCount);
				datatableView.setRecordsFiltered(totalCount);
				if (columnConfigurations != null) {
					String filteredCount = getFilteredRowCount(config,
							columnConcat, groupBy, filterQuery);
					datatableView.setRecordsFiltered(filteredCount);
				}

			if (summaryEnabled != null
					&& "true".equalsIgnoreCase(summaryEnabled.trim())) {
				JSONArray summaryData = getRowSummary(config, columnConcat,
						filterQuery, columnsList);
				datatableView.setSummaryData(summaryData);
			} else {
				datatableView.setSummaryData(new JSONArray());
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return datatableView;
	}
	
	/* getDataTableClientSIde */
	public E2emfView getDataTableClientSide(String source, String database,
			String table, String columns, String columnConfigurations,
			String filter, Integer orderby, String orderType,
			String groupByEnabled, String summaryEnabled) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTableView datatableView = new DataTableView();
		StringBuilder query = new StringBuilder();
		String filterQuery = "";
		String columnConcat = "";

		ResultSet rs = null;
		String groupBy = "";
		ColumnConfigure[] columnsList = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = getDatabaseConnection(source, database);
			Gson gson = new Gson();
			if (columnConfigurations != null) {
				columnsList = gson.fromJson(columnConfigurations,
						ColumnConfigure[].class);
				if (groupByEnabled != null
						&& "true".equalsIgnoreCase(groupByEnabled.trim())) {
					columns = "";
					for (ColumnConfigure column : columnsList) {
						if (column.getGroupbyaggregate() != null
								&& !column.getGroupbyaggregate()
										.equalsIgnoreCase("")) {
							if (columns == "") {
								columns = column.getGroupbyaggregate() + "("
										+ column.getColumnName() + ")  as "
										+ column.getColumnName();
							} else {
								columns += ", " + column.getGroupbyaggregate()
										+ "(" + column.getColumnName()
										+ ")  as " + column.getColumnName();
							}
						} else {
							if ("".equalsIgnoreCase(columns)) {
								columns = column.getColumnName();
							} else {
								columns += ", " + column.getColumnName();
							}

							if ("".equalsIgnoreCase(groupBy)) {
								groupBy = column.getColumnName();
							} else {
								groupBy += ", " + column.getColumnName();
							}
						}
					}
				}
			}
			if (source.equalsIgnoreCase(HIVE)) {
				query.append("SELECT " + columns + " FROM " + table
						+ " WHERE 1=1 ");
				if (filter != null && !"".equalsIgnoreCase(filter.trim())
						&& !"null".equalsIgnoreCase(filter)) {
					filterQuery = " AND (" + filter + ")";
					query.append(filterQuery);
				}
				if (groupBy != null && !"".equalsIgnoreCase(groupBy)) {
					query.append(" group by " + groupBy);
				}
				if (orderby != null) {
					orderby = orderby + 1;
					query.append(" ORDER BY " + Integer.toString(orderby) + " "
							+ orderType);
				}

			} else {
				query.append("SELECT " + columns + " FROM " + table
						+ " WHERE 1=1 ");
				if (filter != null && !"".equalsIgnoreCase(filter.trim())
						&& !"null".equalsIgnoreCase(filter)) {
					filterQuery = " AND (" + filter + ")";
					query.append(filterQuery);
				}
				if (groupBy != null && !"".equalsIgnoreCase(groupBy)) {
					query.append(" group by " + groupBy);
				}
				if (orderby != null) {
					orderby = orderby + 1;
					query.append(" ORDER BY " + Integer.toString(orderby) + " "
							+ orderType);
				}

			}

			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				JSONArray convertedJson = dbUtil.convert(rs);
				datatableView.setData(convertedJson);
				String totalCountQuery = "";
				if (groupBy != null && !"".equalsIgnoreCase(groupBy)) {
					totalCountQuery = "SELECT COUNT(*) as x, count(*),COUNT(*) OVER () AS count  FROM "
							+ table + " group by " + groupBy + " limit 1 ";
					
				} else {
					totalCountQuery = "SELECT COUNT(*) as count FROM " + table;
				}
				if (rs != null)
					rs.close();
				System.out.println(totalCountQuery);
				rs = stmt.executeQuery(totalCountQuery);
				while (rs.next()) {
					datatableView.setRecordsTotal(rs.getString("count"));
					datatableView.setRecordsFiltered(rs.getString("count"));
				}
				if (!"".equalsIgnoreCase(filterQuery.trim())
						|| !"".equalsIgnoreCase(columnConcat.trim())) {
					String filterNumQuery = "";
					if (groupBy != null && !groupBy.equalsIgnoreCase("")) {
						filterNumQuery = "SELECT COUNT(*) as x, count(*),COUNT(*) OVER () AS count  FROM "
								+ table
								+ " WHERE 1=1 "
								+ filterQuery
								+ columnConcat
								+ " group by "
								+ groupBy
								+ " limit 1 ";
					} else {
						filterNumQuery = "SELECT COUNT(*) as count FROM "
								+ table + " WHERE 1=1 " + filterQuery
								+ columnConcat;
					}
					if (rs != null)
						rs.close();
					System.out.println(filterNumQuery);
					rs = stmt.executeQuery(filterNumQuery);
					if (rs != null) {
						while (rs.next()) {
							datatableView.setRecordsFiltered(rs
									.getString("count"));
						}
					}
				}

				if (summaryEnabled != null
						&& "true".equalsIgnoreCase(summaryEnabled.trim())) {
					String summaryQuery = "";

					for (ColumnConfigure column : columnsList) {
						if (column.getSummaryaggregate() != null
								&& !column.getSummaryaggregate()
										.equalsIgnoreCase("")) {
							if ("".equalsIgnoreCase(summaryQuery.trim())) {
								summaryQuery += "SELECT "
										+ column.getSummaryaggregate() + "("
										+ column.getColumnName() + ")  as "
										+ column.getColumnName();
							} else {
								summaryQuery += " , "
										+ column.getSummaryaggregate() + "("
										+ column.getColumnName() + ")  as "
										+ column.getColumnName();
							}
						}
					}

					summaryQuery += " FROM " + table + " WHERE 1=1 ";
					if (!summaryQuery.trim().equalsIgnoreCase(
							filterQuery.trim())
							|| !"".equalsIgnoreCase(columnConcat.trim())) {
						summaryQuery += filterQuery + columnConcat;
					}
					System.out.println(summaryQuery);
					rs = stmt.executeQuery(summaryQuery);
					JSONArray convertedSummaryJson = dbUtil.convert(rs);
					datatableView.setSummaryData(convertedSummaryJson);
				} else {
					datatableView.setSummaryData(new JSONArray());
				}

			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);

		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return datatableView;

	}
	//getDataFromExcel
	public String getDataFromExcel(String fileName){
		String response = null;
		Gson gson = new Gson();
		JSONArray convertedJson = null;
		try{
			FileInputStream fis = new FileInputStream(fileName);
			// Create Workbook instance for xlsx/xls file input stream
			Workbook workbook = null;
			if (fileName.toLowerCase().endsWith("xlsx")) {
				workbook = new XSSFWorkbook(fis);
			} else if (fileName.toLowerCase().endsWith("xls")) {
				workbook = new HSSFWorkbook(fis);
			}
			int numberOfSheets = workbook.getNumberOfSheets();
			for (int i = 0; i < numberOfSheets; i++) {
				// Get the nth sheet from the workbook
				Sheet sheet = workbook.getSheetAt(i);
				// every sheet has rows, iterate over them
				convertedJson = new JSONArray();
				Iterator<Row> rowIterator = sheet.iterator();
				int rowcount = 0;
				String[] columns = null;
				while (rowIterator.hasNext()) {					
					Row row = rowIterator.next();
					LinkedHashMap<String, String> jsonOrderedMap = new LinkedHashMap<String, String>();
					if(rowcount == 0){
						columns = new String[row.getLastCellNum()];
						for (int j = 0; j < row.getLastCellNum(); j++) {
							Cell cell = row.getCell(j, Row.CREATE_NULL_AS_BLANK);
							columns[j] = cell.getStringCellValue().trim();
						}
					}else{
						jsonOrderedMap = new LinkedHashMap<String, String>();
						for (int cn = 0; cn < row.getLastCellNum(); cn++) {
							Cell cell = row.getCell(cn, Row.CREATE_NULL_AS_BLANK);
							if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
								jsonOrderedMap.put(columns[cn],String.valueOf(cell.getNumericCellValue()));
							}else{
								jsonOrderedMap.put(columns[cn],cell.getStringCellValue().trim());	
							}
						}
						JSONObject orderedJson = new JSONObject(jsonOrderedMap);
						convertedJson.put(orderedJson);
					}
					rowcount++;
				}
			}
			response =  convertedJson.toString();
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		return response;
	}
	public int saveProfile(ProfileConfig profileConfig/*
													 * configString id, String
													 * displayname, String
													 * source, String database,
													 * String table, String
													 * fact, String customfacts,
													 * String config, String
													 * filters, String summary,
													 * String createdby, String
													 * remarks, String type,
													 * Integer stackcount,
													 * String additionalconfig
													 */) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		int maxProfileId = 0;
		Connection con = null;
		Statement stmt = null;
		int id;
		String displayname;
		String source;
		String database;
		String table;
		String fact;
		String customFacts;
		String config;
		String filters;
		String summary;
		String createdby;
		String remarks;
		String type;
		String app;
		Integer stackcount;
		String additionalConfig;
		String notificationFilter;
		try {
			id = profileConfig.getId();
			displayname = profileConfig.getDisplayname();
			source = profileConfig.getSource();
			database = profileConfig.getDatabase();
			table = profileConfig.getTable();
			fact = profileConfig.getFact();
			customFacts = profileConfig.getCustomFacts();
			config = profileConfig.getConfig();
			filters = profileConfig.getFilter();
			summary = profileConfig.getSummary();
			createdby = profileConfig.getCreatedBy();
			remarks = profileConfig.getRemarks();
			type = profileConfig.getType();
			app = profileConfig.getApp();
			stackcount = profileConfig.getStackCount();
			additionalConfig = profileConfig.getAdditionalconfig();
			notificationFilter = profileConfig.getNotificationFilter();
			con = dbUtil.getPostGresConnection();

			if (con != null) {
				stmt = con.createStatement();
				if (config != null && !"".equalsIgnoreCase(config.trim()))
					config = config.replaceAll("'", "@");
				if (filters != null && !"".equalsIgnoreCase(filters.trim()))
					filters = filters.replaceAll("'", "@");
				if (summary != null && !"".equalsIgnoreCase(summary.trim()))
					summary = summary.replaceAll("'", "@");
				if (id == 0) {
					String selectMaxQuery = "select max(id) from profile_analyser";
					System.out.println(selectMaxQuery);
					rs = stmt.executeQuery(selectMaxQuery);
					if (rs.next()) {
						maxProfileId = rs.getInt(1);
					}
					maxProfileId = maxProfileId + 1;
/*					query.append("INSERT INTO profile_analyser(id, displayname, source, `database`, fact,customfacts,configtable, config,filters,summary, createdby,remarks,type,stackcount,additional_config,app,notification_filter) VALUES ("
							+ maxProfileId
							+ ",'"
							+ displayname
							+ "','"
							+ source
							+ "','"
							+ database
							+ "','"
							+ fact
							+ "','"
							+ customFacts
							+ "','"
							+ table
							+ "','"
							+ config
							+ "','"
							+ filters
							+ "','"
							+ summary
							+ "','"
							+ createdby
							+ "','"
							+ remarks
							+ "','"
							+ type
							+ "',"
							+ stackcount
							+ ",'"
							+ additionalConfig
							+ "','"
							+ app
							+ "','"
							+ notificationFilter + "');");
					stmt.executeUpdate(query.toString());*/
					
					String str = "INSERT INTO profile_analyser(id, displayname, source, `database`, fact,customfacts,configtable, config,filters,summary, createdby,remarks,type,stackcount,additional_config,app,notification_filter) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement preparedStatement = con.prepareStatement(str);
					preparedStatement.setInt(1, maxProfileId);
					preparedStatement.setString(2, displayname);
					preparedStatement.setString(3, source);
					preparedStatement.setString(4, database);
					preparedStatement.setString(5, fact);
					preparedStatement.setString(6, customFacts);
					preparedStatement.setString(7, table);
					preparedStatement.setString(8, config);
					preparedStatement.setString(9, filters);
					preparedStatement.setString(10, summary);
					preparedStatement.setString(11, createdby);									
					preparedStatement.setString(12, remarks);
					preparedStatement.setString(13, type);
					preparedStatement.setInt(14, stackcount);
					preparedStatement.setString(15, additionalConfig);
					preparedStatement.setString(16, app);
					preparedStatement.setString(17, notificationFilter);
					
					preparedStatement.executeUpdate();
				} else {					
						maxProfileId = id;
						String str = "UPDATE profile_analyser SET displayname=?,source=?,`database`=?,configtable=?,config=?,lastmodifiedby=?,fact=?,customfacts=?,remarks=?,type=?,stackcount=?,filters=?,summary=?,additional_config=?,notification_filter=?,app=?,lastmodifiedon = NOW() where id=?";
						PreparedStatement preparedStatement = con.prepareStatement(str);
						preparedStatement.setString(1, displayname);
						preparedStatement.setString(2, source);
						preparedStatement.setString(3, database);
						preparedStatement.setString(4, table);
						preparedStatement.setString(5, config);
						preparedStatement.setString(6, createdby);
						preparedStatement.setString(7, fact);
						preparedStatement.setString(8, customFacts);
						preparedStatement.setString(9, remarks);
						preparedStatement.setString(10, type);
						preparedStatement.setInt(11, stackcount);
						preparedStatement.setString(12, filters);
						preparedStatement.setString(13, summary);
						preparedStatement.setString(14, additionalConfig);
						preparedStatement.setString(15, notificationFilter);
						preparedStatement.setString(16, app);
						preparedStatement.setInt(17, id);
						
						preparedStatement.executeUpdate();
				}
				String logsQuery = "INSERT INTO profile_analyser_logs(refid, displayname, source, `database`, configtable, config,createdby, createdon, lastmodifiedby, lastmodifiedon, fact, customfacts, remarks,profilekey, actionid, type, stackcount, filters, summary, additional_config)"
						+ " SELECT id, displayname, source, `database`, configtable, config, createdby,createdon, lastmodifiedby, lastmodifiedon, fact, customfacts, remarks, profilekey,actionid, type, stackcount, filters, summary, additional_config   FROM profile_analyser"
						+ " WHERE id ='" + maxProfileId + "'";
				stmt.executeUpdate(logsQuery);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);

		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		
		
		return maxProfileId;
		
			
		}
		
	

	public DataTableView getMyProfiles(String createdby,String type)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTableView datatableView = new DataTableView();
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbUtil.getPostGresConnection();
			query.append("SELECT id, p.displayname, source, `database`, fact, customfacts, configtable, createdby, createdon, lastmodifiedby, lastmodifiedon,remarks,profilekey,actionid,type,stackcount,r.displayname as app FROM profile_analyser as p LEFT OUTER JOIN roles as r ON p.app = r.name WHERE 1=1");
			if (createdby != null && !"".equalsIgnoreCase(createdby)) {
				query.append(" AND createdby = '" + createdby + "'");
			}
			if (type != null && !"".equalsIgnoreCase(type)) {
				query.append(" AND type = '" + type + "'");
			}
			
			query.append(" ORDER by createdon DESC ");
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				JSONArray convertedJson = dbUtil.convert(rs);
				datatableView.setData(convertedJson);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);

		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return datatableView;
	}

	public DataTableView getProfileHistory(String refid) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTableView datatableView = new DataTableView();
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbUtil.getPostGresConnection();
			query.append("SELECT id, displayname, source, `database`, fact, customfacts, configtable, createdby, createdon, lastmodifiedby, lastmodifiedon,remarks,profilekey,actionid,type,stackcount FROM profile_analyser_logs WHERE refid='"
					+ refid + "'");

			query.append(" ORDER by lastmodifiedon DESC ");
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				JSONArray convertedJson = dbUtil.convert(rs);
				datatableView.setData(convertedJson);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);

		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return datatableView;
	}

	public DataTableView getProfileConfig(ProfileConfig profileConfig)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTableView datatableView = new DataTableView();
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		ProfileView profileView = null;
		String createdBy;
		String profileKey;
		String actionID;
		String displayName;
		int id;
		String ids;
		int viewID;
		String dataSourceId=null;
		try {
			JSONArray dataQueryArr = new JSONArray();
			createdBy = profileConfig.getCreatedBy();
			profileKey = profileConfig.getProfileKey();
			id = profileConfig.getId();
			ids = profileConfig.getIds();
			viewID = profileConfig.getViewID();
			actionID = profileConfig.getActionId();
			displayName = profileConfig.getDisplayname();
			con = dbUtil.getPostGresConnection();
			query.append("SELECT a.id,a.app, displayname, config ");
			query.append(", a.summary,additional_config, source, `database`, fact, customfacts, configtable, createdby, createdon, lastmodifiedby, lastmodifiedon, remarks, type, stackcount,notification_filter ");
			query.append(", a.filters AS filters");
			if(viewID < 0){
				query.append(", 0 as viewid");
			}else{
				query.append(", v.filters  AS viewFilters");
				query.append(", CASE WHEN v.view_setting is null THEN null ELSE  v.view_setting END as view_setting");
				query.append(", CASE WHEN v.id is null then 0 else v.id end as viewid");
				query.append(", CASE WHEN v.name is null then null else v.name end as viewname");
				query.append(", CASE WHEN v.isdefault is null then null else v.isdefault end as isdefault");
				query.append(", CASE WHEN v.ispublic is null then null else v.ispublic end as ispublic");
				query.append(", CASE WHEN v.filter_query is null then '' else v.filter_query end as filterquery");
			}
			
			query.append(", CASE WHEN created_by='").append(createdBy)
					.append("' THEN TRUE else FALSE end as viewtype");
			query.append(" FROM profile_analyser AS a LEFT OUTER JOIN profile_views as v");
			query.append(" ON a.id = v.profile_id");
			if (viewID > 0) {
				query.append(" AND v.id = ").append(viewID);
			} else if (viewID != -1) {
				query.append(" AND v.isdefault = TRUE");
				query.append(" and v.created_by = '").append(createdBy)
						.append("'");
			}
			if (profileKey != null && !"".equalsIgnoreCase(profileKey)) {
				query.append(" WHERE profilekey = '" + profileKey + "'");
			} else if (ids != null && !"".equalsIgnoreCase(ids)) {
				query.append(" WHERE a.id in (" + ids + ")");
			}  else if (id > 0) {
				query.append(" WHERE a.id = '" + id + "'");
			}else if (actionID != null && !"".equalsIgnoreCase(actionID)){
				query.append(" WHERE a.actionid = '" + actionID + "'");
			} else if(displayName != null && !"".equalsIgnoreCase(displayName)) {
				query.append(" WHERE a.displayname = '" + displayName + "'");
			}
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				JSONArray convertedJson = dbUtil.convert(rs);
				datatableView.setData(convertedJson);				
				for(int cj=0;cj<convertedJson.length();cj++){
					if(!convertedJson.getJSONObject(cj).isNull("additional_config") && !("".equals(convertedJson.getJSONObject(cj).getString("additional_config")))){
						String additionalConfig = convertedJson.getJSONObject(cj).getString("additional_config");					
						org.json.simple.JSONObject json = null;
					     //Check if the object is of JSON or not.
					    try{
					      json = (org.json.simple.JSONObject)new JSONParser().parse(additionalConfig);
					    } catch(Exception ex) {
					      json = null;
					    }
					    if(json != null && json.get("dataSourceId") != null){
					    	if(dataSourceId != null){
					    		dataSourceId =dataSourceId +","+ (String) json.get("dataSourceId");
					    	}else{
					    		dataSourceId = (String) json.get("dataSourceId");
					    	}
					     }
//						if(json != null && json.get("dataSourceId") != null){
//							ResultSet dataQueryRs = null;
//							dataQueryRs = stmt.executeQuery("select config from profile_analyser WHERE id = '" + json.get("dataSourceId") + "'");
//							while (dataQueryRs.next()) {
//								datatableView.setDataQuery(dataQueryRs.getString("config"));
//							}
//						}
					}
				}
			
				if(dataSourceId != null){
				ResultSet dataQueryRs = null;
				dataQueryRs = stmt.executeQuery("select config,id from profile_analyser WHERE id in (" + dataSourceId +")");
					while (dataQueryRs.next()) {
						if(ids !=null){
							JSONObject dataQueryObj = new JSONObject(dataQueryRs.getString("config"));
							dataQueryObj.put("profileID",Integer.toString(dataQueryRs.getInt("id")));
							dataQueryArr.put(dataQueryObj);
							datatableView.setDataQuery(dataQueryArr.toString());
						}else{
						datatableView.setDataQuery(dataQueryRs.getString("config"));
						}
					}
				}
	
			}
			// to get views related to profile.
			profileView = new ProfileView();
			profileView.setProfileId(datatableView.getData().getJSONObject(0).getInt("id"));
			profileView.setCreatedBy(createdBy);
			datatableView.setProfileViews(getViews(profileView));

			// to get email notification of current view
			EmailNotificationDao emailNotificationDao = new EmailNotificationDao();
			profileView.setId(datatableView.getData().getJSONObject(0).getInt("viewid"));
			datatableView.setEmailNotification(emailNotificationDao.getEmailNotification(profileView));
			ProfileActionsDao profileActionsDao = new ProfileActionsDao();
			datatableView.setProfileActions(profileActionsDao.getProfileActions(profileView.getProfileId(), 0,ids));
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);

		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return datatableView;
	}

	public E2emfView getProfileHistoryConfig(String id) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTableView datatableView = new DataTableView();
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbUtil.getPostGresConnection();
			query.append("SELECT id, displayname,config,filters,summary,additional_config, source, `database`,fact, customfacts, configtable, createdby, createdon, lastmodifiedby, lastmodifiedon, remarks,type,stackcount FROM profile_analyser_logs WHERE id= "
					+ id);
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				JSONArray convertedJson = dbUtil.convert(rs);
				datatableView.setData(convertedJson);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);

		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return datatableView;
	}

	public StreamingOutput downloadDatatableToExcel(
			DataTable datatableExport) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Report report = null;
		String reportName = null;
		String queryToExecute = null;
		String tableName = null;
		String filter = null;
		StringBuilder where = new StringBuilder(" WHERE 1=1 ");
		String columns = null;
		String searchValue = null;
		String columnConcat = null;
		Integer orderby = 0;

		String orderType = null;
		String source = null;
		String dataQuery = null;
		String database = null;
		String groupBy = null;
		Connection con = null;
		Statement stmt = null;
		StreamingOutput reportdata = null;
		String paramCon = null;
		try {

			source = datatableExport.getSource();
			database = datatableExport.getDatabase();
			paramCon = getDatabaseName(database);
			con = getDatabaseConnection(source, database);
			reportName = datatableExport.getDisplayName();
			tableName = datatableExport.getTable();
			dataQuery = datatableExport.getQuery();
			UserDao userDao = new UserDao();
			columns = datatableExport.getColumns();			
			String initialQuery = "SELECT " + columns.replaceAll("'", "\"")
					+ " FROM ";
			filter = datatableExport.getFilter();
			groupBy = datatableExport.getGroupBy();
			orderby = Integer.parseInt(datatableExport.getOrderField());
			orderType = datatableExport.getOrderType();
			
			if((dataQuery != null && !"".equalsIgnoreCase(dataQuery)) && 
					((dataQuery.contains("<FILTER>") && filter != null && !"".equalsIgnoreCase(filter.trim())
					&& !"null".equalsIgnoreCase(filter)) || (dataQuery.contains("<GROUPBY>") && groupBy != null && !"".equalsIgnoreCase(groupBy.trim())
					&& !"null".equalsIgnoreCase(groupBy)) || (dataQuery.contains("<ORDERBY>") && orderby != null))) {
				where = new StringBuilder(""); //Re-initiate String builder.
			}
			if (filter != null && !"".equalsIgnoreCase(filter.trim())
					&& !"null".equalsIgnoreCase(filter)) {
				if(dataQuery != null && dataQuery.contains("<FILTER>")) {
					dataQuery = dataQuery.replaceAll("<FILTER>", filter);
				} else {
					where.append(" AND (" + filter + ")");
				}
			}
			searchValue = datatableExport.getSearchValue();
			if (searchValue != null && !"".equalsIgnoreCase(searchValue.trim())
					&& !"null".equalsIgnoreCase(searchValue)) {
				String[] columnList = datatableExport.getDatabaseColumns()
						.split(",");
				String fieldtype = source.equalsIgnoreCase("postgreSQL") ? "text"
						: "String";
				for (int i = 0; i < columnList.length; i++) {
					if (i == 0)
						columnConcat = "CAST(" + columnList[i] + " AS "
								+ fieldtype + ")";
					else
						columnConcat += ",' ',CAST(" + columnList[i] + " AS "
								+ fieldtype + " )";
				}
				if(dataQuery != null && !"".equalsIgnoreCase(dataQuery) ){
					dataQuery = dataQuery.replaceAll("<SEARCH>"," AND ( LOWER(CONCAT(" + columnConcat
							+ ")) like '%" + searchValue.toLowerCase() + "%')");
				} else {
					where.append(" AND ( LOWER(CONCAT(" + columnConcat
							+ ")) like '%" + searchValue.toLowerCase() + "%')");
				}
			} else {
				if(dataQuery != null && !"".equalsIgnoreCase(dataQuery) ){
					dataQuery = dataQuery.replaceAll("<SEARCH>","");
				}
			}
			
			if (groupBy != null) {
				if(dataQuery != null && dataQuery.contains("<GROUPBY>")) {
					dataQuery = dataQuery.replaceAll("<GROUPBY>"," GROUP BY " + groupBy + " ");
				} else {
					where.append(" GROUP BY " + groupBy + " ");
				}
			}
			
			if (orderType == null || "null".equalsIgnoreCase(orderType))
				orderType = " DESC";
			if (orderby != null) {
				orderby = orderby + 1;
				if(dataQuery != null && dataQuery.contains("<ORDERBY>")) {
					dataQuery = dataQuery.replaceAll("<ORDERBY>", " ORDER BY " + Integer.toString(orderby) + " "
							+ orderType);
				} else {
					where.append(" ORDER BY " + Integer.toString(orderby) + " "
							+ orderType);
				}
				
			}
			if(dataQuery != null && !"".equalsIgnoreCase(dataQuery)){
				queryToExecute = new StringBuilder(dataQuery)
						.append(where.toString()).toString();
			}else{
				queryToExecute = new StringBuilder(initialQuery).append(tableName)
						.append(where.toString()).toString();
			}
			report = new Report();
			report.query = queryToExecute;
			report.name = reportName;
			report.dbname = paramCon;
			report.source = source;
			reportdata = userDao.getReportsAndGenerateExcel(report);
			_logger.info("Fetched Report object from DAO " + report);
		} catch (Exception ex) {
			Log.Error("Exception occured :" + ex.getMessage());
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, null, stmt);
			Log.Info("All Resources are closed :");
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportdata;
	}
	public File downloadDatatableToCSV(
			DataTable datatableExport) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Report report = null;
		String reportName = null;
		String queryToExecute = null;
		String tableName = null;
		String filter = null;
		StringBuilder where = new StringBuilder(" WHERE 1=1 ");
		String columns = null;
		String searchValue = null;
		String columnConcat = null;
		Integer orderby = 0;

		String orderType = null;
		String source = null;
		String dataQuery = null;
		String database = null;
		String groupBy = null;
		Connection con = null;
		Statement stmt = null;
		File reportdata = null;
		String paramCon = null;
		try {

			source = datatableExport.getSource();
			database = datatableExport.getDatabase();
			paramCon = getDatabaseName(database);
			con = getDatabaseConnection(source, database);
			reportName = datatableExport.getDisplayName();
			tableName = datatableExport.getTable();
			dataQuery = datatableExport.getQuery();
			UserDao userDao = new UserDao();
			columns = datatableExport.getColumns();			
			String initialQuery = "SELECT " + columns.replaceAll("'", "\"")
					+ " FROM ";
			filter = datatableExport.getFilter();
			groupBy = datatableExport.getGroupBy();
			orderby = Integer.parseInt(datatableExport.getOrderField());
			orderType = datatableExport.getOrderType();
			
			if((dataQuery != null && !"".equalsIgnoreCase(dataQuery)) && 
					((dataQuery.contains("<FILTER>") && filter != null && !"".equalsIgnoreCase(filter.trim())
					&& !"null".equalsIgnoreCase(filter)) || (dataQuery.contains("<GROUPBY>") && groupBy != null && !"".equalsIgnoreCase(groupBy.trim())
					&& !"null".equalsIgnoreCase(groupBy)) || (dataQuery.contains("<ORDERBY>") && orderby != null))) {
				where = new StringBuilder(""); //Re-initiate String builder.
			}
			if (filter != null && !"".equalsIgnoreCase(filter.trim())
					&& !"null".equalsIgnoreCase(filter)) {
				if(dataQuery != null && dataQuery.contains("<FILTER>")) {
					dataQuery = dataQuery.replaceAll("<FILTER>", filter);
				} else {
					where.append(" AND (" + filter + ")");
				}
			}
			searchValue = datatableExport.getSearchValue();
			if (searchValue != null && !"".equalsIgnoreCase(searchValue.trim())
					&& !"null".equalsIgnoreCase(searchValue)) {
				String[] columnList = datatableExport.getDatabaseColumns()
						.split(",");
				String fieldtype = source.equalsIgnoreCase("postgreSQL") ? "text"
						: "String";
				for (int i = 0; i < columnList.length; i++) {
					if (i == 0)
						columnConcat = "CAST(" + columnList[i] + " AS "
								+ fieldtype + ")";
					else
						columnConcat += ",' ',CAST(" + columnList[i] + " AS "
								+ fieldtype + " )";
				}
				if(dataQuery != null && !"".equalsIgnoreCase(dataQuery) ){
					dataQuery = dataQuery.replaceAll("<SEARCH>"," AND ( LOWER(CONCAT(" + columnConcat
							+ ")) like '%" + searchValue.toLowerCase() + "%')");
				} else {
					where.append(" AND ( LOWER(CONCAT(" + columnConcat
							+ ")) like '%" + searchValue.toLowerCase() + "%')");
				}
			} else {
				if(dataQuery != null && !"".equalsIgnoreCase(dataQuery) ){
					dataQuery = dataQuery.replaceAll("<SEARCH>","");
				}
			}
			
			if (groupBy != null) {
				if(dataQuery != null && dataQuery.contains("<GROUPBY>")) {
					dataQuery = dataQuery.replaceAll("<GROUPBY>"," GROUP BY " + groupBy + " ");
				} else {
					where.append(" GROUP BY " + groupBy + " ");
				}
			}
			
			if (orderType == null || "null".equalsIgnoreCase(orderType))
				orderType = " DESC";
			if (orderby != null) {
				orderby = orderby + 1;
				if(dataQuery != null && dataQuery.contains("<ORDERBY>")) {
					dataQuery = dataQuery.replaceAll("<ORDERBY>", " ORDER BY " + Integer.toString(orderby) + " "
							+ orderType);
				} else {
					where.append(" ORDER BY " + Integer.toString(orderby) + " "
							+ orderType);
				}
				
			}
			if(dataQuery != null && !"".equalsIgnoreCase(dataQuery)){
				queryToExecute = new StringBuilder(dataQuery)
						.append(where.toString()).toString();
			}else{
				queryToExecute = new StringBuilder(initialQuery).append(tableName)
						.append(where.toString()).toString();
			}
			report = new Report();
			report.query = queryToExecute;
			report.name = reportName;
			report.dbname = paramCon;
			report.source = source;
			reportdata = userDao.getReportsAndGenerateCsv(report,datatableExport);
			_logger.info("Fetched Report object from DAO " + report);
		} catch (Exception ex) {
			Log.Error("Exception occured :" + ex.getMessage());
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, null, stmt);
			Log.Info("All Resources are closed :");
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportdata;
	}

	public String linkToAction(String refId, String action, int actionId)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder query = new StringBuilder();
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbUtil.getPostGresConnection();
			stmt = con.createStatement();
			query.append("UPDATE profile_analyser SET profilekey = '" + action
					+ "', actionid = '" + actionId + "' WHERE id = '" + refId
					+ "'");
			stmt.executeUpdate(query.toString());
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, null, stmt);

		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully Linked profile.\",\"actionId\":"
				+ actionId + "}";
	}

	public Action getAction(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Action actionToStore = null;
		ResultSet rs = null;
		Action param = null;
		Connection con = null;
		Statement stmt = null;
		StringBuilder query = new StringBuilder();
		String queryToBuild;
		if (obj != null) {
			param = (Action) obj[0];
			_logger.info("Inserting  Values for Role Object : " + param);
			try {
				queryToBuild = "select a.action_id, a.action, a.display_name, a.parent,b.display_name as parent_name, a.sequence, a.icon, a.enabled,a.sources from actions a inner join actions b on b.action_id = a.parent where a.action = '"
						+ param.getAction() + "'";
				
				if(param.getId() > 0) {
					queryToBuild = "select a.action_id, a.action, a.display_name, a.parent,b.display_name as parent_name, a.sequence, a.icon, a.enabled,a.sources from actions a inner join actions b on b.action_id = a.parent where a.action = '"
							+ param.getAction() + "' and a.action_id = " + param.getId();
				}
				
				query.append(queryToBuild);
				con = dbUtil.getPostGresConnection();
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());

				while (rs.next()) {
					actionToStore = new Action();
					actionToStore.setId(rs.getInt("action_id"));
					actionToStore.setAction(rs.getString("action"));
					actionToStore.setDisplayname(rs.getString("display_name"));
					actionToStore.setParent(rs.getInt("parent"));
					actionToStore.setSequence(rs.getInt("sequence"));
					actionToStore.setIcon(rs.getString("icon"));
					actionToStore.setEnabled(rs.getInt("enabled"));
					actionToStore.setParentName(rs.getString("parent_name"));
					actionToStore.setSource(rs.getString("sources"));
				}
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(con, rs, stmt);

			}
			PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
			return actionToStore;
		} else {
			_logger.error("Input object is null");
			return null;
		}
	}

	public DataAnalyserView getTableData(String source, String database,
			String table, Map<String, String> criteriaKeyValueMap)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataAnalyserView view = new DataAnalyserView();
		StringBuilder query = new StringBuilder();

		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		StringBuilder where = new StringBuilder(" WHERE ");
		try {
			con = getDatabaseConnection(source, database);
			if (source.equalsIgnoreCase(HIVE)) {
				query.append("SELECT * FROM " + table);
			} else {
				query.append("SELECT * FROM " + table);
			}

			if (criteriaKeyValueMap != null) {
				String key = null;
				String value = null;
				boolean isFirstEntry = true;
				for (Map.Entry<String, String> entry : criteriaKeyValueMap
						.entrySet()) {
					key = entry.getKey();
					value = entry.getValue();

					if (isFirstEntry) {
						isFirstEntry = false;
						where.append(key.trim()).append(" = '").append(value)
								.append("'");

					} else {
						where.append(" and ").append(key.trim()).append(" = '")
								.append(value).append("'");
					}
				}
			}
			query.append(where.toString());
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				JSONArray convertedJson = dbUtil.convert(rs);
				view.setJsonArray(convertedJson);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);

		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return view;
	}

	public DataAnalyserView getCalendarData(String source, String database,
			String table, String eventID, String eventName, String eventStart,
			String eventEnd, String eventColor, String start, String end,
			String filters) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataAnalyserView view = new DataAnalyserView();

		StringBuilder calendarQuery = new StringBuilder("SELECT ");
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		StringBuilder where = new StringBuilder(" WHERE ");
		try {
			con = getDatabaseConnection(source, database);
			calendarQuery.append(eventID).append(" as eventid, ");
			calendarQuery.append(eventName).append(" as title, ");
			calendarQuery.append(eventStart).append(" as start, ");
			calendarQuery.append(eventEnd).append(" as end");

			if (eventColor != null && !"".equalsIgnoreCase(eventColor.trim()))
				calendarQuery.append(", ").append(eventColor)
						.append(" as color");
			calendarQuery.append(" from ").append(table);
			if (start != null && !"".equalsIgnoreCase(start.trim())) {
				where.append(eventStart).append(">= '").append(start)
						.append("'");
			}
			if (end != null && !"".equalsIgnoreCase(end.trim())) {
				where.append(" and ").append(eventEnd).append(" <= '")
						.append(end).append("'");
			}
			if (filters != null && !"".equalsIgnoreCase(filters.trim())) {
				where.append(" and ").append(filters);
			}
			calendarQuery.append(where);
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(calendarQuery.toString());
				rs = stmt.executeQuery(calendarQuery.toString());
				JSONArray convertedJson = dbUtil.convert(rs);
				view.setJsonArray(convertedJson);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return view;
	}

	public List<LinkedHashMap<String, Object>> adHocReportDownload(
			Report report, AdHocReportQuery adHocReport) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<LinkedHashMap<String, Object>> reportdata = new ArrayList<>();
		try {
			UserDao userDao = new UserDao();
			String filter = adHocReport.getFilter();
			_logger.info("filter from adhoc report:"+filter);
			_logger.info("filter from adhoc report:"+filter);
			String searchValue = adHocReport.getSearchValue();
			_logger.info("searchValue from adhoc report:"+searchValue);
			if (filter == null || "".equalsIgnoreCase(filter.trim())
					|| "null".equalsIgnoreCase(filter)) {
				filter = " 1=1 ";
			}
			//changed for qfind exc el searchvalue issue
			if (searchValue == null || "".equalsIgnoreCase(searchValue.trim())
					|| "null".equalsIgnoreCase(searchValue)) {
				searchValue = "\'%\'";
			}
			_logger.info("post check searchValue from adhoc report:"+searchValue);
			_logger.info("post check filter from adhoc report:"+filter);
			report.setQuery(report.getQuery().replaceAll("<!FILTER!>", filter));
			report.setQuery(report.getQuery().replaceAll("<!SEARCH!>",searchValue));

			reportdata = userDao.getReports(report);
			_logger.info("Fetched Report object from DAO " + report);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportdata;
	}

	public List<ProfileView> saveView(ProfileView profileView) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		PreparedStatement ps = null;
		Connection con = null;
		int viewId = 0;
		int profileId;
		String name;
		ResultSet rs = null;
		Statement stmt = null;
		List<ProfileView> views = null;
		StringBuilder query = new StringBuilder();
		String createdBy;
		try {
			con = dbUtil.getPostGresConnection();
			stmt = con.createStatement();
			profileId = profileView.getProfileId();
			name = profileView.getName();
			createdBy = profileView.getCreatedBy();
			query.append("select id from profile_views where profile_id=")
					.append(profileId).append(" and name='").append(name)
					.append("' and created_by ='").append(createdBy)
					.append("'");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			if (rs.next()) {
				viewId = rs.getInt(1);
			}
			if (profileView.isDefault()) {
				query = new StringBuilder(
						"update profile_views set isdefault=FALSE where profile_id = ? and created_by=?");
				ps = con.prepareStatement(query.toString());
				ps.setInt(1, profileId);
				ps.setString(2, profileView.getCreatedBy());
				ps.addBatch();
				ps.executeBatch();
				ps.close();
			}

			if (viewId == 0) {
				query = new StringBuilder(
						"insert into profile_views (profile_id, name, filters, created_by, view_setting,isdefault,ispublic,filter_query,modified_on,view_config) values (?,?,?,?,?,?,?,?,?,?)");
			} else {
				query = new StringBuilder(
						"update profile_views set profile_id=?,name= ?, filters=?, modified_by=?, view_setting= ?,isdefault=?,ispublic=?,filter_query=?,modified_on=?,view_config=? where id = ?");

			}
			ps = con.prepareStatement(query.toString());
			ps.setInt(1, profileId);
			ps.setString(2, name);
			ps.setString(3, profileView.getViewFilters());
			ps.setString(4, profileView.getCreatedBy());
			ps.setString(5, profileView.getViewSetting());
			ps.setBoolean(6, profileView.isDefault());
			ps.setBoolean(7, profileView.isPublic());
			ps.setString(8, profileView.getFilterQuery());
			java.util.Date date = new java.util.Date();
			ps.setTimestamp(9, new Timestamp(date.getTime()));
			ps.setString(10, profileView.getViewConfig());
			if (viewId != 0) {
				ps.setInt(11, viewId);
			}
			ps.addBatch();
			ps.executeBatch();

			views = getViews(profileView);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			if (ps != null) {
				ps.close();
			}
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return views;
	}

	public List<ProfileView> getViews(ProfileView profileView) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		int profileID;
		String createdBy;
		List<ProfileView> views = null;
		ProfileView view;
		SimpleDateFormat df;
		try {
			views = new ArrayList<>();
			df = new SimpleDateFormat("d MMM yy hh:mm aaa");
			profileID = profileView.getProfileId();
			createdBy = profileView.getCreatedBy();
			con = dbUtil.getPostGresConnection();
			query.append(
					"SELECT id,profile_id, name, created_by, modified_on, isdefault, ispublic, view_config, CASE WHEN created_by='")
					.append(createdBy)
					.append("' THEN TRUE else FALSE end as viewtype  FROM profile_views WHERE");
			query.append(" (created_by = '").append(createdBy)
					.append("' OR isPublic = TRUE)");
			if (profileID > 0) {
				query.append(" AND profile_id = ").append(profileID);
			}
			query.append(" ORDER by created_on DESC ");
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					view = new ProfileView();
					view.setId(rs.getInt("id"));
					view.setProfileId(rs.getInt("profile_id"));
					view.setName(rs.getString("name"));
					view.setCreatedBy(rs.getString("created_by"));
					if(rs.getTimestamp("modified_on")!=null)
						view.setCreatedOn(df.format(rs.getTimestamp("modified_on")));
					view.setPublic(rs.getBoolean("ispublic"));
					view.setDefault(rs.getBoolean("isdefault"));
					view.setViewType(rs.getBoolean("viewtype"));
					if(rs.getString("view_config")!=null){
						view.setViewConfig(rs.getString("view_config"));						
					}else{
						view.setViewConfig(" ");
					}					
					views.add(view);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);

		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return views;
	}

	public boolean updateToDB(UpdateColumns updateColumns) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		StringBuilder query = new StringBuilder();
		StringBuilder primaryKeyCombined = new StringBuilder();
		Audit audit;
		String source;
		String database;
		boolean enableAudit = false;
		boolean isCreate = false;
		List<Audit> primaryKeys;
		String primaryKeyValue = null;
		try {
			primaryKeys = updateColumns.getPrimaryKey();
			if (primaryKeys == null || primaryKeys.isEmpty()) {
				isCreate = true;
			}
			source = updateColumns.getSource();
			database = updateColumns.getDatabase();
			con = getDatabaseConnection(source,
					updateColumns.getDatabase());
			stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
					java.sql.ResultSet.CONCUR_READ_ONLY);
			if(isCreate || "hive".equalsIgnoreCase(source)) {
				query = new StringBuilder("INSERT INTO ").append(
						updateColumns.getTable()).append(" ( ");
			} else  {
				query = new StringBuilder("UPDATE ").append(
						updateColumns.getTable()).append(" SET ");
			}
			
			int columnCounter = 0;
			if(primaryKeys != null && "hive".equalsIgnoreCase(source)) {
				for (Audit primaryKey : primaryKeys) {
					if (columnCounter != 0){
						query.append(" , ");
					}
					
						query.append(primaryKey.getFieldName());
						columnCounter++;
					}
				query.append(" ,");
			}
				
		
				columnCounter = 0;
			for (Audit column : updateColumns.getValues()) {
				if (columnCounter != 0)
					query.append(",");
				if(isCreate || "hive".equalsIgnoreCase(source)) {
					query.append(column.getFieldName());
				} else {
					query.append(column.getFieldName()).append(" = ? ");
				}			
				columnCounter++;
				if (Character.toString(column.getType()).equalsIgnoreCase("P"))
					primaryKeyValue = column.getNewValue();
			}
			
			if(isCreate || "hive".equalsIgnoreCase(source)) {
				query.append(" ) VALUES (");
			} else {
				query.append(" WHERE ");
			}
			
			if(!isCreate) {
				columnCounter = 0;
				if(primaryKeys != null) {
					for (Audit primaryKey : primaryKeys) {
						if (columnCounter != 0){
							if(!"hive".equalsIgnoreCase(source)){
								query.append(" and ");
							}else{
							query.append(" , ");
						 }
						}
						if(isCreate || "hive".equalsIgnoreCase(source)) {
							query.append("'"+primaryKey.getNewValue()+"'");
						} else {
							query.append(primaryKey.getFieldName()).append(" = ?");
						}
						
						if (primaryKey.isEnableAudit()) {
							primaryKeyCombined.append(primaryKey.getNewValue());
							enableAudit = true;
						}
						columnCounter++;
					}
				}
				if("hive".equalsIgnoreCase(source)){
				query.append(" ,");
				}
				if("hive".equalsIgnoreCase(source)){
					 for(Audit column : updateColumns.getValues()) {
							String columnType = column.getFieldType();
							String columnValue = column.getNewValue();
							if (columnType == null || ("text").equalsIgnoreCase(columnType)
										|| ("txtarea").equalsIgnoreCase(columnType)) {
									query.append("'"+columnValue+"'");
								} else if (("boolean").equalsIgnoreCase(columnType)) {
									query.append(Boolean.parseBoolean(columnValue));
								} else if (("number").equalsIgnoreCase(columnType)) {
									query.append( Integer.parseInt(columnValue));
//									query.append( Float.parseFloat(columnValue));
								} else if (("date").equalsIgnoreCase(columnType)) {
									query.append(java.sql.Date.valueOf(columnValue));
								}else if (("timestamp").equalsIgnoreCase(columnType)) {
									query.append(java.sql.Timestamp.valueOf(columnType));
								}
							if(columnCounter < (updateColumns.getValues().size())) {
								query.append(",");
							}
							columnCounter++;
						}
						query.append(")");
				}
			} else {
				enableAudit = updateColumns.isEnableAudit();
				columnCounter = 0;
				for(Audit column : updateColumns.getValues()) {
					String columnType = column.getFieldType();
					String columnValue = column.getNewValue();
					if (!"hive".equalsIgnoreCase(source))
						query.append("?");
					else {
						if (columnType == null || ("text").equalsIgnoreCase(columnType)
								|| ("txtarea").equalsIgnoreCase(columnType)) {
							query.append("'"+columnValue+"'");
						} else if (("boolean").equalsIgnoreCase(columnType)) {
							query.append(Boolean.parseBoolean(columnValue));
						} else if (("number").equalsIgnoreCase(columnType)) {
							query.append( Float.parseFloat(columnValue));
						} else if (("date").equalsIgnoreCase(columnType)) {
							query.append(java.sql.Date.valueOf(columnValue));
						}else if (("timestamp").equalsIgnoreCase(columnType)) {
							query.append(java.sql.Timestamp.valueOf(columnType));
						}
					}
					if(columnCounter < (updateColumns.getValues().size() - 1)) {
						query.append(",");
					}
					columnCounter++;
				}
				query.append(")");
			}
			if ("hive".equalsIgnoreCase(source)){
				stmt.executeUpdate(query.toString());
			} else {
				ps = con.prepareStatement(query.toString());
			}

			// Setting Column to prepare statement
			columnCounter = 0;
			for (Audit column : updateColumns.getValues()) {
				if (enableAudit && !Character.toString(column.getType()).equalsIgnoreCase("P")) {
					audit = new Audit();
					audit = column;
					audit.setPrimaryKey(primaryKeyCombined.toString());
					audit.setUserName(updateColumns.getUserName());
					audit.setUserId(updateColumns.getUserId());
					audit.setTableName(updateColumns.getTable());
					audit.setSource(source);
					audit.setDatabase(database);
					if ("hive".equalsIgnoreCase(source))
						audit.setPrimaryKey(primaryKeyValue);
					updateAuditLog(audit);
				}
				if (ps != null){ //"hive".equalsIgnoreCase(source) && 
					String columnType = column.getFieldType();
					String columnValue = column.getNewValue();
					columnCounter++;
					if (columnType == null || ("text").equalsIgnoreCase(columnType)
							|| ("txtarea").equalsIgnoreCase(columnType)) {
						ps.setString(columnCounter, columnValue);
					} else if (("boolean").equalsIgnoreCase(columnType)) {
						ps.setBoolean(columnCounter,
								Boolean.parseBoolean(columnValue));
					} else if (("number").equalsIgnoreCase(columnType)) {
						ps.setFloat(columnCounter, Float.parseFloat(columnValue));
					} else if (("date").equalsIgnoreCase(columnType)) {
						ps.setDate(columnCounter,java.sql.Date.valueOf(columnValue));
					}else if (("timestamp").equalsIgnoreCase(columnType)) {
						ps.setTimestamp(columnCounter, java.sql.Timestamp.valueOf(columnValue));
					}
				}
			}
			if(!isCreate && !"hive".equalsIgnoreCase(source)) {
				for (Audit primaryKey : primaryKeys) {
					String columnType = primaryKey.getFieldType();
					String columnValue = primaryKey.getNewValue();
					columnCounter++;
					if (columnType == null || ("text").equalsIgnoreCase(columnType)
							|| ("select").equalsIgnoreCase(columnType)
							|| ("txtarea").equalsIgnoreCase(columnType)) {
						ps.setString(columnCounter, columnValue);
					} else if (("boolean").equalsIgnoreCase(columnType)) {
						ps.setBoolean(columnCounter,
								Boolean.parseBoolean(columnValue));
					} else if (("number").equalsIgnoreCase(columnType)) {
						ps.setFloat(columnCounter, Float.parseFloat(columnValue));
					} else if (("date").equalsIgnoreCase(columnType)) {
						ps.setDate(columnCounter,java.sql.Date.valueOf(columnValue));
					}else if (("timestamp").equalsIgnoreCase(columnType)) {
						ps.setTimestamp(columnCounter, java.sql.Timestamp.valueOf(columnValue));
					}
				}
			}		
			if (!"hive".equalsIgnoreCase(source) && ps != null)
				ps.executeUpdate();
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
			return false;
		} finally {
			if (ps != null) {
				ps.close();
			}
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return true;
	}
	
	public Map<String, List<String>> getUpdatedRows(String sourceName,
			String dbName, String tableName, String primaryKeyColumn, String primaryKeys,
			String filter, String orderBy, String columns, String groupBy) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		StringBuilder query = new StringBuilder();
		HashMap<String, List<String>> result = null;
		List<String> rowList;
		String pkeyValue;
		try {
			query.append("select " + primaryKeyColumn + ", ");
			query.append(columns);
			query.append(" from " + tableName);
			if(primaryKeyColumn != null && !primaryKeyColumn.isEmpty()) {
				query.append(" where ");
				query.append(primaryKeyColumn + " in (" + primaryKeys + ") ");
			}
			if(filter != null && !filter.isEmpty()) {
				query.append(" AND " + filter);
			}
			if(groupBy != null && !groupBy.isEmpty()) {
				query.append(" group by " + groupBy);
				query.append(", " + primaryKeyColumn);
			} else {
				query.append(" group by " + primaryKeyColumn);
			}
			if(orderBy != null && !orderBy.isEmpty()) {
				query.append(" order by " + orderBy);
			} else {
				query.append(" order by 1 ");
			}
			query.append(";");
			con = getDatabaseConnection(sourceName, dbName);
			stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
					java.sql.ResultSet.CONCUR_READ_ONLY);
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			if(rs.next()) {
				result = new HashMap<>();
				pkeyValue = "";
				do {
					pkeyValue = rs.getString(primaryKeyColumn);
					if(result.containsKey(pkeyValue)) {
						rowList = result.get(pkeyValue);
					} else {
						rowList = new ArrayList<>();
					}
					
					rowList.add(rs.getString(columns));
					result.put(pkeyValue, rowList);
				} while(rs.next());
			}			
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);

		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return result;
	}
	
	private String updateAuditLog(Audit audit) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		StringBuilder query = new StringBuilder();
		String source;
		try {
			con = getDatabaseConnection(audit.getSource(), audit.getDatabase());
			stmt = con.createStatement();
			source = audit.getSource();
			if ("hive".equalsIgnoreCase(source)){
				query = new StringBuilder(
						"INSERT INTO audit(type, tablename, primarykey, fieldname,displayname, oldvalue, newvalue,username,userid,id,updatedate) VALUES ('")
				.append(String.valueOf(audit.getType()))
				.append("','").append(audit.getTableName())
				.append("','").append(audit.getPrimaryKey())
				.append("','").append(audit.getFieldName())
				.append("','").append(audit.getDisplayName())
				.append("','").append(audit.getOldValue())
				.append("','").append(audit.getNewValue())
				.append("','").append(audit.getUserName())
				.append("','").append(audit.getUserId());
					int maxId = 0;
					rs = stmt.executeQuery("SELECT Max(id) from audit");
					if (rs.next()) {
						maxId = rs.getInt(1);
					}
					rs.close();
					query.append("',").append(maxId+1);
					DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					Date date = new Date();
					query.append(",'").append(dateFormat.format(date)).append("')");
					stmt.executeUpdate(query.toString());
			} else {
				query = new StringBuilder(
						"INSERT INTO audit(type, tablename, primarykey, fieldname,displayname, oldvalue, newvalue,username,userid) VALUES (?, ?, ?, ?, ?, ? , ? , ? , ?)");
				ps = con.prepareStatement(query.toString());
				ps.setString(1, String.valueOf(audit.getType()));
				ps.setString(2, audit.getTableName());
				ps.setString(3, audit.getPrimaryKey());
				ps.setString(4, audit.getFieldName());
				ps.setString(5, audit.getDisplayName());
				ps.setString(6, audit.getOldValue());
				ps.setString(7, audit.getNewValue());
				ps.setString(8, audit.getUserName());
				ps.setString(9, audit.getUserId());
				ps.executeUpdate();
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			if (ps != null) {
				ps.close();
			}
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return null;
	};

	public boolean insertToDB(UpdateColumns updateColumns) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		StringBuilder query = new StringBuilder();
		try {
			if (!updateColumns.getPrimaryKey().isEmpty()) {
				con = getDatabaseConnection(updateColumns.getSource(),
						updateColumns.getDatabase());
				stmt = con.createStatement();
				query = new StringBuilder("INSERT INTO ").append(
						updateColumns.getTable()).append(" ( ");
				int columnCounter = 0;
				StringBuilder valuesStr = new StringBuilder("");
				for (Audit column : updateColumns.getValues()) {
					if (columnCounter != 0) {
						query.append(",");
						valuesStr.append(",");
					}
					query.append(column.getFieldName());
					valuesStr.append("?");
					columnCounter = 1;
				}
				query.append(" ) VALUES( ");
				query.append(valuesStr.toString() + ")");
				columnCounter = 0;

				ps = con.prepareStatement(query.toString());

				// Setting Column to prepare statement
				columnCounter = 0;
				for (Audit column : updateColumns.getValues()) {
					String columnType = column.getFieldType();
					String columnValue = column.getNewValue();
					columnCounter++;
					if (columnType == null
							|| ("text").equalsIgnoreCase(columnType)) {
						ps.setString(columnCounter, columnValue);
					} else if (("boolean").equalsIgnoreCase(columnType)) {
						ps.setBoolean(columnCounter,
								Boolean.parseBoolean(columnValue));
					} else if (("number").equalsIgnoreCase(columnType)) {
						ps.setFloat(columnCounter,
								Float.parseFloat(columnValue));
					} else if (("date").equalsIgnoreCase(columnType)) {
						ps.setDate(columnCounter,
								java.sql.Date.valueOf(columnValue));
					}
				}
				ps.addBatch();
				ps.executeBatch();
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
			return false;
		} finally {
			if (ps != null) {
				ps.close();
			}
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return true;
	}

	public boolean deleteFromDB(UpdateColumns updateColumns) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		StringBuilder query = new StringBuilder();
		try {
			if (!updateColumns.getPrimaryKey().isEmpty()) {
				con = getDatabaseConnection(updateColumns.getSource(),
						updateColumns.getDatabase());
				stmt = con.createStatement();
				query = new StringBuilder("DELETE FROM ").append(
						updateColumns.getTable()).append(" WHERE ");
				int columnCounter = 0;
				for (Audit column : updateColumns.getValues()) {
					if (columnCounter != 0)
						query.append(",");
					query.append(column.getFieldName()).append(" = ? ");
				}
				ps = con.prepareStatement(query.toString());
				// Setting Column to prepare statement
				columnCounter = 0;
				for (Audit primaryKey : updateColumns.getPrimaryKey()) {
					String columnType = primaryKey.getFieldType();
					String columnValue = primaryKey.getNewValue();
					columnCounter++;
					if (columnType == null
							|| ("text").equalsIgnoreCase(columnType)) {
						ps.setString(columnCounter, columnValue);
					} else if (("boolean").equalsIgnoreCase(columnType)) {
						ps.setBoolean(columnCounter,
								Boolean.parseBoolean(columnValue));
					} else if (("number").equalsIgnoreCase(columnType)) {
						ps.setFloat(columnCounter,
								Float.parseFloat(columnValue));
					} else if (("date").equalsIgnoreCase(columnType)) {
						ps.setDate(columnCounter,
								java.sql.Date.valueOf(columnValue));
					}
				}
				ps.addBatch();
				ps.executeBatch();
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
			return false;
		} finally {
			if (ps != null) {
				ps.close();
			}
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return true;
	}
	public String queryExecutor(String profileIds,String users, String profileName, String criticality,boolean sendMail) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
        String response = "";
		String data =null;
		String status = null;
		String absoluteFilePath = "";
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		FileOutputStream out = null;
		 String sourceName = null;
		 String databaseName = null;
		 String expectedOutput = null;
		 String queryName = null;
		 String description = null;
		 StringBuilder responseData =null;
		 String outputType ="";
		 String[] priorityData = criticality.split(",");
		 String priority=null;
		 String suiteStatus="";
		//Create blank workbook
	      XSSFWorkbook workbook = new XSSFWorkbook(); 
	      //Create a blank sheet
	      XSSFSheet spreadsheet = workbook.createSheet( 
	      " Query Status ");
	      //Create row object
	      XSSFRow row;
	      //This data needs to be written (Object[])
	      Map < String, Object[] > resultInfo = 
	      new TreeMap < String, Object[] >();
	      resultInfo.put( "0", new Object[] { 
	      "Name", "Query","Description","Expected Output","Actual Output","Status","Criticality" });
	    JSONArray arrJSON = new JSONArray(profileIds);
		String config = null;
		try{
			for (int j=0; j<arrJSON.length(); j++) {
				String actualOutput = null;
			 responseData = new StringBuilder();
			 priority = priorityData[j];
			StringBuilder query = new StringBuilder();
		    JSONObject item = arrJSON.getJSONObject(j).getJSONObject("map");
		     sourceName = item.getString("source");
		     databaseName = item.getString("database");
		    config = item.getString("config").replace("\n", "").replace("\r", "");
		    JSONObject configObj = new JSONObject(config);
		    String userQuery = configObj.getString("query");
		    userQuery = userQuery.replaceAll("@", "'");
		    queryName = item.getString("displayname");
		    expectedOutput = configObj.getString("expectedOutput");
		    if(configObj.has("outputType")){
		    outputType = configObj.getString("outputType");
		    }
		    description = item.getString("remarks");
		String paramCon = getDatabaseName(databaseName);
			if ((HIVE).equalsIgnoreCase(sourceName)) {
				con = dbUtil.getHiveConnection(paramCon);
			} else {
				con = dbUtil.getPostGresConnection(paramCon);
			}
			query.append(userQuery);
			if (con != null) {
				stmt = con.createStatement();
				try{
					System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				ResultSetMetaData rsmd=rs.getMetaData();  
				ArrayList<String> columns = new ArrayList<String>();
				for(int i=1;i<=rsmd.getColumnCount();i++){
					columns.add(rsmd.getColumnName(i));  
			}
				while (rs.next()) {
					for (String columnName : columns) {
					    data = rs.getString(columnName);
					    if(data ==""){
					    	data = "null";
					    }
					}
					responseData=responseData.append(" ").append(data);
					}
				PrismHandler.handleFinally(con, rs, stmt);
				String[] range =expectedOutput.split(" to ");
				actualOutput = responseData.toString().trim();
				if(outputType !="" && outputType !=null && outputType.equalsIgnoreCase("range")){
				  if(Double.parseDouble(range[0]) <= Double.parseDouble(actualOutput) && Double.parseDouble(actualOutput) <= Double.parseDouble(range[1])){
					  status = "success"; 
				  }else{
					  status = "error";  
					  suiteStatus  ="Failed!";
				  }
				}else if(actualOutput.equals(expectedOutput)){
					status ="success";
				}else{
					status = "error";
					suiteStatus = "Failed!";
				}
			}catch(Exception ex) {
				status = ex.getMessage();
				actualOutput = "";
			}
				String serialNo = Integer.toString(j+1);
				resultInfo.put( serialNo, new Object[] { 
			    		  queryName, userQuery,description, expectedOutput ,actualOutput,status,priority});
			}
			}
				int rowid = 0;
				
			      //Iterate over data and write to sheet
			      Set < String > keyid = resultInfo.keySet();
			      for (String key : keyid)
			      {
			         row = spreadsheet.createRow(rowid++);
			         Object [] objectArr = resultInfo.get(key);
			         int cellid = 0;
			         for (Object obj : objectArr)
			         {
			            Cell cell = row.createCell(cellid++);
			            try{
			            	cell.setCellValue((String) obj);
			            }catch(Exception ex){
			            	cell.setCellValue("limit exceeded");
			            }
			         }
			      }
			      //Write the workbook in file system
			      String tempDir = System.getProperty("java.io.tmpdir");
					//File filePath = new File(tempDir + File.separator +"Resultsheet.xlsx");
//			     String absoluteFilePath="E:\\Resultsheet.xlsx";
			      if(suiteStatus.isEmpty()){
			    	  suiteStatus = "successfully Executed!";
			      }
			      absoluteFilePath = tempDir + File.separator + profileName+"_Resultsheet.xlsx";
//				   file = new File("E:\\R.xlsx");
			       out = new FileOutputStream(absoluteFilePath);
			      workbook.write(out);
			      workbook.close();
			      out.flush();
			      out.close();
			      if(sendMail){
			    	  response =  queryMail(users, profileName,suiteStatus);
			      }else{
			    	  response = absoluteFilePath;
			      }
			  		
			      
		    // boolean b = file.delete();
			}catch(Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		return response ;
	}
	
	public String queryMail(String username ,String profileName,String status) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response =null;
		try{
			List<File> attachedFiles = new ArrayList<File>();
			String tempDir = System.getProperty("java.io.tmpdir");
			File file = new File(tempDir + File.separator + profileName+"_Resultsheet.xlsx");
			attachedFiles.add(file);
		MailService mailer = new MailService();
		//response = mailer.sendGenericEmailWithAttachment("Test Suite: "+profileName+" "+status, "PFA result ", username, attachedFiles);
		response = mailer.sendGenericMailX("Test Suite: "+profileName+" "+status,"PFA the results", username,attachedFiles);
		}
		 catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} 

			PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());

	return response;
	}
	public String excutequery(String source,String database,String profileId) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String status = null;
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		con = dbUtil.getPostGresConnection();
		query.append("select config from profile_analyser ");
		if(profileId != null){
			query.append(" WHERE id in (" + profileId + ")");
		}
		try{
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				JSONArray convertedJson = dbUtil.convert(rs);
   				if(!convertedJson.getJSONObject(0).isNull("config") && !("".equals(convertedJson.getJSONObject(0).getString("config")))){
					String config = convertedJson.getJSONObject(0).getString("config");					
					org.json.simple.JSONObject json = null;				
				     //Check if the object is of JSON or not.
				    try{
				      json = (org.json.simple.JSONObject)new JSONParser().parse(config);
				    } catch(Exception ex) {
				      json = null;
				    }
					if(json != null && json.size() >0){
						List<String> statusList= new ArrayList<>();
						con = getDatabaseConnection(source, database);	
						Statement stmtJSON = null;
						 for (Object key : json.keySet()) {
							 try{
								
								stmtJSON = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
								 
								 //String keyStr = (String)key;
							     String lavelQuery = (String)json.get(key);
							     lavelQuery = lavelQuery.replaceAll("[\\t\\n\\r]"," ");
							     if(lavelQuery.toLowerCase().indexOf("insert ") >-1 || lavelQuery.toLowerCase().indexOf("delete ") >-1 || 
							    		 lavelQuery.toLowerCase().indexOf("update ") >-1 || lavelQuery.toLowerCase().indexOf("drop ") >-1){
								     String queries[] = lavelQuery.split(";");
								     for(String querie:queries){  
								    	 stmtJSON.executeUpdate(querie.replaceAll("@", "'"));
								     }
							     }else{
							    	 String queries[] = lavelQuery.split(";");
								     for(String querie:queries){  
								    	 stmtJSON.executeQuery(querie.replaceAll("@", "'"));
								     }
							     }
								 statusList.add(key+" : Excuted Succesfully");								 
							 }
							 catch(Exception ex) {
								 statusList.add(key+" : "+ex.getMessage());
							 }finally{
								 if(stmtJSON != null){
									 stmtJSON.close();
								 }
							 }
						}
						 Gson gson = new Gson();
						 status = gson.toJson(statusList);
					}
				}
			}	
		}
		catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		return status;
	}
	
	
}
