package com.relevance.prism.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.StreamingOutput;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.relevance.prism.dao.AdminDao;
import com.relevance.prism.dao.DataAnalyserDao;
import com.relevance.prism.data.Action;
import com.relevance.prism.data.AdHocReportQuery;
import com.relevance.prism.data.ChartConfiguration;
import com.relevance.prism.data.Column;
import com.relevance.prism.data.ColumnInformation;
import com.relevance.prism.data.DataAnalyserView;
import com.relevance.prism.data.DataTable;
import com.relevance.prism.data.DataTableView;
import com.relevance.prism.data.E2emfView;
import com.relevance.prism.data.EmailNotification;
import com.relevance.prism.data.ProfileConfig;
import com.relevance.prism.data.ProfileView;
import com.relevance.prism.data.Report;
import com.relevance.prism.data.TableDetails;
import com.relevance.prism.data.UpdateColumns;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class DataAnalyserService extends BaseService{
	public List<String> getDatabaseList(String source) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<String> databaseList = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			databaseList = dataAnalyserDao.getDatabaselist(source);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return databaseList;
	}
	public ChartConfiguration getChartConfigurationList() throws Exception{
		  PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		  ChartConfiguration chartMap = null;
		  try {
		   DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
		   chartMap = dataAnalyserDao.getChartConfigurationList();
		  }catch(Exception e){
		   PrismHandler.handleException(e, true);
		  }
		  PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		  return chartMap;
	}
	public ColumnInformation getcolumnInfo(String source, String database,String table, String column,String filters,boolean isDetails) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ColumnInformation columnInfo = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			columnInfo = dataAnalyserDao.getcolumnInfo(source,database,table, column,filters,isDetails);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return columnInfo;
	}
	
	public List<String> getTableList(String source, String database)  throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<String> tableList = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			tableList = dataAnalyserDao.getTablelist(source,database);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return tableList;
	}

	public List<Column> getColumnList(String source, String database,
			String table,String query)  throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Column> columnList = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			columnList = dataAnalyserDao.getColumnlist(source,database,table,query);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return columnList;
	}

	public E2emfView getData(/*String source, String database,
			String table, String columns,String filters,FieldList fieldAndDescriptionList*/ ProfileConfig config)  throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		E2emfView dataView = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			dataView = dataAnalyserDao.getData(config);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return dataView;
	}

	public E2emfView getDataPagination(/*String source, String database,
			String table, String columns, String columnConfigurations, String filter, Integer orderby,String orderType, String pagenumber, String pagesize,String searchValue,String groupByEnabled, String summaryEnabled*/ ProfileConfig config) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		E2emfView dataView = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			dataView = dataAnalyserDao.getDataPagination(config);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return dataView;
	}
	
	public Map<String, String> getMultipleDataPagination(List<ProfileConfig> configs) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Map<String, String> dataViews = new HashMap<>();
		E2emfView dataView = null;
		try{
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			for(ProfileConfig config : configs) {
				dataView = dataAnalyserDao.getDataPagination(config);
				dataViews.put(config.getTable(), dataView.toString());
			}			
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return dataViews;
	}
	
	public Map<String, List<Column>> getMultiColumnList(String source, String database,
			String tables,String query)  throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Column> columnList = null;
		Map<String, List<Column>> mappedColumns = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			if(tables == null) {
				tables = "";
			}
			String[] tableList = tables.split(",");
			mappedColumns = new HashMap<>();
			for(String table : tableList) {
				columnList = dataAnalyserDao.getColumnlist(source,database,table,query);
				mappedColumns.put(table, columnList);
			}
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return mappedColumns;
	}
	
	/*getDataTableClientSide*/
	public E2emfView getDataTableClientSide(String source, String database,
			String table, String columns, String columnConfigurations, String filter, Integer orderby,String orderType, String groupByEnabled, String summaryEnabled) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		E2emfView dataView = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			dataView = dataAnalyserDao.getDataTableClientSide(source,database,table,columns,columnConfigurations,filter,orderby,orderType,groupByEnabled,summaryEnabled);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return dataView;
	}
	
	//getDataFromExcel
	public String getDataFromExcel(String uploadedFileLocation) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			response = dataAnalyserDao.getDataFromExcel(uploadedFileLocation);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	public String saveProfile(ProfileConfig profileConfig) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		int dataView=0;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			dataView = dataAnalyserDao.saveProfile(profileConfig);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return Integer.toString(dataView);
	}

	public E2emfView getMyProfiles(String createdby,String type) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		E2emfView dataView = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			dataView = dataAnalyserDao.getMyProfiles(createdby,type);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return dataView;
	}

	public E2emfView getProfileHistory(String refid) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		E2emfView dataView = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			dataView = dataAnalyserDao.getProfileHistory(refid);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return dataView;
	}
	public E2emfView getProfileConfig(ProfileConfig profileConfig) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		E2emfView dataView = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			dataView = dataAnalyserDao.getProfileConfig(profileConfig);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return dataView;
	}

	public E2emfView getProfileHistoryConfig(String id) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		E2emfView dataView = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			dataView = dataAnalyserDao.getProfileHistoryConfig(id);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return dataView;
	}
	
	public StreamingOutput downloadDatatableToExcel (
			DataTable datatableExport) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StreamingOutput reportsData = null;
		try {
			Log.Info("getPOReportsByDynamicQuery() method called on ReportsService");
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			reportsData = dataAnalyserDao.downloadDatatableToExcel(datatableExport);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportsData;
	}
	public File downloadDatatableToCSV (
			DataTable datatableExport) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		File reportsData = null;
		try {
			Log.Info("getPOReportsByDynamicQuery() method called on ReportsService");
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			reportsData = dataAnalyserDao.downloadDatatableToCSV(datatableExport);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportsData;
	}
	public String publishProfile(String refId, String action, String source,
			Action actionInstance, int actionId) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String dataView = null;
		try {
			AdminDao adminDao = new AdminDao();
			if(actionId >0){
				adminDao.updateAction(actionInstance).getId();
			}else{
				actionId = Integer.parseInt(adminDao.insertAction(actionInstance));
			}
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			
			if(source != null && source.toString().length()>30){//If Source Contains Multiple Tab Config Info
					dataView = dataAnalyserDao.linkToAction(refId,action,actionId);
			}else{
				dataView = dataAnalyserDao.linkToAction(refId,action,actionId);
			}
			
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return dataView;
	}
	
	public Action getAction(String actionName, String actionId) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Action action = null;
		try{
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			action = new Action();
			if(actionName == null || "".equalsIgnoreCase(actionName)) {
				throw new Exception("No Action Name found");
			}
			action.setAction(actionName);
			if(actionId != null && !"".equalsIgnoreCase(actionId)) {
				action.setId(Integer.parseInt(actionId));
			}
			action = dataAnalyserDao.getAction(action);
			return action;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return action;
	}

	public DataAnalyserView getTableData(String source, String database,
			String table, String criteria) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataAnalyserView dataView = null;
		String[] criteriaTokens;
		String[] keyValueTokens = null;
		Map<String, String> criteriaKeyValueMap = new HashMap<>();
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			if(criteria != null ){
				criteriaTokens = criteria.split(",");

				//name1:BABYCENTER ADVERTISING, LLC
				String tempKey = null;
				String tempValue = null;
				for(String param : criteriaTokens){

					if(param.contains(":"))
					{  						
						keyValueTokens = param.split(":");
						tempKey = keyValueTokens[0];
						tempValue = keyValueTokens[1];
						criteriaKeyValueMap.put(keyValueTokens[0], keyValueTokens[1]);

					}else{
						tempValue = tempValue + "," +param;
						criteriaKeyValueMap.remove(tempKey);
						criteriaKeyValueMap.put(tempKey, tempValue);
					}


				}
				dataView = dataAnalyserDao.getTableData(source,database,table,criteriaKeyValueMap);
			}
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return dataView;
	}

	public DataAnalyserView getCalendarData(String source, String database,
			String table, String eventID, String eventName, String eventStart,
			String eventEnd, String eventColor, String start, String end,String filters) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataAnalyserView dataView = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			dataView = dataAnalyserDao.getCalendarData(source,
					database, table, eventID,eventName,eventStart,eventEnd,eventColor,start,end,filters);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return dataView;
	}
	
	public List<LinkedHashMap<String, Object>> adHocReportDownload(Report report,AdHocReportQuery adHocReport ) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List <LinkedHashMap <String,Object>> reportsData = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			reportsData = dataAnalyserDao.adHocReportDownload(report,adHocReport);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportsData;
	}

	public List<ProfileView> saveView(ProfileView profileView) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<ProfileView> views = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			views = dataAnalyserDao.saveView(profileView);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return views;
	}

	public List<ProfileView> getViews(ProfileView profileView) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<ProfileView> views = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			views = dataAnalyserDao.getViews(profileView);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return views;
	}
	public List<TableDetails> getProfileTableDetails(EmailNotification emailNotification)throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTableView dataTableView = null;
		JSONArray config =  new JSONArray();
		JSONArray charts;
		StringBuilder query;
		StringBuilder profileFilter;
		List<TableDetails> tableDetailsList = null;
		ProfileConfig profileConfig;
		String notificationFilter = null;
		try {
			profileConfig = new ProfileConfig();
			profileConfig.setId(emailNotification.getProfileId());
			profileConfig.setViewID(emailNotification.getViewId());
			profileConfig.setCreatedBy(emailNotification.getCreatedBy());
			tableDetailsList = new ArrayList<>();
			profileFilter = new StringBuilder();
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			
			dataTableView = dataAnalyserDao.getProfileConfig(profileConfig);
			JSONArray configData = dataTableView.getData();
			if (configData.length() > 0) {
				JSONObject obj  = configData.getJSONObject(0);
				config = new JSONArray(obj.getString("config"));
				String filterJson = obj.getString("filters");
				String source=obj.getString("source");
				String filterQuery=getFilterQuery(filterJson, source);
				if (obj.has("notification_filter") && !obj.isNull("notification_filter"))
					notificationFilter = obj.getString("notification_filter");
				if (filterQuery != null && !"null".equalsIgnoreCase(filterQuery) && !"".equalsIgnoreCase(filterQuery))
					profileFilter.append(filterQuery);
				
				if (!("once").equalsIgnoreCase(emailNotification.getFrequency()) && notificationFilter != null && !"null".equalsIgnoreCase(notificationFilter) && !"".equalsIgnoreCase(notificationFilter)){
					if (!("").equalsIgnoreCase(profileFilter.toString()))
						profileFilter.append(" AND ");
					
					String fromDate = null;
					String toDate;
					Date today = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
					toDate =  sdf.format(today);
					Calendar c = Calendar.getInstance(); 
					c.setTime(today);
					if (("daily").equalsIgnoreCase(emailNotification.getFrequency())){ 
						c.add(Calendar.DATE, -1);
						today = c.getTime();
						fromDate = sdf.format(today);
					} else if (("weekly").equalsIgnoreCase(emailNotification.getFrequency())){
						c.add(Calendar.DATE, -7);
						today = c.getTime();
						fromDate = sdf.format(today);
					} else if (("monthly").equalsIgnoreCase(emailNotification.getFrequency())){
						c.add(Calendar.MONTH, -1);
						today = c.getTime();
						fromDate = sdf.format(today);
					} else if (("quaterly").equalsIgnoreCase(emailNotification.getFrequency())){
						c.add(Calendar.MONTH, -3);
						today = c.getTime();
						fromDate = sdf.format(today);
					}
					if (fromDate != null){
						profileFilter.append("unix_timestamp(").append(notificationFilter).append(" , 'MM/dd/yyyy')");
						profileFilter.append(" >= unix_timestamp('").append(fromDate).append("' , 'MM/dd/yyyy') ");
					}
					profileFilter.append(" AND unix_timestamp(").append(notificationFilter).append(" , 'MM/dd/yyyy')");
					profileFilter.append(" <= unix_timestamp('").append(toDate).append("' , 'MM/dd/yyyy') ");
					
				}
			}
			
			for (int j =0; j < config.length();j++){
				JSONObject widgetConfig  = config.getJSONObject(j);
				charts  = widgetConfig.getJSONArray("charts");
				for (int x =0; x < charts.length();x++){
					JSONObject chart  = charts.getJSONObject(x);
					String chartType = chart.getString("charttype");
					
					if ("datatable".equalsIgnoreCase(chartType)){
						query = new StringBuilder();
						StringBuilder tableFilter = new StringBuilder();
						TableDetails tableDetails = new TableDetails();
						JSONArray columns;
						String tableHeader = widgetConfig.getString("header");
						JSONObject chartData  = chart.getJSONObject("data");
						profileConfig.setSource(chartData.getString("source"));
						profileConfig.setDatabase(chartData.getString("\"database\""));
						profileConfig.setTable(chartData.getString("table"));
						String tableDefaultFilter = chartData.getString("filter");
						tableFilter.append(profileFilter);
						if (tableDefaultFilter != null && !"null".equalsIgnoreCase(tableDefaultFilter) && !"".equalsIgnoreCase(tableDefaultFilter)){
							if (!("").equalsIgnoreCase(tableFilter.toString()))
								tableFilter.append(" AND ");
							tableDefaultFilter = tableDefaultFilter.replaceAll("@", "'");
							tableFilter.append(tableDefaultFilter);
						}
						profileConfig.setFilter(tableFilter.toString());
						columns = chart.getJSONArray("columns");
						for (int k =0; k < columns.length();k++){
							if (k!=0)
								query.append(" , ");
							JSONObject column  = columns.getJSONObject(k);
							query.append(column.getString("data")).append(" AS \"").append(column.getString("title")).append("\"");
						}
						profileConfig.setColumns(query.toString());
						DataAnalyserView dataAnalyserView  = (DataAnalyserView)dataAnalyserDao.getData(profileConfig,true);
						tableDetails.setData(dataAnalyserView.getJsonArray());
						tableDetails.setFileName(tableHeader);
						tableDetailsList.add(tableDetails);
					}
				}
			}
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return tableDetailsList;
	}
	public String updateToDB(UpdateColumns updateColumns) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		boolean status = false;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			status = dataAnalyserDao.updateToDB(updateColumns);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return Boolean.toString(status);
	}
	
	public String insertToDB(UpdateColumns updateColumns) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		boolean status = false;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			status = dataAnalyserDao.insertToDB(updateColumns);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return Boolean.toString(status);
	}
	
	public String deleteFromDB(UpdateColumns updateColumns) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		boolean status = false;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			status = dataAnalyserDao.deleteFromDB(updateColumns);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return Boolean.toString(status);
	}
	public String excutequery(String source,String database,String id)throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String status = null;;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			status = dataAnalyserDao.excutequery(source,database,id);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return status;
	}
	
	public String queryExecutor(String profileIds, String users ,String profileName ,String criticality,boolean sendMail)throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String status = null;
		E2emfView configResult=null;
		JSONObject profileConfigParameter = null;
		List<Object> queriesList = null;
		String[] profilesIdData = profileIds.split(",");
		DataTableView view = null;
		Gson gson = new Gson();
		ProfileConfig profileConfig = new ProfileConfig();
		  try {
			  queriesList =new ArrayList<>();
		   DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
		   for (String profile : profilesIdData) {
		    int profileId = Integer.parseInt(profile);
		    profileConfig.setId(profileId);
		    configResult = dataAnalyserDao.getProfileConfig(profileConfig);
		    view = (DataTableView) configResult;
		    profileConfigParameter = new JSONObject(
					gson.toJson(view.getData()));
				profileConfigParameter = profileConfigParameter
						.getJSONArray("myArrayList")
						.getJSONObject(0).getJSONObject("map");
				queriesList.add(profileConfigParameter);
			}
			
			String queriesListInString = gson.toJson(queriesList);
			status = dataAnalyserDao.queryExecutor(queriesListInString,users,profileName,criticality,sendMail);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return status;
	}
	public Map<String, List<String>> getUpdatedRows(String sourceName, String dbName, 
			String tableName, String primaryKeyColumn, String primaryKeys, String filter, 
			String orderBy, String columns, String groupBy)  throws Exception {
		Map<String, List<String>> result = null;
		try {
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			result = dataAnalyserDao.getUpdatedRows(sourceName, dbName, tableName,
					primaryKeyColumn, primaryKeys, filter, orderBy, columns, groupBy);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return result;
	}
	
	public static String getFilterQuery(String filterConfigJson,String source){
		String filterQuery;
		filterQuery = " 1=1";
		try{
		JSONArray filterConfig=new JSONArray(filterConfigJson);
		if (filterConfig == null || ( filterConfig != null && filterConfig.length()==0))
			return "";
		filterQuery = " 1=1";
		for ( int i = 0; i < filterConfig.length(); i++) {
			JSONObject filter=(JSONObject)filterConfig.get(i);
			
			String mappedField = (String) filter.get("fieldName");
			
			String  inNotIn = " IN",fromDateOperator=">=",toDateOperator="<=";			
			JSONArray defaultValue=(filter.has("defaultvalue"))?(JSONArray)filter.get("defaultvalue"):new JSONArray("[]");
			if(filter.has("innotin")   ||filter.has("innotin") ){
				inNotIn = " NOT IN";
				fromDateOperator="<=";
				toDateOperator=">=";
				
				
			}
			String lookupType=(String)filter.get("fieldType");
			if ("lookup".equalsIgnoreCase(lookupType) || "select".equalsIgnoreCase(lookupType)) {
				String lookupIds = "";
				JSONArray lookupData =defaultValue;
				if (lookupData.length()==0)
					continue;
				
				if(lookupData.length()==1 && filter.has("isSingle")){
					JSONObject lookupobj=(JSONObject)lookupData.get(0);
					String lookupid=(String)lookupobj.get("id");
					lookupIds = "'" + lookupid + "'";							
				} else {
					//JSONArray lookupDataArr=new JSONArray(lookupData);
					for ( int x = 0; x < lookupData.length(); x++) {
						JSONObject lookupobj=(JSONObject)lookupData.get(x);
						String lookupid=(String)lookupobj.get("id");
						if (x == 0 )							
							lookupIds = "'" + lookupid+ "'";
						else
							lookupIds += ",'" + lookupid + "'";
					}							
				}
				filterQuery += " AND (" + mappedField
				+ inNotIn+ " (" + lookupIds + ")";
			if (lookupIds.equals("''") && inNotIn.equals(" IN"))
				filterQuery += " OR " + mappedField +" is null )";
			else if (lookupIds.equals("''") && inNotIn.equals(" NOT IN"))
				filterQuery += " OR " + mappedField +" is not null )";
			else
				filterQuery += " )";
			} else if (lookupType.equals("NUM")) {
				if (filter.has("from-defaultvalue")
						&& !((String)filter.get("from-defaultvalue")).equals(""))
					filterQuery += " AND " + mappedField
							+ " >= " + (String)filter.get("from-defaultvalue");
				if (filter.has("to-defaultvalue")
						&& !((String)filter.get("to-defaultvalue")).equals(""))
					filterQuery += " AND " + mappedField
							+ " <= " + (String)filter.get("to-defaultvalue");
			} else if (lookupType.equals("DATE") || lookupType.equals("dynamicDate")) {
				String fromDate = (String)filter.get("from-defaultvalue");
				String toDate = (String)filter.get("to-defaultvalue");
				/*if(lookupType.equals("dynamicDate")  && isNaN(Date.parse(filterConfig[i]["from-defaultvalue"]))){
					fromDate = eval(filterConfig[i]["from-defaultvalue"]);
					toDate = eval(filterConfig[i]["to-defaultvalue"]);
				}*/
				if (source.equals("postgreSQL")){
					if (filter.has("from-defaultvalue")
							&& !((String)filter.get("from-defaultvalue")).equals(""))
						filterQuery += " AND "
								+ mappedField
								+  fromDateOperator+" to_date('"
								+ fromDate
								+ "', 'MM/dd/yyyy') ";
					if (filter.has("to-defaultvalue")
							&& !((String)filter.get("to-defaultvalue")).equals(""))
						filterQuery += " AND "
								+ mappedField
								+ toDateOperator+" to_date('"
								+ toDate
								+ "', 'MM/dd/yyyy')+1";
				}else{
					if (filter.has("from-defaultvalue")
							&& !((String)filter.get("from-defaultvalue")).equals(""))
						filterQuery += " AND unix_timestamp("
								+ mappedField
								+ ", 'MM/dd/yyyy') "+fromDateOperator+" unix_timestamp('"
								+ fromDate
								+ "', 'MM/dd/yyyy') ";
					if (filter.has("to-defaultvalue")
							&& !((String)filter.get("to-defaultvalue")).equals(""))
						filterQuery += " AND unix_timestamp("
								+ mappedField
								+ ", 'MM/dd/yyyy') "+toDateOperator+" unix_timestamp('"
								+ toDate
								+ "', 'MM/dd/yyyy')";
				}
			} else {
				if (filter.has("defaultvalue")
						&& !((String)filter.get("defaultvalue")).equals(""))
					filterQuery += " AND " + mappedField
							+ inNotIn+ " ('" + filter.get("defaultvalue")
							+ "')";
			}
		}
		}catch(Exception ex) {}
		return filterQuery;
	}

}
