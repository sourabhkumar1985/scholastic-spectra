package com.relevance.prism.util;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.opencsv.CSVReader;

public class CSVToExcelConverterUtil extends BaseUtil {

	public static File convertCSVTOXLs(String csvFilePath, String fileName) throws Exception {
		//PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ArrayList arList = null;
		ArrayList al = null;
		String thisLine;
		int count = 0;
		FileInputStream fis = new FileInputStream(csvFilePath);
		DataInputStream myInput = new DataInputStream(fis);
		String tempDir = System.getProperty("java.io.tmpdir");
		File xlsFile = new File(tempDir + File.separator + fileName + ".xlsx");
		FileOutputStream fileOut = new FileOutputStream(xlsFile);
		try {
			final XSSFWorkbook hwb=new XSSFWorkbook();
			XSSFSheet  sheet =  hwb.createSheet(fileName); 
			CSVReader reader = new CSVReader(new FileReader(csvFilePath), ',' , '"' , 0);               
			//Read CSV line by line and use the string array as you want       
			String[] row;
			int rowcount = 0;
			while ((row = reader.readNext()) != null) {
				XSSFRow xlsrow = sheet.createRow(0 + rowcount);
				System.out.println("Number of columns : " + row.length);
				if(row.length > 11){
					System.out.println("Row : " + (rowcount+1) + ":" + row[0] + ":"+ row[1]+ ":" + row[2]);
				}
				for (int k = 0; k < row.length; k++) {
					XSSFCell cell = xlsrow.createCell(k);
					String data = row[k];
					if (data.startsWith("=")) {
						cell.setCellType(Cell.CELL_TYPE_STRING);
						data = data.replaceAll("\"", "");
						data = data.replaceAll("=", "");
						cell.setCellValue(data);
					} else if (data.startsWith("\"")) {
						data = data.replaceAll("\"", "");
						cell.setCellType(Cell.CELL_TYPE_STRING);
						cell.setCellValue(data);
					} else {
						data = data.replaceAll("\"", "");
						cell.setCellType(Cell.CELL_TYPE_NUMERIC);
						cell.setCellValue(data);
					}
				}
				rowcount++;	
			}
			hwb.write(fileOut);			
			Log.Info("Excel file has been generated.");
			return xlsFile;
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			fis.close();
			fileOut.close();
		}		
		return xlsFile;
	}	
	
	public static void main(String args[]) throws Exception{
		String csvFilePath = "C:/Users/mbose4/Desktop/Desktop Files-Lenovo Laptop/PPA/Mail Digest/PPA-File Conversion - Exception/Detail View.csv";
		File xlsFile = convertCSVTOXLs(csvFilePath, "Detail View");
		System.out.println("Excel File Path :" + xlsFile.getAbsolutePath());
	}
}
