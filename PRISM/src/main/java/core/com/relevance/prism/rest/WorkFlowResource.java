package com.relevance.prism.rest;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.data.WfEntity;
import com.relevance.prism.data.WfTransition;
import com.relevance.prism.service.WorkFlowService;
import com.relevance.prism.util.PrismHandler;

	/**
	*
	* @author: Shawkath Khan & Manikantan & Madhan 
	* @Created_Date: Feb 19, 2016
	* @Purpose: Workflow Engine Web Service Methods
	* @Modified_By: Saurabh jha
	* @Modified_Date:  
	*/
@Path("/workflow")
public class WorkFlowResource extends BaseResource {
	@Context
	ServletContext context;

	public WorkFlowResource(@Context ServletContext value) {
		this.context = value;
		//System.out.println("Conext:" + this.context);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getentity")
	public String getEntity(@FormParam("entityid") String entityId){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			WorkFlowService workFlowService = new WorkFlowService();
			WfEntity wfEntity = workFlowService.getEntity(entityId); 
			Gson gson = new Gson();
			response = gson.toJson(wfEntity);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getentitylist")
	public String getEntityList(){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			WorkFlowService workFlowService = new WorkFlowService();
			ArrayList<WfEntity> wfEntityList = workFlowService.getEntityList(); 
			Gson gson = new Gson();
			response = gson.toJson(wfEntityList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getnexttransitions")
	public String getNextTransitions(@FormParam("entityid") String entityId, @FormParam("role") String role){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			WorkFlowService workFlowService = new WorkFlowService();
			WfEntity wfEntity = workFlowService.getEntity(entityId);
			ArrayList<WfTransition> nextTransitions = workFlowService.getNextTransitions(wfEntity, role);
			Gson gson = new Gson();
			response = gson.toJson(nextTransitions);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/applynexttransition")
	public String applyNextTransition(@FormParam("entityid") String entityId, @FormParam("user") String userId, @FormParam("role") String userRole, @FormParam("action") String action, @FormParam("comments") String comments){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			WorkFlowService workFlowService = new WorkFlowService();
			WfEntity wfEntity = workFlowService.getEntity(entityId);
			response = workFlowService.applyNextTransition(wfEntity, action, userId, userRole, comments);;
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addtoworkflow")
	public String addToWorkFlow(@FormParam("user") String userId, @FormParam("entityid") String entityId, @FormParam("startstate") String startState, @FormParam("comments") String comments){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			WorkFlowService workFlowService = new WorkFlowService();
			response = workFlowService.addToWorkflow(userId, entityId, startState, comments);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getalltransitions")
	public String getAllTransitions(){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			WorkFlowService workFlowService = new WorkFlowService();
			ArrayList<WfTransition> allTransitions = workFlowService.getAllTransitions();
			Gson gson = new Gson();
			response = gson.toJson(allTransitions);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateworkflowstatus")
	public String updateworkflowstatus(@FormParam(value="entityid") String entityid, @FormParam(value="status") String status,
			@FormParam(value="updatedby") String updatedby, @FormParam(value="comments") String comments) throws Exception{
		String response="";
		Boolean isUpdated = false;
		try {
			WorkFlowService workFlowService = new WorkFlowService();
			isUpdated = workFlowService.updateworkflowstatus(entityid, status,updatedby,comments);
			
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
			response = "{ \"isUpdated\"  :\""+isUpdated+"\"} "; 
			return response;
		} 
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		response = "{ \"isUpdated\"  :\""+isUpdated+"\"} "; 
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getworkflowauditlist")
	public String getWorkFlowAuditList(@FormParam(value="entityid") String entityid){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			WorkFlowService workFlowService = new WorkFlowService();
			ArrayList<WfEntity> wfEntityList = workFlowService.getWorkFlowAuditList(entityid); 
			Gson gson = new Gson();
			response = gson.toJson(wfEntityList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
