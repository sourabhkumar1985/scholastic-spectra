package com.relevance.prism.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.relevance.prism.dao.UserDao;
import com.relevance.prism.data.Report;
import com.relevance.prism.data.Role;
import com.relevance.prism.data.SecondaryRole;
import com.relevance.prism.data.User;
import com.relevance.prism.data.UserAdditionalDetails;
import com.relevance.prism.data.UserSecondaryRole;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class UserService extends BaseService {

	public Role getTheme(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Role role = null;
		try {
			Log.Info("getTheme method called on UserService");
			UserDao userDao = new UserDao();
			role = userDao.getTheme(obj);
			Log.Info("Fetched Theme  from DAO " + role);

		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return role;
	}

	/* user email notification */
	public String sendCredentialsToUser(String subject, String message,
			String username) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User user = new User();
		try {
			Log.Info("getUser method called on UserService");
			UserDao userDao = new UserDao();
			String[] users = username.split("\\,");
			for (String selectedUser : users) {
				user.setUsername(selectedUser);
				user = userDao.sendCredentialsToUser(user);
				Log.Info("Fetched User object from DAO " + user);
				MailService mailer = new MailService();
				String emailRecepients = user.getEmail();
				if (emailRecepients != null && !emailRecepients.equals("null")) {
					mailer.sendGenericEmailWithAttachment(subject, message,
							emailRecepients, null);
					return "{\"message\":\"Mail has been sent sucessfully\"}";
				}
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return null;
	}
	/* user password to user */
	public String sendPasswordToUser(String username,String subject) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User user = new User();
		String message = null;
		try {
			Log.Info("getUser method called on UserService");
			UserDao userDao = new UserDao();
			user.setUsername(username);
			user = userDao.sendCredentialsToUser(user);
			Log.Info("Fetched User object from DAO " + user);
			if(user.getUsername() != null && !user.getUsername().equals("null")){
				MailService mailer = new MailService();
				String emailRecepients = user.getEmail();
				String password = user.getPassword();
				message ="Your password is : "+password;
				if (emailRecepients != null && !emailRecepients.equals("null")) {
					mailer.sendGenericEmailWithAttachment(subject, message,emailRecepients, null);
					return "{\"success\":\"Password has been sent sucessfully.\"}";
				}
			}else{
				return "{\"error\":\"User does not exists\"}";
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return null;
	}
	// ///////////////////////////////////////////////////////////
	public String insertUser(User user) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			UserDao userDao = new UserDao();
			responseMessage = userDao.insertUser(user);

			Log.Info("Fetched User object from DAO " + user);
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}

	public User getUser(User user) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		//User user = new User();
		try {
			Log.Info("getUser method called on UserService");
			UserDao userDao = new UserDao();
			
			user = userDao.getUser(user);
			Log.Info("Fetched User object from DAO " + user.getUsername());

		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return user;
	}

	public String updateUser(User user) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			Log.Info("updateUser method called on UserService");
			UserDao userDao = new UserDao();
			responseMessage = userDao.updateUser(user);
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	public String resetPassword(String username,String password,String newpassword) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			Log.Info("reset password method called on UserService");
			UserDao userDao = new UserDao();
			responseMessage = userDao.resetPassword(username,password,newpassword);
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	public String updateUserLastLoginDate(UserAdditionalDetails useradditional) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			Log.Info("updateUser method called on UserService");
			UserDao userDao = new UserDao();
			responseMessage = userDao.updateUserLastLoginDate(useradditional);
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}

	public String updateUserPrimaryRole(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			Log.Info("updateUser method called on UserService");
			UserDao userDao = new UserDao();
			User user = new User();
			user.setUsername(obj[0].toString());
			user.setPrimaryRole(obj[1].toString());
			responseMessage = userDao.updateUserPrimaryRole(user);
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}

	public String deleteUser(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String successMessage = null;
		try {
			Log.Info("deleteUser method called on UserService");
			UserDao userDao = new UserDao();
			User user = new User();
			user.setUsername(obj[0].toString());
			successMessage = userDao.deleteUser(user);
			return successMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return successMessage;
	}

	public List<User> getUserList() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<User> userList = null;
		try {
			UserDao userDao = new UserDao();
			userList = userDao.getUserList();
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return userList;
	}

	public User insertUserRole(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserDao userDao = new UserDao();
		User user = new User();
		try {
			Log.Info("insertRoleForUser method called on UserService");
			user.setUsername(obj[0].toString());
			List<String> userRoles = new ArrayList<>();
			userRoles.add(0, obj[1].toString().trim());
			user.setRoles(userRoles);
			user = userDao.insertUserRoles(user);
			Log.Info("Role inserted in DB." + user);
			return user;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return user;
	}

	public String deleteUserRole(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserDao userDao = new UserDao();
		User user = new User();
		String responseMessage = null;
		try {
			Log.Info("deleteRoleFromUser method called on UserService");
			user.setUsername(obj[0].toString());
			String[] roles = (obj[1].toString()).split(",");
			user.setRolesAsArray(roles);
			responseMessage = userDao.deleteUserRoles(user);
			Log.Info("User Role deleted in DB." + user);
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}

	public List<Role> getRoles(ArrayList<Role> roles) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserDao userDao = new UserDao();
		List<Role> roleList = new ArrayList<>();
		try {
			Log.Info("getUserRoles method called on UserService");
			roleList = userDao.getRoles(roles);
			Log.Info("Fetched User object from DAO " + roleList);
			return roleList;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return roleList;
	}

	public String insertRole(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User user = new User();
		String responseMessage = null;
		try {
			Log.Info("insertRole method called on UserService");
			UserDao userDao = new UserDao();
			user.setDisplayname(obj[0].toString());
			user.setName(obj[1].toString());
			user.setFavicon(obj[2].toString());
			user.setLogo(obj[3].toString());
			user.setLoginContent(obj[4].toString());
			user.setThemeName(obj[5].toString());
			user.setMenuOnTop(Long.parseLong(obj[6].toString()));
			user.setDisplayOrder(Long.parseLong(obj[7].toString()));
			user.setWidgetClass(obj[8].toString());
			user.setIcon(obj[9].toString());
			user.setWidgetColor(obj[10].toString());
			user.setFixedHeader(Long.parseLong(obj[11].toString()));
			user.setFixedNavigation(Long.parseLong(obj[12].toString()));
			user.setFixedRibbon(Long.parseLong(obj[13].toString()));
			user.setSso(Long.parseLong(obj[14].toString()));
			user.setSignUp(Long.parseLong(obj[15].toString()));
			user.setParentRole(obj[16].toString());
			user.setTopRightIcon(obj[17].toString());
			responseMessage = userDao.insertRole(user);

			Log.Info("Fetched Role object from DAO " + user);
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}

	public String updateRole(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			Log.Info("updateRole method called on UserService");
			UserDao userDao = new UserDao();
			User user = new User();
			user.setName(obj[1].toString());
			user.setDisplayname(obj[0].toString());
			user.setFavicon(obj[2].toString());
			user.setLogo(obj[3].toString());
			user.setLoginContent(obj[4].toString());
			user.setThemeName(obj[5].toString());
			user.setMenuOnTop(Long.parseLong(obj[6].toString()));
			user.setDisplayOrder(Long.parseLong(obj[7].toString()));
			user.setWidgetClass(obj[8].toString());
			user.setIcon(obj[9].toString());
			user.setWidgetColor(obj[10].toString());
			user.setFixedHeader(Long.parseLong(obj[11].toString()));
			user.setFixedNavigation(Long.parseLong(obj[12].toString()));
			user.setFixedRibbon(Long.parseLong(obj[13].toString()));
			user.setSso(Long.parseLong(obj[14].toString()));
			user.setSignUp(Long.parseLong(obj[15].toString()));
			user.setParentRole(obj[16].toString());
			user.setTopRightIcon(obj[17].toString());
			responseMessage = userDao.updateRole(user);
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}

	public String getUserRoleActionList(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserDao userDao = new UserDao();
		User user = new User();
		String resultString = null;
		try {
			Log.Info("getRoleActions method called on UserService");
			user.setUsername(obj[0].toString());
			resultString = userDao.getUserRoleActionList(user);
			Log.Info("Fetched User object with assingned Actions from DAO "
					+ user);
			if (resultString == null || resultString.isEmpty()
					|| "[]".equals(resultString) || resultString.length() == 0) {
				return "{\"message\":\"The logged in user does not have any role assigned\"}";
			}
			return resultString;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return resultString;
	}

	public List<SecondaryRole> getSecondaryRoles(String email) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserDao userDao = new UserDao();
		List<SecondaryRole> secondaryRolesList = new ArrayList<>();
		try {
			Log.Info("getSecondaryRoles method called on UserService");
			secondaryRolesList = userDao.getSecondaryRoles(email);
			Log.Info("Fetched Secondary Roles from DAO " + secondaryRolesList);
			return secondaryRolesList;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return secondaryRolesList;
	}

	public List<UserSecondaryRole> getUserSecondaryRoles(String app,
			String username) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserDao userDao = new UserDao();
		List<UserSecondaryRole> userSecondaryRolesList = new ArrayList<>();
		try {
			Log.Info("getUserSecondaryRoles method called on UserService");
			userSecondaryRolesList = userDao.getUserSecondaryRoles(app,
					username);
			Log.Info("Fetched User Secondary Roles from DAO "
					+ userSecondaryRolesList);
			return userSecondaryRolesList;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return userSecondaryRolesList;
	}

	public List<UserSecondaryRole> getDataSourcesForUser(String app,
			String username) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserDao userDao = new UserDao();
		List<UserSecondaryRole> userSecondaryRolesList = new ArrayList<>();
		try {
			Log.Info("getDataSourcesForUser method called on UserService");
			userSecondaryRolesList = userDao.getDataSourcesForUser(app,
					username);
			Log.Info("Fetched Data Sources For User from DAO "
					+ userSecondaryRolesList);
			return userSecondaryRolesList;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return userSecondaryRolesList;
	}

	public String insertUserSecondaryRole(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserSecondaryRole userSecondaryRole = new UserSecondaryRole();
		String responseMessage = null;
		try {
			Log.Info("insertUserSecondaryRole method called on UserService");
			UserDao userDao = new UserDao();
			userSecondaryRole.setUserName(obj[0].toString());
			userSecondaryRole.setSecondaryRole(obj[1].toString());
			userSecondaryRole.setDataSource(obj[2].toString());
			responseMessage = userDao
					.insertUserSecondaryRole(userSecondaryRole);

			Log.Info("Inserted Secondary Role in DB " + userSecondaryRole);
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}

	public String updateUserSecondaryRole(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserSecondaryRole userSecondaryRole = new UserSecondaryRole();
		String responseMessage = null;
		try {
			Log.Info("insertSecondaryRole method called on UserService");
			UserDao userDao = new UserDao();
			userSecondaryRole.setUserSecondaryRoleId(Integer.parseInt(obj[0]
					.toString()));
			userSecondaryRole.setUserName(obj[0].toString());
			userSecondaryRole.setSecondaryRole(obj[1].toString());
			userSecondaryRole.setDataSource(obj[2].toString());
			responseMessage = userDao
					.updateUserSecondaryRole(userSecondaryRole);

			Log.Info("Updated Secondary Role in DB " + userSecondaryRole);
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}

	public String deleteUserSecondaryRole(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserSecondaryRole secondaryRole = new UserSecondaryRole();
		String responseMessage = null;
		try {
			Log.Info("insertSecondaryRole method called on UserService");
			UserDao userDao = new UserDao();
			secondaryRole.setUserSecondaryRoleId(Integer.parseInt(obj[0]
					.toString()));

			responseMessage = userDao.deleteUserSecondaryRole(secondaryRole);

			Log.Info("Deleted Secondary Role from DB " + secondaryRole);
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}

	// Adding UserAuthentication

	public User userAuthentication(User user) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
        User userId= null;
		try {
			UserDao userDao = new UserDao();
			userId = userDao.userAuthentication(user);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return userId;
	}

	public Report getReportQueryByReportName(String reportName)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Report resultReport = null;
		try {
			UserDao userDao = new UserDao();
			Report report = new Report();
			report.setName(reportName);
			resultReport = userDao.getReportQueryByReportName(report);
		} catch (Exception e) {
			Log.Error("Exception while fetching Report details from DB "
					+ e.getMessage());
			Log.Error(e);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return resultReport;
	}

	public ArrayList<LinkedHashMap<String, Object>> getReports(String id)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserDao userDao = new UserDao();
		Report report = new Report();
		ArrayList<LinkedHashMap<String, Object>> reportdata = new ArrayList<>();
		try {
			if (id == null || id.trim().isEmpty()) {
				String query = "select * from E2E_SAP_RPT_L2_PARTNERFUNCTION limit 65000";
				report.setQuery(query);
				reportdata = userDao
						.getReports(report);

			} else {
				report.id = id;
				report = userDao.getReportQuery(report);
				reportdata = userDao
						.getReports(report);
				Log.Info("Fetched Report object from DAO " + report);
			}

		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportdata;
	}

	public List<Report> getReportsGridView(String id) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserDao userDao = new UserDao();
		Report report = new Report();
		List<Report> listReport = new ArrayList<>();
		try {
			Log.Info("getUserRoles method called on UserService");
			listReport = userDao.getReportQueryGrid();
			Log.Info("Fetched Report object from DAO Grid View " + report);

		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return listReport;
	}

	public String getUserPrimaryRole(String userName) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserDao userDao = new UserDao();
		String userPrimaryRole = null;
		try {
			Log.Info("getUserPrimaryRole method called on UserService");
			userPrimaryRole = userDao.getUserPrimaryRole(userName);
			Log.Info("Fetched User Primary Role");
			return userPrimaryRole;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return userPrimaryRole;
	}

	public List<Role> getUserRolesForDisplay(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserDao userDao = new UserDao();
		User user = new User();
		List<Role> roleList = null;
		try {
			Log.Info("getUserRoles method called on UserService");
			user.setUsername(obj[0].toString());
			roleList = userDao.getUserRoles(user);
			Log.Info("Fetched User object from DAO " + user);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return roleList;
	}

	public Role getRole(String roleName) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserDao userDao = new UserDao();
		Role role = null;
		try {
			role = userDao.getRole(roleName);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return role;
	}

	public List<LinkedHashMap<String, String>> getColumnValueList(
			Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserDao userDao = new UserDao();
		String env = null;
		String db = null;
		String table = null;
		String whereCondition = null;
		List<LinkedHashMap<String, String>> output = null;
		try {
			Log.Info("getColumnValueList method called on UserService");
			env = obj[0].toString();
			db = obj[1].toString();
			table = obj[2].toString();
			if (obj[3] != null) {
				whereCondition = obj[3].toString();
			} else {
				whereCondition = null;
			}
			output = userDao.getColumnValueList(env, db, table, whereCondition);
			Log.Info("Fetched ColumnValueList object from DAO");
			return output;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return output;
	}
	
	public List<String> getEmailAddressByBusinessRole(String businessRoleName) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<String> usersList = null;
		try {
			Log.Info("getEmailAddressByBusinessRole method called on UserService");
			UserDao userDao = new UserDao();
			usersList = userDao.getEmailAddressByBusinessRole(businessRoleName);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return usersList;
	}

}
