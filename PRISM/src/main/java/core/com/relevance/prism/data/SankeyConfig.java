package com.relevance.prism.data;

import java.util.ArrayList;
import java.util.HashMap;

public class SankeyConfig {
	private ArrayList<SankeyLink> sankeyLinkList;
	private HashMap<String, SankeyFilter> sankeyFilterMap;
	
	
	public ArrayList<SankeyLink> getSankeyLinkList() {
		return sankeyLinkList;
	}
	public void setSankeyLinkList(ArrayList<SankeyLink> sankeyLinkList) {
		this.sankeyLinkList = sankeyLinkList;
	}
	public HashMap<String, SankeyFilter> getSankeyFilterMap() {
		return sankeyFilterMap;
	}
	public void setSankeyFilterMap(HashMap<String, SankeyFilter> sankeyFilterMap) {
		this.sankeyFilterMap = sankeyFilterMap;
	}

	
}
