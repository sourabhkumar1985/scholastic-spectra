package com.relevance.prism.service;

import java.util.HashMap;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.ModifiableSolrParams;

import com.google.gson.Gson;
import com.relevance.prism.data.E2emfBusinessObject;
import com.relevance.prism.data.E2emfView;
//import com.relevance.eemf.data.E2emfMaterialFlowData;
import com.relevance.prism.data.GoodsHistoryList;
import com.relevance.prism.data.MaterialReportList;
import com.relevance.prism.data.SearchParam;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
/**
 * @author Manikantan
 * E2emf Search Service 
 *
 */
public class TokenAnalysisService extends BaseService implements Service {

	//E2emfAppUtil appUtil;
	//E2emfDbUtil dbUtil;
	
	String solarUrl = "SOLR_SERVER_URL";
	String solarInstanceName = "";
	
	static String solarServerInsanceUrl = null;

	public TokenAnalysisService() {

		//appUtil = new E2emfAppUtil();
		//dbUtil = new E2emfDbUtil();
	}

	@Override
	public String getJsonObject() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getJsonObject(Object... obj) throws Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String searchResultJson = null;
		HttpSolrServer solarServer = null;
		QueryResponse queryResponse = null;
		
		SearchParam searchParam = null;
		String searchCriteria = null;
		
		if(obj != null && obj.length >1){
			searchParam = (SearchParam)obj[0];
			searchCriteria = ((String) obj[1]).trim();
			Log.Info("Searching initiated for " + searchCriteria);
		}
		if(searchCriteria != null && searchCriteria.equalsIgnoreCase("tokenAnalysisService") && searchParam.getFacet() == null) {
			
			 solarServer = getSolarServerConnection();
			 
			  ModifiableSolrParams parameters = new ModifiableSolrParams();	 
			  parameters.set("q", "keywords:" + searchParam.getKey());//Search only in all data column
			  parameters.set("defType", "edismax");
			  //parameters.set("wt", "xml");//Not required since converting from the object directly
			  parameters.set("start", "0");
			  parameters.set("rows", "0");
			  parameters.set("qf", "rawdata rank^2 keywords");
			  parameters.set("facet", "true");
			  parameters.set("facet.field", "facet");
			  
			  //parameters.set("fq", "facet:"+queryFilterVal);	 

			  try {
				  if(solarServer != null){
					  queryResponse = solarServer.query(parameters);  
					  Log.Info("Recieved response from solar successfully "  );
				  }else{
					  Log.Error("Unable to connect to solarServer...");
				  }
				  			
				  Gson gson = new Gson();
				  HashMap<String, String> facetMap = new HashMap<String, String>();
				  
				  //First for loop is for fieldname as "facet"
				  for(FacetField facetField : queryResponse.getFacetFields())
				  {
						  for(Count facetFieldCount : facetField.getValues())
						  {
							  facetMap.put(facetFieldCount.getName(), "" + facetFieldCount.getCount());
						  }
				  }
				  String json = gson.toJson(facetMap);
				  searchResultJson = json;
			  }
			  		
			  catch (SolrServerException e) {					  
				  	Log.Error("Exception in getting Solar Server Connection." + e.getStackTrace());
					Log.Error(e);
					throw new AppException(e.getMessage(),e.getStackTrace(),"Error while processing the request.", 2 ,e.getCause(), true);
			  }
			  catch(Exception e){
					Log.Error("Exception Fetching Token Analysis Details." + e.getStackTrace());
					Log.Error(e);
					if(e instanceof NullPointerException){
						throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
					}
					throw new AppException(e.getMessage(),e.getStackTrace(),"Error while processing the request.", 2 ,e.getCause(), true);
			  }
			
		}
	//To implement detailed search	
	  else if(searchCriteria != null && searchCriteria.equalsIgnoreCase("tokenAnalysisService")  && searchParam.getFacet() != null) {
					
					 
					 solarServer = getSolarServerConnection();
					   
					  ModifiableSolrParams parameters = new ModifiableSolrParams();	 
					  parameters.set("q", "keywords:" + searchParam.getKey());
					  parameters.set("defType", "edismax");
					  //parameters.set("wt", "xml");//Not required since converting from the object directly
					  parameters.set("start", "0");
					  parameters.set("rows", "50");
					  //parameters.set("fl", "output");
					  parameters.set("qf", "rawdata rank^2 keywords");
					  parameters.set("fq", "facet:" + searchParam.getFacet());	 

					  try {
						  if(solarServer != null){
							  queryResponse = solarServer.query(parameters);  
							  Log.Info("Recieved response from solar successfully "  );
						  }else{
							  Log.Error("Unable to connect to solarServer...");
						  }
						  			

						  Gson gson = new Gson();
						  String json = gson.toJson(queryResponse.getResults());
						 						  
						  searchResultJson = json;
						  //System.out.println("parsedJSONOutPut:" + searchResultJson);
						  //Log.Info("parsedJSONOutPut:" +searchResultJson);
					  }
					  catch (SolrServerException e) {					  
						  	Log.Error("Exception in getting Solar Server Connection." + e.getStackTrace());
							Log.Error(e);
							throw new AppException(e.getMessage(),e.getStackTrace(),"Error while processing the request.", 2 ,e.getCause(), true);
					  }
					  catch(Exception e){
							Log.Error("Exception Fetching Token Analysis Details." + e.getStackTrace());
							Log.Error(e);
							if(e instanceof NullPointerException){
								throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
							}
							throw new AppException(e.getMessage(),e.getStackTrace(),"Error while processing the request.", 2 ,e.getCause(), true);					 
					}
				}		
	  else if(searchCriteria != null && searchCriteria.equalsIgnoreCase("tokenAnalysisVisualizationService")  && searchParam.getFacet() != null) {
			
			 
			 solarServer = getSolarServerConnection();
			   
			  ModifiableSolrParams parameters = new ModifiableSolrParams();	 
			  
			  String rankFilter = "";
			  
			  if ("2".equalsIgnoreCase(searchParam.getType()))
					rankFilter = "  and (rank:2 or rank:3)";
			  else
				  rankFilter = "  and (rank:1)";
			  
			  parameters.set("q", "keywords:" + searchParam.getKey()+ rankFilter);
			  parameters.set("defType", "edismax");
			  //parameters.set("wt", "xml");//Not required since converting from the object directly
			  parameters.set("start", "0");
			  parameters.set("rows", "50");
			  parameters.set("fl", "weight");
			  parameters.set("group", true);
			  parameters.set("group.field",searchParam.getFacet());
			  parameters.set("stats","on");
			  parameters.set("stats.field","weight");
			  parameters.set("stats.facet",searchParam.getFacet());
			  parameters.set("qf", "rawdata rank^2 keywords");
			  //parameters.set("fq", "facet:" + searchParam.getFacet());	 

			  try {
				  if(solarServer != null){
					  queryResponse = solarServer.query(parameters);  
					  Log.Info("Recieved response from solar successfully "  );
				  }else{
					  Log.Error("Unable to connect to solarServer...");
				  }
				  Gson gson = new Gson();
				  String json = gson.toJson(queryResponse.getFieldStatsInfo());
				 						  
				  searchResultJson = json;
				  //System.out.println("parsedJSONOutPut:" + searchResultJson);
				  //Log.Info("parsedJSONOutPut:" +searchResultJson);
				  
			  }
			  catch (SolrServerException e) {					  
				  	Log.Error("Exception in getting Solar Server Connection." + e.getStackTrace());
					Log.Error(e);
					throw new AppException(e.getMessage(),e.getStackTrace(),"Error while processing the request.", 2 ,e.getCause(), true);

			  }
			  catch(Exception e){
					Log.Error("Exception Fetching Token Analysis Details." + e.getStackTrace());
					Log.Error(e);
					if(e instanceof NullPointerException){
						throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
					}
				throw new AppException(e.getMessage(),e.getStackTrace(),"Error while processing the request.", 2 ,e.getCause(), true);			  }
		}	
		return searchResultJson;

	}

	private HttpSolrServer getSolarServerConnection() throws Exception{
		HttpSolrServer serverCon = null;
		
		if(solarServerInsanceUrl == null){
			//solarUrl = appUtil.getAppProperty(E2emfConstants.solarTokenAnalysisUrl);
			solarInstanceName = E2emfAppUtil.getAppProperty(E2emfConstants.solarInstanceTokenAnalysisName);
			//solarServerInsanceUrl = solarUrl+solarInstanceName+"/";				
		}
			
		try
		{		  
			serverCon = new HttpSolrServer(solarInstanceName);
		} 
		catch(Exception e)
		{
				PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return serverCon;
	}

	@Override
	public E2emfBusinessObject getBusinessObject() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GoodsHistoryList getGoodsHistoryViewObject(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public E2emfBusinessObject getBusinessObject(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public E2emfView getViewObject(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E2emfView getViewObject(E2emfBusinessObject material) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateJsonString(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MaterialReportList getMaterialReportViewObject(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	/*@Override
	public E2emfMaterialFlowData getE2emfMaterialFlowData(String source)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	public String getActions(Object... obj) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getRoles(Object... obj) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPrimaryRoles(Object... obj) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSearchSuggestion(Object... obj) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

}
