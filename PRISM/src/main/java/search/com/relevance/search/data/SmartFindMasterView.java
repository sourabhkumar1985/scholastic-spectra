package com.relevance.search.data;

public class SmartFindMasterView {

	private String sfCount;
	private String key;
	private String indexStatus;
	private String category;
	private String ftype;
	private String imageKey;
	private String imagePath;
	private String duplicate;
	private String modDate;
	private String ocrStatus;
	private String oimageName;
	private String patternMatchingStatus;
	private String pngConverted;
	private String sourcePath;
	private String spellCheckStatus;
	private String subCategory;
	private String system;
	private String threadProcessed;
	private String titleFound;
	private String componentsFound;
	private String docNumFound;
	private String manufacturerFound;
	private String setName;
	private String solrWMPath;
	private String solrBigimgPath;
	private String solrSmallimgPath;
	private String jpgConverted;
	private String wmConverted;
	public String getSolrWMPath() {
		return solrWMPath;
	}
	public void setSolrWMPath(String solrWMPath) {
		this.solrWMPath = solrWMPath;
	}
	public String getSolrBigimgPath() {
		return solrBigimgPath;
	}
	public void setSolrBigimgPath(String solrBigimgPath) {
		this.solrBigimgPath = solrBigimgPath;
	}
	public String getSolrSmallimgPath() {
		return solrSmallimgPath;
	}
	public void setSolrSmallimgPath(String solrSmallimgPath) {
		this.solrSmallimgPath = solrSmallimgPath;
	}
	public String getJpgConverted() {
		return jpgConverted;
	}
	public void setJpgConverted(String jpgConverted) {
		this.jpgConverted = jpgConverted;
	}
	public String getWmConverted() {
		return wmConverted;
	}
	public void setWmConverted(String wmConverted) {
		this.wmConverted = wmConverted;
	}
	public String getSetName() {
		return setName;
	}
	public void setSetName(String setName) {
		this.setName = setName;
	}
	public String getSfCount() {
		return sfCount;
	}
	public void setSfCount(String sfCount) {
		this.sfCount = sfCount;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getIndexStatus() {
		return indexStatus;
	}
	public void setIndexStatus(String indexStatus) {
		this.indexStatus = indexStatus;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getFtype() {
		return ftype;
	}
	public void setFtype(String ftype) {
		this.ftype = ftype;
	}
	public String getImageKey() {
		return imageKey;
	}
	public void setImageKey(String imageKey) {
		this.imageKey = imageKey;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getDuplicate() {
		return duplicate;
	}
	public void setDuplicate(String duplicate) {
		this.duplicate = duplicate;
	}
	public String getModDate() {
		return modDate;
	}
	public void setModDate(String modDate) {
		this.modDate = modDate;
	}
	public String getOcrStatus() {
		return ocrStatus;
	}
	public void setOcrStatus(String ocrStatus) {
		this.ocrStatus = ocrStatus;
	}
	public String getOimageName() {
		return oimageName;
	}
	public void setOimageName(String oimageName) {
		this.oimageName = oimageName;
	}
	public String getPatternMatchingStatus() {
		return patternMatchingStatus;
	}
	public void setPatternMatchingStatus(String patternMatchingStatus) {
		this.patternMatchingStatus = patternMatchingStatus;
	}
	public String getPngConverted() {
		return pngConverted;
	}
	public void setPngConverted(String pngConverted) {
		this.pngConverted = pngConverted;
	}
	public String getSourcePath() {
		return sourcePath;
	}
	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}
	public String getSpellCheckStatus() {
		return spellCheckStatus;
	}
	public void setSpellCheckStatus(String spellCheckStatus) {
		this.spellCheckStatus = spellCheckStatus;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	public String getSystem() {
		return system;
	}
	public void setSystem(String system) {
		this.system = system;
	}
	public String getThreadProcessed() {
		return threadProcessed;
	}
	public void setThreadProcessed(String threadProcessed) {
		this.threadProcessed = threadProcessed;
	}
	public String getTitleFound() {
		return titleFound;
	}
	public void setTitleFound(String titleFound) {
		this.titleFound = titleFound;
	}
	public String getComponentsFound() {
		return componentsFound;
	}
	public void setComponentsFound(String componentsFound) {
		this.componentsFound = componentsFound;
	}
	public String getDocNumFound() {
		return docNumFound;
	}
	public void setDocNumFound(String docNumFound) {
		this.docNumFound = docNumFound;
	}
	public String getManufacturerFound() {
		return manufacturerFound;
	}
	public void setManufacturerFound(String manufacturerFound) {
		this.manufacturerFound = manufacturerFound;
	}
	}
