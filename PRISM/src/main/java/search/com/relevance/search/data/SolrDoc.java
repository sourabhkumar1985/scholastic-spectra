package com.relevance.search.data;

import java.util.Date;
import java.util.HashMap;

public class SolrDoc {
	private String id;
	private String rawData;
	private String keywords;
	private String rank;
	private String weight;
	private String facet;
	private String source;
	private Date modDate;
	private HashMap<String, String> outputMap;
	
	public HashMap<String, String> getOutputMap() {
		return outputMap;
	}
	public void setOutputMap(HashMap<String, String> outputMap) {
		this.outputMap = outputMap;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRawData() {
		return rawData;
	}
	public void setRawData(String rawData) {
		this.rawData = rawData;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getFacet() {
		return facet;
	}
	public void setFacet(String facet) {
		this.facet = facet;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public Date getModDate() {
		return modDate;
	}
	public void setModDate(Date modDate) {
		this.modDate = modDate;
	}
	

}
