package com.relevance.search.data;

import com.relevance.prism.data.E2emfBusinessObject;

/**
 * @author Mani
 * E2emf Search Business Object  
 *
 */
public class QueryItem extends E2emfBusinessObject
{

	private String key;
	private String type;
	private String field;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	
	
}
