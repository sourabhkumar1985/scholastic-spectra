package com.relevance.search.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.relevance.prism.dao.BaseDao;
import com.relevance.prism.data.ClinicalSpeciality;
import com.relevance.prism.data.NGramResult;
import com.relevance.prism.data.SearchLog;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismDbUtil;
import com.relevance.prism.util.SmartFindSpellChecker;
import com.relevance.search.data.NGramAnalyserStored;
import com.relevance.search.data.NGramEntry;
import com.relevance.search.data.QueryItem;
import com.relevance.search.data.QueryParam;
import com.relevance.search.data.SmartFindDocument;
import com.relevance.search.data.SmartFindMasterView;
import com.relevance.search.service.InstrumentSearchService;

public class InstrumentSearchDao extends BaseDao{
	private static String searchHistoryQuery = "SELECT userid, username, display_query, json_query, type, module, logdate,bookmarkname,id FROM search_log";
	private static String smartFindL2Query = "SELECT \"key\", imagekey, imagepath, moddate, oimagename, sourcepath, indexstatus, category, ftype, isduplicate, ocrstatus, patternmatchingstatus, pngconverted, spellcheckstatus, subcategory, system, threadprocessed, titlefound, componentsfound, docnumfound, manufacturerfound FROM ";
	private static String clinicalSpecialityQuery = "SELECT * FROM cspeciality order by priority desc";
	private static String imageSetsQuery = "SELECT distinct imageset FROM solrmaster";
	
		public List<SearchLog> getSearchHistory(SearchLog searchQuery, String orderBy, String orderStyle) throws AppException{
		StringBuilder queryToExecute = null;
		StringBuilder where = null;
		StringBuilder orderby = null;
		List<SearchLog> searchHistory = new ArrayList<SearchLog>();
		SearchLog searchLog = null;
		Connection connection = null;
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy hh:mm:ss a zzz");
		try {
			where = new StringBuilder(" where ");
			orderby = new StringBuilder(" order by logdate desc ");
			if (!searchQuery.getUserID().equalsIgnoreCase(null)
					&& !searchQuery.getUserID().equalsIgnoreCase("NULL")) {
				where.append(" userid = '").append(searchQuery.getUserID())
						.append("' and ");
			}
			if (!searchQuery.getModule().equalsIgnoreCase(null)
					&& !searchQuery.getModule().equalsIgnoreCase("NULL")) {
				where.append(" module = '").append(searchQuery.getModule())
						.append("' and ");
			}
			if (!searchQuery.getType().equalsIgnoreCase(null)
					&& !searchQuery.getType().equalsIgnoreCase("NULL")) {
				where.append(" type = '").append(searchQuery.getType())
						.append("'");
			}
			if (orderBy != null && !"".equalsIgnoreCase(orderBy)) {
				where.append(" order by ").append(orderBy);
			}
			if (orderStyle != null && !"".equalsIgnoreCase(orderStyle)) {
				where.append(" ").append(orderStyle);
			}
			
			
			queryToExecute = new StringBuilder(searchHistoryQuery)
					.append(where);
			connection = dbUtil.getPostGresConnection();
			Statement stmt1 = connection.createStatement();
			ResultSet rs = stmt1.executeQuery(queryToExecute.toString());

			if (rs != null) {
				while (rs.next()) {
					searchLog = new SearchLog();
					searchLog.setUserName(rs.getString("username"));
					searchLog.setUserID(rs.getString("userid"));
					searchLog.setType(rs.getString("type"));
					searchLog.setModule(rs.getString("module"));
					searchLog.setJsonQuery(rs.getString("json_query"));
					searchLog.setDisplayQuery(rs.getString("display_query"));
					searchLog.setLogDate(sdf.format(rs.getTimestamp("logdate")));
					searchLog.setId(rs.getInt("id"));
					if (rs.getString("bookmarkname") != null)
						searchLog.setBookmarkName(rs.getString("bookmarkname"));
					
					if(searchLog.getLogDate() != null && !"".equals(searchLog.getLogDate())) {
						Date expDate = sdf.parse(searchLog.getLogDate());
						Calendar c = Calendar.getInstance(); 
						c.setTime(expDate); 
						c.add(Calendar.DATE, 60);
						expDate = c.getTime();
						searchLog.setExpirationDate(sdf.format(expDate));
					}
					
					searchHistory.add(searchLog);
				}
			}
			return searchHistory;
		} catch (SQLException sql) {
			Log.Error("SQLException while retrieving the user list from DB"+ sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 1 ,sql.getCause(), true);
		} catch (Exception e) {
			Log.Error("Exception while retrieving user list from DB" + e);
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 2 ,e.getCause(), true);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
			}
		}
	}
	
	
	public ArrayList<SmartFindMasterView> getSmartFindMasterDetails(Object... obj) throws AppException {
		ArrayList<SmartFindMasterView> viewList = new ArrayList<SmartFindMasterView>();
		
		StringBuilder queryToExecute = null;
		ResultSet rs = null;
		Connection con = null;
		try {
				Log.Info("Fetching the DefaultView for Smart Find Master Profile ... ");
				String tableName = "sf_ag_master";
				
				queryToExecute = new StringBuilder();
				queryToExecute.append("select sfcount,indexstatus,category,ftype,isduplicate");
				queryToExecute.append(",ocrstatus,patternmatchingstatus,pngconverted,spellcheckstatus");
				queryToExecute.append(",subcategory,system,threadprocessed,titlefound,componentsfound,docnumfound,manufacturerfound,imageset3,jpgconverted,wmconverted");
				
				queryToExecute.append(" from ");
				queryToExecute.append(tableName);
				
				
				//con = dbUtil.getHiveConnection(E2emfConstants.smartFindDB);
				con = dbUtil.getPostGresConnection();
				Log.Info("Using Smart Find Postgress Connection...");

				Log.Debug("SmartFind Query to Execute :"+queryToExecute.toString());

				Statement stmt = con.createStatement();

				rs = stmt.executeQuery(queryToExecute.toString());
				if (rs != null) {
					
					while (rs.next()) {
						SmartFindMasterView view = new SmartFindMasterView();
						view.setSfCount(rs.getString("sfcount"));
						view.setIndexStatus(rs.getString("indexstatus"));
						view.setCategory(rs.getString("category"));
						view.setFtype(rs.getString("ftype"));
						view.setDuplicate(rs.getString("isduplicate"));
						view.setOcrStatus(rs.getString("ocrstatus"));
						view.setPatternMatchingStatus(rs.getString("patternmatchingstatus"));
						view.setPngConverted(rs.getString("pngconverted"));
						view.setSpellCheckStatus(rs.getString("spellcheckstatus"));
						view.setSubCategory(rs.getString("subcategory"));
						view.setSystem(rs.getString("system"));
						view.setThreadProcessed(rs.getString("threadprocessed"));
						view.setTitleFound(rs.getString("titlefound"));
						view.setComponentsFound(rs.getString("componentsfound"));
						view.setDocNumFound(rs.getString("docnumfound"));
						view.setManufacturerFound(rs.getString("manufacturerfound"));
						view.setSetName(rs.getString("imageset3"));
						//view.setSolrWMPath(rs.getString("solr_wm_path"));
						//view.setSolrBigimgPath(rs.getString("solr_bigimg_path"));
						//view.setSolrSmallimgPath(rs.getString("solr_smallimg_path"));
						view.setJpgConverted(rs.getString("jpgconverted"));
						viewList.add(view);						
					}
				}	
				

			
		}catch(SQLException sql){
			Log.Error("SQLException while Fetching Master Details for SmartFindMaster Profile." + sql);
			throw new AppException(sql.getMessage(),"Error while processing the request.", 1 ,sql.getCause(), true);
		}catch(Exception e){
			Log.Error("Exception fetching the Details for SmartFindMaster Profile. \n " + e);
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
		}finally{
			if(con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
				
		}
		return viewList;

	}

	public ArrayList<SmartFindMasterView> getSmartFindL2Details(Object... obj) throws AppException {
		ArrayList<SmartFindMasterView> viewList = new ArrayList<SmartFindMasterView>();
		
		StringBuilder queryToExecute = null;
		ResultSet rs = null;
		Connection con = null;
		String system = null;
		String indexstatus = null;
		String category = null;
		String ftype = null;
		String isduplicate = null;
		String ocrstatus = null;
		String patternmatchingstatus = null;
		String pngconverted = null;
		String spellcheckstatus = null;
		String subcategory = null;
		String threadprocessed = null;
		String titlefound = null;
		String componentsfound = null;
		String docnumfound = null;
		String manufacturerfound = null;
		try {
				if (obj != null && obj.length >= 1) {
					system = (String) obj[0];
					indexstatus = (String) obj[1];
					category = (String) obj[2];
					ftype = (String) obj[3];
					isduplicate = (String) obj[4];
					ocrstatus = (String) obj[5];
					patternmatchingstatus = (String) obj[6];
					pngconverted = (String) obj[7];
					spellcheckstatus = (String) obj[8];
					subcategory = (String) obj[9];
					threadprocessed = (String) obj[10];
					titlefound = (String) obj[11];
					componentsfound = (String) obj[12];
					docnumfound = (String) obj[13];
					manufacturerfound = (String) obj[14];
					
					Log.Info("Fetching the L2 View for Smart Find... ");
					String tableName = "smartfindmaster1";
					StringBuilder where = null;
					queryToExecute = new StringBuilder();
					
					//key,imagekey,imagepath,moddate,oimagename,sourcepath
					//queryToExecute.append("select key,imagekey,imagepath,moddate,oimagename,sourcepath,indexstatus,category,ftype,isduplicate");
					//queryToExecute.append(",ocrstatus,patternmatchingstatus,pngconverted,spellcheckstatus");
					//queryToExecute.append(",subcategory,system,threadprocessed,titlefound,componentsfound,docnumfound,manufacturerfound");
					
					//queryToExecute.append(" from ");
					//queryToExecute.append(tableName);
					where = new StringBuilder(" where system = '").append(system).append("'");
					
					if (indexstatus == null || indexstatus.equalsIgnoreCase(null) || indexstatus.equalsIgnoreCase("NULL")) {
						where.append(" and (indexstatus ='' or indexstatus is null)");
					} else { 
						where.append(" and indexstatus = '").append(indexstatus).append("'");
					}					
					if (category == null || category.equalsIgnoreCase(null) || category.equalsIgnoreCase("NULL")) {
						where.append(" and (category ='' or category is null)");
					} else { 
						where.append(" and category = '").append(category).append("'");
					}
					if (ftype == null || ftype.equalsIgnoreCase(null) || ftype.equalsIgnoreCase("NULL")) {
						where.append(" and (ftype ='' or ftype is null)");
					} else { 
						where.append(" and ftype = '").append(ftype).append("'");
					}
					if (isduplicate == null || isduplicate.equalsIgnoreCase(null) || isduplicate.equalsIgnoreCase("NULL")) {
						where.append(" and (isduplicate ='' or isduplicate is null)");
					} else { 
						where.append(" and isduplicate = '").append(isduplicate).append("'");
					}
					if (ocrstatus == null || ocrstatus.equalsIgnoreCase(null) || ocrstatus.equalsIgnoreCase("NULL")) {
						where.append(" and (ocrstatus ='' or ocrstatus is null)");
					} else { 
						where.append(" and ocrstatus = '").append(ocrstatus).append("'");
					}
					if (patternmatchingstatus == null || patternmatchingstatus.equalsIgnoreCase(null) || patternmatchingstatus.equalsIgnoreCase("NULL")) {
						where.append(" and (patternmatchingstatus ='' or patternmatchingstatus is null)");
					} else { 
						where.append(" and patternmatchingstatus = '").append(patternmatchingstatus).append("'");
					}
					if (pngconverted == null || pngconverted.equalsIgnoreCase(null) || pngconverted.equalsIgnoreCase("NULL")) {
						where.append(" and (pngconverted ='' or pngconverted is null)");
					} else { 
						where.append(" and pngconverted = '").append(pngconverted).append("'");
					}
					if (spellcheckstatus == null || spellcheckstatus.equalsIgnoreCase(null) || spellcheckstatus.equalsIgnoreCase("NULL")) {
						where.append(" and (spellcheckstatus ='' or spellcheckstatus is null)");
					} else { 
						where.append(" and spellcheckstatus = '").append(spellcheckstatus).append("'");
					}
					if (subcategory == null || subcategory.equalsIgnoreCase(null) || subcategory.equalsIgnoreCase("NULL")) {
						where.append(" and (subcategory ='' or subcategory is null)");
					} else { 
						where.append(" and subcategory = '").append(subcategory).append("'");
					}
					if (threadprocessed == null || threadprocessed.equalsIgnoreCase(null) || threadprocessed.equalsIgnoreCase("NULL")) {
						where.append(" and (threadprocessed ='' or threadprocessed is null)");
					} else { 
						where.append(" and threadprocessed = '").append(threadprocessed).append("'");
					}
					if (titlefound == null || titlefound.equalsIgnoreCase(null) || titlefound.equalsIgnoreCase("NULL")) {
						where.append(" and (titlefound ='' or titlefound is null)");
					} else { 
						where.append(" and titlefound = '").append(titlefound).append("'");
					}
					if (componentsfound == null || componentsfound.equalsIgnoreCase(null) || componentsfound.equalsIgnoreCase("NULL")) {
						where.append(" and (componentsfound ='' or componentsfound is null)");
					} else { 
						where.append(" and componentsfound = '").append(componentsfound).append("'");
					}
					if (docnumfound == null || docnumfound.equalsIgnoreCase(null) || docnumfound.equalsIgnoreCase("NULL")) {
						where.append(" and (docnumfound ='' or docnumfound is null)");
					} else { 
						where.append(" and docnumfound = '").append(docnumfound).append("'");
					}
					if (manufacturerfound == null || manufacturerfound.equalsIgnoreCase(null) || manufacturerfound.equalsIgnoreCase("NULL")) {
						where.append(" and (manufacturerfound ='' or manufacturerfound is null)");
					} else { 
						where.append(" and manufacturerfound = '").append(manufacturerfound).append("'");
					}
					
					queryToExecute = new StringBuilder(smartFindL2Query).append(tableName).append(where.toString()).append(" limit 50");
					
					con = dbUtil.getHiveConnection(E2emfConstants.smartFindDB);
					Log.Info("Using Smart Find HIVE Connection...");
	
					Log.Info("SmartFind Query to Execute :"+queryToExecute.toString());
	
					Statement stmt = con.createStatement();
	
					rs = stmt.executeQuery(queryToExecute.toString());
					if (rs != null) {
						
						while (rs.next()) {
							SmartFindMasterView view = new SmartFindMasterView();
							view.setKey(rs.getString("key"));
							view.setImageKey(rs.getString("imagekey"));
							view.setImagePath(rs.getString("imagepath"));
							view.setModDate(rs.getString("moddate"));
							view.setOimageName(rs.getString("oimagename"));
							view.setSourcePath(rs.getString("sourcepath"));
							view.setIndexStatus(rs.getString("indexstatus"));
							view.setCategory(rs.getString("category"));
							view.setFtype(rs.getString("ftype"));
							view.setDuplicate(rs.getString("isduplicate"));
							view.setOcrStatus(rs.getString("ocrstatus"));
							view.setPatternMatchingStatus(rs.getString("patternmatchingstatus"));
							view.setPngConverted(rs.getString("pngconverted"));
							view.setSpellCheckStatus(rs.getString("spellcheckstatus"));
							view.setSubCategory(rs.getString("subcategory"));
							view.setSystem(rs.getString("system"));
							view.setThreadProcessed(rs.getString("threadprocessed"));
							view.setTitleFound(rs.getString("titlefound"));
							view.setComponentsFound(rs.getString("componentsfound"));
							view.setDocNumFound(rs.getString("docnumfound"));
							view.setManufacturerFound(rs.getString("manufacturerfound"));
							
							viewList.add(view);						
						}
					}	
				}
		}catch(SQLException sql){
			Log.Error("SQLException while Fetching L2 Details for SmartFind..." + sql);
			throw new AppException(sql.getMessage(),"Error while processing the request.", 1 ,sql.getCause(), true);
		}catch(Exception e){
			Log.Error("Exception fetching the L2 Details for SmartFind.... \n " + e);
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
		}finally{
			if(con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
				
		}
		return viewList;

	}
	public List<ClinicalSpeciality> getClinicalSpeciality(String rules) throws AppException{
		StringBuilder queryToExecute = null;
		List<ClinicalSpeciality> clinicalSpecialityList = new ArrayList<ClinicalSpeciality>();
		ClinicalSpeciality clinicalSpeciality = null;
		Connection connection = null;
		try {
			if(rules != null && rules.equalsIgnoreCase(E2emfConstants.ALL)){
				queryToExecute = new StringBuilder(clinicalSpecialityQuery);
			}
			else if(rules != null && rules.length() > 0){
				queryToExecute = new StringBuilder("SELECT * FROM cspeciality").append(" where rule in ('").append(rules).append("') order by priority desc");
			}
			connection = dbUtil.getPostGresConnection();
			Statement stmt1 = connection.createStatement();
			ResultSet rs = stmt1.executeQuery(queryToExecute.toString());

			if (rs != null) {
				while (rs.next()) {
					clinicalSpeciality = new ClinicalSpeciality();
					clinicalSpeciality.setCompany(rs.getString("company"));
					clinicalSpeciality.setClinicalSpeciality(rs.getString("clinic"));
					clinicalSpeciality.setId(rs.getString("id"));
					clinicalSpeciality.setIsEnabled(rs.getBoolean("enabled"));
					clinicalSpeciality.setIsOverride(rs.getBoolean("user_override"));
					clinicalSpeciality.setPriority(rs.getString("priority"));
					clinicalSpeciality.setRule(rs.getString("rule"));
					clinicalSpeciality.setCondition(rs.getString("condition"));
					clinicalSpecialityList.add(clinicalSpeciality);
				}
			}
			return clinicalSpecialityList;
		} catch (SQLException sql) {
			Log.Error("SQLException while retrieving the user list from DB"+ sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 1 ,sql.getCause(), true);
		} catch (Exception e) {
			Log.Error("Exception while retrieving user list from DB" + e);
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 2 ,e.getCause(), true);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
			}
		}
	}
	
	public String refreshNgram() throws AppException{
		Connection connection = null;
		InstrumentSearchService instrumentSearchService = new InstrumentSearchService();
		String result = null;
		QueryItem queryItem = null;
		NGramResult nGramResult = null;
		ArrayList<QueryItem> queryItemList =null;
		connection = dbUtil.getPostGresConnection();
		
		try {
			Statement stmt = connection.createStatement();
			Statement stmtCategory = connection.createStatement();
			Statement stmtSubcategory = connection.createStatement();
			Statement stmtCategoryDefault = connection.createStatement();
			Statement stmtSubcategoryDefault = connection.createStatement();
			Statement stmtUpdateSpecialChar= connection.createStatement();
			stmt.executeUpdate("delete from sf_ngram");
			for (int i=1;i<5;i++){
				QueryParam queryParam = new QueryParam();
				queryParam.setStart("0");
				queryParam.setRows("500000");
				queryParam.setOutputField("original");
				result = instrumentSearchService.getNGramAnalsyer(i, queryParam, false);			
				nGramResult = new NGramResult();
				nGramResult.setnGram(i);
				nGramResult.setResult(result);
				insertNgram(nGramResult);
				
				ResultSet rsCategoryDefault = stmtCategoryDefault.executeQuery("select category from sf_categories where parent is null or parent = ''");
				if (rsCategoryDefault != null) {
					while (rsCategoryDefault.next()) {
						ArrayList<QueryItem> queryItemListCategoryDefault = new ArrayList<QueryItem>();
						QueryItem queryItemCategoryDefault = new QueryItem();
						queryItemCategoryDefault.setField("categoryl0");
						queryItemCategoryDefault.setKey( rsCategoryDefault.getString("category"));
						queryItemCategoryDefault.setType("exact");
						queryItemListCategoryDefault.add(queryItemCategoryDefault);
						QueryParam queryParamSourceCategoryDefault = new QueryParam();
						queryParamSourceCategoryDefault.setStart("0");
						queryParamSourceCategoryDefault.setRows("500000");
						queryParamSourceCategoryDefault.setOutputField("original");
						queryParamSourceCategoryDefault.setQueryItems(queryItemListCategoryDefault);
						
						result = instrumentSearchService.getNGramAnalsyer(i, queryParamSourceCategoryDefault, false);			
						nGramResult = new NGramResult();
						nGramResult.setnGram(i);
						nGramResult.setCategory(rsCategoryDefault.getString("category"));
						nGramResult.setResult(result);
						insertNgram(nGramResult);
						
						ResultSet rsSubcategoryDefault = stmtSubcategoryDefault.executeQuery("select category from sf_categories where parent = '"+ rsCategoryDefault.getString("category") +"'");
						if (rsSubcategoryDefault != null) {
							while (rsSubcategoryDefault.next()) {
								ArrayList<QueryItem> queryItemListSubcategoryDefault = new ArrayList<QueryItem>();
								queryItemListSubcategoryDefault.add(queryItemCategoryDefault);
								QueryItem queryItemSubcategoryDefault = new QueryItem();
								queryItemSubcategoryDefault.setField("categoryl0");
								queryItemSubcategoryDefault.setKey(rsSubcategoryDefault.getString("category"));
								queryItemSubcategoryDefault.setType("exact");
								queryItemListSubcategoryDefault.add(queryItemSubcategoryDefault);
								QueryParam queryParamSourceCategorySubcategoryDefault = new QueryParam();
								queryParamSourceCategorySubcategoryDefault.setStart("0");
								queryParamSourceCategorySubcategoryDefault.setRows("500000");
								queryParamSourceCategorySubcategoryDefault.setOutputField("original");
								queryParamSourceCategorySubcategoryDefault.setQueryItems(queryItemListSubcategoryDefault);
								
								result = instrumentSearchService.getNGramAnalsyer(i, queryParamSourceCategorySubcategoryDefault, false);			
								nGramResult = new NGramResult();
								nGramResult.setnGram(i);
								nGramResult.setCategory(rsCategoryDefault.getString("category"));
								nGramResult.setSubCategory(rsSubcategoryDefault.getString("category"));
								nGramResult.setResult(result);
								insertNgram(nGramResult);
							}
						}
					}
				}
				
				ResultSet rs = stmt.executeQuery("select distinct(source) from solrmaster");
				if (rs != null) {
					while (rs.next()) {
						queryItemList = new ArrayList<QueryItem>();
						queryItem = new QueryItem();
						queryItem.setField("source");
						queryItem.setKey( rs.getString("source"));
						queryItem.setType("exact");
						queryItemList.add(queryItem);
						
						QueryParam queryParamSource = new QueryParam();
						queryParamSource.setStart("0");
						queryParamSource.setRows("500000");
						queryParamSource.setOutputField("original");
						queryParamSource.setQueryItems(queryItemList);
						
						result = instrumentSearchService.getNGramAnalsyer(i, queryParamSource, false);		
						nGramResult = new NGramResult();
						nGramResult.setnGram(i);
						nGramResult.setSource(rs.getString("source"));
						nGramResult.setResult(result);
						insertNgram(nGramResult);
						ResultSet rsCategory = stmtCategory.executeQuery("select category from sf_categories where parent is null or parent = ''");
						if (rsCategory != null) {
							while (rsCategory.next()) {
								ArrayList<QueryItem> queryItemListCategory = new ArrayList<QueryItem>();
								queryItemListCategory.add(queryItem);
								QueryItem queryItemCategory = new QueryItem();
								queryItemCategory.setField("categoryl0");
								queryItemCategory.setKey( rsCategory.getString("category"));
								queryItemCategory.setType("exact");
								queryItemListCategory.add(queryItemCategory);
								QueryParam queryParamSourceCategory = new QueryParam();
								queryParamSourceCategory.setStart("0");
								queryParamSourceCategory.setRows("500000");
								queryParamSourceCategory.setOutputField("original");
								queryParamSourceCategory.setQueryItems(queryItemListCategory);
								
								result = instrumentSearchService.getNGramAnalsyer(i, queryParamSourceCategory, false);			
								nGramResult = new NGramResult();
								nGramResult.setnGram(i);
								nGramResult.setSource(rs.getString("source"));
								nGramResult.setCategory(rsCategory.getString("category"));
								nGramResult.setResult(result);
								insertNgram(nGramResult);
								
								ResultSet rsSubcategory = stmtSubcategory.executeQuery("select category from sf_categories where parent = '"+ rsCategory.getString("category") +"'");
								if (rsSubcategory != null) {
									while (rsSubcategory.next()) {
										ArrayList<QueryItem> queryItemListSubcategory = new ArrayList<QueryItem>();
										queryItemListSubcategory.add(queryItem);
										queryItemListSubcategory.add(queryItemCategory);
										QueryItem queryItemSubcategory = new QueryItem();
										queryItemSubcategory.setField("categoryl0");
										queryItemSubcategory.setKey( rsSubcategory.getString("category"));
										queryItemSubcategory.setType("exact");
										queryItemListSubcategory.add(queryItemSubcategory);
										QueryParam queryParamSourceCategorySubcategory = new QueryParam();
										queryParamSourceCategorySubcategory.setStart("0");
										queryParamSourceCategorySubcategory.setRows("500000");
										queryParamSourceCategorySubcategory.setOutputField("original");
										queryParamSourceCategorySubcategory.setQueryItems(queryItemListSubcategory);
										
										result = instrumentSearchService.getNGramAnalsyer(i, queryParamSourceCategorySubcategory, false);			
										nGramResult = new NGramResult();
										nGramResult.setnGram(i);
										nGramResult.setSource(rs.getString("source"));
										nGramResult.setCategory(rsCategory.getString("category"));
										nGramResult.setSubCategory(rsSubcategory.getString("category"));
										nGramResult.setResult(result);
										insertNgram(nGramResult);
									}
								}
							}
						}
					}
				}
				//stmtUpdateSpecialChar.executeUpdate("update sf_ngram set result =  replace(result,'\\','') where result like '%\\\\%'");
			}
			String updateSpecialChar = "update sf_ngram set result =  replace(result,E'\\','') where result like '%\\\\%'";
			stmtUpdateSpecialChar.executeUpdate(updateSpecialChar);
		} catch (Exception e) {
			Log.Error("Exception while retrieving user list from DB" + e);
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 2 ,e.getCause(), true);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
			}
		}
		return null;
	}
	
	private String insertNgram(NGramResult nGram) throws AppException{
		Connection connection = null;
		if (nGram != null) {
			Log.Info("Inserting  Values for User Object : " + nGram);
			try {
				String insertQuery = "INSERT INTO sf_ngram(source, category, subcategory, ngram, company, clinicalspeciality, result) values('"
						+ nGram.getSource()
						+ "','"
						+ nGram.getCategory()
						+ "','"
						+ nGram.getSubCategory()
						+ "','"
						+ nGram.getnGram()
						+ "','"
						+ nGram.getCompany()
						+ "','"
						+ nGram.getClinicalSpeciality()
						+ "','"
						+ nGram.getResult()
						+ "')";
				connection = dbUtil.getPostGresConnection();
				Statement stmt = connection.createStatement();
				stmt.executeUpdate(insertQuery);
				return "{\"message\":\"successfully inserted sf_ngram in table\"}";
			} catch (SQLException sql) {
				Log.Error("SQLException while inserting ngram details in table." + sql);
				String message = sql.getMessage();
				String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
				throw new AppException(newmessage,"Error while processing the request.", 1 ,sql.getCause(), true);
			} catch (Exception e) {
				Log.Error("Exception while inserting ngram details in table." + e);
				if(e instanceof NullPointerException){
					throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
				}
				String message = e.getMessage();
				String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
				throw new AppException(newmessage,"Error while processing the request.", 2 ,e.getCause(), true);
			} finally {
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e) {
						Log.Error(e);
						throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
					}
				}
			}
		}//end of if
		else
		{
			Log.Error("Input obj is null");
			return "{\"message\":\"Failed to  inserted ngram in table\"}";
		}
	}
	public String getNGramAnalsyerStoredResults(int gram, String source,
			String category, String subcategory, int range, boolean isDictionaryWords)  throws AppException{
		StringBuilder queryToExecute = null;
		StringBuilder where = null;
		String results = null;
		Connection connection = null;
		try {
			where = new StringBuilder(" where ");
			if (gram > 0) {
				where.append(" ngram = ").append(gram);
			}
			if (source != null && !source.equalsIgnoreCase(null)
					&& !source.equalsIgnoreCase("NULL")) {
				where.append(" and source = '").append(source)
						.append("'");
			}else {
				where.append(" and source = 'null'");
			}
			if (category != null && !category.equalsIgnoreCase(null)
					&& !category.equalsIgnoreCase("NULL")) {
				where.append(" and category = '").append(category)
						.append("'");
			}else{
				where.append(" and category = 'null'");
			}
			if (subcategory!=null && !subcategory.equalsIgnoreCase(null)
					&& !subcategory.equalsIgnoreCase("NULL")) {
				where.append(" and subcategory = '").append(subcategory)
						.append("'");
			}else{
				where.append(" and subcategory = 'null'");
			}
			queryToExecute = new StringBuilder("select result from sf_ngram")
					.append(where);
			connection = dbUtil.getPostGresConnection();
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(queryToExecute.toString());

			if (rs != null) {
				while (rs.next()) {
					results = rs.getString("result");
				}
			}
			Gson gson = new Gson();
			NGramAnalyserStored nGramAnalyerStored = gson.fromJson(results, new TypeToken<NGramAnalyserStored>() {}.getType());
			List<NGramEntry> nGramList = nGramAnalyerStored.getnGramAnalyser();
			List<NGramEntry> resultNGramList = new ArrayList<>();
			NGramAnalyserStored resultNGramAnalyerStored = new NGramAnalyserStored();
			resultNGramList = SmartFindSpellChecker.checkSpelling(nGramList, isDictionaryWords, range);
			resultNGramAnalyerStored.setnGramAnalyser(resultNGramList);
			String resultJson = gson.toJson(resultNGramAnalyerStored);
			return resultJson;
		} catch (SQLException sql) {
			Log.Error("SQLException while retrieving the user list from DB"+ sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 1 ,sql.getCause(), true);
		} catch (Exception e) {
			Log.Error("Exception while retrieving user list from DB" + e);
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 2 ,e.getCause(), true);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
			}
		}
	}
	
	//Clinical Speciality - Insert, Update, Delete
	
	public String insertClinicalSpeciality(Object... obj) throws AppException {
		ClinicalSpeciality param = null;		
		Connection connection = null;
		if (obj != null) {
			param = (ClinicalSpeciality) obj[0];
			Log.Info("Inserting  Values for Clinical Speciality Object : " + param);
		try {
			connection = getPostGresDBConnection();
			Statement stmt = connection.createStatement();
				
			String insertClinicalSpecialityQuery = "insert into cspeciality (company, clinic) values("
					+ " '"
					+ param.getCompany()
					+ "','"
					+ param.getClinicalSpeciality()
					+"')";
			stmt.executeUpdate(insertClinicalSpecialityQuery);
			return "{\"message\":\"successfully inserted ClinicalSpeciality in DB.\"}";
		} catch (SQLException sql) {
			Log.Error("SQLException while inserting Clinical Speciality in table." + sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 1 ,sql.getCause(), true);
		} catch (Exception e) {
			Log.Error("Exception while inserting Clinical Speciality in table." + e);
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}			
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 2 ,e.getCause(), true);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					Log.Error(e);
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
			}
		}
		
		}//end of if 
		else
		{
			Log.Error("Input object is Null");
			return "{\"message\":\"Failed to insert Clinical Speciality in DB.\"}";
		}
	}

	public String updateClinicalSpeciality(Object... obj) throws AppException {
		ClinicalSpeciality param = null;		
		Connection connection = null;
				
		if (obj != null) {
			param = (ClinicalSpeciality) obj[0];
			Log.Info("Updating  Values for Clinical Speciality Object : " + param);
		try {
			connection = getPostGresDBConnection();
			Statement stmt = connection.createStatement();
			String updateQueryForCompany = "update cspeciality set company = '" + param.getCompany() + "' where id = '" + param.getId() + "'";
			String updateQueryForClinic = "update cspeciality set clinic = '" + param.getClinicalSpeciality() + "' where id = '" + param.getId() + "'";
	     	stmt.executeUpdate(updateQueryForCompany);			
			stmt.executeUpdate(updateQueryForClinic);
		  	return "{\"message\":\"successfully updated Clinical Speciality in DB.\"}";
		} catch (SQLException sql) {
			Log.Error("SQLException while Updating Clinical Speciality in table." + sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 1 ,sql.getCause(), true);
		} catch (Exception e) {
			Log.Error("Exception while Updating Clinical Speciality details in table." + e);
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}			
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 2 ,e.getCause(), true);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
			}
		}
		
		}//end of if 
		else
		{
			Log.Error("Input object is Null");
			return "{\"message\":\"Failed to Update Clinical Speciality in DB.\"}";
		}
	}
	
	public String deleteClinicalSpeciality(Object... obj) throws AppException {
		ClinicalSpeciality param = null;		
		Connection connection = null;
				
		if (obj != null) {
			param = (ClinicalSpeciality) obj[0];
			Log.Info("Deleting  Values for Clinical Speciality Object : " + param);
		try {
			connection = getPostGresDBConnection();
			Statement stmt = connection.createStatement();
			
			String deleteQueryForClinicalSpeciality = "delete from cspeciality where company = '" + param.getCompany() + "'";
			
			stmt.executeUpdate(deleteQueryForClinicalSpeciality);
			return "{\"message\":\"successfully deleted Clinical Speciality in DB.\"}";
		} catch (SQLException sql) {
			Log.Error("SQLException while deleting Clinical Speciality in table." + sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 1 ,sql.getCause(), true);
		} catch (Exception e) {
			Log.Error("Exception while deleting Clinical Speciality in table." + e);
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}			
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 2 ,e.getCause(), true);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
			}
		}
		
		}//end of if 
		else
		{
			Log.Error("Input object is Null");
			return "{\"message\":\"Failed to delete Clinical Speciality in DB.\"}";
		}
	}
	
	public String updateObsoleteStatusInPostGresTable(String id, String status) throws NullPointerException, AppException{
		
		Connection connection = null;
		Statement stmt = null;
		String updateQuery = null;
		try{
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			if(status.equalsIgnoreCase("true")){
				updateQuery = "update solrmaster set deleted = 'true' where key like '%" + id + "%'";
			}else{
				updateQuery = "update solrmaster set deleted = 'false' where key like '%" + id + "%'";
			}			
			Log.Info("Postgres Update Querty : " + updateQuery);
			stmt.executeUpdate(updateQuery);
			return "{\"message\":\"successfully updated delete status in postgres table\"}";
		}catch(AppException appe){
			Log.Error("AppException while updating delete status in postgres table." + appe);
		}catch (SQLException sql) {
			Log.Error("SQLException while updating delete status in postgres table." + sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 1 ,sql.getCause(), true);
		} catch (Exception e) {
			Log.Error("Exception while updating solr master table." + e);
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 2 ,e.getCause(), true);
		} finally {
			if (connection != null) {
				try {
					if(stmt != null){
						stmt.close();
					}	
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
			}
		}
		return "{\"message\":\"successfully updated delete status in postgres table\"}";
	}
	
	public ArrayList<SmartFindDocument> getImageSetList() throws AppException{
		StringBuilder queryToExecute = null;
		ArrayList<SmartFindDocument> smartFindDocumentList = new ArrayList<SmartFindDocument>();
		Connection connection = null;
		try {
		
			queryToExecute = new StringBuilder(imageSetsQuery);
			connection = dbUtil.getPostGresConnection();
			Statement stmt1 = connection.createStatement();
			ResultSet rs = stmt1.executeQuery(queryToExecute.toString());

			if (rs != null) {
				while (rs.next()) {
					SmartFindDocument smartFindDocument = new SmartFindDocument();
					smartFindDocument.setImageset(rs.getString("imageset"));
					smartFindDocumentList.add(smartFindDocument);
				}
			}
			return smartFindDocumentList;
		} catch (SQLException sql) {
			Log.Error("SQLException while retrieving the imageset list from DB"+ sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 1 ,sql.getCause(), true);
		} catch (Exception e) {
			Log.Error("Exception while retrieving imageset list from DB" + e);
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 2 ,e.getCause(), true);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
			}
		}
	}
	
	public Connection getPostGresDBConnection() throws AppException, SQLException {
		PrismDbUtil dbUtil = new PrismDbUtil();
		Connection connection = dbUtil.getPostGresConnection();
		return connection;
	}
}
