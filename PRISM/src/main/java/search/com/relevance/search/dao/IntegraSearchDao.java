package com.relevance.search.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.relevance.prism.dao.BaseDao;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.PrismHandler;
import com.relevance.search.data.IntegraAgilePart;

public class IntegraSearchDao extends BaseDao {
	/*public List<String> getBOM(String source) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<String> databaseList = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbUtil.getPostGresConnection();
			query.append("SELECT name FROM app_properties");
		
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					databaseList.add(rs.getString("name"));
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return databaseList;
	}*/
	
	public List<String> getBOMIds() throws Exception  {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<String> infoList = new ArrayList<>();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		String query = null;
		try {
			con = dbUtil.getPostGresConnection(E2emfConstants.qfind_db);
			query = "SELECT distinct materialnumber from integra_bom_oracle;";
			if (con != null) {
			 stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
			 rs = stmt.executeQuery(query.toString());
			 if(rs.next()) {
				 do{
					 infoList.add(rs.getString("materialnumber"));
				 }while(rs.next());
			 }
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return infoList;
	}
	
	public List<IntegraAgilePart> getBOM(String skuNumber,String field,String Type, String tableName, String source, String dataBase) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		IntegraAgilePart skuInfo  = null;
		List<IntegraAgilePart> skuInfoList = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		
		try {
			if(source != null && source.equalsIgnoreCase("hive")) {
				if(dataBase != null && !dataBase.isEmpty()) {
					con = dbUtil.getHiveConnection(dataBase);
				} else {
					con = dbUtil.getHiveConnection(E2emfConstants.qfind_db);
				}
			} else {
				if(dataBase != null && !dataBase.isEmpty()) {
					con = dbUtil.getPostGresConnection(dataBase);
				} else {
					con = dbUtil.getPostGresConnection(E2emfConstants.qfind_db);
				}
			}
			
			//query.append("SELECT level, parent, materialnumber, description, rowid, parttype, rev, revreleasedate, functionalteam, bomincorpdate FROM integra_bom");
			if(tableName == null || tableName.isEmpty()) {
				tableName = "integra_bom_oracle";
			}
			query.append("SELECT parent, materialnumber, description FROM " + tableName);
			if (skuNumber != null && !skuNumber.isEmpty()){
				query.append(" WHERE ").append(field).append(" = '").append(skuNumber).append("'");
			}else{
				query.append(" WHERE parent is NULL");
			}
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					String parent  = rs.getString("parent");
					String materialNumber = rs.getString("materialnumber");
					skuInfo = new IntegraAgilePart();
//					skuInfo.setLevel(rs.getInt("level"));
					if ("forward".equalsIgnoreCase(Type))
						skuInfo.setChildren(getBOM(materialNumber,"parent",Type, tableName, source, dataBase));
					else if (parent != null)
						skuInfo.setChildren(getBOM(parent,"materialnumber",Type, tableName, source, dataBase));
					skuInfo.setParent(parent);
					skuInfo.setPartNumber(materialNumber);
					skuInfo.setKey(materialNumber);
					skuInfo.setDescription(rs.getString("description"));
//					skuInfo.setRowId(rs.getInt("rowid"));
//					skuInfo.setPartType(rs.getString("parttype"));
//					skuInfo.setRev(rs.getString("rev"));
//					skuInfo.setRevReleaseDate(rs.getString("revreleasedate"));
//					skuInfo.setFunctionalTerm(rs.getString("functionalteam"));
//					skuInfo.setBomIncorpDate(rs.getString("bomincorpdate"));
					skuInfoList.add(skuInfo);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return skuInfoList;
	}
	
	public boolean updateFileStructure(String tableName, String dbName, String columnName, String primaryKeys, String value, String primaryKeyColumnName, String keyValueMap) throws Exception  {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		String[] arr;
		int count = 0;
		boolean flag = false;
		try{
			con = dbUtil.getPostGresConnection(dbName);
			 
			 arr = primaryKeys.split(",");
			 query.append("update "+tableName+" set " + columnName + " = '" + value + "' where ");
			  
			 for (String pkey : arr) {
				 if(count == arr.length - 1) {
					 query.append("" + primaryKeyColumnName + " = '"+ pkey +"' ");  
				 } else {
		    	     query.append("" + primaryKeyColumnName + " = '"+ pkey +"' or ");
				 }			 
			 }			
			 stmt = con.createStatement();
			 count = stmt.executeUpdate(query.toString());
			 flag = true;
		}  catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return flag;
	}	
	
	public boolean genericUpdate(String query,String source,String dataBase) throws Exception  {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		boolean flag = false;
		try{
			if("postgres".equalsIgnoreCase(source)) {
				con = dbUtil.getHiveConnection(dataBase);
			} else {
				con = dbUtil.getPostGresConnection(dataBase);
			}
			
			 stmt = con.createStatement();
			 stmt.executeUpdate(query);
			 flag = true;
		}  catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return flag;
	}	
	
	public List<String> getIntegraCategories(String source,String dbName, String tableName, String columnName, String regEx, boolean _union) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		ResultSetMetaData rsmd = null;
		Connection con = null;
		Statement stmt = null;
		List<String> arr = null;
		boolean isMultipleFields = false;
		try{
			if(source != null && source.equalsIgnoreCase("hive")) {
				con = dbUtil.getHiveConnection(dbName);
			} else {
				con = dbUtil.getPostGresConnection(dbName);
			}

			if(_union){
				query.append("select distinct "+columnName.split(",")[0]+" from (select distinct "+columnName.split(",")[0]+"  from "+tableName+" union "
						+ "select distinct "+columnName.split(",")[1]+" from "+tableName+")a where "+columnName.split(",")[0]+" is not null ");
			} else {
				query.append("select ");
				if(regEx != null){
					if(regEx.equals("distinct")){
						query.append("distinct("+columnName+")");
					}
				} else {
					query.append(columnName);
				}
				query.append(" from ");
				query.append(tableName);
			}
			
			stmt = con.createStatement();
			rs = stmt.executeQuery(query.toString());
			rsmd = rs.getMetaData();
			
			//Get Resultset metadata if multiple fields involved.
			if(columnName.split(",").length > 1) {
				isMultipleFields = true;
			}
			if(rs.next()) {
				arr = new ArrayList<>();
				do{
					if(isMultipleFields) {
						Map<String, String> columnValuePair= new HashMap<>();
						for(int c = 1; c <= rsmd.getColumnCount() ; c++) {
							columnValuePair.put(rsmd.getColumnName(c), rs.getString(rsmd.getColumnName(c)));
						}
						Gson gson = new Gson();
						String jsonStr = gson.toJson(columnValuePair);
						arr.add(jsonStr);
					} else {
						arr.add(rs.getString(columnName));
					}
				} while(rs.next());
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return arr;
	}
	
	public HashMap<String, String> getIntegraBrands(String dbName, String tableName) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		HashMap<String, String> result = null;
		try{
			con = dbUtil.getPostGresConnection(dbName);
			query.append("select ");
			query.append("brandname, solrqueryparam");
			query.append(" from ");
			query.append(tableName);
			query.append("where visible = true order by rowid");
			stmt = con.createStatement();
			rs = stmt.executeQuery(query.toString());
			if(rs.next()) {
				result = new HashMap<>();
				do{
					result.put(rs.getString("brandname"), rs.getString("solrqueryparam"));
				} while(rs.next());
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return result;
	}
	public HashMap<String, List<String>> getMasterDataDetails(String key, String dbName) throws Exception {
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		HashMap<String, List<String>> result = null;
		List<String> resultLine = null;
		ResultSetMetaData rsmd;
		StringBuilder baseQuery = new StringBuilder(); 
		int count = 0;
		HashMap<String, String> queryNameMap = null;
		Iterator it;
		try{
			queryNameMap = new HashMap<>();
			queryNameMap.put("FDA", "select  '',mpid,med_product,' ',' ',' ',  med_product_name , name,  invented_name_part ,  scientific_name_part ,  strength_part ,' ',  formulation_part ,  intended_use_part ,  company_name ,  ingredient ,' ',' ',' ',' ',' ',' ',  market_auth ,  market_auth_num,  country ,  legal_status_supply ,  auth_status ,  auth_status_date ,  valid_period ,  market_auth_holder ,  med_regulatory_agency ,  app_id_num ,  app_type ,  market_status ,  pharmaceutical_product ,' ',  unit_presentation ,' ',' ','' from idmp_ndc ");
			queryNameMap.put("EMA","select '',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',  market_auth ,  market_auth_num,  country ,  legal_status_supply ,  auth_status ,  auth_status_date ,  valid_period ,  market_auth_holder ,  med_regulatory_agency ,  app_id_num ,  app_type ,  market_status ,' ',' ',' ',' ',' ',' ' from idmp_emea ");
			queryNameMap.put("DrugBank", "select '',' ', '', med_product_classification ,  classification_sys ,  classification_value ,' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',  ' ' ,' ',  specified_subs ,  specified_subs_group ,' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ' from idmp_drugbank ");
			queryNameMap.put("DailyMed", "select '',' ',' ',' ',' ',' ',' ',' ',' ',' ','',  pharmaceutical_dose_form ,' ',' ',' ',' ',  ingredient_role ,  substance ,  specified_subs ,  ingredient_role ,' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',administrable_dose_form,  unit_presentation ,  pharmaceutical_product_qty ,  route_admin ,' ' from idmp_dailymed ");
			queryNameMap.put("PrescribingInfo", "select '',' ','',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',  unit_presentation ,  pharmaceutical_product_qty ,  route_admin ,  contraindications  from idmp_alexion ");
			it = queryNameMap.entrySet().iterator();
			result = new HashMap<>();
			while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        con = dbUtil.getPostGresConnection(dbName);
				query = new StringBuilder();
				baseQuery = new StringBuilder();
				baseQuery.append(pair.getValue());								
				query.append(baseQuery.toString());
				query.append("limit 1;");
				
				stmt = con.createStatement();
				rs = stmt.executeQuery(query.toString());
				rsmd = rs.getMetaData();
				count = rsmd.getColumnCount();
				rs.close();
				stmt.close();
				con.close();
				
				con = dbUtil.getPostGresConnection(dbName);
				query = new StringBuilder();
					
				query.append(baseQuery.toString());
				query.append(" where id = "+key+" limit 1;");
				stmt = con.createStatement();
				rs = stmt.executeQuery(query.toString());
				
				
				if(rs.next()) {
					resultLine = new ArrayList<>();
					for(int i = 1; i <= count; i++) {
						if(i == 1) {
							resultLine.add(pair.getKey().toString());
						}
						else {
							if(rs.getString(i) == null) {
								resultLine.add("");
							} else {
								resultLine.add(rs.getString(i));
							}
							
						}					
					}	
					result.put(pair.getKey().toString(), resultLine);
				} else {
					resultLine = new ArrayList<>();
					for(int i = 1; i <= count; i++) {
						if(i == 1) {
							resultLine.add(pair.getKey().toString());
							continue;
						}
						resultLine.add("");
					}
					//resultLine.add("");
					//resultLine.add("");
					//resultLine.add("");
					result.put(pair.getKey().toString(), resultLine);
				}
				rs.close();
				stmt.close();
				con.close();
		    }
			
		}  catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return result;
	}
	
	public HashMap<String, List<String>> getMasterData(String key, String dbName) throws Exception {
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		HashMap<String, List<String>> result = null;
		List<String> resultLine = null;
		ResultSetMetaData rsmd;
		try{
			key = key.replaceAll("[^a-zA-Z0-9]", "");
			con = dbUtil.getPostGresConnection(dbName);
			query.append("select country_of_origin, title, item_number from mdm_label where trim(regexp_replace(item_number,'\\W+', '', 'g')) = '"+key+"' limit 1;");			
			stmt = con.createStatement();
			rs = stmt.executeQuery(query.toString());
			result = new HashMap<>();
			resultLine = new ArrayList<>();
			if(rs.next()) {				
				do{
					resultLine.add("Label");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add(rs.getString("title") == null ? "" : rs.getString("title") );
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add(rs.getString("country_of_origin"));
					//resultLine.add("");
				} while(rs.next());				
			} else {
				resultLine.add("Label");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
			}
			result.put("Label", resultLine);
			rs.close();
			stmt.close();
			con.close();
			
			//Catalog
			con = dbUtil.getPostGresConnection(dbName);
			query = new StringBuilder();
			query.append("select product_brand, category, franchise, category, type, title from mdm_catalog where trim(regexp_replace(item_number,'\\W+', '', 'g')) = '"+key+"' limit 1;");			
			stmt = con.createStatement();
			rs = stmt.executeQuery(query.toString());
			resultLine = new ArrayList<>();
			if(rs.next()) {
				do{
					resultLine.add("CMS");
					resultLine.add((rs.getString("product_brand") == null) ? "" :   rs.getString("product_brand"));
					resultLine.add("");
					resultLine.add(rs.getString("category")== null ? "" : rs.getString("category") );
					resultLine.add(rs.getString("title") == null ? "" : rs.getString("title"));
					resultLine.add("");
					resultLine.add("");
					resultLine.add(rs.getString("franchise") == null ? "" : rs.getString("franchise"));
					resultLine.add(rs.getString("category")== null ? "" : rs.getString("category") );
					resultLine.add(rs.getString("type") == null ? "" : rs.getString("type") );
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");
					resultLine.add("");		
				} while(rs.next());				
			} else {
				resultLine.add("CMS");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
				resultLine.add("");
			}
			result.put("Catalog", resultLine);
			rs.close();
			stmt.close();
			con.close();
						
			//spreadsheet
			con = dbUtil.getPostGresConnection(dbName);
			query = new StringBuilder();
			StringBuilder baseQuery = new StringBuilder();
			baseQuery.append("select catalog, brand,'', specialty_use, instrument_description, '','' , physician_developer_name,");
			baseQuery.append("'', '', instr_type,'','', '', shape, blade, grade, finish, composition, usage,'', '' , '', '', overall_length, working_length,");
			baseQuery.append("tip_width_1, tip_length_1, tip_width_2, tip_length_2, '', angle, spread_open, bite, ga_fr_size, size_style,");
			baseQuery.append("jaws_blades, '', '', '', '' from spreadsheet_datasource ");
			
			query.append(baseQuery.toString());
			query.append("limit 1;");
			
			stmt = con.createStatement();
			rs = stmt.executeQuery(query.toString());
			rsmd = rs.getMetaData();
			int count = rsmd.getColumnCount();
			rs.close();
			stmt.close();
			con.close();
			
			con = dbUtil.getPostGresConnection(dbName);
			query = new StringBuilder();
				
			query.append(baseQuery.toString());
			query.append(" where  trim(regexp_replace(catalog,'\\W+', '', 'g')) = '"+key+"' limit 1;");
			stmt = con.createStatement();
			rs = stmt.executeQuery(query.toString());
			
			
			if(rs.next()) {
				resultLine = new ArrayList<>();
				for(int i = 1; i <= count; i++) {
					if(i == 1) {
						resultLine.add("Spread Sheet");
					}
//					else if(i == 2) {
//						resultLine.add("");
//					}
					else {
						if(rs.getString(i) == null) {
							resultLine.add("");
						} else {
							resultLine.add(rs.getString(i));
						}
						
					}					
				}	
//				resultLine.add("");
//				resultLine.add("");
//				resultLine.add("");
				//resultLine.add("");
				result.put("SpreadSheet", resultLine);
			} else {
				resultLine = new ArrayList<>();
				for(int i = 1; i <= count; i++) {
					if(i == 1) {
						resultLine.add("Spread Sheet");
						continue;
					}
					resultLine.add("");
				}
				//resultLine.add("");
				//resultLine.add("");
				//resultLine.add("");
				result.put("SpreadSheet", resultLine);
			}
			rs.close();
			stmt.close();
			con.close();
			
			//Item Master
			con = dbUtil.getPostGresConnection(dbName);
			query = new StringBuilder();
			baseQuery = new StringBuilder();
			baseQuery.append("select item_number, '', '', '', title, '', '',");
			baseQuery.append("'', '', item_type,'','', '', '', '', '', '', '', '', '' , '', '', '', '',");
			baseQuery.append("'', '', '', '', '', '', '', '', '', '',");
			baseQuery.append("'', '', '', '', '', '' from mdm_item_master ");
			
			query.append(baseQuery.toString());
			query.append("limit 1;");
			
			stmt = con.createStatement();
			rs = stmt.executeQuery(query.toString());
			rsmd = rs.getMetaData();
			count = rsmd.getColumnCount();
			rs.close();
			stmt.close();
			con.close();
			
			con = dbUtil.getPostGresConnection(dbName);
			query = new StringBuilder();
				
			query.append(baseQuery.toString());
			query.append(" where trim(regexp_replace(item_number,'\\W+', '', 'g')) = '"+key+"' limit 1;");
			stmt = con.createStatement();
			rs = stmt.executeQuery(query.toString());
			
			
			if(rs.next()) {
				resultLine = new ArrayList<>();
				for(int i = 1; i <= count; i++) {
					if(i == 1) {
						resultLine.add("Item Master");
					}
//					else if(i == 2) {
//						resultLine.add("");
//					}
					else {
						if(rs.getString(i) == null) {
							resultLine.add("");
						} else {
							resultLine.add(rs.getString(i));
						}
						
					}					
				}	
//				resultLine.add("");
//				resultLine.add("");
//				resultLine.add("");
				//resultLine.add("");
				result.put("ItemMaster", resultLine);
			} else {
				resultLine = new ArrayList<>();
				for(int i = 1; i <= count; i++) {
					if(i == 1) {
						resultLine.add("Item Master");
						continue;
					}
					resultLine.add("");
				}
				//resultLine.add("");
				//resultLine.add("");
				//resultLine.add("");
				result.put("ItemMaster", resultLine);
			}
			rs.close();
			stmt.close();
			con.close();
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return result;
	}
}
