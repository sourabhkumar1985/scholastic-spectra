package com.relevance.search.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.relevance.prism.util.AppException;
import com.relevance.prism.util.PrismDbUtil;
import com.relevance.prism.util.Log;
import com.relevance.search.data.SmartFindAuditDocument;
import com.relevance.search.data.SmartFindDocument;

public class SmartFindAuditDao {

	public SmartFindDocument getSmartFindDocument(String solrid) throws AppException, SQLException{
		Connection connection = null;
		connection = getPostGresDBConnection();
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		String smartFindDocumentSelectQuery = null;
		SmartFindDocument smartFindDocument = null;
		try {
			smartFindDocumentSelectQuery = "select key, components, docnum, manufacturer, relateditems, source, title, userupdated, "
					+ "smallimagepath, imagepath, filepath, cspeciality, categoryl0, lables, rank, date_1, deleted, partnumber, "
					+ "finished_goods_number, component_number, notes, imageset, clicked, downloaded, emailed, "
					+ "revision, datetimestamp, pngimage, jpgimage, "
					+ "userwhoupdated, mlocation, pages, docnum_only from solrmaster where key = '" + solrid + "'";
			rs = stmt.executeQuery(smartFindDocumentSelectQuery);
			if (rs != null) {
				while (rs.next()) {
					smartFindDocument = new SmartFindDocument();
					smartFindDocument.setId(rs.getString("key"));
					smartFindDocument.setComponents(rs.getString("components"));
					smartFindDocument.setDocnum(rs.getString("docnum"));
					smartFindDocument.setManufacturer(rs.getString("manufacturer"));
					smartFindDocument.setRelateditems(rs.getString("relateditems"));
					smartFindDocument.setSource(rs.getString("source"));
					smartFindDocument.setTitle(rs.getString("title"));
					smartFindDocument.setUserupdated(rs.getString("userupdated"));
					smartFindDocument.setSmallimagepath(rs.getString("smallimagepath"));
					smartFindDocument.setImagepath(rs.getString("imagepath"));
					smartFindDocument.setFilepath(rs.getString("filepath"));
					smartFindDocument.setCategoryl0(rs.getString("categoryl0"));
					smartFindDocument.setCspeciality(rs.getString("cspeciality"));
					smartFindDocument.setLables(rs.getString("lables"));
					smartFindDocument.setRank(rs.getString("rank"));
					smartFindDocument.setDate_1(rs.getString("date_1"));
					smartFindDocument.setDeleted(rs.getString("deleted"));
					smartFindDocument.setPartnumber(rs.getString("partnumber"));
					smartFindDocument.setComponentNumber(rs.getString("component_number"));
					smartFindDocument.setFinishedGoodsNumber(rs.getString("finished_goods_number"));
					smartFindDocument.setNotes(rs.getString("notes"));
					smartFindDocument.setImageset(rs.getString("imageset"));
					final String clicked = rs.getString("clicked");
					if(clicked != null && !clicked.isEmpty() && !clicked.equals("null"))
					{
						smartFindDocument.setClicked(Long.parseLong(clicked));
					}
					
					final String downloaded = rs.getString("downloaded");
					if(downloaded != null && !downloaded.isEmpty() && !downloaded.equals("null"))
					{
						smartFindDocument.setDownloaded(Long.parseLong(downloaded));
					}
					
					final String emailed = rs.getString("emailed");
					if(emailed != null && !emailed.isEmpty() && !emailed.equals("null"))
					{
						smartFindDocument.setEmailed(Long.parseLong(emailed));
					}
					smartFindDocument.setRevision(rs.getString("revision"));
					
					smartFindDocument.setDatetimestamp(rs.getString("datetimestamp"));
					smartFindDocument.setPngimage(rs.getString("pngimage"));
					smartFindDocument.setJpgimage(rs.getString("jpgimage"));
					smartFindDocument.setUserwhoupdated(rs.getString("userwhoupdated"));
					smartFindDocument.setmLocation(rs.getString("mlocation"));
					smartFindDocument.setPages(rs.getString("pages"));
					smartFindDocument.setDocnumOnly(rs.getString("docnum_only"));
					break;
				}
				return smartFindDocument;
			}else{
				return null;
			}
			
		} catch (SQLException sql) {
			Log.Error("Exception while retrieving the Smart Find Document from solrmaster."
					+ sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",
					".");
			throw new AppException(newmessage,
					"Error while processing the request.", 1, sql.getCause(),
					true);
		} catch (Exception e) {
			Log.Error("Exception while retrieving the Smart Find Document from solrmaster."
					+ e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",
					".");
			throw new AppException(newmessage,
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (connection != null) {
				try {
					stmt.close();
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}
			}
		}		
	}
	
	public ArrayList<SmartFindDocument> getSmartFindBackupInformation(String solrid) throws AppException, SQLException{
		Connection connection = null;
		connection = getPostGresDBConnection();
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		String smartFindDocumentSelectQuery = null;
		ArrayList<SmartFindDocument> smartFindDocumentsList = new ArrayList<SmartFindDocument>();
		try {			
			smartFindDocumentSelectQuery = "select key, id, components, docnum, manufacturer, relateditems, source, title, userupdated, smallimagepath, imagepath, filepath, cspeciality, categoryl0, lables, rank, date_1, deleted, partnumber, finished_goods_number, component_number, notes, imageset, clicked, downloaded, emailed, revision from solrmaster_replicate where key = '" + solrid + "'";
			rs = stmt.executeQuery(smartFindDocumentSelectQuery);
			if (rs != null) {
				while (rs.next()) {
					SmartFindDocument smartFindDocument = new SmartFindDocument();
					smartFindDocument.setKey(rs.getString("key"));
					smartFindDocument.setId(rs.getString("id"));
					smartFindDocument.setComponents(rs.getString("components"));
					smartFindDocument.setDocnum(rs.getString("docnum"));
					smartFindDocument.setManufacturer(rs.getString("manufacturer"));
					smartFindDocument.setRelateditems(rs.getString("relateditems"));
					smartFindDocument.setSource(rs.getString("source"));
					smartFindDocument.setTitle(rs.getString("title"));
					smartFindDocument.setUserupdated(rs.getString("userupdated"));
					smartFindDocument.setSmallimagepath(rs.getString("smallimagepath"));
					smartFindDocument.setImagepath(rs.getString("imagepath"));
					smartFindDocument.setFilepath(rs.getString("filepath"));
					smartFindDocument.setCategoryl0(rs.getString("categoryl0"));
					smartFindDocument.setCspeciality(rs.getString("cspeciality"));
					smartFindDocument.setLables(rs.getString("lables"));
					smartFindDocument.setRank(rs.getString("rank"));
					smartFindDocument.setDate_1(rs.getString("date_1"));
					smartFindDocument.setDeleted(rs.getString("deleted"));
					smartFindDocument.setPartnumber(rs.getString("partnumber"));
					smartFindDocument.setComponentNumber(rs.getString("component_number"));
					smartFindDocument.setFinishedGoodsNumber(rs.getString("finished_goods_number"));
					smartFindDocument.setNotes(rs.getString("notes"));
					smartFindDocument.setImageset(rs.getString("imageset"));
					final String clicked = rs.getString("clicked");
					if(clicked != null && !clicked.isEmpty() && !clicked.equals("null"))
					{
						smartFindDocument.setClicked(Long.parseLong(clicked));
					}
					
					final String downloaded = rs.getString("downloaded");
					if(downloaded != null && !downloaded.isEmpty() && !downloaded.equals("null"))
					{
						smartFindDocument.setDownloaded(Long.parseLong(downloaded));
					}
					
					final String emailed = rs.getString("emailed");
					if(emailed != null && !emailed.isEmpty() && !emailed.equals("null"))
					{
						smartFindDocument.setEmailed(Long.parseLong(emailed));
					}
					smartFindDocument.setRevision(rs.getString("revision"));
					//smartFindDocument.setUserLastUpdated(userLastUpdated);
					
					smartFindDocumentsList.add(smartFindDocument);
				}
				return smartFindDocumentsList;
			}else{
				return null;
			}
			
		} catch (SQLException sql) {
			Log.Error("Exception while retrieving the Smart Find Audit Documents from solrmaster."
					+ sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",
					".");
			throw new AppException(newmessage,
					"Error while processing the request.", 1, sql.getCause(),
					true);
		} catch (Exception e) {
			Log.Error("Exception while retrieving the Smart Find Audit Documents from solrmaster."
					+ e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",
					".");
			throw new AppException(newmessage,
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (connection != null) {
				try {
					stmt.close();
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}
			}
		}		
	}
	
	public ArrayList<SmartFindAuditDocument> getSmartFindAuditInformation(String solrid) throws AppException, SQLException{
		Connection connection = null;
		connection = getPostGresDBConnection();
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		String smartFindDocumentSelectQuery = null;
		ArrayList<SmartFindAuditDocument> smartFindAuditDocumentsList = new ArrayList<SmartFindAuditDocument>();
		try {
			smartFindDocumentSelectQuery = "select id, solrid, from_id, to_id, user_who_updated, date_time from solrmaster_audit where solrid = '" + solrid + "'";
			rs = stmt.executeQuery(smartFindDocumentSelectQuery);
			if (rs != null) {
				while (rs.next()) {
					SmartFindAuditDocument smartFindAuditDocument = new SmartFindAuditDocument();
					smartFindAuditDocument.setId(rs.getString("id"));
					smartFindAuditDocument.setSolrid(rs.getString("solrid"));
					smartFindAuditDocument.setFromId(rs.getString("from_id"));
					smartFindAuditDocument.setToId(rs.getString("to_id"));
					smartFindAuditDocument.setUserWhoUpdated(rs.getString("user_who_updated"));
					smartFindAuditDocument.setDateWithTimeStamp(rs.getString("date_time"));
															
					smartFindAuditDocumentsList.add(smartFindAuditDocument);
				}
			}
			return smartFindAuditDocumentsList;
		} catch (SQLException sql) {
			Log.Error("Exception while retrieving the Smart Find Audit Documents from solrmaster_audit table."
					+ sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",
					".");
			throw new AppException(newmessage,
					"Error while processing the request.", 1, sql.getCause(),
					true);
		} catch (Exception e) {
			Log.Error("Exception while retrieving the Smart Find Audit Documents from solrmaster_audit table."
					+ e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",
					".");
			throw new AppException(newmessage,
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (connection != null) {
				try {
					stmt.close();
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}
			}
		}		
	}
	
	public String addCatalogUpdateDetailsInAuditTable(String solrid, String user, SmartFindDocument oldSmartFindDocument, SmartFindDocument newSmartFindDocument) throws AppException, SQLException{
		Connection connection = null;
		connection = getPostGresDBConnection();
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		long maxid = 0;
		long maxToId = 0;
		long idNextToMaxId = 0;
		String smartFindDocumentSelectQuery = null; 
		String smartFindDocumentMaxToSelectQuery = null;
		String smartFindDocumentMaxIdSelectQuery = null;
		String smartFindDocumentUpdateQuery = null;
		String solrMasterReplicateInsertQuery = null;
		Timestamp timestamp = new Timestamp(new java.util.Date().getTime());
		try {
			smartFindDocumentSelectQuery = "select id from solrmaster_audit where solrid = '" + solrid + "'";
			smartFindDocumentMaxIdSelectQuery = "select max(id) as maxid from solrmaster_audit";
			rs = stmt.executeQuery(smartFindDocumentMaxIdSelectQuery);
			if(rs != null){
				while(rs.next()){
					String longString = rs.getString(1);
					if(longString != null){
						maxid = Long.valueOf(longString).longValue();
					}
				}
			}
			maxid = maxid + 1;
			rs1 = stmt.executeQuery(smartFindDocumentSelectQuery);
			if(rs1 != null){
				smartFindDocumentMaxToSelectQuery = "select max(to_id) as to from solrmaster_audit where solrid = '" + solrid + "'";
				rs2 = stmt.executeQuery(smartFindDocumentMaxToSelectQuery);
				if(rs2 != null){
					while(rs2.next()){
						String longString = rs2.getString(1);
						if(longString != null){
							maxToId = Long.valueOf(longString).longValue();
						}															
					}					
				}
				idNextToMaxId = maxToId + 1;
				smartFindDocumentUpdateQuery = "insert into solrmaster_audit (id, solrid, from_id, to_id, user_who_updated, date_time) values ("+ maxid + ",'"+ solrid + "'," + maxToId + "," + idNextToMaxId + ",'"+ user + "','" + timestamp +"')";
				stmt.executeUpdate(smartFindDocumentUpdateQuery);
			}else{
				smartFindDocumentUpdateQuery = "insert into solrmaster_audit (id, solrid, from_id, to_id, user_who_updated, date_time) values ("+ maxid + ",'"+ solrid + "'," + maxToId + "," + idNextToMaxId + ",'"+ user + "','" + timestamp +"')";
				stmt.executeUpdate(smartFindDocumentUpdateQuery);
			}
			solrMasterReplicateInsertQuery = "insert into solrmaster_replicate (id, key, components, docnum, manufacturer, relateditems, source, title, "
					+ "userupdated, smallimagepath, imagepath, filepath, cspeciality, categoryl0, lables, rank, date_1, imageset, partnumber, "
					+ "finished_goods_number, component_number, deleted, notes, revision, clicked, downloaded, emailed, datetimestamp, pngimage, jpgimage, "
					+ "userwhoupdated, mlocation, pages, docnum_only ) values"					
					+ "("
					+ maxToId 
					+",'" + solrid
					+"','" + oldSmartFindDocument.getComponents()
					+"','" + oldSmartFindDocument.getDocnum()
					+"','" + oldSmartFindDocument.getManufacturer()
					+"','" + oldSmartFindDocument.getRelateditems()
					+"','" + oldSmartFindDocument.getSource()
					+"','" + oldSmartFindDocument.getTitle()
					+"','" + oldSmartFindDocument.getUserupdated()
					+"','" + oldSmartFindDocument.getSmallimagepath()
					+"','" + oldSmartFindDocument.getImagepath()
					+"','" + oldSmartFindDocument.getFilepath()
					+"','" + oldSmartFindDocument.getCspeciality()
					+"','" + oldSmartFindDocument.getCategoryl0()
					+"','" + oldSmartFindDocument.getLables()
					+"','" + oldSmartFindDocument.getRank()
					+"','" + oldSmartFindDocument.getDate_1()
					+"','" + oldSmartFindDocument.getImageset()
					+"','" + oldSmartFindDocument.getPartnumber()
					+"','" + oldSmartFindDocument.getFinishedGoodsNumber()
					+"','" + oldSmartFindDocument.getComponentNumber()
					+"','" + oldSmartFindDocument.getDeleted()
					+"','" + oldSmartFindDocument.getNotes()
					+"','" + oldSmartFindDocument.getRevision()
					+"'," + oldSmartFindDocument.getClicked()
					+"," + oldSmartFindDocument.getDownloaded()
					+"," + oldSmartFindDocument.getEmailed()
					+",'" + oldSmartFindDocument.getDatetimestamp()
					+"','" + oldSmartFindDocument.getPngimage()
					+"','" + oldSmartFindDocument.getJpgimage()
					+"','" + oldSmartFindDocument.getUserwhoupdated()
					+"','" + oldSmartFindDocument.getmLocation()
					+"','" + oldSmartFindDocument.getPages()
					+"','" + oldSmartFindDocument.getDocnumOnly()
					+"')";
			stmt.executeUpdate(solrMasterReplicateInsertQuery);
			
			solrMasterReplicateInsertQuery = "insert into solrmaster_replicate (id, key, components, docnum, manufacturer, relateditems, source, title, "
					+ "userupdated, smallimagepath, imagepath, filepath, cspeciality, categoryl0, lables, rank, date_1, imageset, partnumber, "
					+ "finished_goods_number, component_number, deleted, notes, revision, clicked, downloaded, emailed, datetimestamp, pngimage, jpgimage, "
					+ "userwhoupdated, mlocation, pages, docnum_only ) values"
					+ "("
					+ idNextToMaxId 
					+",'" + solrid
					+"','" + newSmartFindDocument.getComponents()
					+"','" + newSmartFindDocument.getDocnum()
					+"','" + newSmartFindDocument.getManufacturer()
					+"','" + newSmartFindDocument.getRelateditems()
					+"','" + newSmartFindDocument.getSource()
					+"','" + newSmartFindDocument.getTitle()
					+"','" + newSmartFindDocument.getUserupdated()
					+"','" + newSmartFindDocument.getSmallimagepath()
					+"','" + newSmartFindDocument.getImagepath()
					+"','" + newSmartFindDocument.getFilepath()
					+"','" + newSmartFindDocument.getCspeciality()
					+"','" + newSmartFindDocument.getCategoryl0()
					+"','" + newSmartFindDocument.getLables()
					+"','" + newSmartFindDocument.getRank()
					+"','" + newSmartFindDocument.getDate_1()
					+"','" + newSmartFindDocument.getImageset()
					+"','" + newSmartFindDocument.getPartnumber()
					+"','" + newSmartFindDocument.getFinishedGoodsNumber()
					+"','" + newSmartFindDocument.getComponentNumber()
					+"','" + newSmartFindDocument.getDeleted()
					+"','" + newSmartFindDocument.getNotes()
					+"','" + newSmartFindDocument.getRevision()
					+"'," + newSmartFindDocument.getClicked()
					+"," + newSmartFindDocument.getDownloaded()
					+"," + newSmartFindDocument.getEmailed()
					+",'" + newSmartFindDocument.getDatetimestamp()
					+"','" + newSmartFindDocument.getPngimage()
					+"','" + newSmartFindDocument.getJpgimage()
					+"','" + newSmartFindDocument.getUserwhoupdated()
					+"','" + newSmartFindDocument.getmLocation()
					+"','" + newSmartFindDocument.getPages()
					+"','" + newSmartFindDocument.getDocnumOnly()
					+"')";
			stmt.executeUpdate(solrMasterReplicateInsertQuery);
			return "{\"message\":\"successfully inserted entries in solrmaster_audit table and in solrmaster_replicate table.\"}";
		} catch (SQLException sql) {
			Log.Error("Exception while inserting the Smart Find Audit Entries from solrmaster tables."
					+ sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",
					".");
			throw new AppException(newmessage,
					"Error while processing the request.", 1, sql.getCause(),
					true);
		} catch (Exception e) {
			Log.Error("Exception while inserting the Smart Find Audit Entries from solrmaster tables."
					+ e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",
					".");
			throw new AppException(newmessage,
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (connection != null) {
				try {
					stmt.close();
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}
			}
		}		
	}
	
	
	public Connection getPostGresDBConnection() throws AppException, SQLException {
		PrismDbUtil dbUtil = new PrismDbUtil();
		Connection connection = dbUtil.getPostGresConnection();
		return connection;
	}
}
