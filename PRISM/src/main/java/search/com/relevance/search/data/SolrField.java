package com.relevance.search.data;

public class SolrField {
	private String Name;
	private String Type;
	private String isIndexed;
	private String isStored;
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getIsIndexed() {
		return isIndexed;
	}
	public void setIsIndexed(String isIndexed) {
		this.isIndexed = isIndexed;
	}
	public String getIsStored() {
		return isStored;
	}
	public void setIsStored(String isStored) {
		this.isStored = isStored;
	}	
}
