package com.relevance.search.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.impl.NoOpResponseParser;
import org.apache.solr.client.solrj.request.QueryRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.common.util.SimpleOrderedMap;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.relevance.prism.data.DataTableView;
import com.relevance.prism.data.E2emfView;
import com.relevance.prism.data.ProfileConfig;
import com.relevance.prism.data.SearchLog;
import com.relevance.prism.data.SearchParam;
import com.relevance.prism.data.SecondaryRole;
import com.relevance.prism.service.BaseService;
import com.relevance.prism.service.DataAnalyserService;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismDbUtil;
import com.relevance.prism.util.PrismHandler;
import com.relevance.search.dao.InstrumentSearchDao;
import com.relevance.search.dao.SearchDao;
import com.relevance.search.dao.SolrIndexerDao;
import com.relevance.search.data.SolrConf;
import com.relevance.search.data.SolrField;
import com.relevance.search.util.SearchCache;

public class SearchService extends BaseService {
	PrismDbUtil dbUtil;

	static String solarUrl = "SOLR_SERVER_URL";
	static String solarInstanceName = "SOLR_SERVER_INSTANCE_NAME";

	static String solarServerInsanceUrl = null;

	private String searchType;

	private JSONObject searchProfile;

	public SearchService() {
		dbUtil = new PrismDbUtil();
		if ("true".equalsIgnoreCase(E2emfAppUtil.getAppProperty("secured"))) {

			System.out.println("*******************inside if condition for security flag check *********************");
			Log.Info("**********************inside if condition for security flag check ********************* ");
			String cdhVersion = null;
			cdhVersion = E2emfAppUtil.getAppProperty(E2emfConstants.CDH_VERSION);
			System.out.println("CDH Vesion : " + cdhVersion);
			Log.Info("CDH Vesion : " + cdhVersion);
			System.out.println("CDH Vesion : " + cdhVersion);
			Log.Info("Trust Store : " + E2emfAppUtil.getAppProperty("truststore"));
			System.out.println("Trust Store : " + E2emfAppUtil.getAppProperty("truststore"));
			Log.Info("Password : " + E2emfAppUtil.getAppProperty("password"));
			System.out.println("Password : " + E2emfAppUtil.getAppProperty("password"));
			Log.Info("JAAS : " + E2emfAppUtil.getAppProperty("jaas"));
			System.out.println("JAAS : " + E2emfAppUtil.getAppProperty("jaas"));
			Log.Info("Krb5 : " + E2emfAppUtil.getAppProperty("krb5"));
			System.out.println("Krb5 : " + E2emfAppUtil.getAppProperty("krb5"));

			System.setProperty("javax.net.ssl.trustStore", E2emfAppUtil.getAppProperty("truststore"));
			System.setProperty("javax.net.ssl.trustStorePassword", E2emfAppUtil.getAppProperty("password"));
			System.setProperty("java.security.auth.login.config", E2emfAppUtil.getAppProperty("jaas"));
			System.setProperty("java.security.krb5.conf", E2emfAppUtil.getAppProperty("krb5"));

		}
	}

	public String getSearchResult(String queryParameter, String solrCollectionName, String profileId, String startRow,
			String overRideParams, List<SecondaryRole> secondaryRoleList, Boolean scEnabled, String profileName,
			String secondaryTabs, HttpSession session, String noRows, HashMap<String, String> overRideMap)
			throws Exception {

		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());

		HttpSolrServer solrServer;
		searchType = E2emfAppUtil.getAppProperty(E2emfConstants.e2emfSearchType);
		String result = "";
		Gson gson = new Gson();
		NoOpResponseParser parser;
		QueryRequest req;
		NamedList<Object> resp;
		SolrQuery solrQuery = null;
		HashMap<String, String> tabHashMap = new HashMap<>();

		try {
			if ("true".equalsIgnoreCase(E2emfAppUtil.getAppProperty("secured"))) {
				setSecuredParameters();
			}

			// Disabling processing of secondary queries
			if (secondaryTabs != null && !secondaryTabs.trim().isEmpty() && "0".equalsIgnoreCase(startRow)) {
				processSecondaryQueries(secondaryTabs, startRow, noRows, session, overRideParams, secondaryRoleList,
						profileName, tabHashMap, queryParameter, overRideMap);
			}

			this.searchProfile = getSearchProfile(profileId, profileName);
			if (this.searchProfile != null) {
				solrCollectionName = this.searchProfile.getString("appName");
				setSearchProfileParams(queryParameter, startRow, noRows);
				// Disabling overrides
				// Dont comment overridesearchprofile imapcts qfind search
				overrideSearchProfile(overRideParams);
				solrQuery = buildSolrQuery(this.searchProfile, overRideMap);

				if (scEnabled) {
					addSecondaryRoleQuery(secondaryRoleList, solrQuery);
				}
			} else {
				solrQuery = buildSolrQuery(queryParameter, overRideMap);
			}

			// Example Solr Parameter:
			// /select?q=*:*&rows=10&wt=json&indent=true&spellcheck.dictionary=false&spellcheck.count=false&collection=Integra,Docspera,SPELLCHECK
			// if (secondaryTabs == null && profileId != null &&
			// !profileId.isEmpty())
			// profileConfigParameter.put("rows", 0);

			if (solrQuery != null && solrCollectionName != null) {
				solrServer = getSolarServerConnection(solrCollectionName);
				if (solrServer != null) {
					Log.Info("Solr Query : " + solrQuery.toString());
					System.out.println("Solr Query : " + solrQuery.toString());
					req = new QueryRequest(solrQuery);
					parser = new NoOpResponseParser();
					parser.setWriterType("json");
					solrServer.setParser(parser);
					resp = solrServer.request(req);
					result = (String) resp.get("response");
					Log.Info("Recieved response from solr successfully ");

				} else {
					Log.Error("Unable to connect to solarServer...");
				}
				if (secondaryTabs != null) {
					tabHashMap.put(profileId, result);
					result = gson.toJson(tabHashMap);
				}
			}

		} catch (Exception e) {
			result = PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return result;
	}

	private SolrQuery buildSolrQuery(String queryParameter, HashMap<String, String> overRideMap) throws JSONException {
		JSONObject jsonQuery = new JSONObject(queryParameter);
		return buildSolrQuery(jsonQuery, overRideMap);
	}

	private SolrQuery buildSolrQuery(JSONObject jsonQuery, HashMap<String, String> overRideMap) throws JSONException {
		SolrQuery query = new SolrQuery();
		String rawQuery = null;
		Iterator it = jsonQuery.keys();

		while (it.hasNext()) {
			String key = (String) it.next();
			if (key != null && key.equalsIgnoreCase("rawquery")) {
				rawQuery = jsonQuery.get(key).toString();
			} else {
				query.set(key, jsonQuery.get(key).toString());
			}
		}

		if (overRideMap != null) {
			it = overRideMap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (pair.getKey().toString().equalsIgnoreCase("rawquery")) {
					rawQuery = (rawQuery == null) ? pair.getValue().toString() : rawQuery + pair.getValue().toString();
				} else if (pair.getValue() != null) {
					query.set(pair.getKey().toString(), pair.getValue().toString());// will
																					// overwrite
																					// the
																					// previous
																					// query
																					// value
																					// if
																					// it
																					// exists
				}

			}
		}

		if (rawQuery != null && rawQuery.contains("=")) {
			String[] rawPairs = rawQuery.split("&");
			for (int i = 0; i < rawPairs.length; i++) {
				String[] pair = rawPairs[i].split("=",2);

				if (pair.length > 1) {
					if (query.getParams(pair[0]) != null && query.getParams(pair[0]).length > 0) {
						query.add(pair[0], pair[1]);
					} else {
						query.set(pair[0], pair[1]);
					}
				}
			}

		}

		// No need to append or overwrite values coming from UI
		/*
		 * String queryParameter = null; JSONObject jsonQueryParameter;
		 * 
		 * if(profileConfigParameter != null) { queryParameter =
		 * profileConfigParameter.toString(); }
		 * 
		 * jsonQueryParameter = new JSONObject(queryParameter);
		 * 
		 * Iterator it = jsonQueryParameter.keys();
		 * 
		 * while( it.hasNext() ) { String key = (String)it.next();
		 * if(key.equalsIgnoreCase("collection")) { String arr[] =
		 * jsonQueryParameter.get(key).toString().split(","); if(arr.length ==
		 * 1) { continue; } } else if(key.equals("rawQuery")) { String arr[] =
		 * jsonQueryParameter.get(key).toString().split("&"); if(arr.length > 1)
		 * { for(String raw : arr) { String subArr[] = raw.split("=");
		 * if(subArr.length > 1) { if(query.get(subArr[0].replace("append:",
		 * "")) != null && subArr[0].contains("append:")) { subArr[0] =
		 * subArr[0].replace("append:", ""); query.add(subArr[0], subArr[1]); }
		 * else { query.set(subArr[0], subArr[1]); } } } } } else {
		 * query.set(key, jsonQueryParameter.get(key).toString()); } }
		 */
		return query;
	}

	private void addSecondaryRoleQuery(List<SecondaryRole> secondaryRoleList, SolrQuery solrQuery) {
		if (!secondaryRoleList.isEmpty()) {
			for (SecondaryRole secondaryRole : secondaryRoleList) {
				StringBuilder fq;
				String groupIds = secondaryRole.getGroupIds();
				String tempFQ = solrQuery.get("fq");
				if (tempFQ == null) {
					fq = new StringBuilder();
				} else {
					fq = new StringBuilder(tempFQ);
					fq.append(" AND ");
				}
				fq.append(" ( ").append(secondaryRole.getFacetName());
				if (groupIds != null && !groupIds.trim().isEmpty()) {

					fq.append(": (")
							.append(secondaryRole.getGroupIds().replaceAll(secondaryRole.getSectionDelimeter(), " OR "))
							.append(" ))");
				} else {
					fq.append(": \"\" ) ");
					String qfindCustomQuery = E2emfAppUtil.getAppProperty(E2emfConstants.QFIND_CUSTOM_QUERY);
					if (qfindCustomQuery != null) {
						fq.append(" ").append(qfindCustomQuery);
					}
				}
				solrQuery.set("fq", fq.toString());
			}
		}
	}

	private void overrideSearchProfile(String overRideParams) throws JSONException {
		JSONObject overRidingObject = null;
		Iterator it;
		if (overRideParams != null && !overRideParams.trim().isEmpty()) {
			try {
				Log.Info("inside OverrideParam---> param -> " + overRideParams);
				overRidingObject = new JSONObject(overRideParams);
			} catch (Exception ex) {
				overRidingObject = null;
			}

			if (overRidingObject != null && this.searchProfile != null) {
				it = overRidingObject.keys();
				while (it.hasNext()) {
					String key = it.next().toString();
					String k = (this.searchProfile.has(key)) ? this.searchProfile.get(key).toString() : "";
					if (!k.isEmpty() && key.equalsIgnoreCase("fq")) {
						k += " AND " + overRidingObject.get(key).toString();
					} else {
						k = overRidingObject.get(key).toString();
					}
					this.searchProfile.put(key, k);
				}

			}
		}
	}

	private JSONObject getSearchProfile(String profileId, String profileName) throws JSONException, Exception {
		DataAnalyserService analyserService;
		ProfileConfig config;
		E2emfView configResult;
		DataTableView view;
		String filter;
		JSONObject profileConfigParameter = null;

		Gson gson = new Gson();

		if ((profileId != null && !profileId.isEmpty()) || (profileName != null && !profileName.isEmpty())) {
			if (SearchCache.QueriesCache.containsKey(profileId)) {
				profileConfigParameter = new JSONObject(SearchCache.QueriesCache.get(profileId));
			} else {
				analyserService = new DataAnalyserService();
				config = new ProfileConfig();
				if (profileId != null && !profileId.isEmpty()) {
					config.setId(Integer.parseInt(profileId));
				} else {
					config.setDisplayname(profileName);
				}

				configResult = analyserService.getProfileConfig(config);
				if (configResult != null) {
					view = (DataTableView) configResult;
					profileConfigParameter = new JSONObject(gson.toJson(view.getData()));
					profileConfigParameter = profileConfigParameter.getJSONArray("myArrayList").getJSONObject(0)
							.getJSONObject("map");
					// Solr Query Built object is present in filters...
					filter = profileConfigParameter.getString("filters");
					profileConfigParameter = new JSONObject(filter);
					if (profileId != null && !profileId.isEmpty()) {
						SearchCache.QueriesCache.put(profileId, filter);
					} else if (profileName != null && !profileName.isEmpty()) {
						SearchCache.QueriesCache.put(profileName, filter);
					}
				}
			}

		}
		return profileConfigParameter;
	}

	private void setSearchProfileParams(String queryParam, String startRow, String noRows) throws JSONException {
		if (this.searchProfile != null) {
			if (this.searchProfile.getString("qt") != null
					&& this.searchProfile.getString("qt").equalsIgnoreCase("/spell")) {
				this.searchProfile.put("spellcheck.q", queryParam);
			} else {
				this.searchProfile.put("q", queryParam);
			}

			if (this.searchProfile.has("rows") && (String) this.searchProfile.get("rows") != "" && startRow != null
					&& !startRow.trim().isEmpty()) {
				this.searchProfile.put("start", startRow);
			}
			if (noRows != null && !"".equalsIgnoreCase(noRows)) {
				this.searchProfile.put("rows", noRows);
			}
			// if(this.searchProfile.has("noRows")
			// && (String) this.searchProfile.get("noRows") != "" && startRow !=
			// null && !startRow.trim().isEmpty()) {
			//
			// }
		}
	}

	private void processSecondaryQueries(String secondaryTabs, String startRow, String noRows, HttpSession session,
			String overRideParams, List<SecondaryRole> secondaryRoleList, String profileName,
			HashMap<String, String> tabHashMap, String queryParameter, HashMap<String, String> overRideMap)
			throws Exception {
		try {
			String[] tabs = secondaryTabs.split("&tabBr&");
			for (String tab : tabs) {
				String[] tabInfo = tab.split(":");
				String tabId = tabInfo[0];
				boolean isEnabled = true;
				boolean canSearch = false;
				if (tabInfo.length > 2 && tabInfo.length == 6) {
					Boolean rolesEnabled = Boolean.parseBoolean(tabInfo[2]);
					String roleDbName = tabInfo[3];
					String searchQuery = tabInfo[4];
					String roleNames = tabInfo[5];
					isEnabled = rolesEnabled;
					if (rolesEnabled) {
						SearchDao dao = new SearchDao();
						List<String> secondaryRoles = new ArrayList<>();
						if (session.getAttribute("searchAppSecondaryRoles") != null) {
							secondaryRoles = (List<String>) session.getAttribute("searchAppSecondaryRoles");
						} else {
							secondaryRoles = dao.getSecondarySearchAppRoles(roleDbName, searchQuery,
									(String) session.getAttribute("userName"));
							session.setAttribute("searchAppSecondaryRoles", secondaryRoles);
						}
						if (roleNames != null && !roleNames.isEmpty()) {
							String[] roles = roleNames.split(",");
							for (String role : roles) {
								if (secondaryRoles.contains(role)) {
									canSearch = true;
									break;
								}
							}
						}
					}
				}

				if (isEnabled) {
					if (canSearch) {
						Boolean tabSCEnabled = Boolean.parseBoolean(tabInfo[1]);
						tabHashMap.put(tabId, getSearchResult(queryParameter, null, tabId, "0", overRideParams,
								secondaryRoleList, tabSCEnabled, profileName, null, session, noRows, overRideMap));
					} else {
						String response = "{\"response\" : {\"numFound\" : 0}}";
						tabHashMap.put(tabId, response);
					}
				} else {
					Boolean tabSCEnabled = Boolean.parseBoolean(tabInfo[1]);
					tabHashMap.put(tabId, getSearchResult(queryParameter, null, tabId, "0", overRideParams,
							secondaryRoleList, tabSCEnabled, profileName, null, session, noRows, overRideMap));
				}

			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
	}

	public boolean updateCache(String id, String name, String query) throws Exception {
		try {
			if (name != null && SearchCache.QueriesCache.containsKey(name)) {
				SearchCache.QueriesCache.put(name, query);
			}
			if (id != null && SearchCache.QueriesCache.containsKey(id)) {
				SearchCache.QueriesCache.put(id, query);
			}
			return true;
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
			return false;
		}
	}

	public String getJsonObject(Object... obj) throws AppException {
		String searchResultJson = null;
		HttpSolrServer solarServer = null;
		QueryResponse queryResponse = null;
		searchType = E2emfAppUtil.getAppProperty(E2emfConstants.e2emfSearchType);

		SearchParam searchParam = null;
		String searchCriteria = null;
		boolean searchOutputColumns = true;
		// String obfuscateFlag = null;
		try {
			if (obj != null && obj.length > 1) {
				searchParam = (SearchParam) obj[0];
				searchCriteria = ((String) obj[1]).trim();

				if (obj.length >= 3 && obj[2] != null) {
					searchOutputColumns = Boolean.parseBoolean((String) obj[2]);
					/*
					 * if (obj[3] != null) obfuscateFlag = (String) obj[3];
					 */
				}

				Log.Info("Searching initiated for " + searchCriteria);
			} /*
				 * else { searchCriteria = (String) obj[0]; Log.Info(
				 * "Searching initiated for " + searchCriteria); }
				 */

			StringBuilder searchStr = new StringBuilder();
			if (searchParam.getKey() != null && searchParam.getKey().isEmpty()) {
				searchStr.append("*");
			} else if ("1".equalsIgnoreCase(searchType)) {
				searchStr.append("rawdata:*");
				searchStr.append(searchParam.getKey().replaceAll(" ", "*"));
				searchStr.append("*");
			} else if (searchOutputColumns) {
				// Replace Special character like - with "+" with AND condition
				String searchWords = searchParam.getKey();
				searchWords = searchWords.replaceAll("-", "+");

				// parameters.set("q", "output:*" +
				// searchParam.getKey().replaceAll(" ", "* OR output:*") + "*");
				searchStr.append("output:");
				searchStr.append("*");
				searchStr.append(searchWords.replaceAll(" ", "* OR output:*"));
				searchStr.append("*");
				searchStr = new StringBuilder(searchStr.toString().replaceAll("\\+", "* AND output:*"));
			} else {
				// parameters.set("q", "rawdata:*" +
				// searchParam.getKey().replaceAll(" ", "* OR rawdata:*") +
				// "*");//Search only in all data column
				searchStr.append("rawdata:");
				searchStr.append("\"");
				searchStr.append(searchParam.getKey().replaceAll(" ", "\" OR rawdata:\""));
				searchStr.append("\"");
				searchStr = new StringBuilder(searchStr.toString().replaceAll("\\+", "\" AND rawdata:\""));
			}

			if (searchCriteria != null && searchCriteria.equalsIgnoreCase("searchService")
					&& searchParam.getFacet() == null) {

				solarServer = getSolarServerConnection(searchParam.getSolrCollectionName());

				ModifiableSolrParams parameters = new ModifiableSolrParams();
				// parameters.set("q", "rawdata:*" +
				// searchParam.getKey().replaceAll(" ", "*") + "*");//Search
				// only in all data column

				parameters.set("q", searchStr.toString());
				parameters.set("defType", "edismax");
				// parameters.set("wt", "xml");//Not required since converting
				// from the object directly
				parameters.set("start", "0");
				parameters.set("rows", "0");
				parameters.set("qf", "rawdata rank^2 keywords");
				parameters.set("facet", "true");
				parameters.set("facet.field", "facet");
				parameters.set("facet.sort", "rank+asc");
				// parameters.set("fq", "facet:"+queryFilterVal);

				try {
					if (solarServer != null) {
						queryResponse = solarServer.query(parameters);
						Log.Info("Recieved response from solar successfully ");
					} else {
						Log.Error("Unable to connect to solarServer...");
					}

					Gson gson = new Gson();
					java.util.LinkedHashMap<String, String> facetMap = new java.util.LinkedHashMap<String, String>();

					// First for loop is for fieldname as "facet"
					if (queryResponse != null && queryResponse.getFacetFields() != null) {
						for (FacetField facetField : queryResponse.getFacetFields()) {
							for (Count facetFieldCount : facetField.getValues()) {
								facetMap.put(facetFieldCount.getName(), Long.toString(facetFieldCount.getCount()));
							}
						}
					}
					String json = gson.toJson(facetMap);
					searchResultJson = json;
				} catch (SolrServerException e) {
					Log.Error("Exception in getting Solar Server Connection." + e);
					throw new AppException(e.getMessage(), "Error while processing the request.", 2, e.getCause(),
							true);

				} catch (Exception e) {
					Log.Error("Exception Fetching JSON Data." + e);
					if (e instanceof NullPointerException) {
						throw new AppException("NullPointerException", "No Indexed Data Available in Collection.", 3,
								e.getCause(), true);
					}
					throw new AppException(e.getMessage(), "Error while processing the request.", 2, e.getCause(),
							true);
				}

			}
			// To implement detailed search
			else if (searchCriteria != null && searchCriteria.equalsIgnoreCase("searchService")
					&& searchParam.getFacet() != null) {

				solarServer = getSolarServerConnection(searchParam.getSolrCollectionName());

				ModifiableSolrParams parameters = new ModifiableSolrParams();
				// parameters.set("q", "rawdata:*" +
				// searchParam.getKey().replaceAll(" ", "*") + "*");

				parameters.set("q", searchStr.toString());
				parameters.set("defType", "edismax");
				// parameters.set("wt", "xml");//Not required since converting
				// from the object directly
				parameters.set("start", "0");
				parameters.set("rows", "50");
				parameters.set("fl", "output,id");
				parameters.set("qf", "rawdata rank^2 keywords");
				parameters.set("fq", "facet:" + searchParam.getFacet());
				// new collection type settings
				if ("1".equalsIgnoreCase(searchType)) {
					parameters.set("group", "true");
					parameters.set("group.field", "key");
					parameters.set("group.main", "true");
				}

				try {
					if (solarServer != null) {
						queryResponse = solarServer.query(parameters);
						Log.Info("Recieved response from solar successfully ");
					} else {
						Log.Error("Unable to connect to solarServer...");
					}

					Gson gson = new Gson();
					String json = gson.toJson(queryResponse.getResults());
					searchResultJson = parseJSONStr(json);
					// System.out.println("parsedJSONOutPut:" +
					// searchResultJson);
					// Log.Info("parsedJSONOutPut:" +searchResultJson);

				} catch (SolrServerException e) {
					Log.Error("Exception in getting Solar Server Connection." + e);
					throw new AppException(e.getMessage(), "Error while processing the request.", 2, e.getCause(),
							true);

				} catch (Exception e) {
					Log.Error("Exception Fetching Search Details." + e);
					if (e instanceof NullPointerException) {
						throw new AppException("NullPointerException", "No Indexed Data Available in Collection.", 3,
								e.getCause(), true);
					}
					throw new AppException(e.getMessage(), "Error while processing the request.", 2, e.getCause(),
							true);
				}
			}
		} catch (Exception e) {
			Log.Error("Exception Fetching JSON Data." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException", "No Indexed Data Available in Collection.", 3,
						e.getCause(), true);
			}
			throw new AppException(e.getMessage(), "Error while processing the request.", 2, e.getCause(), true);
		}
		return searchResultJson;

	}

	public String getSearchSuggestion(Object... obj) throws AppException {
		String searchResultJson = null;
		HttpSolrServer solarServer = null;
		QueryResponse queryResponse = null;

		SearchParam searchParam = null;
		if (obj != null && obj.length >= 1) {
			searchParam = (SearchParam) obj[0];
		}

		ModifiableSolrParams parameters = new ModifiableSolrParams();
		parameters.set("spellcheck.q", searchParam.getKey());
		parameters.set("defType", "edismax");
		parameters.set("qt", "/suggest");

		solarServer = getSolarServerConnection(searchParam.getSolrCollectionName());
		try {
			if (solarServer != null) {
				queryResponse = solarServer.query(parameters);
				Log.Info("Recieved response from solar successfully ");
			} else {
				Log.Error("Unable to connect to solarServer...");
			}
			Gson gson = new Gson();
			String json = gson.toJson(queryResponse.getSpellCheckResponse().getSuggestions());
			searchResultJson = json;
		} catch (SolrServerException e) {
			Log.Error("Exception in getting Solar Server Connection." + e);
			throw new AppException(e.getMessage(), "Error while processing the request.", 2, e.getCause(), true);

		} catch (Exception e) {
			Log.Error("Exception Fetching JSON Data." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException", "No Indexed Data Available in Collection.", 3,
						e.getCause(), true);
			}
			throw new AppException(e.getMessage(), "Error while processing the request.", 2, e.getCause(), true);
		}
		return searchResultJson;
	}

	private String parseJSONStr(String jsonStr) throws AppException {

		org.json.JSONArray jsonArr = null;
		String outputJSONStr = "";
		// List<String> outputList = new ArrayList<String>();

		try {

			jsonArr = new org.json.JSONArray(jsonStr);
			int length = jsonArr.length();
			// System.out.println("length:" + length);
			outputJSONStr = "[";

			for (int i = 0; i < length; i++) {
				JSONObject jsonObj = (JSONObject) jsonArr.get(i);
				outputJSONStr = outputJSONStr.concat("{\"key\":\"").concat(jsonObj.getString("id").concat("\","));
				String output = jsonObj.getString("output");
				outputJSONStr = outputJSONStr.concat(output.substring(1));
				if (i != length - 1) {
					outputJSONStr = outputJSONStr.concat(",");
				}
			}
			outputJSONStr = outputJSONStr.concat("]");
		} catch (JSONException e) {
			Log.Error(e);
		} catch (Exception e) {
			Log.Error("Exception parsing the JSON String." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException", "No Indexed Data Available in Collection.", 3,
						e.getCause(), true);
			}
			throw new AppException(e.getMessage(), "Error while processing the request.", 2, e.getCause(), true);
		}
		return outputJSONStr;
	}

	private HttpSolrServer getSolarServerConnection(String solrCollectionName) throws AppException {
		HttpSolrServer serverCon = null;
		String solrCollectionUrl = E2emfAppUtil.getAppProperty(solrCollectionName);
		/*
		 * String solarInstanceName= null; if(solarServerInsanceUrl == null){
		 */
		// solarUrl = appUtil.getAppProperty(E2emfConstants.solarUrl);
		/*
		 * if (Source.equalsIgnoreCase("sap")) solarInstanceName =
		 * E2emfConstants.solarSAPInstanceName; else if
		 * (Source.equalsIgnoreCase("jde")) solarInstanceName =
		 * E2emfConstants.solarJDEInstanceName; else if
		 * (Source.equalsIgnoreCase("gp")) solarInstanceName =
		 * E2emfConstants.solarGPInstanceName; else if
		 * (Source.equalsIgnoreCase("apac")) solarInstanceName =
		 * E2emfConstants.solarAPACInstanceName; else if
		 * (Source.equalsIgnoreCase("bcs")) solarInstanceName =
		 * E2emfConstants.solarBCSInstanceName; if(solarInstanceName!= null) {
		 * solarInstanceName = E2emfAppUtil.getAppProperty(solarInstanceName); }
		 */
		// solarServerInsanceUrl = solarUrl+solarInstanceName+"/";
		/* } */

		try {
			serverCon = new HttpSolrServer(solrCollectionUrl);

		} catch (Exception e) {
			Log.Error("Exception connecting to solr server." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException", "No Indexed Data Available in Collection.", 3,
						e.getCause(), true);
			}
			throw new AppException(e.getMessage(), "Error while processing the request.", 2, e.getCause(), true);
		}

		return serverCon;
	}

	public ArrayList<SolrField> getSolrSchema(String solrCollectionName) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());

		QueryResponse queryResponse = null;
		SolrServer server = null;
		ArrayList<SolrField> solrFields = null;
		SolrField solrField = null;
		NamedList responseHeader = null;
		SolrQuery query = null;
		ArrayList<SimpleOrderedMap> fields = null;
		try {
			server = getSolarServerConnection(solrCollectionName);
			query = new SolrQuery();
			query.add("qt", "/schema/fields");
			queryResponse = server.query(query);
			responseHeader = queryResponse.getResponseHeader();
			fields = (ArrayList<SimpleOrderedMap>) queryResponse.getResponse().get("fields");
			solrFields = new ArrayList<>();
			for (SimpleOrderedMap field : fields) {
				solrField = new SolrField();
				solrField.setName(field.get("name").toString());
				solrField.setType(field.get("type").toString());
				solrField.setIsIndexed(field.get("indexed").toString());
				solrField.setIsStored(field.get("stored").toString());
				solrFields.add(solrField);
			}

		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return solrFields;
	}

	public String indexSolr(SolrConf solrConf, String filterColumnName, String filterColumnType,
			String filterColumnValue) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			if ("true".equalsIgnoreCase(E2emfAppUtil.getAppProperty("secured"))) {
				setSecuredParameters();
			}

			SolrIndexerDao solrindexer = new SolrIndexerDao();
			response = solrindexer.indexData(solrConf, filterColumnName, filterColumnType, filterColumnValue);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	private void setSecuredParameters() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
			String cdhVersion = null;
			cdhVersion = E2emfAppUtil.getAppProperty(E2emfConstants.CDH_VERSION);
			System.out.println("CDH Vesion : " + cdhVersion);
			Log.Info("CDH Vesion : " + cdhVersion);
			System.setProperty("javax.net.ssl.trustStore", E2emfAppUtil.getAppProperty("truststore"));
			System.setProperty("javax.net.ssl.trustStorePassword", E2emfAppUtil.getAppProperty("password"));
			System.setProperty("java.security.auth.login.config", E2emfAppUtil.getAppProperty("jaas"));
			System.setProperty("java.security.krb5.conf", E2emfAppUtil.getAppProperty("krb5"));
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}

	public boolean deleteSearchLog(int id) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		boolean response = false;
		try {
			SearchDao dao = new SearchDao();
			dao.deleteSearchLog(id);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	public List<SearchLog> getSearchHistory(SearchLog searchQuery, String orderBy, String orderStyle)
			throws AppException {
		List<SearchLog> searchHisory = new ArrayList<>();
		try {
			Log.Info("getSearchHistory method called on InstrumentSearchService");
			InstrumentSearchDao instrumentSearchDao = new InstrumentSearchDao();
			searchHisory = instrumentSearchDao.getSearchHistory(searchQuery, orderBy, orderStyle);
			return searchHisory;
		} catch (AppException appe) {
			throw appe;
		} catch (Exception e) {
			Log.Error("Exception while fetching SearchHistory " + e);
		}
		return searchHisory;
	}
}