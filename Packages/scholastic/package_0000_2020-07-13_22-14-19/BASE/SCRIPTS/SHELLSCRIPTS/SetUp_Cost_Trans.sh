#!/bin/bash

export PYTHON_EGG_CACHE=./myeggs

LOG=shell-cost_trans_debit-$(date +%s%N).log

echo "Start: $(date +%T)"
connect_impala=$1 # ip-10-130-13-108.ec2.internal:21000
QUERY_FILE=$2 	  # file name to be passed

if [ ! -f $QUERY_FILE ];
then
  ERROR="Unable to access [ query-file: $QUERY_FILE ], check existence and permissions"
  echo "$ERROR" >> ${LOG}
  hadoop fs -put ${LOG} /sch/logs
  exit 1
fi

impala-shell -i $connect_impala -f $QUERY_FILE  >> ${LOG} 2>&1 # where we will filter out and keep only Debit transactions we can call this table as cost_var_inv_Debit 
RC=$?
if [ $RC -ne 0 ];
then
  echo "<ERROR> Error during exec debit transaction  table"
  hadoop fs -put ${LOG} /sch/logs
  exit 2
fi


rm ${LOG}

