#!/bin/bash

export PYTHON_EGG_CACHE=./myeggs

if hdfs dfs -test -e /sch/trigger/wftrigger.txt ; 
then
	hadoop fs -rmr /sch/trigger/wftrigger.txt
	RC=$?
		if [ $RC -eq 0 ];
		then
				echo "flag has been deleted" 
		else 
				echo " flag has not been deleted"
				exit 1
		fi 
else
	echo "file already deleted"
fi