DROP TABLE IF EXISTS rlsf_prod.ohq_inv_as_of_date_by_cost_rec_hist;
CREATE EXTERNAL TABLE rlsf_prod.ohq_inv_as_of_date_by_cost_rec_hist (   oracle_item STRING,
   isbn13 STRING,
   border_item STRING,
   opus_id STRING,
   cost_center STRING,
   org_id STRING,
   lc_p_eo_rec_trans_id STRING,
   trans_eo_inventory_item_id STRING,
   totalsum DECIMAL(38,6),   date_ STRING ) PARTITIONED BY (   run_date STRING ) 
   STORED AS PARQUET LOCATION '/sch/l4/ohq_inv_as_of_date_by_cost_rec_hist';
