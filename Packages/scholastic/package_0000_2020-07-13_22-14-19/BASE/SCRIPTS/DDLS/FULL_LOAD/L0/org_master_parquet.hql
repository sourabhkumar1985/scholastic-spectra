DROP TABLE IF EXISTS rlsf_prod.org_master_parquet;
CREATE EXTERNAL TABLE rlsf_prod.org_master_parquet (
	oiorgid STRING,
	oidescr STRING,
	oicode STRING,
	oitype STRING
	) STORED AS PARQUET LOCATION 'hdfs://nameservice1/sch/l0/org_master_parquet';