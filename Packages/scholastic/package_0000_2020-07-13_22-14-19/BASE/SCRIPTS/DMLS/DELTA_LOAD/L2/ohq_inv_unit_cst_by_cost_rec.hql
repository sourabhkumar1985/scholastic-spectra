-- the firt insert will include all the latest date records from the final table 
insert into table rlsf_prod.ohq_inv_unit_cst_by_cost_rec
select distinct
inventory_item_id as transactionseoinventoryitemid,
org_id as transactionseoinventoryorgid,
lc_p_eo_rec_trans_id as cstlayercostspeorectrxnid,
unit_cost as cost_unit,
layer_date as transactionseocreationdate,
'' as costdistributionlineseocostelementid,
'' as distributionlineid
from rlsf_prod.ohq_inv_as_of_date_with_layer_cost deb
join rlsf_prod.cost_trans_rank_assign_delta assign on assign.transactionseoinventoryitemid = deb.inventory_item_id and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber};

-- the second insert will insert only those records where there is a match between final table and delta table 
-- and creation date should be greater in delta table when compared to final table 
insert into table rlsf_prod.ohq_inv_unit_cst_by_cost_rec
select distinct 
deb.transactionseoinventoryitemid,
deb.transactionseoinventoryorgid,
deb.cstlayercostspeorectrxnid,
cast((cast(deb.COSTDISTRIBUTIONLINESEOENTEREDCURRENCYAMOUNT as decimal(38,6)) / cast(deb.COSTDISTRIBUTIONSEOLAYERQUANTITY as decimal(38,6)))  as decimal(38,6)) as cost_unit, 
deb.transactionseocreationdate as transactionseocreationdate,
deb.costdistributionlineseocostelementid,
deb.distributionlineid
from  rlsf_prod.cost_trans_debit_delta deb  -- change table name accordingly
join rlsf_prod.cost_trans_rank_assign_delta assign on assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
join rlsf_prod.ohq_inv_as_of_date_with_layer_cost final on (
final.inventory_item_id = deb.transactionseoinventoryitemid and 
final.org_id = deb.transactionseoinventoryorgid and 
final.lc_p_eo_rec_trans_id = deb.cstlayercostspeorectrxnid and 
deb.transactionseocreationdate > final.layer_date)

where (deb.cstlayercostspeocosttransactiontype = "ADJUST" or deb.cstlayercostspeocosttransactiontype= "RECEIPT")
and cast(deb.costdistributionlineseoenteredcurrencyamount as double) <> 0 
and cast((cast(deb.costdistributionseolayerquantity as double) * cast(cstlayercostspeounitcost as double)) as decimal(32,4)) =
cast(deb.costdistributionlineseoenteredcurrencyamount as decimal(32,4));


-- the below insert will include all the records whihc arent present in the final table 
with unique_records_filter as 
(
select distinct 
deb.transactionseoinventoryitemid as transactionseoinventoryitemid,
deb.transactionseoinventoryorgid as transactionseoinventoryorgid,
deb.cstlayercostspeorectrxnid as cstlayercostspeorectrxnid,
cast((cast(deb.COSTDISTRIBUTIONLINESEOENTEREDCURRENCYAMOUNT as decimal(38,6)) / cast(deb.COSTDISTRIBUTIONSEOLAYERQUANTITY as decimal(38,6)))  as decimal(38,6)) as cost_unit, 
deb.transactionseocreationdate as transactionseocreationdate,
deb.costdistributionlineseocostelementid as costdistributionlineseocostelementid,
deb.distributionlineid as distributionlineid
from  rlsf_prod.cost_trans_debit_delta deb  -- change table name accordingly
join rlsf_prod.cost_trans_rank_assign_delta assign on assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid 
where (deb.cstlayercostspeocosttransactiontype = "ADJUST" or deb.cstlayercostspeocosttransactiontype= "RECEIPT")
and cast(deb.costdistributionlineseoenteredcurrencyamount as double) <> 0 
and cast((cast(deb.costdistributionseolayerquantity as double) * cast(cstlayercostspeounitcost as double)) as decimal(32,4)) =
cast(deb.costdistributionlineseoenteredcurrencyamount as decimal(32,4))
)
insert into table rlsf_prod.ohq_inv_unit_cst_by_cost_rec
select uniq.* from unique_records_filter uniq
left anti join rlsf_prod.ohq_inv_unit_cst_by_cost_rec cost_rec on (
cost_rec.transactionseoinventoryitemid = uniq.transactionseoinventoryitemid and 
cost_rec.transactionseoinventoryorgid = uniq.transactionseoinventoryorgid and 
cost_rec.cstlayercostspeorectrxnid = uniq.cstlayercostspeorectrxnid );
