-- the firt insert will include all the latest date records from the final table 
insert into table rlsf_prod.ohq_inv_qty_by_cost_rec
select distinct
inventory_item_id as transactionseoinventoryitemid,
total_qty as costdistributionseolayerquantity,
org_id as transactionseoinventoryorgid,
date_ as transactionseocreationdate,
'' as transactionseocstinvtransactionid,
lc_p_eo_rec_trans_id as cstlayercostspeorectrxnid
from rlsf_prod.ohq_inv_as_of_date_with_layer_cost deb
join rlsf_prod.cost_trans_rank_assign_delta assign on assign.transactionseoinventoryitemid = deb.inventory_item_id and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber};


-- the second insert will insert only those records where there is a match between final table and delta table 
-- and creation date should be greater in delta table when compared to final table 
insert into table rlsf_prod.ohq_inv_qty_by_cost_rec 
select distinct 
deb.transactionseoinventoryitemid as transactionseoinventoryitemid,
cast(deb.costdistributionseolayerquantity as decimal(38,6)) as costdistributionseolayerquantity,
deb.transactionseoinventoryorgid,
deb.transactionseocreationdate as  transactionseocreationdate,
deb.transactionseocstinvtransactionid,
deb.cstlayercostspeorectrxnid
from  rlsf_prod.cost_trans_debit_delta deb  
join rlsf_prod.cost_trans_rank_assign_delta assign on assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
join rlsf_prod.ohq_inv_as_of_date_with_layer_cost final on (
final.inventory_item_id = deb.transactionseoinventoryitemid and 
final.org_id = deb.transactionseoinventoryorgid and 
final.lc_p_eo_rec_trans_id = deb.cstlayercostspeorectrxnid and 
deb.transactionseocreationdate > final.date_)
where deb.costdistributionlineseocostelementid='300000073384740'   
and (deb.cstlayercostspeocosttransactiontype = "ISSUE" or deb.cstlayercostspeocosttransactiontype= "RECEIPT");


with unique_records_filter as 
(
select distinct 
deb.transactionseoinventoryitemid as transactionseoinventoryitemid,
cast(deb.costdistributionseolayerquantity as decimal(38,6)) as costdistributionseolayerquantity,
deb.transactionseoinventoryorgid,
deb.transactionseocreationdate as  transactionseocreationdate,
deb.transactionseocstinvtransactionid,
deb.cstlayercostspeorectrxnid
from  rlsf_prod.cost_trans_debit_delta deb  
join rlsf_prod.cost_trans_rank_assign_delta assign on assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
where deb.costdistributionlineseocostelementid='300000073384740'   
and (deb.cstlayercostspeocosttransactiontype = "ISSUE" or deb.cstlayercostspeocosttransactiontype= "RECEIPT")
)

-- the below insert will include all the records whihc arent present in the final table 
insert into table rlsf_prod.ohq_inv_qty_by_cost_rec 
select uniq.* from unique_records_filter uniq
left anti join rlsf_prod.ohq_inv_qty_by_cost_rec cost_rec on (
cost_rec.transactionseoinventoryitemid = uniq.transactionseoinventoryitemid and 
cost_rec.transactionseoinventoryorgid = uniq.transactionseoinventoryorgid and 
cost_rec.cstlayercostspeorectrxnid = uniq.cstlayercostspeorectrxnid );