invalidate metadata rlsf_prod.oracle_items_parquet;
invalidate metadata rlsf_prod.org_master_parquet;
invalidate metadata rlsf_prod.inv_master_distinct_parquet;
invalidate metadata rlsf_prod.sch_bi_extract;
invalidate metadata rlsf_prod.cost_trans_prod_allorgs_parquet;

insert overwrite table rlsf_prod.cost_trans_debit_delta
select
cast(dist_cost_element_id as string) as costdistributionlineseocostelementid ,
cast(distribution_id  as string) as distributionlineid,
cast(inventory_item_id as string) as transactionseoinventoryitemid ,
cast(layer_quantity as string) as costdistributionseolayerquantity,
cast(org_id as string) as transactionseoinventoryorgid,
cast(TXn_CREATION_DATE  as string) as transactionseocreationdate, -- yet to get the mapping.
cast(cst_inv_transaction_id  as string) as transactionseocstinvtransactionid,
cast(rec_trxn_id as string) as cstlayercostspeorectrxnid,
cast(dist_cost_txn_type as string) as cstlayercostspeocosttransactiontype ,
cast(entered_currency_amount as string) as costdistributionlineseoenteredcurrencyamount ,
cast(layer_unit_cost as string) as cstlayercostspeounitcost
from  rlsf_prod.cost_trans_prod_allorgs_parquet -- table name needs to be changed
where trim(distribution_dr_cr_sign) = "DR"
and trim(val_unit_id)<>'' -- we will uncomment this condition  if only when the busniess confirms
;
