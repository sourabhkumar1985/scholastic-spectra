insert into table rlsf_prod.ohq_inv_oh_qty_by_cost_rec
select 
date_,
oracle_id as org_id,
lc_p_eo_rec_trans_id,
sum(layer_qty_sum) over (partition by trans_eo_inventory_item_id,lc_p_eo_rec_trans_id,oracle_id order by date_ asc rows between unbounded preceding and current row)
as layer_qty_sum,
trans_eo_inventory_item_id 
from rlsf_prod.ohq_inv_total_qty_by_cost_rec cost
join rlsf_prod.cost_trans_rank_assign assign on assign.transactionseoinventoryitemid = cost.trans_eo_inventory_item_id and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber};

