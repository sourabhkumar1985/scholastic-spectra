insert overwrite table rlsf_prod.cost_trans_rank_assign
select 
transactionseoinventoryitemid,
row_number() OVER (
    ORDER BY transactionseoinventoryitemid) as ranknumber  
from (select distinct   
transactionseoinventoryitemid
from rlsf_prod.cost_trans_debit ) ll;
