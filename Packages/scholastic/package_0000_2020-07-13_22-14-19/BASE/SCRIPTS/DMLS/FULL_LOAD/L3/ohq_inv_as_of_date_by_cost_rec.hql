insert into table rlsf_prod.ohq_inv_as_of_date_by_cost_rec
select  
items.oracle_id as oracle_item ,
items.isbn13 as isbn13 ,
items.short_id as border_item ,
items.opus_num as opus_id ,
org.oicode as cost_center,
t1.org_id,
t1.lc_p_eo_rec_trans_id,
t1.trans_eo_inventory_item_id,
sum(cast(t1.layer_qty_sum as decimal(38,6))) as totalsum,
t1.date_,
config.run_date  as run_date 
from rlsf_prod.ohq_inv_oh_qty_by_cost_rec t1
join rlsf_prod.cost_trans_rank_assign assign on assign.transactionseoinventoryitemid = t1.trans_eo_inventory_item_id and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
join (select org_id,lc_p_eo_rec_trans_id,trans_eo_inventory_item_id,max(date_) as maxdate
from rlsf_prod.ohq_inv_oh_qty_by_cost_rec
group by org_id,lc_p_eo_rec_trans_id,trans_eo_inventory_item_id) maxdate
on 
t1.org_id=maxdate.org_id and t1.lc_p_eo_rec_trans_id=maxdate.lc_p_eo_rec_trans_id and 
t1.trans_eo_inventory_item_id = maxdate.trans_eo_inventory_item_id and t1.date_=maxdate.maxdate
left outer join rlsf_prod.inv_master_distinct_parquet  dist on dist.inventory_item_id = t1.trans_eo_inventory_item_id
left outer join rlsf_prod.oracle_items_parquet items on items.oracle_id=dist.oracle_item_id
left outer join rlsf_prod.org_master_parquet org on trim(org.oiorgid) = trim(t1.org_id)
cross join rlsf_prod.ohq_config_date config 
group by t1.org_id,t1.lc_p_eo_rec_trans_id,t1.trans_eo_inventory_item_id,t1.date_,
oracle_item,isbn13,border_item,opus_id,cost_center,config.run_date;
