insert into table rlsf_prod.ohq_inv_unit_cst_by_cost_rec
select distinct 
deb.transactionseoinventoryitemid,
deb.transactionseoinventoryorgid,
deb.cstlayercostspeorectrxnid,
cast(deb.cstlayercostspeounitcost as decimal(38,6)) as cost_unit, 
deb.dist_lines_last_update_date  as transactionseocreationdate, -- changed the column mapping to dist_lines_last_update_date 
deb.costdistributionlineseocostelementid,
deb.distributionlineid,
deb.cstlayercostspeocosttransactiontype
from  rlsf_prod.cost_trans_debit deb  
join rlsf_prod.cost_trans_rank_assign assign on assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
where (
(deb.cstlayercostspeocosttransactiontype = "ADJUST" and 
cast(deb.COSTDISTRIBUTIONSEOLAYERQUANTITY as decimal(38,6)) > 0) 
or deb.cstlayercostspeocosttransactiontype= "RECEIPT")
and cast(deb.costdistributionlineseoenteredcurrencyamount as double) <> 0 
;
