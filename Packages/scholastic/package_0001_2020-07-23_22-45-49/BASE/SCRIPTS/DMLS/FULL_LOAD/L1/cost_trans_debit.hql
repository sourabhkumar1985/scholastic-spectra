

insert overwrite table rlsf_prod.cost_trans_debit
select
cast(dist_cost_element_id as string) as costdistributionlineseocostelementid ,
cast(distribution_line_id  as string) as distributionlineid, -- revert if the values arent matching
cast(inventory_item_id as string) as transactionseoinventoryitemid ,
cast(layer_quantity as string) as costdistributionseolayerquantity,
cast(org_id as string) as transactionseoinventoryorgid,
cast(TXn_CREATION_DATE  as string) as transactionseocreationdate, -- yet to get the mapping.
cast(cst_inv_transaction_id  as string) as transactionseocstinvtransactionid,
cast(rec_trxn_id as string) as cstlayercostspeorectrxnid,
cast(dist_cost_txn_type as string) as cstlayercostspeocosttransactiontype ,
cast(entered_currency_amount as string) as costdistributionlineseoenteredcurrencyamount ,
cast(layer_unit_cost as string) as cstlayercostspeounitcost,
cast(dist_lines_last_update_date as string) as dist_lines_last_update_date
from  rlsf_prod.cost_trans_prod_allorgs_parquet trans  -- table name needs to be changed
where trim(distribution_dr_cr_sign) = "DR"
and trim(val_unit_id)<>'' -- we will uncomment this condition  if only when the busniess confirms
;
