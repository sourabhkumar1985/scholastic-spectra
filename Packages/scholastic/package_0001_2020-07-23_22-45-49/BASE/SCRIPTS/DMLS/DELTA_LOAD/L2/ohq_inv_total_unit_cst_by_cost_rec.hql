with maxdate_filter as (
select 
transactionseoinventoryitemid,
cstlayercostspeorectrxnid,
transactionseoinventoryorgid,
max(transactionseocreationdate) as maxdate ,
cstlayercostspeocosttransactiontype
from rlsf_prod.ohq_inv_unit_cst_by_cost_rec
group by 
transactionseoinventoryitemid,
cstlayercostspeorectrxnid,
transactionseoinventoryorgid,
cstlayercostspeocosttransactiontype
)


insert into table rlsf_prod.ohq_inv_total_unit_cst_by_cost_rec
select 
cost.transactionseoinventoryitemid as trans_eo_inventory_item_id,
cost.cstlayercostspeorectrxnid as lc_p_eo_rec_trans_id,
max(cost.transactionseocreationdate) as date_,
cost.transactionseoinventoryorgid as oracle_id ,
sum(cost.cost_unit) as cost_unit 
from rlsf_prod.ohq_inv_unit_cst_by_cost_rec cost
join rlsf_prod.cost_trans_rank_assign_delta assign on assign.transactionseoinventoryitemid = cost.transactionseoinventoryitemid  and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
join maxdate_filter fil on fil.transactionseoinventoryitemid = cost.transactionseoinventoryitemid 
and fil.cstlayercostspeorectrxnid = cost.cstlayercostspeorectrxnid and fil.transactionseoinventoryorgid = cost.transactionseoinventoryorgid
and cost.transactionseocreationdate =fil.maxdate and cost.cstlayercostspeocosttransactiontype = fil.cstlayercostspeocosttransactiontype

group by trans_eo_inventory_item_id,lc_p_eo_rec_trans_id,oracle_id;
